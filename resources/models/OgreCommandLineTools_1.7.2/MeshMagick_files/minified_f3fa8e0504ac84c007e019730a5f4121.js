/* Array
(
    [0] => Array
        (
            [0] => lib/tiki-js.js
            [1] => lib/comments/commentslib.js
            [2] => lib/menubuilder/menu.js
        )

)
 */var feature_no_cookie='n';function tr(str){if(typeof lang[str]=='string'){return lang[str];}else{return str;}}
var lang={};function browser(){var b=navigator.appName;if(b=="Netscape"){this.b="ns";}
else{this.b=b;}
this.version=navigator.appVersion;this.v=parseInt(this.version,10);this.ns=(this.b=="ns"&&this.v>=5);this.op=(navigator.userAgent.indexOf('Opera')>-1);this.safari=(navigator.userAgent.indexOf('Safari')>-1);this.op7=(navigator.userAgent.indexOf('Opera')>-1&&this.v>=7);this.ie56=(this.version.indexOf('MSIE 5')>-1||this.version.indexOf('MSIE 6')>-1);this.ie567=(this.version.indexOf('MSIE 5')>-1||this.version.indexOf('MSIE 6')>-1||this.version.indexOf('MSIE 7')>-1);this.iewin=(this.ie56&&navigator.userAgent.indexOf('Windows')>-1);this.iewin7=(this.ie567&&navigator.userAgent.indexOf('Windows')>-1);this.iemac=(this.ie56&&navigator.userAgent.indexOf('Mac')>-1);this.moz=(navigator.userAgent.indexOf('Mozilla')>-1);this.moz13=(navigator.userAgent.indexOf('Mozilla')>-1&&navigator.userAgent.indexOf('1.3')>-1);this.oldmoz=(navigator.userAgent.indexOf('Mozilla')>-1&&navigator.userAgent.indexOf('1.4')>-1||navigator.userAgent.indexOf('Mozilla')>-1&&navigator.userAgent.indexOf('1.5')>-1||navigator.userAgent.indexOf('Mozilla')>-1&&navigator.userAgent.indexOf('1.6')>-1);this.ns6=(navigator.userAgent.indexOf('Netscape6')>-1);this.docom=(this.ie56||this.ns||this.iewin||this.op||this.iemac||this.safari||this.moz||this.oldmoz||this.ns6);}
function toggleCols(id,zeromargin,maincol){var showit='show_'+escape(id);if(!zeromargin){zeromargin='';}
if(!id){id='';}
if(!maincol){maincol='col1';}
if(document.getElementById(id).style.display=="none"){document.getElementById(id).style.display="block";if(zeromargin=='left'){document.getElementById(maincol).style.marginLeft='';if(!document.getElementById(maincol).style.marginLeft){document.getElementById(maincol).style.marginLeft=$("#"+id).width()+"px";}
setSessionVar(showit,'y');}else{document.getElementById(maincol).style.marginRight='';if(!document.getElementById(maincol).style.marginRight){document.getElementById(maincol).style.marginRight=$("#"+id).width()+"px";}
setSessionVar(showit,'y');}}else{document.getElementById(id).style.display="none";if(zeromargin=='left'){document.getElementById(maincol).style.marginLeft='0';setSessionVar(showit,'n');}else{document.getElementById(maincol).style.marginRight='0';setSessionVar(showit,'n');}}}
function toggle_dynamic_var(name){name1='dyn_'+name+'_display';name2='dyn_'+name+'_edit';if(document.getElementById(name1).style.display=="none"){document.getElementById(name2).style.display="none";document.getElementById(name1).style.display="inline";}else{document.getElementById(name1).style.display="none";document.getElementById(name2).style.display="inline";}}
function chgArtType(){var articleType=document.getElementById('articletype').value;var typeProperties=articleTypes[articleType];var propertyList=['show_topline','y','show_subtitle','y','show_linkto','y','show_lang','y','show_author','y','use_ratings','y','heading_only','n','show_image_caption','y','show_pre_publ','y','show_post_expire','y','show_image','y','show_expdate','y'];if(typeof articleCustomAttributes!='undefined'){propertyList=propertyList.concat(articleCustomAttributes);}
var l=propertyList.length,property,value;for(var i=0;i<l;i++){property=propertyList[i++];value=propertyList[i];if(typeProperties[property]==value||(!typeProperties[property]&&value=="n")){display="";}else{display="none";}
if(document.getElementById(property)){document.getElementById(property).style.display=display;}else{j=1;while(document.getElementById(property+'_'+j)){document.getElementById(property+'_'+j).style.display=display;j++;}}}}
function chgMailinType(){if(document.getElementById('mailin_type').value!='article-put'){document.getElementById('article_topic').style.display="none";document.getElementById('article_type').style.display="none";}else{document.getElementById('article_topic').style.display="";document.getElementById('article_type').style.display="";}}
function toggleSpan(id){if(document.getElementById(id).style.display=="inline"){document.getElementById(id).style.display="none";}else{document.getElementById(id).style.display="inline";}}
function toggleBlock(id){if(document.getElementById(id).style.display=="none"){document.getElementById(id).style.display="block";}else{document.getElementById(id).style.display="none";}}
function toggleTrTd(id){if(document.getElementById(id).style.display=="none"){document.getElementById(id).style.display="";}else{document.getElementById(id).style.display="none";}}
function showTocToggle(){if(document.createTextNode){var linkHolder=document.getElementById('toctitle');if(!linkHolder){return;}
var outerSpan=document.createElement('span');outerSpan.className='toctoggle';var toggleLink=document.createElement('a');toggleLink.id='togglelink';toggleLink.className='internal';toggleLink.href='javascript:toggleToc()';toggleLink.appendChild(document.createTextNode(tocHideText));outerSpan.appendChild(document.createTextNode('['));outerSpan.appendChild(toggleLink);outerSpan.appendChild(document.createTextNode(']'));linkHolder.appendChild(document.createTextNode(' '));linkHolder.appendChild(outerSpan);if(getCookie("hidetoc")=="1"){toggleToc();}}}
function changeText(el,newText){if(el.innerText){el.innerText=newText;}else if(el.firstChild&&el.firstChild.nodeValue){el.firstChild.nodeValue=newText;}}
function toggleToc(){var toc=document.getElementById('toc').getElementsByTagName('ul')[0];var toggleLink=document.getElementById('togglelink');if(toc&&toggleLink&&toc.style.display=='none'){changeText(toggleLink,tocHideText);toc.style.display='block';setCookie("hidetoc","0");}else{changeText(toggleLink,tocShowText);toc.style.display='none';setCookie("hidetoc","1");}}
function chgTrkFld(f,o){var opt=0;document.getElementById('z').style.display="none";document.getElementById('zDescription').style.display="";document.getElementById('zStaticText').style.display="none";document.getElementById('zStaticTextToolbars').style.display="none";for(var i=0;i<f.length;i++){var c=f.charAt(i);if(document.getElementById(c)){var ichoiceParent=document.getElementById('itemChoicesRow');var ichoice=document.getElementById(c+'itemChoices');if(c==o){document.getElementById(c).style.display="";document.getElementById('z').style.display="block";if(c=='S'){document.getElementById('zDescription').style.display="none";document.getElementById('zStaticText').style.display="";document.getElementById('zStaticTextToolbars').style.display="";}
if(ichoice){ichoice.style.display="";ichoiceParent.style.display="";}else{ichoiceParent.style.display="none";}}else{document.getElementById(c).style.display="none";if(ichoice){ichoice.style.display="none";}}}}}
function chgTrkLingual(item){document.getElementById("multilabelRow").style.display=(item=='t'||item=='a')?'':'none';}
function multitoggle(f,o){for(var i=0;i<f.length;i++){if(document.getElementById('fid'+f[i])){if(f[i]==o){document.getElementById('fid'+f[i]).style.display="block";}else{document.getElementById('fid'+f[i]).style.display="none";}}}}
function setMenuCon(foo){var it=foo.split(",");document.getElementById('menu_url').value=it[0];document.getElementById('menu_name').value=it[1];if(it[2]){document.getElementById('menu_section').value=it[2];}else{document.getElementById('menu_section').value='';}
if(it[3]){document.getElementById('menu_perm').value=it[3];}else{document.getElementById('menu_perm').value='';}}
function genPass(w1){vo="aeiouAEU";co="bcdfgjklmnprstvwxzBCDFGHJKMNPQRSTVWXYZ0123456789_$%#";s=Math.round(Math.random());l=8;p='';for(i=0;i<l;i++){if(s){letter=vo.charAt(Math.round(Math.random()*(vo.length-1)));s=0;}else{letter=co.charAt(Math.round(Math.random()*(co.length-1)));s=1;}
p=p+letter;}
document.getElementById(w1).value=p;}
function setUserModule(foo1){document.getElementById('usermoduledata').value=foo1;}
function replaceLimon(vec){document.getElementById(vec[0]).value=document.getElementById(vec[0]).value.replace(vec[1],vec[2]);}
function setSelectionRange(textarea,selectionStart,selectionEnd){$(textarea).selection(selectionStart,selectionEnd);}
function getTASelection(textarea){var ta_id=$(textarea).attr("id"),r,cked;if($('#cke_contents_'+ta_id).length!==0){cked=typeof CKEDITOR!=='undefined'?CKEDITOR.instances[ta_id]:null;if(cked){var sel=cked.getSelection();if(sel&&sel.getType()===CKEDITOR.SELECTION_TEXT){if(CKEDITOR.env.ie){output=sel.document.$.selection.createRange().text;}else{output=sel.getNative().toString();}
return output;}}}else{if(typeof $(textarea).attr("selectionStartSaved")!='undefined'&&$(textarea).attr("selectionStartSaved")){return textarea.value.substring($(textarea).attr("selectionStartSaved"),$(textarea).attr("selectionEndSaved"));}else if(typeof textarea.selectionStart!='undefined'){return textarea.value.substring(textarea.selectionStart,textarea.selectionEnd);}else{r=document.selection.createRange();return r.text;}}}
var ieFirstTimeInsertKludge=null;function storeTASelection(area_id){if($('#cke_contents_'+area_id).length===0){var $el=$("#"+area_id);var sel=$el.selection();$el.attr("selectionStartSaved",sel.start).attr("selectionEndSaved",sel.end).attr("scrollTopSaved",$el.attr("scrollTop"));}
if(ieFirstTimeInsertKludge===null){ieFirstTimeInsertKludge=true;}}
function setCaretToPos(textarea,pos){setSelectionRange(textarea,pos,pos);}
function getCaretPos(textarea){if(typeof textarea.selectionEnd!='undefined'){return textarea.selectionEnd;}else if(document.selection){textarea.focus();var range=document.selection.createRange();if(range===null){return 0;}
var re=textarea.createTextRange();var rc=re.duplicate();re.moveToBookmark(range.getBookmark());rc.setEndPoint('EndToStart',re);return rc.text.length?rc.text.length:0;}else{return 0;}}
function insertAt(elementId,replaceString,blockLevel,perLine,replaceSelection){var $textarea=$('#'+elementId);var toBeReplaced=/text|page|area_id/g;var hiddenParents=$textarea.parents('fieldset:hidden:last');if(hiddenParents.length){hiddenParents.show();}
if($('#cke_contents_'+elementId).length!==0){var cked=typeof CKEDITOR!=='undefined'?CKEDITOR.instances[elementId]:null;if(cked){var isPlugin=replaceString.match(/^\s?\{/m);if(isPlugin){isPlugin=replaceString.match(/\}\s?$/m);}
isPlugin=isPlugin&&isPlugin.length>0;var sel=cked.getSelection(),rng;if(sel){rng=sel.getRanges();if(rng.length){rng=rng[0];}}
var plugin_el;if(isPlugin&&rng&&!rng.collapsed){var com=cked.getSelection().getStartElement();if(typeof com!=='undefined'&&com&&com.$){while(com.$.nextSibling&&com.$!==rng.endContainer.$){com=new CKEDITOR.dom.element(com.$.nextSibling);if($(com.$).hasClass("tiki_plugin")||$(com.$).find(".tiki_plugin").length===0){break;}}
if(!$(com.$).hasClass("tiki_plugin")){plugin_el=$(com.$).find(".tiki_plugin");if(plugin_el.length==1){com=new CKEDITOR.dom.element(plugin_el[0]);}else{plugin_el=$(com.$).parents(".tiki_plugin");if(plugin_el.length==1){com=new CKEDITOR.dom.element(plugin_el[0]);}else{var plugin_type=replaceString.match(/^\s?\{([\w]+)/);if(plugin_type.length>1){plugin_type=plugin_type[1].toLowerCase();}
plugin_el=$(com.$).find("[plugin="+plugin_type+"].tiki_plugin");if(plugin_el.length==1){com=new CKEDITOR.dom.element(plugin_el[0]);}else{}}}}}
if($(com.$).hasClass("tiki_plugin")){$(com.$).replaceWith(document.createTextNode(replaceString));cked.reParse();return;}}}
cked.insertText(replaceString);if(isPlugin||replaceString.match(/^\s?\(\(.*?\)\)\s?$/)){cked.reParse();}
return;}
if(!$textarea.length&&elementId==="fgal_picker"){$(".cke_dialog_contents").find("input:first").val(replaceString.replace("&amp;","&"));return;}else if($textarea.is(":input")&&elementId==="fgal_picker_id"){$textarea.val(replaceString);return;}
$textarea[0].focus();var val=$textarea.val();var selection=$textarea.selection();var scrollTop=$textarea[0].scrollTop;if(selection.start===0&&selection.end===0&&typeof $textarea.attr("selectionStartSaved")!='undefined'){if($textarea.attr("selectionStartSaved")){selection.start=$textarea.attr("selectionStartSaved");selection.end=$textarea.attr("selectionEndSaved");if($textarea.attr("scrollTopSaved")){scrollTop=$textarea.attr("scrollTopSaved");$textarea.attr("scrollTopSaved","");}
$textarea.attr("selectionStartSaved","").attr("selectionEndSaved","");}else{selection.start=getCaretPos($textarea[0]);selection.end=selection.start;}}
var lines,startoff=0,endoff=0;if($textarea[0].createTextRange&&$textarea[0].value!==val){val=$textarea[0].value;if(val.substring(selection.start,selection.start+1)==="\n"){selection.start++;}
lines=val.substring(0,selection.start).match(/\r\n/g);if(lines){startoff-=lines.length;}}
var selectionStart=selection.start;var selectionEnd=selection.end;if(blockLevel){selectionStart=val.lastIndexOf("\n",selectionStart-1)+1;var blockEnd=val.indexOf("\r",selectionEnd);if(blockEnd<0){selectionEnd=val.indexOf("\n",selectionEnd);}else{selectionEnd=blockEnd;}
if(selectionEnd<0){selectionEnd=val.length;}}
var newString='';if((selectionStart!=selectionEnd)){if(perLine){lines=val.substring(selectionStart,selectionEnd).split("\n");for(k=0;lines.length>k;++k){if(lines[k].length!==0){newString+=replaceString.replace(toBeReplaced,lines[k]);}
if(k!=lines.length-1){newString+="\n";}}}else{if(replaceSelection){newString=replaceString;}else if(replaceString.match(toBeReplaced)){newString=replaceString.replace(toBeReplaced,val.substring(selectionStart,selectionEnd));}else{newString=replaceString+'\n'+val.substring(selectionStart,selectionEnd);}}
$textarea.val(val.substring(0,selectionStart)
+newString
+val.substring(selectionEnd));lines=newString.match(/\r\n/g);if(lines){endoff-=lines.length;}
setSelectionRange($textarea[0],selectionStart+startoff,selectionStart+startoff+newString.length+endoff);}else{$textarea.val(val.substring(0,selectionStart)
+replaceString
+val.substring(selectionEnd));lines=replaceString.match(/\r\n/g);if(lines){endoff-=lines.length;}
setCaretToPos($textarea[0],selectionStart+startoff+replaceString.length+endoff);}
$textarea.attr("scrollTop",scrollTop);if($.browser.msie&&ieFirstTimeInsertKludge){setTimeout(function(){if(newString.length){setSelectionRange($textarea[0],parseInt(selectionStart,10)+parseInt(startoff,10),parseInt(selectionStart,10)+parseInt(startoff,10)+newString.length+parseInt(endoff,10));}
$textarea.attr("scrollTop",scrollTop);},1000);ieFirstTimeInsertKludge=false;}
if(hiddenParents.length){hiddenParents.hide();}
if(typeof auto_save_id!="undefined"&&auto_save_id.length>0&&typeof auto_save=='function'){auto_save(elementId,auto_save_id[0]);}}
function setUserModuleFromCombo(id,textarea){document.getElementById(textarea).value=document.getElementById(textarea).value
+document.getElementById(id).options[document.getElementById(id).selectedIndex].value;}
function toggle(foo){var display=$("#"+foo).css('display');if(display=="none"){show(foo,true,"menu");}else{if(display=="block"){hide(foo,true,"menu");}else{show(foo,true,"menu");}}}
function flip_thumbnail_status(id){var elem=document.getElementById(id);if(elem.className=='thumbnailcontener'){elem.className+=' thumbnailcontenerchecked';}else{elem.className='thumbnailcontener';}}
function flip_class(itemid,class1,class2){var elem=document.getElementById(itemid);if(elem&&typeof elem!='undefined'){elem.className=elem.className==class1?class2:class1;setCookie('flip_class_'+itemid,elem.className);}}
function tikitabs(focus,max,ini){var didit=false,didone=false;if(!ini){ini=1;}
for(var i=ini;i<=max;i++){var tabname='tab'+i;var content='content'+i;if(document.getElementById(tabname)&&typeof document.getElementById(tabname)!='undefined'){if(i==focus){show(content);setCookie('tab',focus);document.getElementById(tabname).className='tabmark';document.getElementById(tabname).className+=' tabactive';didit=true;}else{hide(content);document.getElementById(tabname).className='tabmark';document.getElementById(tabname).className+=' tabinactive';}
if(!didone){didone=true;}}}
if(didone&&!didit){show('content'+ini);setCookie('tab',ini);document.getElementById('tab'+ini).className='tabmark';document.getElementById('tab'+ini).className+=' tabactive';}}
function setfolderstate(foo,def,img,status){if(!status){status=getCookie(foo,"menu","o");}
if(!img){if(document.getElementsByName('icn'+foo)[0].src.search(/[\\\/]/)){img=document.getElementsByName('icn'+foo)[0].src.replace(/.*[\\\/]([^\\\/]*)$/,"$1");}else{img='folder.png';}}
var src=img;if(status=='c'){hide(foo,false,"menu");}else{show(foo,false,"menu");}
if(status=='c'&&def!='d'){src=src.replace(/^o/,'');}else if(status!='c'&&def=='d'&&src.indexOf('o')!==0){src='o'+img;}
document.getElementsByName('icn'+foo)[0].src=document.getElementsByName('icn'+foo)[0].src.replace(/[^\\\/]*$/,src);}
function setheadingstate(foo){var status=getCookie(foo,"showhide_headings");if(status=="o"){show(foo);collapseSign("flipper"+foo);}else{if(!document.getElementById(foo).style.display=="none"){hide(foo);expandSign("flipper"+foo);}}}
function setsectionstate(foo,def,img,status){if(!status){status=getCookie(foo,"menu","o");}
if(status=="o"){show(foo);if(img){src="o"+img;}}else if(status!="c"&&def!='d'){show(foo);if(img){src="o"+img;}}else{hide(foo);if(img){src=img;}}
if(img&&document.getElementsByName('icn'+foo).length){document.getElementsByName('icn'+foo)[0].src=document.getElementsByName('icn'+foo)[0].src.replace(/[^\\\/]*$/,src);}}
function icntoggle(foo,img){if(!img){if($("#icn"+foo).attr("src").search(/[\\\/]/)){img=$("#icn"+foo).attr("src").replace(/.*[\\\/]([^\\\/]*)$/,"$1");}else{img='folder.png';}}
if($("#"+foo+":hidden").length){show(foo,true,"menu");$("#icn"+foo).attr("src",$("#icn"+foo).attr("src").replace(/[^\\\/]*$/,'o'+img));}else{hide(foo,true,"menu");img=img.replace(/(^|\/|\\)o(.*)$/,'$1$2');$("#icn"+foo).attr("src",$("#icn"+foo).attr("src").replace(/[^\\\/]*$/,img));}}
function getHttpRequest(method,url,async)
{if(async===undefined){async=false;}
var request;if(window.XMLHttpRequest){request=new XMLHttpRequest();}else if(window.ActiveXObject)
{try
{request=new ActiveXObject("Microsoft.XMLHTTP");}
catch(ex)
{request=new ActiveXObject("MSXML2.XMLHTTP");}}
else{return false;}
if(!request){return false;}
request.open(method,url,async);return request;}
function setSessionVar(name,value){var request=getHttpRequest("GET","tiki-cookie-jar.php?"+name+"="+escape(value));request.send('');tiki_cookie_jar[name]=value;}
function setCookie(name,value,section,expires,path,domain,secure){if(getCookie(name,section)==value){return true;}
if(!expires){expires=new Date();expires.setFullYear(expires.getFullYear()+1);}
if(expires==="session"){expires="";}
if(feature_no_cookie=='y'){var request=getHttpRequest("GET","tiki-cookie-jar.php?"+name+"="+escape(value));try{request.send('');tiki_cookie_jar[name]=value;return true;}
catch(ex){setCookieBrowser(name,value,section,expires,path,domain,secure);return false;}}
else{setCookieBrowser(name,value,section,expires,path,domain,secure);return true;}}
function setCookieBrowser(name,value,section,expires,path,domain,secure){if(section){valSection=getCookie(section);name2="@"+name+":";if(valSection){if(new RegExp(name2).test(valSection)){valSection=valSection.replace(new RegExp(name2+"[^@;]*"),name2+value);}else{valSection=valSection+name2+value;}
setCookieBrowser(section,valSection,null,expires,path,domain,secure);}
else{valSection=name2+value;setCookieBrowser(section,valSection,null,expires,path,domain,secure);}}
else{var curCookie=name+"="+escape(value)+((expires)?"; expires="+expires.toGMTString():"")
+((path)?"; path="+path:"")+((domain)?"; domain="+domain:"")+((secure)?"; secure":"");document.cookie=curCookie;}}
function getCookie(name,section,defval){if(feature_no_cookie=='y'&&(window.XMLHttpRequest||window.ActiveXObject)&&typeof tiki_cookie_jar!="undefined"&&tiki_cookie_jar.length>0){if(typeof tiki_cookie_jar[name]=="undefined"){return defval;}
return tiki_cookie_jar[name];}
else{return getCookieBrowser(name,section,defval);}}
function getCookieBrowser(name,section,defval){if(typeof defval==="undefined"){defval=null;}
if(section){var valSection=getCookieBrowser(section);if(valSection){var name2="@"+name+":";var val=valSection.match(new RegExp(name2+"([^@;]*)"));if(val){return unescape(val[1]);}else{return defval;}}else{return defval;}}else{var dc=document.cookie;var prefix=name+"=";var begin=dc.indexOf("; "+prefix);if(begin==-1){begin=dc.indexOf(prefix);if(begin!==0){return defval;}}else{begin+=2;}
var end=document.cookie.indexOf(";",begin);if(end==-1){end=dc.length;}
return unescape(dc.substring(begin+prefix.length,end));}}
function deleteCookie(name,section,expires,path,domain,secure){if(section){valSection=getCookieBrowser(section);name2="@"+name+":";if(valSection){if(new RegExp(name2).test(valSection)){valSection=valSection.replace(new RegExp(name2+"[^@;]*"),"");setCookieBrowser(section,valSection,null,expires,path,domain,secure);}}}
else{document.cookie=name+"="
+((path)?"; path="+path:"")+((domain)?"; domain="+domain:"")+"; expires=Thu, 01-Jan-70 00:00:01 GMT";}}
function fixDate(date){var base=new Date(0);var skew=base.getTime();if(skew>0){date.setTime(date.getTime()-skew);}}
function flipWithSign(foo){if(document.getElementById(foo).style.display=="none"){show(foo,true,"showhide_headings");collapseSign("flipper"+foo);}else{hide(foo,true,"showhide_headings");expandSign("flipper"+foo);}}
function setFlipWithSign(foo){if(getCookie(foo,"showhide_headings","o")=="o"){collapseSign("flipper"+foo);show(foo);}else{expandSign("flipper"+foo);hide(foo);}}
function expandSign(foo){if(document.getElementById(foo)){document.getElementById(foo).firstChild.nodeValue="[+]";}}
function collapseSign(foo){if(document.getElementById(foo)){document.getElementById(foo).firstChild.nodeValue="[-]";}}
function go(o){if(o.options[o.selectedIndex].value!==""){location=o.options[o.selectedIndex].value;o.options[o.selectedIndex]=1;}
return false;}
function targetBlank(url,mode){var features='menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes';switch(mode){case'nw':break;case'popup':features='menubar=no,toolbar=no,location=no,directories=no,fullscreen=no,titlebar=no,hotkeys=no,status=no,scrollbars=yes,resizable=yes';break;default:break;}
blankWin=window.open(url,'_blank',features);}
function confirmTheLink(theLink,theMsg)
{if(typeof(window.opera)!='undefined'){return true;}
var is_confirmed=confirm(theMsg);return is_confirmed;}
function insertImgFile(elementId,fileId,oldfileId,type,page,attach_comment){textarea=$('#'+elementId)[0];fileup=$('input[name='+fileId+']')[0];oldfile=$('input[name='+oldfileId+']')[0];prefixEl=$('input[name=prefix]')[0];prefix="img/wiki_up/";if(!textarea||!fileup){return;}
if(prefixEl){prefix=prefixEl.value;}
filename=fileup.value;oldfilename=oldfile.value;if(filename==oldfilename||filename===""){return;}
oldfile.value=filename;if(filename.indexOf("/")>=0){dirs=filename.split("/");filename=dirs[dirs.length-1];}
if(filename.indexOf("\\")>=0){dirs=filename.split("\\");filename=dirs[dirs.length-1];}
if(filename.indexOf(":")>=0){dirs=filename.split(":");filename=dirs[dirs.length-1];}
if(type=="file"){str="{file name=\""+filename+"\"";var desc=$('#'+attach_comment).val();if(desc){str=str+" desc=\""+desc+"\"";}
str=str+"}";}else{str="{img src=\"img/wiki_up/"+filename+"\" }\n";}
insertAt(elementId,str);}
var img_form_count=2;function addImgForm(){var new_text=document.createElement('span');new_text.setAttribute('id','picfile'+img_form_count);new_text.innerHTML='<input name=\'picfile'+img_form_count+'\' type=\'file\' onchange=\'javascript:insertImgFile("editwiki","picfile'+img_form_count+'","hasAlreadyInserted","img")\'/><br />';document.getElementById('new_img_form').appendChild(new_text);needToConfirm=true;img_form_count++;}
function wiki3d_open(page,width,height){window.open('tiki-wiki3d.php?page='+page,'wiki3d','width='+width+',height='+height+',scrolling=no');}
function protectEmail(nom,domain,sep){return'<a class="wiki" href="mailto:'+nom+'@'+domain+'">'+nom+sep+domain+'</a>';}
browser();window.name='tiki';var fgals_window=null;function openFgalsWindow(filegal_manager_url,reload){if(fgals_window&&typeof fgals_window.document!="undefined"&&typeof fgals_window.document!="unknown"&&!fgals_window.closed){if(reload){fgals_window.location.replace(filegal_manager_url);}
fgals_window.focus();}else{fgals_window=window.open(filegal_manager_url,'_blank','menubar=1,scrollbars=1,resizable=1,height=500,width=800,left=50,top=50');}
$(window).unload(function(){fgals_window.close();});}
function wordCount(maxSize,source,cpt,message){var formcontent=source.value;str=formcontent.replace(/^\s+|\s+$/g,'');formcontent=str.split(/[^\S]+/);if(maxSize>0&&formcontent.length>maxSize){alert(message);source.value=source.value.substr(0,source.value.length-1);}else{document.getElementById(cpt).value=formcontent.length;}}
function charCount(maxSize,source,cpt,message){var formcontent=source.value;if(maxSize>0&&formcontent.length>maxSize){alert(message);source.value=source.value.substr(0,maxSize);}else{document.getElementById(cpt).value=formcontent.length;}}
function show_plugin_form(type,index,pageName,pluginArgs,bodyContent)
{var target=document.getElementById(type+index);var content=target.innerHTML;var form=build_plugin_form(type,index,pageName,pluginArgs,bodyContent);target.innerHTML='';target.appendChild(form);}
function popup_plugin_form(area_id,type,index,pageName,pluginArgs,bodyContent,edit_icon)
{if($.ui){return popupPluginForm(area_id,type,index,pageName,pluginArgs,bodyContent,edit_icon);}
var container=document.createElement('div');container.className='plugin-form-float';var textarea=$('#'+area_id)[0];var minimize=document.createElement('a');var icon=document.createElement('img');minimize.appendChild(icon);minimize.href='javascript:void(0)';container.appendChild(minimize);icon.src='pics/icons/cross.png';icon.style.position='absolute';icon.style.top='5px';icon.style.right='5px';icon.style.border='none';if(!index){index=0;}
if(!pageName){pageName='';}
if(!pluginArgs){pluginArgs={};}
if(!bodyContent){if(document.getTASelection){bodyContent=document.getTASelection(textarea);}else if(window.getTASelection){bodyContent=window.getTASelection(textarea);}else if(document.selection){bodyContent=document.selection.createRange().text;}else{bodyContent='';}}
var form=build_plugin_form(type,index,pageName,pluginArgs,bodyContent);form.onsubmit=function()
{var meta=tiki_plugins[type];var params=[];var edit=edit_icon;for(i=0;i<form.elements.length;i++){element=form.elements[i].name;var matches=element.match(/params\[(.*)\]/);if(matches===null){continue;}
var param=matches[1];var val=form.elements[i].value;if(val!==''){params.push(param+'="'+val+'"');}}
var blob='{'+type.toUpperCase()+'('+params.join(',')+')}'+(typeof form.content!='undefined'?form.content.value:'')+'{'+type.toUpperCase()+'}';if(edit){return true;}else{insertAt(area_id,blob);document.body.removeChild(container);}
return false;};minimize.onclick=function(){var edit=edit_icon;if(edit){edit.style.display='inline';}
document.body.removeChild(container);};document.body.appendChild(container);if(edit_icon){edit_icon.style.display='none';}
container.appendChild(form);handlePluginFieldsHierarchy(type);}
function build_plugin_form(type,index,pageName,pluginArgs,bodyContent)
{var form=document.createElement('form');form.method='post';form.action='tiki-wikiplugin_edit.php';form.className='wikiplugin_edit';var hiddenPage=document.createElement('input');hiddenPage.type='hidden';hiddenPage.name='page';hiddenPage.value=pageName;form.appendChild(hiddenPage);var hiddenType=document.createElement('input');hiddenType.type='hidden';hiddenType.name='type';hiddenType.value=type;form.appendChild(hiddenType);var hiddenIndex=document.createElement('input');hiddenIndex.type='hidden';hiddenIndex.name='index';hiddenIndex.value=index;form.appendChild(hiddenIndex);var meta=tiki_plugins[type];var header=document.createElement('h3');header.innerHTML=meta.name;form.appendChild(header);var desc=document.createElement('div');desc.innerHTML=meta.description;form.appendChild(desc);var table=document.createElement('table');table.className='normal';table.id='plugin_params';form.appendChild(table);for(param in meta.params){if(meta.params[param].advanced){var br=document.createElement('br');form.appendChild(br);var span_advanced_button=document.createElement('span');span_advanced_button.className='button';form.appendChild(span_advanced_button);var advanced_button=document.createElement('a');advanced_button.innerHTML=tr('Advanced options');advanced_button.onclick=function(){flip('plugin_params_advanced');};span_advanced_button.appendChild(advanced_button);var table_advanced=document.createElement('table');table_advanced.className='normal';table_advanced.style.display='none';table_advanced.id='plugin_params_advanced';form.appendChild(table_advanced);break;}}
var potentiallyExtraPluginArgs=pluginArgs;var rowNumber=0;var rowNumberAdvanced=0;for(param in meta.params)
{if(typeof(meta.params[param])!='object'||meta.params[param].name=='array'){continue;}
var row;if(meta.params[param].advanced&&!meta.params[param].required&&typeof pluginArgs[param]==="undefined"){row=table_advanced.insertRow(rowNumberAdvanced++);}else{row=table.insertRow(rowNumber++);}
build_plugin_form_row(row,param,meta.params[param].name,meta.params[param].required,pluginArgs[param],meta.params[param].description,meta.params[param]);delete potentiallyExtraPluginArgs[param];}
for(extraArg in potentiallyExtraPluginArgs){if(extraArg===''){continue;}
row=table.insertRow(rowNumber++);build_plugin_form_row(row,extraArg,extraArg,'extra',pluginArgs[extraArg],extraArg);}
var bodyRow=table.insertRow(rowNumber++);var bodyCell=bodyRow.insertCell(0);var bodyField=document.createElement('textarea');bodyField.cols='70';bodyField.rows='12';var bodyDesc=document.createElement('div');if(meta.body){bodyDesc.innerHTML=meta.body;}else{bodyRow.style.display='none';}
bodyField.name='content';bodyField.value=bodyContent;bodyRow.className='formcolor';bodyCell.appendChild(bodyDesc);bodyCell.appendChild(bodyField);bodyCell.colSpan='2';var submitRow=table.insertRow(rowNumber++);var submitCell=submitRow.insertCell(0);var submit=document.createElement('input');submit.type='submit';submitCell.colSpan=2;submitCell.appendChild(submit);submitCell.className='submit';return form;}
function build_plugin_form_row(row,name,label_name,requiredOrSpecial,value,description,paramDef)
{var label=row.insertCell(0);var field=row.insertCell(1);row.className='formcolor';row.id='param_'+name;label.innerHTML=label_name;label.style.width='130px';switch(requiredOrSpecial){case(true):label.style.fontWeight='bold';break;case('extra'):label.style.fontStyle='italic';}
var input;if(paramDef&&paramDef.options){input=document.createElement('select');input.name='params['+name+']';for(var o=0;o<paramDef.options.length;o++){var opt=document.createElement('option');opt.value=paramDef.options[o].value;var opt_text=document.createTextNode(paramDef.options[o].text);opt.appendChild(opt_text);if(value&&opt.value==value){opt.selected=true;}
input.appendChild(opt);}}else{input=document.createElement('input');input.type='text';input.name='params['+name+']';if(value){input.value=value;}}
var desc=document.createElement('div');desc.style.fontSize='x-small';desc.innerHTML=description;field.appendChild(input);if(paramDef&&paramDef.type=='image'){icon=document.createElement('img');icon.src='pics/icons/image.png';input.id=paramDef.area?paramDef.area:'fgal_picker';icon.onclick=function(){openFgalsWindowArea(paramDef.area?paramDef.area:'fgal_picker');};field.appendChild(icon);}else if(paramDef&&paramDef.type=='fileId'){var help=document.createElement('span');input.id=paramDef.area?paramDef.area:'fgal_picker';help.onclick=function(){openFgalsWindowArea(paramDef.area?paramDef.area:'fgal_picker');};help.innerHTML=" <a href='#'>"+tr('Pick a file.')+"</a>";field.appendChild(help);}
field.appendChild(desc);if(paramDef&&paramDef.filter){if(paramDef.filter=="pagename"){$(input).tiki("autocomplete","pagename");}else if(paramDef.filter=="groupname"){$(input).tiki("autocomplete","groupname",{multiple:true,multipleSeparator:"|"});}else if(paramDef.filter=="username"){$(input).tiki("autocomplete","username",{multiple:true,multipleSeparator:"|"});}else if(paramDef.filter=="date"){$(input).tiki("datepicker");}}}
function openFgalsWindowArea(area){openFgalsWindow('tiki-list_file_gallery.php?filegals_manager='+area,true);}
var m_strUpperCase="ABCDEFGHIJKLMNOPQRSTUVWXYZ";var m_strLowerCase="abcdefghijklmnopqrstuvwxyz";var m_strNumber="0123456789";var m_strCharacters="!@#$%^&*?_~";function checkPassword(strPassword)
{var nScore=0;if(strPassword.length<5)
{nScore+=5;}
else if(strPassword.length>4&&strPassword.length<8)
{nScore+=10;}
else if(strPassword.length>7)
{nScore+=25;}
var nUpperCount=countContain(strPassword,m_strUpperCase);var nLowerCount=countContain(strPassword,m_strLowerCase);var nLowerUpperCount=nUpperCount+nLowerCount;if(nUpperCount===0&&nLowerCount!==0)
{nScore+=10;}
else if(nUpperCount!==0&&nLowerCount!==0)
{nScore+=20;}
var nNumberCount=countContain(strPassword,m_strNumber);if(nNumberCount==1)
{nScore+=10;}
if(nNumberCount>=3)
{nScore+=20;}
var nCharacterCount=countContain(strPassword,m_strCharacters);if(nCharacterCount==1)
{nScore+=10;}
if(nCharacterCount>1)
{nScore+=25;}
if(nNumberCount!==0&&nLowerUpperCount!==0)
{nScore+=2;}
if(nNumberCount!==0&&nLowerUpperCount!==0&&nCharacterCount!==0)
{nScore+=3;}
if(nNumberCount!==0&&nUpperCount!==0&&nLowerCount!==0&&nCharacterCount!==0)
{nScore+=5;}
return nScore;}
function runPassword(strPassword,strFieldID)
{var nScore=checkPassword(strPassword);var ctlBar=document.getElementById(strFieldID+"_bar");var ctlText=document.getElementById(strFieldID+"_text");if(!ctlBar||!ctlText){return;}
ctlBar.style.width=nScore+"%";if(nScore>=90)
{var strIcon="<img src='pics/icons/accept.png' style='vertical-align:middle' alt='Very Secure' />";var strText=tr("Very Secure");var strColor="#0ca908";}
else if(nScore>=80)
{strIcon="<img src='pics/icons/accept.png' style='vertical-align:middle' alt='Secure' />";strText=tr("Secure");vstrColor="#0ca908";}
else if(nScore>=70)
{strIcon="<img src='pics/icons/accept.png' style='vertical-align:middle' alt='Very Strong' />";strText=tr("Very Strong");strColor="#0ca908";}
else if(nScore>=60)
{strIcon="<img src='pics/icons/accept.png' style='vertical-align:middle' alt='Strong' />";strText=tr("Strong");strColor="#0ca908";}
else if(nScore>=40)
{strIcon=" ";strText=tr("Average");strColor="#e3cb00";}
else if(nScore>=25)
{strIcon="<img src='pics/icons/exclamation.png' style='vertical-align:middle' alt='Weak' />";strText=tr("Weak");strColor="#ff0000";}
else
{strIcon="<img src='pics/icons/exclamation.png' style='vertical-align:middle' alt='Very weak' />";strText=tr("Very Weak");strColor="#ff0000";}
ctlBar.style.backgroundColor=strColor;ctlText.innerHTML="<span>"+strIcon+" "+tr("Strength")+": "+strText+"</span>";}
function countContain(strPassword,strCheck)
{var nCount=0;for(i=0;i<strPassword.length;i++)
{if(strCheck.indexOf(strPassword.charAt(i))>-1)
{nCount++;}}
return nCount;}
function checkPasswordsMatch(in1,in2,el){if($(in1).val().length&&$(in1).val()==$(in2).val()){$(el).html("<img src='pics/icons/accept.png' style='vertical-align:middle' alt='Secure' /><em>"+tr("Passwords match")+"</em>");return true;}else{$(el).html("");return false;}}
function pollsAddOption()
{var newOption=$('<input />').attr('type','text').attr('name','options[]');$('#tikiPollsOptions').append($('<div></div>').append(newOption));}
function pollsToggleQuickOptions()
{$('#tikiPollsQuickOptions').toggle();}
function hidedisabled(divid,value){if(value=='disabled'){document.getElementById(divid).style.display='none';}else{document.getElementById(divid).style.display='block';}}
function adjustThumbnails(){var i,j,h=0;var t=document.getElementById("thumbnails").childNodes;for(i=0;i<t.length;i++){if(t[i].className=="thumbnailcontener"){var t2=t[i].childNodes;for(j=0;j<t2.length;j++){if(t2[j].className=="thumbnail"){t2[j].style.height="100%";t2[j].style.overflow="visible";}}
if(t[i].offsetHeight>=h){h=t[i].offsetHeight;t[i].style.height=h+"px";}else if(t[i].offsetHeight<h){t[i].style.height=h+"px";}}}
for(i=0;i<t.length;i++){if(t[i].className=="thumbnailcontener"){if(t[i].offsetHeight<=h){t[i].style.height=h+"px";}else{break;}}}}
function open_webdav(url){if(typeof ActiveXObject!='undefined'){EditDocumentButton=new ActiveXObject("SharePoint.OpenDocuments.1");EditDocumentButton.EditDocument(url);}else{prompt(tr('URL to open this file with WebDAV'),url);}}
function ccsValueToInteger(str){var v=str.replace(/[^\d]*$/,"");if(v){v=parseInt(v,10);}
if(isNaN(v)){return 0;}else{return v;}}
function checkbox_list_check_all(form,list,checking){for(var checkbox in list){document.forms[form].elements[list[checkbox]].checked=checking;}}$(document).ready(function(){comments_fill_field('anonymous_name');comments_fill_field('anonymous_email');comments_fill_field('anonymous_website');$('#comments_previewComment').click(function(){action=$('#editpostform').attr('action');$('#editpostform').attr('action',action.replace('#comments','#form'));});$('#comments_postComment').click(function(){comments_anonymous_fields();});$('#comments_previewComment').click(function(){comments_anonymous_fields();});});function comments_anonymous_fields(){if($('#anonymous_name').length){setCookie('anonymous_name',$('#anonymous_name').val());}
if($('#anonymous_email').length){setCookie('anonymous_email',$('#anonymous_email').val());}
if($('#anonymous_website').length){setCookie('anonymous_website',$('#anonymous_website').val());}}
function comments_fill_field(id){var field_content=getCookie(id);if(field_content){$('#'+id).val(field_content);}}function sfHoverEvents(sfEls){var len=sfEls.length;for(var i=0;i<len;i+=1){sfEls[i].onmouseover=function(){this.className+=" sfhover";};sfEls[i].onmouseout=function(){this.className=this.className.replace(" sfhover","");};}}
function sfHover(){var ULs=document.getElementsByTagName("UL");var len=ULs.length;for(var i=0;i<len;i++){if(ULs[i].className.indexOf("cssmenu_horiz")!==-1){sfHoverEvents(ULs[i].getElementsByTagName("LI"));}
if(ULs[i].className.indexOf("cssmenu")!==-1){sfHoverEvents(ULs[i].getElementsByTagName("LI"));}
if(ULs[i].className.indexOf("cssmenu_vert")!==-1){sfHoverEvents(ULs[i].getElementsByTagName("LI"));}}}
if(window.attachEvent){window.attachEvent("onload",sfHover);}