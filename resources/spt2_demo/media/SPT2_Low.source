void vs_main( 
		float4 Pos : POSITION,
		float2 Tex	: TEXCOORD0,
		float2 Tex2 : TEXCOORD1,
		float delta : BLENDWEIGHT,

		out float4 oPos : POSITION,
		out float2 oTex : TEXCOORD0,
		out float4 oSplat12 : TEXCOORD1,
		out float4 oSplat34 : TEXCOORD2,
		out float2 oDetail : TEXCOORD3,
		out float4 oDepth   : TEXCOORD4,

		uniform float4x4 matWorldViewProj,
		uniform float4x4 matWorld,
		uniform float4 vViewPosition,
		uniform float4 vSplatScales,
		uniform float4 vDetailScales,
		uniform float morphFactor,
		uniform float fMaxDistance
		 )
{

	
	Pos.y += delta * morphFactor;


	oPos = mul( matWorldViewProj, Pos);
	float3 vWorldPos = mul(matWorld, Pos).xyz;
	//oNormal = Norm;

	//float3 EyeDir = vViewPosition.xyz - vWorldPos;
	float3 EyeDir = vWorldPos - vViewPosition.xyz;
	//EyeDir.z *= -1;
	float Depth = length(EyeDir);
   
	oDepth.xyz = EyeDir / Depth;
	oDepth.w = Depth / (fMaxDistance * 2.0f);


	oTex = Tex;

	oSplat12.xy = vWorldPos.xz / vSplatScales.x;
	oSplat12.zw = vWorldPos.xz / vSplatScales.y;
	oSplat34.xy = vWorldPos.xz / vSplatScales.z;
	oSplat34.zw = vWorldPos.xz / vSplatScales.w;
	

	oDetail = Tex * vDetailScales.x;
}

void ps_main( 
		float2 texCoord : TEXCOORD0,
		float4 Splat12 : TEXCOORD1,
		float4 Splat34 : TEXCOORD2,
		float2 Detail : TEXCOORD3,
		float4 Depth   : TEXCOORD4,

		out float4 oColor : COLOR0,

		uniform sampler2D texLightmap,
		uniform sampler2D texCoverage,
		uniform sampler2D texSplat,
		uniform sampler2D texSplat1,
		uniform sampler2D texSplat2,
		uniform sampler2D texSplat3,
		uniform sampler2D texSplat4,
		uniform sampler2D texDetail1,
		uniform samplerCUBE texSkyBox,
		uniform sampler1D texExtinction

		 )
{

   float4 fLight = tex2D(texLightmap, texCoord);

   float4 vCoverageColor = tex2D(texCoverage, texCoord);
   
    
   float4 vSplatColor = tex2D(texSplat, texCoord);
   vSplatColor /= vSplatColor.x + vSplatColor.y + vSplatColor.z + vSplatColor.w;
   float4 vSplat1 = tex2D(texSplat1, Splat12.xy) * vSplatColor.x;
   float4 vSplat2 = tex2D(texSplat2, Splat12.zw) * vSplatColor.y;
   float4 vSplat3 = tex2D(texSplat3, Splat34.xy) * vSplatColor.z;
   float4 vSplat4 = tex2D(texSplat4, Splat34.zw) * vSplatColor.w;
   
 
   oColor = vSplat1 + vSplat2 + vSplat3 + vSplat4;


   float fDetail = tex2D(texDetail1, Detail).x;

   oColor *= fDetail;
            
   oColor.xyz *= vCoverageColor.xyz * 0.9f;
   
   oColor *= (fLight.w * 2.0 + 0.5);

#ifdef USE_SCATTERING  
   float3 SkyColor = texCUBE(texSkyBox, Depth.xyz).xyz;
   float ExtinctionVal = tex1D(texExtinction, saturate(Depth.w)).x;
 
   oColor.xyz = lerp(SkyColor, oColor.xyz , ExtinctionVal);
#endif
}

