void ps_main( 
	float2 texCoord  : TEXCOORD0,
	out float4 oColor : COLOR,

	uniform sampler2D Texture0,
	uniform float3 vLightDir,
	uniform float2 vScale,
	uniform float fMaxDist,
	uniform float fSize
	 )
{
   const float fInvSize = 1.0f / fSize;

#ifdef USE_PS_3
   float3 vRay = normalize(vLightDir * float3(-1,-1,-1));
   vRay.xz /= vScale.x;
   vRay.y /= vScale.y;

   vRay = normalize(vRay) / 255.0f;

   
   float3 pos = float3(texCoord.x, tex2D(Texture0, texCoord).x, texCoord.y);
   float3 startPos = pos;
   
   float4 height;
   height.x = -1000.0f;

   float2 dxTex = ddx(texCoord);
   float2 dyTex = ddy(texCoord);
   //dxTex.x = ddx(texCoord.x);
   //dxTex.y = ddy(texCoord.y);
   //float4 dxyCoord = float4(0,0,dxTex.x,dyTex.x);
   
   int i=0;

   while(i < 255 && height.x < pos.y)
   {

     pos += vRay;   
     //dxyCoord.xy = pos.xz;
     //height = tex2D(Texture0, dxyCoord);
     height = tex2D(Texture0, pos.xz,dxTex,dyTex);

     i++;

   }   
   
   oColor = float4(1,1,1,1);
   if (height.x >= pos.y)
   {
      float f = saturate(length(startPos.y - pos.y) /fMaxDist) * 0.2f;
      //oColor = float4(f,f,f,1);
	oColor.w = f;
   }
#else
   oColor = float4(1,1,1,1);
#endif

   float A = tex2D(Texture0,texCoord + float2(fInvSize, 0.0f)).x;
   float B = tex2D(Texture0,texCoord + float2(0.0f, -fInvSize)).x;
   float C = tex2D(Texture0,texCoord + float2(-fInvSize,0.0f)).x;
   float D = tex2D(Texture0,texCoord + float2(0.0f, fInvSize)).x;
   
   float3 vNorm;
   vNorm.x = C - A;
   vNorm.z = D - B;
   vNorm.y = 2.0f * (vScale.x * fInvSize) / vScale.y;
   vNorm = normalize(vNorm);
   
   //oColor.xyz *= saturate(dot(vNorm, normalize(vLightDir * float3(-1,-1,1))));

   oColor.w *= saturate(dot(vNorm, normalize(vLightDir * float3(-1,-1,1))));
   oColor.xyz = (vNorm + 1.0f) * 0.5f;
    
   
   
}
