#include "AtmosphericCubeMap.h"
#include "Terrain.h"

#include <OgreMaterialManager.h>
#include <OgreTextureManager.h>
#include <OgreSceneManager.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreMeshManager.h>
#include <OgreHardwareBufferManager.h>
#include <OgreSubMesh.h>
#include <OgreEntity.h>
#include <OgreStringConverter.h>
#include <OgreLogManager.h>

#include <fstream>
#include <time.h>
#include <cassert>

using namespace SPT;
using namespace Ogre;
using namespace std;

namespace SPT
{

	

	AtmosphericCubeMap::AtmosphericCubeMap()
	  : mTerrain(0),
	    mSceneMgr(0),
	    mCamera(0),
	    mSkyNode(0),
	    mSkySphere(0)
	{

	}

	AtmosphericCubeMap::AtmosphericCubeMap( 
		Terrain* pTerrain,
		Ogre::SceneManager* pSceneMgr, 
		Ogre::Camera* pCamera, 
		const Ogre::String& strImportSettingsFile,
		const Ogre::String& strSkyBoxMatName,
		const Ogre::String& strTerrainMatName,
		const Ogre::String& strCubeMapTexName /*= "SPT_SkyBox_Tex"*/,
		const Ogre::String& strExtinctionTexName /*= "SPT_Ext_Tex"*/,
		const Ogre::String& strTerrainMaxDistanceParamName /*= "fMaxDistance"*/,
		const Ogre::String& strSkyBoxTexUnitName /*= "SkyBox"*/,
		const Ogre::String& strTerrainSkyTexUnitName /*= "SkyBox"*/,
		const Ogre::String& strTerrainExtinctionTexUnitName /*= "Extinction"*/ )
	  : mSkyBoxTexUnitName(strSkyBoxTexUnitName),
	    mTerrainSkyTexUnitName(strTerrainSkyTexUnitName),
	    mTerrainExtinctionTexUnitName(strTerrainExtinctionTexUnitName),
	    mTerrainMaxDistanceParamName(strTerrainMaxDistanceParamName),
	    mExtinctionTexName(strExtinctionTexName),
	    mCubeMapTexName(strCubeMapTexName),
	    mTerrain(pTerrain),
	    mSceneMgr(pSceneMgr),
	    mCamera(pCamera),
	    mSkyNode(0),
	    mSkySphere(0)
	{

		mSkyBoxMaterial = MaterialManager::getSingleton().getByName(strSkyBoxMatName);
		assert(!mSkyBoxMaterial.isNull());
		
		mTerrainMaterial = MaterialManager::getSingleton().getByName(strTerrainMatName);
		assert(!mSkyBoxMaterial.isNull());

		importSettings(strImportSettingsFile,true);

		

		/*updateCoefficients();

		updateExtinction();

		updateFaces();*/

		//createSphere(strCubeMapTexName + "_Mesh",100.0f);

		//mSkySphere = mSceneMgr->createEntity(strCubeMapTexName + "_Ent",strCubeMapTexName + "_Mesh");
		mSkySphere = mSceneMgr->createEntity(strCubeMapTexName + "_Ent","cube.mesh");
		mSkySphere->setMaterialName(strSkyBoxMatName);
		mSkySphere->setRenderQueueGroup(RENDER_QUEUE_SKIES_EARLY);
		mSkySphere->setCastShadows(false);
		mSkySphere->setQueryFlags(0);

		mSkyNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		mSkyNode->attachObject(mSkySphere);


	}


	AtmosphericCubeMap::AtmosphericCubeMap( 
		Terrain* pTerrain,
		Ogre::SceneManager* pSceneMgr,
		Ogre::Camera* pCamera,
		Ogre::Real fPlanetRadius, 
		Ogre::Real fAtmosphereRadius, 
		Ogre::Real fHG, 
		Ogre::Real fSunBrightness, 
		Ogre::Real fMaxDistance, 
		const Ogre::Vector3& vLightDir,
		const Ogre::Vector3& vMie,
		const Ogre::Vector3& vRaleigh,
		const Ogre::String& strSkyBoxMatName,
		const Ogre::String& strTerrainMatName,
		size_t iExtinctionTexSize /*= 1024*/,
		size_t iSkyBoxTexSize /*= 512*/,
		Ogre::Real fRaleighCoef /*= 0.2f*/, 
		Ogre::Real fMieCoef /*= 0.001*/, 
		const Ogre::String& strCubeMapTexName /*= "SPT_SkyBox_Tex"*/,
		const Ogre::String& strExtinctionTexName /*= "SPT_Ext_Tex"*/,
		const Ogre::String& strTerrainMaxDistanceParamName /*= "fMaxDistance"*/,
		const Ogre::String& strSkyBoxTexUnitName /*= "SkyBox"*/,
		const Ogre::String& strTerrainSkyTexUnitName /*= "SkyBox"*/,
		const Ogre::String& strTerrainExtinctionTexUnitName /*= "Extinction"*/)
	  : mPlanetRadius(fPlanetRadius),
	    mAtmosphereRadius(fAtmosphereRadius),
	    mHG(fHG),
	    mRaleighCoef(fRaleighCoef),
	    mMieCoef(fMieCoef),
	    mRaleigh(vRaleigh),
	    mMie(vMie),
	    mSunBrightness(fSunBrightness),
	    mMaxDistance(fMaxDistance),
	    mSkyBoxTexUnitName(strSkyBoxTexUnitName),
	    mTerrainSkyTexUnitName(strTerrainSkyTexUnitName),
	    mTerrainExtinctionTexUnitName(strTerrainExtinctionTexUnitName),
	    mTerrainMaxDistanceParamName(strTerrainMaxDistanceParamName),
	    mExtinctionTexName(strExtinctionTexName),
	    mCubeMapTexName(strCubeMapTexName),
	    mExtTextureSize(iExtinctionTexSize),
	    mLinTextureSize(iSkyBoxTexSize),
	    mLightDir(vLightDir),
	    mTerrain(pTerrain),
	    mSceneMgr(pSceneMgr),
	    mCamera(pCamera),
	    mSkyNode(0),
	    mSkySphere(0)
	{


		mSkyBoxMaterial = MaterialManager::getSingleton().getByName(strSkyBoxMatName);
		assert(!mSkyBoxMaterial.isNull());
		mTerrainMaterial = MaterialManager::getSingleton().getByName(strTerrainMatName);
		assert(!mTerrainMaterial.isNull());
		updateCoefficients();

		updateExtinction();

		updateFaces();

		//createSphere(strCubeMapTexName + "_Mesh",100.0f);

		//mSkySphere = mSceneMgr->createEntity(strCubeMapTexName + "_Ent",strCubeMapTexName + "_Mesh");
		mSkySphere = mSceneMgr->createEntity(strCubeMapTexName + "_Ent","cube.mesh");
		mSkySphere->setMaterialName(strSkyBoxMatName);
		mSkySphere->setRenderQueueGroup(RENDER_QUEUE_SKIES_EARLY);
		mSkySphere->setCastShadows(false);
		mSkySphere->setQueryFlags(0);

		mSkyNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		mSkyNode->attachObject(mSkySphere);

		//mSceneMgr->setSkyBox(true,strSkyBoxMatName);



	}



	AtmosphericCubeMap::~AtmosphericCubeMap()
	{
		if (mSkyNode)
		{
			mSkyNode->detachAllObjects();
			mSkyNode->removeAndDestroyAllChildren();

			mSkyNode->getParentSceneNode()->removeAndDestroyChild(mSkyNode->getName());
		}

		if (mSkySphere)
		{
			mSceneMgr->destroyEntity(mSkySphere);
			mSkySphere = 0;
		}

		if (mSkySphereMesh.isNull() == false)
		{
			mSkySphereMesh->unload();
			mSkySphereMesh.setNull();
		}

		if (mExtinction.isNull() == false)
		{
			TextureManager::getSingleton().remove(mExtinction->getName());
			mExtinction->unload();
			mExtinction.setNull();
		}

		if (mCubeMap.isNull() == false)
		{
			TextureManager::getSingleton().remove(mCubeMap->getName());
			mCubeMap->unload();
			mCubeMap.setNull();
		}
	}

	Ogre::Real AtmosphericCubeMap::CalculateSkyLength( const Ogre::Vector3 &inDirection )
	{
		Vector3 dir = inDirection;
		dir.normalise();

		Real b, c;

		b = 2.0f*mPlanetRadius*dir.y;
		c = mPlanetRadius*mPlanetRadius - mAtmosphereRadius*mAtmosphereRadius;

		// This is the positive result, and that's the one we want, so only solve for this quadratic solution.    

		return (-b + sqrtf(b*b -4.0f*c))/2.0f;
	}

	Ogre::Vector3 AtmosphericCubeMap::CalculateExtinctionAmount( Ogre::Real distance )
	{
		Vector3 Fex;

		Fex.x = expf(-mBetaExtinction.x*distance);
		Fex.y = expf(-mBetaExtinction.y*distance);
		Fex.z = expf(-mBetaExtinction.z*distance);
		return Fex;
	}

	Ogre::Vector3 AtmosphericCubeMap::CalculateLin( const Ogre::Vector3 &viewDirection )
	{

		// Get the cosine of the angle between the light direction and view direction.

		Real cosmTheta = ((-mLightDir).dotProduct(viewDirection)) / (mLightDir.length() * viewDirection.length());

		// Calculate the raleigh scattering at this angle

		// fR(mTheta) = 3/16pi * (1 + cos^2(mTheta))

		Real fR = 3.0f/(16.0f*Math::PI) * (2.0f + 0.5f * cosmTheta*cosmTheta);

		// Now the mie scattering

		Real denominator = sqrtf(1.0f+mHG*mHG + 2.0f*mHG*cosmTheta);
		Real fM = (1.0f-mHG)*(1.0f-mHG) / (4.0f*Math::PI * denominator*denominator*denominator);

		// Now the full Bsc(mTheta)

		Vector3 betaScattering = mBetaRaleighC*fR + mBetaMieC*fM;


		// We now need the color of the sun (Based on its position in the sky)


		Real skyLength = CalculateSkyLength(-mLightDir);

		Vector3 sunExtinction = CalculateExtinctionAmount(skyLength);

		Vector3 sunColor = sunExtinction * mSunBrightness;


		Real viewLength = CalculateSkyLength(viewDirection);

		Vector3 viewExtinction     = CalculateExtinctionAmount(viewLength);

		Vector3 finalColor = sunColor*betaScattering/mBetaExtinction*(1-viewExtinction);

		Real exposure = -1.0f;
		finalColor.x = 1.0f-expf(finalColor.x*exposure);
		finalColor.y = 1.0f-expf(finalColor.y*exposure);
		finalColor.z = 1.0f-expf(finalColor.z*exposure);
		// TODO: Take into account the amount of inscattering (Reduced by the shadow of the earth from the sun).

		return finalColor;
	}

	void AtmosphericCubeMap::updateExtinction()
	{
		if (mExtinction.isNull())
			createExtinction();

		updateCoefficients();
		// First up, lock the texture

		HardwarePixelBufferSharedPtr pixelBuffer = mExtinction->getBuffer();
		pixelBuffer->lock(HardwareBuffer::HBL_DISCARD);
		const PixelBox& pBox = pixelBuffer->getCurrentLock();

		uint8* pBuffer = static_cast<uint8*>(pBox.data);


		Real exp = (mBetaExtinction.x+mBetaExtinction.y+mBetaExtinction.z)/3.0f; // Crap average


		Real farthest = 1-expf(-exp*mMaxDistance*70);

		for(size_t i = 0; i < mExtTextureSize; i++)
		{
			Real s = ((Real)i) * mMaxDistance / ((Real)(mExtTextureSize-1));

			Real check = 1-expf(-exp*s*20.0);

			check /= farthest;
			check = 1-check;


			check = std::min(std::max(check,0.0f),1.0f) * 255.0f;
			*pBuffer++ = static_cast<uint8>(check);


		}

		pixelBuffer->unlock();
	}

	void AtmosphericCubeMap::updateFaces()
	{
		if (mCubeMap.isNull())
			createFaces();

		updateCoefficients();

		Real size = 0.5f * (Real)(mLinTextureSize-1);

		Real w, h;

		//Quaternion q(Degree(0),Vector3::UNIT_Z);

		//Quaternion yRot(Degree(mTheta),Vector3::UNIT_Y);
		//Quaternion xRot(Degree(-mEpsilon),Vector3::UNIT_X);

		//Quaternion qRot = (q * yRot) * xRot;
		//mLightDir = qRot * Vector3::UNIT_Z;
		//mLightDir.normalise();

		//pSunLight->setDirection(mLightDir * Vector3(-1,-1,-1));

		//uchar* pCubeMapBuffer = new uchar[mLinTextureSize * mLinTextureSize * 6 * 3];

		for (int face = 0;face < 6;face++)
		{


			HardwarePixelBufferSharedPtr pixelBuffer = mCubeMap->getBuffer(face);
			pixelBuffer->lock(HardwareBuffer::HBL_DISCARD);
			const PixelBox& pBox = pixelBuffer->getCurrentLock();

			uint8* pBuffer = static_cast<uint8*>(pBox.data);
			//uint8* pBuffer = pCubeMapBuffer + (mLinTextureSize * mLinTextureSize * 3 * face);


			for (size_t y=0;y<mLinTextureSize;y++)
			{
				h = (Real)y;
				h -= size;

				for (size_t x = 0; x < mLinTextureSize ; x++)
				{
					w = (Real)x;
					w -= size;      
					Vector3 normal;

					switch(face)
					{
					case CF_POS_X:
						normal = Vector3(size, -h, -w);
						break;
					case CF_NEG_X:
						normal = Vector3(-size, -h, w);
						break;
					case CF_POS_Y:
						normal = Vector3(w, size, h);
						break;
					case CF_NEG_Y:
						normal = Vector3(w, -size, -h);
						break;
					case CF_POS_Z:
						normal = Vector3(w, -h, size);
						break;
					case CF_NEG_Z:
						normal = Vector3(-w, -h, -size);
						break;
					default:
						break;
					}

					Vector3 color = CalculateLin(normal.normalisedCopy());

					*pBuffer++ = static_cast<uint8>(std::min(std::max(color.x * 255.0f,0.0f),255.0f));
					*pBuffer++ = static_cast<uint8>(std::min(std::max(color.y * 255.0f,0.0f),255.0f));
					*pBuffer++ = static_cast<uint8>(std::min(std::max(color.z * 255.0f,0.0f),255.0f));
					*pBuffer++ = 255;

				}
			}

			pixelBuffer->unlock();
		}

		//Image img;
		//img.loadDynamicImage(pCubeMapBuffer,mLinTextureSize,mLinTextureSize,1,PF_BYTE_RGB,true,6);

		//mCubeMap->loadImage(img);
	}

	void AtmosphericCubeMap::updateCoefficients()
	{
		mBetaMieC = mMie * mMieCoef;

		mBetaRaleighC = mRaleigh * mRaleighCoef;
		mBetaExtinction = mBetaMieC + mBetaRaleighC;
	}

	void AtmosphericCubeMap::createExtinction()
	{
		if (mExtinction.isNull() == false)
		{
			mExtinction->unload();
			mExtinction.setNull();
		}

		mExtinction = TextureManager::getSingleton().createManual(mExtinctionTexName,"General",TEX_TYPE_1D,mExtTextureSize,1,1,0,Ogre::PF_L8);

		Material::TechniqueIterator itTech = mTerrainMaterial->getTechniqueIterator();
		while (itTech.hasMoreElements())
		{
			Technique::PassIterator itPass = itTech.getNext()->getPassIterator();
			while (itPass.hasMoreElements())
			{
				TextureUnitState* pTexState = itPass.getNext()->getTextureUnitState(mTerrainExtinctionTexUnitName);
				if (pTexState)
				{
					pTexState->setTextureName(mExtinctionTexName,TEX_TYPE_1D);
					pTexState->setTextureAddressingMode(TextureUnitState::TAM_CLAMP);
				}
				
			}
		}
	}

	void AtmosphericCubeMap::createFaces()
	{
		if (mCubeMap.isNull() == false)
		{
			mCubeMap->unload();
			mCubeMap.setNull();
		}

		mCubeMap = TextureManager::getSingleton().createManual(mCubeMapTexName,"General",TEX_TYPE_CUBE_MAP,mLinTextureSize,mLinTextureSize,1,0,Ogre::PF_R8G8B8A8);


		Material::TechniqueIterator itTech = mTerrainMaterial->getTechniqueIterator();
		while (itTech.hasMoreElements())
		{
			Technique::PassIterator itPass = itTech.getNext()->getPassIterator();
			while (itPass.hasMoreElements())
			{
				Pass* pPass = itPass.getNext();
				if (pPass->hasVertexProgram() && pPass->getVertexProgramParameters()->_findNamedConstantDefinition(mTerrainMaxDistanceParamName))
					pPass->getVertexProgramParameters()->setNamedConstant(mTerrainMaxDistanceParamName,mMaxDistance);
				
// 				if (pPass->hasFragmentProgram())
// 				{
// 					String lang = pPass->getFragmentProgram()->getLanguage();
// 					if (lang == "cg")
// 					{
// 						SPTParamDictionary* dict = static_cast<SPTParamDictionary*>(pPass->getFragmentProgram()->getParamDictionary());
// 
// 
// 						ParamCommand* command = dict->getComm("compile_arguments");
// 						String arguments = command->doGet(pPass->getFragmentProgram().get());
// 						StringVector argList = StringUtil::split(arguments," ");
// 						StringVector macroList = StringUtil::split(mTerrain->getTerrainFragmentMacros()," ");
// 						String curArg, macroArg;
// 
// 						StringVector::iterator itMacros = macroList.begin();
// 						while(itMacros != macroList.end())
// 						{
// 							macroArg = *itMacros;
// 							StringUtil::trim(macroArg);
// 							bool foundDuplicate = false;
// 							StringVector::iterator it = argList.begin();
// 							while(it != argList.end())
// 							{
// 								curArg = *it;
// 								StringUtil::trim(curArg);
// 								if (StringUtil::endsWith(curArg,macroArg,false))
// 								{
// 									foundDuplicate = true;
// 									break;
// 								}
// 								++it;
// 							}
// 
// 							if (foundDuplicate == false)
// 								arguments += " -D" + macroArg;
// 						
// 							++itMacros;
// 						}
// 
// 						StringUtil::trim(arguments);
// 
// 						command->doSet(pPass->getFragmentProgram().get(),arguments);
// 						LogManager::getSingleton().logMessage("**** Args: " + arguments);
// 
// 						pPass->getFragmentProgram()->reload();					
// 					}
// 					//preprocessor_defines
// 				}



				TextureUnitState* pTexState = pPass->getTextureUnitState(mTerrainSkyTexUnitName);
				if (pTexState)
				{
					pTexState->setTextureName(mCubeMapTexName,TEX_TYPE_CUBE_MAP);
					pTexState->setTextureAddressingMode(TextureUnitState::TAM_CLAMP);
				}

			}
		}

		itTech = mSkyBoxMaterial->getTechniqueIterator();
		while (itTech.hasMoreElements())
		{
			Technique::PassIterator itPass = itTech.getNext()->getPassIterator();
			while (itPass.hasMoreElements())
			{
				TextureUnitState* pTexState = itPass.getNext()->getTextureUnitState(mTerrainSkyTexUnitName);
				if (pTexState)
				{
					pTexState->setTextureName(mCubeMapTexName,TEX_TYPE_CUBE_MAP);
					pTexState->setTextureAddressingMode(TextureUnitState::TAM_CLAMP);
				}

			}
		}
	}

	void AtmosphericCubeMap::updateMaxDistance( Ogre::Real fMaxDistance )
	{
		mMaxDistance = fMaxDistance;

		updateExtinction();
	}

	void AtmosphericCubeMap::createSphere( const Ogre::String& strName, const Ogre::Real r, const size_t nRings /*= 16*/, const size_t nSegments /*= 16*/ )
	{
		mSkySphereMesh = MeshManager::getSingleton().createManual(strName, ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
		SubMesh *pSphereVertex = mSkySphereMesh->createSubMesh();

		mSkySphereMesh->sharedVertexData = new VertexData();
		VertexData* vertexData = mSkySphereMesh->sharedVertexData;

		// define the vertex format
		VertexDeclaration* vertexDecl = vertexData->vertexDeclaration;
		size_t currOffset = 0;
		// positions
		vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_POSITION);
		currOffset += VertexElement::getTypeSize(VET_FLOAT3);
		// normals
		vertexDecl->addElement(0, currOffset, VET_FLOAT3, VES_NORMAL);
		currOffset += VertexElement::getTypeSize(VET_FLOAT3);
		// two dimensional texture coordinates
		vertexDecl->addElement(0, currOffset, VET_FLOAT2, VES_TEXTURE_COORDINATES, 0);
		currOffset += VertexElement::getTypeSize(VET_FLOAT2);

		// allocate the vertex buffer
		vertexData->vertexCount = (nRings + 1) * (nSegments+1);
		HardwareVertexBufferSharedPtr vBuf = HardwareBufferManager::getSingleton().createVertexBuffer(vertexDecl->getVertexSize(0), vertexData->vertexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
		VertexBufferBinding* binding = vertexData->vertexBufferBinding;
		binding->setBinding(0, vBuf);
		float* pVertex = static_cast<float*>(vBuf->lock(HardwareBuffer::HBL_DISCARD));

		// allocate index buffer
		pSphereVertex->indexData->indexCount = 6 * nRings * (nSegments + 1);
		pSphereVertex->indexData->indexBuffer = HardwareBufferManager::getSingleton().createIndexBuffer(HardwareIndexBuffer::IT_16BIT, pSphereVertex->indexData->indexCount, HardwareBuffer::HBU_STATIC_WRITE_ONLY, false);
		HardwareIndexBufferSharedPtr iBuf = pSphereVertex->indexData->indexBuffer;
		unsigned short* pIndices = static_cast<unsigned short*>(iBuf->lock(HardwareBuffer::HBL_DISCARD));

		float fDeltaRingAngle = (Math::PI / nRings);
		float fDeltaSegAngle = (2 * Math::PI / nSegments);
		unsigned short wVerticeIndex = 0 ;

		// Generate the group of rings for the sphere
		for( size_t ring = 0; ring <= nRings; ring++ ) {
			float r0 = r * sinf (ring * fDeltaRingAngle);
			float y0 = r * cosf (ring * fDeltaRingAngle);

			// Generate the group of segments for the current ring
			for(size_t seg = 0; seg <= nSegments; seg++) {
				float x0 = r0 * sinf(seg * fDeltaSegAngle);
				float z0 = r0 * cosf(seg * fDeltaSegAngle);

				// Add one vertex to the strip which makes up the sphere
				*pVertex++ = x0;
				*pVertex++ = y0;
				*pVertex++ = z0;

				Vector3 vNormal = Vector3(x0, y0, z0).normalisedCopy();
				*pVertex++ = vNormal.x;
				*pVertex++ = vNormal.y;
				*pVertex++ = vNormal.z;

				*pVertex++ = (float) seg / (float) nSegments;
				*pVertex++ = (float) ring / (float) nRings;

				if (ring != nRings) {
					// each vertex (except the last) has six indices pointing to it
					*pIndices++ = wVerticeIndex + nSegments + 1;
					*pIndices++ = wVerticeIndex;               
					*pIndices++ = wVerticeIndex + nSegments;
					*pIndices++ = wVerticeIndex + nSegments + 1;
					*pIndices++ = wVerticeIndex + 1;
					*pIndices++ = wVerticeIndex;
					wVerticeIndex ++;
				}
			}; // end for seg
		} // end for ring

		// Unlock
		vBuf->unlock();
		iBuf->unlock();
		// Generate face list
		pSphereVertex->useSharedVertices = true;

		// the original code was missing this line:
		mSkySphereMesh->_setBounds( AxisAlignedBox( Vector3(-r, -r, -r), Vector3(r, r, r) ), false );
		mSkySphereMesh->_setBoundingSphereRadius(r);
		// this line makes clear the mesh is loaded (avoids memory leaks)
		mSkySphereMesh->load();
	}

	void AtmosphericCubeMap::updatePosition()
	{
		mSkyNode->setPosition(mCamera->getDerivedPosition());
	}

	void AtmosphericCubeMap::exportSettings( const Ogre::String& strFileName )
	{
		fstream fOutFile(strFileName.c_str(),ios::out);
		if (fOutFile.is_open())
		{
#if (OGRE_PLATFORM == OGRE_PLATFORM_WIN32)
			char cTime[9];
			char cDate[9];
			_strtime_s(cTime,9);
			_strdate_s(cDate,9);
			fOutFile << "#Atmospheric Settings " << cDate << " " << cTime << endl;
#else
			time_t rawtime;
			time ( &rawtime );
			fOutFile << "#Atmospheric Settings " << ctime(&rawtime) << endl;
#endif
			fOutFile << mPlanetRadius << endl;
			fOutFile << mAtmosphereRadius << endl;
			fOutFile << mHG << endl;
			fOutFile << mRaleighCoef << endl;
			fOutFile << mMieCoef << endl;
			fOutFile << StringConverter::toString(mRaleigh) << endl;
			fOutFile << StringConverter::toString(mMie) << endl;
			fOutFile << mSunBrightness << endl;
			fOutFile << mMaxDistance << endl;
			fOutFile << StringConverter::toString(mLightDir) << endl;
			fOutFile << mExtTextureSize << endl;
			fOutFile << mLinTextureSize << endl;

			fOutFile.close();
		}
	}

	void AtmosphericCubeMap::importSettings( const Ogre::String& strFileName, bool bAutoUpdate /*= false*/ )
	{
		fstream fInFile(strFileName.c_str(),ios::in);
		if (fInFile.is_open())
		{

			String strBuffer;

			// Header
			getline(fInFile,strBuffer);

			getline(fInFile,strBuffer);
			mPlanetRadius = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mAtmosphereRadius = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mHG = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mRaleighCoef = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mMieCoef = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mRaleigh = StringConverter::parseVector3(strBuffer);

			getline(fInFile,strBuffer);
			mMie = StringConverter::parseVector3(strBuffer);

			getline(fInFile,strBuffer);
			mSunBrightness = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mMaxDistance = StringConverter::parseReal(strBuffer);

			getline(fInFile,strBuffer);
			mLightDir = StringConverter::parseVector3(strBuffer);

			getline(fInFile,strBuffer);
			mExtTextureSize = StringConverter::parseUnsignedInt(strBuffer);

			getline(fInFile,strBuffer);
			mLinTextureSize = StringConverter::parseUnsignedInt(strBuffer);

			fInFile.close();

			if (bAutoUpdate)
			{
				updateExtinction();
				updateFaces();
			}
		}
		else
		{
			LogManager::getSingleton().logMessage("Scattering settings file " + strFileName + " not found!");
		}
	}

	Ogre::ParamCommand* AtmosphericCubeMap::SPTParamDictionary::getComm( const Ogre::String& name )
	{
		return getParamCommand(name);
	}
}
