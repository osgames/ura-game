#include "HeightmapReader.h"
#include "Heightmap.h"

#include <OgreString.h>

#include "RAWHeightmapReader.h"
#include "PNGHeightmapReader.h"

using namespace SPT;
using namespace Ogre;

#if (USE_SINGLETON == 1)
template<> SPT::HeightmapReader* Ogre::Singleton<SPT::HeightmapReader>::ms_Singleton = 0;
#endif

namespace SPT
{

#if (USE_SINGLETON == 1)
	HeightmapReader* HeightmapReader::getSingletonPtr(void)
	{
		return ms_Singleton;
	}
	HeightmapReader& HeightmapReader::getSingleton(void)
	{
		assert( ms_Singleton );  return ( *ms_Singleton );
	}
#endif

	void HeightmapReader::readHeightmap( 
		const std::string& strFileName, 
		Heightmap* pHeightmap, 
		size_t iWidth /*= 0*/, 
		size_t iOffsetX /*= 0*/, 
		size_t iOffsetY /*= 0*/,
		size_t iSpacing /*= 1*/,
		bool bHasBorder /*= false*/ )
	{
		if (StringUtil::endsWith(strFileName,".raw"))
		{
			RAWHeightmapReader rawReader;
			rawReader.readHeightmap(strFileName,pHeightmap,iWidth,iOffsetX,iOffsetY,iSpacing);
		}
		else if (StringUtil::endsWith(strFileName,".png"))
		{
			PNGHeightmapReader pngReader;
			pngReader.readHeightmap(strFileName,pHeightmap,iWidth,iOffsetX,iOffsetY,iSpacing,bHasBorder);
		}
	}


}