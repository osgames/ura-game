#pragma once

#include "Prerequisites.h"
#include "TerrainModifier.h"
#include <OgrePrerequisites.h>
#include <OgreVector3.h>
#include <OgreVector2.h>
#include "Vec2D.h"
#include <OgreSharedPtr.h>

namespace SPT
{

	class Brush
	{
	public:
		Brush();
		Brush(const Ogre::String& imageName);
		Brush(Ogre::Image& image, const Ogre::String& imageName);
		Brush(const float* pImageData, size_t width, size_t height, const Ogre::String& imageName);
		~Brush();

		int getWidth(){return mImageWidth;}
		int getHeight(){return mImageHeight;}

		float* getData(){return mImageData;}

		float getAt(int x, int y);

		const Ogre::String& getTexName(){return mBrushTexName;}

		bool isInterpolated(){return mBilinearInterpolation;}
		void setInterpolated(bool lerp = true){mBilinearInterpolation = lerp;}

	private:

		Ogre::String mBrushTexName;

		int mImageWidth;
		int mImageHeight;
		float* mImageData;

		bool mBilinearInterpolation;
	};

	

	//////////////////////////////////////////////////////////////////////////

	class BrushDisplacement : public TerrainModifier
	{
	public:
		enum { FlatModeNormal = 0, FlatModeCenter = 1, FlatModeBottom = 2 }; 

		BrushDisplacement(BrushPtr pBrush, const Ogre::Vector3& center, Ogre::Real width, Ogre::Real height, Ogre::Real intensity);

		BrushDisplacement(BrushPtr pBrush, const Ogre::Vector3& center, Ogre::Real width, Ogre::Real height, const unsigned long flatMode);

		void displace(Vec2D<double> vTopLeft, double fWidth, Heightmap* pHeightmap, float fScale  ) const;

		void displaceTexture(Vec2D<double> vTopLeft, double fWidth, Ogre::TexturePtr pTex, int iChannel = 0) const;

		bool isInBounds(const Vec2D<double>& vMin, const Vec2D<double>& vMax) const;

		const Ogre::String getType() const {return "BrushDisplacement";}

	private:
		Ogre::Vector3 mCenter;
		Ogre::Real mWidth;
		Ogre::Real mHeight;
		Ogre::Real mIntensity;
		Ogre::Vector2 mMin;
		Ogre::Vector2 mMax;

		BrushPtr mBrush;

		bool mBflatten;
		unsigned long mFlattenMode;
	};
}
