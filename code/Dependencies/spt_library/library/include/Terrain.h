#pragma once

#include <Ogre.h>
#include <OgrePrerequisites.h>
#include <OgreVertexIndexData.h>
#include <OgreMaterial.h>
#include <OgreVector3.h>
#include <OgreSceneManager.h>
#include "Prerequisites.h"
#include "TerrainModifier.h"
#if (USE_SINGLETON == 1)
#include <OgreSingleton.h>
#endif


namespace SPT
{
	class ObjectHandler;
	class SceneObject;
	class Terrain;

	class SPTRaySceneQuery : public Ogre::DefaultRaySceneQuery
	{
	public:
		SPTRaySceneQuery(Ogre::SceneManager* creator);
		SPTRaySceneQuery(Terrain* terrain);
		~SPTRaySceneQuery();

		void execute(Ogre::RaySceneQueryListener* listener);
	protected:
		Ogre::SceneQuery::WorldFragment mWorldFragment;
		Terrain* mTerrain;
	};

	class TerrainEventListener
	{
	public:
		virtual void meshCreated(TerrainMesh* mesh){}
		virtual void meshDestroyed(TerrainMesh* mesh){}
		virtual void meshUpdated(TerrainMesh* mesh){}
	};


#if (USE_SINGLETON == 1)
	class Terrain : public Ogre::Singleton<Terrain>
#else
	class Terrain
#endif
	{
	public:
		Terrain();
		Terrain(
			Ogre::Camera* pCamera, 
			Ogre::SceneManager* pSceneMgr);
		~Terrain();

#if (USE_SINGLETON == 1)
		static Terrain& getSingleton();

		static Terrain* getSingletonPtr();
#endif

		void addIndexData(Ogre::IndexData* pData, size_t iLOD);
		Ogre::IndexData* getIndexData(size_t iLOD);

		void setCamera(Ogre::Camera* pCamera){mCamera = pCamera;}
		Ogre::Camera* getCamera(){return mCamera;}

		enum AutoClampState
		{
			ACS_OFF = 0,
			ACS_BELOW,
			ACS_ALWAYS
		};

		AutoClampState getClampState() const { return mClampState; }
		
		Ogre::Real getClampBonusHeight() const {return mClampBonusHeight; }

		bool getClampUseRoot() {return mClampUseRoot;}
		bool getClampUseGeoMorph(){return mClampUseGeoMorph;}

		void setClampState(
			AutoClampState val, 
			Ogre::Real heightBonus = 0.0f, 
			bool useRoot = false, 
			bool useMorph = false) 
		{ 
			mClampState = val; 
			mClampBonusHeight = heightBonus; 
			mClampUseRoot = useRoot; 
			mClampUseGeoMorph = useMorph; 
		}

		enum AutoClampUpdateState
		{
			ACUS_MOVEMENT = 0,
			ACUS_TIME,
			ACUS_BOTH
		};



		Ogre::Real getSkirtLength() const { return mSkirtLength; }
		void setSkirtLength(Ogre::Real val) { mSkirtLength = val; }
		void setSkirtLengthPercent(Ogre::Real val = 10.0f){ mSkirtLength = mTerrainHeight * (val / 100.0f);}

		Ogre::Real getMorphSpeed() const { return mMorphSpeed; }
		void setMorphSpeed(Ogre::Real val) { mMorphSpeed = val; }

		Ogre::Real getLODCheckTime() const { return mLODCheckTime; }
		void setLODCheckTime(Ogre::Real val) { mLODCheckTime = val; }

		//void setTerrainNode(Ogre::SceneNode* pTerrainNode){mTerrainNode = pTerrainNode;}
		Ogre::SceneNode* getTerrainNode(){return mTerrainNode;}

		void setSceneManager(Ogre::SceneManager* pSceneMgr){mSceneMgr = pSceneMgr;}
		Ogre::SceneManager* getSceneManager(){return mSceneMgr;}

		void setMaterial(const Ogre::String& materialName);
		Ogre::MaterialPtr getMaterial(){return mTerrainMaterial;}

		//void setZFirstMaterial(const Ogre::String& materialName);
		//Ogre::MaterialPtr getZFirstMaterial(){return mTerrainZFirstMaterial;}

		void setTerrainHeight(Ogre::Real height){mTerrainHeight = height;}
		Ogre::Real getTerrainHeight(){return mTerrainHeight;}

		//typedef std::vector<TerrainModifier*> TerrainMods;

		//void addTerrainMod(TerrainModifier* modifier);
		//TerrainMods::iterator getTerrainModIterator();
		//TerrainMods::iterator getTerrainModIteratorEnd();

		void setMaxDepth(size_t iMaxDepth){mMaxDepth = iMaxDepth;}
		size_t getMaxDepth(){return mMaxDepth;}


		void setWidth(double dWidth){mWidth = dWidth;}
		double getWidth(){return mWidth;}

		void initialize(const Ogre::String& heightmapName, size_t iWidth = 0);
		void destroy();

		void updateHeightmap();

		Heightmap* getHeightmap(){return mHeightmap;}

		QNode* getRootNode(){return mRootNode;}

		bool getHeightAt(Ogre::Vector3& vPos, Ogre::Real rBonus = 0.0f, bool bUseRoot = false, bool bUseGeoMorphing = false);

		Ogre::Vector3 getNormalAt(Ogre::Vector3& vPos, bool bUseRoot = false, bool bUseGeoMorphing = false);



		bool getRayHeight(const Ogre::Ray& vRay, Ogre::Vector3& vReturnPos, bool bUseRoot = false, bool bUseGeoMorphing = false);

#ifdef GAME_EDIT_MODE
		bool getRayHeightOptimized( Ogre::Vector3 camPosition, const Ogre::Ray& vRay,
			Ogre::Vector3& vReturnPos, bool bUseRoot = false, bool bUseGeoMorphing = false );
#endif

		void _checkIn(){mTotalNodes++;}

		void onFrameStart(Ogre::Real fTime);

		void onFrameEnd(Ogre::Real fTime);

		bool getAutoUpdateLightmap() const { return mAutoUpdateLightmap; }
		void setAutoUpdateLightmap(bool val) { mAutoUpdateLightmap = val; }

		void updateLightmap();

		QNode* addTerrainModifier(const TerrainModifier& modifier);

		AtmosphericCubeMap* createAtmosphere(
			Ogre::Real fPlanetRadius, 
			Ogre::Real fAtmosphereRadius, 
			Ogre::Real fHG, 
			Ogre::Real fSunBrightness, 
			Ogre::Real fMaxDistance, 
			const Ogre::Vector3& vMie,
			const Ogre::Vector3& vRaleigh,
			const Ogre::String& strSkyBoxMatName,
			size_t iExtinctionTexSize = 1024,
			size_t iSkyBoxTexSize = 512,
			Ogre::Real fRaleighCoef = 0.2f, 
			Ogre::Real fMieCoef = 0.001, 
			const Ogre::String& strCubeMapTexName = "SPT_SkyBox_Tex",
			const Ogre::String& strExtinctionTexName = "SPT_Ext_Tex",
			const Ogre::String& strTerrainMaxDistanceParamName = "fMaxDistance",
			const Ogre::String& strSkyBoxTexUnitName = "SkyBox",
			const Ogre::String& strTerrainSkyTexUnitName = "SkyBox",
			const Ogre::String& strTerrainExtinctionTexUnitName = "Extinction");

		AtmosphericCubeMap* createAtmosphere(
			const Ogre::String& strImportSettingsFile,
			const Ogre::String& strSkyBoxMatName,
			const Ogre::String& strCubeMapTexName = "SPT_SkyBox_Tex",
			const Ogre::String& strExtinctionTexName = "SPT_Ext_Tex",
			const Ogre::String& strTerrainMaxDistanceParamName = "fMaxDistance",
			const Ogre::String& strSkyBoxTexUnitName = "SkyBox",
			const Ogre::String& strTerrainSkyTexUnitName = "SkyBox",
			const Ogre::String& strTerrainExtinctionTexUnitName = "Extinction");

		GPULightmapper* createLightmapper(
			const Ogre::String& strLightmapMatName,
			const Ogre::String& strCompositorName, 
			size_t iMaxLightmapSize = 2048,
			size_t iMaxHeightmapTexSize = 2049,
			const Ogre::String& strLightmapScaleParamName = "vScale",
			const Ogre::String& strLightmapHeightmapSizeParamName = "fSize",
			const Ogre::String& strHeightmapTexName = "SPT_Heightmap_Tex", 
			const Ogre::String& strRenderTextureName = "SPT_Lightmap_RTT", 
			const Ogre::String& strLightmapTexUnitName = "Heightmap", 
			const Ogre::String& strTerrainTexUnitName = "Lightmap", 
			const Ogre::String& strCompositorScaleParamName = "vScale");

		AtmosphericCubeMap* getAtmosphere(){return mSkyMap;}
		GPULightmapper* getLightmapper(){return mLightmapper;}

		void setLightDirection(const Ogre::Vector3& vLightDir);


		void quickSetup(
			const Ogre::String& strHeightmapName, 
			size_t iHeightmapWidth, 
			const Ogre::String& strTerrainMaterialName, 
			Ogre::Real fTerrainWidth, 
			Ogre::Real fTerrainHeight,
			Ogre::Real fTerrainQuickLoadTime = 3.0f);

		void quickSetup(
			const Ogre::String& strHeightmapName, 
			const Ogre::String& strTerrainMaterialName, 
			Ogre::Real fTerrainWidth, 
			Ogre::Real fTerrainHeight,
			Ogre::Real fTerrainQuickLoadTime = 3.0f );

		void quickSetup(
			size_t iHeightmapWidth, 
			const Ogre::String& strTerrainMaterialName, 
			Ogre::Real fTerrainWidth, 
			Ogre::Real fTerrainHeight,
			Ogre::Real fTerrainQuickLoadTime = 3.0f);

		void saveTerrain(const Ogre::String& filePath);
		

		ObjectHandler* getObjectHandler() const { return mObjectHandler; }
		void setObjectHandler(ObjectHandler* val) { mObjectHandler = val; }

		Ogre::RaySceneQuery* 
			createRayQuery(const Ogre::Ray& ray, unsigned long mask = 0xFFFFFFFF);

		bool getDiscardGeometryData() const { return mDiscardGeometryData; }
		void setDiscardGeometryData(bool val) { mDiscardGeometryData = val; }

		bool getUseChunkUVs() const { return mUseChunkUVs; }
		void setUseChunkUVs(bool val) { mUseChunkUVs = val; }

		void setTerrainEventListener(TerrainEventListener* listener);

		void _fireMeshCreated(TerrainMesh* mesh);
		void _fireMeshDestroyed(TerrainMesh* mesh);
		void _fireMeshUpdated(TerrainMesh* mesh);

		Ogre::String getDefaultMaterialScheme();

		void setMaterialSchemeParams(const Ogre::String& paramName, const Ogre::Vector3& paramVal, const Ogre::String& schemeName = "Default");
		void setMaterialSchemeParams(const Ogre::String& paramName, const Ogre::Vector4& paramVal, const Ogre::String& schemeName = "Default");
		void setMaterialSchemeParams(const Ogre::String& paramName, const Ogre::Matrix4& paramVal, const Ogre::String& schemeName = "Default");
		void setMaterialSchemeParams(const Ogre::String& paramName, const Ogre::Real& paramVal, const Ogre::String& schemeName = "Default");


		void testApply(const Ogre::String& matName);

		const Ogre::String& getTerrainFragmentMacros(){return mTerrainFragmentMacros;}

		BrushPtr createBrush(const Ogre::String& brushImageName);
		void destroyBrush(const Ogre::String& brushImageName);
		void destroyBrush(BrushPtr pBrush);

		void destroyAllBrushes();

		bool brushExists(const Ogre::String& brushImageName);

		BrushPtr getBrush(size_t index);
		BrushPtr getBrush(const Ogre::String& brushImageName);

		void initializeBrushDecal(const Ogre::String& brushTexName, const Ogre::Vector2& size);
		void destroyBrushDecal();

		void setBrushSize(const Ogre::Vector2& size);

		void setBrushPosition(const Ogre::Vector3& pos);

		bool getBrushVisible();
		void setBrushVisible(bool visibile = true);

		Ogre::Vector4 _getBrushParams();

		Ogre::Real getLODDistBias() const { return mLODDistBias; }
		void setLODDistBias(Ogre::Real val);

		void setSelectedTexture(const Ogre::String& texName);
		void clearSelectedTexture();
		Ogre::TexturePtr getSelectedTexture() const { return mSelectedTexture; }

		void saveSelectedTexture(const Ogre::String& filePath);

		bool getDeformation() const { return mDeformation; }
		void setDeformation(bool val) { mDeformation = val; }

		int getEditChannel() const { return mEditChannel; }
		void setEditChannel(int val) { mEditChannel = val; }
	private:

		/*
		 * The getrayheight() function becomes slow when moving the mouse cursor to the sky 
		 * and off the terrain. We use the timer in edit mode so that we call the function every
		 * once in a while. This way we don't slow down the computer/mouse cursor much.
		 */
		#ifdef GAME_EDIT_MODE
			Ogre::Timer* mOptimizeTimer;
			bool mBoolTouchedTerrain;
		#endif

		typedef std::map<size_t, Ogre::IndexData*> IndexStorage;
		IndexStorage mIndexStore;

		Ogre::Camera* mCamera;
		Ogre::SceneNode* mTerrainNode;
		Ogre::SceneManager* mSceneMgr;
		Ogre::MaterialPtr mTerrainZFirstMaterial;
		Ogre::MaterialPtr mTerrainMaterial;

		Ogre::Real mTerrainHeight;
		Ogre::Real mSkirtLength;
		Ogre::Real mMorphSpeed;
		Ogre::Real mLODCheckTime;
		Ogre::Real mPreMorphSpeed;
		Ogre::Real mPreLODCheckTime;
		bool mInitialized;

		Ogre::Real mQuickLoadTime;

		AutoClampState mClampState;
		Ogre::Real mClampBonusHeight;
		bool mClampUseRoot;
		bool mClampUseGeoMorph;
		//TerrainMods mModifiers;

		GPULightmapper* mLightmapper;
		AtmosphericCubeMap* mSkyMap;

		size_t mMaxDepth;

		size_t mTotalNodes;
		double mWidth;

		Heightmap* mHeightmap;

		QNode* mRootNode;

		HeightmapReader* mSingleton;

		ObjectHandler* mObjectHandler;

		Ogre::Vector3 mLightDirection;

		bool mAutoUpdateLightmap;

		bool mDiscardGeometryData;

		bool mUseChunkUVs;

		TerrainEventListener* mEventListener;
		TerrainEventListener mDefaultListener;

		Ogre::String mTerrainFragmentMacros;

		typedef std::map<Ogre::String, BrushPtr> BrushStorage;
		BrushStorage mBrushStorage;


		Ogre::Vector2 mBrushSize;
		bool mShowBrush;
		Ogre::Vector3 mBrushPosition;
		Ogre::String mBrushTextureName;
		Ogre::TextureUnitState* mBrushTexState;
		Ogre::Pass* mDecalPass;

		Ogre::TexturePtr mSelectedTexture;

		bool mDeformation;

		int mEditChannel;
		Ogre::Real mLODDistBias;
	};
}