#pragma once

#include "Prerequisites.h"
#include <OgrePrerequisites.h>
#include <OgreSingleton.h>

namespace SPT
{

	class HeightmapReader 
	#if (USE_SINGLETON == 1) 
		: public Ogre::Singleton<HeightmapReader>
	#endif

	{
	public:
		HeightmapReader(){};

		virtual ~HeightmapReader(){};

#if (USE_SINGLETON == 1) 
		static HeightmapReader& getSingleton();
		static HeightmapReader* getSingletonPtr();
#endif

		void readHeightmap(
			const std::string& strFileName, 
			Heightmap* pHeightmap, 
			size_t iWidth = 0, 
			size_t iOffsetX = 0, 
			size_t iOffsetY = 0,
			size_t iSpacing = 1,
			bool bHasBorder = false);


	private:
	};
}