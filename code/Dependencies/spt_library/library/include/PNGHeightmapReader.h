#pragma once

#include "Prerequisites.h"

namespace SPT
{
	class PNGHeightmapReader
	{
	public:
		void readHeightmap(
			const std::string& strFileName, 
			Heightmap* pHeightmap, 
			size_t iWidth = 0, 
			size_t iOffsetX = 0, 
			size_t iOffsetY = 0,
			size_t iSpacing = 1,
			bool bHasBorder = false);

	private:
	};
}