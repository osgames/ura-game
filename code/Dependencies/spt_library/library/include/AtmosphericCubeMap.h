#pragma once

#include "Prerequisites.h"
#include <OgrePrerequisites.h>
#include <OgreMaterial.h>
#include <OgreTexture.h>
#include <OgreVector3.h>
#include <OgreMesh.h>

namespace SPT
{
	class AtmosphericCubeMap
	{
	public:
		AtmosphericCubeMap();
		AtmosphericCubeMap(
			Terrain* pTerrain,
			Ogre::SceneManager* pSceneMgr, 
			Ogre::Camera* pCamera, 
			const Ogre::String& strImportSettingsFile,
			const Ogre::String& strSkyBoxMatName,
			const Ogre::String& strTerrainMatName,
			const Ogre::String& strCubeMapTexName = "SPT_SkyBox_Tex",
			const Ogre::String& strExtinctionTexName = "SPT_Ext_Tex",
			const Ogre::String& strTerrainMaxDistanceParamName = "fMaxDistance",
			const Ogre::String& strSkyBoxTexUnitName = "SkyBox",
			const Ogre::String& strTerrainSkyTexUnitName = "SkyBox",
			const Ogre::String& strTerrainExtinctionTexUnitName = "Extinction");

		AtmosphericCubeMap(
			Terrain* pTerrain,
			Ogre::SceneManager* pSceneMgr,
			Ogre::Camera* pCamera,
			Ogre::Real fPlanetRadius, 
			Ogre::Real fAtmosphereRadius, 
			Ogre::Real fHG, 
			Ogre::Real fSunBrightness, 
			Ogre::Real fMaxDistance, 
			const Ogre::Vector3& vLightDir,
			const Ogre::Vector3& vMie,
			const Ogre::Vector3& vRaleigh,
			const Ogre::String& strSkyBoxMatName,
			const Ogre::String& strTerrainMatName,
			size_t iExtinctionTexSize = 1024,
			size_t iSkyBoxTexSize = 512,
			Ogre::Real fRaleighCoef = 0.2f, 
			Ogre::Real fMieCoef = 0.001, 
			const Ogre::String& strCubeMapTexName = "SPT_SkyBox_Tex",
			const Ogre::String& strExtinctionTexName = "SPT_Ext_Tex",
			const Ogre::String& strTerrainMaxDistanceParamName = "fMaxDistance",
			const Ogre::String& strSkyBoxTexUnitName = "SkyBox",
			const Ogre::String& strTerrainSkyTexUnitName = "SkyBox",
			const Ogre::String& strTerrainExtinctionTexUnitName = "Extinction");

		~AtmosphericCubeMap();


		// I didn't create this!
		// It's from the Wiki
		void createSphere(const Ogre::String& strName, const Ogre::Real r, const size_t nRings = 16, const size_t nSegments = 16);


		Ogre::Real CalculateSkyLength(const Ogre::Vector3 &inDirection);

		Ogre::Vector3 CalculateExtinctionAmount(Ogre::Real distance);


		Ogre::Vector3 CalculateLin(const Ogre::Vector3 &viewDirection);

		void createExtinction();

		void createFaces();

		void updateExtinction();

		void updateFaces();

		void updateCoefficients();

		void updateMaxDistance(Ogre::Real fMaxDistance);

		void updatePosition();

		void setLightDirection(const Ogre::Vector3& vLightDir){mLightDir = vLightDir;}
		const Ogre::Vector3& getLightDirection(){return mLightDir;}

		Ogre::Real getPlanetRadius() const { return mPlanetRadius; }
		void setPlanetRadius(Ogre::Real val) { mPlanetRadius = val; }

		Ogre::Real getAtmosphereRadius() const { return mAtmosphereRadius; }
		void setAtmosphereRadius(Ogre::Real val) { mAtmosphereRadius = val; }

		Ogre::Real getHG() const { return mHG; }
		void setHG(Ogre::Real val) { mHG = val; }

		Ogre::Real getRaleighCoef() const { return mRaleighCoef; }
		void setRaleighCoef(Ogre::Real val) { mRaleighCoef = val; }

		Ogre::Real getMieCoef() const { return mMieCoef; }
		void setMieCoef(Ogre::Real val) { mMieCoef = val; }

		Ogre::Vector3 getRaleigh() const { return mRaleigh; }
		void setRaleigh(Ogre::Vector3 val) { mRaleigh = val; }

		Ogre::Vector3 getMie() const { return mMie; }
		void setMie(Ogre::Vector3 val) { mMie = val; }

		Ogre::Real getSunBrightness() const { return mSunBrightness; }
		void setSunBrightness(Ogre::Real val) { mSunBrightness = val; }

		Ogre::Real getMaxDistance() const { return mMaxDistance; }
		void setMaxDistance(Ogre::Real val) { mMaxDistance = val; }

		void exportSettings(const Ogre::String& strFileName);
		void importSettings(const Ogre::String& strFileName, bool bAutoUpdate = false);

		// Quick cheats to make my life easier
		enum CubeFace
		{
			CF_POS_X = 0,
			CF_NEG_X,
			CF_POS_Y,
			CF_NEG_Y,
			CF_POS_Z,
			CF_NEG_Z
		};

		Ogre::TexturePtr getSkyTexture(){return mCubeMap;}

		Ogre::TexturePtr getExtinctionTexture(){return mExtinction;}

		class SPTParamDictionary : protected Ogre::ParamDictionary
		{
		public:
			Ogre::ParamCommand* getComm(const Ogre::String& name);
		};

	private:
		Ogre::Real mPlanetRadius;
		Ogre::Real mAtmosphereRadius;
		Ogre::Real mHG;
		Ogre::Real mRaleighCoef;
		Ogre::Real mMieCoef;
		Ogre::Vector3 mRaleigh;
		Ogre::Vector3 mMie;
		Ogre::Vector3 mBetaRaleighC;
		Ogre::Vector3 mBetaMieC;
		Ogre::Vector3 mBetaExtinction;
		Ogre::Real mSunBrightness;
		Ogre::Real mMaxDistance;
		Ogre::MaterialPtr mSkyBoxMaterial;
		Ogre::MaterialPtr mTerrainMaterial;

		Ogre::String mSkyBoxTexUnitName;
		Ogre::String mTerrainSkyTexUnitName;
		Ogre::String mTerrainExtinctionTexUnitName;
		Ogre::String mTerrainMaxDistanceParamName;

		Ogre::String mExtinctionTexName;
		Ogre::String mCubeMapTexName;
		size_t mExtTextureSize;
		size_t mLinTextureSize;
		Ogre::Vector3 mLightDir;
		//Ogre::Real mTheta;
		//Ogre::Real mEpsilon;

		Terrain* mTerrain;

		Ogre::TexturePtr mCubeMap;
		Ogre::TexturePtr mExtinction;

		Ogre::SceneManager* mSceneMgr;
		Ogre::Camera* mCamera;

		Ogre::SceneNode* mSkyNode;
		Ogre::Entity* mSkySphere;
		Ogre::MeshPtr mSkySphereMesh;
	};
}