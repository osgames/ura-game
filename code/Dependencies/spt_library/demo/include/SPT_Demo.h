#include "ExampleApplication.h"
#include "Terrain.h"
#include "AtmosphericCubeMap.h"
#include "CircleDisplacement.h"
#include "RectFlatten.h"
#include "BrushDisplacement.h"
#include "ObjectHandler.h"
#include <Ogre.h>

#include <fstream>


using namespace SPT;
using namespace Ogre;

Terrain* gTerrain;

Light* gSunLight;

// Kind of hacky, but can't use Camera as a Node
SceneNode* gFollowNode;

RaySceneQuery* gRSQ = 0;
size_t iHeadCount = 0;

Viewport* gViewport;

String gCurrentScheme;

Pass* gPass;

BrushPtr gBrush0;
BrushPtr gBrush1;
BrushPtr gBrush2;

SceneNode* gRectNode;


void createSPT(Camera* pCamera, SceneManager* pSceneMgr)
{

	// Create SPT

	gTerrain = new Terrain(pCamera,pSceneMgr);

	gTerrain->setClampState(Terrain::ACS_BELOW,15,false,true);

	gTerrain->setDiscardGeometryData(false);

	// Do a quick setup

	gTerrain->quickSetup("Highlands_Height.png","SPT_Highlands",64000.0,6000.0);

	// All this handles the atmosphere

	//gTerrain->createAtmosphere("BlueSky.txt","SkySphere");


	gTerrain->createAtmosphere(
		637800,638640,
		-0.985148,
		95.0213,
		640000.0,
		Vector3(2.9427e-006, 9.4954e-006, 1.40597e-005),
		Vector3(9.43648e-005, 3.36762e-005, 6.59358e-006),
		"SkySphere",512,256);

	gTerrain->createLightmapper("Lightmapper","SPT_Comp",2048,2049);

	gTerrain->setLightDirection(gSunLight->getDirection());

	//////////////////////////////////////////////////
	/*						UPDATE					*/
	//////////////////////////////////////////////////

	/*	
	This is defaulted to true, so set this only if you
	want total control over light-map generation.
	Good for small/minor height changes. 
	*/

	gTerrain->setAutoUpdateLightmap(false);


	//////////////////////////////////////////////////

	//gTerrain->getAtmosphere()->exportSettings("TestExport.txt");
	//gTerrain->getAtmosphere()->importSettings("TestExport.txt",true);

	gTerrain->initializeBrushDecal("brush0.png",Vector2(1000,1000));

	// And you're done!

	const size_t iTotalOgreHeads = 50;

	SceneObject* pObj = 0;
	Vector3 startPos = Vector3::ZERO;

	for (size_t i=0;i<iTotalOgreHeads;i++)
	{
		pObj = gTerrain->getObjectHandler()->createSceneObject("OgreHead" + StringConverter::toString(i),"ogrehead.mesh");
		pObj->setAutoClampState(Terrain::ACS_ALWAYS);	
		pObj->useBoundingBoxBase();
		pObj->biasHeightBonus(-2.0f);
		pObj->setTargetNode(gFollowNode,(Math::RangeRandom(-50,50) > 0.0f) ? true : false);
		startPos.x = Math::RangeRandom(-32000,32000);
		startPos.z = Math::RangeRandom(-32000,32000);
		pObj->moveObject(startPos);
		pObj->setAutoClampTime(Math::RangeRandom(0.0f,5.0f));
		pObj->setAutoRotateTime(Math::RangeRandom(0.0f,2.0f));
		iHeadCount++;
	}


	gRSQ = gTerrain->createRayQuery(pCamera->getCameraToViewportRay(0.5f,0.5f));
}

void destroySPT()
{
	delete gRSQ;
	gRSQ = 0;

	delete gTerrain;
	gTerrain = 0;


}


class SPTDemoFrameListener : public ExampleFrameListener
{
protected:
	Real mBaseSpeed;
	Real mCurrentSpeed;
	SceneManager* mSceneMgr;


public:
	SPTDemoFrameListener(SceneManager* sceneMgr, RenderWindow* win, Camera* cam) 
		: ExampleFrameListener(win,cam),
		mSceneMgr(sceneMgr)

	{
		mBaseSpeed = 400.0f;
		mCurrentSpeed = 1.0f;
	}
	bool frameStarted(const FrameEvent& evt)
	{

		if (ExampleFrameListener::frameStarted(evt) == false)
			return false;


		for (size_t k = OIS::KC_1; k <= OIS::KC_9;k++)
		{
			if (mKeyboard->isKeyDown(OIS::KeyCode(k)))
				mCurrentSpeed = (Real(k - OIS::KC_1) + 1.0f) * 2.0f;
		}

		if (mKeyboard->isKeyDown(OIS::KC_0))
			mCurrentSpeed = 0.5f;

		mMoveSpeed = mCurrentSpeed * mBaseSpeed;

		// Update our cheezy follow node
		gFollowNode->setPosition(mCamera->getPosition());


		// Displays the current movement speed
		mDebugText = "Current speed: " + StringConverter::toString(mMoveSpeed) + " m/s";



		if (gTerrain == 0 && mKeyboard->isKeyDown(OIS::KC_RETURN) && mTimeUntilNextToggle <= 0)
		{
			createSPT(mCamera,mSceneMgr);
			mTimeUntilNextToggle = 0.5f;
		}

		if (gTerrain && mKeyboard->isKeyDown(OIS::KC_DELETE) && mTimeUntilNextToggle <= 0)
		{
			destroySPT();
			mTimeUntilNextToggle = 0.5f;
		}


		if (gTerrain == 0)
			return true;


		// This is set to update every frame for SPT
		gTerrain->onFrameStart(evt.timeSinceLastFrame);

		static bool bDeforming = false;

		if (mKeyboard->isKeyDown(OIS::KC_J) && mTimeUntilNextToggle <= 0)
		{
			Vector3 vLightDir = Vector3(Math::RangeRandom(-10,10),Math::RangeRandom(-10,-0.1),Math::RangeRandom(-10,10));
			vLightDir.normalise();

			gTerrain->setLightDirection(vLightDir);
			gSunLight->setDirection(vLightDir);


			mTimeUntilNextToggle = 0.2;
		}

		if (mKeyboard->isKeyDown(OIS::KC_I) && mTimeUntilNextToggle <= 0)
		{
			/*if (gTerrain->getAutoUpdateLightmap() == false)
			{
				gTerrain->updateLightmap();
			}*/
			//vSplatScales 
			Vector4 vSplatScales;
			for (int i=0;i<4;i++)
				vSplatScales[i] = Math::RangeRandom(1.0f,1000.0f);

			gTerrain->setMaterialSchemeParams("vSplatScales",vSplatScales,gCurrentScheme);

			mTimeUntilNextToggle = 0.1f;
		}

		static BrushPtr pBrush = gBrush0;
		if (mKeyboard->isKeyDown(OIS::KC_NUMPAD1))
		{
			pBrush = gBrush0;
			gTerrain->initializeBrushDecal(pBrush->getTexName(),Vector2(1000,1000));
		}
		if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
		{
			pBrush = gBrush1;
			gTerrain->initializeBrushDecal(pBrush->getTexName(),Vector2(1000,1000));
		}
		if (mKeyboard->isKeyDown(OIS::KC_NUMPAD3))
		{
			pBrush = gBrush2;
			gTerrain->initializeBrushDecal(pBrush->getTexName(),Vector2(1000,1000));
		}

		static bool bGetNewPos = true;
		static Vector3 vBrushPos = Vector3::ZERO;
		if (mKeyboard->isKeyDown(OIS::KC_L) && mTimeUntilNextToggle <= 0)
		{

			/*AtmosphericCubeMap* pAtm = gTerrain->getAtmosphere();
			pAtm->setHG(-0.96);
			pAtm->setSunBrightness(4);
			//pAtm->setRaleighCoef(0.02);
			pAtm->setMieCoef(0.1);

			gTerrain->setLightDirection(Vector3(0,0,1));
			gSunLight->setDirection(Vector3(0,0,1));

			mTimeUntilNextToggle = 0.2;*/

			
			if (bGetNewPos)
			{
				Ray ray = mCamera->getCameraToViewportRay(0.5,0.5);
				if (gTerrain->getRayHeight(ray,vBrushPos,true,false))
					bGetNewPos = false;
				
			}

			if (bGetNewPos == false)
			{
				gTerrain->addTerrainModifier(BrushDisplacement(pBrush,vBrushPos,2000,2000,5.0f));
				gRectNode->setPosition(vBrushPos + Vector3(0,5,0));
				gRectNode->setScale(2000,1,2000);

				gTerrain->setBrushVisible(true);
				gTerrain->setBrushSize(Vector2(2000,2000));
				gTerrain->setBrushPosition(vBrushPos);

				bDeforming = true;
			}


			mTimeUntilNextToggle = 0.0f;
		}

		if (mKeyboard->isKeyDown(OIS::KC_L) == false)
		{
			bGetNewPos = true;
		}

		static Real rFlattenHeight = 0.0f;
		static bool bGetNewHeight = true;
		if (mKeyboard->isKeyDown(OIS::KC_K) && mTimeUntilNextToggle <= 0)
		{
			Ray ray = mCamera->getCameraToViewportRay(0.5,0.5);

			Vector3 vRayPos = Vector3::ZERO;

			if (gTerrain->getRayHeight(ray,vRayPos,true,false))
			{
				if (bGetNewHeight)
				{
					rFlattenHeight = vRayPos.y;
					bGetNewHeight = false;
				}
				else
				{
					vRayPos.y = rFlattenHeight;
				}
				gTerrain->addTerrainModifier(RectFlatten(vRayPos,2000,1000));
				//gTerrain->addTerrainModifier(BrushDisplacement(gBrush0,vRayPos,2000,1000,20.0f));
				gRectNode->setPosition(vRayPos + Vector3(0,5,0));
				gRectNode->setScale(2000,1,1000);

				gTerrain->setBrushVisible(true);
				gTerrain->setBrushSize(Vector2(2000,1000));
				gTerrain->setBrushPosition(vRayPos);
			}

			bDeforming = true;
			


			mTimeUntilNextToggle = 0.0f;
		}
		if (mKeyboard->isKeyDown(OIS::KC_K) == false)
		{
			bGetNewHeight = true;
		}


		if (mKeyboard->isKeyDown(OIS::KC_H) && mTimeUntilNextToggle <= 0)
		{
			// Randomly displaces the terrain as a big crater
			Vector3 pos = Vector3(Math::RangeRandom(-16000,16000),0.0f,Math::RangeRandom(-16000,16000));

			if (gTerrain->getHeightAt(pos,0.0f,true,false))
			{
				Real radius = Math::RangeRandom(500,16000);
				Real power = Math::RangeRandom(-20000,20000);
				QNode* pHighest = gTerrain->addTerrainModifier(CircleDisplacement(pos,radius,power));

			}

			bDeforming = true;

			mTimeUntilNextToggle = 0.2;
		}

		if (mKeyboard->isKeyDown(OIS::KC_Y) && mTimeUntilNextToggle <= 0)
		{
			gRSQ->setRay(mCamera->getCameraToViewportRay(0.5f,0.5f));
			RaySceneQueryResult& qryResult = gRSQ->execute();
			RaySceneQueryResult::iterator i = qryResult.begin();
			if (i != qryResult.end() && i->worldFragment)
			{
				SceneObject* pObj = gTerrain->getObjectHandler()->createSceneObject("OgreHead" + StringConverter::toString(iHeadCount++),"ogrehead.mesh");
				pObj->setAutoClampState(Terrain::ACS_ALWAYS);	
				pObj->useBoundingBoxBase();
				pObj->biasHeightBonus(-2.0f);
				pObj->setTargetNode(gFollowNode,(Math::RangeRandom(-50,50) > 0.0f) ? true : false);
				pObj->moveObject(i->worldFragment->singleIntersection);
				pObj->setAutoClampTime(Math::RangeRandom(0.0f,5.0f));
				pObj->setAutoRotateTime(Math::RangeRandom(0.0f,2.0f));
				static_cast<Entity*>(pObj->getObject())->setMaterialName("ScatteredOgre");

				//gTerrain->testApply("ScatteredOgre",static_cast<Entity*>(pObj->getObject()));
			}


			mTimeUntilNextToggle = 0.1f;
		}

		static bool bQualityToggle = true;
		if (mKeyboard->isKeyDown(OIS::KC_U) && mTimeUntilNextToggle <= 0)
		{
			bQualityToggle = !bQualityToggle;

			if (bQualityToggle)
				gCurrentScheme = "Medium";
			else
				gCurrentScheme = "Low";

			gViewport->setMaterialScheme(gCurrentScheme);

			mTimeUntilNextToggle = 0.5f;
		}

		if (gTerrain->getAutoUpdateLightmap() == false && bDeforming && mKeyboard->isKeyDown(OIS::KC_K) == false && mKeyboard->isKeyDown(OIS::KC_H) == false && mKeyboard->isKeyDown(OIS::KC_L) == false)
		{
			gTerrain->updateLightmap();
			bDeforming = false;
		}



		return true;
	}

	bool frameEnded(const FrameEvent& evt)
	{


		// Updates the end-of-frame duties of SPT
		if (gTerrain)
			gTerrain->onFrameEnd(evt.timeSinceLastFrame);

		return ExampleFrameListener::frameEnded(evt);
	}


};





class SPTDemoApplication : public ExampleApplication
{
public:
    SPTDemoApplication() 
	{
		gTerrain = 0;



	}
	~SPTDemoApplication()
	{
		destroySPT();

		gBrush0 = NULL;
		gBrush1 = NULL;
		gBrush2 = NULL;


	}


protected:


    // Just override the mandatory create scene method
    void createScene(void)
    {
		/*mWindow->resize(1280,720);
		mWindow->windowMovedOrResized();
		mCamera->setAspectRatio(1280.0f / 720.0f);*/

		

		LogManager::getSingleton().getDefaultLog()->setLogDetail(Ogre::LL_BOREME);
        // Set ambient light
        mSceneMgr->setAmbientLight(ColourValue(0.5,0.5,0.5));
	
		gViewport = mCamera->getViewport();

		gBrush0 = BrushPtr(new Brush("brush0.png"));
		gBrush1 = BrushPtr(new Brush("brush1.png"));
		gBrush2 = BrushPtr(new Brush("brush2.png"));

		gBrush0->setInterpolated(true);
		gBrush1->setInterpolated(true);
		gBrush2->setInterpolated(true);
		



		gSunLight = mSceneMgr->createLight("Sun");
		gSunLight->setType(Light::LT_DIRECTIONAL);
		gSunLight->setDirection(-0.808863, -0.488935, -0.326625);


		mCamera->getViewport()->setBackgroundColour(ColourValue(0.75,0.75,0.75));
		mCamera->setNearClipDistance(10);
		mCamera->setFarClipDistance(100000);

		mCamera->setPosition(0,2500,3500);
		mCamera->lookAt(0,0,0);


		gFollowNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("ChaseNode");

		createSPT(mCamera,mSceneMgr);

		gCurrentScheme = gTerrain->getDefaultMaterialScheme();

		gTerrain->testApply("ScatteredOgre");

		ManualObject* pManualObj = mSceneMgr->createManualObject("RectOutline");
		pManualObj->begin("",RenderOperation::OT_LINE_STRIP);

		pManualObj->position(-0.5f,0,-0.5f);
		pManualObj->position(0.5f,0,-0.5f);
		pManualObj->position(0.5f,0,0.5f);
		pManualObj->position(-0.5f,0,0.5f);
		pManualObj->position(-0.5f,0,-0.5f);

		pManualObj->end();

		gRectNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		gRectNode->attachObject(pManualObj);


    }

	void createFrameListener()
	{
		mFrameListener= new SPTDemoFrameListener(mSceneMgr, mWindow, mCamera);
		mFrameListener->showDebugOverlay(true);
		mRoot->addFrameListener(mFrameListener);
	}

};
