/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NETWORK_MESSAGES_H
#define NETWORK_MESSAGES_H

#include "MessageIdentifiers.h"

enum
{
	// Server list.
	ID_SERVER_LIST = ID_USER_PACKET_ENUM,
	// Login information.
	ID_LOGIN_INFO,
	// Unique session key.
	ID_SESSION_KEY,
	// Character list request from client to server.
	ID_CHARACTER_LIST_REQUEST,
	// Updated character list.
	ID_CHARACTER_LIST,

	ID_JOIN_WORLD,
	ID_JOIN_WORLD_ACCEPTED,
	ID_LEAVE_WORLD,

	// Update information about all players in a certain area for the client.
	ID_SNAPSHOT, // Sends a snapshot with delta compression.
	ID_FULL_SNAPSHOT, // Sends a snapshot with the full information.

	ID_REQUEST_FULL_SNAPSHOT,

	// I'll probably make the full snapshot update the character's real position, then update the positions of things
	// relative to the player.  Same goes for the snapshot, except the character's position will be a delta compressed version.

	// Movement requests.
	ID_HALT,
	ID_FORWARD,
	ID_BACK,
	ID_LEFT,
	ID_RIGHT,
	ID_FORWARD_LEFT,
	ID_FORWARD_RIGHT,
	ID_BACK_LEFT,
	ID_BACK_RIGHT,


	// For simulation server.
	ID_RAYCAST_REQUEST,
	ID_RAYCAST_RESULT,
	ID_SIMULATION_SERVER_ACCEPTED,
	ID_SIMULATION_SERVER_CONNECT_REQUEST,

	ID_UPDATE_ORIENTATION,
	ID_UPDATE_POSITION,

	ID_CHAT_SAY,

	ID_COMMAND,
	ID_DB_QUERY_ACCOUNT,
	ID_DB_ADD_ACCOUNT,
	ID_NPC_SNAPSHOT,

	ID_REQUEST_NPC_INTERACTION,
	ID_INCOMING_NPC_INTERACTION,

	ID_CHARACTER_MOVEMENT_CHANGE,

	ID_INVENTORY_FULL_SNAPSHOT,
	ID_INVENTORY_UPDATE,

	ID_TRADE_OPEN,
	ID_TRADE_REQUEST,
	ID_TRADE_ADD,
	ID_TRADE_REMOVE,
	ID_TRADE_ACCEPT,
	ID_TRADE_CANCEL	
};

#endif
