/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVER_MAIN_H
#define SERVER_MAIN_H

#include <iostream>
#include <sstream>
#include <vector>
#include <RakPeer.h>
#include <RakNetworkFactory.h>
#include "../Dependencies/NetworkMessages.h"
#include <BitStream.h>
#include <StringCompressor.h>
#include <fstream>
#include <cmath>
#include <string>

#include "Timer.h"
#include "Character.h"
#include "Log.h"
#include "SQL_Data.h"
#include "Data.h"
#include "NPC.h"
#include "Item.h"

#include <ode/ode.h>

#include <mysql++.h>
#include <DBInterface.h>
#include <DBCharacter.h>
#include <Account.h>
#include <DBConnector.h>

#include <tinyxml.h>


namespace server
{

class Trade
{
public:
	Character *to;
	Character *from;
	std::vector<Item *> character_to_items;
	std::vector<Item *> character_from_items;
	bool character_to_accept;
	bool character_from_accept;

	Trade() : character_to_accept( false ), character_from_accept( false )
	{
	}
};

using namespace std;

#define MAX_CLIENTS 600

RakPeerInterface *mPeer;

std::vector<Trade> trade;

int mFrameCount = 0;
bool sim_server_connect = false;
bool sim_server_timeout = false;
bool use_internal_simulation = false;
bool runLoop;

std::vector<Character *> character_list;
std::vector<Character *> online_characters;
std::vector<int> chars_to_send;
SLog *mLog;
Timer *mTimer;

int elapse = 6;

dTriMeshDataID ode_model;
std::vector<dGeomID> ode_geom;
dGeomID ode_ray;
dSpaceID space;

//sqlite3 *db;
char **sql_argv;
int sql_argc;
bool sql_arg_fresh; // To tell if argv is new or old.
SQL_Character_Query sql_character_query;

NPC test_npc;
std::vector<NPC *> npc_list;

dbinterface::DBInterface *dbi;

std::vector<Item *> all_items; // Temporary list of all items until it is replaced by the database.

int int_pow( int base, int n )
{
	int tmp = base;
	for( int i = 1; i < n; i++ )
	{
		tmp *= base;
	}
	return tmp;
}

// 8 bits of pure hell on wheels right at your UDP doorstep.
void write256Integer( int number, RakNet::BitStream &stream )
{
	for( int i = 8; i > 0; i-- )
	{
		int place_weight = 128;
		if( number >= place_weight )
		{
			number -= place_weight;
			stream.Write1();
		}
		else
			stream.Write0();
	}
}

// Most efficient if using 2 or more in one packet and number is larger than 256, otherwise use write256Integer()
// limit must be result of a power of n base 2, so 1, 2, 4, 8, 16, 32, etc.
void writeLimitedInteger( int number, int limit, RakNet::BitStream &stream )
{
	int power = int(log(float(limit))) / int(log(2.0f)); // Log base 2 of limit.
	for( int i = power; i > 0; i-- )
	{
		int place_weight = int_pow( 2, i-1 );
		if( number >= place_weight )
		{
			number -= place_weight;
			stream.Write1();
		}
		else
			stream.Write0();
	}
}

void send_ID_INVENTORY_FULL_SNAPSHOT( Character *c )
{
	RakNet::BitStream outgoing;
	unsigned char id = ID_INVENTORY_FULL_SNAPSHOT;
	outgoing.Write( id );
	int size = (int)c->items.size();
	outgoing.Write( size );
	for( unsigned int i = 0; i < c->items.size(); i++ )
	{
		outgoing.Write( c->items[i]->id );
	}
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, c->systemAddress, false );
}

// Writes all nearby characters to nearby_characters to nearby_characters.
void getNearbyCharacters( Character *character, vector<Character *> &nearby_characters )
{
	// Doing a sequential search right now... About as inefficient as humanly possible.  We'll go back later and optimize things.
	for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
	{
		if( character->position_x < (online_characters[indx]->position_x + 10000) &&
			character->position_x > (online_characters[indx]->position_x - 10000) &&
			character->position_y < (online_characters[indx]->position_y + 10000) &&
			character->position_y > (online_characters[indx]->position_y - 10000) &&
			character->position_z < (online_characters[indx]->position_z + 10000) &&
			character->position_z > (online_characters[indx]->position_z - 10000)
			)
		{
			nearby_characters.push_back( online_characters[indx] );
		}
	}
}

void getInfo( RPCParameters *rpcParameters )
{
	for( unsigned int indx = 0; indx < character_list.size(); indx++ )
	{
		if( rpcParameters->sender == character_list[indx]->systemAddress && character_list[indx]->is_online )
		{
			rpcParameters->replyToSender->Write( character_list[indx]->position_x );
			rpcParameters->replyToSender->Write( character_list[indx]->position_y );
			rpcParameters->replyToSender->Write( character_list[indx]->position_z );
		}
	}
}

void log( std::string message )
{
	mLog->logMessage( message );
}

void sendCharacterList( std::string name, SystemAddress sa )
{
	std::vector<std::string> name_list;
	std::vector<float> money_list;

	unsigned char Id = ID_CHARACTER_LIST;
	
	for( int i = 0; i < character_list.size(); i++ )
	{
		if( character_list[i]->login_username.compare( name ) == 0 )
		{
			name_list.push_back( character_list[i]->name );
			money_list.push_back( character_list[i]->money );
		}
	}
	int size = name_list.size();

	RakNet::BitStream out;
	out.Write( Id );
	out.Write( size );
	for( int i = 0; i < size; i++ )
	{
		stringCompressor->EncodeString( name_list[i].c_str(), 128, &out );
		out.Write( money_list[i] );
	}
	mPeer->Send( &out, HIGH_PRIORITY, RELIABLE_ORDERED, 0, sa, false );
}

int str_to_int( std::string str )
{
	std::stringstream s;
	s << str;
	int i;
	s >> i;
	return i;
}

// Temporary hack to get characters from the DB.  Since the server will be re-done soon, this should be acceptable for now; albeit horridly inefficient.
void loadCharacterList()
{
	std::vector<dbinterface::Account *> accounts;
	try
	{
		if( dbi->getAllAccounts( accounts ) )
		{
			for( unsigned int i = 0; i < accounts.size(); i++ )
			{
				std::vector<dbinterface::DBCharacter *> dbi_characters;
				if( accounts[i]->getAllCharacters( dbi_characters ) )
				{
					for( unsigned int x = 0; x < dbi_characters.size(); x++ )
					{
						std::string name = std::string(dbi_characters[x]->getFirstName() + " " + dbi_characters[x]->getLastName());
						bool already_exists = false;
						for( unsigned int y = 0; y < character_list.size(); y++ )
						{
							if( name == character_list[y]->name )
							{
								already_exists = true;
								delete dbi_characters[x];
								dbi_characters[x] = 0;
								break;
							}
						}
						if( !already_exists )
						{
							Character *character = new Character();
							character_list.push_back( character );
							character->login_username = accounts[i]->getEmail();
							character->name = name;
							character->money = 0.0f;
							character->position_x = 0.0f;
							character->position_y = 0.0f;
							character->position_z = 0.0f;

							character->id = str_to_int( dbi_characters[x]->getCharacterID() );
							delete dbi_characters[x];
							dbi_characters[x] = 0;
						}
					}
				}
			}
		}
	}
	catch (mysqlpp::BadQuery& er) {
	// Handle any connection or query errors that may come up
	cerr << "Error: " << er.what() << endl;
	}
	catch (mysqlpp::BadConversion& er) {
	// Handle bad conversions
	cerr << "Error: " << er.what() << "\"." << endl
	<< "retrieved data size: " << er.retrieved
	<< " actual data size: " << er.actual_size << endl;
	return ;
	}
	catch (exception& er) {
	cerr << "Error: " << er.what() << endl;
	return;
	}
}

// DEPRECATED
void loadCharacterList_DEPRECATED()
{
	TiXmlDocument *m_document;
	double m_version = 1.0;
	std::string fileCharacterList("characterlist.xml");

	try
	{
		m_document = new TiXmlDocument( fileCharacterList.c_str() );
		m_document->LoadFile();
		if( m_document->Error() )	// Error will be reported here.
		{
			// Log the error
			std::string str1("Server/main.cpp:  Error opening file: ");
			str1.append( fileCharacterList );
			log( str1.c_str() );

			delete m_document;
			m_document = NULL;
			return;
		}
	}
	catch(...)
	{
		// Error will be reported here.
		delete m_document;
		m_document = NULL;
		return;
	}

	
	TiXmlElement *m_root_element = m_document->RootElement();
	if( strcmp( m_root_element->Value(), "CharacterList" ) != 0 )
	{
		// Error will be thrown here.  The file is not in the correct format.
	}
	if( atof(m_root_element->Attribute( "version" )) != m_version )
	{
		// Error will be thrown here.  Incorrect version.
	}

	TiXmlElement *element = m_root_element->FirstChildElement( "Account" );
	while( element )
	{
		TiXmlElement *_element = element->FirstChildElement( "Character" );
		while( _element )
		{
			Character *character = new Character();
			character_list.push_back( character );
			character->login_username = std::string( element->Attribute( "Username" ) );

			character->name = std::string( _element->Attribute( "Name" ) );
			character->money = atof( _element->Attribute( "Money" ) );
			character->position_x = atof( _element->Attribute( "Position_X" ) );
			character->position_y = atof( _element->Attribute( "Position_Y" ) );
			character->position_z = atof( _element->Attribute( "Position_Z" ) );

			SLog *log = new SLog( "logs/characters/" + character->name + ".log", false );
			character->log = log;

			std::string st;
			st.append(             "==================" );
			for( int i = 0; i < character->name.size(); i++ )
				st.append( "=" );
			st.append(                                                      "======================" );
			log->logSilentMessage( st );
			log->logSilentMessage( "Character log for " + character->name + " successfully created!" );

			_element = _element->NextSiblingElement();
		}

		element = element->NextSiblingElement();
	}
	delete m_document;
}

int random_range(int lowest_number, int highest_number)
{
    if(lowest_number > highest_number){
        swap(lowest_number, highest_number);
    }

    int range = highest_number - lowest_number + 1;
    return lowest_number + int(range * rand()/(RAND_MAX + 1.0));
}

// Updates all characters near specified character of specified movement change.
void player_movement_change( Character *character, short movement )
{
	std::vector<Character *> chars_to_send;
	for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
	{
		if( character->position_x < (online_characters[indx]->position_x + 10000) &&
			character->position_x > (online_characters[indx]->position_x - 10000) &&
			character->position_y < (online_characters[indx]->position_y + 10000) &&
			character->position_y > (online_characters[indx]->position_y - 10000) &&
			character->position_z < (online_characters[indx]->position_z + 10000) &&
			character->position_z > (online_characters[indx]->position_z - 10000)
			)
		{
			chars_to_send.push_back( online_characters[indx] );
		}
	}

	RakNet::BitStream outgoing;
	unsigned char type_id = ID_CHARACTER_MOVEMENT_CHANGE;
	outgoing.Write( type_id );
	stringCompressor->EncodeString( character->name.c_str(), 64, &outgoing );
	outgoing.Write( character->current_movement );
	log( character->name.c_str() );
	for( int i = 0; i < chars_to_send.size(); i++ )
	{
		mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, chars_to_send[i]->systemAddress, false );
	}
}

void leave_world( Packet *packet )
{
	bool found_one = false;
	for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
	{
		if( online_characters[indx]->systemAddress == packet->systemAddress )
		{
			log( "Player " + online_characters[indx]->name + " has disconnected from the server. " + "(" + std::string(packet->systemAddress.ToString()) + ")" );
			online_characters[indx]->is_online = false;
			online_characters.erase( online_characters.begin() + indx );
			found_one = true;
		}
	}
	if( !found_one )
	{
		log( "Could not find character when attempted to leave world." );
	}
}

void send_full_snapshot( Character *character )
{
	// Doing a sequential search right now... About as inefficient as humanly possible.  We'll go back later and optimize things.
	for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
	{
		if( character->position_x < (online_characters[indx]->position_x + 1000) &&
			character->position_x > (online_characters[indx]->position_x - 1000) &&
			character->position_y < (online_characters[indx]->position_y + 1000) &&
			character->position_y > (online_characters[indx]->position_y - 1000) &&
			character->position_z < (online_characters[indx]->position_z + 1000) &&
			character->position_z > (online_characters[indx]->position_z - 1000)
			)
		{
			chars_to_send.push_back( indx );
		}
	}

	int number_of_players = chars_to_send.size();

	RakNet::BitStream outgoing;
	unsigned char typeId = ID_FULL_SNAPSHOT;
	outgoing.Write( typeId );

	number_of_players = chars_to_send.size();
	outgoing.Write( number_of_players );

	for( int indx = 0; indx < number_of_players; indx++ )
	{
		int temp = chars_to_send[indx];
		if( online_characters[temp] == character )
		{
			// This is the client's character... Need to inform the client that this is the case.
			stringCompressor->EncodeString( "__CLIENT__", 128, &outgoing );
		}
		else
		{
			stringCompressor->EncodeString( online_characters[temp]->name.c_str(), 128, &outgoing );
			//std::cout << online_characters[temp]->name.c_str();
		}

		outgoing.Write( online_characters[temp]->position_x );
		outgoing.Write( online_characters[temp]->position_y );
		outgoing.Write( online_characters[temp]->position_z );
		outgoing.Write( online_characters[temp]->orientation_w );
		outgoing.Write( online_characters[temp]->orientation_x );
		outgoing.Write( online_characters[temp]->orientation_y );
		outgoing.Write( online_characters[temp]->orientation_z );
	}

	// Next comes any NPCs in the area.
	// For now we just send the test NPC.
	outgoing.Write( 1 ); // Number of NPCs
	outgoing.Write( test_npc.position_x );
	outgoing.Write( test_npc.position_y );
	outgoing.Write( test_npc.position_z );
	stringCompressor->EncodeString( test_npc.model.c_str(), 128, &outgoing );
	stringCompressor->EncodeString( test_npc.greeting_text.c_str(), 256, &outgoing );
	stringCompressor->EncodeString( test_npc.name.c_str(), 128, &outgoing );


	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, character->systemAddress, false );


	chars_to_send.clear();
}

// Sends list of all npcs nearby to specified character's system.
void send_npc_list( Character *character )
{
	RakNet::BitStream outgoing;
	unsigned char typeId = ID_NPC_SNAPSHOT;

	// NPC list maintained in the database, so retrieve them then build the nearby ones into a new list.
	// For now I'll just send out a placeholder then come back in later once the db is ready.


	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, character->systemAddress, false );
}

void connectToSimulationServer()
{
	string ip;
	ifstream file;
	file.open( "simulation_server.ip" );
	if( !file )
	{
		log( "ERROR: Unable to open file simulation_server.ip" );
		return;
	}
	file >> ip;
	file.close();
	mPeer->Connect( ip.c_str(), 38742, 0, 0 );
}

void send_raycast_request( Packet *packet )
{
}

void update()
{
	if( !sim_server_connect && !sim_server_timeout )
	{
		if( mTimer->end() >= 10.0 )
		{
			//log("Simulation server timed out.");
			sim_server_timeout = true;
			use_internal_simulation = true;
			mTimer->begin();
		}
	}

	if( mTimer->end() >= 30.0 )
	{
		loadCharacterList(); // Loads the character list periodically to keep things fresh.
		log( "CharacterList refresh" );
		mTimer->begin();
	}
}

void loadWorld()
{
}

void send_chat_message_to_player( std::string type, Character *receive_character, Character *send_character, std::string message )
{
	unsigned char typeId;

	if( type == "say" )
	{
		typeId = ID_CHAT_SAY;
	}
	else
	{
		log( "Bad send_chat_message_to_player type... Internal error!" );
		return;
	}

	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	stringCompressor->EncodeString( message.c_str(), 128, &outgoing );
	stringCompressor->EncodeString( receive_character->name.c_str(), 128, &outgoing );
	stringCompressor->EncodeString( send_character->name.c_str(), 128, &outgoing );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, receive_character->systemAddress, false );
}

bool parseBool( std::string b )
{
	if( b == "true" )
		return true;
	if( b == "false" )
		return false;
	return false;
}

std::string toString( int val )
{
	std::stringstream out;
	out << val;
	return out.str();
}
std::string toString( float val )
{
	std::stringstream out;
	out << val;
	return out.str();
}

// Loads an Ogre *.mesh.xml file into Opcode.
bool loadModel( std::string file,
			    Point offset = Point(0,0,0),
			    Point scale = Point(1,1,1) )
{
	file = "resources/" + file;

	int *it = 0;
	dVector3 *vec = 0;

	int f_count = 0;
	int v_count = 0;
	int counter = 0;

	TiXmlDocument *m_document;
	try
	{
		m_document = new TiXmlDocument( file.c_str() );
		m_document->LoadFile();
		if( m_document->Error() )	// Error will be reported here;
		{
			// Log the error
			std::string str1("Server/main.cpp:  Error opening file: ");
			str1.append(file);
			log( str1.c_str() );
			
			delete m_document;
			m_document = NULL;
			return false;
		}
	}
	catch(...)
	{
		// Error will be reported here.
		delete m_document;
		m_document = NULL;
		return false;
	}

	
	TiXmlElement *m_root_element = m_document->RootElement();
	if( strcmp( m_root_element->Value(), "mesh" ) != 0 )
	{
		log( "Invalid file loaded... " + file );
	}

	TiXmlElement *element = m_root_element->FirstChildElement( "submeshes" );
	while( element )
	{
		TiXmlElement *_element = element->FirstChildElement( "submesh" );
		while( _element )
		{
			TiXmlElement *__element = _element->FirstChildElement( "faces" );
			f_count = atoi(__element->Attribute( "count" ));

			// Make a dynamic array;
			it = new int[f_count*3];
			

			// Make sure there is a face count bigger than 0;
			if (f_count  <= 0)
			{
				if ( it )	// if 'it' has a dynamic array, then free it;
				{
					delete [] it;
				}

				log( "Server/main.cpp:  Error Face count is less than 1 in 'loadModel()'");
				return false;
			}

			// Check for memory allocation error. If there is, then return false;
			if (it == 0)
			{
				log( "Server/main.cpp:  Error allocating dynamic memory for 'it' in 'loadModel()'");
				return false;
			}


			//cout << "FACE COUNT = " << f_count << endl;

			if( __element )
			{
				counter = 0;	// Reset the counter;
				

				TiXmlElement *___element = __element->FirstChildElement( "face" );
				while( ___element )
				{
					//cout <<"FACE:  ";
					it[counter] = atoi(___element->Attribute( "v1" ));
					//cout << it[counter] << ", ";
					counter++;

					it[counter] = atoi(___element->Attribute( "v2" ));
					//cout << it[counter] << ", ";
					counter++;

					it[counter] = atoi(___element->Attribute( "v3" ));
					//cout << it[counter] << endl;
					counter++;

					___element = ___element->NextSiblingElement( "face" );
				}
				//cout << "END OF FACE" << endl;
				//cout << "count = " << counter << "  , f_count = " << f_count << endl;
				
				// Make sure we got all the faces. If not then free memory and return false;
				// Divide the counter by 3 (3 vertex)to get the face count;
				counter /=  3;
				if ( counter <=0 || counter != f_count)
				{
					if ( it )	// if 'it' has a dynamic array, then free it;
					{
						delete [] it;
					}

					log( "Server/main.cpp:  Error did not get all faces in 'loadModel()'");
					return false;
				} 
			}
		
			TiXmlElement *geometry = _element->FirstChildElement( "geometry" );
			if( geometry )
			{
				v_count = atoi(geometry->Attribute( "vertexcount" ));

				// Make a dynamic array;
				vec = new dVector3[v_count];
				
				// Make sure there is a vertex count bigger than 0;
				if (v_count  <= 0)
				{
					if ( it )	// if 'it' has a dynamic array, then free it;
					{
						delete [] it;
					}

					if ( vec )	// if 'vec' has a dynamic array, then free it;
					{
						delete [] vec;
					}

					log( "Server/main.cpp:  Error Vertex count is less than 1 in 'loadModel()'");
					return false;
				}

				// Check for memory allocation error. If there is, then free memory and return false;
				if (vec == 0)
				{
					if ( it )	// if 'it' has a dynamic array, then free it;
					{
						delete [] it;
					}

					log( "Server/main.cpp:  Error allocating dynamic memory for 'vec' in 'loadModel()'");
					return false;
				}


				counter = 0;	// Reset the counter;

				//cout << "VERTEX COUNT = " << v_count << endl;

				TiXmlElement *vertex_buffer = geometry->FirstChildElement( "vertexbuffer" );
				if( vertex_buffer )
				{
					if( parseBool(vertex_buffer->Attribute( "positions" )) )
					{
						TiXmlElement *vertex = vertex_buffer->FirstChildElement( "vertex" );
						while( vertex )
						{
							//cout << "VERTEX:  " << "(" << counter << ") ";

							TiXmlElement *position = vertex->FirstChildElement( "position" );
							
							vec[counter][0] = atof(position->Attribute( "x" )) * ((float)scale.x);
							//cout << vec[counter][0] << ", ";

							vec[counter][1] = atof(position->Attribute( "y" )) * ((float)scale.y);
							//cout << vec[counter][1] << ", ";

							vec[counter][2] = atof(position->Attribute( "z" )) * ((float)scale.z);
							//cout << vec[counter][2] << endl;
							
							counter++;	// Update the counter;

							vertex = vertex->NextSiblingElement( "vertex" );
						}
						//cout << "VERTEX END" << endl;
						//cout << "counter = " << counter << "  , v_count = " << v_count << endl;

						// Make sure we got all the vertex. If not then free memory and return false;
						if ( counter != v_count)
						{
							if ( it )	// if 'it' has a dynamic array, then free it;
							{
								delete [] it;
							}

							if ( vec )	// if 'vec' has a dynamic array, then free it;
							{
								delete [] vec;
							}
		
							log( "Server/main.cpp:  Error did not get all vertex in 'loadModel()'");
							return false;
						}
					}
				}
			}

			_element = _element->NextSiblingElement();
		}

		element = element->NextSiblingElement();
	}

	dGeomID geom;

	std::string type = "box"; // This is here for now for testing purposes.
	if( type == "box" )
	{
		int least = -1;
		float least_value = 0.0f;
		int greatest = -1;
		float greatest_value = 0.0f;

		for( int i = 0; i < v_count; i++ ) // Find the minimum and maximum corners of the cube.
		{
			float total = vec[i][0] + vec[i][1] + vec[i][2];
			if( least == -1 )
			{
				least_value = total;
				least = i;
			}
			if( greatest == -1 )
			{
				greatest_value = total;
				greatest = i;
			}
		
			if( total < least_value )
			{
				least_value = total;
				least = i;
			}
			else if( total > greatest_value )
			{
				greatest_value = total;
				greatest = i;
			}
		}

		if( least == -1 || greatest == -1 )
		{
			// Serious problem!
			log( "ERROR: main.cpp: Failed loading physics model type box." );
			return false;
		}

		/* Need to watch this code... Could do something funky... Pull a fast one if you catch my drift. */

		Point min_corner( vec[least][0], vec[least][1], vec[least][2] );
		//cout << "min_corner x:" << min_corner.x << " y:" << min_corner.y << " z: " << min_corner.z << "\n";
		Point max_corner( vec[greatest][0], vec[greatest][1], vec[greatest][2] );
		//cout << "max_corner x:" << max_corner.x << " y:" << max_corner.y << " z: " << max_corner.z << "\n";
		Point center( (min_corner.x + max_corner.x) / 2, (min_corner.y + max_corner.y) / 2, (min_corner.z + max_corner.z) / 2 );
		//cout << "center x:" << center.x << " y:" << center.y << " z: " << center.z << "\n";
		Point fixed_max_corner( max_corner.x - center.x, max_corner.y - center.y, max_corner.z - center.z );
		//cout << "fixed_max_corner x:" << fixed_max_corner.x << " y:" << fixed_max_corner.y << " z: " << fixed_max_corner.z << "\n";

		geom = dCreateBox( space, max_corner.x - min_corner.x, max_corner.y - min_corner.y, max_corner.z - min_corner.z );
		ode_geom.push_back( geom );

		// Distance from original (fixed) max_corner to the new max_corner.
		float dist_orig_new = sqrt( pow((max_corner.x - fixed_max_corner.x),2) +
					    pow((max_corner.y - fixed_max_corner.y),2) +
					    pow((max_corner.z - fixed_max_corner.z),2) );
		//cout << "dist_orig_new: " << dist_orig_new << "\n";

		// Distance from center of the box to the new max_corner.
		float dist_center_new = sqrt( pow((max_corner.x - center.x),2) +
					      pow((max_corner.y - center.y),2) +
					      pow((max_corner.z - center.z),2) );
		//cout << "dist_center_new: " << dist_center_new << "\n";

		// Returns angle between the two (i.e. how much the modeler rotated it).
		float angle = asin( dist_orig_new / dist_center_new );
		//cout << "angle: " << angle << "\n";

		// Now the primitive can be rotated to the proper orientation.
		dMatrix3 matrix;
		dRFromAxisAndAngle( matrix, 0, 1, 0, angle );
		dGeomSetRotation( geom, matrix );
	}



	//ode_model = dGeomTriMeshDataCreate();
	//dGeomTriMeshDataBuildSimple( ode_model, (dReal*)vec, v_count, it, f_count );
	//dGeomTriMeshDataBuildSingle( ode_model, (dReal*)vec, 3 * sizeof(float), v_count, it, f_count, 3 * sizeof(int) );
	//ode_geom = dCreateTriMesh( space, ode_model, 0, 0, 0 );
	dGeomSetPosition( geom, offset.x, offset.y, offset.z );
	//cout << dGeomGetPosition( ode_geom )[1];
	//dGeomSetPosition( ode_geom, 0, 0, 0 );

	// Free the dynamic memory;
	if( it ) delete [] it;
	if( vec ) delete [] vec;
	delete m_document;

	return true;
}

// Returns an item based on id, otherwise 0.
Item *getItem( int id )
{
	for( int i = 0; i < all_items.size(); i++ )
	{
		if( all_items[i]->id == id )
		{
			return all_items[i];
		}
	}
	return 0;
}

void setupODE()
{
	space = dSimpleSpaceCreate( 0 );
	ode_ray = dCreateRay( space, 200 );
}

bool rayCast( Point dir, Point orig, Point &intersection, float &depth )
{
	// Eventually this needs to be subdivided so that there won't be as much overhead.
	dGeomRaySet( ode_ray, orig.x, orig.y, orig.z, dir.x, dir.y, dir.z );
	dContactGeom geom[1];
	for( int i = 0; i < ode_geom.size(); i++ )
	{
		if( dCollide( ode_ray, ode_geom[i], 1, geom, sizeof(dContactGeom) ) == 1 )
		{
			intersection = Point( geom[0].pos[0], geom[0].pos[1], geom[0].pos[2] );
			depth = geom[0].depth;
			return true;
		}
	}
	return false;
}

static int sql_callback(void *custom_data, int argc, char **argv, char **azColName)
{
	int i;
	for(i=0; i<argc; i++)
	{
		log( argv[i] );
	}
	std::string custom = *(std::string *)custom_data;

	if( custom == std::string("character_query") )
	{
		// line 1: name
		// line 2: pass
		// line 3: pos_x
		// line 4: pos_y
		// line 5: pos_z
		sql_character_query.name = argv[0];
		sql_character_query.password = argv[1];
		sql_character_query.position.x = atof(argv[2]);
		sql_character_query.position.y = atof(argv[3]);
		sql_character_query.position.z = atof(argv[4]);
	}

	sql_argv = argv;
	sql_argc = argc;
	sql_arg_fresh = false;

	return 0;
}

void sql_query( std::string command, std::string custom_data )
{
	char *zErrMsg = 0;
//	int rc = sqlite3_exec( db, command.c_str(), sql_callback, &custom_data, &zErrMsg );
//	if( rc != SQLITE_OK )
//	{
//		log( "SQL error: " + std::string(zErrMsg) );
//		sqlite3_free( zErrMsg );
//	}
}

Character *getCharacter( SystemAddress systemAddress )
{
	for( unsigned int indx = 0; indx < character_list.size(); indx++ )
	{
		if( character_list[indx]->systemAddress == systemAddress )
		{
			return character_list[indx];
		}
	}

	return 0;
}
Character *getCharacter( std::string username )
{
	for( unsigned int indx = 0; indx < character_list.size(); indx++ )
	{
		if( character_list[indx]->name == username )
		{
			return character_list[indx];
		}
	}

	return 0;
}

void send_ID_UPDATE_POSITION( Point pos, SystemAddress system_address )
{
	char typeId = ID_UPDATE_POSITION;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	outgoing.Write( pos.x );
	outgoing.Write( pos.y );
	outgoing.Write( pos.z );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, system_address, false );
}

// name of npc, text npc is sending, systemAddress to send to.
void send_npc_interaction( std::string name, std::string text, SystemAddress system_address )
{
	char typeId = ID_INCOMING_NPC_INTERACTION;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	stringCompressor->EncodeString( name.c_str(), 128, &outgoing );
	stringCompressor->EncodeString( text.c_str(), 1024, &outgoing );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, system_address, false );
}

// All Characters in the trade queue will be unable to open another trade until the current one is closed.
Trade &add_trade_to_queue( Character *from, Character *to )
{
	Trade t;
	t.from = from;
	t.to = to;
	trade.push_back( t );
	return t;
}

 // Sent from the server to the players to inform that the trade is ready to commence.
void send_trade_open( Trade &t )
{
	char typeId = ID_TRADE_OPEN;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, t.from->systemAddress, false );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, t.to->systemAddress, false );
}

// Sent to players involved in trade to let them know a cancel has occured.
void send_trade_cancel( Trade &t )
{
	char typeId = ID_TRADE_CANCEL;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, t.from->systemAddress, false );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, t.to->systemAddress, false );
}

/*
 * add:
 *		false = remove item
 *		true = add item
 */
void send_inventory_update_notification( Character *c, bool add, Item *item )
{
	char typeId = ID_INVENTORY_UPDATE;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	outgoing.Write( add );
	outgoing.Write( item->id );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, c->systemAddress, false );
}

bool item_transfer( Item *item, Character *from, Character *to )
{
	// Check to make sure player 'from' has item.
	bool completed = false;
	for( int i = 0; from->items.size(); i++ )
	{
		if( from->items[i] == item )
		{
			delete from->items[i];
			from->items.erase(from->items.begin() + i);
			completed = true;
		}
	}

	// If player doesn't have the item, return false.
	if (!completed)
	{
		return false;
	}

	// Add the item to the 'to' player's inventory.
	to->items.push_back(item);

	// Inform the players.
	send_inventory_update_notification( from, false, item );
	send_inventory_update_notification( to, true, item );

	return true;
}

bool perform_trade( Trade &t )
{
	// Make sure both players have accepted the trade.
	if( !t.character_from_accept || !t.character_to_accept )
		return false;

	// Transfer all items from 'to' to 'from'
	for( int i = 0; i < t.character_to_items.size(); i++ )
	{
		if( !item_transfer( t.character_to_items[i], t.to, t.from ) )
		{
			log( "FATAL ERROR: Item transfer failed!" );
			return false;
		}
	}

	// Transfer all items from 'from' to 'to'
	for( int i = 0; i < t.character_from_items.size(); i++ )
	{
		if( !item_transfer( t.character_from_items[i], t.from, t.to ) )
		{
			log( "FATAL ERROR: Item transfer failed!" );
			return false;
		}
	}

	return true;
}

int init_server()
{
	mTimer = new Timer();
	mLog = new SLog( "server.log" );
	setupODE();

	mLog->logMessage( "===========================================" );
	mLog->logMessage( "Project Ura Interface Server is now online." );
	mLog->logMessage( "===========================================\n" );

	// ODE stuff... ODE is a little bit messed up at the moment.
	
	//loadModel( "cube.mesh.xml", Point(0,0,0), Point(1,5,1) );
	//loadModel( "cube.mesh.xml", Point(0,-15,0) );
	loadModel( "rect.mesh.xml" );
//	Point result;
//	float depth;
//	if( rayCast( Point(0,-1,0), Point(0,100,0), result, depth ) )
//	{
//		log( "Result x: " + toString( result.x ) );
//		log( "Result y: " + toString( result.y ) );
//		log( "Result z: " + toString( result.z ) );
//		log( "Depth: " + toString( depth ) );
//	}
//
//return 0; // break it for testing purposes.
	


	
	sql_arg_fresh = true;
	//int rc = sqlite3_open( "../test.db", &db );
	//if( rc )
	//{
	//	fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	//	sqlite3_close(db);
	//	return 1;
	//}

	


	/*sqlite3 *db;
	char *zErrMsg = 0;
	int rc;

	rc = sqlite3_open( "test.db", &db );
	if( rc )
	{
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
		sqlite3_close(db);
		return 1;
	}
	std::string n = "use";
	rc = sqlite3_exec(db, "SELECT one FROM test", sql_callback, &n, &zErrMsg);
	if( rc!=SQLITE_OK )
	{
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	sqlite3_close(db);*/

	 

	/**********************
	 * INITIALIZE NETWORK *
	 **********************/
	mPeer = RakNetworkFactory::GetRakPeerInterface();
	mPeer->Startup( MAX_CLIENTS, 0, &SocketDescriptor( 38741, 0 ), 1 );
	mPeer->SetMaximumIncomingConnections( MAX_CLIENTS );


	///////////////////////////////////////////////////////////
	/// Connect to the database	on the mysqld port			///
	/// Database name = 'test', user='test', pass='test'	///
	///////////////////////////////////////////////////////////
	//dbi = new dbinterface::DBInterface( "TrinityReign", "127.0.0.1", "monty", "some_pass", 3306 );
	dbi = new dbinterface::DBInterface( "test", "localhost", "test", "test", 3306 );
	 
	
	loadCharacterList();
	connectToSimulationServer();


	for( unsigned int indx = 0; indx < character_list.size(); indx++ )
	{
		character_list[indx]->is_online = false;
	}

	REGISTER_STATIC_RPC( mPeer, getInfo );

	test_npc.position_x = -14.072f;
	test_npc.position_y = 747.02f;
	test_npc.position_z = -427.885f;
	test_npc.greeting_text = "Wassup G UNIT!!!";
	test_npc.model = "athene.mesh";
	test_npc.name = "testnpc";

	npc_list.push_back( &test_npc );

	runLoop = true;



/*****************
	Character *character = new Character();
	character->name = "Joe Walsh";
	player_movement_change( character, 0 );

	for( int i = 0; i < 5; i++ )
	{
		Item *item = new Item();
		item->id = i;
		all_items.push_back( item );
	}

	Character *c = getCharacter( "Denny Kunker" );

	if( c )
	{
		for( unsigned int i = 0; i < all_items.size(); i++ )
		{
			c->items.push_back( all_items[i] );
		}
	}
*************************/
	
	 

	return 42;
}

int update_server()
{
	update();

	Packet *mPacket = mPeer->Receive();

	if( mPacket )
	{
		switch( mPacket->data[0] )
		{
		case ID_SIMULATION_SERVER_ACCEPTED:
			log("Successfully connected to simulation server.");
			sim_server_connect = true;
			break;
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			leave_world( mPacket );
			break;
		case ID_REMOTE_CONNECTION_LOST:
			leave_world( mPacket );
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED: // This only tries to connect to the simulation server, so can only be accepted by simulation server.
			{
				if( !sim_server_connect )
				{
					unsigned char Id = ID_SIMULATION_SERVER_CONNECT_REQUEST;
					RakNet::BitStream out;
					out.Write( Id );
					stringCompressor->EncodeString( "alkajsdf89asd8973;jkjalkdsjf;las78*9432olhjklhsjkflda7893241hkjhasdfkbcmx,vxc7dsaf", 64, &out );
					mPeer->Send( &out, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mPacket->systemAddress, false );
				}
				else
				{
					// Probably some kind of hacking attempt... Log and disconnect whoever sent it.
					mPeer->CloseConnection( mPacket->systemAddress, true );
					mLog->logMessage( "IP " + std::string(mPacket->systemAddress.ToString()) + " has been detected attempting to tamper with the system.  Disconnecting user." );
				}
			}
			break;
		case ID_NEW_INCOMING_CONNECTION:
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			leave_world( mPacket );
			break;
		case ID_CONNECTION_LOST:
			leave_world( mPacket );
			break;
		case ID_LOGIN_INFO:
			break;
		case ID_CHARACTER_LIST_REQUEST:
			{
				char name[128];
				unsigned char Id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( Id );
				stringCompressor->DecodeString( name, 128, &incoming );
				sendCharacterList( std::string( name ), mPacket->systemAddress );
			}
			break;
		case ID_JOIN_WORLD:
			{
				unsigned char Id;
				char character_name[128];
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( Id );
				stringCompressor->DecodeString( character_name, 128, &incoming );

				for( unsigned int indx = 0; indx < character_list.size(); indx++ )
				{
					if( character_list[indx]->name.compare( character_name ) == 0 )
					{
						character_list[indx]->is_online = true;
						character_list[indx]->systemAddress = mPacket->systemAddress;
						online_characters.push_back( character_list[indx] );
						log( std::string(character_name) + " has entered world." );
						sql_query( "select * from character where name = '" + std::string(character_name) + "';", "character_query" );
						/*if( sql_arg_fresh )
						{
							log( "ARG NOT FRESH.  CALLBACK ISSUE." );
						}
						else
						{*/
						send_ID_UPDATE_POSITION( Point( sql_character_query.position.x,
														sql_character_query.position.y,
														sql_character_query.position.z ),
														character_list[indx]->systemAddress );

						/*}*/
					}
				}

				/*
				 * Normally the user will only be given permission to enter the world
				 * when everything is cleared that there aren't two instances of his
				 * character amongst other possible issues.  For now, I'll just let
				 * them in since the code is very undeveloped.
				 */

				unsigned char typeId = ID_JOIN_WORLD_ACCEPTED;
				RakNet::BitStream out;
				out.Write( typeId );
				mPeer->Send( &out, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mPacket->systemAddress, false );
			}
			break;
		case ID_LEAVE_WORLD:
			leave_world( mPacket );
			break;
		case ID_REQUEST_FULL_SNAPSHOT:
			// For now, send out full snapshot until delta snapshot gets implemented.
			for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
			{
				if( mPacket->systemAddress == online_characters[indx]->systemAddress )
					send_full_snapshot( online_characters[indx] );
			}
			break;
		case ID_HALT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_HALT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_HALT );
			}
			break;
		case ID_FORWARD:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_FORWARD;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_FORWARD );
			}
			break;
		case ID_BACK:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_BACK;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_BACK );
			}
			break;
		case ID_LEFT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_LEFT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_LEFT );
			}
			break;
		case ID_RIGHT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_RIGHT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_RIGHT );
			}
			break;
		case ID_BACK_LEFT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_BACK_LEFT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_BACK_LEFT );
			}
			break;
		case ID_BACK_RIGHT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_BACK_RIGHT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_BACK_RIGHT );
			}
			break;
		case ID_FORWARD_LEFT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_FORWARD_LEFT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_FORWARD_LEFT );
			}
			break;
		case ID_FORWARD_RIGHT:
			{
				// Update the player instance with the movement ID.
				Character *character = getCharacter( mPacket->systemAddress );
				character->current_movement = ID_FORWARD_RIGHT;

				// Now update all characters within a certain range with the halt command.
				player_movement_change( character, ID_FORWARD_RIGHT );
			}
			break;

		case ID_RAYCAST_REQUEST:
			if( !use_internal_simulation )
			{
				// Make sure that (x) seconds have elapsed before allowing this request to be sent to the simulation server, then send.
				send_raycast_request( mPacket );
			}
			else if( use_internal_simulation )
			{
				// Simulate locally... For use in low pop servers.

			}
			break;
		case ID_UPDATE_ORIENTATION: // only yaw needed from client.
			{
				float w, x, y, z;
				unsigned char Id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( Id );
				incoming.Read( w );
				incoming.Read( x );
				incoming.Read( y );
				incoming.Read( z );
				Character *character = getCharacter( mPacket->systemAddress );
				character->orientation_w = w;
				character->orientation_x = x;
				character->orientation_y = y;
				character->orientation_z = z;

				// Now send the orientation update to all nearby characters.
				std::vector<Character *> nearby_characters;
				getNearbyCharacters( character, nearby_characters ); // Populates the vector.

				RakNet::BitStream outgoing;
				unsigned char type_id = ID_UPDATE_ORIENTATION;
				outgoing.Write( type_id );
				stringCompressor->EncodeString( character->name.c_str(), 64, &outgoing );
				outgoing.Write( w );
				outgoing.Write( x );
				outgoing.Write( y );
				outgoing.Write( z );

				for( int i = 0; i < nearby_characters.size(); i++ )
				{
					mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, nearby_characters[i]->systemAddress, false );
				}
			}
			break;
		case ID_UPDATE_POSITION:
			{
				float x, y, z;
				unsigned char Id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( Id );
				incoming.Read( x );
				incoming.Read( y );
				incoming.Read( z );
				for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
				{
					if( online_characters[indx]->systemAddress == mPacket->systemAddress )
					{
						online_characters[indx]->position_x = x;
						online_characters[indx]->position_y = y;
						online_characters[indx]->position_z = z;
					}
				}
			}
			break;
		case ID_CHAT_SAY:
			{
				Character *character = 0;
				char message[128];
				unsigned char typeId;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( typeId );
				stringCompressor->DecodeString( message, 128, &incoming );
				for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
				{
					if( online_characters[indx]->systemAddress == mPacket->systemAddress )
					{
						character = online_characters[indx];
					}
				}

				if( character )
				{
					for( unsigned int indx = 0; indx < online_characters.size(); indx++ )
					{
						if( character->position_x < (online_characters[indx]->position_x + 1000) &&
							character->position_x > (online_characters[indx]->position_x - 1000) &&
							character->position_y < (online_characters[indx]->position_y + 1000) &&
							character->position_y > (online_characters[indx]->position_y - 1000) &&
							character->position_z < (online_characters[indx]->position_z + 1000) &&
							character->position_z > (online_characters[indx]->position_z - 1000)
							)
						{
							send_chat_message_to_player( "say", online_characters[indx], character, std::string(message) );
							log( "Chat message (" + character->name + "): " + std::string(message) );
						}
					}
				}
			}
			break;
		case ID_COMMAND:
			{
				unsigned char id;
				char command[128];
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id );
				stringCompressor->DecodeString( command, 128, &incoming );
				std::string str_cmd = std::string(command);

				if( str_cmd == "admin_shutdown" ) // This will need to eventually check the user against a list of authorized admins and ask for a password.
				{
					runLoop = false;
					log( std::string("admin_shutdown called from ") + std::string( mPacket->systemAddress.ToString() ) );
				}
				else if( str_cmd.substr(0, 15) == "admin_teleport " )
				{
					Character *TempAdmin = getCharacter( mPacket->systemAddress );
					if( TempAdmin )
					{
						Character *TempChar = getCharacter( str_cmd.substr( 15, str_cmd.length() - 15 ).c_str() );
						if( TempChar )
						{
							TempAdmin->position_x = TempChar->position_x;
							TempAdmin->position_y = TempChar->position_y;
							TempAdmin->position_z = TempChar->position_z;
							send_ID_UPDATE_POSITION( Point( TempAdmin->position_x,
															TempAdmin->position_y,
															TempAdmin->position_z ),
													 mPacket->systemAddress
													);
													 
						}
					}
				}
				else if( str_cmd.substr(0, 11) == "admin_move " )
				{
					if( str_cmd.substr(11, 5) == "vert " )
					{
						Character *TempAdmin = getCharacter( mPacket->systemAddress );
						if( TempAdmin )
						{
							TempAdmin->position_y = TempAdmin->position_y + atof( str_cmd.substr(16, str_cmd.length()-16).c_str() );
							send_ID_UPDATE_POSITION( Point( TempAdmin->position_x,
															TempAdmin->position_y,
															TempAdmin->position_z ),
													 mPacket->systemAddress
													);
						}
					}
					else if( str_cmd.substr(11, 4) == "hor " )
					{
						Character *TempAdmin = getCharacter( mPacket->systemAddress );
						if( TempAdmin )
						{
							TempAdmin->position_y = TempAdmin->position_y + atof( str_cmd.substr(15, str_cmd.length()-15).c_str() );
							send_ID_UPDATE_POSITION( Point( TempAdmin->position_x,
															TempAdmin->position_y,
															TempAdmin->position_z ),
													 mPacket->systemAddress
													);
						}
					}
				}
				else if( str_cmd.substr(0, 13) == "admin_noclip " ) // Not fully functional yet.
				{
					Character *TempAdmin = getCharacter( mPacket->systemAddress );
					if( TempAdmin )
					{
						if( str_cmd.substr(13, 1) == "1" )
							TempAdmin->noclip = true;
						else if( str_cmd.substr(13,1) == "0" )
							TempAdmin->noclip = false;
					}
				}
			}
			break;
		case ID_REQUEST_NPC_INTERACTION:
			{
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				unsigned char id;
				incoming.Read( id );
				char _name[128];
				stringCompressor->DecodeString( _name, 128, &incoming );
				string name( _name );

				// First find the character that belongs to the system that sent the request.
				Character *character = getCharacter( mPacket->systemAddress );
				if( character == 0 )
				{
					log( std::string("NPC Interaction error involing systemAddress ") + std::string(mPacket->systemAddress.ToString()) );
					break;
				}

				// Here is where NPC name will be searched.
				for( int indx = 0; indx < npc_list.size(); indx++ )
				{
					if( npc_list[indx]->name == name )
					{
						// Found the NPC.  Now find the distance from the player to see if an interaction is possible.
						if( character->position_x < (npc_list[indx]->position_x + 50) &&
							character->position_x > (npc_list[indx]->position_x - 50) &&
							character->position_y < (npc_list[indx]->position_y + 50) &&
							character->position_y > (npc_list[indx]->position_y - 50) &&
							character->position_z < (npc_list[indx]->position_z + 50) &&
							character->position_z > (npc_list[indx]->position_z - 50)
							)
						{
							// An interaction with the NPC is possible, so flag that on the character, then send the initial NPC data.
							character->npc_interaction_allowed.push_back( name );
							send_npc_interaction( name, npc_list[indx]->greeting_text, mPacket->systemAddress );
						}
						else
						{
							/*
							 * The client sent a request but is out of range, which means either the data is not synchronized correctly,
							 * the player got horribly unlucky with a small precision error, or the client is hacked.  We'll assume
							 * that the data is not synchronized correctly and send the client a full snapshot.
							 */
							send_full_snapshot( character );
						}
					}
				}
			}
			break;
		case ID_TRADE_REQUEST:
			{
				unsigned char id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id ); // Packet id (ID_TRADE_REQUEST)
				char _name[128];
				stringCompressor->DecodeString( _name, 128, &incoming ); // Name of character to trade with
				string name( _name );

				Character *from = getCharacter( mPacket->systemAddress );
				Character *to = getCharacter( name );

				for( int i = 0; i < trade.size(); i++ )
				{
					if( from == trade[i].from ||
						to == trade[i].from ||
						from == trade[i].to ||
						to == trade[i].to )
					{
						// Stop the trade since one of the players is already in the process of trading.
						break;
					}
				}

				send_trade_open( add_trade_to_queue( from, to ) );
			}
			break;
			case ID_TRADE_ACCEPT:
			{
				unsigned char id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id ); // Packet id (ID_TRADE_ACCEPT)

				Trade *t = 0;
				Character *to = getCharacter( mPacket->systemAddress );

				int trade_num;

				if( to )
				{
					for( int i = 0; i < trade.size(); i++ )
					{
						if( to == trade[i].from )
						{
							trade[i].character_from_accept = true;
							t = &trade[i];
							trade_num = i;
						}
						else if( to == trade[i].to )
						{
							trade[i].character_to_accept = true;
							t = &trade[i];
							trade_num = i;
						}
					}

					if( t->character_from_accept && t->character_to_accept )
					{
						// Both characters have accepted; perform the trade.
						perform_trade( *t );
						//ADD HERE: inform players that trade is complete.
						trade.erase( trade.begin() + trade_num );
					}
				}
			}
			break;
//TODO: ADD MORE ERROR CHECKING IN THIS CASE
			case ID_TRADE_ADD:
			{
				unsigned char id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id ); // Packet id (ID_TRADE_ADD)
				int item_id;
				incoming.Read( item_id );

				Character *to = getCharacter( mPacket->systemAddress );

				for( int i = 0; i < trade.size(); i++ )
				{
					if( to == trade[i].from ||
						to == trade[i].to )
					{
						RakNet::BitStream outgoing;
						outgoing.Write( id );
						outgoing.Write( item_id );

						Item *item = getItem( item_id );

						if( item )
						{
							if( to == trade[i].from )
							{
								trade[i].character_from_items.push_back( item );
								mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, trade[i].to->systemAddress, false );
								break;
							}
							else
							{
								trade[i].character_to_items.push_back( item );
								mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, trade[i].from->systemAddress, false );
								break;
							}
						}
						else
						{
							log( "ID_TRADE_ADD: Invalid item id..." );
						}

						break;
					}
				}
			}
			break;
//TODO: ADD MORE ERROR CHECKING IN THIS CASE
			case ID_TRADE_REMOVE:
			{
				unsigned char id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id ); // Packet id (ID_TRADE_REMOVE)
				int item_id;
				incoming.Read( item_id );

				Character *to = getCharacter( mPacket->systemAddress );

				for( int i = 0; i < trade.size(); i++ )
				{
					if( to == trade[i].from ||
						to == trade[i].to )
					{
						RakNet::BitStream outgoing;
						outgoing.Write( id );
						outgoing.Write( item_id );

						Item *item = getItem( item_id );

						if( item )
						{
							if( to == trade[i].from )
							{
								for( int x = 0; x < trade[i].character_from_items.size(); x++ )
								{
									if( item == trade[i].character_from_items[x] )
									{
										// Destroy item.
										trade[i].character_from_items.erase(
											trade[i].character_from_items.begin() + x
											);
										mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, trade[i].to->systemAddress, false );
										break;
									}
								}
								
								break;
							}
							else
							{
								for( int x = 0; x < trade[i].character_to_items.size(); x++ )
								{
									if( item == trade[i].character_to_items[x] )
									{
										// Destroy item.
										trade[i].character_to_items.erase(
											trade[i].character_to_items.begin() + x
											);
									}
								}
								mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, trade[i].from->systemAddress, false );
								break;
							}
						}
						else
						{
							log( "ID_TRADE_REMOVE: Invalid item id..." );
						}

						break;
					}
				}
			}
			break;
			case ID_TRADE_OPEN: // Received from player means player has accepted to open a trade.
			{ // This case isn't being used at the moment.
				unsigned char id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id ); // Packet id (ID_TRADE_REQUEST)

				Character *to = getCharacter( mPacket->systemAddress );
				for( int i = 0; i < trade.size(); i++ )
				{
					if( trade[i].to == to )
					{
						send_trade_open( trade[i] );
					}
				}
			}
			break;
			case ID_TRADE_CANCEL:
			{
				unsigned char id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( id ); // Packet id (ID_TRADE_CANCEL)

				Character *to = getCharacter( mPacket->systemAddress );

				if( to )
				{
					for( int i = 0; i < trade.size(); i++ )
					{
						if( to == trade[i].from ||
							to == trade[i].to )
						{
							send_trade_cancel( trade[i] );
							trade.erase( trade.begin() + i );
							break;
						}
					}
				}
			}
			break;
			case ID_INVENTORY_FULL_SNAPSHOT:
				{
					unsigned char typeId;
					RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
					incoming.Read( typeId );
					Character *c = getCharacter( mPacket->systemAddress );
					if( c )
						send_ID_INVENTORY_FULL_SNAPSHOT( c );
					}
				break;
		default:
			unsigned char Id;
			RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
			incoming.Read( Id );
			log( "Unknown message received: " + toString(((int)Id)) );
			break;
		}

		mPeer->DeallocatePacket( mPacket );
	}
	return 0; // The meaning of life.
}

bool shutdown_server()
{
	/****************
	 * CLEANUP
	 ***************/
	delete dbi;

	RakNetworkFactory::DestroyRakPeerInterface( mPeer );

//	sqlite3_close(db);

	for( int indx = 0; indx < character_list.size(); indx++ )
	{
		delete character_list[indx]->log;
		delete character_list[indx];
	}
	character_list.clear();

	delete mLog;
	delete mTimer;

	return true;
}

} // End namespace.

#endif
