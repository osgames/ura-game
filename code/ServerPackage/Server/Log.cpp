/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Log.h"
#include <time.h>

namespace server
{

SLog::SLog( std::string _file, bool erase_old )
{
	file = _file;

	if( erase_old )
	{
		// Erase the old log.
		std::fstream writer( file.c_str(), std::ios::out );
		writer.close();
	}
}

SLog::~SLog()
{
}

void SLog::logMessage( std::string message )
{
	time_t tm = time(0);
	std::string time_string = std::string( ctime(&tm) );
	std::string final;
	final.append( "[" );
	final.append( time_string.substr(0,time_string.size()-1) ); // Remove the line break that ctime() adds.
	final.append( "] " );
	final.append( message );
	final.append("\n");
	
	std::fstream writer( file.c_str(), std::ios::out|std::ios::app );
	std::cout << final.c_str();
	writer << final.c_str();
	writer.close();
}

void SLog::logSilentMessage( std::string message )
{
	time_t tm = time(0);
	std::string time_string = std::string( ctime(&tm) );
	std::string final;
	final.append( "[" );
	final.append( time_string.substr(0,time_string.size()-1) ); // Remove the line break that ctime() adds.
	final.append( "] " );
	final.append( message );
	final.append("\n");
	
	std::fstream writer( file.c_str(), std::ios::out|std::ios::app );
	writer << final.c_str();
	writer.close();
}

} // End server namespace
