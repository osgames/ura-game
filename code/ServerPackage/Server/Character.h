/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHARACTER_H
#define CHARACTER_H

#include "../Dependencies/NetworkMessages.h"
#include <string.h>

namespace server
{

class SLog;
class Item;

class Character
{
public:
	std::string name;
	std::string login_username;

	float money;

	float orientation_x;
	float orientation_y;
	float orientation_z;
	float orientation_w;

	float position_x;
	float position_y;
	float position_z;

	float yaw;

	SystemAddress systemAddress;

	bool is_online;

	SLog *log;

	bool noclip;

	std::vector<std::string> npc_interaction_allowed; // All allowed interactions between NPCs will be listed here if available.

	std::vector<Item *> items;

	short current_movement;

	int id;
};
} // End server namespace

#endif