/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SERVER_NPC
#define SERVER_NPC

#include <vector>
#include <string>

class Quest;

namespace server
{
	/*
	 * Test NPC class which has enough to get the network code off the ground,
	 * then we can come back in and change what we need to change.
	 */
	class NPC
	{
	private:
	public:
		NPC();
		std::vector<std::string> inventory_list;
		float position_x;
		float position_y;
		float position_z;
		float orientation_w;
		float orientation_x;
		float orientation_y;
		float orientation_z;
		std::string model;
		std::string greeting_text;
		std::vector<Quest *> available_quests;
		std::string name;
		int money; // Stored as the lowest unit of currency in the game.
	};
}

#endif
