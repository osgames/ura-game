#include <iostream>
#include <vector>
#include <string>

#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "DBInterface.h"
#include "Account.h"

using namespace std;
using namespace cgicc;
using namespace dbinterface;

Cgicc cgi;

bool parseField( string fieldname, string &str )
{
  string tmp;
  form_iterator name = cgi.getElement( fieldname );
  if(name != cgi.getElements().end()) // Sucessfully read string from cgicc.
  {
    tmp = **name;
    for( int i = 0; i < tmp.length(); i++ )
    {
      if( (tmp[i] < 'A' || tmp[i] > 'Z') &&
	  (tmp[i] < 'a' || tmp[i] > 'z') )
      {
	cout << "Account creation unsuccessful; ";
	cout << fieldname;
	cout << " contains illegal characters.  Valid characters are a-z, A-Z" << endl;
	cout << body() << endl;
	return false;
      }
    }
  }
  else
  {
    // cgicc related error..
    cout << "Account creation failed; ";
    cout << fieldname;
    cout << " entry error." << endl;
    cout << body() << endl;
    return false;
  }
  str = tmp;
  return true;
}

bool parseEmailField( string fieldname, string &str )
{
  string tmp;
  form_iterator name = cgi.getElement( fieldname );
  if(name != cgi.getElements().end()) // Sucessfully read string from cgicc.
  {
    tmp = **name;
    for( int i = 0; i < tmp.length(); i++ )
    {
      if( (tmp[i] < 'A' || tmp[i] > 'Z') &&
	  (tmp[i] < 'a' || tmp[i] > 'z') &&
	  (tmp[i] < '0' || tmp[i] > '9') &&
	  tmp[i] != '@' &&
	  tmp[i] != '.' )
      {
	cout << "Account creation unsuccessful; ";
	cout << fieldname;
	cout << " contains illegal characters.  Valid characters are a-z, A-Z, @, ." << endl;
	cout << body() << endl;
	return false;
      }
    }
  }
  else
  {
    // cgicc related error..
    cout << "Account creation failed; ";
    cout << fieldname;
    cout << " entry error." << endl;
    cout << body() << endl;
    return false;
  }
  str = tmp;
  return true;
}

// Used by description.
bool parseFieldWithSpaces( string fieldname, string &str )
{
  string tmp;
  form_iterator name = cgi.getElement( fieldname );
  if(name != cgi.getElements().end()) // Sucessfully read string from cgicc.
  {
    tmp = **name;
    for( int i = 0; i < tmp.length(); i++ )
    {
      if( (tmp[i] < 'A' || tmp[i] > 'Z') &&
	  (tmp[i] < 'a' || tmp[i] > 'z') &&
	  (tmp[i] < '0' || tmp[i] > '9') &&
	  tmp[i] != ' ' && tmp[i] != '.' &&
	  tmp[i] != ',' && tmp[i] != ';' )
      {
	cout << "Account creation unsuccessful; ";
	cout << fieldname;
	cout << " contains illegal characters.  Valid characters are a-z, A-Z, 0-9" << endl;
	cout << body() << endl;
	return false;
      }
    }
  }
  else
  {
    // cgicc related error..
    cout << "Account creation failed; ";
    cout << fieldname;
    cout << " entry error." << endl;
    cout << body() << endl;
    return false;
  }
  str = tmp;
  return true;
}

bool parseNumericField( string fieldname, string &str )
{
  string tmp;
  form_iterator name = cgi.getElement( fieldname );
  if(name != cgi.getElements().end()) // Sucessfully read string from cgicc.
  {
    tmp = **name;
    for( int i = 0; i < tmp.length(); i++ )
    {
      if( (tmp[i] < '0' || tmp[i] > '9') )
      {
	cout << "Account creation unsuccessful; ";
	cout << fieldname;
	cout << " contains illegal characters.  Valid characters are 0-9" << endl;
	cout << body() << endl;
	return false;
      }
    }
  }
  else
  {
    // cgicc related error..
    cout << "Account creation failed; ";
    cout << fieldname;
    cout << " entry error." << endl;
    cout << body() << endl;
    return false;
  }
  str = tmp;
  return true;
}

bool parseNumericAndCharacterField( string fieldname, string &str )
{
  string tmp;
  form_iterator name = cgi.getElement( fieldname );
  if(name != cgi.getElements().end()) // Sucessfully read string from cgicc.
  {
    tmp = **name;
    for( int i = 0; i < tmp.length(); i++ )
    {
      if( (tmp[i] < '0' || tmp[i] > '9') &&
	  (tmp[i] < 'A' || tmp[i] > 'Z') &&
	  (tmp[i] < 'a' || tmp[i] > 'z') )
      {
	cout << "Account creation unsuccessful; ";
	cout << fieldname;
	cout << " contains illegal characters.  Valid characters are 0-9" << endl;
	cout << body() << endl;
	return false;
      }
    }
  }
  else
  {
    // cgicc related error..
    cout << "Account creation failed; ";
    cout << fieldname;
    cout << " entry error." << endl;
    cout << body() << endl;
    return false;
  }
  str = tmp;
  return true;
}

int main(int argc, char **argv)
{
  try
  {
    // Declare needed variables.
    string account_email, account_password, account_firstname, account_lastname, account_country;
    string character_firstname, character_lastname, character_gender, character_age, character_description;
    char c_character_gender;

    // Initialize external libraries.
    DBInterface dbi( "uradb", "localhost", "ura", "1337", 1337 );

    // Generate the initial HTTP information.
    cout << HTTPHTMLHeader() << endl;
    cout << html() << head( title( "Project Ura Account Creation" ) ) << endl;
    cout << body() << endl;

    // Account creation.

    // Get e-mail address.
    if( !parseEmailField( "email", account_email ) )
    {
      return 0;
    }

    // Get password.
    if( !parseNumericAndCharacterField( "password", account_password ) )
    {
      return 0;
    }

    // Get first name.
    if( !parseField( "firstname", account_firstname ) )
    {
      return 0;
    }

    // Get last name.
    if( !parseField( "lastname", account_lastname ) )
    {
      return 0;
    }

    // Get country.
    if( !parseField( "country", account_country ) )
    {
      return 0;
    }



    // Character creation; temporary

    // Get first name.
    if( !parseField( "character_firstname", character_firstname ) )
    {
      return 0;
    }

    // Get last name.
    if( !parseField( "character_lastname", character_lastname ) )
    {
      return 0;
    }

    // Get gender.
    if( !parseField( "character_gender", character_gender ) )
    {
      return 0;
    }
    if( character_gender == "m" || character_gender == "f" )
    {
      c_character_gender = character_gender[0];
    }
    else
    {
      cout << "Invalid character input.  I'm pretty sure you did this on purpose.  Shame on you." << br() << endl;
      return 0;
    }

    // Get age.
    if( !parseNumericField( "character_age", character_age ) )
    {
      return 0;
    }

    // Get description.
    if( !parseFieldWithSpaces( "character_description", character_description ) )
    {
      return 0;
    }



    // Made it this far; try to create the account in the database.
    Account *acct = dbi.createAccount( account_email, account_firstname, account_lastname, account_country, "1" );
    if( acct )
    {
      if( acct->setPassword( account_password ) )
      {
	 cout << "Account creation successful.  You may now log in." << br() << endl;
      }
      else
      {
	cout << "Account creation unsuccessful; password assign failure." << endl;
	cout << body() << endl;
	return 0;
      }

      if( acct->createCharacter( character_firstname, character_lastname, c_character_gender, character_age, character_description ) )
      {
	cout << "Character successfully created." << endl;
      }
      else
      {
	cout << "Error during character creation (character name possibly already in use?)." << endl;
      }
      cout << body() << endl;
    }
    else
    {
      cout << "Account creation unsuccessful.  DB interfacing failed.  Please contact an administrator." << br() << endl;
      cout << "email: " << account_email << br() << "first name: " << account_firstname << br() << endl;
      cout << "last name: " << account_lastname << br() << "country: " << account_country << br() << endl;
      cout << body() << endl;
    }
  }
  catch(exception& e)
  {
    // handle any errors - omitted for brevity
  }
}
