/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <fstream>

namespace server
{

class SLog
{
private:
	std::fstream *writer;
	std::string file;
public:
	SLog( std::string file, bool erase_old = true );
	~SLog();

	/* Log message and display it in console. */
	void logMessage( std::string message );

	/* Log message without displaying it in console. */
	void logSilentMessage( std::string message );
};

} // End server namespace

#endif
