#include "DBConnector.h"
#include <mysql++.h>
#include "Globals.h"

using namespace dbinterface;

DBConnector::DBConnector( std::string db_name, std::string addr, std::string username, std::string password, unsigned int port ) : mConnection( 0 )
{
	mConnection = new mysqlpp::Connection( false );
	if( !mConnection->connect( db_name.c_str(), addr.c_str(), username.c_str(), password.c_str(), port ) )
		logMessage( std::string("DB connection failed: ") + std::string(mConnection->error()) );
}

DBConnector::~DBConnector()
{
	if( mConnection )
	{
		delete mConnection;
		mConnection = 0;
	}
}

mysqlpp::Query DBConnector::query( std::string qry )
{
	return mConnection->query( qry );
}
