#include "DBInterface.h"
#include "Account.h"
#include "NPC.h"
#include "DBConnector.h"
#include "Globals.h"
#include "Utilities.h"
#include "Administration.h"
#include "DBCharacter.h"
#include <vector>
#include <algorithm>
#include <mysql++.h>

using namespace dbinterface;


//###################//
//### TABLE NAMES ###//
//###################//
// "accounts"
// "characters"



DBInterface::DBInterface( std::string db_name, std::string addr, std::string username, std::string password, unsigned int port )
: mConnector( 0 ), mUtilities( 0 ), mAdministration( 0 )
{
	mConnector = new DBConnector( db_name, addr, username, password, port );
	mUtilities = new Utilities( this );
	mAdministration = new Administration( this );

	//######################################################//
	//### We create the tables that we need here for now ###//
	//######################################################//
/********************	  
	///////////////////////////////////////  
	/// First Accounts table - 8 values ///
	///////////////////////////////////////
	//CREATE TABLE accounts (account_id CHAR(20), email CHAR(20), firstname CHAR(20), lastname CHAR(20), country CHAR(20), lastip CHAR(20), lastlogin CHAR(20), security CHAR(20)); 

	std::string q = "CREATE TABLE accounts (";
		q += "account_id CHAR(20), ";
		q += "email CHAR(20), ";
		q += "firstname CHAR(20), ";
		q += "lastname CHAR(20), ";
		q += "country CHAR(20), ";
		q += "lastip CHAR(20), ";
		q += "lastlogin CHAR(20), ";
		q += "security CHAR(20));";

	// Send the query //
	mysqlpp::Query qry = getConnector()->query(q);

	// Check for and log any errors // 
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		std::cout << "DBInterface: Query ERROR: " << qry.error() << std::endl;
	}

	
	  

	//////////////////////////////////////////  
	/// Second table characters -13 values ///
	//////////////////////////////////////////
	// CREATE TABLE characters (character_id CHAR(20), accounts_account_id CHAR(20), sectors_sector_id CHAR(20), instances_instance_id CHAR(20), cFirstname CHAR(20), cLastname CHAR(20), cDescription CHAR(20), cGender CHAR(20), cAge CHAR(20), cXpos CHAR(20), cYpos CHAR(20), cZpos CHAR(20), isNPC CHAR(20));

	q = "CREATE TABLE characters (";
		q += "character_id CHAR(20), ";
		q += "accounts_account_id CHAR(20), ";
		q += "sectors_sector_id CHAR(20), ";
		q += "instances_instance_id CHAR(20), ";
		q += "cFirstname CHAR(20), ";
		q += "cLastname CHAR(20), ";
		q += "cDescription CHAR(20), ";
		q += "cGender CHAR(20), ";
		q += "cAge CHAR(20), ";
		q += "cXpos CHAR(20), ";
		q += "cYpos CHAR(20), ";
		q += "cZpos CHAR(20), ";
		q += "isNPC CHAR(20));";
		 

		 
	// Send the query //
	qry = getConnector()->query(q);

	// Check for and log any errors // 
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		std::cout << "DBInterface: Query ERROR: " << qry.error() << std::endl;
	}
******************/

}

DBInterface::~DBInterface()
{
	for( std::map<std::string, Account *>::iterator MapItor = list_Account.begin(); MapItor != list_Account.end(); MapItor++ )
	{
		Account *Value = (*MapItor).second;
		delete Value;
	}

	for( std::map<std::string, NPC *>::iterator MapItor = list_NPC.begin(); MapItor != list_NPC.end(); MapItor++ )
	{
		NPC *Value = (*MapItor).second;
		delete Value;
	}

	if( mAdministration )
	{
		delete mAdministration;
		mAdministration = 0;
	}

	if( mUtilities )
	{
		delete mUtilities;
		mUtilities = 0;
	}

	if( mConnector )
	{
		delete mConnector;
		mConnector = 0;
	}
}

 

Account *DBInterface::getAccount( std::string email, bool log )
{
std::cout << "############## = " << std::endl;

	 
	if( list_Account.find( email ) == list_Account.end() )
	{
		// Try to pull it from the database.
		// Query database for account.
		mysqlpp::Query qry = getConnector()->query( "SELECT * FROM accounts WHERE email='" + email + "';" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each e-mail address should be unique.  Log it and continue as usual.
				if( log )
				  logMessage( "DB error: e-mail address issue found in DBInterface::getAccount()" );
			}
			else if( res.num_rows() <= 0 )
			{
				if( log )
				  logMessage( "No results for query in DBInterface::getAccount()" );
				return 0;
			}
std::cout << "1 ###" << std::endl;

			std::string id;
			std::string tmpEmail;
			std::string firstname;
			std::string lastname;
			std::string country;
			std::string lastip;
			std::string lastlogin;
			std::string security;


			res[0]["account_id"].to_string(id);
std::cout << "2 ###" << std::endl;
			res[0]["email"].to_string(tmpEmail);
			res[0]["firstname"].to_string(firstname);
			res[0]["lastname"].to_string(lastname);
			res[0]["country"].to_string(country);
			res[0]["lastip"].to_string(lastip);
			res[0]["lastlogin"].to_string(lastlogin);
			res[0]["security"].to_string(security);
std::cout << "3 ###" << std::endl;
			Account *account = new Account(
				this,
				id,
				tmpEmail,
				firstname,
				lastname,
				country,
				lastip,
				lastlogin,
				security
				);
std::cout << "4 ###" << std::endl;
			list_Account[email] = account;
std::cout << "5 ###" << std::endl;
			return account;
		}
		else
		{
std::cout << "6 ###" << std::endl;
			if( log )
			  logMessage( "DB connector error: failed to get item list in DBInterface::getAccount()" );
			return 0;
		}
	}
	else
	{
std::cout << "7 ###" << std::endl;
		return list_Account[email];
	}
	return 0;
}

bool DBInterface::getAllAccounts( std::vector<Account *> &accounts )
{
std::cout << "1 getAllAccounts ###" << std::endl;
	mysqlpp::Query qry = getConnector()->query(
			"SELECT email FROM accounts;" );
std::cout << "2 getAllAccounts ###" << std::endl;
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
std::cout << "3 getAllAccounts ###" << std::endl;
			if( res.num_rows() >= 1 )
			{
				std::vector<Account *> vec;
				for( int i = 0; i < res.size(); i++ )
				{
					std::string email;
					res[i]["email"].to_string( email );
					Account *acct = getAccount( email );
					vec.push_back( acct );
				}
				accounts = vec;
			}
			else if( res.num_rows() <= 0 )
			{
				return false;
			}

			return true;
		}
		else
		{
std::cout << "4 getAllAccounts ###" << std::endl;
			return false;
		}

	return true;
}


Account *DBInterface::createAccount( std::string email, std::string firstname, std::string lastname,
		std::string country, std::string security )
{
	if( getAccount( email, false ) )
	{
		return 0;
	}
	Account *temp = new Account( this, email, firstname, lastname, country, security );
	list_Account[email] = temp;
	return temp;
}

NPC *DBInterface::getNPC( std::string firstname, std::string lastname )
{
	if( list_NPC.find( firstname + ' ' + lastname ) == list_NPC.end() )
	{
		// Try to pull it from the database.
		// Query database for npc.
		mysqlpp::Query qry = getConnector()->query(
			"SELECT character_id FROM characters WHERE cFirstname='" + firstname +
			"' AND cLastname='" + lastname + "';" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB error: name issue found in DBInterface::getNPC()" );
			}
			else if( res.num_rows() <= 0 )
			{
				logMessage( "No results for query in DBInterface::getNPC()" );
				return 0;
			}

			std::string id;
			res[0]["character_id"].to_string( id );
			NPC *npc = new NPC( this, id );

			list_NPC[firstname + ' ' + lastname] = npc;

			return npc;
		}
		else
		{
			logMessage( "DB connector error: failed to get item list in DBInterface::getNPC()" );
			return 0;
		}
	}
	else
	{
		return list_NPC[firstname + ' ' + lastname];
	}
	return 0;
}

DBConnector *DBInterface::getConnector()
{
	return mConnector;
}

Utilities *DBInterface::getUtilities()
{
	return mUtilities;
}

Administration *DBInterface::getAdministration()
{
	return mAdministration;
}

DBCharacter *DBInterface::getCharacter( std::string firstname, std::string lastname, bool log )
{
	if( list_Character.find( firstname + ' ' + lastname ) == list_Character.end() )
	{
		// Try to pull it from the database.
		// Query database for character.
		mysqlpp::Query qry = getConnector()->query(
			"SELECT character_id FROM characters WHERE cFirstname='" + firstname +
			"' AND cLastname='" + lastname + "';" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB error: name issue found in DBInterface::getCharacter()" );
			}
			else if( res.num_rows() <= 0 )
			{
				if( log )
				      logMessage( "No results for query in DBInterface::getCharacter()" );
				return 0;
			}

			std::string id;
			res[0]["character_id"].to_string( id );
			DBCharacter *character = new DBCharacter( this, id );

			list_Character[firstname + ' ' + lastname] = character;

			return character;
		}
		else
		{
			logMessage( "DB connector error: failed to get item list in DBInterface::getCharacter()" );
			return 0;
		}
	}
	else
	{
		return list_Character[firstname + ' ' + lastname];
	}
	return 0;
}

bool DBInterface::notifyOffline( DBCharacter *character )
{
	if( list_Character.find( character->getFirstName() + ' ' + character->getLastName() ) != list_Character.end() )
	{
		delete list_Character[character->getFirstName() + ' ' + character->getLastName()];
		list_Character.erase( character->getFirstName() + ' ' + character->getLastName() );
		return true;
	}
	return false;
}

bool DBInterface::notifyOffline( Account *account )
{
	if( list_Account.find( account->getFirstName() + ' ' + account->getLastName() ) != list_Account.end() )
	{
		delete list_Account[account->getFirstName() + ' ' + account->getLastName()];
		list_Account.erase( account->getFirstName() + ' ' + account->getLastName() );
		return true;
	}
	return false;
}

bool DBInterface::notifyOffline( NPC *npc )
{
	if( list_NPC.find( npc->getFirstName() + ' ' + npc->getLastName() ) != list_NPC.end() )
	{
		delete list_NPC[npc->getFirstName() + ' ' + npc->getLastName()];
		list_NPC.erase( npc->getFirstName() + ' ' + npc->getLastName() );
		return true;
	}
	return false;
}
