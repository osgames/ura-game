#ifndef VECTOR3_H
#define VECTOR3_H

#include <string>

namespace dbinterface
{

class DBInterface;

class Vector3
{
private:
	float x;
	float y;
	float z;
	friend class DBCharacter;
	friend class NPC;
	Vector3();
	std::string sCharacterID;
	DBInterface *mInterface;
public:
	Vector3( std::string id, DBInterface *dbi );
	bool setX( float val );
	bool setY( float val );
	bool setZ( float val );
};

}

#endif
