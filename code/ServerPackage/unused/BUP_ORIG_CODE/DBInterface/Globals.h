#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include <iostream>

namespace dbinterface
{

static void logMessage( std::string msg )
{
	std::cout << msg << "<br>" << std::endl;
}

}

#endif
