#include "NPC.h"

#include "DBInterface.h"
#include "DBConnector.h"
#include "Utilities.h"
#include "Globals.h"
#include <mysql++.h>

using namespace dbinterface;

NPC::NPC( DBInterface *dbi, std::string character_id )
{
	sCharacterID = character_id;
	mInterface = dbi;

	// Populate the NPC from the DB.
	mysqlpp::Query qry = mInterface->getConnector()->query( 
			"SELECT * FROM characters WHERE character_id=" + character_id + ";" );
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and continue as usual.
			logMessage( "DB error: multiple id issue found in NPC::NPC()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "FATAL ERROR: No results for query in NPC::NPC()" );
			return;
		}

		fYaw = 0.0f;

		std::string tmp;
		Vector3 *vtmp = new Vector3( character_id, dbi );
		res[0]["cFirstName"].to_string( sFirstName );
		res[0]["cLastName"].to_string( sLastName );
		res[0]["cGender"].to_string( tmp );
		cGender = tmp[0];
		res[0]["cAge"].to_string( tmp );
		iAge = mInterface->getUtilities()->toInt( tmp );
		res[0]["cXpos"].to_string( tmp );
		vtmp->x = mInterface->getUtilities()->toFloat( tmp );
		res[0]["cYpos"].to_string( tmp );
		vtmp->y = mInterface->getUtilities()->toFloat( tmp );
		res[0]["cZpos"].to_string( tmp );
		vtmp->z = mInterface->getUtilities()->toFloat( tmp );
		mPosition = vtmp;
	}
	else
	{
		logMessage( "DB connector error: failed to get item list in NPC::NPC()" );
		return;
	}
}

NPC::~NPC()
{
	if( mPosition )
	{
		delete mPosition;
		mPosition = 0;
	}
}
