#include "Administration.h"
#include "Attribute.h"
#include "Skill.h"
#include "Stat.h"
#include "DBInterface.h"
#include "DBException.h"
#include "DBConnector.h"
#include "Globals.h"
#include <mysql++.h>

using namespace dbinterface;

Administration::Administration( DBInterface *dbi )
{
	mInterface = dbi;
}

Stat *Administration::insertStat( std::string name, std::string description )
{
	std::map<std::string, Stat *>::iterator it = list_Stat.find( name );
	if( it == list_Stat.end() ) // Didn't find an existing item with the same name.
	{
		if( mInterface == 0 )
		  throw DBException( (char*) "Actor::createStat(): mInterface is NULL." );
		mysqlpp::Query qry = mInterface->getConnector()->query(
			"INSERT INTO stats VALUES (null, '" + name + "', '" + description + "');" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Actor::createStat()" );
			return 0; // Found given stat in the map, but failed to delete it from the DB.
		}
		Stat *stat = new Stat();
		stat->setName( name );
		stat->setDescription( description );
		list_Stat[name] = stat;
		return stat; // Success.
	}
	logMessage( "Actor::createStat() failed: existing item found." );
	return 0; // Found an existing item.  Just return 0.
}

Skill *Administration::insertSkill()
{
	return 0;
}

Attribute *Administration::insertAttribute()
{
	return 0;
}

bool Administration::removeStat( Stat *stat )
{
	std::map<std::string, Stat *>::iterator it = list_Stat.find( stat->getName() );
	if( it != list_Stat.end() ) // Found an existing item.
	{
		delete it->second;
		list_Stat.erase( it );

		if( mInterface == 0 )
		  throw DBException( (char*) "Actor::removeStat(): mInterface is NULL." );
		mysqlpp::Query qry = mInterface->getConnector()->query( "DELETE FROM stats WHERE statName='" + stat->getName() + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Actor::removeStat()" );
			return false; // Found given stat in the map, but failed to delete it from the DB.
		}
		return true; // Success.
	}
	return false; // Failed to find given stat in the map.
}

bool Administration::removeSkill( Skill *skill )
{
	std::map<std::string, Skill *>::iterator it = list_Skill.find( "" );
	delete it->second;
	list_Skill.erase( it );
	return false;
}

bool Administration::removeAttribute( Attribute *attribute )
{
	std::map<std::string, Attribute *>::iterator it = list_Attribute.find( "" );
	delete it->second;
	list_Attribute.erase( it );
	return false;
}

std::map<std::string, Stat *> Administration::getStatMap()
{
	return list_Stat;
}
