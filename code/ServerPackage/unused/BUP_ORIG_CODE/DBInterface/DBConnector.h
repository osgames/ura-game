#ifndef DBCONNECTOR_H
#define DBCONNECTOR_H

#include <string>

namespace mysqlpp
{
	class Connection;
	class Query;
}

namespace dbinterface
{

class DBConnector
{
private:
	mysqlpp::Connection *mConnection;
public:
	DBConnector( std::string db_name, std::string addr, std::string username, std::string password, unsigned int port );
	~DBConnector();

	/*!
	 * Performs a query via mysql++
	 * @return a mysqlpp::Query.  mysqlpp must be included in order to work with this type.
	 */
	mysqlpp::Query query( std::string qry );
};

}

#endif
