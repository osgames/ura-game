#ifndef UTILITIES_H
#define UTILITIES_H

#include <map>
#include <string>

namespace dbinterface
{

class DBInterface;

/*!
 * Contains commonly used utility methods that do
 * not require to be part of a specific instance.
 */
class Utilities
{
private:
	// All IDs are maintained in memory since there
	// shouldn't be very many and they should be commonly used.
	std::map<std::string, std::string> mapSkillID;
	std::map<std::string, std::string> mapAttributeID;
	std::map<std::string, std::string> mapStatID;
	DBInterface *mInterface;
public:
	Utilities( DBInterface *dbi );

	/*!
	 * Has the overhead of a SELECT/WHERE query.
	 * @return the skill_id based on the specified name, otherwise returns an empty string.
	 */
	std::string getSkillID( std::string name );

	/*!
	 * Has the overhead of a SELECT/WHERE query.
	 * @return the attribute_id based on the specified name, otherwise returns an empty string.
	 */
	std::string getAttributeID( std::string name );

	/*!
	 * Has the overhead of a SELECT/WHERE query.
	 * @return the stat_id based on the specified name, otherwise returns an empty string.
	 */
	std::string getStatID( std::string name );

	/*!
	 * Converts from a string to int.
	 */
	int toInt( std::string s );

	/*!
	 * Converts from a string to float.
	 */
	float toFloat( std::string s );

	/*!
	 * Converts from an int to a string.
	 */
	std::string toString( int val );

	/*!
	 * Converts from a float to a string.
	 */
	std::string toString( float val );
};

}

#endif
