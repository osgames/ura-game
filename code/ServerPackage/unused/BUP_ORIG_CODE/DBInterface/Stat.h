#ifndef STAT_H
#define STAT_H

#include <string>

namespace dbinterface
{

class Stat
{
private:
	std::string sName;
	std::string sDescription;
public:
	std::string getName();
	void setName( std::string name );
	std::string getDescription();
	void setDescription( std::string description );
};

}

#endif
