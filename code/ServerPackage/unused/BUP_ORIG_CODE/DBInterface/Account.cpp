#include "Account.h"
#include "DBInterface.h"
#include "DBConnector.h"
#include "Globals.h"
#include "DBCharacter.h"
#include <mysql++.h>

using namespace dbinterface;
 
Account::Account( DBInterface *dbi, std::string email, std::string firstname, std::string lastname,
				 std::string country, std::string security ) :
mChat( 0 ), mInterface( 0 ), sEmail( email ), sFirstName( firstname ),
sLastName( lastname ), sCountry( country ), sSecurity( security )
{
	// Used for building an Account instance for loading into the database.
	mInterface = dbi;
	sTableName = "account";
	
	/********************
	mysqlpp::Query qry = mInterface->getConnector()->query(
		std::string("INSERT INTO accounts") +
		std::string(" VALUES (") +
		std::string("null, ") +
		std::string("'") + email + std::string("', ") +
		std::string("'") + firstname + std::string("', ") +
		std::string("'") + lastname + std::string("', ") +
		std::string("'") + country + std::string("', ") +
		std::string("'', ") +
		std::string("null, ") +
		security +
		std::string(");") );
	************************/

	
	mysqlpp::Query qry = mInterface->getConnector()->query(
		std::string("INSERT INTO accounts") +
		std::string(" VALUES (") +
		std::string("null, ") +
		std::string("'") + email + std::string("', ") +
		std::string("'") + firstname + std::string("', ") +
		std::string("'") + lastname + std::string("', ") +
		std::string("null, null, null, null);"));
	

	if( !qry.execute() )
	{
		logMessage( "Query execute failed in Account::Account() - See line below for more info." );
		logMessage( qry.error() );
		return;
	}

	mysqlpp::Query nqry = mInterface->getConnector()->query(
		"SELECT account_id FROM accounts WHERE email='" +
		email + "';"
		);
	if( mysqlpp::StoreQueryResult res = nqry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and continue as usual.
			logMessage( "DB error: multiple of same e-mail address found in Account::Account()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "DB error: no account with specified e-mail found in Account::Account() - This means query execute probably failed." );
			return;
		}

		res[0]["account_id"].to_string( sID );
	}
}

Account::Account( DBInterface *dbi, std::string id, std::string email, std::string firstname, std::string lastname,
		std::string country, std::string lastip, std::string lastlogin, std::string security ) :
mChat( 0 ), mInterface( 0 ), sEmail( email ), sFirstName( firstname ), sID( id ),
sLastName( lastname ), sCountry( country ), sLastIP( lastip ), sLastLogin( lastlogin ), sSecurity( security )
{
	// Only used for building an Account instance from the database.
	mInterface = dbi;
	sTableName = "account";
}

Account::~Account()
{
	mInterface = 0;
}

bool Account::getAllCharacters( std::vector<DBCharacter *> &val )
{
	mysqlpp::Query qry = mInterface->getConnector()->query(
			"SELECT * FROM characters WHERE accounts_account_id=" + getID() + ";" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() >= 1 )
			{
				std::vector<DBCharacter *> vec;
				for( int i = 0; i < res.size(); i++ )
				{
					std::string id;
					res[i]["character_id"].to_string( id );
					DBCharacter *character = new DBCharacter( mInterface, id );
					vec.push_back( character );
				}
				val = vec;
			}
			else if( res.num_rows() <= 0 )
			{
				return false;
			}

			return true;
		}
		else
		{
			logMessage( "DB connector error: failed to get item list in Account::getAllCharacters()" );
			return false;
		}

	return true;
}

DBCharacter *Account::createCharacter(
		std::string first_name,
		std::string last_name,
		char gender,
		std::string age,
		std::string description
		)
{
	if( mInterface->list_Character.find( first_name + " " + last_name ) != mInterface->list_Character.end() ||
		mInterface->getCharacter( first_name, last_name, false ) )
	{
		// The name already exists in one of the character lists.
		logMessage( "createCharacter failed; name already exists." );
		return 0;
	}

	DBCharacter *temp = 0;

	std::string gend;
	if( gender == 'm' )
	{
		gend = "0";
	}
	else if( gender == 'f' )
	{
		gend = "1";
	}
	else
	{
		return 0;
	}

	mysqlpp::Query qry = mInterface->getConnector()->query(
		std::string("INSERT INTO characters VALUES (") +
		"null, " + // character_id
		getID() + ", " + // accounts_account_id
		"null, " + // sectors_sector_id
		"null, " + // instances_instance_id
		"'" + first_name + "', " + // cFirstname
		"'" + last_name + "', " + // cLastname
		"'" + description + "', " + // cDescription
		gend + ", " + // cGender
		age + ", " + // cAge
		"0.0, " + // cXpos
		"0.0, " + // cYpos
		"0.0, " + // cZpos
		"false" + // isNPC
		");"
		);

	if( !qry.execute() )
	{
		logMessage( "Query execute failed in Account::createCharacter (SQL INSERT) (see line below for more details)" );
		logMessage( qry.error() );
		return 0;
	}

	temp = new DBCharacter(
				mInterface,
				this,
				first_name,
				last_name,
				gender,
				age,
				description
				);

	mInterface->list_Character[first_name + " " + last_name] = temp;

	return temp;
}

void Account::setEmail( std::string str )
{
	if( sEmail != str )
	{
		// Find the row by the old e-mail address and change it to the new one.
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET email='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setEmail()" );
		}
		sEmail = str;
	}
}

void Account::setFirstName( std::string str )
{
	if( sFirstName != str )
	{
		sFirstName = str;
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET firstname='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setFirstName()" );
		}
	}
}

void Account::setLastName( std::string str )
{
	if( sLastName != str )
	{
		sLastName = str;
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET lastname='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setLastName()" );
		}
	}
}

void Account::setCountry( std::string str )
{
	if( sCountry != str )
	{
		sCountry = str;
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET country='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setCountry()" );
		}
	}
}

void Account::setLastIP( std::string str )
{
	if( sLastIP != str )
	{
		sLastIP = str;
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET lastip='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setLastIP()" );
		}
	}
}

void Account::setLastLogin( std::string str )
{
	if( sLastLogin != str )
	{
		sLastLogin = str;
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET lastlogin='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setLastLogin()" );
		}
	}
}

void Account::setSecurity( std::string str )
{
	if( sSecurity != str )
	{
		sSecurity = str;
		mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE " + sTableName + " SET security='" + str + "' WHERE email='" + sEmail + "';" );
		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Account::setSecurity()" );
		}
	}
}

bool Account::setPassword( std::string str )
{
	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT accounts_account_id FROM passwords WHERE accounts_account_id='" +
		getID() + "';"
		);
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and kill.
			logMessage( "DB error: multiple of same password found in Account::setPassword()" );
			return false;
		}
		else if( res.num_rows() == 1 )
		{
			// Update the existing row in the passwords table.
			mysqlpp::Query qry = mInterface->getConnector()->query( "UPDATE passwords SET sPassword='" + str + "' WHERE accounts_account_id=" + getID() + ";" );
			if( !qry.execute() )
			{
				logMessage( "Query execute failed in Account::setPassword() (res.num_rows()==1)" );
				return false;
			}
			return true;
		}
		else if( res.num_rows() == 0 )
		{
			// Add a new row to the passwords table.
			mysqlpp::Query qry = mInterface->getConnector()->query( std::string("INSERT INTO passwords VALUES(") +
										std::string("NULL, ") +
										getID() + ", " +
										"'" + str + "');" );
			if( !qry.execute() )
			{
				logMessage( "Query execute failed in Account::setPassword() (res.num_rows()==0)" );
				return false;
			}
			return true;
		}
	}
	return false;
}

std::string Account::getEmail()
{
	return sEmail;
}

std::string Account::getFirstName()
{
	return sFirstName;
}

std::string Account::getLastName()
{
	return sLastName;
}

std::string Account::getCountry()
{
	return sCountry;
}

std::string Account::getLastIP()
{
	return sLastIP;
}

std::string Account::getLastLogin()
{
	return sLastLogin;
}

std::string Account::getSecurity()
{
	return sSecurity;
}

std::string Account::getID()
{
	return sID;
}

std::string Account::getPassword()
{
	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT sPassword FROM passwords WHERE accounts_account_id='" +
		getID() + "';"
		);
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() == 1 )
		{
			// Successful.  Get the password and return it.
			std::string tmp;
			res[0]["sPassword"].to_string( tmp );
			return tmp;
		}
		else if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and continue as usual.
			logMessage( "DB error: multiple of same password found in Account::setPassword()" );
		}
		else if( res.num_rows() == 0 )
		{
			logMessage( "DB error: no password found for specified account." );
		}
	}
	return NULL;
}
