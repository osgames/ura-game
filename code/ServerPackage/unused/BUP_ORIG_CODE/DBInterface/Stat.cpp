#include "Stat.h"

using namespace dbinterface;

std::string Stat::getName()
{
	return sName;
}

void Stat::setName( std::string name )
{
	sName = name;
}

std::string Stat::getDescription()
{
	return sDescription;
}

void Stat::setDescription( std::string description )
{
	sDescription = description;
}
