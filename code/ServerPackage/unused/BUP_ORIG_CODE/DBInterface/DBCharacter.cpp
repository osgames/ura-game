#include "DBCharacter.h"
#include "DBConnector.h"
#include "DBInterface.h"
#include "Utilities.h"
#include "Globals.h"
#include "Vector3.h"
#include "Account.h"
#include <mysql++.h>

using namespace dbinterface;

DBCharacter::DBCharacter( DBInterface *dbi, std::string id )
{
	mInterface = dbi;
	sCharacterID = id;

	// Populate the Character from the DB.
	mysqlpp::Query qry = mInterface->getConnector()->query(
			"SELECT * FROM characters WHERE character_id=" + id + ";" );
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and continue as usual.
			logMessage( "DB error: multiple id issue found in DBCharacter::DBCharacter()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "FATAL ERROR: No results for query in DBCharacter::DBCharacter()" );
			return;
		}

		fYaw = 0.0f;

		std::string tmp;
		Vector3 *vtmp = new Vector3( id, dbi );
		res[0]["cFirstName"].to_string( sFirstName );
		res[0]["cLastName"].to_string( sLastName );
		res[0]["cGender"].to_string( tmp );
		cGender = tmp[0];
		res[0]["cAge"].to_string( tmp );
		iAge = mInterface->getUtilities()->toInt( tmp );
		res[0]["cXpos"].to_string( tmp );
		vtmp->x = mInterface->getUtilities()->toFloat( tmp );
		res[0]["cYpos"].to_string( tmp );
		vtmp->y = mInterface->getUtilities()->toFloat( tmp );
		res[0]["cZpos"].to_string( tmp );
		vtmp->z = mInterface->getUtilities()->toFloat( tmp );
		mPosition = vtmp;
	}
	else
	{
		logMessage( "DB connector error: failed to get item list in DBCharacter::DBCharacter()" );
		return;
	}
}

DBCharacter::DBCharacter(
		DBInterface *dbi,
		Account *account,
		std::string firstname,
		std::string lastname,
		char gender,
		std::string age,
		std::string description
		)
{
	mInterface = dbi;
	mAccount = account;
	sFirstName = firstname;
	sLastName = lastname;
	cGender = gender;
	iAge = mInterface->getUtilities()->toInt( age );

	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT character_id FROM characters WHERE cFirstname='" +
		firstname + "' AND cLastname='" +
		lastname + "';"
		);
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and continue as usual.
			logMessage( "DB error: multiple of same first/last name found in DBCharacter::DBCharacter()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "DB error: no account with specified first/last name found in DBCharacter::DBCharacter() - This means query execute probably failed." );
			return;
		}

		res[0]["character_id"].to_string( sCharacterID );

		mPosition = new Vector3( sCharacterID, mInterface );
	}
}

DBCharacter::~DBCharacter()
{
	if( mPosition )
	{
		delete mPosition;
		mPosition = 0;
	}
}
