#ifndef DBEXCEPTION_H
#define DBEXCEPTION_H

#include <exception>

namespace dbinterface
{

class DBException : public std::exception
{
private:
	char *p_cMessage;
public:
	DBException();
	DBException( char *msg );
	const char *what();
};

}

#endif
