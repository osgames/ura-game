#include "Utilities.h"
#include "DBInterface.h"
#include "DBConnector.h"
#include "Globals.h"
#include <mysql++.h>

using namespace dbinterface;

Utilities::Utilities( DBInterface *dbi )
{
	mInterface = dbi;
}

std::string Utilities::getSkillID( std::string name )
{
	if( mapSkillID.find( name ) == mapSkillID.end() )
	{
		mysqlpp::Query qry = mInterface->getConnector()->query
			(
			"SELECT skill_id FROM skills WHERE skillName='" +
			name +
			"';"
			);
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB warning: duplicate Skill names found in skills table (Actor::getSkillID())" );
			}
			else if( res.num_rows() <= 0 )
			{
				logMessage( "No results for query in Actor::getSkillID()" );
				return "";
			}

			std::string id;
			res[0]["skill_id"].to_string(id);
			mapSkillID[name] = id;

			return id;
		}

		logMessage( "DB query failure in Actor::getSkillID()" );
		return "";
	}
	return mapSkillID[name];
}

std::string Utilities::getAttributeID( std::string name )
{
	if( mapAttributeID.find( name ) == mapAttributeID.end() )
	{
		mysqlpp::Query qry = mInterface->getConnector()->query
			(
			"SELECT attribute_id FROM skills WHERE attribName='" +
			name +
			"';"
			);
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB warning: duplicate Attribute names found in attributes table (Actor::getAttributeID())" );
			}
			else if( res.num_rows() <= 0 )
			{
				logMessage( "No results for query in Actor::getAttributeID()" );
				return "";
			}

			std::string id;
			res[0]["attribute_id"].to_string(id);
			mapAttributeID[name] = id;

			return id;
		}

		logMessage( "DB query failure in Actor::getAttributeID()" );
		return "";
	}
	return mapAttributeID[name];
}

std::string Utilities::getStatID( std::string name )
{
	if( mapStatID.find( name ) == mapStatID.end() )
	{
		mysqlpp::Query qry = mInterface->getConnector()->query
			(
			"SELECT stat_id FROM stats WHERE statName='" +
			name +
			"';"
			);
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB warning: duplicate Stat names found in stats table (Actor::getStatID())" );
			}
			else if( res.num_rows() <= 0 )
			{
				logMessage( "No results for query in Actor::getStatID()" );
				return "";
			}

			std::string id;
			res[0]["stat_id"].to_string(id);
			mapStatID[name] = id;

			return id;
		}

		logMessage( "DB query failure in Actor::getStatID()" );
		return "";
	}
	return mapStatID[name];
}

int Utilities::toInt( std::string s )
{
     std::istringstream stream( s );
     int t;
     stream >> t;
     return t;
}

float Utilities::toFloat( std::string s )
{
     std::istringstream stream( s );
     float t;
     stream >> t;
     return t;
}

std::string Utilities::toString( int val )
{
	std::stringstream oss;
	oss << val;
	return oss.str();
}

std::string Utilities::toString( float val )
{
	std::stringstream oss;
	oss << val;
	return oss.str();
}
