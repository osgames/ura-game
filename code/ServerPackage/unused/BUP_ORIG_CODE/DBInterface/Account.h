#ifndef ACCOUNT_H
#define ACCOUNT_H

#include <string>
#include <vector>
#include <map>

namespace dbinterface
{

class DBCharacter;
class Chat;
class DBInterface;

/*!
 * Provides a class view of the contents of a row of the accounts table,
 * along with easy access to related elements and common functions to
 * edit those elements.
 */
class Account
{
private:

//####################################################
	std::string sTableName;
	Chat *mChat;
	DBInterface *mInterface;
	std::string sEmail;
	std::string sFirstName;
	std::string sLastName;
	std::string sCountry;
	std::string sLastIP;
	std::string sLastLogin;
	std::string sSecurity;
	std::string sID;

public:



//##########################################################
	// Used for populating a database with a new account.
	Account( DBInterface *dbi, std::string email, std::string firstname, std::string lastname,
		std::string country, std::string security );
	// Used for populating an existing account with database information.
	Account( DBInterface *dbi, std::string id, std::string email, std::string firstname, std::string lastname,
		std::string country, std::string lastip, std::string lastlogin, std::string security );
	~Account();

	/*
	 * Warning: This has potential to generate high overhead.
	 */
	bool getAllCharacters( std::vector<DBCharacter *> &val );

	/*!
	 * Creates a new character within this account.
	 * This has the overhead of a getCharacter() method in addition to a SELECT and INSERT.
	 * @param first_name First name of character; limited to 20 characters.
	 * @param last_name Last name of character; limited to 20 characters.
	 * @param gender Character gender; 'm' and 'f' are valid candidates.
	 * @param age Age of character; Up to 4 digits.
	 * @param description Description of character; 1000 character limit.
	 * @return 0 on failure; pointer to new character on success.
	 */
	DBCharacter *createCharacter(
		std::string first_name, // 20 character limit.
		std::string last_name, // 20 character limit.
		char gender, // 'm' or 'f' are valid candidates.
		std::string age, // Up to 4 digits.
		std::string description = "" // 1000 character limit.
		);

	/*!
	 * Sets the e-mail address of the account.
	 */
	void setEmail( std::string str );

	/*!
	 * Sets the first name of the account.
	 */
	void setFirstName( std::string str );

	/*!
	 * Sets the last name of the account.
	 */
	void setLastName( std::string str );

	/*!
	 * Sets the country of the account.
	 */
	void setCountry( std::string str );

	/*!
	 * Sets the last ip address that logged into the account.
	 */
	void setLastIP( std::string str );

	/*!
	 * Sets the last date/time that the account was logged into.
	 */
	void setLastLogin( std::string str );

	/*!
	 * Sets the security level of the account.
	 */
	void setSecurity( std::string str );

	/*!
	 * Sets the account password.
	 */
	bool setPassword( std::string str );

	/*!
	 * Get the account e-mail address.
	 * @return the e-mail address as a string.
	 */
	std::string getEmail();

	/*!
	 * Get the account first name.
	 * @return the first name as a string.
	 */
	std::string getFirstName();

	/*!
	 * Get the account last name.
	 * @return the last name as a string.
	 */
	std::string getLastName();

	/*!
	 * Get the account country.
	 * @return the country as a string.
	 */
	std::string getCountry();

	/*!
	 * Get the last ip address that logged into the account.
	 * @return the last ip address as a string.
	 */
	std::string getLastIP();

	/*!
	 * Get the last login date/time.
	 * @return the last login as a string.
	 */
	std::string getLastLogin();

	/*!
	 * Get the account security level.  This entails
	 * granted permissions, etc.
	 * @return the security level as a string.
	 */
	std::string getSecurity();

	/*!
	 * Get the account table ID, as found
	 * in the accounts table as 'account_id'
	 * @return the account ID as a string.
	 */
	std::string getID();

	/*!
	 * Get the account password.
	 * @return the password as a string.
	 */
	std::string getPassword();
};

}

#endif
