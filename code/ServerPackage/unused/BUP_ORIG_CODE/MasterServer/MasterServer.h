/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MASTERSERVER_H
#define MASTERSERVER_H

#include <vector>

namespace master_server
{

class Server;

class MasterServer
{
private:
	void loadServerList();
	void loadUserList();
public:
	~MasterServer();
	MasterServer();
	std::vector<Server *> server_list;
};

} // End master_server namespace

#endif

