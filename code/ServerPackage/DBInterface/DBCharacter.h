#ifndef DBINTERFACE_CHARACTER_H
#define DBINTERFACE_CHARACTER_H

#include "Actor.h"
#include <string>

namespace dbinterface
{

class Account;

class DBCharacter : public Actor
{
private:
	Account *mAccount; // DBCharacters are always members of accounts.
public:
	// Constructor for building existing character from DB.
	DBCharacter(
		DBInterface *dbi,
		std::string id );

	// Constructor for creating a new character.
	DBCharacter(
		DBInterface *dbi,
		Account *account,
		std::string firstname,
		std::string lastname,
		char gender,
		std::string age,
		std::string description = ""
		);
	~DBCharacter();
};

}

#endif
