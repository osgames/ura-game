#include "Vector3.h"
#include "DBInterface.h"
#include "DBConnector.h"
#include "Utilities.h"
#include <mysql++.h>

using namespace dbinterface;

Vector3::Vector3() : x( 0.0f ), y( 0.0f ), z( 0.0f )
{
}

Vector3::Vector3( std::string id, DBInterface *dbi ) : x( 0.0f ), y( 0.0f ), z( 0.0f )
{
	sCharacterID = id;
	mInterface = dbi;
}

bool Vector3::setX( float val )
{
	x = val;
	mysqlpp::Query qry = mInterface->getConnector()->query(
		"UPDATE characters SET cXpos=" +
		mInterface->getUtilities()->toString( val ) +
		" WHERE character_id=" + sCharacterID + ";"
		);
	return qry.execute();
}

bool Vector3::setY( float val )
{
	y = val;
	mysqlpp::Query qry = mInterface->getConnector()->query(
		"UPDATE characters SET cYpos=" +
		mInterface->getUtilities()->toString( val ) +
		" WHERE character_id=" + sCharacterID + ";"
		);
	return qry.execute();
}

bool Vector3::setZ( float val )
{
	z = val;
	mysqlpp::Query qry = mInterface->getConnector()->query(
		"UPDATE characters SET cZpos=" +
		mInterface->getUtilities()->toString( val ) +
		" WHERE character_id=" + sCharacterID + ";"
		);
	return qry.execute();
}
