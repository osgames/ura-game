#ifndef ADMINISTRATION_H
#define ADMINISTRATION_H

#include <string>
#include <map>

namespace dbinterface
{

class Stat;
class Skill;
class DBInterface;
class Quest;
class Attribute;

class Administration
{
private:
	DBInterface *mInterface;
	std::map<std::string, Skill *> list_Skill; // Actor Skill set... 0..*
	std::map<std::string, Stat *> list_Stat; // Actor Stat set... 0..*
	std::map<std::string, Attribute *> list_Attribute; // Actor Attribute set... 0..*
public:
	Administration( DBInterface *dbi );

	/*!
	 * Assigns a stat point to the player.  Will overwrite an existing one
	 * or make/update a new points row if one doesn't exist.
	 */
	Stat *insertStat( std::string name, std::string description );

	/*!
	 * Destroys an existing Stat; this includes removal from the database.
	 */
	bool removeStat( Stat *stat );

	/*!
	 * Gets the map for the Stat list for direct access.
	 */
	std::map<std::string, Stat *> getStatMap();

	/*!
	 * Assigns a skill point to the player.  Will overwrite an existing one
	 * or make/update a new points row if one doesn't exist.
	 */
	Skill *insertSkill();

	/*!
	 * Destroys an existing Skill that the character possesses; this includes removal from the database.
	 */
	bool removeSkill( Skill *skill );

	/*!
	 * Assigns a attribute point to the player.  Will overwrite an existing one
	 * or make/update a new points row if one doesn't exist.
	 */
	Attribute *insertAttribute();

	/*!
	 * Destroys an existing Attribute; this includes removal from the database.
	 */
	bool removeAttribute( Attribute *attribute );
};

}

#endif
