#ifndef DBINTERFACE_H
#define DBINTERFACE_H

#include <string>
#include <map>
#include <vector>

namespace dbinterface
{

class NPC;
class Account;
class DBConnector;
class Utilities;
class Administration;
class DBCharacter;

/*!
 * Main interfacing class for the DB Interface.  The DB Interface is intended
 * to ease commonly used database queries by facading them behind familiar
 * data structures.  Queries are performed in a reasonably smart way and the
 * DB Interface has been designed with modularity in mind so that bugs may be
 * quickly and easily tracked down or changes may be made without writing hacks.
 */
class DBInterface
{
private:
	std::map<std::string, NPC *> list_NPC;
	std::map<std::string, Account *> list_Account;
	std::map<std::string, DBCharacter *> list_Character; // Online characters only.
	DBConnector *mConnector;
	Utilities *mUtilities;
	Administration *mAdministration;
	friend class Account;

public:
	/*!
	 * Initializes the library.
	 * @param db_name Name of the database.
	 * @param addr IP address of the database server.
	 * @param username Username of the mysql account which is to access the database.
	 * @param password Password of the mysql account which is to access the database.
	 * @param port Port of the database server to connect to.
	 */
	DBInterface(std::string databaseName, std::string ipAddress, 
		std::string userName, std::string password, unsigned int portNumber);
	~DBInterface();

	/*!
	 * Note that this call performs a query on several rows of account data, but
	 * does not query character information.  If log set to false, logging is disabled
	 * during this call.  This is useful for things such as CGI scripts, etc.
	 * @param email the email address of the desired account.
	 * @param log true (default) to enable logging for this specific function call; false to disable logging for this function call
	 * @return pointer to the instance of the specified Account class, otherwise 0.
	 */
	Account *getAccount( std::string email, bool log = true );

	/*!
	 * Note that this call performs a query on many rows of data but does not query character information.
	 * WARNING: This call has potential for very high overhead!
	 * @param accounts returns a vector with pointers to all of the accounts.
	 * @return pointer to the instance of the specified Account class, otherwise 0.
	 */
	bool getAllAccounts( std::vector<Account *> &accounts );

	/*!
	 * Creates a new Account in the database and returns a pointer to it, otherwise returns 0.
	 * This should have the overhead of a standard SQL INSERT.
	 */
	Account *createAccount(std::string id, std::string email, 
	std::string firstName, std::string lastName, std::string password);
	 

	bool createCharacter(
		Account* account,
		std::string characterID,
		std::string first_name,
		std::string last_name,
		char gender,
		std::string age,
		std::string description = ""
		);

	/*!
	 * Note that this call has low overhead as it does not query all data on the specified NPC.
	 * Specific data structures which are members of NPC may and most likely do query data
	 * upon activation.
	 * @return pointer to the instance of the specified NPC class, otherwise 0.
	 */
	NPC *getNPC( std::string firstname, std::string lastname );

	/*!
	 * @return a pointer to the DBConnector instance, otherwise 0.
	 */
	DBConnector *getConnector();

	/*!
	 * @return a pointer to the Utilities class which contains many useful helper methods.
	 */
	Utilities *getUtilities();

	/*!
	 * @return a pointer to the Administration class which contains methods for authoring the DB data.
	 */
	Administration *getAdministration();

	/*!
	 * @return a pointer to the specified DBCharacter or 0 if none could be found in the DB.
	 */
	DBCharacter *getCharacter( std::string first_name, std::string last_name, bool log = true );

	/*!
	 * Deallocates the character from memory.  Good idea for if the player disconnects.
	 */
	bool notifyOffline( DBCharacter *character );

	/*!
	 * Deallocates the account from memory.  Good idea for if the player disconnects.
	 */
	bool notifyOffline( Account *account );

	/*!
	 * Deallocates the NPC from memory.  Good idea for if the NPC is not accessed for
	 * several hours, or if the server is low on memory, etc.
	 */
	bool notifyOffline( NPC *npc );
};

}

#endif
