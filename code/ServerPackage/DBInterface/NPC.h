#ifndef NPC_H
#define NPC_H

#include "Actor.h"

namespace dbinterface
{

class DBInterface;

class NPC : public Actor
{
public:
	NPC( DBInterface *dbi, std::string character_id );
	~NPC();
};

}

#endif
