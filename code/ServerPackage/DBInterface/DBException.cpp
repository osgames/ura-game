#include "DBException.h"

using namespace dbinterface;

DBException::DBException()
{
  p_cMessage = (char*) "Generic exception has occured.";
}

DBException::DBException( char *msg )
{
	p_cMessage = msg;
}

const char *DBException::what()
{
	return p_cMessage;
}
