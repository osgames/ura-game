#include "Actor.h"

#include "Vector3.h"
#include "Inventory.h"
#include "Race.h"
#include "Skill.h"
#include "Stat.h"
#include "Quest.h"
#include "Faction.h"
#include "Attribute.h"
#include "DBInterface.h"
#include "DBConnector.h"
#include "Globals.h"
#include "DBException.h"
#include "Utilities.h"

#include <string>
#include <map>
#include <mysql++.h>

using namespace dbinterface;

std::string Actor::getCharacterID()
{
	if( sCharacterID != "" )
		return sCharacterID;

	mysqlpp::Query qry = mInterface->getConnector()->query
		(
		"SELECT character_id FROM characters WHERE cFirstname='" +
		sFirstName +
		"' AND cLastname='" +
		sLastName +
		"';"
		);
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each name should be unique.  Log it and continue as usual.
			logMessage( "DB warning: duplicate character names found in characters table (Actor::getCharacterID())" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "No results for query in Actor::getCharacterID()" );
			return "";
		}

		std::string id;
		res[0]["character_id"].to_string(id);
		sCharacterID = id;

		return id;
	}

	logMessage( "DB query failure in Actor::getCharacterID()" );
	return "";
}

Vector3 *Actor::getPosition()
{
	return mPosition;
}

float Actor::getYaw()
{
	return fYaw;
}

void Actor::setYaw(float yaw)
{
	fYaw = yaw;
}

//Race *Actor::getRace()
//{
//	return mRace;
//}

std::string Actor::getSkillPoints(std::string id)
{
	// This will be used later on when priority memory caching is implemented.
	/*if( list_Skill.find( name ) == list_Skill.end() )
	{
		return 0;
	}
	return list_Skill[name];*/

	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT skillPoints FROM points WHERE characters_character_id='" + getCharacterID() +
		"' AND skills_skill_id='" + id +
		"';"
		);

	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each should be unique.  Log it and continue as usual.
			logMessage( "DB warning: name issue found in Actor::getSkillPoints()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "No results for query in Actor::getSkillPoints()" );
			return "";
		}

		std::string points;
		res[0]["skillPoints"].to_string( points );
		return points;
	}
	else
	{
		logMessage( "DB connector error: failed query in Actor::getSkillPoints()" );
		return "";
	}
	return "";
}

bool Actor::updatePoint( std::string id_name,
						std::string point_name,
						std::string id,
						std::string point_value )
{
	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT " + id_name +
		"FROM points WHERE characters_character_id=" +
		getCharacterID() + "AND " + id_name + "=" +
		id + ";"
		);
	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each should be unique.  Log it and continue as usual.
			logMessage( "DB warning: multiple of same ID for one character found in points table (Actor::updatePoint())" );
		}
		else if( res.num_rows() == 0 )
		{
			// No results found so check for a null field we can use.
			qry = mInterface->getConnector()->query(
			"SELECT " + id_name +
			"FROM points WHERE characters_character_id=" +
			getCharacterID() + "AND " + id_name + "=null;"
			);
			if( mysqlpp::StoreQueryResult res = qry.store() )
			{
				if( res.num_rows() >= 1 )
				{
					// A null field exists; update it.
					qry = mInterface->getConnector()->query(
					"UPDATE points SET " + point_name + "=" +
					point_value + " WHERE characters_character_id=" +
					getCharacterID() + " AND " + id_name + "=null;"
					);
					return qry.execute();
				}
			}
		}
		else
		{
			logMessage( "DB Error: Serious error has occured in Actor::updatePoint()" );
		}
	}
	else
	{
		logMessage( "DB connector error: failed query in Actor::updatePoint()" );
		return false;
	}

	// Made it this far so the query exists.
	qry = mInterface->getConnector()->query(
		"UPDATE points SET " + point_name + "=" +
		point_value + " WHERE characters_character_id=" +
		getCharacterID() + " AND " + id_name + "=" + id + ";"
		);

	return qry.execute();
}

bool Actor::updateSkillPoint( std::string id, std::string point_value )
{
	return updatePoint( "skills_skill_id", "skillPoints", id, point_value );
}

bool Actor::updateStatPoint( std::string id, std::string point_value )
{
	return updatePoint( "stats_stat_id", "statPoints", id, point_value );
}

bool Actor::updateAttributePoint( std::string id, std::string point_value )
{
	return updatePoint( "attributes_attribute_id", "attribPoints", id, point_value );
}

std::string Actor::getStatPoints(std::string id)
{
	// This will be used later on when priority memory caching is implemented.
	/*if( list_Skill.find( name ) == list_Skill.end() )
	{
		return 0;
	}
	return list_Skill[name];*/

	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT statPoints FROM points WHERE characters_character_id='" + getCharacterID() +
		"' AND stats_stat_id='" + id +
		"';"
		);

	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each should be unique.  Log it and continue as usual.
			logMessage( "DB warning: name issue found in Actor::getStatPoints()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "No results for query in Actor::getStatPoints()" );
			return "";
		}

		std::string points;
		res[0]["statPoints"].to_string( points );
		return points;
	}
	else
	{
		logMessage( "DB connector error: failed query in Actor::getStatPoints()" );
		return "";
	}
	return "";
}

//Quest *Actor::getQuest(std::string name)
//{
//	if( list_Quest.find( name ) == list_Quest.end() )
//	{
//		return 0;
//	}
//	return list_Quest[name];
//}
//
//Faction *Actor::getFaction(std::string name)
//{
//	if( list_Faction.find( name ) == list_Faction.end() )
//	{
//		return 0;
//	}
//	return list_Faction[name];
//}

std::string Actor::getAttributePoints(std::string id)
{
	/*if( list_Attribute.find( name ) == list_Attribute.end() )
	{
		return 0;
	}
	return list_Attribute[name];*/

		// This will be used later on when priority memory caching is implemented.
	/*if( list_Skill.find( name ) == list_Skill.end() )
	{
		return 0;
	}
	return list_Skill[name];*/

	mysqlpp::Query qry = mInterface->getConnector()->query(
		"SELECT attribPoints FROM points WHERE characters_character_id='" + getCharacterID() +
		"' AND attributes_attribute_id='" + id +
		"';"
		);

	if( mysqlpp::StoreQueryResult res = qry.store() )
	{
		if( res.num_rows() > 1 )
		{
			// This is problematic since each should be unique.  Log it and continue as usual.
			logMessage( "DB warning: name issue found in Actor::getAttributePoints()" );
		}
		else if( res.num_rows() <= 0 )
		{
			logMessage( "No results for query in Actor::getAttributePoints()" );
			return "";
		}

		std::string points;
		res[0]["attribPoints"].to_string( points );
		return points;
	}
	else
	{
		logMessage( "DB connector error: failed query in Actor::getAttributePoints()" );
		return "";
	}
	return "";
}

Inventory *Actor::getInventory()
{
	return mInventory;
}

//Faction *Actor::createFaction()
//{
//	return 0;
//}
//
//Quest *Actor::createQuest()
//{
//	return 0;
//}



//bool Actor::removeFaction( Faction *faction )
//{
//	std::map<std::string, Faction *>::iterator it = list_Faction.find( "" );
//	delete it->second;
//	list_Faction.erase( it );
//	return false;
//}
//
//bool Actor::removeQuest( Quest *quest )
//{
//	std::map<std::string, Quest *>::iterator it = list_Quest.find( "" );
//	delete it->second;
//	list_Quest.erase( it );
//	return false;
//}


bool Actor::createPointSet(
	std::string skill, std::string skill_points,
	std::string stat, std::string stat_points,
	std::string attribute, std::string attribute_points
					)
{
	mysqlpp::Query qry = mInterface->getConnector()->query(
		std::string("INSERT INTO points (") +
		"null, " +
		getCharacterID() + ", " +
		mInterface->getUtilities()->getSkillID( skill ) + ", " +
		mInterface->getUtilities()->getStatID( stat ) + ", " +
		mInterface->getUtilities()->getAttributeID( attribute ) + ", " +
		skill_points + ", " +
		stat_points + ", " +
		attribute_points +
		");");

		if( !qry.execute() )
		{
			logMessage( "Query execute failed in Actor::createPointSet()" );
			return false;
		}
		return true;
}



//void Actor::populateSkills()
//{
//	/*if( mInterface == 0 )
//			throw DBException( "Actor::createStat(): mInterface is NULL." );
//	mysqlpp::Query qry = mInterface->getConnector()->query(
//		std::string( "SELECT * FROM points " ) +
//		std::string( "INNER JOIN characters " ) +
//		std::string( "ON points.characters_character_id=characters.character_id " ) +
//		std::string( "INNER JOIN skills " ) +
//		std::string( "ON points.skills_skill_id=skills.skill_id" ) +
//		std::string( ";" )
//		);*/
//}
//
//void Actor::populateStats()
//{
//}
//
//void Actor::populateQuests()
//{
//}
//
//void Actor::populateFactions()
//{
//}
//
//void Actor::populateAttributes()
//{
//}

std::string Actor::getFirstName()
{
	return sFirstName;
}

std::string Actor::getLastName()
{
	return sLastName;
}
