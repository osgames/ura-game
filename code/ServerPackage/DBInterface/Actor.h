#ifndef ACTOR_H
#define ACTOR_H

#include <map>
#include "Vector3.h"

namespace dbinterface
{

class Inventory;
class Race;
class Skill;
class Stat;
class Quest;
class Faction;
class Attribute;
class DBInterface;

/*!
 * Actor is a base class inherited by classes such as NPC and
 * Character which need the general functionality of an interactive living creature.
 * All 'get' methods return 0 if unsuccessful.  If inheriting from this class,
 * make sure to initialize mInterface.
 */
class Actor
{
protected:
	std::string sCharacterID; // Character ID.
	Vector3 *mPosition; // Position of the Actor.
	float fYaw; // Yaw of the Actor (part of the orientation).
	Inventory *mInventory; // Actor Inventory... 1
	Race *mRace; // Actor Race information... 1
	std::string sFirstName; // Actor unique first name.
	std::string sLastName; // Actor unique last name.
	DBInterface *mInterface; // Main interface class.
	char cGender; // Actor gender.
	int iAge;
	std::map<std::string, Faction *> list_Faction; // Actor Faction set... 0..*
	std::map<std::string, Quest *> list_Quest; // Actor Quest set... 0..*
	
	/*
	 * Internal generic point updating method.
	 * Has higher overhead due to the need to perform several checks on the DB.
	 */
	bool updatePoint(	std::string id_name,
				std::string point_name,
				std::string id,
				std::string point_value );

public:
	/*!
	 * @return the character ID as a string.
	 */
	virtual std::string getCharacterID();

	/*!
	 * @return the position of the actor as a Vector3.
	 */
	virtual Vector3 *getPosition();

	/*!
	 * Convenience function that returns the yaw since yaw is the most common orientation change.
	 * @return yaw as inputted.
	 */
	virtual float getYaw();

	/*!
	 * Convenience function since yaw is the most common orientation change.
	 * Note that this changes orientation.
	 */
	virtual void setYaw(float yaw);

	/*
	 * Returns the race of the Actor, or 0 if none is present.
	 */
	//virtual Race *getRace();

	/*!
	*  Get skill points based on skill_id.
	*/
	virtual std::string getSkillPoints(std::string id);

	/*!
	 * Updates the specified skill point id for this Actor.
	 * If skill point doesn't exist for this Actor, a new one
	 * will be added.  Warning: this has higher overhead so
	 * try not to use it when possible.
	 * @return true for success; false for failure.
	 */
	virtual bool updateSkillPoint( std::string id, std::string point_value );

	/*!
	 * Updates the specified stat point id for this Actor.
	 * If stat point doesn't exist for this Actor, a new one
	 * will be added.  Warning: this has higher overhead so
	 * try not to use it when possible.
	 * @return true for success; false for failure.
	 */
	virtual bool updateStatPoint( std::string id, std::string point_value );

	/*!
	 * Updates the specified stat point id for this Actor.
	 * If stat point doesn't exist for this Actor, a new one
	 * will be added.  Warning: this has higher overhead so
	 * try not to use it when possible.
	 * @return true for success; false for failure.
	 */
	virtual bool updateAttributePoint( std::string id, std::string point_value );

	/*!
	*  Get stat points based on stat_id.
	*/
	virtual std::string getStatPoints(std::string id);

	/*
	*  Get quest based on name.  Useful for finding one quest.
	*/
	//virtual Quest *getQuest(std::string name);

	/*
	*  Get faction based on name.  Useful for finding one faction.
	*/
	//virtual Faction *getFaction(std::string name);

	/*!
	 * @return the inventory for the Actor, or 0 if none exists.
	 */
	virtual Inventory *getInventory();

	/*!
	*  Get attribute based on name.  Useful for finding one attribute.
	*/
	virtual std::string getAttributePoints(std::string id);

	/*
	 * Create a new empty Faction.
	 */
	//virtual Faction *createFaction();

	/*
	 * Destroys an existing Faction; this includes removal from the database.
	 */
	//virtual bool removeFaction( Faction *faction );

	/*
	 * Create a new empty Quest.
	 */
	//virtual Quest *createQuest();

	/*
	 * Destroys an existing Quest; this includes removal from the database.
	 */
	//virtual bool removeQuest( Quest *quest );

	/*!
	 * Creates a new point set in the database.  Writing this as a set instead of individually is more efficient.
	 * Pass a string with the word "null" for any fields not needed.
	 */
	virtual bool createPointSet(
		std::string skill, std::string skill_points,
		std::string stat, std::string stat_points,
		std::string attribute, std::string attribute_points
								);

	/*
	 * Populates the map maintained in this class with all Skills found in the database.
	 * This should be more efficient than individually loading each skill as it's needed.
	 */
	//virtual void populateSkills();

	/*
	 * Populates the map maintained in this class with all Stats found in the database.
	 * This should be more efficient than individually loading each skill as it's needed.
	 */
	//virtual void populateStats();

	/*
	 * Populates the map maintained in this class with all Quests found in the database.
	 * This should be more efficient than individually loading each skill as it's needed.
	 */
	//virtual void populateQuests();

	/*
	 * Populates the map maintained in this class with all Factions found in the database.
	 * This should be more efficient than individually loading each skill as it's needed.
	 */
	//virtual void populateFactions();

	/*
	 * Populates the map maintained in this class with all Attribute found in the database.
	 * This should be more efficient than individually loading each skill as it's needed.
	 */
	//virtual void populateAttributes();

	/*!
	 * Gets the first name of the actor.
	 */
	virtual std::string getFirstName();

	/*!
	 * Gets the last name of the actor.
	 */
	virtual std::string getLastName();
};

}

#endif
