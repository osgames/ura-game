#include "DBInterface.h"
#include "Account.h"
#include "NPC.h"
#include "DBConnector.h"
#include "Globals.h"
#include "Utilities.h"
#include "Administration.h"
#include "DBCharacter.h"
#include <vector>
#include <algorithm>
#include <mysql++.h>

using namespace dbinterface;


//###################//
//### TABLE NAMES ###//
//###################//
// "accounts"
// "characters"
 
 
 
DBInterface::DBInterface(std::string databaseName, std::string ipAddress, 
		std::string userName, std::string password, unsigned int portNumber)
: mConnector( 0 ), mUtilities( 0 ), mAdministration( 0 )
{
	mConnector = new DBConnector(databaseName, ipAddress, userName, password, portNumber);
	mUtilities = new Utilities( this );
	mAdministration = new Administration( this );
}

DBInterface::~DBInterface()
{
	for( std::map<std::string, Account *>::iterator MapItor = list_Account.begin(); MapItor != list_Account.end(); MapItor++ )
	{
		Account *Value = (*MapItor).second;
		delete Value;
	}

	for( std::map<std::string, NPC *>::iterator MapItor = list_NPC.begin(); MapItor != list_NPC.end(); MapItor++ )
	{
		NPC *Value = (*MapItor).second;
		delete Value;
	}

	if( mAdministration )
	{
		delete mAdministration;
		mAdministration = 0;
	}

	if( mUtilities )
	{
		delete mUtilities;
		mUtilities = 0;
	}

	if( mConnector )
	{
		delete mConnector;
		mConnector = 0;
	}
}

 

Account *DBInterface::getAccount( std::string email, bool log )
{
std::cout << "############## = " << std::endl;

	 
	if( list_Account.find( email ) == list_Account.end() )
	{
		// Try to pull it from the database.
		// Query database for account.
		mysqlpp::Query qry = getConnector()->query( "SELECT * FROM accounts WHERE email='" + email + "';" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each e-mail address should be unique.  Log it and continue as usual.
				if( log )
				  logMessage( "DB error: e-mail address issue found in DBInterface::getAccount()" );
			}
			else if( res.num_rows() <= 0 )
			{
				if( log )
				  logMessage( "No results for query in DBInterface::getAccount()" );
				return 0;
			}
std::cout << "1 ###" << std::endl;

			std::string id;
			std::string tmpEmail;
			std::string firstname;
			std::string lastname;
			std::string country;
			std::string lastip;
			std::string lastlogin;
			std::string security;


			res[0]["account_id"].to_string(id);
std::cout << "2 ###" << std::endl;
			res[0]["email"].to_string(tmpEmail);
			res[0]["firstname"].to_string(firstname);
			res[0]["lastname"].to_string(lastname);
			res[0]["country"].to_string(country);
			res[0]["lastip"].to_string(lastip);
			res[0]["lastlogin"].to_string(lastlogin);
			res[0]["security"].to_string(security);
std::cout << "3 ###" << std::endl;
			Account *account = new Account(
				this,
				id,
				tmpEmail,
				firstname,
				lastname,
				country,
				lastip,
				lastlogin,
				security
				);
std::cout << "4 ###" << std::endl;
			list_Account[email] = account;
std::cout << "5 ###" << std::endl;
			return account;
		}
		else
		{
std::cout << "6 ###" << std::endl;
			if( log )
			  logMessage( "DB connector error: failed to get item list in DBInterface::getAccount()" );
			return 0;
		}
	}
	else
	{
std::cout << "7 ###" << std::endl;
		return list_Account[email];
	}
	return 0;
}

bool DBInterface::getAllAccounts( std::vector<Account *> &accounts )
{
std::cout << "1 getAllAccounts ###" << std::endl;
	mysqlpp::Query qry = getConnector()->query(
			"SELECT email FROM accounts;" );
std::cout << "2 getAllAccounts ###" << std::endl;
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
std::cout << "3 getAllAccounts ###" << std::endl;
			if( res.num_rows() >= 1 )
			{
				std::vector<Account *> vec;
				for( int i = 0; i < res.size(); i++ )
				{
					std::string email;
					res[i]["email"].to_string( email );
					Account *acct = getAccount( email );
					vec.push_back( acct );
				}
				accounts = vec;
			}
			else if( res.num_rows() <= 0 )
			{
				return false;
			}

			return true;
		}
		else
		{
std::cout << "4 getAllAccounts ###" << std::endl;
			return false;
		}

	return true;
}



Account *DBInterface::createAccount(std::string id, std::string email, 
	std::string firstName, std::string lastName, std::string password)
{
	if( getAccount(email, false) )
	{
		return NULL;
	}

	Account *temp = new Account(this, id, email, firstName, lastName, password);
	list_Account[email] = temp;
	return temp;
}



bool DBInterface::createCharacter(
		Account* account,
		std::string characterID,
		std::string first_name,
		std::string last_name,
		char gender,
		std::string age,
		std::string description)
{
	if ( this->list_Character.find( first_name + " " + last_name ) != this->list_Character.end() ||
		this->getCharacter(first_name, last_name, false) )
	{
		// The name already exists in one of the character lists.
		logMessage( "createCharacter failed; name already exists." );
		return false;
	}


	// Get the Account id //
	std::string accountID = account->getID();

	
	DBCharacter *temp = 0;

	std::string gend;
	if( gender == 'm' )
	{
		gend = "0";
	}
	else if( gender == 'f' )
	{
		gend = "1";
	}
	else
	{
		return false;
	}
	
	/*********************
	mysqlpp::Query qry = this->getConnector()->query(
		std::string("INSERT INTO characters VALUES (") +
		"'" + characterID + std::string("', ") + // character_id
		"'" + accountID + "', " + // accounts_account_id
		"null, " + // sectors_sector_id
		"null, " + // instances_instance_id
		"'" + first_name + "', " + // cFirstname
		"'" + last_name + "', " + // cLastname
		"'" + description + "', " + // cDescription
		gend + ", " + // cGender
		age + ", " + // cAge
		"0.0, " + // cXpos
		"0.0, " + // cYpos
		"0.0, " + // cZpos
		"false" + // isNPC
		");"
		);
	***************************/


	  
	mysqlpp::Query qry = this->getConnector()->query(
		std::string("INSERT INTO characters VALUES (") +
		characterID + ", " +		// character_id
		accountID + ", " +			// accounts_account_id
		"null, " +					// sectors_sector_id
		"null, " +					// instances_instance_id
		"'" + first_name + "', " +	// cFirstname
		"'" + last_name + "', " +	// cLastname
		"'" + description + "', " + // cDescription
		gend + ", " +				// cGender
		age + ", " +				// cAge
		"0.0, " +					// cXpos
		"0.0, " +					// cYpos
		"0.0, " +					// cZpos
		"false" +					// isNPC
		");"
		);


	 

	  
	if( !qry.execute() )
	{
		logMessage( "Query execute failed in Account::createCharacter (SQL INSERT) (see line below for more details)" );
		logMessage( qry.error() );
		return false;
	}

	temp = new DBCharacter(
				this,
				account,
				first_name,
				last_name,
				gender,
				age,
				description
				);

	this->list_Character[first_name + " " + last_name] = temp;
}


 
NPC *DBInterface::getNPC( std::string firstname, std::string lastname )
{
	if( list_NPC.find( firstname + ' ' + lastname ) == list_NPC.end() )
	{
		// Try to pull it from the database.
		// Query database for npc.
		mysqlpp::Query qry = getConnector()->query(
			"SELECT character_id FROM characters WHERE cFirstname='" + firstname +
			"' AND cLastname='" + lastname + "';" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB error: name issue found in DBInterface::getNPC()" );
			}
			else if( res.num_rows() <= 0 )
			{
				logMessage( "No results for query in DBInterface::getNPC()" );
				return 0;
			}

			std::string id;
			res[0]["character_id"].to_string( id );
			NPC *npc = new NPC( this, id );

			list_NPC[firstname + ' ' + lastname] = npc;

			return npc;
		}
		else
		{
			logMessage( "DB connector error: failed to get item list in DBInterface::getNPC()" );
			return 0;
		}
	}
	else
	{
		return list_NPC[firstname + ' ' + lastname];
	}
	return 0;
}

DBConnector *DBInterface::getConnector()
{
	return mConnector;
}

Utilities *DBInterface::getUtilities()
{
	return mUtilities;
}

Administration *DBInterface::getAdministration()
{
	return mAdministration;
}

DBCharacter *DBInterface::getCharacter( std::string firstname, std::string lastname, bool log )
{
	if( list_Character.find( firstname + ' ' + lastname ) == list_Character.end() )
	{
		// Try to pull it from the database.
		// Query database for character.
		mysqlpp::Query qry = getConnector()->query(
			"SELECT character_id FROM characters WHERE cFirstname='" + firstname +
			"' AND cLastname='" + lastname + "';" );
		if( mysqlpp::StoreQueryResult res = qry.store() )
		{
			if( res.num_rows() > 1 )
			{
				// This is problematic since each name should be unique.  Log it and continue as usual.
				logMessage( "DB error: name issue found in DBInterface::getCharacter()" );
			}
			else if( res.num_rows() <= 0 )
			{
				if( log )
				      logMessage( "No results for query in DBInterface::getCharacter()" );
				return 0;
			}

			std::string id;
			res[0]["character_id"].to_string( id );
			DBCharacter *character = new DBCharacter( this, id );

			list_Character[firstname + ' ' + lastname] = character;

			return character;
		}
		else
		{
			logMessage( "DB connector error: failed to get item list in DBInterface::getCharacter()" );
			return 0;
		}
	}
	else
	{
		return list_Character[firstname + ' ' + lastname];
	}
	return 0;
}

bool DBInterface::notifyOffline( DBCharacter *character )
{
	if( list_Character.find( character->getFirstName() + ' ' + character->getLastName() ) != list_Character.end() )
	{
		delete list_Character[character->getFirstName() + ' ' + character->getLastName()];
		list_Character.erase( character->getFirstName() + ' ' + character->getLastName() );
		return true;
	}
	return false;
}

bool DBInterface::notifyOffline( Account *account )
{
	if( list_Account.find( account->getFirstName() + ' ' + account->getLastName() ) != list_Account.end() )
	{
		delete list_Account[account->getFirstName() + ' ' + account->getLastName()];
		list_Account.erase( account->getFirstName() + ' ' + account->getLastName() );
		return true;
	}
	return false;
}

bool DBInterface::notifyOffline( NPC *npc )
{
	if( list_NPC.find( npc->getFirstName() + ' ' + npc->getLastName() ) != list_NPC.end() )
	{
		delete list_NPC[npc->getFirstName() + ' ' + npc->getLastName()];
		list_NPC.erase( npc->getFirstName() + ' ' + npc->getLastName() );
		return true;
	}
	return false;
}
