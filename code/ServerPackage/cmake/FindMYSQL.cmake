# Try to find mysql
#
# Once found, this will define
# NAME_FOUND -- did we find it?
# NAME_INCLUDE_DIRS -- path to includes
# NAME_LIBRARIES -- libraries to link against

include(LibFindMacros)

# Include dir
find_path(MYSQL_INCLUDE_DIR
	NAMES mysql.h
	#PATHS /usr/include/ode
	#C:/OgreSDK/include/ode
)

# And the library
find_library(MYSQL_LIBRARY
	NAMES mysql
	#PATHS C:/OgreSDK/lib
)

# Finish up
set(MYSQL_PROCESS_INCLUDES MYSQL_INCLUDE_DIR)
set(MYSQL_PROCESS_LIBS MYSQL_LIBRARY)
libfind_process(MYSQL)

