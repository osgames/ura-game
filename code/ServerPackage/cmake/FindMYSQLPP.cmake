# Try to find mysqlpp
#
# Once found, this will define
# NAME_FOUND -- did we find it?
# NAME_INCLUDE_DIRS -- path to includes
# NAME_LIBRARIES -- libraries to link against

include(LibFindMacros)

# Include dir
find_path(MYSQLPP_INCLUDE_DIR
	NAMES mysql++.h
	#PATHS /usr/include/ode
	#C:/OgreSDK/include/ode
)

# And the library
find_library(MYSQLPP_LIBRARY
	NAMES mysqlpp
	#PATHS C:/OgreSDK/lib
)

# Finish up
set(MYSQLPP_PROCESS_INCLUDES MYSQLPP_INCLUDE_DIR)
set(MYSQLPP_PROCESS_LIBS MYSQLPP_LIBRARY)
libfind_process(MYSQLPP)

