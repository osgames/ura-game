# Try to find PCRE
#
# Once found, this will define
# PCRE_FOUND -- did we find it?
# PCRE_INCLUDE_DIRS -- path to includes
# PCRE_LIBRARIES -- libraries to link against

include(LibFindMacros)

# Include dir
find_path(PCRE_INCLUDE_DIR
	NAMES pcre.h
)

# And the library
find_library(PCRE_LIBRARY
	NAMES pcre
)

# Finish up
set(PCRE_PROCESS_INCLUDES PCRE_INCLUDE_DIR)
set(PCRE_PROCESS_LIBS PCRE_LIBRARY)
libfind_process(PCRE)

