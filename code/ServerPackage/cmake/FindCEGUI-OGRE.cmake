# Try to find CEGUI-OGRE
#
# Once found, this will define
# CEGUI-OGRE_FOUND -- did we find it?
# CEGUI-OGRE_INCLUDE_DIRS -- path to includes
# CEGUI-OGRE_LIBRARIES -- libraries to link against

include(LibFindMacros)

# Dependencies
# We find CEGUI ourselves to avoid pulling in an incompatible version
# when the OgreSDK is used
libfind_package(CEGUI-OGRE OGRE)

# Get some hints from pkg-config
libfind_pkg_check_modules(CEGUI-OGRE_PKGCONF CEGUI-OGRE)
libfind_pkg_check_modules(CEGUI_PKGCONF CEGUI)


# CEGUI Include dir
find_path(CEGUI_INCLUDE_DIR
	NAMES CEGUI.h
	PATHS ${CEGUI_PKGCONF_INCLUDE_DIRS}
	${OGRE_INCLUDE_DIR}/CEGUI
	/usr/include/CEGUI
	C:/OgreSDK/include/CEGUI
)

	
# Include dir (we need an extra hint in order to find CEGUI-OGRE,
# as it lives with the OGRE headers in some installations)
find_path(CEGUI-OGRE_INCLUDE_DIR
	NAMES OgreCEGUIRenderer.h
	PATHS ${CEGUI-OGRE_PKGCONF_INCLUDE_DIRS}
	C:/OgreSDK/samples/include
	HINTS ${OGRE_INCLUDE_DIR}
)

# And the libraries
find_library(CEGUI_LIBRARY
	NAMES CEGUIBase
	PATHS ${CEGUI_PKGCONF_LIBRARY_DIRS}
	C:/OgreSDK/lib
# TODO: fix library hinting code and turn it into something reusable
)

find_library(CEGUI-OGRE_LIBRARY
	NAMES CEGUIOgreRenderer
	OgreGUIRenderer
	PATHS ${CEGUI-OGRE_PKGCONF_LIBRARY_DIRS}
	C:/OgreSDK/lib
)

# Finish up
set(CEGUI-OGRE_PROCESS_INCLUDES CEGUI-OGRE_INCLUDE_DIR OGRE_INCLUDE_DIRS CEGUI_INCLUDE_DIR)
set(CEGUI-OGRE_PROCESS_LIBS CEGUI-OGRE_LIBRARY OGRE_LIBRARIES CEGUI_LIBRARY)
libfind_process(CEGUI-OGRE)

