/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#include "MasterServer.h"
#include "Server.h"

#include <tinyxml.h>
#include <iostream>


namespace master_server
{

MasterServer::MasterServer()
{
	loadServerList();
}
 
MasterServer::~MasterServer()
{
	for( int indx = 0; indx < server_list.size(); indx++ )
	{
		delete server_list[indx];
	}
}

void MasterServer::loadServerList()
{
	TiXmlDocument *m_document;
	double m_version = 1.0;
	try
	{
		m_document = new TiXmlDocument( "serverlist.xml" );

		
		if ( !m_document->LoadFile() )
		{
			std::cout << "Error loading the file serverlist.xml \n";
		}

		
		if( m_document->Error() )
		{
			// Error will be reported here.
			delete m_document;
			m_document = NULL;
			return;
		}
	}
	catch(...)
	{
		// Error will be reported here.
		delete m_document;
		m_document = NULL;
		return;
	} 

	
	TiXmlElement *m_root_element = m_document->RootElement();
	if( strcmp( m_root_element->Value(), "ServerList" ) != 0 )
	{
		// Error will be thrown here.  The file is not in the correct format.
		std::cout << "Error: loadServerList(): m_root_element->Value() \n";
	}
	if( atof(m_root_element->Attribute( "version" )) != m_version )
	{
		// Error will be thrown here.  Incorrect version.
		std::cout << "Error: loadServerList(): version \n";
	}

	TiXmlElement *element = m_root_element->FirstChildElement( "Server" );
	while( element )
	{
		Server *s = new Server();
		server_list.push_back( s );

		s->name = element->Attribute( "Name" );
		s->ip_address = element->Attribute( "Address" );
		element = element->NextSiblingElement();
	}
	delete m_document;
}



} // End master_server namespace
