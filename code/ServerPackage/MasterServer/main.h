/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MASTERSERVER_MAIN_H__
#define __MASTERSERVER_MAIN_H__

#include <iostream>
#include <sstream>
#include <vector>
#include "Server.h"
#include "MasterServer.h"
#include <tinyxml.h>
#include <RakPeer.h>
#include <RakNetworkFactory.h>
#include <BitStream.h>
#include <StringCompressor.h>

#include "../Dependencies/NetworkMessages.h"
#include "DBInterface.h"
#include "Account.h"


#define MAX_CLIENTS 600


namespace master_server
{
	 
using namespace dbinterface;

bool bLoop;


char *zErrMsg = 0;

int g_argc;
char **g_argv;

MasterServer *mMasterServer = 0;

DBInterface *mDBInterface;



using namespace std;

RakPeerInterface *mPeer;

static int callback(void *NotUsed, int argc, char **argv, char **azColName)
{
   g_argc = argc;
   g_argv = argv;
   return 0;
}

int random_range(int lowest_number, int highest_number)
{
    if(lowest_number > highest_number){
        swap(lowest_number, highest_number);
    }

    int range = highest_number - lowest_number + 1;
    return lowest_number + int(range * rand()/(RAND_MAX + 1.0));
}

// Invalid username or password.
void notifyInvalidPassword( SystemAddress &sa )
{
	RakNet::BitStream output;
	unsigned char typeId = ID_INVALID_PASSWORD;
	output.Write( typeId );

	mPeer->Send( &output, HIGH_PRIORITY, RELIABLE_ORDERED, 0, sa, false );
}

bool initialize()
{
	/****************
	 * INITIALIZE
	 ***************/
	mMasterServer = new MasterServer();
	
	mPeer = RakNetworkFactory::GetRakPeerInterface();
	mPeer->Startup( MAX_CLIENTS, 0, &SocketDescriptor( 38740, 0 ), 1 );
	mPeer->SetMaximumIncomingConnections( MAX_CLIENTS );

	 
	///////////////////////////////////////////////////////////
	/// Connect to the database	on the mysqld port			///
	/// Database name = 'test', user='test', pass='test'	///
	///////////////////////////////////////////////////////////
	//mDBInterface = new DBInterface( "uradb", "localhost", "ura", "1337", 1337 );
	//mDBInterface = new DBInterface( "test", "127.0.0.1", "test", "test", 3306 );
	//mDBInterface = new DBInterface( "test", "localhost", "test", "test", 3306 );
	mDBInterface = new DBInterface( "test", "localhost", "root", "test", 3306 );
	 
	bLoop = true;
	  
	  
	
	///////////////////////////////////////////////////////////
	/// CREATE TWO TEMP ACCOUNTS AND CHARACTERS FOR TESTING ///
	///////////////////////////////////////////////////////////

	// first account and character //
	std::string accountID1 = "0";
	std::string characterID1 = "0";

	// second account and character //
	std::string accountID2 = "1";
	std::string characterID2 = "1";


	/// Create the first account and character ///
	Account* tmpAccount1 = mDBInterface->createAccount(
		std::string(accountID1),		// account id;
		std::string("test1"),			// email
		std::string("test"),			// firstname
		std::string("test"),
		std::string("test")
		);
	 
	mDBInterface->createCharacter(tmpAccount1, characterID1,"john","doe", 'm',"12");
	
	/// Create the Second account and character ///
	Account* tmpAccount2 = mDBInterface->createAccount(
		std::string(accountID2),		// account id;
		std::string("test2"),			// email
		std::string("test"),			// firstname
		std::string("test"),
		std::string("test")
		);
	 
	mDBInterface->createCharacter(tmpAccount2, characterID2,"mike","doe", 'm',"12");
	 


	return true;
}

bool update()
{
	Packet *mPacket = mPeer->Receive();

	if( mPacket )
	{
		switch( mPacket->data[0] )
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			break;
		case ID_REMOTE_CONNECTION_LOST:
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			std::cout << "ID_REMOTE_NEW_INCOMING_CONNECTION\n";
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			break;
		case ID_NEW_INCOMING_CONNECTION:
			std::cout << "ID_NEW_INCOMING_CONNECTION\n";
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			break;
		case ID_CONNECTION_LOST:
			break;
		case ID_LOGIN_INFO:
			{
				std::cout << "ID_LOGIN_INFO Received.\n";
				unsigned char typeId;
				char username[128], password[128];
				RakNet::BitStream info( mPacket->data, mPacket->length, false ); // The false is for efficiency so we don't make a copy of the passed data
				info.Read( typeId );
				stringCompressor->DecodeString( username, 128, &info );
				stringCompressor->DecodeString( password, 128, &info );
				bool found = false; // If none found then the username is bad.

				// FIXME IMP //
				//for( unsigned int indx = 0; indx < mMasterServer->account_list.size(); indx++ )
				{
					Account *acct = mDBInterface->getAccount( std::string(username) );
					if (acct) // Account exists.
					{
std::cout << "1 ### Main.h\n";
						found = true;

						//if( acct->getPassword() == std::string(password) )
						if (true)
						{
std::cout << "2 ### Main.h\n";
							// Successful password.  Send server list to user.
							RakNet::BitStream output;
							unsigned char typeId = ID_SERVER_LIST;
							int size = mMasterServer->server_list.size();
							output.Write( typeId );
							output.Write( size );
std::cout << "3 ### Main.h\n";							
							for( int indx = 0; indx < mMasterServer->server_list.size(); indx++ )
							{
								stringCompressor->EncodeString( mMasterServer->server_list[indx]->name.c_str(), 128, &output );
								stringCompressor->EncodeString( mMasterServer->server_list[indx]->ip_address.c_str(), 128, &output );
								output.Write( mMasterServer->server_list[indx]->status );
							}
std::cout << "4 ### Main.h\n";

							//std::string session_key;
							//std::ostringstream o;
							//for( int i = 0; i < 64; i++ )
							//{
							//	o << random_range( 0, 9 );
							//	session_key.append( o.str() );
							//}
							//stringCompressor->EncodeString( session_key.c_str(), 64, &output );

							mPeer->Send( &output, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mPacket->systemAddress, false );
std::cout << "5 ### Main.h\n";
						}
						else
						{
std::cout << "6 ### Main.h\n";
							// Invalid password.
							notifyInvalidPassword( mPacket->systemAddress );
std::cout << "7 ### Main.h\n";
						}
					}
				}
				if( !found )
				{
std::cout << "8 ### Main.h\n";
					// Invalid username.
					notifyInvalidPassword( mPacket->systemAddress );
std::cout << "9 ### Main.h\n";
				}
			}
			break;

			/****************************************************************************
			 * Commented out for security reasons... I left them here because they      *
			 * could later be made into methods for internal use by the master server.  *
			 ****************************************************************************/

			//case ID_DB_QUERY_ACCOUNT:
			//	{
			//		RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
			//		unsigned char typeId;
			//		char username[51];
			//		incoming.Read( typeId );
			//		stringCompressor->DecodeString( username, 50, &incoming );
			//		RakNet::BitStream outgoing;
			//		typeId = ID_DB_QUERY_ACCOUNT;
			//		outgoing.Write( typeId );
			//		int rc = sqlite3_exec(
			//		db,
			//		std::string("SELECT * FROM account WHERE username = '" + 
			//		std::string(username) + "';").c_str(),
			//		callback,
			//		0,
			//		&zErrMsg
			//		);
			//		if( g_argc > 0 )
			//		{
			//			outgoing.Write( true );
			//		}
			//		else
			//		{
			//			outgoing.Write( false );
			//		}

			//		mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mPacket->systemAddress, false );
			//	}
			//	break;
			//case ID_DB_ADD_ACCOUNT:
			//	{
			//		RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
			//		unsigned char typeId;
			//		incoming.Read( typeId );
			//		char username[51];
			//		char password[51];
			//		char email[51];
			//		char auth_pass[51];
			//		stringCompressor->DecodeString( username, 50, &incoming );
			//		stringCompressor->DecodeString( password, 50, &incoming );
			//		stringCompressor->DecodeString( email, 50, &incoming );
			//		stringCompressor->DecodeString( auth_pass, 50, &incoming );

			//		if( std::string(auth_pass) == "*7ahdBudfbBWEY76627F#89(#*#&@hgdfsugwqegiHFJKDSGWQgeywqgiegwqegG&2621%^&^!" )
			//		{
			//			rc = sqlite3_exec(
			//			db,
			//			std::string("INSERT INTO account VALUES ('" + std::string(username) + 
			//			"', '" +
			//			std::string(password) + "', '" + std::string(email) + "');").c_str(),
			//			callback,
			//			0,
			//			&zErrMsg
			//			);
			//			if( rc!=SQLITE_OK )
			//			{
			//			  cout << "SQL error: " << zErrMsg;
			//			  sqlite3_free( zErrMsg );
			//			}

			//			typeId = ID_DB_ADD_ACCOUNT;
			//			RakNet::BitStream outgoing;
			//			outgoing.Write( typeId );
			//			outgoing.Write( true );
			//			mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mPacket->systemAddress, false );
			//		}
			//		else
			//		{
			//			// Someone's trying to tamper.  Log and disconnect user.
			//			mPeer->AddToBanList( mPacket->systemAddress.ToString( false ) );
			//			mPeer->CloseConnection( mPacket->systemAddress, true );
			//		}
			//	}
			//	break;
		default:
			std::cout << "Unknown message received.\n";
			unsigned char id;
			RakNet::BitStream bt( mPacket->data, mPacket->length, false );
			bt.Read( id );
			std::cout << (int)id;
			break;
		}

		mPeer->DeallocatePacket( mPacket );
	}
	return true;
}

void shutdown()
{
	/****************
	 * CLEANUP
	 ***************/
	delete mDBInterface;
	RakNetworkFactory::DestroyRakPeerInterface( mPeer );
	delete mMasterServer;
}

} // End master_server namespace

#endif
