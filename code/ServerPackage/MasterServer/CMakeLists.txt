###################################
### Set VC++ warning level to 0 ###
###################################
if(CMAKE_BUILD_TOOL MATCHES "(msdev|devenv|nmake)")
    add_definitions(/W0)
endif()



### INCLUDE DIRECTORIES ###
include_directories(
.
${CMAKE_SOURCE_DIR}/../Dependencies/tinyxml ${CMAKE_SOURCE_DIR}/../Dependencies/ ${CMAKE_SOURCE_DIR}/../Dependencies/raknet/ 
${CMAKE_SOURCE_DIR}/DBInterface/ 
${ODE_INCLUDE_DIRS}
${MYSQL_INCLUDE_DIRS}
${MYSQLPP_INCLUDE_DIRS}
)


### SOURCEFILES - This is a CONSOLE APP ###
add_executable(ura-MasterServer 

# Main #
main.cpp
MasterServer.cpp
Server.cpp

# CLOSE THE BRACE #
)

### LINK LIBRARIES ###
target_link_libraries(
ura-MasterServer
${MATHLIB} 
tinyxml
raknet DBInterface
${ODE_LIBRARIES}
${MYSQL_LIBRARIES} ${MYSQLPP_LIBRARIES} 
)

