/*
Trinity Reign, an open source MMORPG.
Copyright (C)  Trinity Reign Dev Team
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/ 


#ifndef OBJECTMD3ARRAY_H
#define OBJECTMD3ARRAY_H


#include <stdlib.h>
#include "GlobalHeader.h"
#include "ObjectMDxArray.h"



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class ObjectMD3Array : protected ObjectMDxArray<T>
{
public:
	ObjectMD3Array(	unsigned pageCapacity,
					unsigned rowCapacity,
					unsigned columnCapacity,
					bool autoAllocate )
		: ObjectMDxArray<T>(pageCapacity, rowCapacity, columnCapacity, autoAllocate)
	{

	}


	void addPage()
	{
		ObjectMDxArray<T>::addPage();
	}

	void addRow(unsigned page)
	{
		ObjectMDxArray<T>::addRow(page);
	}

	void addColumn(unsigned page, unsigned row, T* type)
	{
		ObjectMDxArray<T>::addColumn(page, row, type);
	}


	////////////////////////////////////////
	/// Get the column pointer and value ///
	////////////////////////////////////////
	T* getColumnPointer(unsigned page, unsigned row, unsigned column)
	{
		return ObjectMDxArray<T>::getColumnPointer(page, row, column);
	}


	T getColumnValue(unsigned page, unsigned row, unsigned column)
	{
		return ObjectMDxArray<T>::getColumnValue(page, row, column);
	}


	////////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns memory itself ///
	////////////////////////////////////////////////////////////////////
	unsigned getColumnCapacity(unsigned page, unsigned row)
	{
		return ObjectMDxArray<T>::getColumnCapacity(page, row);
	}

	unsigned getColumnAllocated(unsigned page, unsigned row)
	{
		return ObjectMDxArray<T>::getColumnAllocated(page, row);
	}

	unsigned getColumnCutOff(unsigned page, unsigned row)
	{
		return ObjectMDxArray<T>::getColumnCutOff(page, row);
	}


	//////////////////////////
	/// Set Column Pointer ///
	//////////////////////////
	void setColumnPointer(unsigned page, unsigned row, T* type)
	{
		ObjectMDxArray<T>::setColumnPointer(page, row, type);
	}

	////////////////////////
	/// Set Column Value ///
	////////////////////////
	void setColumnValue(unsigned page, unsigned row, T type)
	{
		ObjectMDxArray<T>::setColumnValue(page, row, type);
	}
};


#endif
