/*
Trinity Reign, an open source MMORPG.
Copyright (C)  Trinity Reign Dev Team
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/ 


#ifndef OBJECTMD1ARRAY_H
#define OBJECTMD1ARRAY_H


#include <stdlib.h>
#include "GlobalHeader.h"
#include "ObjectMDxArray.h"


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define OBJECTMD1ARRAY_DEFAULT_PAGE	0
#define OBJECTMD1ARRAY_DEFAULT_ROW	0



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class ObjectMD1Array : protected ObjectMDxArray<T>
{
public:
	ObjectMD1Array(unsigned columnCapacity, bool autoAllocate)
		: ObjectMDxArray<T>(1, 1, columnCapacity, autoAllocate)
	{
		///////////////////////////////////////////////////////////////////////
		/// If We don't auto allocate than we allocate a page and row 		///
		/// This allows us to save memory, if we just want to have a big -	///
		///  unallocated container.											///
		///////////////////////////////////////////////////////////////////////
		if (!autoAllocate)
		{
			ObjectMDxArray<T>::addPage();
			ObjectMDxArray<T>::addRow(OBJECTMD1ARRAY_DEFAULT_PAGE);
		}
	}


	void addColumn(T* type)
	{
		ObjectMDxArray<T>::addColumn(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW, type);
	}


	////////////////////////////////////////
	/// Get the column pointer and value ///
	////////////////////////////////////////
	T* getColumnPointer(unsigned column)
	{
		return ObjectMDxArray<T>::getColumnPointer(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW, column);
	}


	T getColumnValue(unsigned column)
	{
		return ObjectMDxArray<T>::getColumnValue(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW, column);
	}


	////////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns memory itself ///
	////////////////////////////////////////////////////////////////////
	unsigned getColumnCapacity()
	{
		return  ObjectMDxArray<T>::getColumnCapacity(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW);
	}

	unsigned getColumnAllocated()
	{
		return ObjectMDxArray<T>::getColumnAllocated(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW);
	}

	unsigned getColumnCutOff()
	{
		return ObjectMDxArray<T>::getColumnCutOff(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW);
	}


	//////////////////////////
	/// Set Column Pointer ///
	//////////////////////////
	void setColumnPointer(T* type)
	{
		ObjectMDxArray<T>::setColumnPointer(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW, type);
	}

	////////////////////////
	/// Set Column Value ///
	////////////////////////
	void setColumnValue(T type)
	{
		ObjectMDxArray<T>::setColumnValue(OBJECTMD1ARRAY_DEFAULT_PAGE, OBJECTMD1ARRAY_DEFAULT_ROW, type);
	}
};


#endif
