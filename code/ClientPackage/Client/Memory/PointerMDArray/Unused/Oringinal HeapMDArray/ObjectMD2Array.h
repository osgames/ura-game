/*
Trinity Reign, an open source MMORPG.
Copyright (C)  Trinity Reign Dev Team
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/ 


#ifndef OBJECTMD2ARRAY_H
#define OBJECTMD2ARRAY_H


#include <stdlib.h>
#include "GlobalHeader.h"
#include "ObjectMDxArray.h"


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define OBJECTMD2ARRAY_DEFAULT_PAGE 0



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class ObjectMD2Array : protected ObjectMDxArray<T>
{
public:
	ObjectMD2Array(unsigned rowCapacity, unsigned columnCapacity, bool autoAllocate)
		: ObjectMDxArray<T>(1, rowCapacity, columnCapacity, autoAllocate)
	{
		///////////////////////////////////////////////////////////////////////
		/// If We don't auto allocate than we allocate a page and row 		///
		/// This allows us to save memory, if we just want to have a big -	///
		///  unallocated container.											///
		///////////////////////////////////////////////////////////////////////
		if (!autoAllocate)
		{
			ObjectMDxArray<T>::addPage();
		}
	}


	void addRow()
	{
		ObjectMDxArray<T>::addRow(OBJECTMD2ARRAY_DEFAULT_PAGE);
	}

	void addColumn(unsigned row, T* type)
	{
		ObjectMDxArray<T>::addColumn(OBJECTMD2ARRAY_DEFAULT_PAGE, row, type);
	}


	////////////////////////////////////////
	/// Get the column pointer and value ///
	////////////////////////////////////////
	T* getColumnPointer(unsigned row, unsigned column)
	{
		return ObjectMDxArray<T>::getColumnPointer(OBJECTMD2ARRAY_DEFAULT_PAGE, row, column);
	}


	T getColumnValue(unsigned row, unsigned column)
	{
		return ObjectMDxArray<T>::getColumnValue(OBJECTMD2ARRAY_DEFAULT_PAGE, row, column);
	}


	////////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns memory itself ///
	////////////////////////////////////////////////////////////////////
	unsigned getColumnCapacity(unsigned row)
	{
		return ObjectMDxArray<T>::getColumnCapacity(OBJECTMD2ARRAY_DEFAULT_PAGE, row);
	}

	unsigned getColumnAllocated(unsigned row)
	{
		return ObjectMDxArray<T>::getColumnAllocated(OBJECTMD2ARRAY_DEFAULT_PAGE, row);
	}

	unsigned getColumnCutOff(unsigned row)
	{
		return ObjectMDxArray<T>::getColumnCutOff(OBJECTMD2ARRAY_DEFAULT_PAGE, row);
	}


	//////////////////////////
	/// Set Column Pointer ///
	//////////////////////////
	void setColumnPointer(unsigned row, T* type)
	{
		ObjectMDxArray<T>::setColumnPointer(OBJECTMD2ARRAY_DEFAULT_PAGE, row, type);
	}

	////////////////////////
	/// Set Column Value ///
	////////////////////////
	void setColumnValue(unsigned row, T type)
	{
		ObjectMDxArray<T>::setColumnValue(OBJECTMD2ARRAY_DEFAULT_PAGE, row, type);
	}
};


#endif
