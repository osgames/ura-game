/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __POINTERARRAY_H__
#define __POINTERARRAY_H__


////////////////////////////////////////////
/// Uncomment this to take off debugging ///
////////////////////////////////////////////
#define POINTERARRAY_DEBUG


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define POINTERARRAY_INDEXSIZE			6

#define POINTERARRAY_INDEX_CAPACITY		0
#define POINTERARRAY_INDEX_ALLOCATED	1
#define POINTERARRAY_INDEX_CUTOFF		2

#define POINTERARRAY_INDEX_TMPLOOP		3
#define POINTERARRAY_INDEX_TEMPINT0		4
#define POINTERARRAY_INDEX_TEMPINT1		5



///////////////////////////////////////////////////////////////
/// WE NEED DIFFERENT CODE FOR C STRINGS AND BUILT IN TYPES	///
/// THIS DEFINE ALLOWS US TO SWITCH AND COPY AND PASTE		///
///////////////////////////////////////////////////////////////
/////#define POINTERARRAY_CSTRING_CODE

///////////////////////////////////////////////////////////
/// WE NEED DIFFERENT CODE FOR BUILT IN TYPES. 			///
/// THIS DEFINE ALLOWS US TO SWITCH AND COPY AND PASTE 	///
///////////////////////////////////////////////////////////
//////#define POINTERARRAY_BUILTINTYPE_CODE



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <class T>
class PointerArray
{
public:
	T** mpub_Array;
	unsigned* mVariableIndex;


	PointerArray(unsigned capacity = 1, bool autoAllocate = false)
	{
//		LOGI("###### PointerArray::PointerArray(unsigned capacity = %i, bool autoAllocate = false)", capacity);
		PointerArray::_ConstructorAllocate(capacity, autoAllocate);
	}

	~PointerArray()
	{
		mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;

//		LOGI("@@@@@@@@@@@@@@@@@@@@@ ~PointerArray()");

		if (mpub_Array)
		{
			for (; mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_ALLOCATED];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				if ( mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]] )
				{
//					LOGI("delete mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]];");

					delete mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]];
					mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]] = NULL;
				}
			}

			delete [] mpub_Array;
			mpub_Array = NULL;
		}


		/// Free the mVariableIndex array ///
		if (mVariableIndex)
		{
			delete [] mVariableIndex;
			mVariableIndex = NULL;
		}
	}



	void _ConstructorAllocate(unsigned capacity, bool autoAllocate)
	{
		mpub_Array = new T*[capacity];
		mVariableIndex = new unsigned[POINTERARRAY_INDEXSIZE];


		/// Set the Array to its capacity and to zero to prevent errors ///
		mVariableIndex[POINTERARRAY_INDEX_CAPACITY]		= capacity;
		mVariableIndex[POINTERARRAY_INDEX_ALLOCATED] 	= 0;
		mVariableIndex[POINTERARRAY_INDEX_CUTOFF] 		= 0;
		mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] 		= 0;
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] 	= 0;
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] 	= 0;


		/// Do We auto allocate the memory? ///
		if (autoAllocate)
		{
			for (; mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < capacity;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				PointerArray::add( new T() );
			}

			///////////////////////////////////////////////////////////////////
			/// After we add the data, we set the new allocation and cutoff ///
			///////////////////////////////////////////////////////////////////
			mVariableIndex[POINTERARRAY_INDEX_CUTOFF] 	= capacity;
		}
	}



	void add(T* type)
	{
		//////////////////////////////////////////////////////////////////////
		/// If the array length >= to the capacity than we need to resize  ///
		//////////////////////////////////////////////////////////////////////
		if ( mVariableIndex[POINTERARRAY_INDEX_ALLOCATED] >=
				mVariableIndex[POINTERARRAY_INDEX_CAPACITY] )
		{
//			LOGI("#@#@#@#@#@#@#@#@#@#@@##@#@# PointerArray array size increased");

			/// Allocate twice the size of the capacity ///
			T** p = new T*[mVariableIndex[POINTERARRAY_INDEX_CAPACITY]*2];
//LOGI("#@#@#@#@ PointerArray 1");
			/// Copy the contents of the old array to the new bigger array ///
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_CAPACITY];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				p[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]] = mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]];
			}
//LOGI("#@#@#@#@ PointerArray 2");
			/// Set the capacity to the now doubled size ///
			mVariableIndex[POINTERARRAY_INDEX_CAPACITY] *= 2;

//LOGI("#@#@#@#@ PointerArray 3");
			/// Free the old arrays memory ///
			delete [] mpub_Array;

			/// Point the old array to the new bigger arrays memory address ///
			mpub_Array = p;
//LOGI("#@#@#@#@ PointerArray 4");
		}


		/// Insert the data into the array ///
		mpub_Array[ mVariableIndex[POINTERARRAY_INDEX_ALLOCATED] ] = type;
//LOGI("#@#@#@#@ PointerArray 5");
		/// Increment the array length since we inserted something in it ///
		mVariableIndex[POINTERARRAY_INDEX_ALLOCATED]++;
//LOGI("#@#@#@#@ PointerArray 6");
	}



	void setCutOff(unsigned size)
	{
		mVariableIndex[POINTERARRAY_INDEX_CUTOFF] = size;
	}


	void setPointer(unsigned column, T* type)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray setPointer(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Add the new column ///
		mpub_Array[column] = type;
	}


	inline unsigned getCapacity() const
	{
		//LOGI("###### capacity = %i",  mVariableIndex[POINTERARRAY_INDEX_CAPACITY]);
		return mVariableIndex[POINTERARRAY_INDEX_CAPACITY];
	}

	inline unsigned getAllocated() const
	{
		return mVariableIndex[POINTERARRAY_INDEX_ALLOCATED];
	}

	inline unsigned getCutOff() const
	{
		//LOGI("###### ROWCUTTOFF = %i",  mVariableIndex[POINTERARRAY_INDEX_CUTOFF]);
		return mVariableIndex[POINTERARRAY_INDEX_CUTOFF];
	}


	T* getArrayPointer()
	{
		return *mpub_Array;
	}

	T* getPointer(unsigned column)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			// FIXME:
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray getByPointer(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Get the columns pointer ///
		return mpub_Array[column];
	}

	T getValue(unsigned column)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			/// FIXME: when the type is char, it wont compile ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray getByValue(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Get the Columns Value ///
		return *(mpub_Array[column]);
	}

	void setValue(unsigned column, T type)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			/// FIXME: when the type is char, it wont compile ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray set(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Add the new column ///
		*(mpub_Array[column]) = type;
	}



///#################################################///
///### THIS CODE IS ONLY USED FOR BUILT IN TYPES ###///
///#################################################///
#ifdef POINTERARRAY_BUILTINTYPE_CODE


	PointerArray(const T* type)
	{
		LOGI("###### PointerArray::PointerArray(const T* type)");

		// Allocate the memory and assign it //
		PointerArray::_ConstructorAllocate(type);
	}

	PointerArray(unsigned capacity, T** type)
	{
		LOGI("###### PointerArray::PointerArray(unsigned capacity = %i, T** type)", capacity);

		// Allocate the memory and assign it by its size //
		PointerArray::_ConstructorAllocate(capacity, true);
		PointerArray::assign(capacity, type);
	}

	PointerArray(unsigned capacity, const T type[])
	{
		LOGI("###### PointerArray::PointerArray(unsigned capacity = %i, const T type[])", capacity);

		// Allocate the memory and assign it by its size //
		PointerArray::_ConstructorAllocate(capacity, true);
		PointerArray::assign(capacity, type);
	}


	void clear()
	{
		mVariableIndex[POINTERARRAY_INDEX_CUTOFF] = 0;
	}


	///////////////////////
	///	Assign the data ///
	///////////////////////
	void assign(unsigned arraySize, const T type[])
	{
LOGI("void PointerArray::ASSIGN(unsigned arraySize, const T type[]");

		/// Store the current strings current Allocation, to know which pointers can change their values ///
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] = PointerArray::getAllocated();


LOGI("arraySize = %i", arraySize);
LOGI("getAllocated = %i", PointerArray::getAllocated());


		/// If the original type size is bigger or equal to the one we want to copy than
		///  everything is fine. Else we have to copy until our old type size is taken up and then
		///  add to it.
		if (mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] >= arraySize)
		{
LOGI("A1");
LOGI("arraySize = %i", arraySize);

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("LOOP");
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]]);
			}
LOGI("A2");
		}
		else	// Our current string doesn't have enough size;
		{
LOGI("B1");
			// Set the current string to the cstring as much as possible using mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]]);
			}
LOGI("B2");
			// If we need to allocate memory, resume at mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] and end at arraySize //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				PointerArray::add( new T(type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]]) );
			}
LOGI("B3");
		}

LOGI("C1");
		/// Set the cutOff point to the size of arraySize ///
		PointerArray::setCutOff(arraySize);
	}


	void assign(unsigned arraySize, T** type)
	{
LOGI("void PointerArray::ASSIGN(unsigned arraySize, T** type");

		/// Store the current strings current Allocation, to know which pointers can change their values ///
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] = PointerArray::getAllocated();


LOGI("arraySize = %i", arraySize);
LOGI("getAllocated = %i", PointerArray::getAllocated());


		/// If the original type size is bigger or equal to the one we want to copy than
		///  everything is fine. Else we have to copy until our old type size is taken up and then
		///  add to it.
		if (mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] >= arraySize)
		{
LOGI("A1");
LOGI("arraySize = %i", arraySize);

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("LOOP");
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], *type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]]);
			}
LOGI("A2");
		}
		else	// Our current string doesn't have enough size;
		{
LOGI("B1");
			// Set the current string to the cstring as much as possible using mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], *type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]]);
			}
LOGI("B2");
			// If we need to allocate memory, resume at mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] and end at arraySize //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				PointerArray::add( new T(*type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]]) );
			}
LOGI("B3");
		}

LOGI("C1");
		/// Set the cutOff point to the size of arraySize ///
		PointerArray::setCutOff(arraySize);
	}


	void appendToCutOff(unsigned arraySize, const T type[])
	{
		LOGI("void appendToCutOff(unsigned arraySize, const T type[])");


		/// We use this for the loops ///
		mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;

		/// Get allocated ///
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] = PointerArray::getAllocated();

		/// Get the cutoff, make sure were not subtracting ///
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] = PointerArray::getCutOff();


///################################################///
///### THIS CODE IS ONLY USED FOR CSTRING TYPES ###///
///################################################///
#ifdef POINTERARRAY_CSTRING_CODE
		if (mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] > 0)
		{
			mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] = PointerArray::getCutOff() - 1;
		}
#endif


		LOGI("cuttOFF = %i", PointerArray::getCutOff() );
		LOGI("arraySize = %i", arraySize);

		/// The new size should be Cuttoff + attached array size ///
		arraySize += mVariableIndex[POINTERARRAY_INDEX_TEMPINT1];



		///########################################################################################///
		///### If the original strings size is bigger or equal to the one we want to copy than	###///
		///###  everything is fine. Else we have to copy until our old string size is taken up	###///
		///###	and then add to it.																###///
		///########################################################################################///
		if (mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] >= arraySize)
		{
			LOGI("###### ENOUGH MEMORY");

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT1];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setvalue");
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] - mVariableIndex[POINTERARRAY_INDEX_TEMPINT1]]);
			}
		}
		else	// Our current string doesn't have enough size;
		{
			LOGI("###### NOT ENOUGH MEMORY");

			///////////////////////////////////////////////////////////////
			/// For the space we already have allocated, set the values ///
			///////////////////////////////////////////////////////////////
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT1];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setValue");
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] - mVariableIndex[POINTERARRAY_INDEX_TEMPINT1]]);
			}


			//////////////////////////////////////////////////////////////////////////////////////
			///	We Don't have enough space to set the values, so we allocate more and set them ///
			//////////////////////////////////////////////////////////////////////////////////////
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("add");
				PointerArray::add(new T(type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] - mVariableIndex[POINTERARRAY_INDEX_TEMPINT1]]));
			}
		}


		/////////////////////////////
		/// Set the CuttOff point ///
		/////////////////////////////
		PointerArray::setCutOff(arraySize);
	}

	void appendToCutOff(unsigned arraySize, T type[])
	{
		LOGI("void appendToCutOff(unsigned arraySize, const T type[])");


		/// We use this for the loops ///
		mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;

		/// Get allocated ///
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] = PointerArray::getAllocated();

		/// Get the cutoff, make sure were not subtracting ///
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] = PointerArray::getCutOff();


///################################################///
///### THIS CODE IS ONLY USED FOR CSTRING TYPES ###///
///################################################///
#ifdef POINTERARRAY_CSTRING_CODE
		if (mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] > 0)
		{
			mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] = PointerArray::getCutOff() - 1;
		}
#endif


		LOGI("cuttOFF = %i", PointerArray::getCutOff() );
		LOGI("arraySize = %i", arraySize);

		/// The new size should be Cuttoff + attached array size ///
		arraySize += mVariableIndex[POINTERARRAY_INDEX_TEMPINT1];



		///########################################################################################///
		///### If the original strings size is bigger or equal to the one we want to copy than	###///
		///###  everything is fine. Else we have to copy until our old string size is taken up	###///
		///###	and then add to it.																###///
		///########################################################################################///
		if (mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] >= arraySize)
		{
			LOGI("###### ENOUGH MEMORY");

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT1];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setvalue");
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] - mVariableIndex[POINTERARRAY_INDEX_TEMPINT1]]);
			}
		}
		else	// Our current string doesn't have enough size;
		{
			LOGI("###### NOT ENOUGH MEMORY");

			///////////////////////////////////////////////////////////////
			/// For the space we already have allocated, set the values ///
			///////////////////////////////////////////////////////////////
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT1];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setValue");
				PointerArray::setValue(mVariableIndex[POINTERARRAY_INDEX_TMPLOOP], type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] - mVariableIndex[POINTERARRAY_INDEX_TEMPINT1]]);
			}


			//////////////////////////////////////////////////////////////////////////////////////
			///	We Don't have enough space to set the values, so we allocate more and set them ///
			//////////////////////////////////////////////////////////////////////////////////////
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = mVariableIndex[POINTERARRAY_INDEX_TEMPINT0];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("add");
				PointerArray::add(new T(type[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] - mVariableIndex[POINTERARRAY_INDEX_TEMPINT1]]));
			}
		}


		/////////////////////////////
		/// Set the CuttOff point ///
		/////////////////////////////
		PointerArray::setCutOff(arraySize);
	}


/// END OF BUILTINTYPE_CODE DEFINE ///
#endif
};



#endif




