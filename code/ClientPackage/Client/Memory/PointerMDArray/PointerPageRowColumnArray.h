/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __POINTERPAGEROWCOLUMNARRAY_H__
#define __POINTERPAGEROWCOLUMNARRAY_H__

 
#include "PointerMDxArray.h"


//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class PointerPageRowColumnArray : protected PointerMDxArray<T>
{
public:
	PointerPageRowColumnArray(unsigned pageCapacity,
					unsigned rowCapacity,
					unsigned columnCapacity,
					bool autoAllocate)
		: PointerMDxArray<T>(pageCapacity, rowCapacity, columnCapacity, autoAllocate)
	{

	}


	void addPage()
	{
		PointerMDxArray<T>::addPage();
	}

	void addRow(unsigned page)
	{
		PointerMDxArray<T>::addRow(page);
	}

	void addColumn(unsigned page, unsigned row, T* type)
	{
		PointerMDxArray<T>::addColumn(page, row, type);
	}
	 
	 
	///////////////////////////////////////////////////
	/// GET: Functions that deal with just the Page ///
	///////////////////////////////////////////////////
	unsigned getPageCapacity() const
	{
		return PointerMDxArray<T>::getPageCapacity();
	}

	unsigned getPageAllocated() const
	{
		return PointerMDxArray<T>::getPageAllocated();
	}

	unsigned getPageCutOff() const
	{
		return PointerMDxArray<T>::getPageCutOff();
	}

	 
	///////////////////////////////////////////////////
	/// GET: Functions that deal with just the Rows ///
	///////////////////////////////////////////////////
	unsigned getRowCapacity(unsigned page) const
	{
		return PointerMDxArray<T>::getRowCapacity(page);
	}

	unsigned getRowAllocated(unsigned page) const
	{
		return PointerMDxArray<T>::getRowAllocated(page);
	}

	unsigned getRowCutOff(unsigned page) const
	{
		return PointerMDxArray<T>::getRowCutOff(page);
	}


	//////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns ///
	//////////////////////////////////////////////////////
	T* getColumnPointer(unsigned page, unsigned row, unsigned column) const
	{
		return PointerMDxArray<T>::getColumnPointer(page, row, column);
	}

	T getColumnValue(unsigned page, unsigned row, unsigned column) const
	{
		return PointerMDxArray<T>::getColumnValue(page, row, column);
	}

	unsigned getColumnCapacity(unsigned page, unsigned row) const
	{
		return PointerMDxArray<T>::getColumnCapacity(page, row);
	}

	unsigned getColumnAllocated(unsigned page, unsigned row) const
	{
		return PointerMDxArray<T>::getColumnAllocated(page, row);
	}

	unsigned getColumnCutOff(unsigned page, unsigned row) const
	{
		return PointerMDxArray<T>::getColumnCutOff(page, row);
	}
	   
	   
	//////////////////////////////////////////////////////
	/// SET: Functions that deal with just the Columns ///
	//////////////////////////////////////////////////////
	void setColumnPointer(unsigned page, unsigned row, unsigned column, T* type)
	{
		PointerMDxArray<T>::setColumnPointer(page, row, column, type);
	}
 
	void setColumnValue(unsigned page, unsigned row, unsigned column, T type)
	{
		PointerMDxArray<T>::setColumnValue(page, row, column, type);
	}
};


#endif
