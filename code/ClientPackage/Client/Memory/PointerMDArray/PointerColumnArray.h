/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

  
#ifndef __POINTERCOLUMNARRAY_H__
#define __POINTERCOLUMNARRAY_H__


#include "PointerMDxArray.h"


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define POINTERCOLUMNARRAY_DEFAULT_PAGE		0
#define POINTERCOLUMNARRAY_DEFAULT_ROW		0



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class PointerColumnArray : protected PointerMDxArray<T>
{
public:
	PointerColumnArray(unsigned columnCapacity, bool autoAllocate)
		: PointerMDxArray<T>(1, 1, columnCapacity, autoAllocate)
	{
		///////////////////////////////////////////////////////////////////////
		/// If We don't auto allocate than we allocate a page and row 		///
		/// This allows us to save memory, if we just want to have a big -	///
		///  unallocated container.											///
		///////////////////////////////////////////////////////////////////////
		if (!autoAllocate)
		{
			PointerMDxArray<T>::addPage();
			PointerMDxArray<T>::addRow(POINTERCOLUMNARRAY_DEFAULT_PAGE);
		}
	}

	
	void addColumn(T* type)
	{
		PointerMDxArray<T>::addColumn(POINTERCOLUMNARRAY_DEFAULT_PAGE, POINTERCOLUMNARRAY_DEFAULT_ROW, type);
	}


	//////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns ///
	//////////////////////////////////////////////////////
	T* getColumnPointer(unsigned column) const
	{
		return PointerMDxArray<T>::getColumnPointer(POINTERCOLUMNARRAY_DEFAULT_PAGE, POINTERCOLUMNARRAY_DEFAULT_ROW, column);
	}

	T getColumnValue(unsigned column) const
	{
		return PointerMDxArray<T>::getColumnValue(POINTERCOLUMNARRAY_DEFAULT_PAGE, POINTERCOLUMNARRAY_DEFAULT_ROW, column);
	}

	unsigned getColumnCapacity() const
	{
		return  PointerMDxArray<T>::getColumnCapacity(POINTERCOLUMNARRAY_DEFAULT_PAGE, POINTERCOLUMNARRAY_DEFAULT_ROW);
	}

	unsigned getColumnAllocated() const
	{
		return PointerMDxArray<T>::getColumnAllocated(POINTERCOLUMNARRAY_DEFAULT_PAGE, POINTERCOLUMNARRAY_DEFAULT_ROW);
	}

	unsigned getColumnCutOff() const
	{
		return PointerMDxArray<T>::getColumnCutOff(POINTERCOLUMNARRAY_DEFAULT_PAGE, POINTERCOLUMNARRAY_DEFAULT_ROW);
	}
	    

	//////////////////////////////////////////////////////
	/// SET: Functions that deal with just the Columns ///
	//////////////////////////////////////////////////////
	void setColumnPointer(unsigned column, T* type)
	{
		PointerMDxArray<T>::setColumnPointer(
			POINTERCOLUMNARRAY_DEFAULT_PAGE,
			POINTERCOLUMNARRAY_DEFAULT_ROW,
			column,
			type);
	}

	void setColumnValue(unsigned column, T type)
	{
		PointerMDxArray<T>::setColumnValue(
			POINTERCOLUMNARRAY_DEFAULT_PAGE,
			POINTERCOLUMNARRAY_DEFAULT_ROW,
			column,
			type);
	}
};


#endif
