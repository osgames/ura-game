/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __POINTERARRAY_H__
#define __POINTERARRAY_H__


////////////////////////////////////////////
/// Uncomment this to take off debugging ///
////////////////////////////////////////////
#define POINTERARRAY_DEBUG


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define POINTERARRAY_INDEXSIZE			6

#define POINTERARRAY_INDEX_CAPACITY		0
#define POINTERARRAY_INDEX_ALLOCATED	1
#define POINTERARRAY_INDEX_CUTOFF		2

#define POINTERARRAY_INDEX_TMPLOOP		3
#define POINTERARRAY_INDEX_TEMPINT0		4
#define POINTERARRAY_INDEX_TEMPINT1		5



///////////////////////////////////////////////////////////////
/// WE NEED DIFFERENT CODE FOR C STRINGS AND BUILT IN TYPES	///
/// THIS DEFINE ALLOWS US TO SWITCH AND COPY AND PASTE		///
///////////////////////////////////////////////////////////////
/////#define POINTERARRAY_CSTRING_CODE

///////////////////////////////////////////////////////////
/// WE NEED DIFFERENT CODE FOR BUILT IN TYPES. 			///
/// THIS DEFINE ALLOWS US TO SWITCH AND COPY AND PASTE 	///
///////////////////////////////////////////////////////////
//////#define POINTERARRAY_BUILTINTYPE_CODE



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <class T>
class PointerArray
{
public:
	T** mpub_Array;
	unsigned* mVariableIndex;


	PointerArray(unsigned capacity = 1, bool autoAllocate = false)
	{
//		LOGI("###### PointerArray::PointerArray(unsigned capacity = %i, bool autoAllocate = false)", capacity);
		PointerArray::_ConstructorAllocate(capacity, autoAllocate);
	}

	~PointerArray()
	{
		mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;

//		LOGI("@@@@@@@@@@@@@@@@@@@@@ ~PointerArray()");

		if (mpub_Array)
		{
			for (; mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_ALLOCATED];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				if ( mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]] )
				{
//					LOGI("delete mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]];");

					delete mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]];
					mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]] = NULL;
				}
			}

			delete [] mpub_Array;
			mpub_Array = NULL;
		}


		/// Free the mVariableIndex array ///
		if (mVariableIndex)
		{
			delete [] mVariableIndex;
			mVariableIndex = NULL;
		}
	}



	void _ConstructorAllocate(unsigned capacity, bool autoAllocate)
	{
		mpub_Array = new T*[capacity];
		mVariableIndex = new unsigned[POINTERARRAY_INDEXSIZE];


		/// Set the Array to its capacity and to zero to prevent errors ///
		mVariableIndex[POINTERARRAY_INDEX_CAPACITY]		= capacity;
		mVariableIndex[POINTERARRAY_INDEX_ALLOCATED] 	= 0;
		mVariableIndex[POINTERARRAY_INDEX_CUTOFF] 		= 0;
		mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] 		= 0;
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT0] 	= 0;
		mVariableIndex[POINTERARRAY_INDEX_TEMPINT1] 	= 0;


		/// Do We auto allocate the memory? ///
		if (autoAllocate)
		{
			for (; mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < capacity;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				PointerArray::add( new T() );
			}

			///////////////////////////////////////////////////////////////////
			/// After we add the data, we set the new allocation and cutoff ///
			///////////////////////////////////////////////////////////////////
			mVariableIndex[POINTERARRAY_INDEX_CUTOFF] 	= capacity;
		}
	}



	void add(T* type)
	{
		//////////////////////////////////////////////////////////////////////
		/// If the array length >= to the capacity than we need to resize  ///
		//////////////////////////////////////////////////////////////////////
		if ( mVariableIndex[POINTERARRAY_INDEX_ALLOCATED] >=
				mVariableIndex[POINTERARRAY_INDEX_CAPACITY] )
		{
//			LOGI("#@#@#@#@#@#@#@#@#@#@@##@#@# PointerArray array size increased");

			/// Allocate twice the size of the capacity ///
			T** p = new T*[mVariableIndex[POINTERARRAY_INDEX_CAPACITY]*2];
//LOGI("#@#@#@#@ PointerArray 1");
			/// Copy the contents of the old array to the new bigger array ///
			for (mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP] < mVariableIndex[POINTERARRAY_INDEX_CAPACITY];
					mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]++)
			{
				p[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]] = mpub_Array[mVariableIndex[POINTERARRAY_INDEX_TMPLOOP]];
			}
//LOGI("#@#@#@#@ PointerArray 2");
			/// Set the capacity to the now doubled size ///
			mVariableIndex[POINTERARRAY_INDEX_CAPACITY] *= 2;

//LOGI("#@#@#@#@ PointerArray 3");
			/// Free the old arrays memory ///
			delete [] mpub_Array;

			/// Point the old array to the new bigger arrays memory address ///
			mpub_Array = p;
//LOGI("#@#@#@#@ PointerArray 4");
		}


		/// Insert the data into the array ///
		mpub_Array[ mVariableIndex[POINTERARRAY_INDEX_ALLOCATED] ] = type;
//LOGI("#@#@#@#@ PointerArray 5");
		/// Increment the array length since we inserted something in it ///
		mVariableIndex[POINTERARRAY_INDEX_ALLOCATED]++;
//LOGI("#@#@#@#@ PointerArray 6");
	}



	void setCutOff(unsigned size)
	{
		mVariableIndex[POINTERARRAY_INDEX_CUTOFF] = size;
	}


	void setPointer(unsigned column, T* type)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray setPointer(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Add the new column ///
		mpub_Array[column] = type;
	}


	inline unsigned getCapacity() const
	{
		//LOGI("###### capacity = %i",  mVariableIndex[POINTERARRAY_INDEX_CAPACITY]);
		return mVariableIndex[POINTERARRAY_INDEX_CAPACITY];
	}

	inline unsigned getAllocated() const
	{
		return mVariableIndex[POINTERARRAY_INDEX_ALLOCATED];
	}

	inline unsigned getCutOff() const
	{
		//LOGI("###### ROWCUTTOFF = %i",  mVariableIndex[POINTERARRAY_INDEX_CUTOFF]);
		return mVariableIndex[POINTERARRAY_INDEX_CUTOFF];
	}


	//T* getArrayPointer() const
	//{
	//	return *mpub_Array;
	//}

	T* getPointer(unsigned column) const
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			// FIXME:
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray getByPointer(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Get the columns pointer ///
		return mpub_Array[column];
	}

	T getValue(unsigned column) const
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			/// FIXME: when the type is char, it wont compile ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray getByValue(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Get the Columns Value ///
		return *(mpub_Array[column]);
	}

	void setValue(unsigned column, T type)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef POINTERARRAY_DEBUG
			/// Check if were out of column bounds ///
			/// FIXME: when the type is char, it wont compile ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: PointerArray set(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Add the new column ///
		*(mpub_Array[column]) = type;
	}
};



#endif




