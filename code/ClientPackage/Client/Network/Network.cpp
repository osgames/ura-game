/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Network.h"
#include <fstream>
#include <Ogre.h>
#include <BitStream.h>
#include <StringCompressor.h>

#include "NetworkStructures.h"
#include "MovementDirection.h"
#include "ClientStateManager.h"
#include "Game.h"
#include "MainMenu.h"		// Used to save the username and password upon connection;
#include "SelfPlayerCharacter.h"
#include "OtherPlayerCharacter.h"
#include "ClientGlobals.h"
//#include "Inventory.h"

//#define HEAVY_LOGGING

using namespace std;
 



template<> Network* Ogre::Singleton<Network>::msSingleton = 0;
Network* const Network::getSingletonPtr(void)
{
	return msSingleton;
}



Network::Network()
{
	connected = false;
	logged_in = false;
	bCancelMasterServerConnect = false;
	bConnectingToMasterServer = false;
	bQuitting = false;
	login_username = "";
	login_password = "";
	master_server_ip = "";
	mPeer = RakNetworkFactory::GetRakPeerInterface();
	mPeer->Startup( 2, 0, &SocketDescriptor(), 1 );
	Ogre::Root::getSingleton().addFrameListener( this );
	mTimer = new Ogre::Timer();
	mConnectionTimer = new Ogre::Timer();
	mSilenceTimer = new Ogre::Timer();
	Ogre::LogManager::getSingleton().logMessage( "Network services started" );
}

Network::~Network()
{
	delete mSilenceTimer;
	delete mConnectionTimer;
	delete mTimer;

	RakNetworkFactory::DestroyRakPeerInterface( mPeer );

	for( unsigned int indx = 0; indx < server_list.size(); indx++ )
	{
		delete server_list[indx];
	}
	server_list.clear();

	for( unsigned int indx = 0; indx < character_list.size(); indx++ )
	{
		delete character_list[indx];
	}
	character_list.clear();
}

bool Network::isConnected()
{
	return connected;
}

void Network::connectToMasterServer()
{
	if( connected && !bCancelMasterServerConnect )
	{
		sendLoginInfo();
	}
	else if( !connected )
	{
		mConnectionTimer->reset();
		bCancelMasterServerConnect = false;
		bConnectingToMasterServer = true;
		string ip;
		ifstream file;
		file.open( "master_server.ip" );
		if( !file )
		{
			Ogre::LogManager::getSingleton().logMessage( "ERROR: Unable to open file master_server.ip" );
			return;
		}
		file >> ip;
		file.close();
		mPeer->Ping(ip.c_str(), DEF_CLIENTGLOBALS_MASTERSERVERPORT, true);
		Ogre::LogManager::getSingleton().logMessage( "Pinging master server (" + std::string(ip.c_str()) + ")..." );
	}
	else
	{
		bCancelMasterServerConnect = false;
	}
}

void Network::realConnectMasterServer( std::string server_name )
{
	mPeer->Connect( server_name.c_str(), DEF_CLIENTGLOBALS_MASTERSERVERPORT, 0, 0 );
	connect_notified = false;
	Ogre::LogManager::getSingleton().logMessage( "Connecting to master server..." );
}

void Network::connectToServer( std::string _ip )
{
	mPeer->Connect( _ip.c_str(), DEF_CLIENTGLOBALS_SERVERPORT, 0, 0 );
}

void Network::disconnect()
{
	clientQuit( true );
	logged_in = false;
	connected = false;

	mPeer->CloseConnection( mPeer->GetSystemAddressFromIndex( 0 ), true );
	mPeer->CloseConnection( mPeer->GetSystemAddressFromIndex( 1 ), true );

	clientQuit( false );
}

void Network::sendLoginInfo()
{
	if( login_username.compare( "" ) == 0 || login_password.compare( "" ) == 0 )
	{
	}
	else
	{
		unsigned char typeId = ID_LOGIN_INFO;
		RakNet::BitStream info;
		info.Write( typeId );
		stringCompressor->EncodeString( login_username.c_str(), 128, &info );
		stringCompressor->EncodeString( login_password.c_str(), 128, &info );

		mPeer->Send( &info, HIGH_PRIORITY, RELIABLE_ORDERED, 0, mPeer->GetSystemAddressFromIndex( 0 ), false );
	}
}

void Network::insert_login_info( std::string username, std::string password )
{
	login_username = username;
	login_password = password;
}

bool Network::frameEnded( const Ogre::FrameEvent &evt )
{
	if( bConnectingToMasterServer && !bCancelMasterServerConnect && (mConnectionTimer->getMilliseconds() > DEF_CLIENTGLOBALS_CONNECTION_TIMEOUT) )
	{
		bConnectingToMasterServer = false;
		bCancelMasterServerConnect = true;
		MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
        if( menu )
            menu->notify( "Timeout when attempting to connect to master server." );
	}

	if( ClientStateManager::getSingletonPtr()->getCurrentState() == AS_GAME )
	{
		if( mTimer->getMilliseconds() > DEF_CLIENTGLOBALS_MILLISECONDS_PER_SNAPSHOT ) // Value of 500 here means 2 snapshots per second.
		{
			RakNet::BitStream out;
			unsigned char typeId = ID_REQUEST_FULL_SNAPSHOT;
			out.Write( typeId );
			mPeer->Send( &out, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );

			mTimer->reset();
		}
	}

	return true;
}

bool Network::frameStarted( const Ogre::FrameEvent &evt )
{
	mPacket = mPeer->Receive();

	if( mPacket )
	{
		switch( mPacket->data[0] )
		{
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			{
				std::string ip = master_server_ip;
				ip.append(DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING);
				if( mPacket->systemAddress == game_server_address )
				{
					// Only disconnect if in game.
					Ogre::LogManager::getSingleton().logMessage( "Remote game server disconnection occured." );
					if( ClientStateManager::getSingleton().getCurrentState() == AS_GAME )
					{
					    ClientStateManager::getSingleton().setState( AS_MAIN_MENU );
					}
                    MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                    if( menu )
                        menu->notify( "Remote game server disconnection occured." );
				}
				else if( strcmp( mPacket->systemAddress.ToString(), ip.c_str() ) == 0 )
				{
					// Only disconnect if not in game.
                    Ogre::LogManager::getSingleton().logMessage( "Remote master server disconnection occured." );
                    MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                    if( menu )
                        menu->notify( "Remote master server disconnection occured." );
				}
				else
				{
					// Has been tampered with.
				}
			}
			break;
		case ID_REMOTE_CONNECTION_LOST:
			{
				std::string ip = master_server_ip;
				ip.append(DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING);
				if( !bQuitting && (mPacket->systemAddress == game_server_address) )
				{
					// Only disconnect if in game.
					Ogre::LogManager::getSingleton().logMessage( "Remote connection to game server lost." );
					if( ClientStateManager::getSingleton().getCurrentState() == AS_GAME )
					{
					    ClientStateManager::getSingleton().setState( AS_MAIN_MENU );
					}
                    MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                    if( menu )
                        menu->notify( "Remote connection to game server lost." );
				}
				else if( !bQuitting && (strcmp( mPacket->systemAddress.ToString(), ip.c_str() ) == 0) )
				{
					// Only disconnect if not in game.
					Ogre::LogManager::getSingleton().logMessage( "Remote connection to master server lost." );
                    MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                    if( menu )
                        menu->notify( "Remote connection to master server lost." );
				}
				else
				{
					// Has been tampered with.
				}
			}
			break;
		case ID_REMOTE_NEW_INCOMING_CONNECTION:
			break;
		case ID_INCOMPATIBLE_PROTOCOL_VERSION:
		{
				Ogre::LogManager::getSingleton().logMessage( "Error Connecting: Incompatible RakNet protocol version." );
                MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                if( menu )
                    menu->notify( "Incompatible RakNet protocol version." );
		}
			break;
		case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				std::string ip = master_server_ip;
				ip.append(DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING);
				if( strcmp( mPacket->systemAddress.ToString(), ip.c_str() ) == 0 )
				{
					// Successful master server connect.
					connected = true;

					Ogre::LogManager::getSingleton().logMessage( "Master server connected." );
				}
				else
				{
					// Successful server connect.
					game_server_address = mPacket->systemAddress;
					unsigned char Id = ID_CHARACTER_LIST_REQUEST;
					RakNet::BitStream out;
					out.Write( Id );
					stringCompressor->EncodeString( login_username.c_str(), 128, &out );
					mPeer->Send( &out, HIGH_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );
				}
			}
			break;
		case ID_NEW_INCOMING_CONNECTION:
			break;
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			break;
		case ID_DISCONNECTION_NOTIFICATION:
			{
				if( mSilenceTimer->getMilliseconds() > 1000 ) // Give it 1000 ms from a client d/c to flush the disconnect message out of the system.
				{
					std::string ip = master_server_ip;
					ip.append(DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING);
					if( !bQuitting && (mPacket->systemAddress == game_server_address) )
					{
						// Only disconnect if in game.
						Ogre::LogManager::getSingleton().logMessage( "Disconnection from game server occured." );
						if( ClientStateManager::getSingleton().getCurrentState() == AS_GAME )
						{
							ClientStateManager::getSingleton().setState( AS_MAIN_MENU );
						}
						MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
						if( menu )
							menu->notify( "Disconnection from game server occured." );
					}
					else if( !bQuitting && (strcmp( mPacket->systemAddress.ToString(), ip.c_str() ) == 0) )
					{
						// Only disconnect if not in game.
						Ogre::LogManager::getSingleton().logMessage( "Disconnection from master server occured." );
						MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
						if( menu )
							menu->notify( "Disconnection from master server occured." );
					}
					else
					{
						// Has been tampered with.
					}
				}
			}
			break;
		case ID_CONNECTION_LOST:
            {
                std::string ip = master_server_ip;
				ip.append(DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING);
				if( connected && mPacket->systemAddress == game_server_address )
				{
					// Only disconnect if in game.
					Ogre::LogManager::getSingleton().logMessage( "Connection to game server lost." );
					if( ClientStateManager::getSingleton().getCurrentState() == AS_GAME )
					{
					    ClientStateManager::getSingleton().setState( AS_MAIN_MENU );
					}
                    MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                    if( menu )
                        menu->notify( "Connection to game server lost." );
				}
				else if( connected && strcmp( mPacket->systemAddress.ToString(), ip.c_str() ) == 0 )
				{
					// Only display notification if not in game.
					Ogre::LogManager::getSingleton().logMessage( "Connection to master server lost." );
                    MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
                    if( menu )
                        menu->notify( "Connection to master server lost." );
				}
				else
				{
					// Has been tampered with.
				}
            }
			break;
		case ID_SERVER_LIST:
			parseServerList();
			break;
		case ID_CHARACTER_LIST:
			{
				// With this "if" check we can make sure we don't call this function multiple times;
				if ( ClientStateManager::getSingletonPtr()->getCurrentState() == AS_CHARACTER_MENU )
				{
					break;
				}

				// Server has sent the requested character list.
				parseCharacterList();
				ClientStateManager::getSingletonPtr()->setState( AS_CHARACTER_MENU );
			}
			break;
		case ID_JOIN_WORLD_ACCEPTED:
			{
				// With this "if" check we can make sure we don't call this function multiple times;
				if ( ClientStateManager::getSingletonPtr()->getCurrentState() == AS_GAME )
				{
					break;
				}

				ClientStateManager::getSingletonPtr()->setState( AS_GAME );
			}
			break;
		case ID_UPDATE_POSITION: // Updates the player's position only (not other characters).
			{
				float pos_x = 0, pos_y = 0, pos_z = 0;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( pos_x );
				incoming.Read( pos_y );
				incoming.Read( pos_z );
				Ogre::Vector3 TempPosition( pos_x, pos_y, pos_z );
				if( ClientStateManager::getSingletonPtr()->getCurrentState() == AS_GAME )
				{
					Game *game = ClientStateManager::getSingletonPtr()->getGame();
					if( game )
					{
						game->getSelfPlayerCharacter()->setPosition( TempPosition );
						game->getSelfPlayerCharacter()->update( 0.0 ); // Force update so that it renders.
					}
				}
			}
			break;
		case ID_UPDATE_ORIENTATION:
			{
				char name[64];
				float w, x, y, z;
				unsigned char Id;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( Id );
				stringCompressor->DecodeString( name, 64, &incoming );
				incoming.Read( w );
				incoming.Read( x );
				incoming.Read( y );
				incoming.Read( z );

				Game *game = ClientStateManager::getSingletonPtr()->getGame();
				if( game )
				{
					OtherPlayerCharacter* const otherPlayer = game->getOtherPlayerCharacter( std::string(name) );
					if (otherPlayer)
					{
						Ogre::Quaternion orientation;
						orientation.w = w;
						orientation.x = x;
						orientation.y = y;
						orientation.z = z;
						otherPlayer->setOrientation(orientation);
					}
				}
			}
			break;
		case ID_FULL_SNAPSHOT: // Only called when all surrounding entity positions are needed.
			{
				Ogre::LogManager *log = Ogre::LogManager::getSingletonPtr();
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );

				#ifdef HEAVY_LOGGING
				log->logMessage( "RECEIVE: Incoming ID_FULL_SNAPSHOT." );
				#endif

				unsigned char typeId;
				incoming.Read( typeId );

				int num_of_players;
				incoming.Read( num_of_players );

				#ifdef HEAVY_LOGGING
				log->logMessage( "RECEIVE: num_of_players: " + Ogre::StringConverter::toString(num_of_players) );
				#endif

				emptyPlayerList();
				for( int indx = 0; indx < num_of_players; indx++ )
				{
					Player *player = new Player();
					player_list.push_back( player );
					char name[128];
					stringCompressor->DecodeString( name, 128, &incoming );
					player->name = std::string( name );

					#ifdef HEAVY_LOGGING
					log->logMessage( "RECEIVE: player->name: " + player->name );
					#endif

					float pos_x, pos_y, pos_z, orient_w, orient_x, orient_y, orient_z;
					incoming.Read( pos_x );
					incoming.Read( pos_y );
					incoming.Read( pos_z );
					incoming.Read( orient_w );
					incoming.Read( orient_x );
					incoming.Read( orient_y );
					incoming.Read( orient_z );

					#ifdef HEAVY_LOGGING
					log->logMessage( "RECEIVE: pos_x: " + Ogre::StringConverter::toString(pos_x) );
					log->logMessage( "RECEIVE: pos_y: " + Ogre::StringConverter::toString(pos_y) );
					log->logMessage( "RECEIVE: pos_z: " + Ogre::StringConverter::toString(pos_z) );
					log->logMessage( "RECEIVE: orient_w: " + Ogre::StringConverter::toString(orient_w) );
					log->logMessage( "RECEIVE: orient_x: " + Ogre::StringConverter::toString(orient_x) );
					log->logMessage( "RECEIVE: orient_y: " + Ogre::StringConverter::toString(orient_y) );
					log->logMessage( "RECEIVE: orient_z: " + Ogre::StringConverter::toString(orient_z) );
					#endif

					//bool p_character;
					//incoming.Read( p_character ); // Currently not working properly.

					player->position.x = pos_x;
					player->position.y = pos_y;
					player->position.z = pos_z;

					player->orientation = Ogre::Quaternion( orient_w, orient_x, orient_y, orient_z );

					//player->player_character = p_character;

					//Ogre::SceneNode *sn = Ogre::Root::getSingleton().getSceneManager( "sceneManager" )->getRootSceneNode()->createChildSceneNode();
					//sn->yaw( Ogre::Degree( yaw ) );
					//player->orientation = sn->getOrientation();
					//Ogre::Root::getSingleton().getSceneManager( "sceneManager" )->destroySceneNode( sn->getName() );

					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( p_character ) );

					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( pos_x ) );
					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( pos_y ) );
					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( pos_z ) );

					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( yaw ) );
				}

				if( ClientStateManager::getSingletonPtr()->getCurrentState() != AS_GAME )
				{
					break;
				}

				Game *game = ClientStateManager::getSingletonPtr()->getGame();
				if( game != NULL )
				{
					//game->fullUpdateAllPlayers();
					game->updateAllPlayers();
				}

				// Now read in NPC data.
				int tmp_dlt;
				char model[128];
				char greeting_text[256];
				char name[128];
				float pos_x, pos_y, pos_z;
				incoming.Read( tmp_dlt ); // Test value is always 1 for now.
				incoming.Read( pos_x );
				incoming.Read( pos_y );
				incoming.Read( pos_z );
				stringCompressor->DecodeString( model, 128, &incoming );
				stringCompressor->DecodeString( greeting_text, 256, &incoming );
				stringCompressor->DecodeString( name, 128, &incoming );

				NonePlayerStaticCharacter* const npc = 
					game->addNonePlayerStaticCharacter( std::string(name), true, Ogre::Vector3( pos_x, pos_y, pos_z ) );
				//npc->setGreetingText( string(greeting_text) );
			}
			break;
		case ID_PONG:
			Ogre::LogManager::getSingleton().logMessage( "Master server responded to ping." );
			if( !logged_in )
				realConnectMasterServer( mPacket->systemAddress.ToString( false ) );
			master_server_ip = std::string( mPacket->systemAddress.ToString( false ) );
			break;
		case ID_CHAT_SAY:
			{
				unsigned char typeId;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( typeId );
				char msg[128];
				char discard[128];
				char send_player[128];
				stringCompressor->DecodeString( msg, 128, &incoming );
				stringCompressor->DecodeString( discard, 128, &incoming );
				stringCompressor->DecodeString( send_player, 128, &incoming );

				Game *game = ClientStateManager::getSingletonPtr()->getGame();
				if( game )
				{
					game->receive_say_chat( std::string(msg), std::string(send_player) );
				}
			}
			break;
		case ID_INCOMING_NPC_INTERACTION: // For the time, the NPC interaction will only be able to send text, but we'll beef that up later.
			{
				/********************************************************
				unsigned char typeId;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( typeId );
				char msg[1024];
				char npc_name[128];
				stringCompressor->DecodeString( npc_name, 128, &incoming );
				stringCompressor->DecodeString( msg, 1024, &incoming );
				std::string text( msg );
				std::string name( npc_name );
				Ogre::LogManager::getSingleton().logMessage( std::string("Incoming NPC interaction (") + npc_name + std::string("): ") + text );

				Game* const game = ClientStateManager::getSingletonPtr()->getGame();
				NonePlayerCharacter* npc = 0;

				if(game)
				{
					npc = game->getNPC(name);

					if (npc)
					{
						npc->setGreetingText(text);
					}
				}
				******************************************************************/
			}
			break;
		case ID_CHARACTER_MOVEMENT_CHANGE:
			{
				unsigned char typeId;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( typeId );
				char ch_name[64];
				stringCompressor->DecodeString( ch_name, 64, &incoming );
				short movement;
				incoming.Read( movement );
				parseMovementChange( std::string(ch_name), movement );
			}
			break;
		case ID_INVALID_PASSWORD:
			{
				// Make sure it's coming from the master server.
				std::string ip = master_server_ip;
				ip.append(DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING);
				if( strcmp( mPacket->systemAddress.ToString(), ip.c_str() ) == 0 )
				{
					disconnect();
					cancelMasterServerConnect();
					MainMenu *menu = ClientStateManager::getSingleton().getMainMenu();
					if( menu )
						menu->notify( "Invalid username or password." );
				}
			}
			break;
		case ID_INVENTORY_FULL_SNAPSHOT:
			{
				unsigned char typeId;
				RakNet::BitStream incoming( mPacket->data, mPacket->length, false );
				incoming.Read( typeId );
				int size;
				incoming.Read( size );

				if( ClientStateManager::getSingletonPtr()->getCurrentState() != AS_GAME )
				{
					Ogre::LogManager::getSingleton().logMessage( "Current game state is not AS_GAME" );
					break;
				}


				/***** FIXME: WE HAVE TO FIX THE INVENTORY CEGUI CODE *********************************
				Inventory *inventory = Inventory::getSingletonPtr();
				inventory->clear();
				for( int i = 0; i < size; i++ )
				{
					int tmp;
					incoming.Read( tmp );
					if( !inventory->addItem( tmp ) )
					{
						Ogre::LogManager::getSingleton().logMessage( "addItem() finished unsuccessfully" );
					}
				}
				*****************************************************************************************/
			}
			break;
		default:
			{
				std::stringstream ss;
				ss << mPacket->data[0];
				std::string str;
				ss >> str;
				Ogre::LogManager::getSingleton().logMessage( str );
			}
			break;
		}

		mPeer->DeallocatePacket( mPacket );
	}

	if( connected && !connect_notified )
	{
		// Call whatever upon successful connect.
		connect_notified = true;
		sendLoginInfo();
	}

	return true;
}

void Network::parseServerList()
{
	// With this "if" check we can make sure we don't call this function multiple times;
	//if ( ClientStateManager::getSingletonPtr()->getCurrentState() == AS_SERVER_MENU )
	//{
	//	return;
	//}

	for( unsigned int indx = 0; indx < server_list.size(); indx++ )
	{
		delete server_list[indx];
	}
	server_list.clear();

	int size;
	unsigned char typeId;
	RakNet::BitStream input( mPacket->data, mPacket->length, false );
	input.Read( typeId );
	input.Read( size );

	for( int indx = 0; indx < size; indx++ )
	{
		Server *server = new Server();
		server_list.push_back( server );

		char _name[128];
		stringCompressor->DecodeString( _name, 128, &input );
		server->server_name = std::string( _name );

		char _ip[128];
		stringCompressor->DecodeString( _ip, 128, &input );
		server->server_ip_address = std::string( _ip );

		bool _status;
		input.Read( _status );
		server->server_status = _status;
	}

	logged_in = true;
	ClientStateManager::getSingletonPtr()->setState(AS_SERVER_MENU);
}

void Network::parseCharacterList()
{
	unsigned char Id;
	int size;

	RakNet::BitStream in( mPacket->data, mPacket->length, false );
	in.Read( Id );
	in.Read( size );
	for( int indx = 0; indx < size; indx++ )
	{
		Character *character = new Character();
		character_list.push_back( character );
		char name[128];
		stringCompressor->DecodeString( name, 128, &in );
		character->name = std::string( name );
		in.Read( character->money );
	}
}

void Network::joinConnectedServer( std::string character_name )
{
//	RakNet::BitStream out;
//	stringCompressor->EncodeString( login_username.c_str(), 128, &out );

//	mPeer->RPC( "getInfo", &out, HIGH_PRIORITY, RELIABLE, 0, game_server_address, false, 0, UNASSIGNED_NETWORK_ID, 0 );

	RakNet::BitStream out;
	unsigned char Id = ID_JOIN_WORLD;
	out.Write( Id );
	stringCompressor->EncodeString( character_name.c_str(), 128, &out );
	mPeer->Send( &out, HIGH_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );
}

void Network::emptyPlayerList()
{
	for( unsigned int indx = 0; indx < player_list.size(); indx++ )
	{
		if( player_list[indx] != NULL )
			delete player_list[indx];
	}
	player_list.clear();
}

void Network::sendIdToServer( int id )
{
	unsigned char typeId = id;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE, 0, game_server_address, false );
}

void Network::sendOrientationUpdate( Ogre::Quaternion orientation )
{
	float orient_w, orient_x, orient_y, orient_z;
	orient_w = orientation.w;
	orient_x = orientation.x;
	orient_y = orientation.y;
	orient_z = orientation.z;

	unsigned char typeId = ID_UPDATE_ORIENTATION;
	RakNet::BitStream outgoing;
	outgoing.Write( typeId );
	outgoing.Write( orient_w );
	outgoing.Write( orient_x );
	outgoing.Write( orient_y );
	outgoing.Write( orient_z );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE, 0, game_server_address, false );

	Ogre::LogManager *log = Ogre::LogManager::getSingletonPtr();

	#ifdef HEAVY_LOGGING
	log->logMessage( "SEND: orient_w: " + Ogre::StringConverter::toString(orient_w) );
	log->logMessage( "SEND: orient_x: " + Ogre::StringConverter::toString(orient_x) );
	log->logMessage( "SEND: orient_y: " + Ogre::StringConverter::toString(orient_y) );
	log->logMessage( "SEND: orient_z: " + Ogre::StringConverter::toString(orient_z) );
	#endif
}

void Network::sendPositionToServer( Ogre::Vector3 pos )
{
	RakNet::BitStream outgoing;
	float x = pos.x, y = pos.y, z = pos.z;
	unsigned char typeId = ID_UPDATE_POSITION;
	outgoing.Write( typeId );
	outgoing.Write( x );
	outgoing.Write( y );
	outgoing.Write( z );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE, 0, game_server_address, false );
}

void Network::sendChatMessage( std::string message )
{
	RakNet::BitStream outgoing;
	unsigned char typeId = ID_CHAT_SAY;
	outgoing.Write( typeId );
	stringCompressor->EncodeString( message.c_str(), 128, &outgoing );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );
}

void Network::sendCommand( std::string cmd )
{
	RakNet::BitStream outgoing;
	unsigned char typeId = ID_COMMAND;
	outgoing.Write( typeId );
	stringCompressor->EncodeString( cmd.c_str(), 128, &outgoing );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );
	checkCommandProcedures( cmd );
	Ogre::LogManager::getSingleton().logMessage( "Command " + cmd + " sent to server." );
}

void Network::checkCommandProcedures( std::string cmd )
{
}

void Network::request_NPC_interaction( std::string npc_name )
{
	RakNet::BitStream outgoing;
	unsigned char typeId = ID_REQUEST_NPC_INTERACTION;
	outgoing.Write( typeId );
	stringCompressor->EncodeString( npc_name.c_str(), 128, &outgoing );
	mPeer->Send( &outgoing, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );
}

void Network::request_Full_Inventory_Snapshot()
{
	RakNet::BitStream outgoing;
	unsigned char typeId = ID_INVENTORY_FULL_SNAPSHOT;
	outgoing.Write( typeId );
	mPeer->Send( &outgoing, HIGH_PRIORITY, RELIABLE_ORDERED, 0, game_server_address, false );
}

void Network::testNPCQuery()
{
	Game *game = ClientStateManager::getSingletonPtr()->getGame();
	if (!game)
		return;
	else
	{
		NonePlayerStaticCharacter *npc = game->getNonePlayerStaticCharacter("testnpc");
		if( npc &&
			game->getSelfPlayerCharacter()->getPosition().x < (npc->getPosition().x + 50) &&
			game->getSelfPlayerCharacter()->getPosition().x > (npc->getPosition().x - 50) &&
			game->getSelfPlayerCharacter()->getPosition().y < (npc->getPosition().y + 50) &&
			game->getSelfPlayerCharacter()->getPosition().y > (npc->getPosition().y - 50) &&
			game->getSelfPlayerCharacter()->getPosition().z < (npc->getPosition().z + 50) &&
			game->getSelfPlayerCharacter()->getPosition().z > (npc->getPosition().z - 50)
			)
	Game* const game = ClientStateManager::getSingletonPtr()->getGame();


	if(game)
		{
			request_NPC_interaction( "testnpc" );
		}
		else
			Ogre::LogManager::getSingleton().logMessage( "testNPCQuery - Not in range." );
	}

}

void Network::parseMovementChange( std::string character_name, short movement_type )
{
	Game* game = ClientStateManager::getSingletonPtr()->getGame();
	

	// Return if were not in game //
	if (game == NULL)
	{
		return;
	}
	 

	SelfPlayerCharacter* const selfPlayerCharacter = game->getSelfPlayerCharacter();
	

	// It this the Self character or another character? //
	if ( selfPlayerCharacter->getName() == character_name )
	{
		switch(movement_type)
		{
			case ID_HALT:
				selfPlayerCharacter->setMovementDirection(URA::MovementDirection::NONE);
				break;
			case ID_FORWARD:
				selfPlayerCharacter->setMovementDirection(URA::MovementDirection::FORWARD);
				break;
			case ID_BACK:
				selfPlayerCharacter->setMovementDirection(URA::MovementDirection::BACKWARD);
				break;
			case ID_LEFT:
				selfPlayerCharacter->setMovementDirection(URA::MovementDirection::LEFT);
				break;
			case ID_RIGHT:
				selfPlayerCharacter->setMovementDirection(URA::MovementDirection::RIGHT);
				break;
		}
	}
	else
	{
		OtherPlayerCharacter* const otherPlayerCharacter = game->getOtherPlayerCharacter(character_name);

		if (otherPlayerCharacter == NULL)
		{
			// This won't stop throwing up falsies in the log until the server doesn't send this client's updates to this client.
			//Ogre::LogManager::getSingleton().logMessage( "parseMovementChange character find failed in Network.cpp." );
			return;
		}

		switch(movement_type)
		{
			case ID_HALT:
				otherPlayerCharacter->setMovementDirection(URA::MovementDirection::NONE);
				break;
			case ID_FORWARD:
				otherPlayerCharacter->setMovementDirection(URA::MovementDirection::FORWARD);
				break;
			case ID_BACK:
				otherPlayerCharacter->setMovementDirection(URA::MovementDirection::BACKWARD);
				break;
			case ID_LEFT:
				otherPlayerCharacter->setMovementDirection(URA::MovementDirection::LEFT);
				break;
			case ID_RIGHT:
				otherPlayerCharacter->setMovementDirection(URA::MovementDirection::RIGHT);
				break;
		}
	}

}  

NonePlayerStaticCharacter* const Network::npcQuery(string &name)
{
	Game* const game = ClientStateManager::getSingletonPtr()->getGame();
	SelfPlayerCharacter* cPlayer = 0;
	NonePlayerStaticCharacter* npc = 0;

	Ogre::Vector3 playerPosition;
	Ogre::Vector3 npcPosition;

	Ogre::Real maxDistanceApart = 800.0;


	if (game)
	{
		// Get the pointers;
		cPlayer = game->getSelfPlayerCharacter();
		npc = game->getNonePlayerStaticCharacter(name);

		// Get the positions;
		playerPosition = cPlayer->getPosition();
		npcPosition = npc->getPosition();


		if (npc)
		{

			if ( playerPosition.x < (npcPosition.x + maxDistanceApart) &&
				playerPosition.x > (npcPosition.x - maxDistanceApart) &&
				playerPosition.y < (npcPosition.y + maxDistanceApart) &&
				playerPosition.y > (npcPosition.y - maxDistanceApart) &&
				playerPosition.z < (npcPosition.z + maxDistanceApart) &&
				playerPosition.z > (npcPosition.z - maxDistanceApart)
				)
			{
				request_NPC_interaction(name);
				return npc;
			}
		}
		else
		{
			Ogre::LogManager::getSingleton().logMessage( name +  " - Not in range." );
		}
	}


	return NULL;
}

void Network::cancelMasterServerConnect()
{
	bCancelMasterServerConnect = true;
}

void Network::clientQuit( bool q )
{
	mSilenceTimer->reset();
	bQuitting = q;
}
