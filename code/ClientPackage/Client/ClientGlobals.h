/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CLIENTGLOBALS_H__
#define __CLIENTGLOBALS_H__
 



/**************************************/
/*** The Client Window Caption Text ***/
/**************************************/
#define DEF_CLIENTGLOBALS_CAPTION_TEXT				"URA Client Stan (UC) - Follow Your Dreams"



////////////////////////////////
/// The theme song file name ///
////////////////////////////////
#define DEF_CLIENTGLOBALS_THEMESONG_FILENAME		"ot2.wav"

 

/********************/
/*** Server stuff ***/
/********************/
/* UnComment this if you want to bypass the server with a hack. Just press enter at the mainMenu */
#define DEF_CLIENTGLOBALS_MAINMENU_BYPASS_SERVER_HACK

#define DEF_CLIENTGLOBALS_SERVERPORT					38741
#define DEF_CLIENTGLOBALS_MASTERSERVERPORT				38740
#define DEF_CLIENTGLOBALS_MASTERSERVERPORT_STRING		":38740"
#define DEF_CLIENTGLOBALS_MILLISECONDS_PER_SNAPSHOT		500	// Value of 500 here means 2 snapshots per second.
#define DEF_CLIENTGLOBALS_CONNECTION_TIMEOUT			10000 // 10 seconds.



/**********************************/
/*** SIMPLE PAGED TERRAIN (SPT) ***/
/**********************************/

// Uncomment this if you want to disable the terrain light map code. //
#define DEF_CLIENTGLOBALS_DISABLE_TERRAIN_LIGHTMAP_CODE


//////////////////
/// FILE NAMES ///
//////////////////

// DEFAULT SPLAT MAP FILE //
#define DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_FILENAME				"Highlands_Splat.png"

// DEFAULT SPLAT MAP MATERIAL NAME //
#define DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_MATERIALNAME			"SPT_Highlands"

// DEFAULT TERRAIN HEIGHTMAP FILE NAME //
#define DEF_CLIENTGLOBALS_TERRAIN_DEFAULT_HEIGHTMAP_FILENAME	"Highlands_Height.png"


/*************/
/*** CEGUI ***/
/*************/
#define DEF_CLIENTGLOBALS_MAX_USERNAME_LENGHT				15
#define DEF_CLIENTGLOBALS_MAX_PASSWORD_LENGHT				10
//#define DEF_CLIENTGLOBALS_MAX_CHATWINDOW_TEXT_RECEIVE_LENGTH		100


/*************/
/*** SOUND ***/
/*************/
#define DEF_CLIENTGLOBALS_SOUNDMANAGER_RESOURCE_DIR				"../../../resources/sounds/"


////////////////////////
/// Character models ///
////////////////////////

// Main Male Character //
#define DEF_CLIENTGLOBALS_MAINCHARACTER_MALE_MESH_NAME				"ninja"
#define DEF_CLIENTGLOBALS_MAINCHARACTER_MALE_MATERIAL_NAME			"ura/Character/MaleNinja"
// Main Female Character
#define DEF_CLIENTGLOBALS_MAINCHARACTER_FEMALE_MESH_NAME			"ninja"
#define DEF_CLIENTGLOBALS_MAINCHARACTER_FEMALE_MATERIAL_NAME		"ura/Character/FemaleNinja"


#endif
