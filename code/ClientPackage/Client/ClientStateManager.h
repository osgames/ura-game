/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CLIENT_STATE_MANAGER_H__
#define __CLIENT_STATE_MANAGER_H__


#include <Ogre.h>
#include "Network.h"
#include "ClientLoop.h"

  
enum AvailableStates
{
	AS_NONE,
	AS_QUIT,			// Quit game.
	AS_MAIN_MENU,		// Open main menu.
	AS_SERVER_MENU,		// Open server menu.
	AS_CHARACTER_MENU,	// Open character menu.
	AS_GAME				// Start in-game.
};



class MainMenu;
class ServerMenu;
class CharacterMenu;
class Game;


 
class ClientStateManager : public Ogre::Singleton<ClientStateManager>
{
private:
	AvailableStates mCurrentState;

	ClientLoop* mClientLoop;

	// The outside Game Gui Windows // 
	MainMenu* mMainMenu;
	ServerMenu* mServerMenu;
	CharacterMenu* mCharacterMenu;

	Game* mGame;

	void _destroyDynamicObjects();
public:
	ClientStateManager();
	~ClientStateManager();

	static ClientStateManager* const getSingletonPtr();

	AvailableStates getCurrentState() const;
	void setState(AvailableStates state);

	MainMenu* const getMainMenu() const;
	ServerMenu* const getServerMenu() const;
	CharacterMenu* const getCharacterMenu() const;

	Game* const getGame() const;
};

#endif
