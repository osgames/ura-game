/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Sound.h"


ALboolean Sound::loadALData( std::string sound )
{
	file = sound;

    // Variables to load into.

    ALenum format;
    ALsizei size;
    ALvoid* data;
    ALsizei freq;
	ALboolean loop;

	// Load wav data into a buffer.
    alGenBuffers(1, &Buffer);
    if (alGetError() != AL_NO_ERROR)
        return AL_FALSE;

    alutLoadWAVFile((ALbyte *)sound.c_str(), &format, &data, &size, &freq, &loop);
    alBufferData(Buffer, format, data, size, freq);
    alutUnloadWAV(format, data, size, freq);

	// Bind buffer with a source.
    alGenSources(1, &Source);

    if (alGetError() != AL_NO_ERROR)
        return AL_FALSE;

    alSourcei (Source, AL_BUFFER,   Buffer   );
    alSourcef (Source, AL_PITCH,    1.0f     );
    alSourcef (Source, AL_GAIN,     1.0f     );
	alSourcef( Source, AL_MAX_DISTANCE, 1000.0f );
	alSourcef( Source, AL_ROLLOFF_FACTOR, 0.15f );
    
	
//    alSourcefv(Source, AL_VELOCITY, SourceVel);

	// Do another error check and return.
    if (alGetError() == AL_NO_ERROR)
        return AL_TRUE;

    return AL_FALSE;
}

void Sound::killALData()
{
    alDeleteBuffers(1, &Buffer);
    alDeleteSources(1, &Source);
}

Sound::Sound( std::string sound_file )
{
	if( !loadALData( sound_file ) )
	{
		Ogre::LogManager::getSingleton().logMessage( "Error: OpenAL could not open specified sound file: " + sound_file );
	}

	setPosition( Ogre::Vector3( 0.0, 0.0, 0.0 ) );
}

Sound::~Sound()
{
	// Causes OpenAL problems.
	//killALData();
}

void Sound::setPosition( Ogre::Vector3 pos )
{
	SourcePos[0] = pos.x;
	SourcePos[1] = pos.y;
	SourcePos[2] = pos.z;
	alSourcefv(Source, AL_POSITION, SourcePos);
}

void Sound::play()
{
	alSourcePlay( Source );
}

void Sound::stop()
{
	alSourceStop( Source );
}

void Sound::pause()
{
	alSourcePause( Source );
}

void Sound::setLoop( bool l )
{
	alSourcei ( Source, AL_LOOPING, l );
}

std::string Sound::getFile()
{
	return file;
}
