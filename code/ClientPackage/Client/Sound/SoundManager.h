/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __SOUND_MANAGER_H__
#define __SOUND_MANAGER_H__

#include <stdlib.h>
#include <AL/alut.h>
#include <Ogre.h>
#include <list>

#include "Sound.h"

/*
 * Handles OpenAL interfacing and creation/destruction of Sound objects.
 */
class SoundManager : public Ogre::Singleton<SoundManager>
{
private:
	ALfloat mListenerPos[3];
	ALfloat mListenerVel[3];
	ALboolean LoadALData();
	Ogre::Quaternion mOrientation;
	std::list<Sound *> mSoundList;
	std::string mResourcesDirectory;
public:
	SoundManager();
	~SoundManager();

	static SoundManager* getSingletonPtr();

	void setPlayerPosition( Ogre::Vector3 pos );
	void setPlayerVelocity( Ogre::Vector3 vel );
	void setPlayerOrientation( Ogre::Quaternion quat );
	Sound *createSound( std::string sound_file );
	bool destroySound( Sound *s );
	void setResourcesDirectory( std::string dir );
	Sound *getSound( std::string sound_file );
};

#endif
