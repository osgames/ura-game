/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __SOUND_H__
#define __SOUND_H__

#include <string>
#include <stdlib.h>

//#include <AL/al.h>
//#include <AL/alc.h>
#include <AL/alut.h>

#include <Ogre.h>

class Sound
{
private:
	// Position of the source sound.
	ALfloat SourcePos[3];

	ALuint Buffer; // Holds the sound data.
	ALuint Source; // Holds the source of sound.
	ALboolean loadALData( std::string sound );
	void killALData();
	std::string file;
public:
	Sound( std::string sound_file );
	~Sound();
	void setPosition( Ogre::Vector3 pos );
	void play();
	void stop();
	void pause();
	void setLoop( bool l );
	std::string getFile();
};

#endif
