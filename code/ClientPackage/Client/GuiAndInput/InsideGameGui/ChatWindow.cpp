/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#include "ChatWindow.h"

 
 
using namespace std;



// The point is to put as much stuff on the heap as possible. And to delete things when not needed or toggled;
ChatWindow::ChatWindow() : mChatWindowSubClass(NULL)
{

}

ChatWindow::~ChatWindow()
{
	if ( mChatWindowSubClass )
	{
		delete mChatWindowSubClass;
		mChatWindowSubClass = NULL;
	}
}

// Prepare the windows for quiting;
// This function saves the layout and destroys the cegui window;
// Note: the code from prepareToQuit() should not be put in its objects destructor;
void ChatWindow::prepareToQuit()
{
	if ( mChatWindowSubClass )
	{
		windowText = mChatWindowSubClass->getText();
		mChatWindowSubClass->prepareToQuit();
	}
}

void ChatWindow::toggle()
{
	/*** If visible, then hide and delete the window and object. Else we create the object ***/

	if ( mChatWindowSubClass )
	{
		windowText = mChatWindowSubClass->getText();
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		mChatWindowSubClass->prepareToQuit();
		delete mChatWindowSubClass;
		mChatWindowSubClass = NULL;		// Let us know the object was deleted;

		#ifdef CHATWINDOW_DEBUG
			cout << "Deleted chatwindow" << endl;
		#endif
	}
	else
	{
		/*** Recreate the window ***/
		mChatWindowSubClass = new ChatWindowSubClass;

		if ( mChatWindowSubClass == NULL )
		{
			cout << "ChatWindow::toggle(): (mChatWindowSubClass == NULL)" <<
				" Error allocating heap memory" << endl;

			// Don't allow us to continue further;
			return;
		}

		mChatWindowSubClass->addChildWindow();

		mChatWindowSubClass->setText( windowText );

		#ifdef CHATWINDOW_DEBUG
			cout << "created chat window" << endl;
		#endif
	}
}

bool ChatWindow::isVisible() const
{
	if (mChatWindowSubClass)
	{
		return mChatWindowSubClass->isVisible();
	}

	return false;
}

bool ChatWindow::isActive() const
{
	if (mChatWindowSubClass)
	{
		return mChatWindowSubClass->isActive();
	}

	return false;
}

void ChatWindow::deactivate()
{
	if ( mChatWindowSubClass )
	{
		mChatWindowSubClass->deactivate();
	}
}

void ChatWindow::clearTextHistory()
{
	if ( mChatWindowSubClass )
	{
		mChatWindowSubClass->clearTextHistory();
	}
}

void ChatWindow::clearInputBox()
{
	if ( mChatWindowSubClass )
	{
		mChatWindowSubClass->clearInputBox();
	}
}

void ChatWindow::sendEnter()
{
	if ( mChatWindowSubClass )
	{
		mChatWindowSubClass->sendEnter();
	}
}

void ChatWindow::receive_say_chat( std::string &message, std::string &player )
{
	if ( mChatWindowSubClass )
	{
		mChatWindowSubClass->receive_say_chat(message, player);
	}
}

CEGUI::Window* const ChatWindow::getWindow() const
{
    return mChatWindowSubClass->getWindow();
}








ChatWindowSubClass::ChatWindowSubClass() : mCeguiChatWindow(NULL), mChatInputBox(NULL), mChatReadBox(NULL)
{

}

void ChatWindowSubClass::prepareToQuit()
{
	/*** Prepare to save the layout ***/
	clearTextHistory();
	clearInputBox();

	/*** Destroy the cegui window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mCeguiChatWindow);
}

void ChatWindowSubClass::toggle()
{
	if ( mChatInputBox->isActive() )	// Make sure were not in the chat editbox;
	{
		return;
	}
	else if( mCeguiChatWindow->isVisible() == false )
	{
		mCeguiChatWindow->show();
	}
	else if( mCeguiChatWindow->isVisible() == true )
	{
		mCeguiChatWindow->hide();
	}
}

bool ChatWindowSubClass::isVisible()
{
	return mCeguiChatWindow->isVisible();
}

bool ChatWindowSubClass::isActive()
{
	if ( mChatInputBox->isActive() )
	{
		return true;
	}

	return false;
}

void ChatWindowSubClass::deactivate()
{
	mCeguiChatWindow->deactivate();
	mChatInputBox->deactivate();
}

void ChatWindowSubClass::sendEnter()
{
	ChatTextParser chatTextParser;
	string chatString;

	if ( mCeguiChatWindow->isVisible() == true )
	{
		if ( mChatInputBox->isActive() == false )
		{
			mCeguiChatWindow->activate();
			mChatInputBox->activate();
		}
		else
		{
			if ( mChatInputBox->getText() ==  "" )
			{
				mCeguiChatWindow->deactivate();
				mChatInputBox->deactivate();
			}
			else	// There should be text in the editbox...lets parse it and send it to the server;
			{
				// Get the editbox text;
				chatString = mChatInputBox->getText().c_str();

				// Don't do anything if no text in the editbox;
				// Else parse the string and send it on to the server;
				if ( chatString != "" )
				{
					chatString = chatTextParser.handleText( chatString );

					// If the string isn't a null then append the string 'chatString';
					if ( chatString != "")
					{
						mChatReadBox->setText( mChatReadBox->getText() + chatString);
						mChatReadBox->getVertScrollbar()->setScrollPosition( mChatReadBox->getText().length() );
					}
				}

				// Clear the editbox after the text has been sent;
				mChatInputBox->setText( "" );
			}
		}
	}
}

void ChatWindowSubClass::clearTextHistory()
{
	mChatReadBox->setText( "" );
}

void ChatWindowSubClass::clearInputBox()
{
	mChatInputBox->setText( "" );
}

void ChatWindowSubClass::receive_say_chat( std::string &message, std::string &player )
{
	mChatReadBox->setText( mChatReadBox->getText() + "[" + player + "]: " + message );
	mChatReadBox->getVertScrollbar()->setScrollPosition( mChatReadBox->getText().length() );
}
 
bool ChatWindowSubClass::addChildWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();


	// Chat window;
	mCeguiChatWindow = windowManager.createWindow("TaharezLook/FrameWindow", "InGameUI_ChatWindow");
	mCeguiChatWindow->setText("   Chat Window");
	mCeguiChatWindow->setSize(CEGUI::USize(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.3, 0)));
	mCeguiChatWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.65, 0) ) );
	// Show the window;
	mCeguiChatWindow->show();

	// chat window input box;
	mChatInputBox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "InGameUI_ChatInput");
	mChatInputBox->setSize(CEGUI::USize(CEGUI::UDim(0.9, 0), CEGUI::UDim(0.15, 0)));
	mChatInputBox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.80, 0) ) );
	//show the input;
	mChatInputBox->show();

	// chat window area;
	mChatReadBox = (CEGUI::MultiLineEditbox *) windowManager.createWindow( "TaharezLook/MultiLineEditbox", "InGameUI_ChatArea" );
	mChatReadBox->setReadOnly(true);
	mChatReadBox->setWordWrapping(true);
	mChatReadBox->setSize(CEGUI::USize(CEGUI::UDim(0.9, 0), CEGUI::UDim(0.6, 0)));
	mChatReadBox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.15, 0) ) );


	// Form the chat window;
	mCeguiChatWindow->addChild( mChatInputBox );
	mCeguiChatWindow->addChild( mChatReadBox );
	
	// Add this window to the main window;
	sheet->addChild( mCeguiChatWindow );


	return true;
}

std::string ChatWindowSubClass::getText()
{
	return std::string( mChatReadBox->getText().c_str() );
}

void ChatWindowSubClass::setText( std::string text )
{
	mChatReadBox->setText(text);
}

CEGUI::Window* const ChatWindowSubClass::getWindow() const
{
    return mCeguiChatWindow;
}


