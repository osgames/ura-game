/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __CHAT_WINDOW_H__
#define __CHAT_WINDOW_H__


#include <CEGUI/CEGUI.h>
#include <string>

#include "ChatTextParser.h"


// Sets the debug mode;
//////#define CHATWINDOW_DEBUG

 

class ChatWindowSubClass
{
protected:
	CEGUI::Window* mCeguiChatWindow;
	CEGUI::Editbox* mChatInputBox;
	CEGUI::MultiLineEditbox* mChatReadBox;
public:

	ChatWindowSubClass();
	void prepareToQuit();

	void toggle();
	bool isVisible();
	bool isActive();
	void deactivate();
	void clearTextHistory();
	void clearInputBox();
	void sendEnter();
	void receive_say_chat( std::string &message, std::string &player );
	bool addChildWindow();

	std::string getText(); // Get the chat text.
	void setText( std::string text );

	CEGUI::Window* const getWindow() const;
};




// The point is to put as much stuff on the heap as possible. And to delete things when not needed or toggled;
class ChatWindow
{
protected:
	ChatWindowSubClass* mChatWindowSubClass;
	std::string windowText;		// Chat window text saved here so it won't be lost on a toggle.
public:
	ChatWindow();
	~ChatWindow();
	void prepareToQuit();

	void toggle();
	bool isVisible() const;
	bool isActive() const;
	void deactivate();
	void clearTextHistory();
	void clearInputBox();
	void sendEnter();
	void receive_say_chat( std::string &message, std::string &player );
	CEGUI::Window* const getWindow() const;
};




#endif
