/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PositionInfoWindow.h"
#include "CeguiManager.h"
#include "Ogre3dManager.h"

#include "ClientStateManager.h"
#include "SelfPlayerCharacter.h"



PositionInfoWindow::PositionInfoWindow()
{ 
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();


	mTmpString = "";
	mTmpOgreVec3 = Ogre::Vector3(0,0,0);

	mLabel = static_cast<CEGUI::Window*>( ceguiManager->createWindow("TaharezLook/StaticText", "PositionInfoWindow_mLabel") );
	mLabel->setSize(CEGUI::USize(CEGUI::UDim(0.5, 0), CEGUI::UDim(0.03, 0)));
	mLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(0.2, 0), CEGUI::UDim(0.95, 0) ) );
	// Disable frame and background;
	mLabel->setProperty("FrameEnabled", "false");
	mLabel->setProperty("BackgroundEnabled", "false");
	// Disable the label so it doesn't steal the events;
	mLabel->disable();

	/// Add the child to the root window ///
	ceguiManager->addChildToRootWindow(mLabel);
}

PositionInfoWindow::~PositionInfoWindow()
{
	CeguiManager::getSingletonPtr()->destroyWindow(mLabel);
}

    
void PositionInfoWindow::update()
{
	// Get the position //
	mTmpOgreVec3 = ClientStateManager::getSingletonPtr()->getGame()->
		getSelfPlayerCharacter()->getPosition();

	// Set the text to the xyz position //
	mTmpString = "Position: ";
	mTmpString += Ogre::StringConverter::toString(mTmpOgreVec3.x);
	mTmpString += ",  ";
	mTmpString += Ogre::StringConverter::toString(mTmpOgreVec3.y);
	mTmpString += ",  ";
	mTmpString += Ogre::StringConverter::toString(mTmpOgreVec3.z);

	// Put the text in the label //
	mLabel->setText(mTmpString);
}

