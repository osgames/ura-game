/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#include "InsideGameGui.h"


 
InsideGameGui::InsideGameGui()
{
	mInputKeyCollector = InputManager::getSingletonPtr()->getInputKeyCollector();

	// Allocate memory //
	mQuitWindow = new QuitWindow();
	mFpsInfoWindow = new FpsInfoWindow();
	mPositionInfoWindow = new PositionInfoWindow();
	//mChatWindow = new ChatWindow();

	// Only load the windows/objects that might be shown at startup //
	//mChatWindow->toggle();
}
 
InsideGameGui::~InsideGameGui()
{
	// Destroy the Quit Window //
	if (mQuitWindow)
	{
		delete mQuitWindow;
		mQuitWindow = NULL;
	}

	if (mFpsInfoWindow)
	{
		delete mFpsInfoWindow;
		mFpsInfoWindow = NULL;
	}

	if (mPositionInfoWindow)
	{
		delete mPositionInfoWindow;
		mPositionInfoWindow = NULL;
	}
	 
	//if (mChatWindow)
	//{
	//	delete mChatWindow;
	//	mChatWindow = NULL;
	//}
}



void InsideGameGui::receive_say_chat( std::string &message, std::string &player )
{
	//if (mChatWindow)
	//{
	//	mChatWindow->receive_say_chat(message, player);
	//}
}



bool InsideGameGui::isWindowActive() const
{
	if ( mQuitWindow->isActive() )		return true;
	//if ( mChatWindow->isActive() )		return true;

	return false;
}


void InsideGameGui::updateInput()
{
	// Quit Window - The loops auto repeats so we don't toggle if its already open //
	if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_ESCAPE) )
	{
		if ( !mQuitWindow->isActive() )
		{
			mQuitWindow->toggle();
		}
	}

	//else if ( mChatWindow && mChatWindow->isActive() )
	//{
	//	mChatWindow->updateInput();
	//}
}
 
void InsideGameGui::updateData()
{
	mFpsInfoWindow->update();
	mPositionInfoWindow->update();
}

