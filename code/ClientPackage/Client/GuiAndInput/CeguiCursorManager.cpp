/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CeguiCursorManager.h"
#include "CeguiManager.h"



CeguiCursorManager::CeguiCursorManager()
{
	// Set the default Mouse Image //
	CEGUI::System* const system = CeguiManager::getSingletonPtr()->getSystem();	
	system->getDefaultGUIContext().getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
} 

CeguiCursorManager::~CeguiCursorManager()
{

}



bool CeguiCursorManager::getCursorScreenPosition(Ogre::Vector2 &screenPosVector2)
{
	//CEGUI::MouseCursor& mc = CEGUI::MouseCursor::getSingleton();
	//CEGUI::Window* const sheet = CEGUI::System::getSingleton().getGUISheet();

	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::MouseCursor& mc = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor();
	
	
	// FIXME:
	/***************************
	if (sheet)
	{
		screenPosVector2.x = mc.getPosition().d_x / (float)sheet->getPixelRect().getWidth();
		screenPosVector2.y = mc.getPosition().d_y / (float)sheet->getPixelRect().getHeight();
		return true;
	}
	************************/

	

	return false;
} 

bool CeguiCursorManager::getCursorOverWidow()
{
	// FIXME:
	//CEGUI::Window* const sheet = CEGUI::System::getSingleton().getGUISheet();
	//CEGUI::MouseCursor& mc = CEGUI::MouseCursor::getSingleton();

	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::MouseCursor& mc = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor();


	if (sheet)
	{
		if ( sheet->getChildAtPosition(mc.getPosition()) )
		{
			return true;
		}
	}


	return false;
}
