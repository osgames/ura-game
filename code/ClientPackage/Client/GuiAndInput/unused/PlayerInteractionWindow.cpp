#include "PlayerInteractionWindow.h"
#include "CeguiManager.h"

#include <Ogre.h>


#define PLAYERINTERACTIONWINDOW_NAME		"PlayerInteractionWindow_mPlayerInteractionWindow"

#define DEF_MIMAGETALK_NAME			"PlayerInteractionWindow_mImageTalk"
#define DEF_MIMAGETALK_BGIMAGESET_NAME		"PlayerInteractionWindow_mImageTalk_ImageSet"
#define DEF_MIMAGETALK_BGIMAGE_FILENAME		"PlayerInteractionTalkImage.png"

#define DEF_MIMAGEBUY_NAME			"PlayerInteractionWindow_mImageBuy"
#define DEF_MIMAGEBUY_BGIMAGESET_NAME		"PlayerInteractionWindow_mImageBuy_ImageSet"
#define DEF_MIMAGEBUY_BGIMAGE_FILENAME		"PlayerInteractionBuyImage.png"

#define DEF_MIMAGELOOK_NAME			"PlayerInteractionWindow_mImageLook"
#define DEF_MIMAGELOOK_BGIMAGESET_NAME		"PlayerInteractionWindow_mImageLook_ImageSet"
#define DEF_MIMAGELOOK_BGIMAGE_FILENAME		"PlayerInteractionLookImage.png"

#define DEF_MIMAGECANCEL_NAME			"PlayerInteractionWindow_mImageCancel"
#define DEF_MIMAGECANCEL_BGIMAGESET_NAME	"PlayerInteractionWindow_mImageCancel_ImageSet"
#define DEF_MIMAGECANCEL_BGIMAGE_FILENAME	"PlayerInteractionCancelImage.png"


#define PLAYERINTERACTIONWINDOW_SIZE_X		0.15f
#define PLAYERINTERACTIONWINDOW_SIZE_Y		0.15f


#define DEF_NUMBER_OF_IMAGES_X 			2
#define DEF_NUMBER_OF_IMAGES_Y 			2
#define DEF_IMAGES_BUFFER_SIZE 			0.02f



using namespace std;




PlayerInteractionWindow::PlayerInteractionWindow()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImagesetManager* const imagesetManager = ceguiManager->getImagesetManager();
	CEGUI::Window* const parentWindow = ceguiManager->getParentWindow();

	string bgImageString_mImageTalk;
	string bgImageString_mImageBuy;
	string bgImageString_mImageLook;
	string bgImageString_mImageCancel;

	// Store the image X and Y sizes to be used;
	float image_size_x = (1.0f / DEF_NUMBER_OF_IMAGES_X) - DEF_IMAGES_BUFFER_SIZE;
	float image_size_y = (1.0f / DEF_NUMBER_OF_IMAGES_Y) - DEF_IMAGES_BUFFER_SIZE;	



	/****************************************/
	/*** Set the Images X and Y positions ***/
	/****************************************/

	// Row one of the images;
	float mImageTalk_pos_x = 0.0f;
	float mImageTalk_pos_y = 0.0f;

	float mImageBuy_pos_x =  (1.0 * image_size_x) + DEF_IMAGES_BUFFER_SIZE;
	float mImageBuy_pos_y =  0.0f;

	// Row two of the images;
	float mImageLook_pos_x = 0.0f;
	float mImageLook_pos_y = (1.0 * image_size_y) + DEF_IMAGES_BUFFER_SIZE;

	float mImageCancel_pos_x = (1.0 * image_size_x) + DEF_IMAGES_BUFFER_SIZE;
	float mImageCancel_pos_y = (1.0 * image_size_y) + DEF_IMAGES_BUFFER_SIZE;


	/***************************/
	/*** Creat the imagesets ***/
	/***************************/
	imagesetManager->createImagesetFromImageFile(DEF_MIMAGETALK_BGIMAGESET_NAME, DEF_MIMAGETALK_BGIMAGE_FILENAME);
 	imagesetManager->createImagesetFromImageFile(DEF_MIMAGEBUY_BGIMAGESET_NAME, DEF_MIMAGEBUY_BGIMAGE_FILENAME);
 	imagesetManager->createImagesetFromImageFile(DEF_MIMAGELOOK_BGIMAGESET_NAME, DEF_MIMAGELOOK_BGIMAGE_FILENAME);
 	imagesetManager->createImagesetFromImageFile(DEF_MIMAGECANCEL_BGIMAGESET_NAME, DEF_MIMAGECANCEL_BGIMAGE_FILENAME);


	/***************************************************/
	/*** Create the imagesets BackGround Set Strings ***/
	/***************************************************/

	// Create the cegui string to load the bg image set;
	bgImageString_mImageTalk = "set:";
	bgImageString_mImageTalk += DEF_MIMAGETALK_BGIMAGESET_NAME;
	bgImageString_mImageTalk += " image:full_image";


	// Create the cegui string to load the bg image set;
	bgImageString_mImageBuy = "set:";
	bgImageString_mImageBuy += DEF_MIMAGEBUY_BGIMAGESET_NAME;
	bgImageString_mImageBuy += " image:full_image";

	// Create the cegui string to load the bg image set;
	bgImageString_mImageLook = "set:";
	bgImageString_mImageLook += DEF_MIMAGELOOK_BGIMAGESET_NAME;
	bgImageString_mImageLook += " image:full_image";

	// Create the cegui string to load the bg image set;
	bgImageString_mImageCancel = "set:";
	bgImageString_mImageCancel += DEF_MIMAGECANCEL_BGIMAGESET_NAME;
	bgImageString_mImageCancel += " image:full_image";


	/*************************************/
	/*** Create the windows to be used ***/
	/*************************************/

	/*** Create the main window ***/
	mPlayerInteractionWindow = windowManager->createWindow("TaharezLook/StaticImage", PLAYERINTERACTIONWINDOW_NAME);
	mPlayerInteractionWindow->setSize(CEGUI::UVector2(CEGUI::UDim(PLAYERINTERACTIONWINDOW_SIZE_X, 0),
		CEGUI::UDim(PLAYERINTERACTIONWINDOW_SIZE_Y, 0)));
	// Keep the main window in the center;
	mPlayerInteractionWindow->setHorizontalAlignment(CEGUI::HA_CENTRE);
	mPlayerInteractionWindow->setVerticalAlignment(CEGUI::VA_CENTRE);


	/*** Create the icon Image Talk ***/
	mImageTalk = windowManager->createWindow("TaharezLook/StaticImage", DEF_MIMAGETALK_NAME);
	mImageTalk->setSize(CEGUI::UVector2(CEGUI::UDim(image_size_x, 0),
		CEGUI::UDim(image_size_y, 0)));
	mImageTalk->setPosition( CEGUI::UVector2(CEGUI::UDim(mImageTalk_pos_x, 0), CEGUI::UDim(mImageTalk_pos_y, 0)));


	/*** Create the icon Image Buy ***/
	mImageBuy = windowManager->createWindow("TaharezLook/StaticImage", DEF_MIMAGEBUY_NAME);
	mImageBuy->setSize(CEGUI::UVector2(CEGUI::UDim(image_size_x, 0),
		CEGUI::UDim(image_size_y, 0)));
	mImageBuy->setPosition( CEGUI::UVector2(CEGUI::UDim(mImageBuy_pos_x, 0), CEGUI::UDim(mImageBuy_pos_y, 0)));


	/*** Create the icon Image Buy ***/
	mImageLook = windowManager->createWindow("TaharezLook/StaticImage", DEF_MIMAGELOOK_NAME);
	mImageLook->setSize(CEGUI::UVector2(CEGUI::UDim(image_size_x, 0),
		CEGUI::UDim(image_size_y, 0)));
	mImageLook->setPosition( CEGUI::UVector2(CEGUI::UDim(mImageLook_pos_x, 0), CEGUI::UDim(mImageLook_pos_y, 0)));


	/*** Create the icon Image Buy ***/
	mImageCancel = windowManager->createWindow("TaharezLook/StaticImage", DEF_MIMAGECANCEL_NAME);
	mImageCancel->setSize(CEGUI::UVector2(CEGUI::UDim(image_size_x, 0),
		CEGUI::UDim(image_size_y, 0)));
	mImageCancel->setPosition( CEGUI::UVector2(CEGUI::UDim(mImageCancel_pos_x, 0), CEGUI::UDim(mImageCancel_pos_y, 0)));


	/****************/
	/*** Settings ***/
	/****************/

	// Set the images;
	mImageTalk->setProperty("Image", bgImageString_mImageTalk);
	mImageBuy->setProperty("Image", bgImageString_mImageBuy);
	mImageLook->setProperty("Image", bgImageString_mImageLook);
	mImageCancel->setProperty("Image", bgImageString_mImageCancel);

	// Make the main window invisible but don't allow the child windows to be invisible;
	mPlayerInteractionWindow->setAlpha(0.0f);
	mImageTalk->setInheritsAlpha(false);
	mImageBuy->setInheritsAlpha(false);
	mImageLook->setInheritsAlpha(false);
	mImageCancel->setInheritsAlpha(false);


	/**************/
	/*** Events ***/
	/**************/

	mImageTalk->subscribeEvent( CEGUI::Window::EventMouseButtonDown,
		CEGUI::Event::Subscriber( &PlayerInteractionWindow::event_click_mImageTalk, this ) );	

	mImageCancel->subscribeEvent( CEGUI::Window::EventMouseButtonDown,
		CEGUI::Event::Subscriber( &PlayerInteractionWindow::event_click_mImageCancel, this ) );	


	/*****************************/
	/*** Add the child windows ***/
	/*****************************/
	mPlayerInteractionWindow->addChildWindow(mImageTalk);
	mPlayerInteractionWindow->addChildWindow(mImageBuy);
	mPlayerInteractionWindow->addChildWindow(mImageLook);
	mPlayerInteractionWindow->addChildWindow(mImageCancel);

	parentWindow->addChildWindow(mPlayerInteractionWindow);
}

PlayerInteractionWindow::~PlayerInteractionWindow()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImagesetManager* const imagesetManager = ceguiManager->getImagesetManager();

	/*** Destory the main window ***/
	windowManager->destroyWindow(PLAYERINTERACTIONWINDOW_NAME);


	/*** Destroy the imageSets ***/
	imagesetManager->destroyImageset(DEF_MIMAGETALK_BGIMAGESET_NAME);
	imagesetManager->destroyImageset(DEF_MIMAGEBUY_BGIMAGESET_NAME);
	imagesetManager->destroyImageset(DEF_MIMAGELOOK_BGIMAGESET_NAME);
	imagesetManager->destroyImageset(DEF_MIMAGECANCEL_BGIMAGESET_NAME);
}


bool PlayerInteractionWindow::event_click_mImageTalk(const CEGUI::EventArgs &e)
{
	mPlayerInteractionWindow->hide();
	

	return true;
}

bool PlayerInteractionWindow::event_click_mImageCancel(const CEGUI::EventArgs &e)
{
	mPlayerInteractionWindow->hide();
	

	return true;
}








