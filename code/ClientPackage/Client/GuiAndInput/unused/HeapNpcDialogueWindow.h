#ifndef HEAPNPCDIALOGUEWINDOW_H
#define HEAPNPCDIALOGUEWINDOW_H

#include <CEGUI/CEGUI.h>
#include "CNPC.h"

class HeapNpcDialogueWindow;



class NpcDialogueWindow
{
protected:
	CNPC* mNPC;

	CEGUI::Window* mNpcDialogueWindow;
	CEGUI::MultiLineEditbox* mEditbox;
public:
	NpcDialogueWindow();
	~NpcDialogueWindow();

	bool addChildWindow(CNPC* const npc);
	CEGUI::Window* const getWindow() const;
};


class HeapNpcDialogueWindow
{
protected:
	NpcDialogueWindow* mNpcDialogueWindow;
public:
	HeapNpcDialogueWindow();
	~HeapNpcDialogueWindow();
	
	void toggle(CNPC* const npc);
	CEGUI::Window* const getWindow() const;
}; 


#endif







