#include "HeapNpcDialogueWindow.h"
#include "CeguiManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "GUI.h"
#include "InGameUI.h"

#include <Ogre.h>


#define NPCDIALOGUEWINDOW_NAME		"NpcDialogueWindow_mNpcDialogueWindow"

#define NPCDIALOGUEWINDOW_SIZE_X		0.5f
#define NPCDIALOGUEWINDOW_SIZE_Y		0.1f


using namespace std;




HeapNpcDialogueWindow::HeapNpcDialogueWindow() : mNpcDialogueWindow(NULL)
{

}

HeapNpcDialogueWindow::~HeapNpcDialogueWindow()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapNpcDialogueWindow::~HeapNpcDialogueWindow()";
	string strLogMessage;


	if (mNpcDialogueWindow)
	{
		strLogMessage = "Deleted mNpcDialogueWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);

		delete mNpcDialogueWindow;
		mNpcDialogueWindow = NULL;
	}
}

void HeapNpcDialogueWindow::toggle(CNPC* const npc)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapNpcDialogueWindow::toggle(...)";
	string strLogMessage;


	/*** If visible, then hide and delete the window and object. Else we create the object ***/
	if (mNpcDialogueWindow)
	{
		/*** Delete the object and set the pointer to NULL ***/
		delete mNpcDialogueWindow;
		mNpcDialogueWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted mNpcDialogueWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mNpcDialogueWindow = new NpcDialogueWindow;

		if (!mNpcDialogueWindow)
		{
			strLogMessage = "mNpcDialogueWindow";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mNpcDialogueWindow->addChildWindow(npc);

		strLogMessage = "Created mNpcDialogueWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

CEGUI::Window* const HeapNpcDialogueWindow::getWindow() const
{
	if (mNpcDialogueWindow)
	{
		return mNpcDialogueWindow->getWindow();
	}

	return NULL;
}








NpcDialogueWindow::NpcDialogueWindow()
{

}

NpcDialogueWindow::~NpcDialogueWindow()
{
	// FIXME:
	//CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	//CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();

	/*** Destory the main window ***/
	//windowManager->destroyWindow(NPCDIALOGUEWINDOW_NAME);

	CEGUI::Window* rootWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	rootWindow->destroyChild(NPCDIALOGUEWINDOW_NAME);
}


CEGUI::Window* const NpcDialogueWindow::getWindow() const
{
	// FIXME:
	//CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	//CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();

	//return windowManager->getWindow(NPCDIALOGUEWINDOW_NAME);

	CEGUI::Window* rootWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	return rootWindow->getChild(NPCDIALOGUEWINDOW_NAME);
}

bool NpcDialogueWindow::addChildWindow(CNPC* const npc)
{
	mNPC = npc;

	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::Window* const parentWindow = ceguiManager->getParentWindow();


	string npcText = mNPC->getName() + ":  " + npc->getGreetingText();


	/*************************************/
	/*** Create the windows to be used ***/
	/*************************************/

	/*** Create the main window ***/
	mNpcDialogueWindow = windowManager->createWindow("TaharezLook/StaticImage", NPCDIALOGUEWINDOW_NAME);
	mNpcDialogueWindow->setSize(CEGUI::USize(CEGUI::UDim(NPCDIALOGUEWINDOW_SIZE_X, 0),
		CEGUI::UDim(NPCDIALOGUEWINDOW_SIZE_Y, 0)));
	// Keep the main window in the center;
	mNpcDialogueWindow->setHorizontalAlignment(CEGUI::HA_CENTRE);
	//mNpcDialogueWindow->setVerticalAlignment(CEGUI::VA_CENTRE);
	mNpcDialogueWindow->setYPosition( ( CEGUI::UDim(0.3, 0) ) );

	/*** mEditbox ***/
	mEditbox = static_cast<CEGUI::MultiLineEditbox*>( windowManager->createWindow( "TaharezLook/MultiLineEditbox", "NpcDialogueWindow_mEditbox") );
	mEditbox->setReadOnly(true);
	mEditbox->setWordWrapping(true);
	mEditbox->setSize(CEGUI::USize(CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
	mEditbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0) ) );


	/****************/
	/*** Settings ***/
	/****************/
	mNpcDialogueWindow->setModalState(true);

	// Set the text;
	mEditbox->setText(npcText);

	/*****************************/
	/*** Add the child windows ***/
	/*****************************/
	mNpcDialogueWindow->addChild(mEditbox);
	parentWindow->addChild(mNpcDialogueWindow);


	return true;
}




