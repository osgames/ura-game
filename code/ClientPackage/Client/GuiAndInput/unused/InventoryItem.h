#ifndef INVENTORYITEM_H
#define INVENTORYITEM_H

namespace CEGUI
{
	class DragContainer;
}

class InventoryItem
{
public:
	// Associated DragContainer from the inventory window.
	CEGUI::DragContainer *dragContainer;

	// Item id as represented in the database.  This is what the item is built from.
	int id;

	int slotnum;
};

#endif
