#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include <memory>

class InventoryItem;
class InGameUIInventoryWindow;

class Inventory
{
private:
	std::vector<InventoryItem *> item_list;
	std::auto_ptr<InGameUIInventoryWindow> mInventoryWindow;
	static Inventory *inventory;
	Inventory();
public:
	// Returns true if item was successfully added, otherwise false.
	bool addItem( int id );
	bool removeItem( int slotnum );
	~Inventory();
	void clear(); // Clear the inventory entirely.
	static Inventory *getSingletonPtr();
};

#endif
