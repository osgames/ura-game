/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SERVER_MENU_H__
#define __SERVER_MENU_H__

#include <CEGUI/CEGUI.h>


class ServerMenu
{
private:
	CEGUI::Listbox* mServer_listbox;
	CEGUI::PushButton* mBack;
	CEGUI::PushButton* mConnect;

	std::vector<CEGUI::ListboxItem *> list;

	// The events;
	bool event_back(const CEGUI::EventArgs &e);
	bool event_connect(const CEGUI::EventArgs &e);
public:
	ServerMenu();
	~ServerMenu();

	bool sendEnter();
	bool sendEscape();
};

#endif