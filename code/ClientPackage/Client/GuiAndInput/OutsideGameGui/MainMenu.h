/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MAIN_MENU_H__
#define __MAIN_MENU_H__

#include <CEGUI/CEGUI.h>
#include <string>


class MainMenu
{
private:
	CEGUI::Window* mUserName_label;
	CEGUI::Window* mPassword_label;
	CEGUI::Editbox* mUsername_editbox;
	CEGUI::Editbox* mPassword_editbox;
	//CEGUI::Checkbox* mPassword_checkbox;
	CEGUI::PushButton* mLogin;
	CEGUI::PushButton* mQuit;

	CEGUI::Window* mLogin_process;

	// Generic notification //
	CEGUI::Window* mNotification;
	CEGUI::MultiLineEditbox* mNotificationLabel;
	CEGUI::PushButton* mNotificationOkay;

	// Events //
	bool event_quit(const CEGUI::EventArgs &e);
	bool event_login(const CEGUI::EventArgs &e);
	bool event_notification_okay(const CEGUI::EventArgs &e);

	void connectingNotify();

	// True if the client is trying to connect to master server.
	// Used to determine if mNotifyOkay will cancel a connect upon being pushed or not.
	bool mbConnecting;
public:
	MainMenu();
	~MainMenu();

	void notify(std::string text);
};

#endif
