/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
  
#include "MainMenu.h"
#include <Ogre.h>
#include "ClientGlobals.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "CeguiManager.h"
#include "ClientStateManager.h"
#include "Network.h"
#include "SoundManager.h"


#define DEF_MAINMENU_WINDOW_NAME		"MainMenu_WindowName"
#define DEF_MAINMENU_BGIMAGESET_NAME	"MainMenu_bgImage"
#define DEF_MAINMENU_BGIMAGE_FILENAME	"MenuBackground.jpg"

 
 
MainMenu::MainMenu()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	std::string strLogFunction = "MainMenu::MainMenu()";
	std::string strLogMessage = "";

	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();
	CEGUI::Window* const parentWindow = ceguiManager->getRootWindow();

	CEGUI::Window* mainMenuWindow = NULL;
	
	Sound* themeSong = NULL;




	Network::getSingleton().clientQuit( true );
	// Handle the network;
	Network::getSingletonPtr()->disconnect();
	Network::getSingleton().clientQuit( false );




	/// Load an image to use as a background ///
	CEGUI::ImageManager::getSingleton().addFromImageFile(DEF_MAINMENU_BGIMAGESET_NAME, DEF_MAINMENU_BGIMAGE_FILENAME);
	
	   
	 

	/*** Create this main window ***/
	mainMenuWindow = static_cast<CEGUI::Window*>( windowManager->createWindow("TaharezLook/StaticImage", DEF_MAINMENU_WINDOW_NAME) );
	mainMenuWindow->setSize(CEGUI::USize(CEGUI::UDim(1.0, 0), CEGUI::UDim(1.0, 0)));
	mainMenuWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0, 0), CEGUI::UDim(0.0, 0)));


	/*** Setup the other stuff ***/
	// Make username Label;
	mUserName_label = static_cast<CEGUI::Window*>( windowManager->createWindow("TaharezLook/StaticText", "MainMenu_mUserName_label") );
	mUserName_label->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.03, 0)));
	mUserName_label->setPosition( CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.37, 0) ) );
	mUserName_label->setText("Account:");
	// Disable frame and background;
	mUserName_label->setProperty("FrameEnabled", "false");
	mUserName_label->setProperty("BackgroundEnabled", "false");
	// Disable the label so it doesn't steal the events;
	mUserName_label->disable();


	// Make password Label;
	mPassword_label = static_cast<CEGUI::Window*>( windowManager->createWindow("TaharezLook/StaticText", "MainMenu_mPassword_label") );
	mPassword_label->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.03, 0)));
	mPassword_label->setPosition( CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.47, 0) ) );
	mPassword_label->setText("Password:");
	// Disable frame and background;
	mPassword_label->setProperty("FrameEnabled", "false");
	mPassword_label->setProperty("BackgroundEnabled", "false");
	// Disable the label so it doesn't steal the events;
	mPassword_label->disable();


	// FIXME - checkbox
	// Make a checkbox option for storing passwords;
	/************
	mPassword_checkbox = static_cast<CEGUI::Checkbox*>( windowManager->createWindow("TaharezLook/Checkbox", "MainMenu_mPassword_checkbox") );
	mPassword_checkbox->setSize(CEGUI::UVector2(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.03, 0)));
	mPassword_checkbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.38, 0), CEGUI::UDim(0.68, 0) ) );
	mPassword_checkbox->setText("  Save Password");
	**************/
	 

	// mQuit button;
	mQuit = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "MainMenu_mQuit") );
	mQuit->setText("Quit");
	mQuit->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mQuit->setPosition( CEGUI::UVector2(CEGUI::UDim(0.7, 0), CEGUI::UDim(0.8, 0) ) );

	// Username edit box;
	mUsername_editbox = static_cast<CEGUI::Editbox*>( windowManager->createWindow("TaharezLook/Editbox", "MainMenu_mUsername_editbox") );
	mUsername_editbox->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mUsername_editbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.4, 0) ) );

	// Password edit box;
	mPassword_editbox = static_cast<CEGUI::Editbox*>( windowManager->createWindow("TaharezLook/Editbox", "MainMenu_mPassword_editbox") );
	mPassword_editbox->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mPassword_editbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.5, 0) ) );
	mPassword_editbox->setTextMasked( true );

	// mLogin button;
	mLogin = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "MainMenu_mLogin") );
	mLogin->setText("Login");
	mLogin->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mLogin->setPosition( CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.6, 0) ) );

	mLogin_process = windowManager->createWindow("TaharezLook/FrameWindow", "MainMenu_mLogin_process");
	mLogin_process->setText("");
	mLogin_process->setSize(CEGUI::USize(CEGUI::UDim(0.30, 0), CEGUI::UDim(0.20, 0)));
	mLogin_process->setPosition( CEGUI::UVector2(CEGUI::UDim(0.325, 0), CEGUI::UDim(0.4, 0) ) );
	mLogin_process->hide();


	/*** Notification window for various messages ***/
	mNotification = windowManager->createWindow("TaharezLook/FrameWindow", "MainMenu_notification");
	mNotification->setText("  Notification");
	mNotification->setSize(CEGUI::USize(CEGUI::UDim(0.33, 0), CEGUI::UDim(0.3, 0)));
	mNotification->setPosition( CEGUI::UVector2(CEGUI::UDim(0.33, 0), CEGUI::UDim(0.4, 0) ) );
	mNotification->hide();

	// Make notification Label;
	mNotificationLabel = static_cast<CEGUI::MultiLineEditbox*>( windowManager->createWindow("TaharezLook/MultiLineEditbox", "MainMenu_notification_label") );
	mNotificationLabel->setSize(CEGUI::USize(CEGUI::UDim(0.8, 0), CEGUI::UDim(0.475, 0)));
	mNotificationLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.2, 0) ) );
	mNotificationLabel->setText("");
	// Disable frame and background;
	mNotificationLabel->setWordWrapping( true );
	mNotificationLabel->setReadOnly( true );
	// Disable the label so it doesn't steal the events;
	//mNotificationLabel->disable();

	mNotificationOkay = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "MainMenu_notification_okay") );
	mNotificationOkay->setText("Okay");
	mNotificationOkay->setSize(CEGUI::USize(CEGUI::UDim(0.3, 0), CEGUI::UDim(0.175, 0)));
	mNotificationOkay->setPosition( CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.725, 0) ) );
	mNotificationOkay->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &MainMenu::event_notification_okay, this ) );


	/**************/
	/*** Events ***/
	/**************/
	mQuit->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &MainMenu::event_quit, this ) );
	mLogin->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &MainMenu::event_login, this ) );


	/**********************************************************************************************/
	/*** For testing purposes we add the user name "test1" and password "test" to the editboxes ***/
	/**********************************************************************************************/
	mUsername_editbox->setText("test1");
	mPassword_editbox->setText("test");


	/*****************************/
	/*** Add the child windows ***/
	/*****************************/
	mNotification->addChild( mNotificationLabel );
	mNotification->addChild( mNotificationOkay );

	mainMenuWindow->addChild( mUserName_label );
	mainMenuWindow->addChild( mPassword_label );
	mainMenuWindow->addChild( mUsername_editbox );
	// FIXME: 
	//////mainMenuWindow->addChild( mPassword_checkbox );
	mainMenuWindow->addChild( mPassword_editbox );
	mainMenuWindow->addChild( mQuit );
	mainMenuWindow->addChild( mLogin );
	mainMenuWindow->addChild( mLogin_process );
	mainMenuWindow->addChild( mNotification );

	parentWindow->addChild(mainMenuWindow);

	/****************/
	/*** Settings ***/
	/****************/

	// Set the background image;
	mainMenuWindow->setProperty("Image", DEF_MAINMENU_BGIMAGESET_NAME);

	// In order to set focus to the editbox on startup, we have to activate the window first then the editbox;
	mainMenuWindow->activate();
	mUsername_editbox->activate();

	// Set max text length;
	mUsername_editbox->setMaxTextLength(DEF_CLIENTGLOBALS_MAX_USERNAME_LENGHT);
	mPassword_editbox->setMaxTextLength(DEF_CLIENTGLOBALS_MAX_PASSWORD_LENGHT);



	//////////////////////////
	/// Fire up some music ///
	//////////////////////////
	themeSong = SoundManager::getSingletonPtr()->createSound(DEF_CLIENTGLOBALS_THEMESONG_FILENAME);
	themeSong->play();



	mbConnecting = false;
	Network::getSingleton().clientQuit(false);
}

MainMenu::~MainMenu()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();

	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::Window* tmpWindow = sheet->getChild(DEF_MAINMENU_WINDOW_NAME);

	//Destory the Window // 
	windowManager->destroyWindow(tmpWindow);

	// Destroy the imageset and texture //
	imageManager->destroy(DEF_MAINMENU_BGIMAGESET_NAME);
	imageManager->destroyImageCollection(DEF_MAINMENU_BGIMAGESET_NAME);
}




bool MainMenu::event_quit(const CEGUI::EventArgs &e)
{
	// Change the Game state to quit;
	ClientStateManager::getSingletonPtr()->setState(AS_QUIT);
	return true;
}

bool MainMenu::event_login(const CEGUI::EventArgs &e)
{
	#ifdef DEF_CLIENTGLOBALS_MAINMENU_BYPASS_SERVER_HACK
		ClientStateManager::getSingletonPtr()->setState(AS_GAME);
		return false;
	#endif

	if( mPassword_editbox->getText() == "" )
	{
		notify( "Password field cannot be blank." );
		return false;
	}
	else if( mUsername_editbox->getText() == "" )
	{
		notify( "Username field cannot be blank." );
		return false;
	}

	Network* const network = Network::getSingletonPtr();
	network->insert_login_info( std::string( mUsername_editbox->getText().c_str() ), std::string( mPassword_editbox->getText().c_str() ) );

	network->connectToMasterServer();

	// Notify the user that it's attempting to connect to the master server.
	connectingNotify();


	return true;
}
 



bool MainMenu::event_notification_okay( const CEGUI::EventArgs &e )
{
	mNotification->hide();

	if (mbConnecting)
	{
		Network* const network = Network::getSingletonPtr();
		network->cancelMasterServerConnect();
		mbConnecting = false;
	}

	return true;
}

void MainMenu::notify( std::string text )
{
	mNotificationOkay->setText( "Okay" );
    mNotificationLabel->setText( text );
    mNotification->show();
	mNotification->activate();
}

void MainMenu::connectingNotify()
{
	mNotificationLabel->setText( "Connecting to master server..." );
	mNotificationOkay->setText( "Cancel" );
	mNotification->show();
	mNotification->activate();
	mbConnecting = true;
}


