/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CharacterMenu.h"
#include "CeguiManager.h"
#include "ClientStateManager.h"
#include "Network.h"
#include "NetworkStructures.h"


#define DEF_CHARACTERMENU_WINDOW_NAME		"CharacterMenu_WindowName"
#define DEF_CHARACTERMENU_BGIMAGESET_NAME	"CharacterMenu_bgImage"
#define DEF_CHARACTERMENU_BGIMAGE_FILENAME	"MenuBackground.jpg"


using namespace std;



CharacterMenu::CharacterMenu()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();
	
	CEGUI::Window* const parentWindow = ceguiManager->getRootWindow();

	Network* const network = Network::getSingletonPtr();

	CEGUI::Window* mainCharacterMenuWindow;




	/// Create the background image ///
	CEGUI::ImageManager::getSingleton().addFromImageFile(DEF_CHARACTERMENU_BGIMAGESET_NAME, DEF_CHARACTERMENU_BGIMAGE_FILENAME);


	// mainCharacterMenuWindow;
	mainCharacterMenuWindow = windowManager->createWindow("TaharezLook/StaticImage", DEF_CHARACTERMENU_WINDOW_NAME);
	mainCharacterMenuWindow->setSize(CEGUI::USize(CEGUI::UDim(1.0, 0), CEGUI::UDim(1.0, 0)));
	mainCharacterMenuWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0, 0), CEGUI::UDim(0.0, 0)));
	// Set the background image;
	mainCharacterMenuWindow->setProperty("Image", DEF_CHARACTERMENU_BGIMAGESET_NAME);


	// mJoin;
	mJoin = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "CharacterMenu_mJoin") );
	mJoin->setText("Join");
	mJoin->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mJoin->setPosition( CEGUI::UVector2(CEGUI::UDim(0.425, 0), CEGUI::UDim(0.8, 0) ) );


	// mBack;
	mBack = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "CharacterMenu_mBack") );
	mBack->setText("Back");
	mBack->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mBack->setPosition( CEGUI::UVector2(CEGUI::UDim(0.7, 0), CEGUI::UDim(0.8, 0) ) );


	// mCharacter_listbox
	mCharacter_listbox = static_cast<CEGUI::Listbox*>( windowManager->createWindow("TaharezLook/Listbox", "CharacterMenu_mCharacter_listbox") );
	mCharacter_listbox->setSize(CEGUI::USize(CEGUI::UDim(0.30, 0), CEGUI::UDim(0.50, 0)));
	mCharacter_listbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.25, 0) ) );


	/**************/
	/*** Events ***/
	/**************/
	mJoin->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &CharacterMenu::event_join, this ) );
	mBack->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &CharacterMenu::event_back, this ) );


	/************************************************/
	/*** Set the listbox with the Character names ***/
	/************************************************/
	CEGUI::ListboxTextItem *item = new CEGUI::ListboxTextItem( "Character Name                  " );
	item->setDisabled( true );
	mCharacter_listbox->addItem( item );
	mList.push_back( item );

	CEGUI::ListboxTextItem *item1 = new CEGUI::ListboxTextItem( "                                " );
	item1->setDisabled( true );
	mCharacter_listbox->addItem( item1 );
	mList.push_back( item1 );
	 

	if (network)
	{
	  for (int indx = 0; indx < (int) network->character_list.size(); indx++)
		{
			CEGUI::ListboxTextItem *item = new CEGUI::ListboxTextItem(network->character_list[indx]->name);
			
			// Set the selection image //
			const CEGUI::Image* selectImage = &CEGUI::ImageManager::getSingleton().get("TaharezLook/TextSelectionBrush");
			item->setSelectionBrushImage(selectImage);
			 
			mCharacter_listbox->addItem( item );
			mList.push_back( item );
		}

		// If the character list size is greater than '0' than select the first character.
		// The Index starts at '2' because there are two unused indexes above it.
		if ( network->character_list.size() > 0 )
		{
			mCharacter_listbox->setItemSelectState (size_t (2), true);
		}
	}


	/***********************/
	/*** Add the windows ***/
	/***********************/
	mainCharacterMenuWindow->addChild( mJoin );
	mainCharacterMenuWindow->addChild( mBack );
	mainCharacterMenuWindow->addChild(mCharacter_listbox);

	parentWindow->addChild(mainCharacterMenuWindow);
}

CharacterMenu::~CharacterMenu()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	// FIXME:
	//CEGUI::ImagesetManager* const imagesetManager = ceguiManager->getImagesetManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();

	Network* const network = Network::getSingletonPtr();


	// reset the list;
	mCharacter_listbox->resetList();


	if (network)
	{
	  for (int indx = 0; indx < (int) network->character_list.size(); indx++)
		{
			delete network->character_list[indx];
		}

		network->character_list.clear();
	}


	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::Window* tmpWindow = sheet->getChild(DEF_CHARACTERMENU_WINDOW_NAME);
	
	// Destroy the window //
	windowManager->destroyWindow(tmpWindow);

	// Destroy the imageset and the texture //
	imageManager->destroy(DEF_CHARACTERMENU_BGIMAGESET_NAME);
	imageManager->destroyImageCollection(DEF_CHARACTERMENU_BGIMAGESET_NAME);
}
 
bool CharacterMenu::event_join(const CEGUI::EventArgs &e)
{
	return sendEnter();
}

bool CharacterMenu::event_back(const CEGUI::EventArgs &e)
{
	return sendEscape();
}

bool CharacterMenu::sendEnter()
{
	Network* const network = Network::getSingletonPtr();


	if( mCharacter_listbox->getFirstSelectedItem() != NULL && mCharacter_listbox->getFirstSelectedItem()->getText() != "" )
	{
		if (network)
		{
			network->joinConnectedServer( std::string( mCharacter_listbox->getFirstSelectedItem()->getText().c_str() ) );
		}
	}

	return true;
}

// Handle Escape key;
bool CharacterMenu::sendEscape()
{
	Network* const network = Network::getSingletonPtr();


	if (network)
	{
		network->disconnect();
	}


	ClientStateManager::getSingletonPtr()->setState( AS_MAIN_MENU );


	return true;
}
