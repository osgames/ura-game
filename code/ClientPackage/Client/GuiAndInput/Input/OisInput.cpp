/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "OisInput.h"
#include <Ogre.h>
#include "Ogre3dManager.h"
#include "CeguiManager.h"

  

OisInput::OisInput()
{
	// Get the GUIContext //
	mGuiContext = CeguiManager::getSingleton().getGUIContext();

	// Get the render window //
	Ogre::RenderWindow* const window = Ogre3dManager::getSingletonPtr()->getRenderWindow();


	// Create the InputKeyCollector //
	mInputKeyCollector = new InputKeyCollector();

	// Create the InputMouseCollector //
	mInputMouseCollector = new InputMouseCollector();

	 
	OIS::ParamList parameters;
	std::ostringstream window_handle_string;
	size_t window_handle;
	unsigned int width, height, depth = 0;
	int left, top = 0;

	// Get a handle to the window (Unified now, no need for OS specific)
	window->getCustomAttribute("WINDOW", &window_handle);

	window_handle_string << window_handle;
	parameters.insert (std::make_pair (std::string ("WINDOW"), window_handle_string.str ()));

	// Turn on keyboard autorepeat for linux;
	/*******************************************/
	// !!!NOTE!!!: Auto repeat causes the keyPressed and keyReleased functions to execute quickly over and over and
                    // generates other bugs in-game.
    /***************************************************/
//	#if defined OIS_LINUX_PLATFORM
//		//parameters.insert( std::make_pair( std::string( "XAutoRepeatOn" ), std::string( "true" ) ) );
//	#endif
//
//	#ifdef WINDOWED
//	  #if defined OIS_WIN32_PLATFORM
//	  parameters.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
//	  parameters.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
//	  parameters.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
//	  parameters.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
//	#endif
//	  #endif
//#ifdef DEBUG
//#ifdef OIS_LINUX_PLATFORM
//#ifndef DEF_CLIENTGLOBALS_URA_LOCK_MOUSE
//	  parameters.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
//	  parameters.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
//	  parameters.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
//	  /*******************************************/
//       // !!!NOTE!!!: Auto repeat causes the keyPressed and keyReleased functions to execute quickly over and over and
//                      // generates other bugs in-game.
//        /***************************************************/
//	  //parameters.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
//#endif
//#endif
//#endif

	// Initialise the OIS input manager
	mOIS_InputManager = OIS::InputManager::createInputSystem (parameters);

	// Enable buffered keyboard support
	mOIS_keyboard = static_cast<OIS::Keyboard*>(mOIS_InputManager->createInputObject (OIS::OISKeyboard, true));

	// Enable buffered mouse support
	mOIS_Mouse = static_cast<OIS::Mouse*>(mOIS_InputManager->createInputObject (OIS::OISMouse, true));


	// Define the renderWindow outline for our mouse
	window->getMetrics (width, height, depth, left, top);


	// Get the mouse state and set the width and height;
	const OIS::MouseState& mouse_state = mOIS_Mouse->getMouseState();
	mouse_state.width = width;
	mouse_state.height = height;


	/*** Set this class as the callback class ***/
	mOIS_keyboard->setEventCallback (this);
	mOIS_Mouse->setEventCallback (this);
}
 
OisInput::~OisInput()
{
	// Destroying the manager will cleanup unfreed devices //
	if (mOIS_InputManager)
	{
		OIS::InputManager::destroyInputSystem(mOIS_InputManager);
	}

	if (mInputKeyCollector)
	{
		delete mInputKeyCollector;
		mInputKeyCollector = NULL;
	}

	if (mInputMouseCollector)
	{
		delete mInputMouseCollector;
		mInputMouseCollector = NULL;
	}
}

  
bool OisInput::keyPressed (const OIS::KeyEvent& arg)
{
	// Tell mInputKeyCollector what keys were dealing with //
	mInputKeyCollector->setKeyPressed( static_cast<InputKeyCollector::KEY>(arg.key) );

	/// Cegui Inject keyDown and Char ///
	mGuiContext->injectKeyDown(static_cast<CEGUI::Key::Scan>(arg.key));
	mGuiContext->injectChar(arg.text);

	return true;
}


bool OisInput::keyReleased (const OIS::KeyEvent& arg)
{
	// Tell mInputKeyCollector what keys were dealing with //
	mInputKeyCollector->setKeyReleased( static_cast<InputKeyCollector::KEY>(arg.key) );

	// Cegui Inject KeyUp;
	mGuiContext->injectKeyUp(static_cast<CEGUI::Key::Scan>(arg.key));

	return true;
}

bool OisInput::mouseMoved(const OIS::MouseEvent& arg)
{
	/// Set the cegui mouse moved ///
	mGuiContext->injectMouseMove(arg.state.X.rel, arg.state.Y.rel);

	/// Set the InputMouseCollector mouse moved ///
	mInputMouseCollector->setMoving(arg.state.X.rel, arg.state.Y.rel);
	

/***************************
	/////////////////////////////
	/// Handle the mousewheel ///
	/////////////////////////////


	// If the scroll values are bigger than or less than zero than we scroll. Else it was just a mouse move;
	if(arg.state.Z.rel > 0)
	{
		// We scroll away from the user;
		mGuiContext->injectMouseWheelChange(+1);
		/////////mGUI->mouseWheelForward();
	}
	else if(arg.state.Z.rel < 0)
	{
		// Were scrowing towards the user;
		mGuiContext->injectMouseWheelChange(-1);
		/////////mGUI->mouseWheelBackward();
	}
*******************************/

	return true;
}

bool OisInput::mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	/// Set the cegui mouse pressed ///
	mGuiContext->injectMouseButtonDown( _convertToCeguiMouseButton(id) );
	
	/// Set the mInputMouseCollector mouse pressed ///
	if (id == OIS::MB_Left)
	{
		mInputMouseCollector->setLeftButtonPressed();
	}
	else if (id == OIS::MB_Right)
	{
		mInputMouseCollector->setRightButtonPressed();
	}
		
	return true;
}

bool OisInput::mouseReleased (const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	/// Set the cegui mouse released ///
	mGuiContext->injectMouseButtonUp( _convertToCeguiMouseButton(id) );

	/// Set the mInputMouseCollector mouse released ///
	if (id == OIS::MB_Left)
	{
		mInputMouseCollector->setLeftButtonReleased();
	}
	else if (id == OIS::MB_Right)
	{
		mInputMouseCollector->setRightButtonReleased();
	}

	/// Set the mouse to not moving ///
	mInputMouseCollector->setNotMoving();
	  
	return true;
}



OIS::Keyboard* const OisInput::getKeyboard() const
{
	return mOIS_keyboard;
}

OIS::Mouse* const OisInput::getMouse() const
{
	return mOIS_Mouse;
}

CEGUI::MouseButton OisInput::_convertToCeguiMouseButton(OIS::MouseButtonID buttonID) const
{
	switch (buttonID)
	{
		case OIS::MB_Left:
			return CEGUI::LeftButton;
		case OIS::MB_Right:
			return CEGUI::RightButton;
		case OIS::MB_Middle:
			return CEGUI::MiddleButton;
		default:
			return CEGUI::LeftButton;
	}
}



void OisInput::update(const float timeSinceLastFrame) const
{
	/// Inject a cegui time pulse which is needed for tooltips and ect ///
	CeguiManager::getSingletonPtr()->injectTimePulse(timeSinceLastFrame);

	/// Turn off the Mouse moving, we don't want it on forever ///
	mInputMouseCollector->setNotMoving();

	/// Update the keyboard and mouse state ///
	mOIS_keyboard->capture();
	mOIS_Mouse->capture();


}

InputKeyCollector* const OisInput::getInputKeyCollector() const
{
	return mInputKeyCollector;
}

InputMouseCollector* const OisInput::getMouseKeyCollector() const
{
	return mInputMouseCollector;
}