/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __INPUTMOUSECOLLECTOR_H__
#define __INPUTMOUSECOLLECTOR_H__


#include <OIS/OIS.h>


class InputMouseCollector
{
protected:
	bool mbLeftButtonPressed;
	bool mbRightButtonPressed;
	bool mbMoving;

	int mMoveRelativeX;
	int mMoveRelativeY;
public:
	InputMouseCollector();

	void setLeftButtonPressed();
	void setRightButtonPressed();
	void setLeftButtonReleased();
	void setRightButtonReleased();
	void setNotMoving();
	void setMoving(const int relativeX, const int relativeY);

	bool isButtonPressed() const;
	bool isLeftButtonPressed() const;
	bool isRightButtonPressed() const;
	bool isMoving() const;

	int getMoveRelativeX() const;
	int getMoveRelativeY() const;
};


#endif