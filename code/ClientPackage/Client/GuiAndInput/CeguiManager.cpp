/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CeguiManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace std;


template<> CeguiManager* Ogre::Singleton<CeguiManager>::msSingleton = 0;

CeguiManager* const CeguiManager::getSingletonPtr()
{
	return msSingleton;
}



CeguiManager::CeguiManager()
{
	mGUIContext = NULL;
	mRenderer = NULL;
	mSystem = NULL;
	mRootWindow = NULL;
	 
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::RenderWindow* const renderWindow = ogre3dManager->getRenderWindow();
	Ogre::RenderTarget* renderTarget = renderWindow;
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	string strErrorFunction = "CeguiManager::CeguiManager()";
	string strErrorWordNameOrMessage;

	 

	try
	{
		/// Create the cegui Ogre Renderer. It doesn't need to be deleted ///
		//mRenderer = &CEGUI::OgreRenderer::create();
		mRenderer = &CEGUI::OgreRenderer::bootstrapSystem(*renderTarget);

		
		/// Store the system pointer /// 
		mSystem = CEGUI::System::getSingletonPtr();


		// tell us a lot about what is going on (see CEGUI.log in the working directory)
		CEGUI::Logger::getSingleton().setLoggingLevel(CEGUI::Informative);
 
		
		/// Load the Scheme ///
		//CEGUI::SchemeManager::getSingleton().createFromFile((CEGUI::utf8*)"TaharezLookSkin.scheme");
		CEGUI::SchemeManager::getSingleton().createFromFile((CEGUI::utf8*)"TaharezLook.scheme");
 

		/// Store the Default Gui context. We do it here because up above will crash ///
		mGUIContext = &mSystem->getDefaultGUIContext();


		/// Set the default font ///
		CEGUI::Font& defaultFont = CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-12.font");
		mGUIContext->setDefaultFont(&defaultFont);

		 

		/// There is no root window so lets create one ///
		mRootWindow = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Root");
		

		 
		//// Setup mouse auto-repeat for the root window ///
		mRootWindow->setMouseAutoRepeatEnabled(true);
		mRootWindow->setAutoRepeatRate(100);


		/// install this as the root GUI sheet ///
		mGUIContext->setRootWindow(mRootWindow);



		/// Setup the mCeguiCursorManager ///
		mCeguiCursorManager = new CeguiCursorManager;
	}
	catch(CEGUI::Exception e)
	{
		strErrorWordNameOrMessage = e.getMessage().c_str();
		ogre3dLogManager->logErrorMessage(strErrorFunction, strErrorWordNameOrMessage);
	}
	 

	/*** If there was an error allocating memory than log it ***/
	if (!mCeguiCursorManager)
	{
		strErrorWordNameOrMessage = "mCeguiCursorManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordNameOrMessage);
	}
} 

CeguiManager::~CeguiManager()
{
	// Destroy the CeguiCursorManager;
	if (mCeguiCursorManager)
	{
		delete mCeguiCursorManager;
		mCeguiCursorManager = NULL;
	}

	// Destroy the cegui root window;
	if (mRootWindow)
	{
		CEGUI::WindowManager::getSingleton().destroyWindow(mRootWindow);
		mRootWindow = NULL;
	}
}



CEGUI::GUIContext* const CeguiManager::getGUIContext() const
{
	return mGUIContext;
}


CEGUI::OgreRenderer* const CeguiManager::getRenderer() const
{
	return mRenderer;
}


CEGUI::System* const CeguiManager::getSystem() const
{
	return mSystem;
}

CEGUI::WindowManager* const CeguiManager::getWindowManager() const
{
	return CEGUI::WindowManager::getSingletonPtr();
}

CEGUI::ImageManager* const CeguiManager::getImagesetManager() const
{
	return CEGUI::ImageManager::getSingletonPtr();
}

CeguiCursorManager* const CeguiManager::getCeguiCursorManager() const
{
	return mCeguiCursorManager;
}

CEGUI::Window* const CeguiManager::getRootWindow() const
{
	return mRootWindow;
}

 

void CeguiManager::injectTimePulse(const float timeElapsed) const
{
	if (mSystem)
	{
		mSystem->injectTimePulse(timeElapsed);
	}
}



CEGUI::Window* CeguiManager::createWindow(CEGUI::String type, CEGUI::String name) const
{
	return CEGUI::WindowManager::getSingletonPtr()->createWindow(type, name);
}

void CeguiManager::destroyWindow(CEGUI::Window* const window) const
{
	if (window)
	{
		CEGUI::WindowManager::getSingletonPtr()->destroyWindow(window);
	}
}



void CeguiManager::addChildToRootWindow(CEGUI::Window* const window) const
{
	mRootWindow->addChild(window);
}