/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CEGUI_MANAGER_H__
#define __CEGUI_MANAGER_H__

 
#include <Ogre.h>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>

#include "CeguiCursorManager.h"


 
class CeguiManager : public Ogre::Singleton<CeguiManager>
{
protected:
	CEGUI::GUIContext* mGUIContext;
	CEGUI::OgreRenderer* mRenderer;
	CEGUI::System* mSystem;
	CEGUI::Window* mRootWindow;
	   
	CeguiCursorManager* mCeguiCursorManager;
public:
	CeguiManager();
	~CeguiManager();

	static CeguiManager* const getSingletonPtr();

	void injectTimePulse(const float timeElapsed) const;

	CeguiCursorManager* const getCeguiCursorManager() const;
	 
	CEGUI::GUIContext* const getGUIContext() const;
	CEGUI::OgreRenderer* const getRenderer() const;
	CEGUI::Window* const getRootWindow() const;
	CEGUI::System* const getSystem() const;
	CEGUI::ImageManager* const getImagesetManager() const;	
	CEGUI::WindowManager* const getWindowManager() const;

	CEGUI::Window* createWindow(CEGUI::String type, CEGUI::String name) const;
	void destroyWindow(CEGUI::Window* const window) const;

	void addChildToRootWindow(CEGUI::Window* const window) const;
};


#endif 

