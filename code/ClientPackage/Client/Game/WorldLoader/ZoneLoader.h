/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ZONELOADER_H__
#define __ZONELOADER_H__


#include <string>
#include <vector>
#include <Ogre.h>

#include "TerrainManager.h"

class CDotScene;

 
/* Necessary to prevent ODE related bug */
struct dxWorld;		/* dynamics world */
struct dxSpace;		/* collision space */
struct dxBody;		/* rigid body (dynamics object) */
struct dxGeom;		/* geometry (collision object) */
struct dxJoint;
struct dxJointNode;
struct dxJointGroup;

typedef struct dxWorld *dWorldID;
typedef struct dxSpace *dSpaceID;
typedef struct dxBody *dBodyID;
typedef struct dxGeom *dGeomID;
typedef struct dxJoint *dJointID;
typedef struct dxJointGroup *dJointGroupID;
/* End of hack */

class ZoneLoader
{
private:
	TerrainManager* mTerrainManager;
	CDotScene *mDotSceneLoader;

	std::vector<Ogre::SceneNode *> scene_node_list;
	std::vector<Ogre::Entity *> entity_list;
	std::vector<Ogre::Light *> light_list;
	Ogre::SceneNode *parent_map_node;

    	std::vector<dGeomID> ode_geom;
    	dGeomID ode_ray;
public:
	ZoneLoader();
	~ZoneLoader();
	bool loadZone(std::string file);
	void loadTestZone();
	bool getHeightAt(Ogre::Vector3 &position);
	bool loadModel(std::string file,
                    Ogre::Vector3 offset = Ogre::Vector3(0,0,0),
                    Ogre::Vector3 scale = Ogre::Vector3(1,1,1),
                    Ogre::Quaternion rotation = Ogre::Quaternion(1,0,0,0) );
	bool loadPhysicsModelSeries( std::string mesh_file_name,
					Ogre::Vector3 offset,
					Ogre::Vector3 scale,
					Ogre::Quaternion rotation );
	void frameStart( Ogre::Real timeSinceLastFrame );
	void frameEnd( Ogre::Real timeSinceLastFrame );

	TerrainManager* const getTerrainManager()
	{
		return mTerrainManager;
	}
};

#endif
