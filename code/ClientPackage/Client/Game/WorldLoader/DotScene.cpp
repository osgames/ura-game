/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*------------------------------------------------------------------------------
*  DotScene.cpp
*
*  This file provides some methods for the parsing of a .scene file, with
*  support for userData.
*------------------------------------------------------------------------------*/

#include "DotScene.h"
#include "Ogre3dManager.h"
#include "ZoneLoader.h"
#include "TerrainManager.h"

#include <boost/lexical_cast.hpp>

//using namespace std;
using namespace Ogre;

static Light* LoadLight( TiXmlElement *XMLLight )
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	TiXmlElement *XMLDiffuse, *XMLSpecular, *XMLAttentuation, *XMLPosition;

	// Create a light (point | directional | spot | radPoint)
	Light* l = sceneManager->createLight( XMLLight->Attribute("name") );
	if( !XMLLight->Attribute("type") || String(XMLLight->Attribute("type")) == "point" )
		l->setType( Light::LT_POINT );
	else if( String(XMLLight->Attribute("type")) == "directional")
		l->setType( Light::LT_DIRECTIONAL );
	else if( String(XMLLight->Attribute("type")) == "spot")
		l->setType( Light::LT_SPOTLIGHT );
	else if( String(XMLLight->Attribute("type")) == "radPoint")
		l->setType( Light::LT_POINT );

	XMLDiffuse = XMLLight->FirstChildElement("colourDiffuse");
	if( XMLDiffuse ){
		ColourValue Diffuse;
		Diffuse.r = Ogre::StringConverter::parseReal( XMLDiffuse->Attribute("r") );
		Diffuse.g = Ogre::StringConverter::parseReal( XMLDiffuse->Attribute("g") );
		Diffuse.b = Ogre::StringConverter::parseReal( XMLDiffuse->Attribute("b") );
		Diffuse.a = 1;
		l->setDiffuseColour(Diffuse);
	}
	XMLSpecular = XMLLight->FirstChildElement("colourSpecular");
	if( XMLSpecular ){
		ColourValue Specular;
		Specular.r = Ogre::StringConverter::parseReal( XMLSpecular->Attribute("r") );
		Specular.g = Ogre::StringConverter::parseReal( XMLSpecular->Attribute("g") );
		Specular.b = Ogre::StringConverter::parseReal( XMLSpecular->Attribute("b") );
		Specular.a = 1;
		l->setSpecularColour(Specular);
	}

	XMLAttentuation = XMLLight->FirstChildElement("lightAttenuation");
	if( XMLAttentuation )
	{
		//get defaults incase not all values specified
		Real range, constant, linear, quadratic;
		range = l->getAttenuationRange();
		constant = l->getAttenuationConstant();
		linear = l->getAttenuationLinear();
		quadratic = l->getAttenuationQuadric();

		if( XMLAttentuation->Attribute("range") )
			range = StringConverter::parseReal( XMLAttentuation->Attribute("range") );
		if( XMLAttentuation->Attribute("constant") )
			constant = StringConverter::parseReal( XMLAttentuation->Attribute("constant") );
		if( XMLAttentuation->Attribute("linear") )
			linear = StringConverter::parseReal( XMLAttentuation->Attribute("linear") );
		if( XMLAttentuation->Attribute("quadratic") )
			quadratic = StringConverter::parseReal( XMLAttentuation->Attribute("quadratic") );

		l->setAttenuation( range, constant, linear, quadratic );
	}

	XMLPosition = XMLLight->FirstChildElement("position");
	if( XMLPosition ) {
		Vector3 p = Vector3(0,0,0);
		if( XMLPosition->Attribute("x") )
			p.x = StringConverter::parseReal( XMLPosition->Attribute("x") );
		if( XMLPosition->Attribute("y") )
			p.y = StringConverter::parseReal( XMLPosition->Attribute("y") );
		if( XMLPosition->Attribute("z") )
			p.z = StringConverter::parseReal( XMLPosition->Attribute("z") );

		l->setPosition( p );
	}

	//castShadows      (true | false) "true"
	l->setCastShadows( true );
	if( XMLLight->Attribute("castShadows") )
		if( String(XMLLight->Attribute("castShadows")) == "false" )
			l->setCastShadows( false );

	//visible         (true | false) "true"
	l->setVisible( true );
	if( XMLLight->Attribute("visible") )
		if( String(XMLLight->Attribute("visible")) == "false" )
			l->setVisible( false );

	return l;
}

bool CDotScene::parseDotScene(ZoneLoader* const zoneLoader,
		const String &SceneName, const String& groupName, SceneNode *pAttachNode, String sPrependNode)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	// set up shared object values
	m_sPrependNode = sPrependNode;

	TiXmlDocument   *XMLDoc;
	TiXmlElement   *XMLRoot, *XMLNodes, *XMLTerrain;


	try
	{
		DataStreamPtr pStream = ResourceGroupManager::getSingleton().
			openResource( SceneName, groupName );

		String data = pStream->getAsString();
		// Open the .scene File
		XMLDoc = new TiXmlDocument();
		XMLDoc->Parse( data.c_str() );
		pStream->close();
		pStream.setNull();

		if( XMLDoc->Error() )
		{
			//We'll just log, and continue on gracefully
			LogManager::getSingleton().logMessage("[dotSceneLoader] The TiXmlDocument reported an error");
			delete XMLDoc;
			return false;
		}
	}
	catch(...)
	{
		//We'll just log, and continue on gracefully
		LogManager::getSingleton().logMessage("[dotSceneLoader] Error creating TiXmlDocument");
		delete XMLDoc;
		return false;
	}



	// Try to continue on Gracefully if there are errors such as duplicate scene node names;
	try
	{
		// Validate the File
		XMLRoot = XMLDoc->RootElement();
		if( String( XMLRoot->Value()) != "scene"  ) {
			LogManager::getSingleton().logMessage( "[dotSceneLoader]Error: Invalid .scene File. Missing <scene>" );
			delete XMLDoc;      
			return false;
		}
	


		// figure out where to attach any nodes we create
		if(pAttachNode == NULL)
		{
			pAttachNode = sceneManager->getRootSceneNode();
		}


		/*** Read in the scene nodes ***/
		XMLNodes = XMLRoot->FirstChildElement( "nodes" );
	
		if( XMLNodes )
		{
			processNode(XMLNodes->FirstChildElement( "node" ), zoneLoader, pAttachNode);
		}


		/*** Handle the Terrain data ***/
		XMLTerrain = XMLRoot->FirstChildElement( "terrains" );
	
		// Read in the scene nodes
		if( XMLTerrain )
		{
			// return false if there was an error reading the Terrain data;
			if ( processTerrain(XMLTerrain->FirstChildElement( "terrain" ), zoneLoader) == false )
			{
				return false;
			}
		}

	}
	catch(Ogre::Exception &e)
	{
		LogManager::getSingleton().logMessage("Client/DotScene.cpp/parseDotScene(): ");
		LogManager::getSingleton().logMessage( e.getFullDescription().c_str() );
		delete XMLDoc;
		return false;
	}
	catch(...)
	{
		LogManager::getSingleton().logMessage("Client/DotScene.cpp/parseDotScene(): An unknown error occured while reading in the file.");

		// Close the XML File
		delete XMLDoc;
		return false;
	}


	return true;
}

void CDotScene::processNode(TiXmlElement *XMLNode, ZoneLoader* const zoneLoader, SceneNode *pAttach)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	TiXmlElement *XMLPosition, *XMLRotation, *XMLScale,  *XMLEntity, *XMLBillboardSet,  *XMLLight, *XMLUserData;


	while( XMLNode )
	{
		// Process the current node
		// Grab the name of the node
		String NodeName = XMLNode->Attribute("name");
		// First create the new scene node
		SceneNode* NewNode = pAttach->createChildSceneNode( m_sPrependNode + NodeName );
		Vector3 TempVec;
		String TempValue;

		// Now position it...
		XMLPosition = XMLNode->FirstChildElement("position");
		if( XMLPosition ){
			TempValue = XMLPosition->Attribute("x");
			TempVec.x = StringConverter::parseReal(TempValue);
			TempValue = XMLPosition->Attribute("y");
			TempVec.y = StringConverter::parseReal(TempValue);
			TempValue = XMLPosition->Attribute("z");
			TempVec.z = StringConverter::parseReal(TempValue);
			NewNode->setPosition( TempVec );
		}

		// Rotate it...
		XMLRotation = XMLNode->FirstChildElement("rotation");
		if( XMLRotation ){
			Quaternion TempQuat;
			TempValue = XMLRotation->Attribute("qx");
			TempQuat.x = StringConverter::parseReal(TempValue);
			TempValue = XMLRotation->Attribute("qy");
			TempQuat.y = StringConverter::parseReal(TempValue);
			TempValue = XMLRotation->Attribute("qz");
			TempQuat.z = StringConverter::parseReal(TempValue);
			TempValue = XMLRotation->Attribute("qw");
			TempQuat.w = StringConverter::parseReal(TempValue);
			NewNode->setOrientation( TempQuat );
		}

		// Scale it.
		XMLScale = XMLNode->FirstChildElement("scale");
		if( XMLScale ){
			TempValue = XMLScale->Attribute("x");
			TempVec.x = StringConverter::parseReal(TempValue);
			TempValue = XMLScale->Attribute("y");
			TempVec.y = StringConverter::parseReal(TempValue);
			TempValue = XMLScale->Attribute("z");
			TempVec.z = StringConverter::parseReal(TempValue);
			Ogre3dManager::getSingletonPtr()->setSceneNodeScale(NewNode, TempVec.x, TempVec.y, TempVec.z);
		}

		XMLLight = XMLNode->FirstChildElement( "light" );
		if( XMLLight )
			NewNode->attachObject( LoadLight(XMLLight) );

		// Check for an Entity
		XMLEntity = XMLNode->FirstChildElement("entity");
		if( XMLEntity )
		{
			String EntityName, EntityMeshFilename;
			EntityName = XMLEntity->Attribute( "name" );
			EntityMeshFilename = XMLEntity->Attribute( "meshFile" );

			// Create entity
			Entity* NewEntity = sceneManager->createEntity(EntityName, EntityMeshFilename);

                        //castShadows      (true | false) "true"
                        NewEntity->setCastShadows( true );
                        if( XMLEntity->Attribute("castShadows") )
                           if( String(XMLEntity->Attribute("castShadows")) == "false" )
                              NewEntity->setCastShadows( false );

                        NewNode->attachObject( NewEntity );

            zoneLoader->loadPhysicsModelSeries( EntityMeshFilename,
                                                 NewNode->getPosition(),
                                                 NewNode->getScale(),
                                                 NewNode->getOrientation() );
		}

		XMLBillboardSet = XMLNode->FirstChildElement( "billboardSet" );
		if( XMLBillboardSet )
		{
			String TempValue;

			BillboardSet* bSet = sceneManager->createBillboardSet( NewNode->getName() );

			BillboardType Type;
			TempValue = XMLBillboardSet->Attribute( "type" );
			if( TempValue == "orientedCommon" )
				Type = BBT_ORIENTED_COMMON;
			else if( TempValue == "orientedSelf" )
				Type = BBT_ORIENTED_SELF;
			else Type = BBT_POINT;

			BillboardOrigin Origin;
			TempValue = XMLBillboardSet->Attribute( "type" );
			if( TempValue == "bottom_left" )
				Origin = BBO_BOTTOM_LEFT;
			else if( TempValue == "bottom_center" )
				Origin = BBO_BOTTOM_CENTER;
			else if( TempValue == "bottomRight"  )
				Origin = BBO_BOTTOM_RIGHT;
			else if( TempValue == "left" )
				Origin = BBO_CENTER_LEFT;
			else if( TempValue == "right" )
				Origin = BBO_CENTER_RIGHT;
			else if( TempValue == "topLeft" )
				Origin = BBO_TOP_LEFT;
			else if( TempValue == "topCenter" )
				Origin = BBO_TOP_CENTER;
			else if( TempValue == "topRight" )
				Origin = BBO_TOP_RIGHT;
			else
				Origin = BBO_CENTER;

			bSet->setBillboardType( Type );
			bSet->setBillboardOrigin( Origin );


			TempValue = XMLBillboardSet->Attribute( "name" );
			bSet->setMaterialName( TempValue );

			int width, height;
			width = (int) StringConverter::parseReal( XMLBillboardSet->Attribute( "width" ) );
			height = (int) StringConverter::parseReal( XMLBillboardSet->Attribute( "height" ) );
			bSet->setDefaultDimensions( width, height );
			bSet->setVisible( true );
			NewNode->attachObject( bSet );

			TiXmlElement *XMLBillboard;

			XMLBillboard = XMLBillboardSet->FirstChildElement( "billboard" );

			while( XMLBillboard )
			{
				Billboard *b;
				// TempValue;
				TempVec = Vector3( 0, 0, 0 );
				ColourValue TempColour(1,1,1,1);

				XMLPosition = XMLBillboard->FirstChildElement( "position" );
				if( XMLPosition ){
					TempValue = XMLPosition->Attribute("x");
					TempVec.x = StringConverter::parseReal(TempValue);
					TempValue = XMLPosition->Attribute("y");
					TempVec.y = StringConverter::parseReal(TempValue);
					TempValue = XMLPosition->Attribute("z");
					TempVec.z = StringConverter::parseReal(TempValue);
				}

				TiXmlElement* XMLColour = XMLBillboard->FirstChildElement( "colourDiffuse" );
				if( XMLColour ){
					TempValue = XMLColour->Attribute("r");
					TempColour.r = StringConverter::parseReal(TempValue);
					TempValue = XMLColour->Attribute("g");
					TempColour.g = StringConverter::parseReal(TempValue);
					TempValue = XMLColour->Attribute("b");
					TempColour.b = StringConverter::parseReal(TempValue);
				}

				b = bSet->createBillboard( TempVec, TempColour);

				XMLBillboard = XMLBillboard->NextSiblingElement( "billboard" );
			}
		}

		XMLUserData = XMLNode->FirstChildElement( "userData" );
		if ( XMLUserData )
		{
			TiXmlElement *XMLProperty;
			XMLProperty = XMLUserData->FirstChildElement("property");
			while ( XMLProperty )
			{
				String first = NewNode->getName();
				String second = XMLProperty->Attribute("name");
				String third = XMLProperty->Attribute("data");
				String type = XMLProperty->Attribute("type");
				nodeProperty newProp(first,second,third,type);
				nodeProperties.push_back(newProp);
				XMLProperty = XMLProperty->NextSiblingElement("property");
			}
		}
//-->		/*
		/*		Added by @lpha_Max ! :-)
		*		"Global" vars :
		*			NewNode --> Node where attach the object...
		*			mSceneMgr --> SceneManager
		*/
		TiXmlElement* XMLCamera = XMLNode->FirstChildElement( "camera" );
		if ( XMLCamera )
		{
			String CameraName = XMLCamera->Attribute( "name" );
			Camera *cam = sceneManager->createCamera(CameraName);
			NewNode->attachObject(cam);
		}

		TiXmlElement* XMLParticle = XMLNode->FirstChildElement( "particleSystem" );
		if ( XMLParticle )
		{
			String ParticleName = XMLParticle->Attribute( "name" );
			String ParticleFile = XMLParticle->Attribute( "file" );
			ParticleSystem* ParticleS = sceneManager->createParticleSystem(ParticleName, ParticleFile);
			NewNode->attachObject(ParticleS);
                }
//<--
		TiXmlElement * ChildXMLNode;
		ChildXMLNode = XMLNode->FirstChildElement( "node" );
		if(ChildXMLNode)
			processNode(ChildXMLNode, zoneLoader, NewNode);	// recurse to do all my children

		XMLNode = XMLNode->NextSiblingElement( "node" ); // process my next sibling
	}
}

bool CDotScene::processTerrain(TiXmlElement *XMLNode, ZoneLoader* const zoneLoader)
{
	StringConverter sc;
	Ogre::String str;

	TerrainManager* const terrainManager = zoneLoader->getTerrainManager();

	TiXmlElement* XMLwidth;
	TiXmlElement* XMLheight;
	TiXmlElement* XMLheightMapData;
	TiXmlElement* XMLheightMapDataSize;
	TiXmlElement* XMLSplatMapFileName;
	TiXmlElement* XMLSplatMapTexture;
	TiXmlElement* XMLSplatMapRegularScales;
	TiXmlElement* XMLSplatMapDetailScales;

	std::string mapName;
	Ogre::Real mapWidth = 0;
	Ogre::Real mapHeight = 0;
	std::string mapHeightData;
	unsigned long long mapHeightDataSize = 0;

	std::string splatMapFileName;
	std::vector<std::string> vSplatMapTextures;
	Ogre::Vector4 vSplatRegularScales;
	Ogre::Vector4 vSplatDetailScales;




	if( XMLNode )
	{
		/****************/
		/*** Map name ***/
		/****************/

		mapName = XMLNode->Attribute("name");

		if (mapName == "")
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No map name." << std::endl;
			return false;
		}


		/*****************/
		/*** Map width ***/
		/*****************/

		XMLwidth = XMLNode->FirstChildElement("width");

		if (XMLwidth)
		{
			str = XMLwidth->Attribute("w");
			mapWidth = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain width." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): Terrain width is not a valid number." << std::endl;
				return false;
			}


			// If the number isn't bigger than zero than we have a problem;
			if ( mapWidth <= 0 )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): Terrain width is zero or less." << std::endl;
				return false;
			}
		}
		else
		{
				std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain width." << std::endl;
				return false;
		}


		/**********************/
		/*** Terrain height ***/
		/**********************/

		XMLheight = XMLNode->FirstChildElement("maxheight");

		if (XMLheight)
		{
			str = XMLheight->Attribute("mh");
			mapHeight = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain height." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): Terrain height is not a valid number." << std::endl;
				return false;
			}


			// If the number isn't bigger than zero than we have a problem;
			if ( mapHeight <= 0 )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): Terrain height is zero or less." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain height." << std::endl;
			return false;
		}


		/**********************/
		/*** Heightmap Data ***/
		/**********************/
		XMLheightMapData = XMLNode->FirstChildElement("heightMapData");

		if (XMLheightMapData)
		{
			mapHeightData = XMLheightMapData->Attribute("data");

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain height data." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain height data.." << std::endl;
			return false;
		}


		/***************************/
		/*** Heightmap Data Size ***/
		/***************************/
		XMLheightMapDataSize = XMLNode->FirstChildElement("heightMapDataSize");

		if (XMLheightMapDataSize)
		{
			str = XMLheightMapDataSize->Attribute("size");


			// Convert the string to a long long;
			mapHeightDataSize = boost::lexical_cast<unsigned long long>(str);


			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain mapHeightDataSize ." << std::endl;
				return false;
			}

			// If the number isn't bigger than zero than we have a problem;
			if ( mapHeightDataSize <= 0 )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): Terrain mapHeightDataSize is zero or less." << std::endl;
				return false;
			}
		}
		else
		{
				std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain mapHeightDataSize." << std::endl;
				return false;
		}


		/***************************/
		/*** Splat map file name ***/
		/***************************/

		XMLSplatMapFileName = XMLNode->FirstChildElement("splatMapFileName");

		if (XMLSplatMapFileName)
		{
			str = XMLSplatMapFileName->Attribute("fileName");

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No SplatMap file name." << std::endl;
				return false;
			}
			else
			{
				splatMapFileName = str;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain height." << std::endl;
			return false;
		}


		/**********************************/
		/*** Set the Splat map Textures ***/
		/**********************************/

		/* texture1 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture1");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No splatTexture1 file name." << std::endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}

		/* texture2 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture2");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No splatTexture2 file name." << std::endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}

		/* texture3 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture3");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No splatTexture3 file name." << std::endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}

		/* texture4 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture4");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): No splatTexture4 file name." << std::endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}



		/************************************/
		/*** Set the Splat Regular scales ***/
		/************************************/

		// splatRegularScales1;
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales1");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.x = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales1." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales1 is not a valid number." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales1." << std::endl;
			return false;
		}


		// splatRegularScales2
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales2");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.y = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales2." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales2 is not a valid number." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales2." << std::endl;
			return false;
		}


		// splatRegularScales3
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales3");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.z = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales3." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales3 is not a valid number." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales3." << std::endl;
			return false;
		}


		// splatRegularScales4
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales4");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.w = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales4." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales4 is not a valid number." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales4." << std::endl;
			return false;
		}



		/***********************************/
		/*** Set the Splat detail scales ***/
		/***********************************/

		// splatDetailScales1;
		XMLSplatMapDetailScales = XMLNode->FirstChildElement("splatDetailScales1");
		if (XMLSplatMapDetailScales)
		{
			str = XMLSplatMapDetailScales->Attribute("RealVal");
			vSplatDetailScales.x = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales1." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales1 is not a valid number." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No splatDetailScales1." << std::endl;
			return false;
		}


		// splatDetailScales2
		XMLSplatMapDetailScales = XMLNode->FirstChildElement("splatDetailScales2");
		if (XMLSplatMapDetailScales)
		{
			str = XMLSplatMapDetailScales->Attribute("RealVal");
			vSplatDetailScales.y = sc.parseReal(str);

			if (str == "")
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales2." << std::endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				std::cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales2 is not a valid number." << std::endl;
				return false;
			}
		}
		else
		{
			std::cout << "ERROR void CDotScene::processTerrain(...): No splatDetailScales2." << std::endl;
			return false;
		}


		/**********************************/
		/*** Set the terrain properties ***/
		/**********************************/
		if (terrainManager)
		{
			terrainManager->setNewTerrain(mapWidth, mapHeight, splatMapFileName);

			terrainManager->setHeightMapData(mapHeightData, mapHeightDataSize);

			terrainManager->setTextures( vSplatMapTextures.at(0), vSplatMapTextures.at(1),
				vSplatMapTextures.at(2), vSplatMapTextures.at(3) );

			terrainManager->setSplatScales(vSplatRegularScales);
			terrainManager->setDetailScales(vSplatDetailScales);
		}
	}
	else
	{
		std::cout << "ERROR void CDotScene::processTerrain(...): No Terrain tag." << std::endl;
		return false;
	}


	return true;
}

String CDotScene::getProperty(String ndNm, String prop)
{
	for ( unsigned int i = 0 ; i < nodeProperties.size(); i++ )
	{
		if ( nodeProperties[i].nodeName == ndNm && nodeProperties[i].propertyNm == prop )
		{
			return nodeProperties[i].valueName;
		}
	}
	return " ";
}

