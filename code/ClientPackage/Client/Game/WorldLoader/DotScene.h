/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*------------------------------------------------------------------------------
*  DotScene.h
*
*  This file provides some methods for the parsing of a .scene file, with
*  support for userData.
*------------------------------------------------------------------------------*/

#ifndef DOT_SCENE_H
#define DOT_SCENE_H

#include <Ogre.h>
#include <vector>

#include "../Dependencies/tinyxml/tinyxml.h"

class ZoneLoader;


class nodeProperty
{
public:
	Ogre::String nodeName;
	Ogre::String propertyNm;
	Ogre::String valueName;
	Ogre::String typeName;

	nodeProperty(Ogre::String node,Ogre::String propertyName,Ogre::String value,Ogre::String type)
	{
		nodeName = node;
		propertyNm = propertyName;
		valueName = value;
		typeName = type;
	}
};

class CDotScene
{
public:
//	CDotScene(ZoneLoader *zone_loader) : mZoneLoader(zone_loader) {}
	CDotScene()  {}
	virtual ~CDotScene() {}

	bool parseDotScene(ZoneLoader* const zoneLoader,
		const Ogre::String &SceneName, const Ogre::String& groupName, 
		Ogre::SceneNode *pAttachNode = NULL, Ogre::String sPrependNode = "");

	Ogre::String getProperty(Ogre::String ndNm, Ogre::String prop);

	std::vector<nodeProperty> nodeProperties;

protected:
	void processNode(TiXmlElement *XMLNode, ZoneLoader* const zoneLoader, Ogre::SceneNode *pAttach);
	bool processTerrain(TiXmlElement *XMLNode, ZoneLoader* const zoneLoader);

	Ogre::String m_sPrependNode;
	ZoneLoader *mZoneLoader;
};

#endif
