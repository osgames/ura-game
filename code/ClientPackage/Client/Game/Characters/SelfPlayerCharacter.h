/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SELFPLAYERCHARACTER_H__
#define __SELFPLAYERCHARACTER_H__

#include <string>
#include <Ogre.h>
  
#include "Game.h"
#include "MovableText.h"
#include "MovementDirection.h"
#include "CharacterMoveDirectionAnimation.h"
#include "SelfCharacterMoveDirectionSound.h"
#include "InputKeyCollector.h"

class Network;
class ClientStateManager;
class RayCastManager;



class SelfPlayerCharacter
{
public:
	/*
	 * Parameter "client" allows player to be either client (camera operations performed), or non-client.
	 */
	SelfPlayerCharacter();
	~SelfPlayerCharacter();


	Ogre::Quaternion last_orientation;
	Ogre::Vector3 last_position;


	// The main update functions //
	void updateInput();
	void update(const Ogre::Real& timeSinceLastFrame);

	// Deals with rotating the camera //
	void rotatePlayerAndCam(double direction);
	void pitchCamera(double direction);

	// Gets Orientations //
	Ogre::Quaternion getCameraOrientation();
	Ogre::Quaternion getTargetOrientation();
	Ogre::Quaternion getPlayerOrientation();

	Ogre::Vector3 getPosition();
	const std::string& getName() const;

	void setPosition( Ogre::Vector3 pos );
	void setOrientation( Ogre::Quaternion quat );
	void setMovementDirection(const URA::MovementDirection movementDirection);

	// Called when a network update of a player position occurs.
	void notifyNetworkUpdate();
protected:
	InputKeyCollector* mInputKeyCollector;
	InputMouseCollector* mInputMouseCollector;

	Ogre::Entity *mEntity;
	Ogre::SceneNode *mSceneNode;


	// Store the move direction //
	URA::MovementDirection mMoveDirection;

	// Deals with rotating the camera //
	double mCameraRotateX;
	double mCameraRotateY;

	
	/** Sec1: Only used if player is client-player. **/
	Ogre::SceneNode *mCameraNode;
	Ogre::SceneNode *mTargetNode;
	Ogre::SceneNode *mPitchTargetNode;
	Ogre::Vector3 mTargetNodeOffset;
	/** Sec1: End. **/

	Network *mNetwork;
	 
	CharacterMoveDirectionAnimation* mCharacterMoveDirectionAnimation;
	SelfCharacterMoveDirectionSound* mSelfCharacterMoveDirectionSound;


	double cam_dist_from_target;
	Ogre::Timer *mTimer;
	double time_between_orientation_change;
	double average_time_between_orientation_change;

	Ogre::Vector3 position;

#ifdef HEAVY_LOGGING
	Ogre::Timer *speed_timer;
	Ogre::Vector3 pos_init;
	Ogre::Vector3 pos_fin;
#endif
	ClientStateManager* mClientStateManager;

	Ogre::Vector3 last_network_position;
	Ogre::Vector3 last_last_network_position;
	Ogre::Vector3 updated_translate;
	Ogre::Vector3 network_position;

	Ogre::Quaternion last_network_orientation;
	Ogre::Quaternion last_last_network_orientation;
	Ogre::Quaternion network_orientation;

	Ogre::Quaternion orientation;

	RayCastManager *mRayCastManager;

	Ogre::Timer *mOrientationTimer;
	 
	Ogre::Vector3 mTmpTranslateVector;
};

#endif
