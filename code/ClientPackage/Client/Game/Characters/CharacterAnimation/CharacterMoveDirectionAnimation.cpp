/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CharacterMoveDirectionAnimation.h"
   
    

CharacterMoveDirectionAnimation::CharacterMoveDirectionAnimation(Ogre::Entity* const entity) 
{
	mAnimationEntity = entity;

	/// Set the Movement to none and the animation to idle ///
	mPreviousMovementDirection = URA::MovementDirection::NONE;
	mCurrentMovementDirection = URA::MovementDirection::NONE;
	
	mAnimationState = mAnimationEntity->getAnimationState( "Idle2" );
	mAnimationState->setLoop( true );
    mAnimationState->setEnabled( true );
}

CharacterMoveDirectionAnimation::~CharacterMoveDirectionAnimation() 
{
	
}


void CharacterMoveDirectionAnimation::setMovementDirection(const URA::MovementDirection movementDirection)
{
	mCurrentMovementDirection = movementDirection;
}


void CharacterMoveDirectionAnimation::update(Ogre::Real timeSinceLastFrame)
{
	mAnimationState->addTime(timeSinceLastFrame);

	/// Make sure were not recieving the same direction ///
	if (mPreviousMovementDirection != mCurrentMovementDirection)
	{
		// Store the new Direction //
		mPreviousMovementDirection = mCurrentMovementDirection;

		//////////////////////////////////////////////////////////////
		/// Set the sound and animation according to the direction ///
		//////////////////////////////////////////////////////////////
		 
		// None //
		if (mCurrentMovementDirection == URA::MovementDirection::NONE)
		{
			mAnimationState->setLoop( false );
			mAnimationState->setEnabled( false );
			mAnimationState = mAnimationEntity->getAnimationState( "Idle2" );
			mAnimationState->setLoop( true );
			mAnimationState->setEnabled( true );
		}
		// Forward //
		else if (mCurrentMovementDirection == URA::MovementDirection::FORWARD)
		{
			mAnimationState->setLoop( false );
			mAnimationState->setEnabled( false );
			mAnimationState = mAnimationEntity->getAnimationState( "Walk" );
			mAnimationState->setLoop( true );
			mAnimationState->setEnabled( true );
		} 
		// Backward //
		else if (mCurrentMovementDirection == URA::MovementDirection::BACKWARD)
		{

		}
		// Left //
		else if (mCurrentMovementDirection == URA::MovementDirection::LEFT)
		{

		}
		// Right //
		else if (mCurrentMovementDirection == URA::MovementDirection::RIGHT)
		{

		}
	}
}