/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __NONPLAYERSTATICCHARACTER_H__
#define __NONPLAYERSTATICCHARACTER_H__


#include <string>
#include <Ogre.h>
#include <MovableText.h>

#include "CharacterMoveDirectionAnimation.h"  

 
class NonePlayerStaticCharacter
{
protected:
	// MAYBE JUST HOLD THE NAME, THE CLIENT DATABASE CAN DO THE REST BY ITS NAME,
	// SUCH AS POSITION AND WHATEVER //
	 
	CharacterMoveDirectionAnimation* mCharacterMoveDirectionAnimation;
	Ogre::SceneNode* mSceneNode;

	bool mbMale;

	// The label //
	Ogre::MovableText *mLabel;
public:
	NonePlayerStaticCharacter( std::string name, bool isMale, Ogre::Vector3 position = Ogre::Vector3(0,0,0) );
	~NonePlayerStaticCharacter();
	 
	void update(Ogre::Real timeSinceLastFrame);

	void setMovementDirection(const URA::MovementDirection movementDirection); 
	void setEmpire(const unsigned int empire);  
	void setPosition(const Ogre::Vector3 pos);

	const std::string& getName() const;
	const Ogre::Vector3& getPosition() const;
};



#endif
