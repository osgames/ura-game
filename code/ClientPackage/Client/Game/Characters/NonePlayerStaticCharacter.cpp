/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NonePlayerStaticCharacter.h"
#include "Ogre3dManager.h"
#include "ClientGlobals.h"
 
  
     
NonePlayerStaticCharacter::NonePlayerStaticCharacter(std::string name, bool isMale, Ogre::Vector3 position)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();


	// Create the scenenode  //
	Ogre::Entity* entity = NULL;
	mSceneNode = ogre3dManager->createSceneNode(name);
	
	     
	
	// Set the mesh and material of the model depending on:
	// Male or female, empire type //
	if (isMale)
	{
		entity = ogre3dManager->createEntity(name, DEF_CLIENTGLOBALS_MAINCHARACTER_MALE_MESH_NAME);
		entity->setMaterialName(DEF_CLIENTGLOBALS_MAINCHARACTER_MALE_MATERIAL_NAME);
	}
	else
	{
		entity = ogre3dManager->createEntity(name, DEF_CLIENTGLOBALS_MAINCHARACTER_FEMALE_MESH_NAME);
		entity->setMaterialName(DEF_CLIENTGLOBALS_MAINCHARACTER_FEMALE_MATERIAL_NAME);
	}

	   
	// Set the size/scale of the model //
	ogre3dManager->setSceneNodeScale(mSceneNode, 0.08, 0.08, 0.08); 

	// Set the position //
	mSceneNode->setPosition(position);
	  
	// Attach the entity //
	ogre3dManager->attachEntityToSceneNode(entity, mSceneNode);

	
	/// Create the mCharacterMoveDirectionAnimation object ///
	mCharacterMoveDirectionAnimation = new CharacterMoveDirectionAnimation(entity);


	///////////////////////
	/// Setup the Label ///
	///////////////////////
	mLabel = new Ogre::MovableText("PLAYER_LABEL_" + mSceneNode->getName(), mSceneNode->getName(), "StarWars", 15.0);
	mLabel->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE); // Center horizontally and display above the node
	mLabel->setAdditionalHeight(18.0);
	mSceneNode->attachObject(mLabel);
}

NonePlayerStaticCharacter::~NonePlayerStaticCharacter()
{
	if (mCharacterMoveDirectionAnimation)
	{
		delete mCharacterMoveDirectionAnimation;
		mCharacterMoveDirectionAnimation = NULL;
	}
}


 
void NonePlayerStaticCharacter::update(Ogre::Real timeSinceLastFrame)
{
	mCharacterMoveDirectionAnimation->update(timeSinceLastFrame);
}
 


void NonePlayerStaticCharacter::setMovementDirection(const URA::MovementDirection movementDirection)
{
	mCharacterMoveDirectionAnimation->setMovementDirection(movementDirection);
}

 
void NonePlayerStaticCharacter::setEmpire(const unsigned int empire)
{

}

void NonePlayerStaticCharacter::setPosition(const Ogre::Vector3 pos)
{
	mSceneNode->setPosition(pos);
}



const std::string& NonePlayerStaticCharacter::getName() const
{
	return mSceneNode->getName();
}

const Ogre::Vector3& NonePlayerStaticCharacter::getPosition() const
{
	return mSceneNode->getPosition();
}

