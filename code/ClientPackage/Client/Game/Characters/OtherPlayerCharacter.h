/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __OTHERPLAYERCHARACTER_H__
#define __OTHERPLAYERCHARACTER_H__

#include <string>
#include <Ogre.h>
  
#include "Game.h"
#include "MovableText.h"
#include "MovementDirection.h"
#include "CharacterMoveDirectionAnimation.h"
#include "SelfCharacterMoveDirectionSound.h"

class Network;
class ClientStateManager;
class RayCastManager;


/*
 * Player class which will handle many player internals, such as interpolation.
 */
class OtherPlayerCharacter
{
public:
	/*
	 * Parameter "client" allows player to be either client (camera operations performed), or non-client.
	 */
	OtherPlayerCharacter( std::string name, bool client = false );
	~OtherPlayerCharacter();

	std::string mName;

	Ogre::Quaternion last_orientation;
	Ogre::Vector3 last_position;
	 
	bool updated; // If true, gives delta.  If false, gives real information.
	bool in_region; // If false, player is out of range.  Information can be removed.

	bool bClient;

	// The main update function //
	void update(Ogre::Real timeSinceLastFrame);

	void rotatePlayerAndCam(double direction);
	void pitchPlayerView(double direction);
	Ogre::Quaternion getCameraOrientation();
	Ogre::Quaternion getTargetOrientation();
	Ogre::Quaternion getPlayerOrientation();
	void directionalTranslate( Ogre::Vector3 dir );

	Ogre::Vector3 getPosition();
	void setPosition( Ogre::Vector3 pos );

	// Called when a network update of a player position occurs.
	void notifyNetworkUpdate();

	void setOrientation( Ogre::Quaternion quat );

	void setMovementDirection(const URA::MovementDirection movementDirection);
private:
	Ogre::Entity *mEntity;
	Ogre::SceneNode *mSceneNode;

	bool client_player;

	/** Sec1: Only used if player is client-player. **/
	Ogre::SceneNode *mCameraNode;
	Ogre::SceneNode *mTargetNode;
	Ogre::SceneNode *mPitchTargetNode;
	Ogre::Vector3 mTargetNodeOffset;
	/** Sec1: End. **/

	Network *mNetwork;
	 
	CharacterMoveDirectionAnimation* mCharacterMoveDirectionAnimation;
	SelfCharacterMoveDirectionSound* mSelfCharacterMoveDirectionSound;


	double cam_dist_from_target;
	Ogre::Timer *mTimer;
	double time_between_orientation_change;
	double average_time_between_orientation_change;

	Ogre::Vector3 position;

#ifdef HEAVY_LOGGING
	Ogre::Timer *speed_timer;
	Ogre::Vector3 pos_init;
	Ogre::Vector3 pos_fin;
#endif

	bool noclip;
	void setNoclip( bool clip );

	ClientStateManager *mGSM;

	Ogre::Vector3 last_network_position;
	Ogre::Vector3 last_last_network_position;
	Ogre::Vector3 updated_translate;
	Ogre::Vector3 network_position;

	Ogre::Quaternion last_network_orientation;
	Ogre::Quaternion last_last_network_orientation;
	Ogre::Quaternion network_orientation;

	Ogre::Quaternion orientation;

	RayCastManager *mRayCastManager;

	Ogre::MovableText *mPlayerLabel;

	Ogre::Timer *mOrientationTimer;
};

#endif
