/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SelfPlayerCharacter.h"
#include "Ogre3dManager.h"
#include "Network.h"
#include "ClientStateManager.h"
#include "RayCastManager.h"
#include "MovableText.h"
#include "SoundManager.h"
#include "InputManager.h"
#include "ClientGlobals.h"
#include "ZoneLoader.h"

 
SelfPlayerCharacter::SelfPlayerCharacter() :
	mSceneNode(NULL), mTargetNode(NULL), mPitchTargetNode(NULL),
	mTimer(NULL),
	mNetwork(NULL),
	mRayCastManager(NULL)
{
	//Ogre::Vector3 startPosition(0, 6000, 0);
	Ogre::Vector3 startPosition(50, 0, 1200);


	// Get the InputKeyCollector //
	mInputKeyCollector = InputManager::getSingletonPtr()->getInputKeyCollector();

	// Get the InputMouseCollector //
	mInputMouseCollector = InputManager::getSingletonPtr()->getMouseKeyCollector();


	// Deals with rotating the camera //
	mCameraRotateY = 0.0;
	mCameraRotateX = 0.0;
	


	mTmpTranslateVector = Ogre::Vector3(0,0,0);


	// Create the scene node and entity //
	// Set the material and attach the entity //
	mSceneNode = Ogre3dManager::getSingletonPtr()->createSceneNode("SelfPlayerCharacter");	
	mEntity = Ogre3dManager::getSingletonPtr()->createEntity("SelfPlayerCharacter", DEF_CLIENTGLOBALS_MAINCHARACTER_MALE_MESH_NAME);
	mEntity->setMaterialName(DEF_CLIENTGLOBALS_MAINCHARACTER_MALE_MATERIAL_NAME);
	Ogre3dManager::getSingletonPtr()->attachEntityToSceneNode(mEntity, mSceneNode);

	 
	// Create the mCharacterMoveDirectionAnimation object //
	mCharacterMoveDirectionAnimation = new CharacterMoveDirectionAnimation(mEntity);

	// Set the mSelfCharacterMoveDirectionSound to null just in case its not the client //
	mSelfCharacterMoveDirectionSound = NULL;


	// Set the Move direction to None for now //
	mMoveDirection = URA::MovementDirection::NONE;


	// Set the position and orientation // 
	orientation = mSceneNode->getOrientation();
	last_orientation = orientation;
	network_orientation = orientation;
	last_network_orientation = orientation;
	last_last_network_orientation = orientation;
	position = startPosition;
	last_position = startPosition;
	network_position = startPosition;
	last_network_position = position;
	last_last_network_position = last_network_position;
	mTargetNodeOffset = Ogre::Vector3( 50, 200, 0 ); // What the camera will look at.

	mTimer = new Ogre::Timer();
	mTimer->reset();
	mClientStateManager = ClientStateManager::getSingletonPtr();

	mOrientationTimer = NULL;

	mNetwork = Network::getSingletonPtr();

	average_time_between_orientation_change = 0.0;
	time_between_orientation_change = 0.0;


	mRayCastManager = new RayCastManager();

		 
	mTargetNode = Ogre3dManager::getSingletonPtr()->
		createChildSceneNode(mSceneNode, "mTargetNode");
	//mTargetNode = mSceneNode->createChildSceneNode();
	mTargetNode->translate( mTargetNodeOffset );
	//mTargetNode->setPosition( mSceneNode->getPosition() );

	Ogre3dManager::getSingletonPtr()->getCamera()->setPosition( 0, 0, 0 );

	mPitchTargetNode = Ogre3dManager::getSingletonPtr()->
		createChildSceneNode(mTargetNode, "mPitchTargetNode");
	//mPitchTargetNode = mTargetNode->createChildSceneNode();
	mPitchTargetNode->attachObject( Ogre3dManager::getSingletonPtr()->getCamera() );
	mPitchTargetNode->translate( 0, 0, 300 ); // Camera position translated from the target node.
	Ogre3dManager::getSingletonPtr()->getCamera()->lookAt( mTargetNode->getPosition() );

	// This is acting as pitch for now, so make the camera pitch a little bit;
	mTargetNode->pitch( Ogre::Degree(-10) );

	mOrientationTimer = new Ogre::Timer();
	mOrientationTimer->reset();

	//////////////////////////////////////////////////////////////////
	/// If the client than Create mSelfCharacterMoveDirectionSound ///
	//////////////////////////////////////////////////////////////////
	mSelfCharacterMoveDirectionSound = new SelfCharacterMoveDirectionSound(mEntity);
	
	 
	 
	// Set the size/scale of the model // 
	Ogre3dManager::getSingletonPtr()->setSceneNodeScale(mSceneNode, 0.08, 0.08, 0.08);
}
	 
SelfPlayerCharacter::~SelfPlayerCharacter()
{
	// Destroy the mEntity //
	Ogre3dManager::getSingletonPtr()->destroyEntity(mEntity);

	if( mOrientationTimer )
	{
		delete mOrientationTimer;
		mOrientationTimer = NULL;
	}

	if( mRayCastManager )
	{
		delete mRayCastManager;
		mRayCastManager = NULL;
	}

	if (mTimer)
	{
		delete mTimer;
		mTimer = NULL;
	}
	 
	if (mPitchTargetNode)
	{
		Ogre3dManager::getSingletonPtr()->destroySceneNode(mPitchTargetNode);
		mPitchTargetNode = NULL;
	}

	if (mTargetNode)
	{
		Ogre3dManager::getSingletonPtr()->destroySceneNode(mTargetNode);
		mTargetNode = NULL;
	}

	if (mSceneNode)
	{
		// First detachAllObjects //
		Ogre3dManager::getSingletonPtr()->detachAllObjects(mSceneNode);

		// Now destroy the node //
		Ogre3dManager::getSingletonPtr()->destroySceneNode(mSceneNode);
		mSceneNode = NULL;
	}

	if (mCharacterMoveDirectionAnimation)
	{
		delete mCharacterMoveDirectionAnimation;
		mCharacterMoveDirectionAnimation = NULL;
	}

	if (mSelfCharacterMoveDirectionSound)
	{
		delete mSelfCharacterMoveDirectionSound;
		mSelfCharacterMoveDirectionSound = NULL;
	}

	  
	mInputKeyCollector = NULL;
	mInputMouseCollector = NULL;
	mNetwork = NULL;
}
 




void SelfPlayerCharacter::updateInput()
{
	/// Check if were moving the mouse aka the camera ///
	if ( mInputMouseCollector->isLeftButtonPressed() &&  mInputMouseCollector->isMoving() )
	{	
		mCameraRotateX = (-(double)mInputMouseCollector->getMoveRelativeX() * 0.9) * 0.5;
		mCameraRotateY = (-(double)mInputMouseCollector->getMoveRelativeY() * 0.35) * 0.5;	
	}

	
	//////////////////////////////////
	/// Update the Move Directions ///
	//////////////////////////////////
	// HALT?
	if ( mInputKeyCollector->getKeyPressedCount() == 0 )
	{
		SelfPlayerCharacter::setMovementDirection(URA::MovementDirection::NONE);
	}
	else
	{
		// FORWARD?
		if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_W) )
		{
			SelfPlayerCharacter::setMovementDirection(URA::MovementDirection::FORWARD);
		}
		// BACKWARD //
		else if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_S) )
		{
			SelfPlayerCharacter::setMovementDirection(URA::MovementDirection::BACKWARD);
		}
		// LEFT //
		else if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_A) )
		{
			SelfPlayerCharacter::setMovementDirection(URA::MovementDirection::LEFT);
		}
		// RIGHT //
		else if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_D) )
		{
			SelfPlayerCharacter::setMovementDirection(URA::MovementDirection::RIGHT);
		}
	}
}


void SelfPlayerCharacter::update(const Ogre::Real& timeSinceLastFrame)
{
	Ogre::Vector3 intersection(0.0, 0.0, 0.0);


	///////////////////////////////
	/// Update the mouse Camera ///
	///////////////////////////////
	SelfPlayerCharacter::rotatePlayerAndCam(mCameraRotateX);
	SelfPlayerCharacter::pitchCamera(mCameraRotateY);


	//////////////////////////////////
	/// Update the Move Directions ///
	//////////////////////////////////
	float characterMovement = 100.0f * timeSinceLastFrame;
	RakNet::BitStream out;

	unsigned char Id = ID_RAYCAST_REQUEST;
	float x_position = SelfPlayerCharacter::getPosition().x;
	float y_position = SelfPlayerCharacter::getPosition().y;
	float z_position = SelfPlayerCharacter::getPosition().z;

	out.Write( Id );
	out.Write(x_position);
	out.Write(y_position);
	out.Write(z_position);

	Network::getSingletonPtr()->mPeer->Send( &out, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, Network::getSingletonPtr()->game_server_address, false );
		 

	if (mMoveDirection == URA::MovementDirection::NONE)
	{
		Network::getSingletonPtr()->sendIdToServer(ID_HALT);
		mTmpTranslateVector = Ogre::Vector3(0, 0, 0);
	}
	else if (mMoveDirection == URA::MovementDirection::FORWARD)
	{
		Network::getSingletonPtr()->sendIdToServer(ID_FORWARD);
		mTmpTranslateVector = Ogre::Vector3(0, 0, -characterMovement);
	}
	else if (mMoveDirection == URA::MovementDirection::BACKWARD)
	{
		Network::getSingletonPtr()->sendIdToServer(ID_BACK);
		mTmpTranslateVector = Ogre::Vector3(0, 0, characterMovement);
	}
	else if (mMoveDirection == URA::MovementDirection::LEFT)
	{
		Network::getSingletonPtr()->sendIdToServer(ID_LEFT);
		mTmpTranslateVector = Ogre::Vector3(-characterMovement, 0, 0);
	}
	else if (mMoveDirection == URA::MovementDirection::RIGHT)
	{
		Network::getSingletonPtr()->sendIdToServer(ID_RIGHT);
		mTmpTranslateVector = Ogre::Vector3(characterMovement, 0, 0);
	}

	 
	///////////////////////////////////////////////////////////////
	/// Raycast the translated position to get the new position ///
	///////////////////////////////////////////////////////////////
	Ogre::Real depth = 101;
	if( mRayCastManager->RaycastFromPoint( position, orientation * mTmpTranslateVector, intersection ) )
	{
		// If RaycastFromPoint return false, it's most likely unable to see in front of it,
		// so there's no need to log it as an error.
		depth = position.distance( intersection );
	}

	// If statement is here since RaycastFromPoint doesn't return true if objects are far enough away.
	if( depth > 5 )
	{
		position = position + (orientation * mTmpTranslateVector);
	}
	else if( mRayCastManager->RaycastFromPoint( Ogre::Vector3(position.x, position.y + 3, position.z), 
		orientation * mTmpTranslateVector, intersection ) )
	{
		// In case there are stairs.
		depth = position.distance( intersection );
		if( depth > 5 )
		{
			position = position + (orientation * mTmpTranslateVector);
		}
	}





	/////////////////////////////////////////////////
	/// Do we need to send an orientation update? ///
	/////////////////////////////////////////////////

	// Eventually interpolation will be handled here.  For now, the player will just skip to positions.
	if (last_orientation != orientation)
	{
		if( mOrientationTimer->getMilliseconds() >= 75 )
		{
			Network::getSingletonPtr()->sendOrientationUpdate(orientation);
			mOrientationTimer->reset();
		}
	}



	//////////////////////////////////////////////////////////////////////////////////
	/// Set the Y position of the player to raycast or terrain. Whatever is higher ///
	//////////////////////////////////////////////////////////////////////////////////

	//Ogre::Vector3 result;
	//RaycastFromPoint( position, Ogre::Vector3::NEGATIVE_UNIT_Y, result );
	//position = Ogre::Vector3( position.x, result.y + 8.5, position.z );

	//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString(position) );
	Ogre::Vector3 terrain_position = position;
	intersection = position;
	

	// Gets the high of the terrain position //
	mClientStateManager->getGame()->getZoneLoader()->getHeightAt(terrain_position);
	
	
	if( mRayCastManager->RaycastFromPoint( Ogre::Vector3(position.x, position.y + 5, position.z), Ogre::Vector3::NEGATIVE_UNIT_Y, intersection ) )
	{
		// Check to see if terrain_position or the raycast is higher.
		if( intersection.y > terrain_position.y )
		{
			position.y = intersection.y;
		}
		else
		{
			position.y = terrain_position.y;
		}
	}
	else
	{
		position.y = terrain_position.y;
	}
	

	 
	if( (last_position != position) && mTimer->getMilliseconds() >= 500 )
	{
		mNetwork->sendPositionToServer( position );
		mTimer->reset();
	}



	mSceneNode->setPosition( position );
	mSceneNode->setOrientation( orientation );


	last_orientation = orientation;
	last_position = position;

	 
	///////////////////////////////////////////////////
	/// Update the mCharacterMoveDirectionAnimation ///
	///////////////////////////////////////////////////
	mCharacterMoveDirectionAnimation->update(timeSinceLastFrame); 
 

	////////////////////////////////////////////////////////////////////////////////////////////
	/// Update the mSelfCharacterMoveDirectionSound - only the client should have move sound ///
	////////////////////////////////////////////////////////////////////////////////////////////
	mSelfCharacterMoveDirectionSound->update(timeSinceLastFrame); 


	//////////////////////////////////
	/// Reset the camera rotations ///
	//////////////////////////////////
	mCameraRotateX = 0.0;
	mCameraRotateY = 0.0;
}
 
void SelfPlayerCharacter::rotatePlayerAndCam(double direction)
{
	mSceneNode->yaw( Ogre::Degree(direction) );
	setOrientation( mSceneNode->getOrientation() );
}

void SelfPlayerCharacter::pitchCamera(double direction)
{
	mTargetNode->pitch( Ogre::Degree(direction) );
}

Ogre::Quaternion SelfPlayerCharacter::getCameraOrientation()
{
	return Ogre3dManager::getSingletonPtr()->getCamera()->getDerivedOrientation();
}

Ogre::Quaternion SelfPlayerCharacter::getTargetOrientation()
{
	return mTargetNode->getOrientation();
}

   


Ogre::Quaternion SelfPlayerCharacter::getPlayerOrientation()
{
	return mSceneNode->getOrientation();
}

Ogre::Vector3 SelfPlayerCharacter::getPosition()
{
	return position;
}

const std::string& SelfPlayerCharacter::getName() const
{
	return mSceneNode->getName();
}


void SelfPlayerCharacter::setPosition( Ogre::Vector3 pos )
{
#ifdef HEAVY_LOGGING
	Ogre::LogManager::getSingleton().logMessage(
		mName + " SelfPlayerCharacter::setPosition: " + Ogre::StringConverter::toString(pos) );
#endif
	last_last_network_position = last_network_position;
	last_network_position = network_position;
	position = pos;
	network_position = pos;
}


void SelfPlayerCharacter::notifyNetworkUpdate()
{

}

void SelfPlayerCharacter::setOrientation( Ogre::Quaternion quat )
{
	last_last_network_orientation = last_network_orientation;
	last_network_orientation = network_orientation;
	orientation = quat;
	network_orientation = quat;
}

void SelfPlayerCharacter::setMovementDirection(const URA::MovementDirection movementDirection)
{
	// Store the movement direction //
	mMoveDirection = movementDirection;

	// Set the animation movement //
	mCharacterMoveDirectionAnimation->setMovementDirection(movementDirection); 

	// Set the sound movement //
	if (mSelfCharacterMoveDirectionSound)
	{
		mSelfCharacterMoveDirectionSound->setMovementDirection(movementDirection);
	}
}

   