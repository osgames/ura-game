/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#ifndef __GAME_H__
#define __GAME_H__



#include <Ogre.h>
#include <CEGUI/CEGUI.h>

#include <vector>

#include "InputManager.h"
#include "InsideGameGui.h"
#include "MovementDirection.h"

class Network;
class ZoneLoader;
class Player;
class SelfPlayerCharacter;
class OtherPlayerCharacter;
class NonePlayerStaticCharacter;
class InsideGameGui;
    
   
   
class Game
{
private:
	InputKeyCollector* mInputKeyCollector;

	Network* mNetwork;
	ZoneLoader* mZoneLoader;
	SelfPlayerCharacter* mSelfPlayerCharacter;
	
	InsideGameGui* mInsideGameGui;

	std::vector<OtherPlayerCharacter *> mOtherPlayerCharacterList;
	std::vector<NonePlayerStaticCharacter *> mNonePlayerStaticCharacterList;

	// COMMENTED //
	//Ogre::Real mPrevFrameTime;
public:
	Game();
	~Game();
	
	// Called each frame //
	void frameStarted(Ogre::Real timeSinceLastFrame);
	void frameEnded(Ogre::Real timeSinceLastFrame);

	//  Updates all of the nearby player's information //
	void updateAllPlayers();
	void fullUpdateAllPlayers();


	/// Recive chat text ////
	void receive_say_chat(std::string message, std::string player);


	/// Get the ZoneLoader - Needs to be changed and taken out ///
	ZoneLoader* const getZoneLoader() const;


	/// Get SelfPlayerCharacter and OtherPlayerCharacter ///
	SelfPlayerCharacter* const getSelfPlayerCharacter() const;
	OtherPlayerCharacter* const getOtherPlayerCharacter(const std::string name);


	/// NonePlayerStaticCharacter ///
	NonePlayerStaticCharacter* addNonePlayerStaticCharacter(std::string name, 
		bool isMale, const Ogre::Vector3 position);
	
	NonePlayerStaticCharacter* const getNonePlayerStaticCharacter(std::string name);
	
	void removeNonePlayerStaticCharacter(std::string name);
};

#endif 
