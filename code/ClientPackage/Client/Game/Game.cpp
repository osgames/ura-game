/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#include "Game.h"
#include "ClientGlobals.h"
#include "Network.h"
#include "NetworkStructures.h"
#include "Ogre3dManager.h"
#include "ClientStateManager.h"
#include "ZoneLoader.h"
#include "SelfPlayerCharacter.h"
#include "OtherPlayerCharacter.h"
#include "NonePlayerStaticCharacter.h"
#include "SoundManager.h"
#include "MovementDirection.h"

 
   
  
Game::Game()
{
	Sound* themeSong = NULL;


	mZoneLoader = new ZoneLoader();
	mSelfPlayerCharacter = new SelfPlayerCharacter();
	mNetwork = Network::getSingletonPtr();

	// Get the InputKeyCollector //
	mInputKeyCollector = InputManager::getSingletonPtr()->getInputKeyCollector();

	// Create InsideGameGui //
	mInsideGameGui = new InsideGameGui();



	//mPrevFrameTime = 0.0;


 
	// Load the zone //
	mZoneLoader->loadZone("devtestmap1.map");
   
	

	// Request the inventory snapshot //
	mNetwork->request_Full_Inventory_Snapshot();



	// Disable main menu sound //
	themeSong = SoundManager::getSingletonPtr()->getSound(DEF_CLIENTGLOBALS_THEMESONG_FILENAME);
	if (themeSong)
	{
		themeSong->stop();
		if( !SoundManager::getSingletonPtr()->destroySound(themeSong) )
		{
			Ogre::LogManager::getSingleton().logMessage( "Game.cpp: Sound not destroyed properly." );
		}
	}
	else
	{
		Ogre::LogManager::getSingleton().logMessage( "Game.cpp: Could not find ot2.wav in SoundManager in order to destroy it." );
	}



	//////////////////////////////////////////
	/// TESTING mNonePlayerStaticCharacter ///
	//////////////////////////////////////////
	int maxNinjaPerRow = 3;
	int maxNinjaRows = 3;
	int distanceSideApart = 100;
	float startXPos = -100;

	int i = 0;
	int j = 0;
	int ninjaCount = 0; 

	Ogre::Vector3 tmpVec3Pos(startXPos, 0, 1000);
	
	std::string testNames = "npctest";

	for (i = 0; i < maxNinjaRows; i++)
	{
		// Reset the x //
		tmpVec3Pos.x = startXPos;

		// Move to the next row behind //
		tmpVec3Pos.z += 50;

		for (j = 0; j < maxNinjaPerRow; j++)
		{
			tmpVec3Pos.x += distanceSideApart;
			Game::addNonePlayerStaticCharacter(testNames + Ogre::StringConverter::toString(ninjaCount), true, tmpVec3Pos);
			ninjaCount++;
		}
	}


	
	/////////////////////////////////////////////////////////
	/// Go through each material and disable the lighting ///
	/////////////////////////////////////////////////////////
	Ogre3dManager::getSingletonPtr()->disableAllMaterialLighting();
}

Game::~Game()
{
	if (mSelfPlayerCharacter)
	{
		delete mSelfPlayerCharacter;
		mSelfPlayerCharacter = NULL;
	}

	if (mZoneLoader)
	{
		delete mZoneLoader;
		mZoneLoader = NULL;
	}

	if (mInsideGameGui)
	{
		delete mInsideGameGui;
		mInsideGameGui = NULL;
	}


	//////////////////////////////////////////////////////////////////
	/// Delete the mOtherPlayerCharacterList elements and clear it ///
	//////////////////////////////////////////////////////////////////
	for ( unsigned int indx = 0; indx < mOtherPlayerCharacterList.size(); indx++ )
	{
		delete mOtherPlayerCharacterList[indx];
		mOtherPlayerCharacterList[indx] = NULL;
	}

	mOtherPlayerCharacterList.clear();


	//////////////////////////////////////////////////////////////////
	/// Delete the mNonePlayerStaticCharacterList elements and clear it ///
	//////////////////////////////////////////////////////////////////
	for ( unsigned int indx = 0; indx < mNonePlayerStaticCharacterList.size(); indx++ )
	{
		delete mNonePlayerStaticCharacterList[indx];
		mNonePlayerStaticCharacterList[indx] = NULL;
	}

	mNonePlayerStaticCharacterList.clear();
	 

	//////////////////////////////////////////////
	/// Destory all the scenNodes and entities ///
	//////////////////////////////////////////////
	Ogre3dManager::getSingletonPtr()->destroyAllEntitiesAndSceneNodes();
}

 





void Game::frameStarted(Ogre::Real timeSinceLastFrame)
{
	//mPrevFrameTime = timeSinceLastFrame;

	/// Update ZoneLoader ///
	if (mZoneLoader)
	{
		mZoneLoader->frameStart(timeSinceLastFrame);
	}




	// If a InsideGameGui window is active or a Return key is down than update InsideGameGui input //
	// When ever a return/enter key is down it means were dealing with gui windows //
	if ( 
		mInsideGameGui->isWindowActive() || 
		mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_RETURN) ||
		mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_ESCAPE)
		)
	{
		mInsideGameGui->updateInput();
	}
	else
	{
		///////////////////////////
		/// Update the GUI data ///
		///////////////////////////
		mInsideGameGui->updateData();
		 
		 
		/////////////////////////////////////////
		/// Update mSelfPlayerCharacter Input ///
		/////////////////////////////////////////
		mSelfPlayerCharacter->updateInput();
	}


	// Update the  Other Player Character list //
	for( unsigned int indx = 0; indx < mOtherPlayerCharacterList.size(); indx++ )
	{
		mOtherPlayerCharacterList[indx]->update(timeSinceLastFrame);
	}

	// Update NonePlayerStaticCharacter list //
	for( unsigned int indx = 0; indx < mNonePlayerStaticCharacterList.size(); indx++ )
	{
		mNonePlayerStaticCharacterList[indx]->update(timeSinceLastFrame);
	}

	
	///////////////////////////////////
	/// Update mSelfPlayerCharacter ///
	///////////////////////////////////
	mSelfPlayerCharacter->update(timeSinceLastFrame);
}


void Game::frameEnded(Ogre::Real timeSinceLastFrame)
{
	/// Update ZoneLoader ///
	if (mZoneLoader)
	{
		mZoneLoader->frameEnd(timeSinceLastFrame);
	}
}



void Game::fullUpdateAllPlayers()
{
/***************************
	for( unsigned int indx = 0; indx < mOtherPlayerCharacterList.size(); indx++ )
	{
		delete mOtherPlayerCharacterList[indx];
	}
	mOtherPlayerCharacterList.clear();

	for( unsigned int indx = 0; indx < mNetwork->mOtherPlayerCharacterList.size(); indx++ )
	{
		if( mNetwork->mOtherPlayerCharacterList[indx]->name.compare( "__CLIENT__" ) == 0 )
		{
			//mSelfPlayerCharacter->setPosition( mNetwork->mOtherPlayerCharacterList[indx]->position );
			//mSelfPlayerCharacter->orientation = mNetwork->mOtherPlayerCharacterList[indx]->orientation;
			//mSelfPlayerCharacter->update( mPrevFrameTime ); // Force update so that it renders.
		}
		else
		{
			SelfPlayerCharacter *player = new SelfPlayerCharacter( mNetwork->mOtherPlayerCharacterList[indx]->name );
			mOtherPlayerCharacterList.push_back( player );

			player->updated = false;
			player->setPosition( mNetwork->mOtherPlayerCharacterList[indx]->position );
			player->setOrientation( mNetwork->mOtherPlayerCharacterList[indx]->orientation );
			player->update( mPrevFrameTime ); // Force update so that it renders.
			player->in_region = true;
			player->notifyNetworkUpdate();
		}
	}
*****************************/
}

void Game::updateAllPlayers()
{
/**********************************
	for( unsigned int indx = 0; indx < mNetwork->mOtherPlayerCharacterList.size(); indx++ )
	{
		if( mNetwork->mOtherPlayerCharacterList[indx]->name.compare( "__CLIENT__" ) == 0 )
		{
		}
		else
		{
			bool flg = false;

			for( unsigned int i = 0; i < mOtherPlayerCharacterList.size(); i++ )
			{
				if( mNetwork->mOtherPlayerCharacterList[indx]->name.compare( mOtherPlayerCharacterList[i]->mName ) == 0 )
				{
					flg = true;

					// Eventually this will be redone to support delta compression.
					mOtherPlayerCharacterList[i]->updated = true;
					mOtherPlayerCharacterList[i]->setPosition( mNetwork->mOtherPlayerCharacterList[indx]->position );
					mOtherPlayerCharacterList[i]->setOrientation( mNetwork->mOtherPlayerCharacterList[indx]->orientation );
					mOtherPlayerCharacterList[i]->in_region = true;
				}
			}

			if( !flg )
			{
				SelfPlayerCharacter *player = new SelfPlayerCharacter( mNetwork->mOtherPlayerCharacterList[indx]->name );
				mOtherPlayerCharacterList.push_back( player );

				player->updated = false;
				player->setPosition( mNetwork->mOtherPlayerCharacterList[indx]->position );
				player->setOrientation( mNetwork->mOtherPlayerCharacterList[indx]->orientation );
				player->in_region = true;
			}
		}

		for( unsigned int indx = 0; indx < mOtherPlayerCharacterList.size(); indx++ )
		{
			if( !mOtherPlayerCharacterList[indx]->in_region )
			{
				delete mOtherPlayerCharacterList[indx];
				mOtherPlayerCharacterList.erase( mOtherPlayerCharacterList.begin() + indx );
			}
		}
	}
*********************************************/
}



 
SelfPlayerCharacter* const Game::getSelfPlayerCharacter() const
{
	return mSelfPlayerCharacter;
}

OtherPlayerCharacter* const Game::getOtherPlayerCharacter(const std::string name)
{
	for( unsigned int indx = 0; indx < mOtherPlayerCharacterList.size(); indx++ )
	{
		if ( mOtherPlayerCharacterList[indx]->mName == name )
		{
			return mOtherPlayerCharacterList[indx];
		}
	}
	return 0;
}

 
ZoneLoader* const Game::getZoneLoader() const
{
    return mZoneLoader;
}


void Game::receive_say_chat( std::string message, std::string player )
{
	/************
	InGameUI* const inGameUI = GUI::getSingletonPtr()->getInGameUI();

	if (inGameUI)
	{
		inGameUI->receive_say_chat(message, player);
	}
	*************/
}

NonePlayerStaticCharacter* Game::addNonePlayerStaticCharacter(std::string name, 
		bool isMale, const Ogre::Vector3 position)
{
	NonePlayerStaticCharacter* const npc = new NonePlayerStaticCharacter(name, true, position);
	mNonePlayerStaticCharacterList.push_back(npc);
	return npc;
}
    

NonePlayerStaticCharacter* const Game::getNonePlayerStaticCharacter(std::string name)
{
	for( unsigned int i = 0; i < mNonePlayerStaticCharacterList.size(); i++ )
	{
		if( mNonePlayerStaticCharacterList[i]->getName() == name )
		{
			return mNonePlayerStaticCharacterList[i];
		}
	}

	return 0;
}
  
 
void Game::removeNonePlayerStaticCharacter(std::string name)
{
	for ( unsigned int i = 0; i < mNonePlayerStaticCharacterList.size(); i++ )
	{
		if( mNonePlayerStaticCharacterList[i]->getName() == name )
		{
			delete mNonePlayerStaticCharacterList[i];
			mNonePlayerStaticCharacterList.erase( mNonePlayerStaticCharacterList.begin() + i );
			break;
		}
	}
}


 