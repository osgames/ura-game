/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2013 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#include "TerrainManager.h"
#include "ClientGlobals.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "CeguiManager.h"


using namespace Ogre;


TerrainManager::TerrainManager() : mTerrain(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	std::string strErrorFunction = "TerrainManager::TerrainManager(...)";
	std::string strErrorWordName;


	/*** Create the sun light ***/
	mSunLight = sceneManager->createLight("Sun");
	mSunLight->setType(Ogre::Light::LT_DIRECTIONAL);
	mSunLight->setDirection(-0.808863, -0.488935, -0.326625);


	/*** Allocate memory ***/
	mMapWidth = new Ogre::Real;
	mMapHeight = new Ogre::Real;
	mVtextureNames = new std::vector<std::string>;
	mSplatmapFilename = new std::string;


	/*** Check for memory allocation error ***/
	if (!mMapWidth)
	{
		strErrorWordName = "mMapWidth";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mMapHeight)
	{
		strErrorWordName = "mMapHeight";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mVtextureNames)
	{
		strErrorWordName = "mVtextureNames";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mSplatmapFilename)
	{
		strErrorWordName = "mSplatmapFilename";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	
	/*** Set the default SplatFilename ***/
	*mSplatmapFilename = DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_FILENAME;
}

TerrainManager::~TerrainManager()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	TextureManager* const textureManager = TextureManager::getSingletonPtr();

	if (mTerrain)
	{
		delete mTerrain;
		mTerrain = NULL;
	}

	if (mMapWidth)
	{
		delete mMapWidth;
		mMapWidth = NULL;
	}

	if (mMapHeight)
	{
		delete mMapHeight;
		mMapHeight = NULL;
	}

	if (mVtextureNames)
	{
		// Remove the textures before deleting;
		for (unsigned long i = 0; i < mVtextureNames->size(); i++)
		{
			textureManager->remove( (*mVtextureNames).at(i) );
		}

		delete mVtextureNames;
		mVtextureNames = NULL;
	}

	if (mSplatmapFilename)
	{
		// Remove the texture before deleting;
		textureManager->remove(*mSplatmapFilename);

		delete mSplatmapFilename;
		mSplatmapFilename = NULL;
	}


	// Destory the sun light;
	sceneManager->destroyLight(mSunLight);
}



void TerrainManager::onFrameStart(const Ogre::Real timeSinceLastFrame)
{
	if (mTerrain)
	{
		mTerrain->onFrameStart(timeSinceLastFrame);
	}
}

void TerrainManager::onFrameEnd(const Ogre::Real timeSinceLastFrame)
{
	if (mTerrain)
	{
		mTerrain->onFrameEnd(timeSinceLastFrame);
	}
}



void TerrainManager::setNewTerrain(Ogre::Real width, Ogre::Real maxHeight, std::string splatmapFileName)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::Camera* const camera = ogre3dManager->getCamera();


	// Make sure we don't try to create another SPT::Terrain when the previous one wasn't deleted;
	if (mTerrain)
	{
		delete mTerrain;
		mTerrain = NULL;
	}


	if (!mTerrain)
	{
		/*
		***	Setup the new SPT::Terrain.
		 */


		// Set the width and height;
		*mMapWidth = width;
		*mMapHeight = maxHeight;


		/*** If the material names are empty strings, then use the defaults ***/

		if ( splatmapFileName.empty() )
		{
			splatmapFileName = DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_FILENAME;
		}


		/*** Setup the terrain ***/
		mTerrain = new SPT::Terrain(camera, sceneManager);


		// image, nothing, X and Z width, height;
		mTerrain->quickSetup(DEF_CLIENTGLOBALS_TERRAIN_DEFAULT_HEIGHTMAP_FILENAME, 1025, DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_MATERIALNAME, *mMapWidth, *mMapHeight);


		// This code can sometimes cause problems with 3d cards, so allow it to be disabled;
		#ifndef DEF_CLIENTGLOBALS_DISABLE_TERRAIN_LIGHTMAP_CODE
			mTerrain->createAtmosphere(	637800,638640,
							-0.985148,
							95.0213,
							640000.0,
							Ogre::Vector3(2.9427e-006, 9.4954e-006, 1.40597e-005),
							Ogre::Vector3(9.43648e-005, 3.36762e-005, 6.59358e-006),
							"SkySphere",512,256);
			mTerrain->createLightmapper("Lightmapper","SPT_Comp", 2048, 2049);
			mTerrain->setAutoUpdateLightmap(true);
			mTerrain->setLightDirection( mSunLight->getDirection() );
		#endif

		
		/*** Setup the splatmap ***/
		//TerrainManager::setSplatMap(splatmapFileName);
	}
}

void TerrainManager::setSplatMap(std::string fileName)
{
	std::string splatMaptexUnitAlias = "SplatMap";


	/*** Get a pointer to the material and to the texture manager ***/
	Ogre::MaterialPtr sptMaterial = MaterialManager::getSingleton().getByName(DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_MATERIALNAME);
	TextureManager* const textureManager = TextureManager::getSingletonPtr();


	/*** Change the materials splatmap texture to a dynamic texture ***/
	Material::TechniqueIterator itTech = sptMaterial->getTechniqueIterator();
	while (itTech.hasMoreElements())
	{
		Technique::PassIterator itPass = itTech.getNext()->getPassIterator();
		while (itPass.hasMoreElements())
		{
			Pass* pPass = itPass.getNext();
			TextureUnitState* pTexState = pPass->getTextureUnitState(splatMaptexUnitAlias);

			if (pTexState)
			{
				pTexState->setTextureName(fileName);
			}
		}
	}


	/*
	***	If the previous mSplatmapFilename doesn't equal to the current file were changing, than remove the old splatTexture,
	***		than assign mSplatmapFilename to the current fileName
	 */
	if (*mSplatmapFilename != fileName)
	{
		textureManager->remove(*mSplatmapFilename);	
	}

	*mSplatmapFilename = fileName;
}

void TerrainManager::setTextures(std::string tex0, std::string tex1, std::string tex2, std::string tex3)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	std::string strErrorFunction = "void TerrainManager::setTextures(...)";
	std::string strErrorMessage;



	Ogre::MaterialPtr sptMaterial = MaterialManager::getSingleton().getByName(DEF_CLIENTGLOBALS_SPLATMAP_DEFAULT_MATERIALNAME);

	std::string defaultTex0Alias = "Splat1";
	std::string defaultTex1Alias = "Splat2";
	std::string defaultTex2Alias = "Splat3";
	std::string defaultTex3Alias = "Splat4";



	if ( sptMaterial.isNull() )
	{
		strErrorMessage = "sptMaterial could not get a pointer to the material";
		ogre3dLogManager->logErrorMessage(strErrorFunction, strErrorMessage);
	}
	else
	{
		Material::TechniqueIterator itTech = sptMaterial->getTechniqueIterator();
		TextureUnitState* pTexState = NULL;
		Pass* pPass = NULL;

		TextureManager* const textureManager = TextureManager::getSingletonPtr();


		/*** Change the textures to the ones we specify ***/
		while (itTech.hasMoreElements())
		{
			Technique::PassIterator itPass = itTech.getNext()->getPassIterator();

			while (itPass.hasMoreElements())
			{
				pPass = itPass.getNext();

				// Get and set the first texture;
				pTexState = pPass->getTextureUnitState(defaultTex0Alias );
				if (pTexState)
				{
					pTexState->setTextureName(tex0);
				}

				// Get and set the 2nd texture;
				pTexState = pPass->getTextureUnitState(defaultTex1Alias );
				if (pTexState)
				{
					pTexState->setTextureName(tex1);
				}

				// Get and set the third texture;
				pTexState = pPass->getTextureUnitState(defaultTex2Alias);
				if (pTexState)
				{
					pTexState->setTextureName(tex2);
				}

				// Get and set the fourth texture;
				pTexState = pPass->getTextureUnitState(defaultTex3Alias);
				if (pTexState)
				{
					pTexState->setTextureName(tex3);
				}
			}
		}


		/*
		*** Remove the unused old textures. Remember that if a texture is still in memory than it doesn't get removed.
		*** So we can just try to remove all, if its still in memory than its being used and isn't removed.
		 */
		for (unsigned long i = 0; i < mVtextureNames->size(); i++)
		{
			textureManager->remove( (*mVtextureNames).at(i) );
		}


		/*** Assign the new textures to mVtextureNames ***/
		mVtextureNames->clear();
		mVtextureNames->push_back(tex0);
		mVtextureNames->push_back(tex1);
		mVtextureNames->push_back(tex2);
		mVtextureNames->push_back(tex3);
	}
}

bool TerrainManager::setHeightMapData(std::string &data, unsigned long long &dataSize)
{
	Ogre::StringConverter strConverter;


	SPT::Heightmap* const hMap = mTerrain->getHeightmap();
	SPT::HEIGHTMAPTYPE* const pHeightMap = hMap->getData();
	SPT::HEIGHTMAPTYPE* hMapHeight;

	std::string heightMapDataStr;
	unsigned long long heightMapDataStrSize = data.size();
	Ogre::Real tmpVal = 0.0;

	unsigned long long strIndex = 0;
	long long counter = 0;
	std::string tmpStr;



	if (heightMapDataStrSize != dataSize)
	{
		return false;
	}


	while (strIndex < heightMapDataStrSize)
	{
		while ( strIndex < heightMapDataStrSize && data.at(strIndex) != ',' )
		{
			tmpStr += data.at(strIndex);

			strIndex++;
		}

		// increment pass the comma;
		strIndex++;


		// convert the string to a real than to SPT::HEIGHTMAPTYPE;
		tmpVal = (SPT::HEIGHTMAPTYPE) strConverter.parseReal(tmpStr);

		hMapHeight = &tmpVal;
		pHeightMap[counter] = *hMapHeight;

		counter++;

		tmpStr.clear();
	}


	// We have to call the update the heightmap function;
	mTerrain->updateHeightmap();


	// This code can sometimes cause problems with 3d cards, so allow it to be disabled;
	// Update the lightmap;
	#ifndef DEF_CLIENTGLOBALS_DISABLE_TERRAIN_LIGHTMAP_CODE
		mTerrain->updateLightmap();
	#endif


	return true;
}

void TerrainManager::setSplatScales(const Ogre::Vector4 v4)
{
	std::string parameterName = "vSplatScales";
	SPT::Terrain* const terrain = TerrainManager::getTerrain();


	if (terrain)
	{
		terrain->setMaterialSchemeParams(parameterName, v4, terrain->getDefaultMaterialScheme());
	}
}

void TerrainManager::setDetailScales(const Ogre::Vector4 v4)
{
	std::string parameterName = "vDetailScales";
	SPT::Terrain* const terrain = TerrainManager::getTerrain();


	if (terrain)
	{
		terrain->setMaterialSchemeParams(parameterName, v4, terrain->getDefaultMaterialScheme());
	}
}

void TerrainManager::updateLightMap()
{
	if (mTerrain)
	{
		mTerrain->updateLightmap();
	}
}



SPT::Terrain* TerrainManager::getTerrain()
{
	return mTerrain;
}

bool TerrainManager::getTerrainLoaded() const
{
	return mTerrain;
}

Ogre::Real TerrainManager::getWidth() const
{
	return *mMapWidth;
}

Ogre::Real TerrainManager::getHeight() const
{
	return *mMapHeight;
}

