/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "EntityAndSceneNodeManager.h"
#include "Ogre3dManager.h"


 
Ogre::SceneNode* const EntityAndSceneNodeManager::createSceneNode(std::string name)
{
	return Ogre3dManager::getSingletonPtr()->getSceneManager()->getRootSceneNode()->
		createChildSceneNode("sn_" + name);
}

Ogre::Entity* const EntityAndSceneNodeManager::createEntity(std::string name, std::string meshName)
{
	return Ogre3dManager::getSingletonPtr()->getSceneManager()->
	createEntity("ent_" + name, meshName + ".mesh");
}
	 
void EntityAndSceneNodeManager::attachEntityToSceneNode(Ogre::Entity* const entity, 
	Ogre::SceneNode* const sceneNode)
{
	sceneNode->attachObject(entity);
}


Ogre::SceneNode* const EntityAndSceneNodeManager::createChildSceneNode(
	Ogre::SceneNode* const parentSceneNode, std::string name)
{
	return parentSceneNode->createChildSceneNode("sn_child_" + name);
}


void EntityAndSceneNodeManager::detachAllObjects(Ogre::SceneNode* const sceneNode)
{
	sceneNode->detachAllObjects();
}

  
void EntityAndSceneNodeManager::destroySceneNode(Ogre::SceneNode* const sceneNode)
{
	Ogre3dManager::getSingleton().getSceneManager()->destroySceneNode(sceneNode);
}
 
void EntityAndSceneNodeManager::destroyEntity(Ogre::Entity* const entity)
{
	Ogre3dManager::getSingleton().getSceneManager()->destroyEntity(entity);
}



void EntityAndSceneNodeManager::destroyAllEntities()
{
	EntityAndSceneNodeManager::_destroyAllEntities( 
		Ogre3dManager::getSingletonPtr()->getRootSceneNode() );
}

void EntityAndSceneNodeManager::_destroyAllEntities(Ogre::Node* const n)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::Entity* ent;
	Ogre::MovableObject *m;

	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	std::string tmp;

	while(object_it.hasMoreElements())
	{
		m = object_it.getNext();

		// Check if the type is a entity and if it is, then delete it;
		if( m->getMovableType() == "Entity" )
		{
			tmp = m->getName();

			// Our entities start with "ent_" that way we know what is what.
			// Only delete the entities that start with "ent_";
			if ( tmp.compare(0,4,"ent_") == 0 )
			{
				ent = sceneManager->getEntity( m->getName() );

				m->getParentSceneNode()->detachObject( m->getName() );
				sceneManager->destroyEntity( ent );
			}
		}
	}
	while(node_it.hasMoreElements())
	{
		EntityAndSceneNodeManager::_destroyAllEntities( node_it.getNext() );
	}
}




void EntityAndSceneNodeManager::destroyAllSceneNodes()
{
	EntityAndSceneNodeManager::_destroyAllSceneNodes(
		Ogre3dManager::getSingletonPtr()->getRootSceneNode() );
}

void EntityAndSceneNodeManager::_destroyAllSceneNodes(Ogre::SceneNode *n)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();
	std::string tmp;

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		Ogre::SceneNode* child = (Ogre::SceneNode*) node_it.getNext();
		tmp = child->getName();

		// Our Scenenodes start with "sn_" that way we know what is what.
		// Only delete the scenenodes that start with "sn_";
		if ( tmp.compare(0,3,"sn_") == 0 )
		{
			sceneManager->getRootSceneNode()->removeAndDestroyChild(tmp);
		}
	}
}






 
void EntityAndSceneNodeManager::setSceneNodeScale(Ogre::SceneNode* const sceneNode,
	const Ogre::Real x, const Ogre::Real y, const Ogre::Real z)
{
	sceneNode->setScale(x, y, z);

	// Log an message because scaling slows things down //
	Ogre3dManager::getSingletonPtr()->getOgre3dLogManager()->
		logMessage( "EntityAndSceneNodeManager::setSceneNodeScale()", 
			"SCALING: " + sceneNode->getName() ); 
}