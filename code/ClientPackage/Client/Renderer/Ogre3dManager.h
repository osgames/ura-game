/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __OGRE3D_MANAGER_H__
#define __OGRE3D_MANAGER_H__

 
#include <Ogre.h>
#include "Ogre3dResourceManager.h"
#include "Ogre3dLogManager.h"
#include "EntityAndSceneNodeManager.h"
  
class Ogre3dManager : public Ogre::Singleton<Ogre3dManager>
{
protected:
	Ogre3dResourceManager* mOgre3dResourceManager;
	Ogre3dLogManager* mOgre3dLogManager;
	EntityAndSceneNodeManager* mEntityAndSceneNodeManager;

	Ogre::Root* mRoot;
	Ogre::SceneManager* mSceneManager;
	Ogre::RenderWindow* mRenderWindow;
	Ogre::Camera* mCamera;
	Ogre::Viewport* mViewport;
public:
	Ogre3dManager();
	~Ogre3dManager();

	static Ogre3dManager* const getSingletonPtr();
	 
	Ogre::Root* const getRoot() const;
	Ogre::SceneNode* const getRootSceneNode() const;
	Ogre::SceneManager* const getSceneManager() const;
	Ogre::RenderWindow* const getRenderWindow() const;
	Ogre::Camera* const getCamera() const;
	Ogre::Viewport* const getViewport() const;

	Ogre3dLogManager* const getOgre3dLogManager() const;

	void startRendering() const;
	void addFrameListener(Ogre::FrameListener* const frameListener) const;

	/// Go through each material and disable the lighting ///
	void disableAllMaterialLighting();

	//////////////////////////////////////////////////////
	/// Functions dealing with entities and sceneNodes ///
	//////////////////////////////////////////////////////
	Ogre::SceneNode* const createSceneNode(std::string name);
	Ogre::Entity* const createEntity(std::string name, std::string meshName);
	void attachEntityToSceneNode(Ogre::Entity* const entity, 
		Ogre::SceneNode* const sceneNode); 
	Ogre::SceneNode* const createChildSceneNode(
		Ogre::SceneNode* const parentSceneNode, std::string name);

	void detachAllObjects(Ogre::SceneNode* const sceneNode);

	void destroySceneNode(Ogre::SceneNode* const sceneNode);
	void destroyEntity(Ogre::Entity* const entity);

	void destroyAllEntities();
	void destroyAllSceneNodes();
	void destroyAllEntitiesAndSceneNodes();

	void setSceneNodeScale(Ogre::SceneNode* const sceneNode,
		const Ogre::Real x, const Ogre::Real y, const Ogre::Real z);
};


#endif

