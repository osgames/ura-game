/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ENTITYANDSCENENODEMANAGER_H__
#define __ENTITYANDSCENENODEMANAGER_H__

#include <Ogre.h>


class EntityAndSceneNodeManager 
{
protected:
	void _destroyAllEntities(Ogre::Node* const n);
	void _destroyAllSceneNodes(Ogre::SceneNode* const n);
public:
	Ogre::SceneNode* const createSceneNode(std::string name);

	Ogre::Entity* const createEntity(std::string name, std::string meshName);
	  
	void attachEntityToSceneNode(Ogre::Entity* const entity, 
		Ogre::SceneNode* const sceneNode); 

	Ogre::SceneNode* const createChildSceneNode(
		Ogre::SceneNode* const parentSceneNode, std::string name);

	void detachAllObjects(Ogre::SceneNode* const sceneNode);

	void destroySceneNode(Ogre::SceneNode* const sceneNode);
	void destroyEntity(Ogre::Entity* const entity);


	void destroyAllEntities();
	void destroyAllSceneNodes();


	void setSceneNodeScale(Ogre::SceneNode* const sceneNode,
		const Ogre::Real x, const Ogre::Real y, const Ogre::Real z);
};


#endif

