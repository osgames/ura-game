/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __TERRAIN_MANAGER_H__
#define __TERRAIN_MANAGER_H__


/**********************/
/*** INCLUDE FILES  ***/
/**********************/
#include <vector>
#include <string>
#include <Ogre.h>

#include <Prerequisites.h>
#include <Terrain.h>
#include <BrushDisplacement.h>
#include <Heightmap.h>


class TerrainManager
{
protected:
	SPT::Terrain* mTerrain;
	Ogre::Real* mMapWidth;
	Ogre::Real* mMapHeight;

	Ogre::Light* mSunLight;

	std::vector<std::string>* mVtextureNames;
	std::string* mSplatmapFilename;
public:
	TerrainManager();
	~TerrainManager();

	void onFrameStart(const Ogre::Real timeSinceLastFrame);
	void onFrameEnd(const Ogre::Real timeSinceLastFrame);	

	void setNewTerrain(Ogre::Real width, Ogre::Real maxHeight, std::string splatmapFileName = "");
	void setSplatMap(std::string fileName);
	void setTextures(std::string tex0, std::string tex1, std::string tex2, std::string tex3);
	bool setHeightMapData(std::string &data, unsigned long long &dataSize);
	void setSplatScales(const Ogre::Vector4 v4);
	void setDetailScales(const Ogre::Vector4 v4);
	void updateLightMap();

	SPT::Terrain* getTerrain();
	bool getTerrainLoaded() const;
	Ogre::Real getWidth() const;
	Ogre::Real getHeight() const;
};


#endif 

