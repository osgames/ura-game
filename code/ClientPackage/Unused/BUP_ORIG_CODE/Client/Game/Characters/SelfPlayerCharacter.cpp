/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SelfPlayerCharacter.h"
#include "Ogre3dManager.h"
#include "Network.h"
#include "ClientStateManager.h"
#include "ZoneLoader.h"
#include "RayCastManager.h"
#include "MovableText.h"
#include "SoundManager.h"
#include "InputManager.h"

SelfPlayerCharacter::SelfPlayerCharacter( std::string name, bool client ) :
	mSceneNode(NULL), mTargetNode(NULL), mPitchTargetNode(NULL),
	mTimer(NULL),
	mName(name), bClient(client), client_player(client),
	mNetwork(NULL),
	mRayCastManager(NULL)
{

	mSceneMgr = Ogre3dManager::getSingleton().getSceneManager();
	mSceneNode = mSceneMgr->getRootSceneNode()->createChildSceneNode( "PLAYERNODE_" + name );
	mEntity = mSceneMgr->createEntity( name + "_Entity", "ninja.mesh" );

	mSceneNode->attachObject( mEntity );
	orientation = mSceneNode->getOrientation();
	last_orientation = orientation;
	network_orientation = orientation;
	last_network_orientation = orientation;
	last_last_network_orientation = orientation;
	position = Ogre::Vector3( 0, 6000, 0 );
	last_position = Ogre::Vector3( 0, 6000, 0 );
	network_position = Ogre::Vector3( 0, 6000, 0 );
	last_network_position = position;
	last_last_network_position = last_network_position;
	mTargetNodeOffset = Ogre::Vector3( 50, 200, 0 ); // What the camera will look at.

	mTimer = new Ogre::Timer();
	mTimer->reset();
	mGSM = ClientStateManager::getSingletonPtr();

	mOrientationTimer = 0;

	mNetwork = Network::getSingletonPtr();

	average_time_between_orientation_change = 0.0;
	time_between_orientation_change = 0.0;

	noclip = false;

	mRayCastManager = new RayCastManager();

	characterMovement = 6; // Roughly estimated default value; should never be used, just here in case.

	mPlayerLabel = 0;

	mAnimationState = 0;

	if( !client )
	{
		mPlayerLabel = new Ogre::MovableText("PLAYER_LABEL_" + mName, mName, "StarWars", 10.0);
		mPlayerLabel->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE); // Center horizontally and display above the node
		mPlayerLabel->setAdditionalHeight(18.0);
		mSceneNode->attachObject(mPlayerLabel);
	}
	else if (client)
	{
		mCamera = mSceneMgr->getCamera( "camera" );
/********************************
		mTargetNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
		mPitchTargetNode = mTargetNode->createChildSceneNode();
		mTargetNode->setPosition( mSceneNode->getPosition() + mTargetNodeOffset );
		mCameraNode = mPitchTargetNode->createChildSceneNode();
		mCameraNode->setAutoTracking( true, mTargetNode );
		mCameraNode->setFixedYawAxis( true );
		mCameraNode->setPosition( 0, 0, 0 );
		mCamera->setPosition( 0, 0, 0 );
		mCameraNode->attachObject( mCamera );
		mCameraNode->setPosition( 0, 30, 50 );
*******************************/


		mTargetNode = mSceneNode->createChildSceneNode();
		mTargetNode->translate( mTargetNodeOffset );
		//mTargetNode->setPosition( mSceneNode->getPosition() );

		mCamera->setPosition( 0, 0, 0 );
		mPitchTargetNode = mTargetNode->createChildSceneNode();
		mPitchTargetNode->attachObject( mCamera );
		mPitchTargetNode->translate( 0, 0, 300 ); // Camera position translated from the target node.
		mCamera->lookAt( mTargetNode->getPosition() );

		// This is acting as pitch for now, so make the camera pitch a little bit;
		mTargetNode->pitch( Ogre::Degree(-10) );

		mOrientationTimer = new Ogre::Timer();
		mOrientationTimer->reset();
	}
	mSceneNode->setScale( 0.08, 0.08, 0.08 );

	mAnimationState = mEntity->getAnimationState( "Idle1" );
	mAnimationState->setLoop( false );
    mAnimationState->setEnabled( false );

	mCurrentMoveSound = 0;
	mSoundMgr = SoundManager::getSingletonPtr();
	mWalkSound = mSoundMgr->createSound( "walk.wav" );
}

SelfPlayerCharacter::~SelfPlayerCharacter()
{
	mSoundMgr->destroySound( mWalkSound );

	if( mOrientationTimer )
	{
		delete mOrientationTimer;
		mOrientationTimer = 0;
	}

	if( mPlayerLabel )
	{
		delete mPlayerLabel;
		mPlayerLabel = 0;
	}

	if( mRayCastManager )
	{
		delete mRayCastManager;
		mRayCastManager = 0;
	}

	if (mTimer)
	{
		delete mTimer;
		mTimer = 0;
	}


	mSceneNode->detachAllObjects();
	mSceneMgr->destroyEntity( mEntity );


	if (mPitchTargetNode)
	{
		mSceneMgr->destroySceneNode( mPitchTargetNode->getName() );
		mPitchTargetNode = 0;
	}

	if (mTargetNode)
	{
		mSceneMgr->destroySceneNode( mTargetNode->getName() );
		mTargetNode = 0;
	}

	if (mSceneNode)
	{
		mSceneMgr->destroySceneNode( mSceneNode->getName() );
		mSceneNode = 0;
	}


	mNetwork = 0;
}

void SelfPlayerCharacter::update( Ogre::Real timeSinceLastFrame )
{
	characterMovement = 100.0f * timeSinceLastFrame;

	// Eventually interpolation will be handled here.  For now, the player will just skip to positions.
	//if( bClient )
	//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( position ) );

    if( mAnimationState )
        mAnimationState->addTime(timeSinceLastFrame);


	if( last_orientation != orientation && bClient )
	{
		if( mOrientationTimer->getMilliseconds() >= 75 )
		{
			Network::getSingletonPtr()->sendOrientationUpdate( orientation );
			mOrientationTimer->reset();
		}
	}
	else if( !bClient )
	{
		// Linear interpolation.
		// Possibly multiplied by the number of snapshots per second?  I'll test it later to find out for sure.
		/*Ogre::Vector3 interp = last_network_position - last_last_network_position;
		updated_translate.x = interp.x * timeSinceLastFrame * 2;
		updated_translate.y = interp.y * timeSinceLastFrame * 2;
		updated_translate.z = interp.z * timeSinceLastFrame * 2;
		position = position + updated_translate;*/


		// Quaternion interpolation.
		/*Ogre::Radian quat_interp = last_network_orientation.getYaw() - last_last_network_orientation.getYaw();*/
		//Ogre::Quaternion q = Ogre::Quaternion::FromAngleAxis( quat_interp, Ogre::Vector3(0,1,0) );
		//Ogre::Real predicted = orientation.getYaw() + quat_interp; // The predicted orientation we need to slerp to.
		//Ogre::Quaternion q;
		//q.FromAngleAxis( quat_interp * timeSinceLastFrame * 2, Ogre::Vector3::UNIT_Y ); // Modified quaternion change.
		//orientation = orientation + q;



		// None //
		//if (mMovementDirection == URA::MovementDirection::NONE)
		//{
		//
		//}
		// Forward
		if (mMovementDirection == URA::MovementDirection::FORWARD)
		{
			this->directionalTranslate( Ogre::Vector3(0, 0, -characterMovement) );
		}
		// Backward 
		else if (mMovementDirection == URA::MovementDirection::BACKWARD)
		{
			this->directionalTranslate( Ogre::Vector3(0, 0, characterMovement) );
		}
		// Left //
		else if (mMovementDirection == URA::MovementDirection::LEFT)
		{
			this->directionalTranslate( Ogre::Vector3(-characterMovement, 0, 0) );
		}
		// Right //
		else if (mMovementDirection == URA::MovementDirection::RIGHT)
		{
			this->directionalTranslate( Ogre::Vector3(characterMovement, 0, 0) );
		}




		if( mGSM->getCurrentState() == AS_GAME )
		{
		    //Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString(position) );
			Ogre::Vector3 terrain_position = position;
			Ogre::Vector3 intersection = position;
			if( mGSM->getGame()->getZoneLoader()->getHeightAt( terrain_position ) ) // Fixates the height to the terrain.
			{
				//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString(position) );
			}
			if( mRayCastManager->RaycastFromPoint( Ogre::Vector3(position.x, position.y + 5, position.z), Ogre::Vector3::NEGATIVE_UNIT_Y, intersection ) )
			{
				// Check to see if terrain_position or the raycast is higher.
				if( intersection.y > terrain_position.y )
				{
					position.y = intersection.y;
				}
				else
				{
					position.y = terrain_position.y;
				}
			}
			else
			{
				position.y = terrain_position.y;
			}
		}

	}
	/*else if( last_orientation != orientation && !bClient )
	{
		if( average_time_between_orientation_change != 0.0 )
		{
			time_between_orientation_change = (double)mTimer->getMilliseconds();
			average_time_between_orientation_change += time_between_orientation_change;
			average_time_between_orientation_change /= 2.0;
		}
		else if( average_time_between_orientation_change == 0.0 )
		{
			time_between_orientation_change = (double)mTimer->getMilliseconds();
			average_time_between_orientation_change += time_between_orientation_change;
		}
		mTimer->reset();
	}
	else if( last_orientation == orientation && !bClient )
	{
		orientation = (1.0 / average_time_between_orientation_change) * (orientation - last_orientation);
	}*/

	if( bClient )
	{
		if( !noclip )
		{
			//Ogre::Vector3 result;
			//RaycastFromPoint( position, Ogre::Vector3::NEGATIVE_UNIT_Y, result );
			//position = Ogre::Vector3( position.x, result.y + 8.5, position.z );
			if( mGSM->getCurrentState() == AS_GAME )
			{
			    //Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString(position) );
				Ogre::Vector3 terrain_position = position;
				Ogre::Vector3 intersection = position;
				if( mGSM->getGame()->getZoneLoader()->getHeightAt( terrain_position ) ) // Fixates the height to the terrain.
				{
					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString(position) );
				}
				if( mRayCastManager->RaycastFromPoint( Ogre::Vector3(position.x, position.y + 5, position.z), Ogre::Vector3::NEGATIVE_UNIT_Y, intersection ) )
				{
					// Check to see if terrain_position or the raycast is higher.
					if( intersection.y > terrain_position.y )
					{
						position.y = intersection.y;
					}
					else
					{
						position.y = terrain_position.y;
					}
				}
				else
				{
					position.y = terrain_position.y;
				}
			}
		}
		if( (last_position != position) && mTimer->getMilliseconds() >= 500 )
		{
			mNetwork->sendPositionToServer( position );
			mTimer->reset();
		}

		mSoundMgr->setPlayerPosition( position );
		mSoundMgr->setPlayerOrientation( orientation );
	}
	mWalkSound->setPosition( position );
	mSceneNode->setPosition( position );
	mSceneNode->setOrientation( orientation );
	 
	//if( mTargetNode )
	//	mTargetNode->setPosition( mSceneNode->getPosition() + mTargetNodeOffset );

	last_orientation = orientation;
	last_position = position;
}

void SelfPlayerCharacter::rotatePlayerAndCam(double direction)
{
	mSceneNode->yaw( Ogre::Degree(direction) );
	setOrientation( mSceneNode->getOrientation() );
}

void SelfPlayerCharacter::pitchPlayerView(double direction)
{
	mTargetNode->pitch( Ogre::Degree(direction) );
}

Ogre::Quaternion SelfPlayerCharacter::getCameraOrientation()
{
	return mCamera->getDerivedOrientation();
}

Ogre::Quaternion SelfPlayerCharacter::getTargetOrientation()
{
	return mTargetNode->getOrientation();
}

//void SelfPlayerCharacter::translate( Ogre::Vector3 dir )
//{
//	//if( mGSM->getCurrentState() == AS_GAME )
// //   {
// //       Ogre::Vector3 intersection;
// //       float depth;
// //       if( mGSM->getGame()->getZoneLoader()->rayCast( dir, position, intersection, depth ) )
// //       {
// //           if( depth < 15 )
// //           {
// //               // Don't allow him to move in this direction.
// //           }
// //           else
// //           {
// //               //position = position + (orientation * dir);
// //               position = position + dir;
// //           }
// //       }
// //       else
// //       {
// //           //position = position + (orientation * dir);
// //           position = position + dir;
// //       }
//
// //       if( mGSM->getGame()->getZoneLoader()->rayCast( Ogre::Vector3(0,-1,0), position, intersection, depth ) )
// //       {
// //           if( depth < 5 )
// //           {
// //               // Don't allow him to move in this direction.
// //           }
// //           else
// //           {
// //               // Fall.
// //           }
// //       }
// //       else
// //       {
// //           // Fall.
// //       }
// //   }
//}

void SelfPlayerCharacter::directionalTranslate( Ogre::Vector3 dir )
{
	if( mGSM->getCurrentState() == AS_GAME )
    {
		Ogre::Real depth = 101;
		Ogre::Vector3 intersection;
		if( mRayCastManager->RaycastFromPoint( position, orientation * dir, intersection ) )
		{
			// If RaycastFromPoint return false, it's most likely unable to see in front of it,
			// so there's no need to log it as an error.
			depth = position.distance( intersection );
		}

		// If statement is here since RaycastFromPoint doesn't return true if objects are far enough away.
		if( depth > 5 )
		{
			position = position + (orientation * dir);
		}
		else if( mRayCastManager->RaycastFromPoint( Ogre::Vector3(position.x, position.y + 3, position.z), orientation * dir, intersection ) )
		{
			// In case there are stairs.
			depth = position.distance( intersection );
			if( depth > 5 )
			{
				position = position + (orientation * dir);
			}
		}
    }
}

Ogre::Quaternion SelfPlayerCharacter::getPlayerOrientation()
{
	return mSceneNode->getOrientation();
}

Ogre::Vector3 SelfPlayerCharacter::getPosition()
{
	return position;
}

void SelfPlayerCharacter::setPosition( Ogre::Vector3 pos )
{
#ifdef HEAVY_LOGGING
	Ogre::LogManager::getSingleton().logMessage(
		mName + " SelfPlayerCharacter::setPosition: " + Ogre::StringConverter::toString(pos) );
#endif
	last_last_network_position = last_network_position;
	last_network_position = network_position;
	position = pos;
	network_position = pos;
}

void SelfPlayerCharacter::setNoclip( bool clip )
{
	if( bClient )
		noclip = clip;
	else
		Ogre::LogManager::getSingleton().logMessage(
			"Warning: could not set noclip on non-client character." );
}

void SelfPlayerCharacter::notifyNetworkUpdate()
{
}

void SelfPlayerCharacter::setOrientation( Ogre::Quaternion quat )
{
	last_last_network_orientation = last_network_orientation;
	last_network_orientation = network_orientation;
	orientation = quat;
	network_orientation = quat;
}

void SelfPlayerCharacter::setSoundAndAnimationOfMovementDirection(URA::MovementDirection movementDirection)
{
	/// Make sure were not recieving the same direction ///
	if (mMovementDirection != movementDirection)
	{
		// Store the new Direction //
		mMovementDirection = movementDirection;


		//////////////////////////////////////////////////////////////
		/// Set the sound and animation according to the direction ///
		//////////////////////////////////////////////////////////////

		// None //
		if (movementDirection == URA::MovementDirection::NONE)
		{
			mAnimationState->setLoop( false );
			mAnimationState->setEnabled( false );
			mAnimationState = mEntity->getAnimationState( "Idle2" );
			mAnimationState->setLoop( true );
			mAnimationState->setEnabled( true );
			if( mCurrentMoveSound )
			{
				mCurrentMoveSound->stop();
				mCurrentMoveSound->setLoop( false );
			}
		}
		// Forward //
		else if (movementDirection == URA::MovementDirection::FORWARD)
		{
			mAnimationState->setLoop( false );
			mAnimationState->setEnabled( false );
			mAnimationState = mEntity->getAnimationState( "Walk" );
			mAnimationState->setLoop( true );
			mAnimationState->setEnabled( true );
			if( mCurrentMoveSound )
			{
				mCurrentMoveSound->stop();
				mCurrentMoveSound->setLoop( false );
			}
			mWalkSound->setLoop( true );
			mWalkSound->play();
			mCurrentMoveSound = mWalkSound;	
		}
		// Backward //
		else if (movementDirection == URA::MovementDirection::BACKWARD)
		{

		}
		// Left //
		else if (movementDirection == URA::MovementDirection::LEFT)
		{

		}
		// Right //
		else if (movementDirection == URA::MovementDirection::RIGHT)
		{

		}
	}
}

