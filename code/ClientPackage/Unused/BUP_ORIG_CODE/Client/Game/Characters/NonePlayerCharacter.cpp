/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NonePlayerCharacter.h"
#include "Ogre3dManager.h"

 

NonePlayerCharacter::NonePlayerCharacter(std::string _name, std::string _model )
{
	Ogre::SceneManager* const sceneManager = Ogre3dManager::getSingleton().getSceneManager();

	name = _name;
	model = _model;
	scene_node = sceneManager->getRootSceneNode()->createChildSceneNode("sn_npc_" + name);
	entity = sceneManager->createEntity("ent_npc_" + name, model);
	scene_node->attachObject(entity);
	position_change = false;
}

NonePlayerCharacter::~NonePlayerCharacter()
{

}

std::string NonePlayerCharacter::getName()
{
	return name;
}

void NonePlayerCharacter::update()
{
	if( position_change )
	{
		scene_node->setPosition( position );
		position_change = false;
	}
}

void NonePlayerCharacter::setPosition(Ogre::Vector3 pos)
{
	position_change = true;
	position = pos;
}

void NonePlayerCharacter::setGreetingText(std::string text)
{
	greeting_text = text;
}

const std::string& NonePlayerCharacter::getGreetingText() const
{
	return greeting_text;
}

void NonePlayerCharacter::setMoney(int amount)
{
	money = amount;
}

int NonePlayerCharacter::getMoney()
{
	return money;
}

Ogre::Vector3 NonePlayerCharacter::getPosition()
{
	return position;
}
