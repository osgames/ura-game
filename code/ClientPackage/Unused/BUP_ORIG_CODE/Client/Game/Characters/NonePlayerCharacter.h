/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __NONPLAYERCHARACTER_H__
#define __NONPLAYERCHARACTER_H__

#include <vector>
#include <string>
#include <Ogre.h>



class NonePlayerCharacter
{
private:
	std::vector<std::string> inventory_list;
	Ogre::Vector3 position;
	float orientation_w;
	float orientation_x;
	float orientation_y;
	float orientation_z;
	std::string model;
	std::string greeting_text;
	std::string name;
	//std::vector<Quest *> available_quests;

	Ogre::SceneNode* scene_node;
	Ogre::Entity* entity;

	bool position_change;

	int money; // Stored as the lowest unit of currency in the game.
public:
	NonePlayerCharacter(std::string name, std::string model);
	~NonePlayerCharacter();

	void update();

	void setPosition(Ogre::Vector3 pos);
	void setGreetingText(std::string text);
	const std::string& getGreetingText() const;
	void setMoney(int amount);

	std::string getName();
	int getMoney();
	Ogre::Vector3 getPosition();
};

#endif
