/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ZoneLoader.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "DotScene.h"

#include <fstream>
#include <Ogre.h>

// FIXME IMP: With ODE we have to define dSINGLE OR dDOUBLE. Were using dSINGLE FOR NOW
#define dSINGLE
#include <ode/ode.h>



using namespace std;
using namespace Ogre;



ZoneLoader::ZoneLoader()
{
	ode_ray = dCreateRay( 0, 200 );
	parent_map_node = 0;

	mTerrainManager = new TerrainManager;
	mDotSceneLoader = new CDotScene();
}

ZoneLoader::~ZoneLoader()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const scene_manager = ogre3dManager->getSceneManager();
	
/*****************************************************
	// This code is pretty much not used now;
	for( unsigned int indx = 0; indx < scene_node_list.size(); indx++ )
	{
		cout << "Destroying scennode = " << scene_node_list[indx]->getName() << endl;
		scene_manager->destroySceneNode( scene_node_list[indx]->getName() );
		scene_node_list[indx] = 0;
	}
	scene_node_list.clear();

	for( unsigned int indx = 0; indx < entity_list.size(); indx++ )
	{
		scene_manager->destroyEntity( entity_list[indx] );
		entity_list[indx] = 0;
	}
	entity_list.clear();
******************************************************/
	
	/*** Free allocated memory ***/
	if (mDotSceneLoader)
	{
		delete mDotSceneLoader;
		mDotSceneLoader = NULL;
	}

	if (mTerrainManager)
	{
		delete mTerrainManager;
		mTerrainManager = NULL;
	}
}

bool ZoneLoader::loadZone( std::string file )
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const scene_manager = ogre3dManager->getSceneManager();

	// Loads from a dotscene.
	parent_map_node = scene_manager->getRootSceneNode()->createChildSceneNode("sn_ZoneLoader_root");
	mDotSceneLoader->parseDotScene(this, file, "General", parent_map_node );


	return true;
}

void ZoneLoader::loadTestZone()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const scene_manager = ogre3dManager->getSceneManager();


	Ogre::SceneNode *sn = scene_manager->getRootSceneNode()->createChildSceneNode();
	Ogre::Entity *ent = scene_manager->createEntity( "test_zone", "cube.mesh" );
	sn->attachObject( ent );
	scene_node_list.push_back( sn );
	entity_list.push_back( ent );
	sn->scale( 20, 0.2, 20 );
	ent->setMaterialName( "Examples/RustySteel" );
}

bool ZoneLoader::getHeightAt( Ogre::Vector3 &position )
{
	if (mTerrainManager)
	{
		if ( mTerrainManager->getTerrain() )
		{
			return mTerrainManager->getTerrain()->getHeightAt(position);
		}
	}

	return false;
}

bool ZoneLoader::loadPhysicsModelSeries( std::string mesh_file_name,
                                         Ogre::Vector3 offset,
                                         Ogre::Vector3 scale,
                                         Ogre::Quaternion rotation )
{
    std::string name = mesh_file_name.substr(0,mesh_file_name.size()-5); // Removes ".mesh"
    name = name.append( std::string(".phy") );
    std::fstream file( name.c_str(), std::ios::in );
    std::string s;
    while( getline( file, s ) )
    {
        loadModel( s, offset, scale, rotation );
    }
    file.close();

    return true;
}

// Loads an Ogre *.mesh.xml file into Opcode.
bool ZoneLoader::loadModel( std::string file,
                            Ogre::Vector3 offset,
                            Ogre::Vector3 scale,
                            Ogre::Quaternion rotation )
{
	file = "resources/" + file;

	int *it = 0;
	dVector3 *vec = 0;

	int f_count = 0;
	int v_count = 0;
	int counter = 0;

	TiXmlDocument *m_document;
	try
	{
		m_document = new TiXmlDocument( file.c_str() );
		m_document->LoadFile();
		if( m_document->Error() )	// Error will be reported here;
		{
			// Log the error
			std::string str1("Client/ZoneLoader.cpp:  Error opening file: ");
			str1.append(file);
			Ogre::LogManager::getSingleton().logMessage( str1.c_str() );

			delete m_document;
			m_document = NULL;
			return false;
		}
	}
	catch(...)
	{
		// Error will be reported here.
		delete m_document;
		m_document = NULL;
		return false;
	}


	TiXmlElement *m_root_element = m_document->RootElement();
	if( strcmp( m_root_element->Value(), "mesh" ) != 0 )
	{
		Ogre::LogManager::getSingleton().logMessage( "Invalid file loaded... " + file );
	}

	TiXmlElement *element = m_root_element->FirstChildElement( "submeshes" );
	while( element )
	{
		TiXmlElement *_element = element->FirstChildElement( "submesh" );
		while( _element )
		{
			TiXmlElement *__element = _element->FirstChildElement( "faces" );
			f_count = atoi(__element->Attribute( "count" ));

			// Make a dynamic array;
			it = new int[f_count*3];


			// Make sure there is a face count bigger than 0;
			if (f_count  <= 0)
			{
				if ( it )	// if 'it' has a dynamic array, then free it;
				{
					delete [] it;
				}

				Ogre::LogManager::getSingleton().logMessage( "Client/ZoneLoader.cpp:  Error Face count is less than 1 in 'loadModel()'");
				return false;
			}

			// Check for memory allocation error. If there is, then return false;
			if (it == 0)
			{
				Ogre::LogManager::getSingleton().logMessage( "Client/ZoneLoader.cpp:  Error allocating dynamic memory for 'it' in 'loadModel()'");
				return false;
			}


			//cout << "FACE COUNT = " << f_count << endl;

			if( __element )
			{
				counter = 0;	// Reset the counter;


				TiXmlElement *___element = __element->FirstChildElement( "face" );
				while( ___element )
				{
					//cout <<"FACE:  ";
					it[counter] = atoi(___element->Attribute( "v1" ));
					//cout << it[counter] << ", ";
					counter++;

					it[counter] = atoi(___element->Attribute( "v2" ));
					//cout << it[counter] << ", ";
					counter++;

					it[counter] = atoi(___element->Attribute( "v3" ));
					//cout << it[counter] << endl;
					counter++;

					___element = ___element->NextSiblingElement( "face" );
				}
				//cout << "END OF FACE" << endl;
				//cout << "count = " << counter << "  , f_count = " << f_count << endl;

				// Make sure we got all the faces. If not then free memory and return false;
				// Divide the counter by 3 (3 vertex)to get the face count;
				counter /=  3;
				if ( counter <=0 || counter != f_count)
				{
					if ( it )	// if 'it' has a dynamic array, then free it;
					{
						delete [] it;
					}

					Ogre::LogManager::getSingleton().logMessage( "Client/ZoneLoader.cpp:  Error did not get all faces in 'loadModel()'");
					return false;
				}
			}

			TiXmlElement *geometry = _element->FirstChildElement( "geometry" );
			if( geometry )
			{
				v_count = atoi(geometry->Attribute( "vertexcount" ));

				// Make a dynamic array;
				vec = new dVector3[v_count];

				// Make sure there is a vertex count bigger than 0;
				if (v_count  <= 0)
				{
					if ( it )	// if 'it' has a dynamic array, then free it;
					{
						delete [] it;
					}

					if ( vec )	// if 'vec' has a dynamic array, then free it;
					{
						delete [] vec;
					}

					Ogre::LogManager::getSingleton().logMessage( "Client/ZoneLoader.cpp:  Error Vertex count is less than 1 in 'loadModel()'");
					return false;
				}

				// Check for memory allocation error. If there is, then free memory and return false;
				if (vec == 0)
				{
					if ( it )	// if 'it' has a dynamic array, then free it;
					{
						delete [] it;
					}

					Ogre::LogManager::getSingleton().logMessage( "Client/ZoneLoader.cpp:  Error allocating dynamic memory for 'vec' in 'loadModel()'");
					return false;
				}


				counter = 0;	// Reset the counter;

				//cout << "VERTEX COUNT = " << v_count << endl;

				TiXmlElement *vertex_buffer = geometry->FirstChildElement( "vertexbuffer" );
				if( vertex_buffer )
				{
					if( Ogre::StringConverter::parseBool(vertex_buffer->Attribute( "positions" )) )
					{
						TiXmlElement *vertex = vertex_buffer->FirstChildElement( "vertex" );
						while( vertex )
						{
							//cout << "VERTEX:  " << "(" << counter << ") ";

							TiXmlElement *position = vertex->FirstChildElement( "position" );

							vec[counter][0] = atof(position->Attribute( "x" )) * ((float)scale.x);
							//cout << vec[counter][0] << ", ";

							vec[counter][1] = atof(position->Attribute( "y" )) * ((float)scale.y);
							//cout << vec[counter][1] << ", ";

							vec[counter][2] = atof(position->Attribute( "z" )) * ((float)scale.z);
							//cout << vec[counter][2] << endl;

							counter++;	// Update the counter;

							vertex = vertex->NextSiblingElement( "vertex" );
						}
						//cout << "VERTEX END" << endl;
						//cout << "counter = " << counter << "  , v_count = " << v_count << endl;

						// Make sure we got all the vertex. If not then free memory and return false;
						if ( counter != v_count)
						{
							if ( it )	// if 'it' has a dynamic array, then free it;
							{
								delete [] it;
							}

							if ( vec )	// if 'vec' has a dynamic array, then free it;
							{
								delete [] vec;
							}

							Ogre::LogManager::getSingleton().logMessage( "Client/ZoneLoader.cpp:  Error did not get all vertex in 'loadModel()'");
							return false;
						}
					}
				}
			}

			_element = _element->NextSiblingElement();
		}

		element = element->NextSiblingElement();
	}

	dGeomID geom;

	std::string type = "box"; // This is here for now for testing purposes.
	if( type == "box" )
	{
		int least = -1;
		float least_value = 0.0f;
		int greatest = -1;
		float greatest_value = 0.0f;

		for( int i = 0; i < v_count; i++ ) // Find the minimum and maximum corners of the cube.
		{
			float total = vec[i][0] + vec[i][1] + vec[i][2];
			if( least == -1 )
			{
				least_value = total;
				least = i;
			}
			if( greatest == -1 )
			{
				greatest_value = total;
				greatest = i;
			}

			if( total < least_value )
			{
				least_value = total;
				least = i;
			}
			else if( total > greatest_value )
			{
				greatest_value = total;
				greatest = i;
			}
		}

		if( least == -1 || greatest == -1 )
		{
			// Serious problem!
			Ogre::LogManager::getSingleton().logMessage( "ERROR: ZoneLoader.cpp: Failed loading physics model type box." );
			return false;
		}

		/* Need to watch this code... Could do something funky... Pull a fast one if you catch my drift. */

		Ogre::Vector3 min_corner( vec[least][0], vec[least][1], vec[least][2] );
		//cout << "min_corner x:" << min_corner.x << " y:" << min_corner.y << " z: " << min_corner.z << "\n";
		Ogre::Vector3 max_corner( vec[greatest][0], vec[greatest][1], vec[greatest][2] );
		//cout << "max_corner x:" << max_corner.x << " y:" << max_corner.y << " z: " << max_corner.z << "\n";
		Ogre::Vector3 center( (min_corner.x + max_corner.x) / 2, (min_corner.y + max_corner.y) / 2, (min_corner.z + max_corner.z) / 2 );
		//cout << "center x:" << center.x << " y:" << center.y << " z: " << center.z << "\n";
		Ogre::Vector3 fixed_max_corner( max_corner.x - center.x, max_corner.y - center.y, max_corner.z - center.z );
		//cout << "fixed_max_corner x:" << fixed_max_corner.x << " y:" << fixed_max_corner.y << " z: " << fixed_max_corner.z << "\n";

		geom = dCreateBox( 0, max_corner.x - min_corner.x, max_corner.y - min_corner.y, max_corner.z - min_corner.z );
		ode_geom.push_back( geom );

		// Distance from original (fixed) max_corner to the new max_corner.
		float dist_orig_new = sqrt( pow((max_corner.x - fixed_max_corner.x),2) +
					    pow((max_corner.y - fixed_max_corner.y),2) +
					    pow((max_corner.z - fixed_max_corner.z),2) );
		//cout << "dist_orig_new: " << dist_orig_new << "\n";

		// Distance from center of the box to the new max_corner.
		float dist_center_new = sqrt( pow((max_corner.x - center.x),2) +
					      pow((max_corner.y - center.y),2) +
					      pow((max_corner.z - center.z),2) );
		//cout << "dist_center_new: " << dist_center_new << "\n";

		// Returns angle between the two (i.e. how much the modeler rotated it).
		float angle = asin( dist_orig_new / dist_center_new );
		//cout << "angle: " << angle << "\n";

		// Now the primitive can be rotated to the proper orientation.
		dQuaternion d_rotation;
		dQFromAxisAndAngle( d_rotation, 0, 1, 0, angle );
		Ogre::Quaternion ogre_quat( d_rotation[0], d_rotation[1], d_rotation[2], d_rotation[3] );
		Ogre::Quaternion new_quat = ogre_quat + rotation;
		d_rotation[0] = new_quat.w;
		d_rotation[1] = new_quat.x;
		d_rotation[2] = new_quat.y;
		d_rotation[3] = new_quat.z;


		dGeomSetQuaternion( geom, d_rotation );
	}



	//ode_model = dGeomTriMeshDataCreate();
	//dGeomTriMeshDataBuildSimple( ode_model, (dReal*)vec, v_count, it, f_count );
	//dGeomTriMeshDataBuildSingle( ode_model, (dReal*)vec, 3 * sizeof(float), v_count, it, f_count, 3 * sizeof(int) );
	//ode_geom = dCreateTriMesh( space, ode_model, 0, 0, 0 );
	dGeomSetPosition( geom, offset.x, offset.y, offset.z );
	//cout << dGeomGetPosition( ode_geom )[1];
	//dGeomSetPosition( ode_geom, 0, 0, 0 );

	// Free the dynamic memory;
	if( it ) delete [] it;
	if( vec ) delete [] vec;
	delete m_document;

	return true;
}

void ZoneLoader::frameStart( Ogre::Real timeSinceLastFrame )
{
	if (mTerrainManager)
	{
		mTerrainManager->onFrameStart(timeSinceLastFrame);
	}
}

void ZoneLoader::frameEnd( Ogre::Real timeSinceLastFrame )
{
	if (mTerrainManager)
	{
		mTerrainManager->onFrameEnd(timeSinceLastFrame);
	}
}