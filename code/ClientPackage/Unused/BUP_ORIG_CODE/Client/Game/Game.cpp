/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Game.h"
#include "ClientGlobals.h"
#include "Network.h"
#include "NetworkStructures.h"
#include "Ogre3dManager.h"
#include "ClientStateManager.h"
#include "ZoneLoader.h"
#include "SelfPlayerCharacter.h"
#include "NonePlayerCharacter.h"
#include "SoundManager.h"
#include "MovementDirection.h"






 
 
Game::Game()
{
	mZoneLoader = new ZoneLoader();
	mSelfPlayerCharacter = new SelfPlayerCharacter( "client_player", true );
	mNetwork = Network::getSingletonPtr();

	// Get the InputKeyCollector //
	mInputKeyCollector = InputManager::getSingletonPtr()->getInputKeyCollector();

	// Get the InputMouseCollector //
	mInputMouseCollector = InputManager::getSingletonPtr()->getMouseKeyCollector();

	// Create InsideGameGui //
	mInsideGameGui = new InsideGameGui();


	cam_rotate_direction_x = 0.0;
	cam_rotate_direction_y = 0.0;
	rotate_direction_y = 0.0;


	x__ = false;
	timer = new Timer();
	unique_num = 0;
	mPrevFrameTime = 0.0;


	Sound* themeSong = NULL;



	// Load the zone //
	//mZoneLoader->loadTestZone(); // For development.
	mZoneLoader->loadZone("devtestmap1.map");
   
	

	// Request the inventory snapshot //
	mNetwork->request_Full_Inventory_Snapshot();



	// Disable main menu sound //
	themeSong = SoundManager::getSingletonPtr()->getSound(DEF_CLIENTGLOBALS_THEMESONG_FILENAME);
	if (themeSong)
	{
		themeSong->stop();
		if( !SoundManager::getSingletonPtr()->destroySound(themeSong) )
		{
			Ogre::LogManager::getSingleton().logMessage( "Game.cpp: Sound not destroyed properly." );
		}
	}
	else
	{
		Ogre::LogManager::getSingleton().logMessage( "Game.cpp: Could not find ot2.wav in SoundManager in order to destroy it." );
	}
}

Game::~Game()
{
	for( unsigned int indx = 0; indx < player_list.size(); indx++ )
	{
		delete player_list[indx];
	}

	player_list.clear();


	if (timer)
	{
		delete timer;
		timer = NULL;
	}

	if (mSelfPlayerCharacter)
	{
		delete mSelfPlayerCharacter;
		mSelfPlayerCharacter = NULL;
	}

	if (mZoneLoader)
	{
		delete mZoneLoader;
		mZoneLoader = NULL;
	}

	if (mInsideGameGui)
	{
		delete mInsideGameGui;
		mInsideGameGui = NULL;
	}


	/*** Destory all the scenNodes and entities ***/
	Game::destroyAllEntitiesAndSceneNodes();
}

 





void Game::frameStarted(Ogre::Real timeSinceLastFrame)
{
	mPrevFrameTime = timeSinceLastFrame;

	/// Update ZoneLoader ///
	if (mZoneLoader)
	{
		mZoneLoader->frameStart(timeSinceLastFrame);
	}




	// If a InsideGameGui window is active or a Return key is down than update InsideGameGui input //
	// When ever a return/enter key is down it means were dealing with gui windows //
	if ( 
		mInsideGameGui->isWindowActive() || 
		mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_RETURN) ||
		mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_ESCAPE)
		)
	{
		mInsideGameGui->updateInput();
	}
	else
	{
		////////////////
		/// KEYBOARD ///
		////////////////

		// HALT?
		if ( mInputKeyCollector->getKeyPressedCount() == 0 )
		{
			Network::getSingletonPtr()->sendIdToServer(ID_HALT);
			mSelfPlayerCharacter->setSoundAndAnimationOfMovementDirection(URA::MovementDirection::NONE);
		}
		else
		{
			float characterMovement = 100.0f * timeSinceLastFrame;
			RakNet::BitStream out;

			unsigned char Id = ID_RAYCAST_REQUEST;
			float x_position = mSelfPlayerCharacter->getPosition().x;
			float y_position = mSelfPlayerCharacter->getPosition().y;
			float z_position = mSelfPlayerCharacter->getPosition().z;

			out.Write( Id );
			out.Write(x_position);
			out.Write(y_position);
			out.Write(z_position);

			Network::getSingletonPtr()->mPeer->Send( &out, MEDIUM_PRIORITY, RELIABLE_ORDERED, 0, Network::getSingletonPtr()->game_server_address, false );

			// FORWARD?
			if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_W) )
			{
				mSelfPlayerCharacter->directionalTranslate( Ogre::Vector3(0, 0, -characterMovement) );

				if( !x__ )
				{
					timer->reset();
					x__ = true;
				}

				if( x__ )
				{
					//if( timer->getMilliseconds() >= 500 )
					//{
					//	//Ogre::LogManager::getSingleton().logMessage( "DEBUG:" );
					//	//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( mSelfPlayerCharacter->position.z ) );
					//	//timer->reset();
					//}

					//Ogre::LogManager::getSingleton().logMessage( Ogre::StringConverter::toString( timer->getMilliseconds() ) );
				}

				Network::getSingletonPtr()->sendIdToServer(ID_FORWARD);
				mSelfPlayerCharacter->setSoundAndAnimationOfMovementDirection(URA::MovementDirection::FORWARD);
			}
			// BACKWARD //
			else if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_S) )
			{
				mSelfPlayerCharacter->directionalTranslate( Ogre::Vector3(0, 0, characterMovement) );
				Network::getSingletonPtr()->sendIdToServer(ID_BACK);
				mSelfPlayerCharacter->setSoundAndAnimationOfMovementDirection(URA::MovementDirection::BACKWARD);

			}
			// LEFT //
			else if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_A) )
			{
				mSelfPlayerCharacter->directionalTranslate( Ogre::Vector3(-characterMovement, 0, 0) );
				Network::getSingletonPtr()->sendIdToServer(ID_LEFT);
				mSelfPlayerCharacter->setSoundAndAnimationOfMovementDirection(URA::MovementDirection::LEFT);
			}
			// RIGHT //
			else if ( mInputKeyCollector->isKeyPressed1(InputKeyCollector::IC_KEY_D) )
			{
				mSelfPlayerCharacter->directionalTranslate( Ogre::Vector3(characterMovement, 0, 0) );
				Network::getSingletonPtr()->sendIdToServer(ID_RIGHT);
				mSelfPlayerCharacter->setSoundAndAnimationOfMovementDirection(URA::MovementDirection::RIGHT);
			}
		}


		///////////////////////////
		/// Update the GUI data ///
		///////////////////////////
		mInsideGameGui->updateData();


		/////////////
		/// MOUSE ///
		/////////////
		if ( mInputMouseCollector->isLeftButtonPressed() )
		{
			if ( mInputMouseCollector->isMoving() )
			{
				
				this->rotatePlayerCam_X( -(double)mInputMouseCollector->getMoveRelativeX() * 0.9 );
				this->rotatePlayerCam_Y( -(double)mInputMouseCollector->getMoveRelativeY() * 0.35 );
			}
		}
	}



	for( unsigned int indx = 0; indx < player_list.size(); indx++ )
	{
		player_list[indx]->update(timeSinceLastFrame);
	}

	// Update NPC list.
	for( unsigned int indx = 0; indx < npc_list.size(); indx++ )
	{
		npc_list[indx]->update();
	}



	mSelfPlayerCharacter->rotatePlayerAndCam(cam_rotate_direction_x);
	mSelfPlayerCharacter->pitchPlayerView(cam_rotate_direction_y);

	mSelfPlayerCharacter->update(timeSinceLastFrame);

	cam_rotate_direction_x = 0.0;
	cam_rotate_direction_y = 0.0;
}


void Game::frameEnded(Ogre::Real timeSinceLastFrame)
{
	/// Update ZoneLoader ///
	if (mZoneLoader)
	{
		mZoneLoader->frameEnd(timeSinceLastFrame);
	}
}



void Game::fullUpdateAllPlayers()
{
/***************************
	for( unsigned int indx = 0; indx < player_list.size(); indx++ )
	{
		delete player_list[indx];
	}
	player_list.clear();

	for( unsigned int indx = 0; indx < mNetwork->player_list.size(); indx++ )
	{
		if( mNetwork->player_list[indx]->name.compare( "__CLIENT__" ) == 0 )
		{
			//mSelfPlayerCharacter->setPosition( mNetwork->player_list[indx]->position );
			//mSelfPlayerCharacter->orientation = mNetwork->player_list[indx]->orientation;
			//mSelfPlayerCharacter->update( mPrevFrameTime ); // Force update so that it renders.
		}
		else
		{
			SelfPlayerCharacter *player = new SelfPlayerCharacter( mNetwork->player_list[indx]->name );
			player_list.push_back( player );

			player->updated = false;
			player->setPosition( mNetwork->player_list[indx]->position );
			player->setOrientation( mNetwork->player_list[indx]->orientation );
			player->update( mPrevFrameTime ); // Force update so that it renders.
			player->in_region = true;
			player->notifyNetworkUpdate();
		}
	}
*****************************/
}

void Game::updateAllPlayers()
{
/**********************************
	for( unsigned int indx = 0; indx < mNetwork->player_list.size(); indx++ )
	{
		if( mNetwork->player_list[indx]->name.compare( "__CLIENT__" ) == 0 )
		{
		}
		else
		{
			bool flg = false;

			for( unsigned int i = 0; i < player_list.size(); i++ )
			{
				if( mNetwork->player_list[indx]->name.compare( player_list[i]->mName ) == 0 )
				{
					flg = true;

					// Eventually this will be redone to support delta compression.
					player_list[i]->updated = true;
					player_list[i]->setPosition( mNetwork->player_list[indx]->position );
					player_list[i]->setOrientation( mNetwork->player_list[indx]->orientation );
					player_list[i]->in_region = true;
				}
			}

			if( !flg )
			{
				SelfPlayerCharacter *player = new SelfPlayerCharacter( mNetwork->player_list[indx]->name );
				player_list.push_back( player );

				player->updated = false;
				player->setPosition( mNetwork->player_list[indx]->position );
				player->setOrientation( mNetwork->player_list[indx]->orientation );
				player->in_region = true;
			}
		}

		for( unsigned int indx = 0; indx < player_list.size(); indx++ )
		{
			if( !player_list[indx]->in_region )
			{
				delete player_list[indx];
				player_list.erase( player_list.begin() + indx );
			}
		}
	}
*********************************************/
}



void Game::rotatePlayerCam_Y(double direction)
{
	cam_rotate_direction_y = direction * 0.5;
}

void Game::rotatePlayerCam_X(double direction)
{
	cam_rotate_direction_x = direction * 0.5;
}

void Game::rotatePlayer_Y(double direction)
{
	rotate_direction_y = direction * 0.5;
}

void Game::setPlayerOrientation(Ogre::Quaternion qt)
{
	mSelfPlayerCharacter->setOrientation(qt);
}

Ogre::Quaternion Game::getPlayerCameraOrientation()
{
	return mSelfPlayerCharacter->getCameraOrientation();
}


SelfPlayerCharacter* Game::getPlayer()
{
	return mSelfPlayerCharacter;
}

SelfPlayerCharacter* Game::getPlayer(std::string name)
{
	for( unsigned int indx = 0; indx < player_list.size(); indx++ )
	{
		if( player_list[indx]->mName == name )
		{
			return player_list[indx];
		}
	}
	return 0;
}


ZoneLoader* Game::getZoneLoader()
{
    return mZoneLoader;
}


void Game::receive_say_chat( std::string message, std::string player )
{
	/************
	InGameUI* const inGameUI = GUI::getSingletonPtr()->getInGameUI();

	if (inGameUI)
	{
		inGameUI->receive_say_chat(message, player);
	}
	*************/
}

NonePlayerCharacter* Game::addNPC(std::string name, std::string model)
{
	NonePlayerCharacter *existing = 0;
	for( unsigned int i = 0; i < npc_list.size(); i++ )
	{
		if( npc_list[i]->getName() == name )
			existing = npc_list[i];
	}

	if( existing == 0 )
	{
		NonePlayerCharacter *npc = new NonePlayerCharacter( name, model );
		npc_list.push_back( npc );
		return npc;
	}

	return existing;
}

void Game::removeNPC(std::string name)
{
	for( unsigned int i = 0; i < npc_list.size(); i++ )
	{
		if( npc_list[i]->getName() == name )
		{
			delete npc_list[i];
			npc_list.erase( npc_list.begin() + i );
			break;
		}
	}
}

NonePlayerCharacter* Game::getNPC(std::string name)
{
	for( unsigned int i = 0; i < npc_list.size(); i++ )
	{
		if( npc_list[i]->getName() == name )
		{
			return npc_list[i];
		}
	}

	return 0;
}



void Game::destroyAllEntitiesAndSceneNodes()
{
	Game::destroyAllEntities();
	Game::destroyAllSceneNodes();
}

void Game::destroyAllEntities()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::SceneNode *rootNode = sceneManager->getRootSceneNode();

	Game::_destroyAllEntities(rootNode);
}

void Game::_destroyAllEntities(Ogre::Node *n)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::Entity* ent;
	Ogre::MovableObject *m;

	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	std::string tmp;

	while(object_it.hasMoreElements())
	{
		m = object_it.getNext();

		// Check if the type is a entity and if it is, then delete it;
		if( m->getMovableType() == "Entity" )
		{
			tmp = m->getName();

			// Our entities start with "ent_" that way we know what is what.
			// Only delete the entities that start with "ent_";
			if ( tmp.compare(0,4,"ent_") == 0 )
			{
				ent = sceneManager->getEntity( m->getName() );

				m->getParentSceneNode()->detachObject( m->getName() );
				sceneManager->destroyEntity( ent );
			}
		}
	}
	while(node_it.hasMoreElements())
	{
		Game::_destroyAllEntities( node_it.getNext() );
	}
}


void Game::destroyAllSceneNodes()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::SceneNode *rootNode = sceneManager->getRootSceneNode();

	Game::_destroyAllSceneNodes(rootNode);
}

void Game::_destroyAllSceneNodes(Ogre::SceneNode *n)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();
	std::string tmp;

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		SceneNode* child = (SceneNode*) node_it.getNext();
		tmp = child->getName();

		// Our Scenenodes start with "sn_" that way we know what is what.
		// Only delete the scenenodes that start with "sn_";
		if ( tmp.compare(0,3,"sn_") == 0 )
		{
			sceneManager->getRootSceneNode()->removeAndDestroyChild(tmp);
		}
	}
}
