/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
 
#ifndef __GAME_H__
#define __GAME_H__
 
#include <Ogre.h>
#include <CEGUI/CEGUI.h>
#include "InputManager.h"
#include "InsideGameGui.h"
#include "MovementDirection.h"

class Network;
class ZoneLoader;
class Player;
class SelfPlayerCharacter;
class NonePlayerCharacter;
class InsideGameGui;


class Game
{
private:
	Network* mNetwork;
	ZoneLoader* mZoneLoader;
	SelfPlayerCharacter* mSelfPlayerCharacter;
	
	InputKeyCollector* mInputKeyCollector;
	InputMouseCollector* mInputMouseCollector;

	InsideGameGui* mInsideGameGui;

	double cam_rotate_direction_y;
	double cam_rotate_direction_x;
	double rotate_direction_y;

	std::vector<SelfPlayerCharacter *> player_list;
	std::vector<NonePlayerCharacter *> npc_list;

	Ogre::Real mPrevFrameTime;

	int unique_num;

	bool x__;
	Ogre::Timer *timer;

	void destroyAllEntities();
	void _destroyAllEntities(Ogre::Node *n);
	void destroyAllSceneNodes();
	void _destroyAllSceneNodes(Ogre::SceneNode *n);
public:
	Game();
	~Game();
	
	// Called each frame //
	void frameStarted(Ogre::Real timeSinceLastFrame);
	void frameEnded(Ogre::Real timeSinceLastFrame);

	//  Updates all of the nearby player's information //
	void updateAllPlayers();
	void fullUpdateAllPlayers();

	// Destroys all the sceneNodes and entities //
	void destroyAllEntitiesAndSceneNodes();

	void rotatePlayerCam_Y( double direction );
	void rotatePlayerCam_X( double direction );
	void rotatePlayer_Y( double direction );
	void setPlayerOrientation( Ogre::Quaternion qt );
	void setCameraDistanceFromTarget( double dist );

	Ogre::Quaternion getPlayerCameraOrientation();

	SelfPlayerCharacter* getPlayer();
	SelfPlayerCharacter *getPlayer( std::string name );
	ZoneLoader *getZoneLoader();
	SelfPlayerCharacter* const getPlayer() const;
	ZoneLoader* const getZoneLoader() const;

	void receive_say_chat( std::string message, std::string player );

	NonePlayerCharacter* addNPC( std::string name, std::string model );
	void removeNPC(std::string name);
	NonePlayerCharacter* getNPC(std::string name); // Get NPC by name.
};

#endif 
