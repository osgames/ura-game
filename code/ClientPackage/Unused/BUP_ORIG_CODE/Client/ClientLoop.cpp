/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ClientLoop.h"

#include "ClientStateManager.h"
#include "Ogre3dManager.h"
#include "CeguiManager.h"
#include "InputManager.h"

 
ClientLoop::ClientLoop()
{
	////////////////////////////////////////////////////
	/// For now we let the FrameListener do the work ///
	////////////////////////////////////////////////////
	Ogre3dManager::getSingletonPtr()->addFrameListener(this);
	Ogre3dManager::getSingletonPtr()->startRendering();
}



bool ClientLoop::frameStarted(const Ogre::FrameEvent& evt)
{
	/// Update the input ///
	InputManager::getSingletonPtr()->update(evt.timeSinceLastFrame);
	 
	/// Update Game: WARNING THE GAME WILL CRASH IF WE DON'T GET THE POINTER HERE ///
	if ( ClientStateManager::getSingletonPtr()->getGame() )
	{
		ClientStateManager::getSingletonPtr()->getGame()
			->frameStarted(evt.timeSinceLastFrame);
	}
	
	return true;
}

bool ClientLoop::frameEnded(const Ogre::FrameEvent& evt)
{
	if ( ClientStateManager::getSingletonPtr()->getGame() )
	{
		ClientStateManager::getSingletonPtr()->getGame()
			->frameEnded(evt.timeSinceLastFrame);
	}

	return true;
}