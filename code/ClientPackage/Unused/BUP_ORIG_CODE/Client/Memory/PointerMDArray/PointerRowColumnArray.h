/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/ 


#ifndef __POINTERROWCOLUMNARRAY_H__
#define __POINTERROWCOLUMNARRAY_H__


#include "PointerMDxArray.h"

    
//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define POINTERROWCOLUMNARRAY_DEFAULT_PAGE 0
 
     

//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class PointerRowColumnArray : protected PointerMDxArray<T>
{
public:
	PointerRowColumnArray(unsigned rowCapacity, unsigned columnCapacity, bool autoAllocate)
		: PointerMDxArray<T>(1, rowCapacity, columnCapacity, autoAllocate)
	{
		///////////////////////////////////////////////////////////////////////
		/// If We don't auto allocate than we allocate a page and row 		///
		/// This allows us to save memory, if we just want to have a big -	///
		///  unallocated container.											///
		///////////////////////////////////////////////////////////////////////
		if (!autoAllocate)
		{
			PointerMDxArray<T>::addPage();
		}
	}


	void addRow()
	{
		PointerMDxArray<T>::addRow(POINTERROWCOLUMNARRAY_DEFAULT_PAGE);
	}

	void addColumn(unsigned row, T* type)
	{
		PointerMDxArray<T>::addColumn(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row, type);
	}


	//////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns ///
	//////////////////////////////////////////////////////
	T* getColumnPointer(unsigned row, unsigned column) const
	{
		return PointerMDxArray<T>::getColumnPointer(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row, column);
	}

	T getColumnValue(unsigned row, unsigned column) const
	{
		return PointerMDxArray<T>::getColumnValue(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row, column);
	}

	unsigned getColumnCapacity(unsigned row) const
	{
		return PointerMDxArray<T>::getColumnCapacity(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row);
	}

	unsigned getColumnAllocated(unsigned row) const
	{
		return PointerMDxArray<T>::getColumnAllocated(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row);
	}

	unsigned getColumnCutOff(unsigned row) const
	{
		return PointerMDxArray<T>::getColumnCutOff(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row);
	}


	///////////////////////////////////////////////////
	/// GET: Functions that deal with just the Rows ///
	///////////////////////////////////////////////////
	unsigned getRowCapacity() const
	{
		return PointerMDxArray<T>::getRowCapacity(POINTERROWCOLUMNARRAY_DEFAULT_PAGE);
	}

	unsigned getRowAllocated() const
	{
		return PointerMDxArray<T>::getRowAllocated(POINTERROWCOLUMNARRAY_DEFAULT_PAGE);
	}

	unsigned getRowCutOff() const
	{
		return PointerMDxArray<T>::getRowCutOff(POINTERROWCOLUMNARRAY_DEFAULT_PAGE);
	}


	//////////////////////////////////////////////////////
	/// SET: Functions that deal with just the Columns ///
	//////////////////////////////////////////////////////
	void setColumnPointer(unsigned row, unsigned column, T* type)
	{
		PointerMDxArray<T>::setColumnPointer(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row, column, type);
	}
	 
	void setColumnValue(unsigned row, unsigned column, T type)
	{
		PointerMDxArray<T>::setColumnValue(POINTERROWCOLUMNARRAY_DEFAULT_PAGE, row, column, type);
	}
};


#endif
