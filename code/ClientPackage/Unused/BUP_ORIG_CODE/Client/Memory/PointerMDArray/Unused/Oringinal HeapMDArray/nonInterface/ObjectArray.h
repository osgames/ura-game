/*
Trinity Reign, an open source MMORPG.
Copyright (C)  Trinity Reign Dev Team
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/ 


#ifndef OBJECTARRAY_H
#define OBJECTARRAY_H


#include <stdlib.h>
#include "GlobalHeader.h"


////////////////////////////////////////////
/// Uncomment this to take off debugging ///
////////////////////////////////////////////
#define OBJECTARRAY_DEBUG


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define OBJECTARRAY_INDEXSIZE			6

#define OBJECTARRAY_INDEX_CAPACITY		0
#define OBJECTARRAY_INDEX_ALLOCATED		1
#define OBJECTARRAY_INDEX_CUTOFF		2

#define OBJECTARRAY_INDEX_TMPLOOP		3
#define OBJECTARRAY_INDEX_TEMPINT0		4
#define OBJECTARRAY_INDEX_TEMPINT1		5



///////////////////////////////////////////////////////////////
/// WE NEED DIFFERENT CODE FOR C STRINGS AND BUILT IN TYPES	///
/// THIS DEFINE ALLOWS US TO SWITCH AND COPY AND PASTE		///
///////////////////////////////////////////////////////////////
/////#define OBJECTARRAY_CSTRING_CODE

///////////////////////////////////////////////////////////
/// WE NEED DIFFERENT CODE FOR BUILT IN TYPES. 			///
/// THIS DEFINE ALLOWS US TO SWITCH AND COPY AND PASTE 	///
///////////////////////////////////////////////////////////
//////#define OBJECTARRAY_BUILTINTYPE_CODE



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <class T>
class ObjectArray
{
public:
	T** mpub_Array;
	unsigned* mVariableIndex;


	ObjectArray(unsigned capacity = 1, bool autoAllocate = false)
	{
		LOGI("###### ObjectArray::ObjectArray(unsigned capacity = %i, bool autoAllocate = false)", capacity);
		ObjectArray::_ConstructorAllocate(capacity, autoAllocate);
	}

	~ObjectArray()
	{
		mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;

		LOGI("@@@@@@@@@@@@@@@@@@@@@ ~ObjectArray()");

		if (mpub_Array)
		{
			for (; mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < mVariableIndex[OBJECTARRAY_INDEX_ALLOCATED];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				if ( mpub_Array[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]] )
				{
					LOGI("delete mpub_Array[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]];");

					delete mpub_Array[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]];
					mpub_Array[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]] = NULL;
				}
			}

			delete [] mpub_Array;
			mpub_Array = NULL;
		}


		/// Free the mVariableIndex array ///
		if (mVariableIndex)
		{
			delete [] mVariableIndex;
			mVariableIndex = NULL;
		}
	}



	void _ConstructorAllocate(unsigned capacity, bool autoAllocate)
	{
		mpub_Array = new T*[capacity];
		mVariableIndex = new unsigned[OBJECTARRAY_INDEXSIZE];


		/// Set the Array to its capacity and to zero to prevent errors ///
		mVariableIndex[OBJECTARRAY_INDEX_CAPACITY]		= capacity;
		mVariableIndex[OBJECTARRAY_INDEX_ALLOCATED] 	= 0;
		mVariableIndex[OBJECTARRAY_INDEX_CUTOFF] 		= 0;
		mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] 		= 0;
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] 		= 0;
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] 		= 0;


		/// Do We auto allocate the memory? ///
		if (autoAllocate)
		{
			for (; mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < capacity;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				ObjectArray::add( new T() );
			}

			///////////////////////////////////////////////////////////////////
			/// After we add the data, we set the new allocation and cutoff ///
			///////////////////////////////////////////////////////////////////
			mVariableIndex[OBJECTARRAY_INDEX_CUTOFF] 	= capacity;
		}
	}



	void add(T* type)
	{
		//////////////////////////////////////////////////////////////////////
		/// If the array length >= to the capacity than we need to resize  ///
		//////////////////////////////////////////////////////////////////////
		if ( mVariableIndex[OBJECTARRAY_INDEX_ALLOCATED] >=
				mVariableIndex[OBJECTARRAY_INDEX_CAPACITY] )
		{
			LOGI("#@#@#@#@#@#@#@#@#@#@@##@#@# ObjectArray array size increased");

			/// Allocate twice the size of the capacity ///
			T** p = new T*[mVariableIndex[OBJECTARRAY_INDEX_CAPACITY]*2];
LOGI("#@#@#@#@ ObjectArray 1");
			/// Copy the contents of the old array to the new bigger array ///
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < mVariableIndex[OBJECTARRAY_INDEX_CAPACITY];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				p[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]] = mpub_Array[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]];
			}
LOGI("#@#@#@#@ ObjectArray 2");
			/// Set the capacity to the now doubled size ///
			mVariableIndex[OBJECTARRAY_INDEX_CAPACITY] *= 2;

LOGI("#@#@#@#@ ObjectArray 3");
			/// Free the old arrays memory ///
			delete [] mpub_Array;

			/// Point the old array to the new bigger arrays memory address ///
			mpub_Array = p;
LOGI("#@#@#@#@ ObjectArray 4");
		}


		/// Insert the data into the array ///
		mpub_Array[ mVariableIndex[OBJECTARRAY_INDEX_ALLOCATED] ] = type;
LOGI("#@#@#@#@ ObjectArray 5");
		/// Increment the array length since we inserted something in it ///
		mVariableIndex[OBJECTARRAY_INDEX_ALLOCATED]++;
LOGI("#@#@#@#@ ObjectArray 6");
	}



	void setCutOff(unsigned size)
	{
		mVariableIndex[OBJECTARRAY_INDEX_CUTOFF] = size;
	}


	void setPointer(unsigned column, T* type)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef OBJECTARRAY_DEBUG
			/// Check if were out of column bounds ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: ObjectArray setPointer(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Add the new column ///
		mpub_Array[column] = type;
	}


	inline unsigned getCapacity() const
	{
		//LOGI("###### capacity = %i",  mVariableIndex[OBJECTARRAY_INDEX_CAPACITY]);
		return mVariableIndex[OBJECTARRAY_INDEX_CAPACITY];
	}

	inline unsigned getAllocated() const
	{
		return mVariableIndex[OBJECTARRAY_INDEX_ALLOCATED];
	}

	inline unsigned getCutOff() const
	{
		//LOGI("###### ROWCUTTOFF = %i",  mVariableIndex[OBJECTARRAY_INDEX_CUTOFF]);
		return mVariableIndex[OBJECTARRAY_INDEX_CUTOFF];
	}


	T* getArrayPointer()
	{
		return *mpub_Array;
	}

	T* getPointer(unsigned column)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef OBJECTARRAY_DEBUG
			/// Check if were out of column bounds ///
			// FIXME:
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: ObjectArray getByPointer(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Get the columns pointer ///
		return mpub_Array[column];
	}

	T getValue(unsigned column)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef OBJECTARRAY_DEBUG
			/// Check if were out of column bounds ///
			/// FIXME: when the type is char, it wont compile ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: ObjectArray getByValue(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Get the Columns Value ///
		return *(mpub_Array[column]);
	}

	void setValue(unsigned column, T type)
	{
		/// Debug check: to see if were out of column bounds ///
		#ifdef OBJECTARRAY_DEBUG
			/// Check if were out of column bounds ///
			/// FIXME: when the type is char, it wont compile ///
			//if ( column < 0 || column >= mpub_Array[column]->getSize() )
			//{
			//	LOGI("ERROR: ObjectArray set(...): Column Out of bounds\n");
			//	return;
			//}
		#endif

		/// Add the new column ///
		*(mpub_Array[column]) = type;
	}



///#################################################///
///### THIS CODE IS ONLY USED FOR BUILT IN TYPES ###///
///#################################################///
#ifdef OBJECTARRAY_BUILTINTYPE_CODE


	ObjectArray(const T* type)
	{
		LOGI("###### ObjectArray::ObjectArray(const T* type)");

		// Allocate the memory and assign it //
		ObjectArray::_ConstructorAllocate(type);
	}

	ObjectArray(unsigned capacity, T** type)
	{
		LOGI("###### ObjectArray::ObjectArray(unsigned capacity = %i, T** type)", capacity);

		// Allocate the memory and assign it by its size //
		ObjectArray::_ConstructorAllocate(capacity, true);
		ObjectArray::assign(capacity, type);
	}

	ObjectArray(unsigned capacity, const T type[])
	{
		LOGI("###### ObjectArray::ObjectArray(unsigned capacity = %i, const T type[])", capacity);

		// Allocate the memory and assign it by its size //
		ObjectArray::_ConstructorAllocate(capacity, true);
		ObjectArray::assign(capacity, type);
	}


	void clear()
	{
		mVariableIndex[OBJECTARRAY_INDEX_CUTOFF] = 0;
	}


	///////////////////////
	///	Assign the data ///
	///////////////////////
	void assign(unsigned arraySize, const T type[])
	{
LOGI("void ObjectArray::ASSIGN(unsigned arraySize, const T type[]");

		/// Store the current strings current Allocation, to know which pointers can change their values ///
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] = ObjectArray::getAllocated();


LOGI("arraySize = %i", arraySize);
LOGI("getAllocated = %i", ObjectArray::getAllocated());


		/// If the original type size is bigger or equal to the one we want to copy than
		///  everything is fine. Else we have to copy until our old type size is taken up and then
		///  add to it.
		if (mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] >= arraySize)
		{
LOGI("A1");
LOGI("arraySize = %i", arraySize);

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("LOOP");
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]]);
			}
LOGI("A2");
		}
		else	// Our current string doesn't have enough size;
		{
LOGI("B1");
			// Set the current string to the cstring as much as possible using mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]]);
			}
LOGI("B2");
			// If we need to allocate memory, resume at mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] and end at arraySize //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				ObjectArray::add( new T(type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]]) );
			}
LOGI("B3");
		}

LOGI("C1");
		/// Set the cutOff point to the size of arraySize ///
		ObjectArray::setCutOff(arraySize);
	}


	void assign(unsigned arraySize, T** type)
	{
LOGI("void ObjectArray::ASSIGN(unsigned arraySize, T** type");

		/// Store the current strings current Allocation, to know which pointers can change their values ///
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] = ObjectArray::getAllocated();


LOGI("arraySize = %i", arraySize);
LOGI("getAllocated = %i", ObjectArray::getAllocated());


		/// If the original type size is bigger or equal to the one we want to copy than
		///  everything is fine. Else we have to copy until our old type size is taken up and then
		///  add to it.
		if (mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] >= arraySize)
		{
LOGI("A1");
LOGI("arraySize = %i", arraySize);

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("LOOP");
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], *type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]]);
			}
LOGI("A2");
		}
		else	// Our current string doesn't have enough size;
		{
LOGI("B1");
			// Set the current string to the cstring as much as possible using mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], *type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]]);
			}
LOGI("B2");
			// If we need to allocate memory, resume at mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] and end at arraySize //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				ObjectArray::add( new T(*type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]]) );
			}
LOGI("B3");
		}

LOGI("C1");
		/// Set the cutOff point to the size of arraySize ///
		ObjectArray::setCutOff(arraySize);
	}


	void appendToCutOff(unsigned arraySize, const T type[])
	{
		LOGI("void appendToCutOff(unsigned arraySize, const T type[])");


		/// We use this for the loops ///
		mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;

		/// Get allocated ///
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] = ObjectArray::getAllocated();

		/// Get the cutoff, make sure were not subtracting ///
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] = ObjectArray::getCutOff();


///################################################///
///### THIS CODE IS ONLY USED FOR CSTRING TYPES ###///
///################################################///
#ifdef OBJECTARRAY_CSTRING_CODE
		if (mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] > 0)
		{
			mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] = ObjectArray::getCutOff() - 1;
		}
#endif


		LOGI("cuttOFF = %i", ObjectArray::getCutOff() );
		LOGI("arraySize = %i", arraySize);

		/// The new size should be Cuttoff + attached array size ///
		arraySize += mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1];



		///########################################################################################///
		///### If the original strings size is bigger or equal to the one we want to copy than	###///
		///###  everything is fine. Else we have to copy until our old string size is taken up	###///
		///###	and then add to it.																###///
		///########################################################################################///
		if (mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] >= arraySize)
		{
			LOGI("###### ENOUGH MEMORY");

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setvalue");
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] - mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1]]);
			}
		}
		else	// Our current string doesn't have enough size;
		{
			LOGI("###### NOT ENOUGH MEMORY");

			///////////////////////////////////////////////////////////////
			/// For the space we already have allocated, set the values ///
			///////////////////////////////////////////////////////////////
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setValue");
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] - mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1]]);
			}


			//////////////////////////////////////////////////////////////////////////////////////
			///	We Don't have enough space to set the values, so we allocate more and set them ///
			//////////////////////////////////////////////////////////////////////////////////////
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("add");
				ObjectArray::add(new T(type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] - mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1]]));
			}
		}


		/////////////////////////////
		/// Set the CuttOff point ///
		/////////////////////////////
		ObjectArray::setCutOff(arraySize);
	}

	void appendToCutOff(unsigned arraySize, T type[])
	{
		LOGI("void appendToCutOff(unsigned arraySize, const T type[])");


		/// We use this for the loops ///
		mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = 0;

		/// Get allocated ///
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] = ObjectArray::getAllocated();

		/// Get the cutoff, make sure were not subtracting ///
		mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] = ObjectArray::getCutOff();


///################################################///
///### THIS CODE IS ONLY USED FOR CSTRING TYPES ###///
///################################################///
#ifdef OBJECTARRAY_CSTRING_CODE
		if (mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] > 0)
		{
			mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1] = ObjectArray::getCutOff() - 1;
		}
#endif


		LOGI("cuttOFF = %i", ObjectArray::getCutOff() );
		LOGI("arraySize = %i", arraySize);

		/// The new size should be Cuttoff + attached array size ///
		arraySize += mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1];



		///########################################################################################///
		///### If the original strings size is bigger or equal to the one we want to copy than	###///
		///###  everything is fine. Else we have to copy until our old string size is taken up	###///
		///###	and then add to it.																###///
		///########################################################################################///
		if (mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0] >= arraySize)
		{
			LOGI("###### ENOUGH MEMORY");

			// Set the current string to the cstring fully using cstrings size //
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setvalue");
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] - mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1]]);
			}
		}
		else	// Our current string doesn't have enough size;
		{
			LOGI("###### NOT ENOUGH MEMORY");

			///////////////////////////////////////////////////////////////
			/// For the space we already have allocated, set the values ///
			///////////////////////////////////////////////////////////////
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("setValue");
				ObjectArray::setValue(mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP], type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] - mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1]]);
			}


			//////////////////////////////////////////////////////////////////////////////////////
			///	We Don't have enough space to set the values, so we allocate more and set them ///
			//////////////////////////////////////////////////////////////////////////////////////
			for (mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] = mVariableIndex[OBJECTARRAY_INDEX_TEMPINT0];
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] < arraySize;
					mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP]++)
			{
				LOGI("add");
				ObjectArray::add(new T(type[mVariableIndex[OBJECTARRAY_INDEX_TMPLOOP] - mVariableIndex[OBJECTARRAY_INDEX_TEMPINT1]]));
			}
		}


		/////////////////////////////
		/// Set the CuttOff point ///
		/////////////////////////////
		ObjectArray::setCutOff(arraySize);
	}


/// END OF BUILTINTYPE_CODE DEFINE ///
#endif
};



#endif




