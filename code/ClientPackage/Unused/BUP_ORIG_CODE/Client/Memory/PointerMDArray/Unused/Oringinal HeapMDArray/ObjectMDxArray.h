/*
Trinity Reign, an open source MMORPG.
Copyright (C)  Trinity Reign Dev Team
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/ 


#ifndef OBJECTMDXARRAY_H
#define OBJECTMDXARRAY_H


#include <stdlib.h>
#include "GlobalHeader.h"
#include "MemoryAllocator/nonInterface/ObjectArray.h"


////////////////////////////////////////////
/// Uncomment this to take off debugging ///
////////////////////////////////////////////
#define OBJECTMDXARRAY_DEBUG


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define OBJECTMDXARRAY_INDEXSIZE			2

#define OBJECTMDXARRAY_INDEX_ROWCAPACITY	0
#define OBJECTMDXARRAY_INDEX_COLUMNCAPACITY	1


//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class ObjectMDxArray
{
protected:
	unsigned* mVariableIndex;
public:
	// PAGE			ROW			COLUMN
	ObjectArray< ObjectArray< ObjectArray<T> > >* mpub_Array;


	ObjectMDxArray(	unsigned pageCapacity,
					unsigned rowCapacity,
					unsigned columnCapacity,
					bool autoAllocate)
	{
		/// Create the main Array ///
		mpub_Array = new ObjectArray< ObjectArray< ObjectArray<T> > >(pageCapacity, autoAllocate);


		/// Assign the VariableIndex ///
		mVariableIndex = new unsigned[OBJECTMDXARRAY_INDEXSIZE];
		mVariableIndex[OBJECTMDXARRAY_INDEX_ROWCAPACITY]	= rowCapacity;
		mVariableIndex[OBJECTMDXARRAY_INDEX_COLUMNCAPACITY]	= columnCapacity;


		/// If autoAllocate is true then add the rows and columns ///
		if (autoAllocate)
		{
			unsigned i = 0;
			unsigned j = 0;
			unsigned k = 0;

			///////////////////////////////////////////////////////////////////////
			/// Automatically allocated space for all the pages, rows, columns	///
			/// The pages are allocated above already							///
			///////////////////////////////////////////////////////////////////////
			for (; i < pageCapacity; i++)
			{
				/// Add the rows and columns ///
				for (j = 0; j < rowCapacity; j++)
				{
					addRow(i);

					/// Add the columns ///
					for (k = 0; k < columnCapacity; k++)
					{
						addColumn(i, j);
					}
				}
			}
		}
	}

	~ObjectMDxArray()
	{
		if (mpub_Array)
		{
			delete mpub_Array;
			mpub_Array = NULL;
		}

		if (mVariableIndex)
		{
			delete [] mVariableIndex;
			mVariableIndex = NULL;
		}
	}



	////////////////
	/// Add Page ///
	////////////////
	void addPage()
	{
		mpub_Array->add( new ObjectArray< ObjectArray<T> >(mVariableIndex[OBJECTMDXARRAY_INDEX_ROWCAPACITY]) );
	}

	///////////////
	/// Add Row ///
	///////////////
	void addRow(unsigned page)
	{
		/// Debug check: to see if were out of page bounds ///
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{ return; }
		#endif

		mpub_Array->mpub_Array[page]->add( new ObjectArray<T>(mVariableIndex[OBJECTMDXARRAY_INDEX_COLUMNCAPACITY]) );
	}

	//////////////////
	/// Add column ///
	//////////////////
	void addColumn(unsigned page, unsigned row)
	{
		/// Debug check: to see if were out of page, row bounds ///
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowInBounds(page, row) )
			{ return; }
		#endif

		/// Add the column to the row ///
		mpub_Array->mpub_Array[page]->mpub_Array[row]->add( new T() );
	}

	void addColumn(unsigned page, unsigned row, T* type)
	{
		/// Debug check: to see if were out of page, row bounds ///
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowInBounds(page, row) )
			{ return; }
		#endif

		/// Add the column to the row ///
		mpub_Array->mpub_Array[page]->mpub_Array[row]->add(type);
	}



	////////////////////////////////////////
	/// Get The column pointer and value ///
	////////////////////////////////////////
	T* getColumnPointer(unsigned page, unsigned row, unsigned column)
	{
		/// Debug check: to see if were out of Row and column bounds ///
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowColumnInBounds(page, row, column) )
			{ return NULL; }
		#endif

		return mpub_Array->mpub_Array[page]->mpub_Array[row]->getPointer(column);
	}


	T getColumnValue(unsigned page, unsigned row, unsigned column)
	{
		/// Debug check: to see if were out of Row and column bounds ///
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowColumnInBounds(page, row, column) )
			{ return NULL; }
		#endif

		return mpub_Array->mpub_Array[page]->mpub_Array[row]->getValue(column);
	}




	/////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Page memory itself ///
	/////////////////////////////////////////////////////////////////
	unsigned getPageCapacity() const
	{
		return mpub_Array->getCapacity();
	}

	unsigned getPageAllocated() const
	{
		return mpub_Array->getAllocated();
	}

	unsigned getPageCutOff() const
	{
		return mpub_Array->getCutOff();
	}


	/////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Rows memory itself ///
	/////////////////////////////////////////////////////////////////
	unsigned getRowCapacity(unsigned page) const
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{ return 0; }
		#endif

		return mpub_Array->mpub_Array[page]->getCapacity();
	}

	unsigned getRowAllocated(unsigned page) const
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{ return 0; }
		#endif

		return mpub_Array->mpub_Array[page]->getAllocated();
	}

	unsigned getRowCutOff(unsigned page) const
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{ return 0; }
		#endif

		return mpub_Array->mpub_Array[page]->getCutOff();
	}


	////////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns memory itself ///
	////////////////////////////////////////////////////////////////////
	unsigned getColumnCapacity(unsigned page, unsigned row)
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowInBounds(page, row) )
			{ return 0; }
		#endif

		return mpub_Array->mpub_Array[page]->mpub_Array[row]->getCapacity();
	}

	unsigned getColumnAllocated(unsigned page, unsigned row)
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowInBounds(page, row) )
			{ return 0; }
		#endif

		return mpub_Array->mpub_Array[page]->mpub_Array[row]->getAllocated();
	}

	unsigned getColumnCutOff(unsigned page, unsigned row) const
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowInBounds(page, row) )
			{ return 0; }
		#endif

		return mpub_Array->mpub_Array[page]->mpub_Array[row]->getCutOff();
	}



	//////////////////////////////////////////////////////////
	/// Set: Functions for the page, row and column cutOff ///
	//////////////////////////////////////////////////////////
	void setPageCutOff(unsigned size) const
	{
		/// Set the page cutoff ///
		mpub_Array->setCutOff(size);
	}

	void setRowCutOff(unsigned page, unsigned size) const
	{
		#ifdef OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{ return; }
		#endif

		mpub_Array->mpub_Array[page]->setCutOff(size);
	}

	void setColumnCutOff(unsigned page, unsigned row, unsigned size) const
	{
		#ifdef  OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowInBounds(page, row) )
			{ return; }
		#endif

		/// Set the row cutoff ///
		mpub_Array->mpub_Array[page]->mpub_Array[row]->setCutOff(size);
	}



	//////////////////////////
	/// Set Column Pointer ///
	//////////////////////////
	void setColumnPointer(unsigned page, unsigned row, unsigned column, T* type)
	{
		#ifdef  OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowColumnInBounds(page, row, column) )
			{ return; }
		#endif

		mpub_Array->mpub_Array[page]->mpub_Array[row]->setPointer(column, type);
	}


	////////////////////////
	/// Set Column Value ///
	////////////////////////
	void setColumnValue(unsigned page, unsigned row, unsigned column, T type)
	{
		#ifdef  OBJECTMDXARRAY_DEBUG
			if ( !ObjectMDxArray::_pageRowColumnInBounds(page, row, column) )
			{ return; }
		#endif

		mpub_Array->mpub_Array[page]->mpub_Array[row]->setValue(column, type);
	}


	///########################################///
	///### DEBUG CHECK FUNCTIONS FOR BOUNDS ###///
	///########################################///
	#ifdef OBJECTMDXARRAY_DEBUG
		inline bool _pageInBounds(unsigned page)
		{
			if ( page < 0 || page >= mpub_Array->getAllocated() )
			{
				LOGI("!!!!!!!! ERROR: ObjectMDxArray _pageInBounds(...): page Out of bounds");
				return false;
			}

			return true;
		}

		inline bool _rowInBounds(unsigned page, unsigned row)
		{
			if ( row < 0 || row >= mpub_Array->mpub_Array[page]->getAllocated() )
			{
				LOGI("!!!!!!!! ERROR: ObjectMDxArray _rowInBounds(...): row Out of bounds");
				return false;
			}

			return true;
		}

		inline bool _columnInBounds(unsigned page, unsigned row, unsigned column)
		{
			if ( column < 0 || column >= mpub_Array->mpub_Array[page]->mpub_Array[row]->getAllocated() )
			{
				LOGI("!!!!!!!! ERROR: ObjectMDxArray _columnInBounds(...): column Out of bounds");
				return false;
			}

			return true;
		}

		inline bool _pageRowInBounds(unsigned page, unsigned row)
		{
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{	return false;	}

			if ( !ObjectMDxArray::_rowInBounds(page, row) )
			{	return false;	}

			return true;
		}

		inline bool _pageRowColumnInBounds(unsigned page, unsigned row, unsigned column)
		{
			if ( !ObjectMDxArray::_pageInBounds(page) )
			{	return false;	}

			if ( !ObjectMDxArray::_rowInBounds(page, row) )
			{	return false;	}

			if ( !ObjectMDxArray::_columnInBounds(page, row, column) )
			{	return false;	}

			return true;
		}
	#endif
};


#endif
