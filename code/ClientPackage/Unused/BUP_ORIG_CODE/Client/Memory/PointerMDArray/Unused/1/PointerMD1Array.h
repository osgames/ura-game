/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __POINTERMD1ARRAY_H__
#define __POINTERMD1ARRAY_H__


#include "PointerMDxArray.h"


//////////////////////
/// VARIABLE INDEX ///
//////////////////////
#define POINTERMD1ARRAY_DEFAULT_PAGE	0
#define POINTERMD1ARRAY_DEFAULT_ROW		0



//////////////////////
/// THE MAIN CLASS ///
//////////////////////
template <typename T>
class PointerMD1Array : protected PointerMDxArray<T>
{
public:
	PointerMD1Array(unsigned columnCapacity, bool autoAllocate)
		: PointerMDxArray<T>(1, 1, columnCapacity, autoAllocate)
	{
		///////////////////////////////////////////////////////////////////////
		/// If We don't auto allocate than we allocate a page and row 		///
		/// This allows us to save memory, if we just want to have a big -	///
		///  unallocated container.											///
		///////////////////////////////////////////////////////////////////////
		if (!autoAllocate)
		{
			PointerMDxArray<T>::addPage();
			PointerMDxArray<T>::addRow(POINTERMD1ARRAY_DEFAULT_PAGE);
		}
	}


	void addColumn(T* type)
	{
		PointerMDxArray<T>::addColumn(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW, type);
	}


	////////////////////////////////////////
	/// Get the column pointer and value ///
	////////////////////////////////////////
	T* getColumnPointer(unsigned column)
	{
		return PointerMDxArray<T>::getColumnPointer(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW, column);
	}


	T getColumnValue(unsigned column)
	{
		return PointerMDxArray<T>::getColumnValue(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW, column);
	}


	////////////////////////////////////////////////////////////////////
	/// GET: Functions that deal with just the Columns memory itself ///
	////////////////////////////////////////////////////////////////////
	unsigned getColumnCapacity()
	{
		return  PointerMDxArray<T>::getColumnCapacity(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW);
	}

	unsigned getColumnAllocated()
	{
		return PointerMDxArray<T>::getColumnAllocated(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW);
	}

	unsigned getColumnCutOff()
	{
		return PointerMDxArray<T>::getColumnCutOff(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW);
	}


	//////////////////////////
	/// Set Column Pointer ///
	//////////////////////////
	void setColumnPointer(T* type)
	{
		PointerMDxArray<T>::setColumnPointer(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW, type);
	}

	////////////////////////
	/// Set Column Value ///
	////////////////////////
	void setColumnValue(T type)
	{
		PointerMDxArray<T>::setColumnValue(POINTERMD1ARRAY_DEFAULT_PAGE, POINTERMD1ARRAY_DEFAULT_ROW, type);
	}
};


#endif
