#ifndef HEAPPLAYERINTERACTIONWINDOW_H
#define HEAPPLAYERINTERACTIONWINDOW_H

#include <CEGUI.h>
#include "CNPC.h"

class HeapPlayerInteractionWindow;


class PlayerInteractionWindow
{
protected:
	CNPC* mNPC;

	CEGUI::Window* mPlayerInteractionWindow;
	CEGUI::Window* mImageTalk;
	CEGUI::Window* mImageBuy;
	CEGUI::Window* mImageLook;
	CEGUI::Window* mImageCancel;

	bool event_click_mImageTalk(const CEGUI::EventArgs &e);
	bool event_click_mImageCancel(const CEGUI::EventArgs &e);
public:
	PlayerInteractionWindow();
	~PlayerInteractionWindow();

	bool addChildWindow(CNPC* const npc);
	CEGUI::Window* const getWindow() const;
};


class HeapPlayerInteractionWindow
{
protected:
	PlayerInteractionWindow* mPlayerInteractionWindow;
public:
	HeapPlayerInteractionWindow();
	~HeapPlayerInteractionWindow();
	
	void toggle(CNPC* const npc);
	CEGUI::Window* const getWindow() const;
}; 


#endif







