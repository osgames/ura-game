/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InGameUIInventoryWindow.h"
#include "Globals.h"
#include "Ogre.h"

InGameUIInventoryWindow::InGameUIInventoryWindow() :
isConstructed(false), COLUMN_COUNT(4), ROW_COUNT(4)
{
}

InGameUIInventoryWindow::~InGameUIInventoryWindow()
{
}

void InGameUIInventoryWindow::toggle()
{
  if (!this->isConstructed) {
    this->addChildWindow();
    this->isConstructed = true;
  }
  else {
    if( mInventoryWindow->isVisible() == false ) {
	mInventoryWindow->show();
    } else if(mInventoryWindow->isVisible() == true) {
	mInventoryWindow->hide();
    }
  }
}

bool InGameUIInventoryWindow::isVisible()
{
  return mInventoryWindow->isVisible();
}

bool InGameUIInventoryWindow::addChildWindow()
{
  //return this->oldAddChildWindow();

  CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
  CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
  if(!bUIInventoryWindowCreated)
    CEGUI::ImagesetManager::getSingleton().createImageset("DriveIcons.imageset");

  // Chat window;
  // If statement is here to prevent the CEGUI log from showing exceptions on startup.
  if(bUIInventoryWindowCreated)
  {
      try{mInventoryWindow = windowManager.getWindow("InGameUI_InventoryWindow");}
      catch(CEGUI::Exception &e){mInventoryWindow = windowManager.createWindow("TaharezLook/FrameWindow", "InGameUI_InventoryWindow");}
  }
  else
  {
      mInventoryWindow = windowManager.createWindow("TaharezLook/FrameWindow", "InGameUI_InventoryWindow");
  }

  mInventoryWindow->setText("   Inventory Window");
  mInventoryWindow->setSize(CEGUI::UVector2(CEGUI::UDim(0, 50 * 4 + 10), CEGUI::UDim(0, 50 * 4 + 50)));
  mInventoryWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.65, 0) ) );
  // Show the window;
  mInventoryWindow->show();

  for(int i = 0; i < this->COLUMN_COUNT * this->ROW_COUNT; ++i) {
    CEGUI::Window* slot = this->createSlot(i);
    mInventoryWindow->addChildWindow(slot);
 //   if (i == 1) {
 //     //create the DragContainer that simplifies event handling.
 //     CEGUI::Window* dragContainer1 = 0;
 //     if(bUIInventoryWindowCreated)
 //     {
 //         try{dragContainer1 = windowManager.getWindow("Inventory/DragContainer1");}
 //         catch(CEGUI::Exception &e){dragContainer1 = windowManager.createWindow("DragContainer", "Inventory/DragContainer1");}
 //         dragContainer1->setArea(CEGUI::URect(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.05, 0),
 //                         CEGUI::UDim(0.95, 0), CEGUI::UDim(0.95, 0)));
 //     }
 //     else
 //     {
 //         dragContainer1 = windowManager.createWindow("DragContainer", "Inventory/DragContainer1");
 //         dragContainer1->setArea(CEGUI::URect(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.05, 0),
 //                         CEGUI::UDim(0.95, 0), CEGUI::UDim(0.95, 0)));
 //     }

 //       CEGUI::Window* item1 = 0;
 //       if(bUIInventoryWindowCreated)
 //       {
 //         try{item1 = windowManager.getWindow("item1");}
 //         catch(CEGUI::Exception &e){item1 = windowManager.createWindow("TaharezLook/StaticImage", "item1");}
 //       }
 //       else
 //       {
 //           item1 = windowManager.createWindow("TaharezLook/StaticImage", "item1");
 //       }
 //     item1->setArea(CEGUI::URect(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0),
	//			  CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
 //     item1->setProperty("Image", "set:DriveIcons image:Lime");
 //     item1->setMousePassThroughEnabled(true);
 //     dragContainer1->addChildWindow(item1);
 //     slot->addChildWindow(dragContainer1);
 //   }
	//else if (i == 7) {
 //     //create the DragContainer that simplifies event handling.
 //     CEGUI::Window* dragContainer7 = 0;
 //     if(bUIInventoryWindowCreated)
 //     {
 //         try{dragContainer7 = windowManager.getWindow("Inventory/DragContainer7");}
 //         catch(CEGUI::Exception &e){dragContainer7 = windowManager.createWindow("DragContainer", "Inventory/DragContainer7");}
 //     }
 //     else
 //     {
 //         dragContainer7 = windowManager.createWindow("DragContainer", "Inventory/DragContainer7");
 //     }

 //     dragContainer7->setArea(CEGUI::URect(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.05, 0),
	//				   CEGUI::UDim(0.95, 0), CEGUI::UDim(0.95, 0)));
 //     CEGUI::Window* item7 = 0;
 //     if(bUIInventoryWindowCreated)
 //     {
 //         try{item7 = windowManager.getWindow("item7");}
 //         catch(CEGUI::Exception &e){item7 = windowManager.createWindow("TaharezLook/StaticImage", "item7");}
 //     }
 //     else
 //     {
 //         item7 = windowManager.createWindow("TaharezLook/StaticImage", "item7");
 //     }
 //     item7->setArea(CEGUI::URect(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0),
	//			  CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
 //     item7->setProperty("Image", "set:DriveIcons image:GlobalDrive");
 //     item7->setMousePassThroughEnabled(true);
 //     dragContainer7->addChildWindow(item7);
 //     slot->addChildWindow(dragContainer7);
 //   }
    this->registerSlotEvent(slot);
  }



  this->subscribeEvents();

  // Add this window to the main window;
  sheet->addChildWindow( mInventoryWindow );

  bUIInventoryWindowCreated = true;

  return true;
}

//----------------------------------------------------------------------------//
bool InGameUIInventoryWindow::handleItemDropped(const CEGUI::EventArgs& args)
{
    // cast the args to the 'real' type so we can get access to extra fields
  std::cout << "Handling Item Dropped!!!!!" << std::endl;
  const CEGUI::DragDropEventArgs& dd_args =
    static_cast<const CEGUI::DragDropEventArgs&>(args);
std::cout << "Child Count: " << dd_args.window->getChildCount() << std::endl;

  if (dd_args.window->getChildCount() < 4) // this is a hack. For some reason there are autowindows attached to the slots.
    {
        // add dragdrop item as child of target if target has no item already
        dd_args.window->addChildWindow(dd_args.dragDropItem);
		Ogre::LogManager::getSingleton().logMessage("DD_ARGS: " +
			Ogre::String(dd_args.dragDropItem->getName().c_str())
		);
        // Now we must reset the item position from it's 'dropped' location,
        // since we're now a child of an entirely different window
        dd_args.dragDropItem->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05f, 0), CEGUI::UDim(0.05f, 0)));
    }

    return true;
}

bool InGameUIInventoryWindow::handleItemEntered(const CEGUI::EventArgs& args)
{
    // cast the args to the 'real' type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args =
    static_cast<const CEGUI::DragDropEventArgs&>(args);
  std::cout << "Entered Window: " << dd_args.window->getName() << std::endl;

    return true;
}

CEGUI::Window *InGameUIInventoryWindow::createSlot(int index)
{
  // Most of these slots are covered by the frame, so this is to
  // center them and make them fully visible. The values are emperical.
  const int LEFT_OFFSET = 8;
  const int TOP_OFFSET = 26;
  const double SUBWIDTH = 0.92;
  const double SUBHEIGHT = 0.86;

  CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
  //create the slot
  CEGUI::Window* slot = 0;
  if(!bUIInventoryWindowCreated)
  {
  slot = windowManager
    .createWindow("TaharezLook/StaticText",
		  "InGameUI_InventoryWindow/Slot" +
		  CEGUI::PropertyHelper::intToString(index));
  }
  else
  {
      slot = windowManager.getWindow(
        "InGameUI_InventoryWindow/Slot" +
        CEGUI::PropertyHelper::intToString(index));
  }

  //determine the dimensions in normalized coordinates
  double width = SUBWIDTH / this->COLUMN_COUNT;
  double height = SUBHEIGHT / this->ROW_COUNT;
  double xPos = (index % 4) * width;
  double yPos = (index / 4) * height;
  slot->setSize(CEGUI::UVector2(CEGUI::UDim(width, 0), CEGUI::UDim(height, 0)));
  slot->setPosition(CEGUI::UVector2(CEGUI::UDim(xPos, LEFT_OFFSET),
				    CEGUI::UDim(yPos, TOP_OFFSET)));
  return slot;
 };

 void InGameUIInventoryWindow::registerSlotEvent(CEGUI::Window* slot) {
   slot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped,
			CEGUI::Event::Subscriber(&InGameUIInventoryWindow::handleItemDropped, this));
   slot->subscribeEvent(CEGUI::Window::EventDragDropItemEnters,
			CEGUI::Event::Subscriber(&InGameUIInventoryWindow::handleItemEntered, this));
 }

 void InGameUIInventoryWindow::subscribeEvents()
 {
     using namespace CEGUI;

     WindowManager& wmgr = WindowManager::getSingleton();

     String base_name = "InGameUI_InventoryWindow/Slot";
     for (int i = 1; i <= 16; ++i)
     {
	 try
	 {
	     // get the window pointer for this slot
	     Window* wnd =
		 wmgr.getWindow(base_name + PropertyHelper::intToString(i));

	     // subscribe the handler.
	     wnd->subscribeEvent(
		 Window::EventDragDropItemDropped,
		 Event::Subscriber(&InGameUIInventoryWindow::handleItemDropped, this));
	 }
	 // if something goes wrong, log the issue but do not bomb!
	 catch(CEGUI::Exception& e)
	 {}
     }
 }

 bool InGameUIInventoryWindow::isSlotOpen( int slot )
 {
	 for( unsigned int i = 0; i < full_slots.size(); i++ )
	 {
		 if( full_slots[i] == slot )
		 {
			 return false;
		 }
	 }

	 return true;
 }

 std::string InGameUIInventoryWindow::int_to_str( int i )
 {
	 std::stringstream ss;
	 ss << i;
	 std::string s;
	 ss >> s;
	 return s;
 }

 bool InGameUIInventoryWindow::addItem( std::string image, int &slotnum )
 {
	 int slot_number = -1;
	 for( unsigned int i = 0; i < COLUMN_COUNT * ROW_COUNT ; i++ )
	 {
		 if( isSlotOpen( i ) )
		 {
			 slot_number = i;
			 break;
		 }
	 }

	 if( slot_number == -1 )
		 return false; // No room in the inventory window, unfortunately.

	 std::string snum = int_to_str( slot_number );

	 CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	 CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	 CEGUI::Window* slot = this->createSlot(slot_number);
	 mInventoryWindow->addChildWindow( slot );

	 //create the DragContainer that simplifies event handling.
      CEGUI::Window* dragContainer = 0;
      if(bUIInventoryWindowCreated)
      {
          try{dragContainer = windowManager.getWindow("Inventory/DragContainer" + snum);}
          catch(CEGUI::Exception &e){dragContainer = windowManager.createWindow("DragContainer", "Inventory/DragContainer" + snum);}
      }
      else
      {
          dragContainer = windowManager.createWindow("DragContainer", "Inventory/DragContainer" + snum);
      }

      dragContainer->setArea(CEGUI::URect(CEGUI::UDim(0.05, 0), CEGUI::UDim(0.05, 0),
					   CEGUI::UDim(0.95, 0), CEGUI::UDim(0.95, 0)));
      CEGUI::Window* item = 0;
      if(bUIInventoryWindowCreated)
      {
          try{item = windowManager.getWindow("item" + snum);}
          catch(CEGUI::Exception &e){item = windowManager.createWindow("TaharezLook/StaticImage", "item" + snum);}
      }
      else
      {
          item = windowManager.createWindow("TaharezLook/StaticImage", "item" + snum);
      }

      item->setArea(CEGUI::URect(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0),
				  CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
      item->setProperty("Image", "set:DriveIcons image:" + image);
      item->setMousePassThroughEnabled(true);
      dragContainer->addChildWindow(item);
      slot->addChildWindow(dragContainer);
	  full_slots.push_back( slot_number );
	  slotnum = slot_number;

	  return true;
 }

 bool InGameUIInventoryWindow::removeItem( int slotnum )
 {
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::Window* dragContainer = 0;
	if(bUIInventoryWindowCreated)
	{
		try{dragContainer = windowManager.getWindow("Inventory/DragContainer" + slotnum);}
		catch(CEGUI::Exception &e){return false;}
	}
	else
	{
		return false;
	}

	CEGUI::Window* item = 0;
	if(bUIInventoryWindowCreated)
	{
		try{item = windowManager.getWindow("item" + slotnum);}
		catch(CEGUI::Exception &e){return false;}
	}
	else
	{
		return false;
	}

	dragContainer->removeChildWindow( item );

	return true;
 }

