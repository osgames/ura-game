#ifndef PLAYERINTERACTIONWINDOW_H
#define PLAYERINTERACTIONWINDOW_H


#include <CEGUI.h>



class PlayerInteractionWindow
{
protected:
	CEGUI::Window* mPlayerInteractionWindow;
	CEGUI::Window* mImageTalk;
	CEGUI::Window* mImageBuy;
	CEGUI::Window* mImageLook;
	CEGUI::Window* mImageCancel;

	bool event_click_mImageTalk(const CEGUI::EventArgs &e);
	bool event_click_mImageCancel(const CEGUI::EventArgs &e);
public:
	PlayerInteractionWindow();
	~PlayerInteractionWindow();
};

#endif
