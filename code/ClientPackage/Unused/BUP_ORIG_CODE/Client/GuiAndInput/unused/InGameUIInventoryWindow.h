/*
Project Ura, an open source MMORPG.
Copyright (C) 2008  Denny Kunker

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef INGAMEUI_HEAP_INVENTORY_WINDOW_H
#define INGAMEUI_HEAP_INVENTORY_WINDOW_H


#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>



class InGameUIInventoryWindow
{
protected:
	CEGUI::Window *mInventoryWindow;
	bool isConstructed;
	const int COLUMN_COUNT, ROW_COUNT;

	bool handleItemDropped(const CEGUI::EventArgs& args);
	bool handleItemEntered(const CEGUI::EventArgs& args);
	CEGUI::Window *createSlot(int index);
	void registerSlotEvent(CEGUI::Window* slot);
	void subscribeEvents();

	std::vector<int> full_slots;

	std::string int_to_str( int i );
public:
	InGameUIInventoryWindow();
	~InGameUIInventoryWindow();
	void prepareToQuit();

	void toggle();
	bool isVisible();
	bool addChildWindow();

	bool isSlotOpen( int slot ); // Returns true if the slot has no item in it; otherwise false.
	bool addItem( std::string image, int &slotnum ); // Adds an item to the inventory window if there is room.  Returns the slot number to slotnum.
	bool removeItem( int slotnum );
};

#endif
