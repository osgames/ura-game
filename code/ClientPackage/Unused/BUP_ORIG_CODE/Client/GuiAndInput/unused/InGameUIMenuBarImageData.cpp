/*
Project Ura, an open source MMORPG.
Copyright (C) 2008  Denny Kunker

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InGameUIMenuBarImageData.h"




InGameUIMenuBarImageData::InGameUIMenuBarImageData() : windowName(NULL), imageSetName(NULL)
{
	count.push_back(0);
}

void InGameUIMenuBarImageData::create( string *winName, string *imgSetName, unsigned int numImages, vector<string> *imgTypes )
{
	windowName = winName;
	imageSetName = imgSetName;
	vImageType = imgTypes;

	count.at(0) = numImages;
}

string InGameUIMenuBarImageData::getImageString(int num, int state)
{
	CEGUI::PropertyHelper propertyHelper;
	string str = "";

	try
	{
		/*** Store the image sets names. its imageset name + count + " image:" + "imageset state name" in a string. ***/

		// First make sure that "num" is valid";
		if ( num < 0 || num > count.at(0) )
		{
			cout << "InGameUIMenuBarImageData::getImageString: num is too large or too small." << endl;
			return "";
		}


		str  = "set:";

		str += ( *imageSetName );	

		// Get the number and add it to the string;
		// It returns a cegui string so use "c_str()" to convert to std::string;
		str += ( propertyHelper.intToString(num).c_str() );

		str += " image:";

		str += (*vImageType)[state];
	}
	catch(...)
	{
		cout << "InGameUIMenuBarImageData::getImageString: An Error occured." << endl;

		// returns "" on error;
		return "";
	}

	#ifdef INGAMEUI_MENUBAR_IMAGEDATA_DEBUG
		cout << "InGameUIMenuBarImageData::getImageString: str = " << str << endl;
	#endif

	return str;
}

string InGameUIMenuBarImageData::getWindowName(int num)
{
	CEGUI::PropertyHelper propertyHelper;
	string str = "";

	try
	{
		/*** Store the imagesets name + number + ".imageset" ***/

		// First make sure that "num" is valid";
		if ( num < 0 || num > count.at(0) )
		{
			cout << "InGameUIMenuBarImageData::getWindowName: num is too large or too small." << endl;
			return "";
		}


		str = ( *windowName );	

		// Get the number and add it to the string;
		// It returns a cegui string so use "c_str()" to convert to std::string;
		str += ( propertyHelper.intToString(num).c_str() );
	}
	catch(...)
	{
		cout << "InGameUIMenuBarImageData::getWindowName: An Error occured." << endl;

		// returns "" on error;
		return "";
	}

	#ifdef INGAMEUI_MENUBAR_IMAGEDATA_DEBUG
		cout << "InGameUIMenuBarImageData::getWindowName: str = " << str << endl;
	#endif

	return str;
}

string InGameUIMenuBarImageData::getImageSetFileName(int num)
{
	CEGUI::PropertyHelper propertyHelper;
	string str = "";

	try
	{
		/*** Store the imagesets name + number + ".imageset" ***/

		// First make sure that "num" is valid";
		if ( num < 0 || num > count.at(0) )
		{
			cout << "InGameUIMenuBarImageData::getImageSetFileName: num is too large or too small." << endl;
			return "";
		}


		str = ( *imageSetName );	

		// Get the number and add it to the string;
		// It returns a cegui string so use "c_str()" to convert to std::string;
		str += ( propertyHelper.intToString(num).c_str() );

		str += ".imageset";
	}
	catch(...)
	{
		cout << "InGameUIMenuBarImageData::getImageSetFileName: An Error occured." << endl;

		// returns "" on error;
		return "";
	}

	#ifdef INGAMEUI_MENUBAR_IMAGEDATA_DEBUG
		cout << "InGameUIMenuBarImageData::getImageSetFileName: str = " << str << endl;
	#endif

	return str;
}

string InGameUIMenuBarImageData::getImageSetName(int num)
{
	CEGUI::PropertyHelper propertyHelper;
	string str = "";

	try
	{
		/*** Store the imagesets name + number + ".imageset" ***/

		// First make sure that "num" is valid";
		if ( num < 0 || num > count.at(0) )
		{
			cout << "InGameUIMenuBarImageData::getImageSetName: num is too large or too small." << endl;
			return "";
		}


		str = ( *imageSetName );	

		// Get the number and add it to the string;
		// It returns a cegui string so use "c_str()" to convert to std::string;
		str += ( propertyHelper.intToString(num).c_str() );
	}
	catch(...)
	{
		cout << "InGameUIMenuBarImageData::getImageSetName: An Error occured." << endl;

		// returns "" on error;
		return "";
	}

	#ifdef INGAMEUI_MENUBAR_IMAGEDATA_DEBUG
		cout << "InGameUIMenuBarImageData::getImageSetName: str = " << str << endl;
	#endif

	return str;
}


