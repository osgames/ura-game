#include "Inventory.h"
#include "InventoryItem.h"
#include "GUI.h"
#include "InGameUI.h"
#include "InGameUIInventoryWindow.h"

Inventory *Inventory::inventory = 0;

bool Inventory::addItem( int id )
{
	if( item_list.size() < 16 )
	{
		int slotnum;
		if( !mInventoryWindow->addItem( "Lime", slotnum ) )
		{
			return false;
		}
		InventoryItem *item = new InventoryItem();
		item_list.push_back( item );
		item->slotnum = slotnum;
		return true;
	}

	return false;
}

bool Inventory::removeItem( int slotnum )
{
	for( unsigned int i = 0; i < item_list.size(); i++ )
	{
		if( slotnum == item_list[i]->slotnum )
		{
			if( !mInventoryWindow->removeItem( slotnum ) )
			{
				return false;
			}

			item_list.erase( item_list.begin() + i );

			return true;
		}
	}

	return false;
}

Inventory::Inventory()
{
	mInventoryWindow = GUI::getSingletonPtr()->getInGameUI()->getInventoryWindow();

	if( !mInventoryWindow.get() )
	{
		Ogre::LogManager::getSingleton().logMessage( "Inventory.cpp: Error while trying to retrieve Inventory Window." );
	}
}

Inventory::~Inventory()
{
	for( unsigned int i = 0; i < item_list.size(); i++ )
	{
		delete item_list[i];
		item_list[i] = 0;
	}
}

void Inventory::clear()
{
	for( unsigned int i = 0; i < item_list.size(); i++ )
	{
		removeItem( item_list[i]->slotnum );
	}
}

Inventory *Inventory::getSingletonPtr()
{
	if( inventory )
	{
		return inventory;
	}

	inventory = new Inventory();
	return inventory;
}
