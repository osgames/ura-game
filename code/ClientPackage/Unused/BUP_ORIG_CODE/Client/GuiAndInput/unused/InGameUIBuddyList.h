#ifndef IN_GAME_UI_BUDDYLIST_H
#define IN_GAME_UI_BUDDYLIST_H


#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

class InGameUI;

using namespace std;
using namespace CEGUI;


/////#define INGAMEUIBUDDYLIST_DEBUG


class InGameUIBuddyList
{
protected:

	// pointer to the main ingameUI class;
	InGameUI* pInGameUI;

	CEGUI::WindowManager *windowManager;
	CEGUI::Window* sheet;
	CEGUI::ImagesetManager* imageSetManager;
	CEGUI::SchemeManager* schemeMgr;

	// main window;
	CEGUI::FrameWindow* frameWindow;	

	// listboxes and items;
	CEGUI::Listbox* listBox;
	CEGUI::Listbox* listBoxOnlineSort;
	CEGUI::Listbox* listBoxOfflineSort;
	CEGUI::ListboxTextItem* item;	
	unsigned int listBoxCurrentItem;

	// slider;
	CEGUI::Slider* slider;
	float previousSliderValue;
	CEGUI::PushButton* slider_scrollbar_pushButton_top;
	CEGUI::PushButton* slider_scrollbar_pushButton_bottom;

	// Buddy list buttons;
	CEGUI::PushButton* buttonAdd;
	CEGUI::PushButton* buttonEdit;
	CEGUI::PushButton* buttonRemove;

	// Text colors;
	CEGUI::colour myColor;
	vector<float> textColorOnline;
	vector<float> textColorOffline;

	// The windows to add or remove a buddy;
	CEGUI::FrameWindow* menuAddName;
	CEGUI::FrameWindow* menuEditName;
	CEGUI::FrameWindow* menuRemoveName;

	CEGUI::Editbox* menuAddNameEditbox;
	CEGUI::Editbox* menuEditNameEditbox;
	CEGUI::Editbox* menuRemoveNameEditbox;

	CEGUI::PushButton* menuAddName_ButtonYes;
	CEGUI::PushButton* menuAddName_ButtonNo;

	CEGUI::PushButton* menuRemoveName_ButtonYes;
	CEGUI::PushButton* menuRemoveName_ButtonNo;

	bool bThumbCalled_by_pushButton_top;
	bool bThumbCalled_by_pushButton;

	enum LISTBOX_EVENT_TYPE { ENUM_THUMB, ENUM_TOP_PUSH_BUTTON, ENUM_BOTTOM_PUSH_BUTTON };

public:
	// The previousSliderValue starts at the top so its 1.0;
	InGameUIBuddyList();
	~InGameUIBuddyList();

	void setInGameUI( InGameUI *gameUI );
	void prepareToQuit();
	void toggle();
	bool refreshBuddyList(const CEGUI::EventArgs& pEventArgs);
	bool refreshBuddyList();
	bool isVisible();
	void deactivate();

	// Functions for testing the buddylist;
	bool testme_loadListBox();
	bool testme_leftclick_online (const CEGUI::EventArgs& pEventArgs);
	bool testme_remove_and_offline(const CEGUI::EventArgs& pEventArgs);
	
	// Called when changes to the slider occur..such as moving up and down the buddylist;
	bool listBoxChangePosition(LISTBOX_EVENT_TYPE type);
	bool event_slider_thumbPositionChange(const CEGUI::EventArgs& pEventArgs);
	bool event_PushButton_top(const CEGUI::EventArgs& pEventArgs);
	bool event_PushButton_bottom(const CEGUI::EventArgs& pEventArgs);
	bool event_mousewheel(const CEGUI::EventArgs& pEventArgs);

	// Buddylist add, edit and remove buttons;
	bool event_add_button(const CEGUI::EventArgs& pEventArgs);
	bool event_edit_button(const CEGUI::EventArgs& pEventArgs);
	bool event_remove_button(const CEGUI::EventArgs& pEventArgs);

	// A window menu to add a name;
	bool event_menuAddName_ButtonNo(const CEGUI::EventArgs& pEventArgs);
	bool event_menuAddName_ButtonYes(const CEGUI::EventArgs& pEventArgs);

	// A window menu to remove a name;
	bool event_menuRemoveName_ButtonNo(const CEGUI::EventArgs& pEventArgs);
	bool event_menuRemoveName_ButtonYes(const CEGUI::EventArgs& pEventArgs);

	// Add and remove a player;
	void removePlayer(const string &name);
	bool addPlayer(const string &name, const bool state);	// state = 0 for offline and 1 for online;

	// Set the player online and offline;
	bool setPlayerOnline(const string &name);
	bool setPlayerOffline(const string &name);

	bool addChildWindow( CEGUI::SchemeManager* schemeManager, CEGUI::ImagesetManager* imgSetManager, 
					CEGUI::WindowManager* winManager, CEGUI::Window* window, bool bLayoutSuccess);

};
 



#endif

