/*
Project Ura, an open source MMORPG.
Copyright (C) 2008  Denny Kunker

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapMenuBar.h"
#include "InGameUI.h"



/*************************************************************************************
There are four functions that deal with how the menubar images change and these are:

*bool event_mouseHover_image(const CEGUI::EventArgs& pEventArgs);
*bool event_imageLeftPress_toFront(const CEGUI::EventArgs& pEventArgs);
*bool event_LeftButtonUp(const CEGUI::EventArgs& pEventArgs);
*bool event_mouseLeave_image(const CEGUI::EventArgs& pEventArgs);


mouseHover - changes the image to a hover image whenever the cursor hovers the button;
leftPress_toFront - changes the image to pressed when the image is pressed;

LeftButtonUp - calls the function mPobject->event_menubar_toggleButton(i, [true or false]).
  which checks if the window is visible or not, and depending on its outcome calls
  setImageActive() or setImageNormal() which changes the states, or the image depending on keyboard or mouse bool.
  When the cursor leaves the image it calls the function mouseLeave_image() which then sets the
  Image depending on its state.

mouseLeave_image - sets the Image depending on its state.

#######################################################################################

When the mouse button isn't released under the button and is held until it leaves the image button, then
Problems can occur. Such as the image toggling when its not suppose to. "mLeftMouseBtnReleased" variable checks if
The user released the mouse button under the image button. If not then we restore the image back
to its previous state;

leftMousebtnRelease deals with the fuctions:
*bool event_imageLeftPress_toFront(const CEGUI::EventArgs& pEventArgs);
*bool event_LeftButtonUp(const CEGUI::EventArgs& pEventArgs);
*bool event_mouseLeave_image(const CEGUI::EventArgs& pEventArgs);
setImageActive(const unsigned int button, const bool keyboard)
setImageNormal(const unsigned int button, const bool keyboard)

****************************************************************************************/





// The point is to put as much stuff on the heap as possible. And to delete things when not needed or toggled;
HeapMenuBar::HeapMenuBar()
{
	mMenubar = NULL;
}

HeapMenuBar::~HeapMenuBar()
{
	if ( mMenubar )	
	{ 
		delete mMenubar; 
	}
}

// Prepare the windows for quiting;
// This function saves the layout and destroys the cegui window;
// Note: the code from prepareToQuit() should not be put in its objects destructor;
void HeapMenuBar::prepareToQuit()
{
	if ( mMenubar )	
	{ 
		mMenubar->prepareToQuit();
	}
}

void HeapMenuBar::toggle(InGameUI *gameUI, const unsigned int numButtons,
	vector<unsigned int>* vActiveButton, vector<string>* vToolTip )
{
	/*** If visible, then hide and delete the window and object. Else we create the object ***/

	if ( mMenubar )	
	{
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		mMenubar->prepareToQuit();
		delete mMenubar;
		mMenubar = NULL;		// Let us know the object was deleted;

		#ifdef HEAPMENUBAR_DEBUG
			cout << "Deleted mMenubar" << endl;
		#endif
	}
	else
	{
		/*** Recreate the window ***/
		mMenubar = new InGameUIMenuBar;
		
		if ( mMenubar == NULL )
		{ 
			cout << "HeapMenuBar::toggle(): (mMenubar == NULL)" <<
				" Error allocating heap memory" << endl;

			// Don't allow us to continue further;
			return;
		}

		mMenubar->addChildWindow(gameUI, numButtons, vActiveButton, vToolTip);

		#ifdef HEAPMENUBAR_DEBUG
			cout << "created mMenubar window" << endl;
		#endif
	}
}



bool HeapMenuBar::isVisible()
{
	if ( mMenubar )
	{
		return mMenubar->isVisible();
	}

	return false;
}

bool HeapMenuBar::isActive()
{
	if ( mMenubar )
	{
		return mMenubar->isActive();
	}

	return false;
}

void HeapMenuBar::deactivate()
{
	if ( mMenubar )
	{
		mMenubar->deactivate();
	}
}

void HeapMenuBar::setImageActive(const unsigned int button, const bool keyboard)
{
	if ( mMenubar )
	{
		mMenubar->setImageActive( button, keyboard );
	}
}


void HeapMenuBar::setImageNormal(const unsigned int button, const bool keyboard)
{
	if ( mMenubar )
	{
		mMenubar->setImageNormal( button, keyboard );
	}
}











InGameUIMenuBar::InGameUIMenuBar()
{ 
	mImgMover = NULL;
	mMenubarImageMover = NULL;
	mImgResizer = NULL;
	mMenubarImageResizer = NULL;
	mMovingWindow = NULL;
	mPobject = NULL;
}

void InGameUIMenuBar::prepareToQuit()
{
	string strTmp = "";
	CEGUI::PropertyHelper phTmp;
	ofstream os;


	/*** If we were dragging or resizing, then disconnect the event and stop ***/
	if(mMovingWindow)
	{
		// We were moving a window, stop doing so;
		mMovingEvent[0]->disconnect();
	}

	/*** Clear the tooltips ***/
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->setTooltipText("");

		// This seems the best to get rid of the tooltips;
		mVpWindow[i]->setTooltip (0);
	}

	/*** Clear the images ***/

	// Clear the image mover;
	mMenubarImageMover->setProperty( "Image", "" );
	// Clear the Image Resizer;
	mMenubarImageResizer->setProperty( "Image", "" );
	
	// Clear the images
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->setProperty("Image", "");
	}


	/*** Save the layout ***/

	// Image Mover;
	os.open( INGAMEUI_MENUBAR_IMAGEMOVER_LAYOUTFILE, std::ios::binary );
	CEGUI::WindowManager::getSingleton().writeWindowLayoutToStream(*mMenubarImageMover,os,true);
	os.close();

	// Image Resizer;
	os.open( INGAMEUI_MENUBAR_IMAGERESIZER_LAYOUTFILE, std::ios::binary );
	CEGUI::WindowManager::getSingleton().writeWindowLayoutToStream(*mMenubarImageResizer,os,true);
	os.close();

	//  Save all the images to their layout files with firstname + number + ".layout";
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		// Make a filename;	
		strTmp = INGAMEUI_MENUBAR_IMAGE_LAYOUTFILE;
		strTmp += phTmp.intToString(i).c_str();
		strTmp += ".layout";

		os.open( strTmp.c_str(), std::ios::binary );
		CEGUI::WindowManager::getSingleton().writeWindowLayoutToStream(*(mVpWindow[i]),os,true);
		os.close();
	}


	/*** Destroy the imagesets ***/
	CEGUI::ImagesetManager::getSingleton().destroyImageset( mImgMover );
	CEGUI::ImagesetManager::getSingleton().destroyImageset( mImgResizer );

	for (int i = 0; i < mVpImageSet.size(); i++)
	{
		CEGUI::ImagesetManager::getSingleton().destroyImageset( mVpImageSet[i] );
	}	


	/*** Destroy the windows ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mMenubarImageMover);
	CEGUI::WindowManager::getSingleton().destroyWindow(mMenubarImageResizer);
	
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		CEGUI::WindowManager::getSingleton().destroyWindow( mVpWindow[i] );
	}
}

void InGameUIMenuBar::toggle()
{
	if ( mVpWindow[0]->isVisible() == false )		// Just use the First image for the checks of visiblility;
	{
		/*** Show and activate all the windows ***/

		// show and activate the ImageMover and ImageResizer;
		mMenubarImageMover->show();
		mMenubarImageResizer->show();

		mMenubarImageMover->activate();
		mMenubarImageResizer->activate();
		
		// Show and activate all the images;
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i]->show();

			mVpWindow[i]->activate();
		}	
	}
	else if( mVpWindow[0]->isVisible() == true )
	{	
		/***  Hide all the windows ***/

		// But first lets disconnect our resizing or moving signal...if there is one;
		if(mMovingWindow)
		{
			UVector2 resizerLoadPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F)) );

			// Set to not being dragged anymore;
			mWindowBeingDragged[0] = false;

			// We were moving a window, stop doing so;
			mMovingEvent[0]->disconnect();
			mMovingWindow = 0;
			
			// Set the ImageResizer Position;
			resizerLoadPosition = mVpWindow[(mNumImageButtons[0]) - 1]->getPosition() + mVpWindow[(mNumImageButtons[0]) - 1]->getSize();
			resizerLoadPosition.d_y.d_scale += mImageResizerYSpace[0];
			mMenubarImageResizer->setPosition( resizerLoadPosition );

			/*** Release the captured input goes to this window ***/
			mMenubarImageResizer->releaseInput();
		}

		// Hide the ImageMover and ImageResizer;
		mMenubarImageMover->hide();
		mMenubarImageResizer->hide();

		// Hide all the images;
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i]->hide();
		}
	}
}

bool InGameUIMenuBar::isVisible()
{
	return mVpWindow[0]->isVisible();
}

bool InGameUIMenuBar::isActive()
{
	/*** Check if the windows are active ***/

	if ( mWindowBeingDragged[0] == true ) 
		return true;

	// Since one image activates them all, only one image needs to be tested;
	if ( mMenubarImageMover->isActive() ) 
		return true;
	

	return false;
}

void InGameUIMenuBar::deactivate()
{
	/*** Deactivate the images ***/

	// resizer and mover;
	mMenubarImageMover->deactivate();
	mMenubarImageResizer->deactivate();

	// the images;
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->deactivate();
	}
}

void InGameUIMenuBar::setImageActive(const unsigned int button, const bool keyboard)
{
	// Set the image state to active;
	mVimageStates[button].setState(IMAGE_TYPE_ACTIVE);

	// If we are using the keyboard then switch the image right away;
	// The mouse is different and deals with presses, hovering and such;
	if ( keyboard )
	{
		mVpWindow[button]->setProperty( "Image", mMenuBarImageData[0].getImageString(button, IMAGE_TYPE_ACTIVE) );
		mLeftMouseBtnReleased[0] = true;
	}

}


void InGameUIMenuBar::setImageNormal(const unsigned int button, const bool keyboard)
{
	// Set the image state to normal;
	mVimageStates[button].setState(IMAGE_TYPE_NORMAL);

	// If we are using the keyboard then switch the image right away;
	// The mouse is different and deals with presses, hovering and such;
	if ( keyboard )
	{
		mVpWindow[button]->setProperty( "Image", mMenuBarImageData[0].getImageString(button, IMAGE_TYPE_NORMAL) );
		mLeftMouseBtnReleased[0] = true;
	}
}



bool InGameUIMenuBar::addChildWindow(InGameUI *gameUI, const unsigned int numButtons,
		vector<unsigned int>* vActiveButton, vector<string>* vToolTip )
{
	// Set the class data member pointers;
	mPobject = gameUI;			// Get the object pointer;

	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();

	// This is for the image resizer load position;
	UVector2 resizerLoadPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F)) );



	/********************************/
	/*** Setup the menubar basics ***/
	/********************************/

	// Set the number of menubar buttons;
	mNumImageButtons.push_back(numButtons);

	// Set the Yspace of the imageresizer image;
	mImageResizerYSpace.push_back(-0.011F);

	mImageWindowName.push_back("InGameUIMenuBar_Image");
	mImagesetName.push_back("ingameui_imageset_menubar_button");

	// Set the image type strings. These will be used to set the image;
	// The #defines are associated with this. EX: "#define IMAGE_TYPE_NORMAL 0"
	mVimageTypes.push_back("Normal");
	mVimageTypes.push_back("Hover");	
	mVimageTypes.push_back("Pressed");
	mVimageTypes.push_back("Active");

	// Create the strings for each imageset and image;
	mMenuBarImageData.push_back( InGameUIMenuBarImageData() );
	mMenuBarImageData[0].create( &(mImageWindowName[0]), &(mImagesetName[0]), mNumImageButtons[0], &mVimageTypes );


	/*** Create the pointers for the windows and imagesets. And create the image states ***/
	for (int i = 0; i < mNumImageButtons[0]; i++)
	{

		// Create the pointers for the windows and imagesets;
		mVpWindow.push_back( 0 );
		mVpImageSet.push_back( 0 );

		// Create the image states. The default is normal;
		mVimageStates.push_back( MenuBarImageState() );
	}


	/*** Set the window dragging and resizing variables ***/

	mMousePosInWindow.push_back( CEGUI::Vector2() );
	mMovingEvent.push_back( CEGUI::Event::Connection() );
	mWindowBeingDragged.push_back(false);


	/**********
		When the mouse button isn't released under the button and is held until it leaves the image button, then
		Problems can occur. Such as the image toggling when its not suppose to. "mLeftMouseBtnReleased" variable checks if
		The user released the mouse button under the image button. If not then we restore the image back
		to its previous state;
	**********/
	mLeftMouseBtnReleased.push_back(true);




	/****************************/
	/*** Create the imageSets ***/
	/****************************/
	try
	{
		// imageMover;
		if ( imageSetManager.isImagesetPresent("InGameUIMenuBar_IM_Mover") == false )
		{
			mImgMover = imageSetManager.createImagesetFromImageFile( "InGameUIMenuBar_IM_Mover", "mover.png" );

		}

		// imageResizer;
		if ( imageSetManager.isImagesetPresent("InGameUIMenuBar_IM_Resizer") == false )
		{
			mImgResizer = imageSetManager.createImagesetFromImageFile( "InGameUIMenuBar_IM_Resizer", "resizer.png" );
		}

		// All the images;
		for (int i = 0; i < mNumImageButtons[0]; i++)
		{
			if ( imageSetManager.isImagesetPresent(mMenuBarImageData[0].getImageSetName(i)) == false )
			{
				mVpImageSet[i] = imageSetManager.createImageset( mMenuBarImageData[0].getImageSetFileName(i) ); 	
			}
		}
	}
	catch( ... )
	{
		cout << "InGameUIMenuBar::addChildWindow(): Error creating the imagesets" << endl;
		return false;
	}




	// If the loading of the layout failes then we create hardcoded windows, settings and events. 
	// If successful we just have to make events and set the settings.
	try
	{
		string strTmp;
		CEGUI::PropertyHelper phTmp;

		/*** load the windows from the layout file ***/
		mMenubarImageMover = windowManager.loadWindowLayout( INGAMEUI_MENUBAR_IMAGEMOVER_LAYOUTFILE );
		mMenubarImageResizer = windowManager.loadWindowLayout( INGAMEUI_MENUBAR_IMAGERESIZER_LAYOUTFILE );

		// Were loading 0-numbuttons images. So we add the first name + number + ".layout";
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			strTmp = INGAMEUI_MENUBAR_IMAGE_LAYOUTFILE;
			strTmp += phTmp.intToString(i).c_str();
			strTmp += ".layout";

			mVpWindow[i] = windowManager.loadWindowLayout( strTmp );
		}
	}
	catch(...)
	{
		#ifdef HEAPMENUBAR_DEBUG
			cout << "InGameUIMenuBar::addChildWindow(): ERROR loading the layout." << endl;
		#endif


		// Images;
		float img_Xsize = 0.06F;
		float img_Ysize = 0.06F;
		float img_Xpos = 0.015F;
		float img_Ypos = 0.0F;		


		/*************************************************************/
		/*** mMenubarImageMover - Make an image to move the menubar ***/
		/*************************************************************/

		// mMenubarImageMover;
		mMenubarImageMover = windowManager.createWindow("TaharezLook/StaticImage", "InGameUIMenuBar_ImageMover");
		mMenubarImageMover->setSize(CEGUI::UVector2(CEGUI::UDim(0.01, 0.0F), CEGUI::UDim(0.01, 0.0F)));
		mMenubarImageMover->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F)));
		// Disable the frame and background;
		mMenubarImageMover->setProperty("FrameEnabled", "false");
		mMenubarImageMover->setProperty("BackgroundEnabled", "false");

		/*****************************************************************/
		/*** mMenubarImageResizer - Make an image to resize the menubar ***/
		/*****************************************************************/
		mMenubarImageResizer = windowManager.createWindow("TaharezLook/StaticImage", "InGameUIMenuBar_ImageResizer");
		mMenubarImageResizer->setSize(CEGUI::UVector2(CEGUI::UDim(0.011, 0.0F), CEGUI::UDim(0.011, 0.0F)));
		// Disable the frame and background;
		mMenubarImageResizer->setProperty("FrameEnabled", "false");
		mMenubarImageResizer->setProperty("BackgroundEnabled", "false");


		/*************************/
		/*** Create the images ***/
		/*************************/	
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i] = windowManager.createWindow( "TaharezLook/StaticImage", mMenuBarImageData[0].getWindowName(i) );
			mVpWindow[i]->setSize( CEGUI::UVector2(CEGUI::UDim(img_Xsize, 0.0F), CEGUI::UDim(img_Ysize, 0.0F)) );
			mVpWindow[i]->setPosition( CEGUI::UVector2(CEGUI::UDim(img_Xpos, 0.0F), CEGUI::UDim(img_Ypos, 0.0F)) );
			// Disable the frame and background;
			mVpWindow[i]->setProperty( "FrameEnabled", "false" );
			mVpWindow[i]->setProperty( "BackgroundEnabled", "false" );

			// Give the next image its position;
			img_Xpos += img_Xsize;
		}


		/**********************/
		/*** Set the events ***/
		/**********************/ 

		/*** Set events for resizer and mover left press ***/
		mMenubarImageMover->subscribeEvent(CEGUI::Window::EventMouseButtonDown, Event::Subscriber(
			&InGameUIMenuBar::event_imageMover_onLeftPress, this));			

		mMenubarImageResizer->subscribeEvent(CEGUI::Window::EventMouseButtonDown, Event::Subscriber(
			&InGameUIMenuBar::event_imageSizer_onLeftPress,	this));


		/*** Set events for image left press ***/
		// images;
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(
					&InGameUIMenuBar::event_imageLeftPress_toFront, this ) );
		}


		/*** Set events for image mouseover/hover ***/
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseEnters, CEGUI::Event::Subscriber( 
					&InGameUIMenuBar::event_mouseHover_image, this ) );
		}


		/*** Set events for mouse leave ***/
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseLeaves, CEGUI::Event::Subscriber( 
					&InGameUIMenuBar::event_mouseLeave_image, this ) );
		}


		/*** Set events for left button release ***/
		mMenubarImageMover->subscribeEvent( CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber( 
					&InGameUIMenuBar::event_LeftButtonUp, this ) );
		mMenubarImageResizer->subscribeEvent( CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber( 
					&InGameUIMenuBar::event_LeftButtonUp, this ) );

		// Images;
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber( 
					&InGameUIMenuBar::event_LeftButtonUp, this ) );
		}



		/************************************************************************************/
		/*** Set the Cleared properties that don't get added to the layout file because - ***/
		/*** CEGUI includes <auto windows> and causes these properties to crash the game. ***/
		/*** Note This is a CEGUI bug.                                                    ***/
		/************************************************************************************/

		// Set the tooltip types;
		for (int i = 0; i < vToolTip->size(); i++)
		{
			mVpWindow[i]->setTooltipType("TaharezLook/Tooltip");
		}
	
		// Set the tooltips;
		for (int i = 0; i < vToolTip->size(); i++)
		{
			mVpWindow[i]->setTooltipText( (*vToolTip)[i] );
		}


		/****************/
		/*** Settings ***/
		/****************/
		
		// Set all the images to normal;
		mMenubarImageMover->setProperty( "Image", "set:InGameUIMenuBar_IM_Mover image:full_image" );
		mMenubarImageResizer->setProperty( "Image", "set:InGameUIMenuBar_IM_Resizer image:full_image" );

		for (int i = 0; i < mNumImageButtons[0]; i++)
		{
			mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_NORMAL) );
		}





		// vActiveButton is a vector of the buttons that are active on the main menu.
		// We use the vector to figure out which buttons to change and then change them;
		if (vActiveButton )
		{
			for (int i = 0; i < vActiveButton->size(); i++)
			{
				int tmp = (*vActiveButton)[i];
		
				mVpWindow[tmp]->setProperty( "Image", mMenuBarImageData[0].getImageString(tmp, IMAGE_TYPE_ACTIVE) );
		
				// Set the state to active;
				mVimageStates[tmp].setState(IMAGE_TYPE_ACTIVE);	
			}
		}	


		// Set the ImageResizer Position 
		resizerLoadPosition = mVpWindow[(mNumImageButtons[0]) - 1]->getPosition() + mVpWindow[(mNumImageButtons[0]) - 1]->getSize();
		resizerLoadPosition.d_y.d_scale += mImageResizerYSpace[0];
		mMenubarImageResizer->setPosition( resizerLoadPosition );


		/********************************************/
		/*** Add these windows to the main window ***/
		/********************************************/
		sheet->addChildWindow( mMenubarImageMover );
		sheet->addChildWindow( mMenubarImageResizer );	

		// Images;
		for (int i = 0; i < mVpWindow.size(); i++)
		{
			sheet->addChildWindow( mVpWindow[i] );
		}

	
		// Let us know everything is ok;
		return true;
	}


	/*****************************************************/
	/*****************************************************/
	/*** If we got here then the loading was a success ***/
	/*****************************************************/
	/*****************************************************/	

	#ifdef HEAPMENUBAR_DEBUG
		cout << "InGameUIMenuBar::addChildWindow(): SUCCESS loading the layout." << endl;
	#endif


	/**********************/
	/*** Set the events ***/
	/**********************/ 

	/*** Set events for resizer and mover left press ***/
	mMenubarImageMover->subscribeEvent(CEGUI::Window::EventMouseButtonDown, Event::Subscriber(
		&InGameUIMenuBar::event_imageMover_onLeftPress, this));			

	mMenubarImageResizer->subscribeEvent(CEGUI::Window::EventMouseButtonDown, Event::Subscriber(
		&InGameUIMenuBar::event_imageSizer_onLeftPress,	this));


	/*** Set events for image left press ***/
	// images;
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(
				&InGameUIMenuBar::event_imageLeftPress_toFront, this ) );
	}


	/*** Set events for image mouseover/hover ***/
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseEnters, CEGUI::Event::Subscriber( 
				&InGameUIMenuBar::event_mouseHover_image, this ) );
	}


	/*** Set events for mouse leave ***/
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseLeaves, CEGUI::Event::Subscriber( 
				&InGameUIMenuBar::event_mouseLeave_image, this ) );
	}


	/*** Set events for left button release ***/
	mMenubarImageMover->subscribeEvent( CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber( 
				&InGameUIMenuBar::event_LeftButtonUp, this ) );
	mMenubarImageResizer->subscribeEvent( CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber( 
				&InGameUIMenuBar::event_LeftButtonUp, this ) );

	// Images;
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->subscribeEvent( CEGUI::Window::EventMouseButtonUp, CEGUI::Event::Subscriber( 
				&InGameUIMenuBar::event_LeftButtonUp, this ) );
	}


	/************************************************************************************/
	/*** Set the Cleared properties that don't get added to the layout file because - ***/
	/*** CEGUI includes <auto windows> and causes these properties to crash the game. ***/
	/*** Note This is a CEGUI bug.                                                    ***/
	/************************************************************************************/

	// Set the tooltip types;
	for (int i = 0; i < vToolTip->size(); i++)
	{
		mVpWindow[i]->setTooltipType("TaharezLook/Tooltip");
	}

	// Set the tooltips;
	for (int i = 0; i < vToolTip->size(); i++)
	{
		mVpWindow[i]->setTooltipText( (*vToolTip)[i] );
	}


	/****************/
	/*** Settings ***/
	/****************/
	
	// Set all the images to normal;
	mMenubarImageMover->setProperty( "Image", "set:InGameUIMenuBar_IM_Mover image:full_image" );
	mMenubarImageResizer->setProperty( "Image", "set:InGameUIMenuBar_IM_Resizer image:full_image" );

	for (int i = 0; i < mNumImageButtons[0]; i++)
	{
		mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_NORMAL) );
	}




	// vActiveButton is a vector of the buttons that are active on the main menu.
	// We use the vector to figure out which buttons to change and then change them;
	if (vActiveButton )
	{
		for (int i = 0; i < vActiveButton->size(); i++)
		{
			int tmp = (*vActiveButton)[i];
	
			mVpWindow[tmp]->setProperty( "Image", mMenuBarImageData[0].getImageString(tmp, IMAGE_TYPE_ACTIVE) );
	
			// Set the state to active;
			mVimageStates[tmp].setState(IMAGE_TYPE_ACTIVE);	
		}
	}	



	// Set the ImageResizer Position 
	resizerLoadPosition = mVpWindow[(mNumImageButtons[0]) - 1]->getPosition() + mVpWindow[(mNumImageButtons[0]) - 1]->getSize();
	resizerLoadPosition.d_y.d_scale += mImageResizerYSpace[0];
	mMenubarImageResizer->setPosition( resizerLoadPosition );

	
	/********************************************/
	/*** Add these windows to the main window ***/
	/********************************************/
	sheet->addChildWindow( mMenubarImageMover );
	sheet->addChildWindow( mMenubarImageResizer );	

	// Images;
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		sheet->addChildWindow( mVpWindow[i] );
	}


	// Let us know everything is ok;
	return true;
}

/*** IMAGES ***/
bool InGameUIMenuBar::event_mouseHover_image(const CEGUI::EventArgs& pEventArgs)
{	
	CEGUI::Window* window = NULL;
	const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
	window = windowEventArgs.window;


	// If we are resizing then don't activate the hover image; 
	if ( mWindowBeingDragged[0] == true )
	{
		return true;
	}

	// Use the windows pointer to figure out which image to change and change it;
	for (int i = 0; i < mNumImageButtons[0]; i++)
	{
		if ( window == mVpWindow[i] )
		{
			// Change the image;
			mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_HOVER) );

			return true;;
		}
	}

	return false;
}



bool InGameUIMenuBar::event_mouseLeave_image(const CEGUI::EventArgs& pEventArgs)
{
	/**********
		When the mouse button isn't released under the button and is held until it leaves the image button, then
		Problems can occur. Such as the image toggling when its not suppose to. "mLeftMouseBtnReleased" variable checks if
		The user released the mouse button under the image button. If not then we restore the image back
		to its previous state;
	**********/

	CEGUI::Window* window = NULL;
	const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
	window = windowEventArgs.window;


	// Use the windows pointer to figure out which image to change and change it;
	for (int i = 0; i < mNumImageButtons[0]; i++)
	{
		if ( window == mVpWindow[i] )
		{
			if ( mVimageStates[i].getState() == IMAGE_TYPE_NORMAL )
			{
				if ( !(mLeftMouseBtnReleased[0]) )
				{
					mVimageStates[i].setState(IMAGE_TYPE_ACTIVE);
					mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_ACTIVE) );

					mLeftMouseBtnReleased[0] = true;
				}
				else
				{
					// Change the image;
					mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_NORMAL) );
				}
			}
			else if ( mVimageStates[i].getState() == IMAGE_TYPE_ACTIVE )
			{
				if ( !(mLeftMouseBtnReleased[0]) )
				{
					mVimageStates[i].setState(IMAGE_TYPE_NORMAL);
					mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_NORMAL) );

					mLeftMouseBtnReleased[0] = true;
				}
				else
				{
					mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_ACTIVE) );
				}
			}

			return true;
		}
	}

	return false;
}



/**********************************************************************************************/
/*** This function handles left button image presses and sets all the menbar windows on top ***/
/**********************************************************************************************/

// Note mouse button presses toggle windows here;
bool InGameUIMenuBar::event_imageLeftPress_toFront(const CEGUI::EventArgs& pEventArgs)
{	
	/**********
		When the mouse button isn't released under the button and is held until it leaves the image button, then
		Problems can occur. Such as the image toggling when its not suppose to. "mLeftMouseBtnReleased" variable checks if
		The user released the mouse button under the image button. If not then we restore the image back
		to its previous state;
	**********/

	CEGUI::Window* window = NULL;
	const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);
	window = windowEventArgs.window;


	/*** Put everything in front ***/
	mMenubarImageMover->moveToFront();
	mMenubarImageResizer->moveToFront();

	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->moveToFront();
	}


	/*** Handle left button presses ***/
	if(mouseEventArgs.button == CEGUI::LeftButton)
	{
		// Always show the pressed image;
		// Determine the state, If its normal than set it active. If its active then set it normal
		// IF there is a function to call then call it;
		for (int i = 0; i < mNumImageButtons[0]; i++)
		{
			if ( window == mVpWindow[i] )
			{
				// Set the pressed image;
				mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_PRESSED) );

				/*** Let us know that the button is now pressed ***/
				mLeftMouseBtnReleased[0] = false;

				/*** Change the state from normal to active and from active to normal ***/
				if ( mVimageStates[i].getState() == IMAGE_TYPE_NORMAL )
				{
					mVimageStates[i].setState(IMAGE_TYPE_ACTIVE);
				}
				else if ( mVimageStates[i].getState() == IMAGE_TYPE_ACTIVE )
				{
					mVimageStates[i].setState(IMAGE_TYPE_NORMAL);
				}

				return true;
			}
		}
	}

	return false;
}


/***************************************************/
/*** This function handles left button up events ***/
/***************************************************/
bool InGameUIMenuBar::event_LeftButtonUp(const CEGUI::EventArgs& pEventArgs)
{	
	/**********
		When the mouse button isn't released under the button and is held until it leaves the image button, then
		Problems can occur. Such as the image toggling when its not suppose to. "mLeftMouseBtnReleased" variable checks if
		The user released the mouse button under the image button. If not then we restore the image back
		to its previous state;
	**********/

	CEGUI::Window* window = NULL;
	const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
	window = windowEventArgs.window;

	UVector2 resizerLoadPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F)) );
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);


	if(mouseEventArgs.button == CEGUI::LeftButton)
	{

		// If we are moving the window then don't activate the hover image; 
		// We have to know were not moving anymore;
		// This also makes sure when the windows are active and prevents the mouse from -
		// - moving the camera;
		mWindowBeingDragged[0] = false;
		
	
		// Use the windows pointer to figure out which image to change and change it;
		// The Hover image is shown whenever the left mouse button is released;
		for (int i = 0; i < mNumImageButtons[0]; i++)
		{
			if ( window == mVpWindow[i] )
			{
				/*** Let us know we released the button normally ***/
				mLeftMouseBtnReleased[0] = true;

				/*** call the mPobject->event_menubar_mouseToggleButton(i) function - to handle toggling a button image ***/
				mPobject->event_menubar_toggleButton(i, false);	// False is to indicate were using the mouse and not keyboard;

				// We should be on the image hovering so change the pic to hover;
				mVpWindow[i]->setProperty( "Image", mMenuBarImageData[0].getImageString(i, IMAGE_TYPE_HOVER) );
			}
		}

		if(mMovingWindow)
		{
			// We were moving a window, stop doing so;
			mMovingEvent[0]->disconnect();
			mMovingWindow = 0;
			
			// Set the ImageResizer Position;
			resizerLoadPosition = mVpWindow[(mNumImageButtons[0]) - 1]->getPosition() + mVpWindow[(mNumImageButtons[0]) - 1]->getSize();
			resizerLoadPosition.d_y.d_scale += mImageResizerYSpace[0];
			mMenubarImageResizer->setPosition( resizerLoadPosition );

			/*** Release the captured input goes to this window ***/
			mMenubarImageResizer->releaseInput();

		}
	}

	// Give the main sheet focus;
	deactivate();

	return true;
}


/*********************************************************************************/
/*** THESE TWO FUNCTIONS ARE FOR MOVING THE MENUBAR WINDOW WITH THE IMAGEMOVER ***/
/*********************************************************************************/
bool InGameUIMenuBar::event_imageMover_onLeftPress(const CEGUI::EventArgs& pEventArgs)
{
	UVector2 resizerLoadPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F)) );
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);
	

	if(mouseEventArgs.button == CEGUI::LeftButton)
	{
		if(mMovingWindow)
		{
			/*********************************************************
			// We were moving a window, stop doing so on a second right click
			mMovingEvent->disconnect();
			mMovingWindow = 0;

			// Set the ImageResizer Position;
			resizerLoadPosition = mVpWindow[mNumImageButtons - 1]->getPosition() + mVpWindow[mNumImageButtons - 1]->getSize();
			resizerLoadPosition.d_y.d_scale += mImageResizerYSpace;
			mMenubarImageResizer->setPosition( resizerLoadPosition );
			**********************************************************/
		}
		else
		{
			// This makes sure when the windows are active and prevents the mouse from -
			// - moving the camera;
			mWindowBeingDragged[0] = true;

			/*** Make all the window bars active and put them in front ***/
			for (int i = 0; i < mVpWindow.size(); i++)
			{
				mVpWindow[i]->moveToFront();
			}

			mMenubarImageMover->moveToFront();
			mMenubarImageResizer->moveToFront();

			/*** Start moving a window  ***/
			const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
			mMovingWindow = windowEventArgs.window;
			mMovingEvent[0] = System::getSingleton().getGUISheet()->subscribeEvent(CEGUI::Window::EventMouseMove, Event::Subscriber(&InGameUIMenuBar::event_imageMover_onWindowMove, this));
			mMousePosInWindow[0] = CoordConverter::screenToWindow(*mMovingWindow, MouseCursor::getSingleton().getPosition());
		}
		return false; // Do not propagate this event to other subscribers
	}
	return true;
}

bool InGameUIMenuBar::event_imageMover_onWindowMove(const CEGUI::EventArgs& pEventArgs)
{
	UVector2 mouseOffset = ( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F) ) );
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);
	Vector2 localMousePos = CoordConverter::screenToWindow(*mMovingWindow, mouseEventArgs.position);
	UVector2 mouseMoveOffset(cegui_absdim(localMousePos.d_x), cegui_absdim(localMousePos.d_y));
	UVector2 mouseOffsetInWindow(cegui_absdim(mMousePosInWindow[0].d_x), cegui_absdim(mMousePosInWindow[0].d_y));
	/////mMovingWindow->setPosition(mMovingWindow->getPosition() + mouseMoveOffset - mouseOffsetInWindow);


	// Get the mouse offset in the window;
	mouseOffset =  mouseMoveOffset - mouseOffsetInWindow;


	// Move the mMenubarImageMover window;
	mMenubarImageMover->setPosition(mMenubarImageMover->getPosition() + mouseOffset);

	// Move the mMenubarImageResizer window;
	mMenubarImageResizer->setPosition(mMenubarImageResizer->getPosition() + mouseOffset);

	// Move all the images;
	for (int i = 0; i < mVpWindow.size(); i++)
	{
		mVpWindow[i]->setPosition(mVpWindow[i]->getPosition() + mouseOffset);
	}

	return true;
}


/*************************************************************************************/
/*** THESE TWO FUNCTIONS ARE FOR RESIZING THE MENUBAR WINDOW WITH THE IMAGERESIZER ***/
/*************************************************************************************/
bool InGameUIMenuBar::event_imageSizer_onLeftPress(const CEGUI::EventArgs& pEventArgs)
{
	UVector2 resizerLoadPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F)) );
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);


	if(mouseEventArgs.button == CEGUI::LeftButton)
	{
		if(mMovingWindow)
		{
			/*********************************************************
			// We were moving a window, stop doing so on a second right click
			mMovingEvent->disconnect();
			mMovingWindow = 0;

			// Set the ImageResizer Position;
			resizerLoadPosition = mVpWindow[mNumImageButtons - 1]->getPosition() + mVpWindow[mNumImageButtons - 1]->getSize();
			resizerLoadPosition.d_y.d_scale += mImageResizerYSpace;
			mMenubarImageResizer->setPosition( resizerLoadPosition );
			*********************************************************/
		}
		else
		{
			// Store the State of the window being dragged so that we can fix a bug - 
			//of the resizer touching the last image and making the hover image appear;
			// This also makes sure when the windows are active and prevents the mouse from -
			// - moving the camera;
			mWindowBeingDragged[0] = true;


			/*** Put everything in front ***/
			for (int i = 0; i < mVpWindow.size(); i++)
			{
				mVpWindow[i]->moveToFront();
			}

			mMenubarImageMover->moveToFront();
			mMenubarImageResizer->moveToFront();

		
			/*** Try to capture the mouse input only to this window ***/
			mMenubarImageResizer->captureInput();


			/*** Start moving a window ***/
			const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
			mMovingWindow = windowEventArgs.window;
			mMovingEvent[0] = System::getSingleton().getGUISheet()->subscribeEvent(CEGUI::Window::EventMouseMove, Event::Subscriber(&InGameUIMenuBar::event_imageSizer_onWindowMove, this));
			mMousePosInWindow[0] = CoordConverter::screenToWindow(*mMovingWindow, MouseCursor::getSingleton().getPosition());
		}
		return false; // Do not propagate this event to other subscribers
	}
	return true;
}

// This function moves all the windows together;
bool InGameUIMenuBar::event_imageSizer_onWindowMove(const CEGUI::EventArgs& pEventArgs)
{
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);
	Vector2 localMousePos = CoordConverter::screenToWindow(*mMovingWindow, mouseEventArgs.position);
	UVector2 mouseMoveOffset(cegui_absdim(localMousePos.d_x), cegui_absdim(localMousePos.d_y));
	UVector2 mouseOffsetInWindow(cegui_absdim(mMousePosInWindow[0].d_x), cegui_absdim(mMousePosInWindow[0].d_y));
	/////mMovingWindow->setPosition(mMovingWindow->getPosition() + mouseMoveOffset - mouseOffsetInWindow);


	// Move the ImageResizer;
	mMenubarImageResizer->setPosition( mMenubarImageResizer->getPosition() +  mouseMoveOffset - mouseOffsetInWindow );


	// Get the ImageResizer position;
	UVector2 uv2_menubarImageResizer = mMenubarImageResizer->getPosition();
	

	// For setting the image widths;
	// we simply get the ImageResizer offset and subtract the first image offset from it, then -
	// - divide it by the number of buttons to get the image widths;
	float resizeOffset = uv2_menubarImageResizer.d_x.d_offset;
	float image0_Offset = mVpWindow[0]->getPosition().d_x.d_offset;
	float imageWidth = (resizeOffset - image0_Offset) / mNumImageButtons[0];	


	/*** Make the image size offset an integer, eitherwise there will be position problems ***/	
	UVector2 img0_size =  mVpWindow[0]->getSize();
	img0_size.d_x.d_offset = ( (int) imageWidth );


	/**********************************************/
	/*** Image0 - all Images use its Y position ***/
	/**********************************************/
	
	// Use this to set the Y position;
	mVpWindow[0]->setSize(mVpWindow[0]->getSize() + mouseMoveOffset - mouseOffsetInWindow);


	// ### This is important because its sets the width for all images;
	mVpWindow[0]->setWidth( img0_size.d_x );


	// int i = 1 because all sizes and positions are modeled after [0];
	for (int i = 1; i < mVpWindow.size(); i++)
	{
		// Set every windows to the size of image0
		mVpWindow[i]->setSize( mVpWindow[0]->getSize() );

		// Set the positions to the previous Image + image0 size;
		mVpWindow[i]->setPosition( (mVpWindow[i - 1]->getPosition() +  mVpWindow[0]->getSize()) ) ;

		// Set the Y position to the first image;	
		mVpWindow[i]->setYPosition( mVpWindow[0]->getYPosition() );
	}
	
	return true;
}


