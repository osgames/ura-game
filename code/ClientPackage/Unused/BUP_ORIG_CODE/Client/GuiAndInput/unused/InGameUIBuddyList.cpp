/*
Project Ura, an open source MMORPG.
Copyright (C) 2008  Denny Kunker

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InGameUIBuddyList.h"
#include "InGameUI.h"



	// The previousSliderValue starts at the top so its 1.0;
	InGameUIBuddyList::InGameUIBuddyList() : listBoxCurrentItem(0), previousSliderValue(1.0F),
			bThumbCalled_by_pushButton(false), bThumbCalled_by_pushButton_top(false)

	{

		/*** Set the buddylist text colors ***/

		// Text online color is Green;
		textColorOnline.push_back( 0.0F );
		textColorOnline.push_back( 1.0F );
		textColorOnline.push_back( 0.0F );

		// Text offline color is red;
		textColorOffline.push_back( 1.0F );
		textColorOffline.push_back( 0.0F );
		textColorOffline.push_back( 0.0F );
	}

	InGameUIBuddyList::~InGameUIBuddyList()
	{

	}

	void InGameUIBuddyList::setInGameUI( InGameUI *gameUI )
	{
		pInGameUI = gameUI;
	}

	void InGameUIBuddyList::prepareToQuit()
	{

	}

	void InGameUIBuddyList::toggle()
	{
/*
		if ( quitMenu->isVisible() == false )
		{
			quitMenu->show();
			quitMenu->activate();	
		}
		else if( quitMenu->isVisible() == true )
		{
			quitMenu->hide();	
		}
*/
	}
	
	bool InGameUIBuddyList::isVisible()
	{
///		return quitMenu->isVisible();
		return true;
	}
	

	void InGameUIBuddyList::deactivate()
	{

	}

	bool InGameUIBuddyList::refreshBuddyList(const CEGUI::EventArgs& pEventArgs)
	{
		refreshBuddyList();
		return true;
	}

	/*** This function is called to for resizing and to  prevent problems with the index and slider position; ***/
	bool InGameUIBuddyList::refreshBuddyList()
	{
		try
		{
			CEGUI::ListboxItem* tmpItem;
			CEGUI::ListboxItem* tmpItemSort;

			const unsigned int OnlineSortItemCount = listBoxOnlineSort->getItemCount();
			const unsigned int OfflineSortItemCount = listBoxOfflineSort->getItemCount();

			unsigned int titleBarHeight =  frameWindow->getTitlebar()->getPixelRect().getHeight();
			unsigned int frameHeight = frameWindow->getPixelRect().getHeight() - titleBarHeight;
			unsigned int listBoxHeight = frameHeight;

			unsigned int maxItemCount = OnlineSortItemCount + OfflineSortItemCount;
			unsigned int itemIndexCombinedHeigth = 0;
			unsigned int numItemsVisible = 0;

			unsigned int onLineCountNeeded = 0;
			unsigned int offLineCountNeeded = 0;

			unsigned int textPixelHeight = 0;

			try
			{
				// Just incase something goes wrong..we can still set the slider to the top and index to 0;
				// These two things will be a fail safe.
				slider->setCurrentValue(1.0f);	// Set the slider to the top with value 1.0;
				listBoxCurrentItem = 0;		// Let everything know the index is back to 0;


				// Get the number of items visible in the list by the listbox height / item pixel size;
				// We can use online or offline listbox to get the textitem height...doesn't matter;
				// What does matter is that there are items in the listboxes. So we have to check -
				// which one has items;

				if ( OnlineSortItemCount > 0 )
				{
					tmpItem = listBoxOnlineSort->getListboxItemFromIndex(0);

				}
				else if ( OfflineSortItemCount > 0 )
				{
					tmpItem = listBoxOfflineSort->getListboxItemFromIndex(0);
				}
				else	// If we have gotten here than there's no items in any listbox;
				{
					return false;
				}

				if ( tmpItem != NULL )		// Check that there is a item selected;
				{
					#ifdef INGAMEUIBUDDYLIST_DEBUG
						cout << "pixel size= " << tmpItem->getPixelSize().d_height << endl;
					#endif

					// Get the text pixel height;
					textPixelHeight = tmpItem->getPixelSize().d_height;
				}
			}
			catch(...)
			{
				cout << "InGameUIBuddyList::refreshBuddyList(): " <<
					"Error in: tmpItem = listBoxOnlineSort or listBoxOfflineSort->getListboxItemFromIndex(0);" << endl;

				return false;
			}
			

			// Get the number of items visible in the list by the listbox height / item pixel size;
			// We can use online or offline listbox to get the textitem height...doesn't matter;
			numItemsVisible = listBoxHeight / textPixelHeight;
	

			// Set the listbox height so that the edges of text ends are showing.
			listBox->setSize( CEGUI::UVector2( CEGUI::UDim( 0, 105 ), CEGUI::UDim( 0, numItemsVisible * textPixelHeight) ) );


			// The numItemsVisible is dependant on the listboxHeight. That becomes a problem when -
			//  -maxItemsCount height combined is less that the listbox height.
			// To fix this situation if maxitemsCount is less than the listbox height we -
			// - make numItemsVisible equal to the maxItemsCount;
			itemIndexCombinedHeigth = textPixelHeight * maxItemCount;
			if ( listBoxHeight > itemIndexCombinedHeigth )
			{
				numItemsVisible = maxItemCount;
			}


			#ifdef INGAMEUIBUDDYLIST_DEBUG
				cout << "numItemsVisible= " << numItemsVisible << endl;
				cout << "maxItemCount= " << maxItemCount << endl;
				cout << "listBoxCurrentItem=" << listBoxCurrentItem << endl;
				cout << endl;
			#endif


			/*****************************************************************************************/
			/*** Set everything back to normal, where the index is at 0, the slider is at the top. ***/
			/*** And  the listbox shows from index 0 to the number of visible items                ***/
			/*****************************************************************************************/

			// clear the listBox;
			listBox->resetList();

			// Set the slider to the top with value 1.0;
			slider->setCurrentValue(1.0f);

			// Let everything know the index is back to 0;
			listBoxCurrentItem = 0;


			/****************************************************************************/
			/*** Refresh the listbox starting from index 0 to number of visible items ***/
			/*** Make the online and offline listboxes appear as one.                 ***/
			/****************************************************************************/
			if ( numItemsVisible <= OnlineSortItemCount )	
			{

				for ( int i = 0; i < numItemsVisible; i++ )
				{
					try
					{	
						tmpItemSort = listBoxOnlineSort->getListboxItemFromIndex( i );
					}
					catch(...)
					{
						cout << "InGameUIBuddyList::refreshBuddyList(): Error in: " << 
							"(0) listBoxOnlineSort->getListboxItemFromIndex();" << endl;

						return false;
					}

					// Create the text item and set its text;
					item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

					// Set the text color;
					myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
					item->setTextColours(myColor);

					// Set the brush;
					item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );

					// Add the item;
					listBox->addItem( item );	
				}
			}
			else if ( numItemsVisible > OnlineSortItemCount )	
			{

				/*** First get the online items...then the offline items next ***/
				for ( int i = 0; i < OnlineSortItemCount; i++ )
				{
					try
					{	
						tmpItemSort = listBoxOnlineSort->getListboxItemFromIndex( i );
					}
					catch(...)
					{
						cout << "InGameUIBuddyList::refreshBuddyList(): Error in: " << 
							"(1) listBoxOnlineSort->getListboxItemFromIndex();" << endl;

						return false;
					}

					// Create the text item and set its text;
					item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

					// Set the text color;
					myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
					item->setTextColours(myColor);

					// Set the brush;
					item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );

					// Add the item;
					listBox->addItem( item );	
				}



				/*** Get the offline items ***/
				offLineCountNeeded = numItemsVisible - OnlineSortItemCount;

				for ( int i = 0; i < offLineCountNeeded; i++ )
				{
					try
					{	
						tmpItemSort = listBoxOfflineSort->getListboxItemFromIndex( i );
					}
					catch(...)
					{
						cout << "InGameUIBuddyList::refreshBuddyList(): Error in: " << 
							"(2) listBoxOfflineSort->getListboxItemFromIndex();" << endl;

						return false;
					}

					// Create the text item and set its text;
					item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

					// Set the text color;
					myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
					item->setTextColours(myColor);

					// Set the brush;
					item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );

					// Add the item;
					listBox->addItem( item );	
				}
			}
		}
		catch( ... )
		{
			cout << "InGameUIBuddyList::refreshBuddyList(): An unknown error occurred." << endl;
			return false;
		}

		return true;
	}


	bool InGameUIBuddyList::event_mousewheel(const CEGUI::EventArgs& pEventArgs)
	{
/*
		if (up)
		listBoxChangePosition( ENUM_TOP_PUSH_BUTTON );
		else if (down)
		listBoxChangePosition( ENUM_BOTTOM_PUSH_BUTTON );
*/

		return true;
	}

	// Were going down the list. The index increases as we go down;
	bool InGameUIBuddyList::event_PushButton_top(const CEGUI::EventArgs& pEventArgs)
	{
		listBoxChangePosition( ENUM_TOP_PUSH_BUTTON );
		return true;
	}

	bool InGameUIBuddyList::event_PushButton_bottom(const CEGUI::EventArgs& pEventArgs)
	{
		listBoxChangePosition( ENUM_BOTTOM_PUSH_BUTTON );
		return true;
	}



	bool InGameUIBuddyList::event_slider_thumbPositionChange(const CEGUI::EventArgs& pEventArgs)
	{
		listBoxChangePosition( ENUM_THUMB );
		return true;
	}



	/*********************************************************************************************************/
	/*** This function handles how the listbox scrolls and changes index. Everything uses this function... ***/
	/***  including the middle mouse button.                                                               ***/
	/*********************************************************************************************************/


	// If the bottom push button is clicked, then we will put the scrollbar at the closest percent
	//  down the listbox and increase the index. So we can view the next index. 
	// That way we scroll down by 1. But we have to be careful because when we
	// set the thumb of the slider an event is called. We want to bypass that, so we do a check in the beginning
	// of the function to see if the event should be skipped.
	// The rest should be pretty easy to figure out;
	bool InGameUIBuddyList::listBoxChangePosition(LISTBOX_EVENT_TYPE type)
	{
		try 
		{
			CEGUI::ListboxItem* tmpItem;
			CEGUI::ListboxItem* tmpItemSort;

			const unsigned int OnlineSortItemCount = listBoxOnlineSort->getItemCount();
			const unsigned int OfflineSortItemCount = listBoxOfflineSort->getItemCount();

			unsigned int titleBarHeight =  frameWindow->getTitlebar()->getPixelRect().getHeight();
			unsigned int frameHeight = frameWindow->getPixelRect().getHeight() - titleBarHeight;
			unsigned int listBoxHeight = frameHeight;

			unsigned int maxItemCount = OnlineSortItemCount + OfflineSortItemCount;
			unsigned int itemIndexCombinedHeigth = 0;

			unsigned int numItemsVisible = 0;
			float currentValue = slider->getCurrentValue();
			unsigned int indexesNeeded = 0;

			float firstIndexPercent = 0.0F;
			float closestPercentIndex = 0.0F;
			float tmpVal = 0.0F;

			unsigned int onLineCountNeeded = 0;
			unsigned int offLineCountNeeded = 0;

			unsigned int textPixelHeight = 0;


			listBox->setSize( CEGUI::UVector2( CEGUI::UDim( 0, 105 ), CEGUI::UDim( 0, frameHeight) ) );

			

			if ( bThumbCalled_by_pushButton == true )
			{
				bThumbCalled_by_pushButton = false;
				cout << "bThumbCalled_by_pushButton" << bThumbCalled_by_pushButton << endl;
				return false;
			}
			else if ( bThumbCalled_by_pushButton_top == true )
			{
				bThumbCalled_by_pushButton_top = false;
				cout << "bThumbCalled_by_pushButton_top" << bThumbCalled_by_pushButton_top << endl;
				return false;
			}


			try
			{
				// Get the number of items visible in the list by the listbox height / item pixel size;
				// We can use online or offline listbox to get the textitem height...doesn't matter;
				// What does matter is that there are items in the listboxes. So we have to check -
				// which one has items;

				if ( OnlineSortItemCount > 0 )
				{
					tmpItem = listBoxOnlineSort->getListboxItemFromIndex(0);

				}
				else if ( OfflineSortItemCount > 0 )
				{
					tmpItem = listBoxOfflineSort->getListboxItemFromIndex(0);
				}
				else	// If we have gotten here than there's no items in any listbox;
				{
					return false;
				}

				if ( tmpItem != NULL )		// Check that there is a item selected;
				{
					#ifdef INGAMEUIBUDDYLIST_DEBUG
						cout << "pixel size= " << tmpItem->getPixelSize().d_height << endl;
					#endif

					// Get the text pixel height;
					textPixelHeight = tmpItem->getPixelSize().d_height;
				}
			}
			catch(...)
			{
				cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): " <<
					"Error in: tmpItem = listBoxOnlineSort or listBoxOfflineSort->getListboxItemFromIndex(0);" << endl;

				return false;
			}
			

			// Get the number of items visible in the list by the listbox height / item pixel size;
			// We can use online or offline listbox to get the textitem height...doesn't matter;
			numItemsVisible = listBoxHeight / textPixelHeight;

			// Decrease numItemsVisible by 1. Taking out the horizonal scrollbar makes us do this;
			numItemsVisible--;

    
			// The numItemsVisible is dependant on the listboxHeight. That becomes a problem when -
			//  -maxItemsCount height combined is less that the listbox height.
			// To fix this situation if maxitemsCount is less than the listbox height we -
			// - make numItemsVisible equal to the maxItemsCount;
			itemIndexCombinedHeigth = textPixelHeight * maxItemCount;
			if ( listBoxHeight > itemIndexCombinedHeigth )
			{
				numItemsVisible = maxItemCount;

				return true;
			}


			// Get the number of indexes needed to see every item. Take into account the number of -
			// - visible items;
			indexesNeeded = maxItemCount - numItemsVisible;


			// Get the first index percent which is used to get the other percents;
			firstIndexPercent = 100.0F / indexesNeeded;
			
			// Get the percent we are closest to;
			closestPercentIndex = firstIndexPercent * ( indexesNeeded - listBoxCurrentItem );


			tmpVal = currentValue * 100.0F;


			#ifdef INGAMEUIBUDDYLIST_DEBUG
				cout << "listBoxHeight = " << listBoxHeight << endl;
				cout << "numItemsVisible= " << numItemsVisible << endl;
				cout << "maxItemCount= " << maxItemCount << endl;
				cout << "indexesNeeded= " << indexesNeeded << endl;
				cout << "firstIndexPercent= " << firstIndexPercent << endl;
				cout << "previousSliderValue= " << previousSliderValue << endl;
				cout << "currentValue= " << currentValue << endl;
				cout << "listBoxCurrentItem=" << listBoxCurrentItem << endl;
				cout << "closestPercentIndex= " << closestPercentIndex << endl;
				cout << "tmpVal= " << tmpVal << endl;
				cout << endl;
			#endif


			// What i did here in these "if and else if" statements is lets say....
			// If the bottom push button is clicked, then we will put the scrollbar at the closest percent
			//  down the listbox and increase the index. So we can view the next index. 
			// That way we scroll down by 1. But we have to be careful because when we
			// set the thumb of the slider an event is called. We want to bypass that so we do a check in the beginning
			// of the function to see if the event should be skipped.
			// The rest should be pretty easy to figure out;
			// oh and make sure to set the listBoxCurrentItem back because its changed again on the bottom;

			// Go down the listbox...increasing the index;
			if ( type == ENUM_BOTTOM_PUSH_BUTTON && listBoxCurrentItem < indexesNeeded )
			{
				listBoxCurrentItem++;
				closestPercentIndex = firstIndexPercent * ( indexesNeeded - listBoxCurrentItem );
				currentValue =  closestPercentIndex * 0.01F;
				
				bThumbCalled_by_pushButton = true;
				slider->setCurrentValue(closestPercentIndex * 0.01F);

				// Make sure to set the listBoxCurrentItem back because its changed again on the bottom;
				listBoxCurrentItem--;
			}
			// The goes up the listboxes...decreasing the index;
			else if ( type == ENUM_TOP_PUSH_BUTTON && listBoxCurrentItem >= 1 )
			{
				listBoxCurrentItem--;
				closestPercentIndex = firstIndexPercent * ( indexesNeeded - listBoxCurrentItem );
				currentValue =  closestPercentIndex * 0.01F;
				
				bThumbCalled_by_pushButton_top = true;
				slider->setCurrentValue(closestPercentIndex * 0.01F);

				// Make sure to set the listBoxCurrentItem back because its changed again on the bottom;
				listBoxCurrentItem++;
			}

			
			// Note: As the slider goes down it decreases with value and as the listBox item goes down -
			// its value increases. and the Opposite is true;
			if ( previousSliderValue > currentValue )
			{
				if ( tmpVal <= closestPercentIndex || (type == ENUM_BOTTOM_PUSH_BUTTON) )
				{
					// return if we have already seen the bottom of the listbox items; 
					if ( listBoxCurrentItem >= indexesNeeded )
					{
						return false;
					}


					// clear the listBox;
					listBox->resetList();
					


					#ifdef INGAMEUIBUDDYLIST_DEBUG
						cout << "INCREASE INSIDE CHECK" << endl;
						cout << endl;
					#endif


					// We let everything know that we increased an index.;
					// .....we go down the listbox and that increases the index;
					listBoxCurrentItem++;


					// If the slider is moved very fast down all the way it... doesn't go -
					// to the last index. Its some kinda cegui bug. Whell we can check if -
					// the slider is down all the way and if it is then set the listBoxcurrentItem -
					// to the indexes needed;
					if ( tmpVal <= 0.0F )
					{
						listBoxCurrentItem = indexesNeeded;
					}


 
					if ( listBoxCurrentItem < OnlineSortItemCount )		// Do we start on the online listbox?;
					{
						// Do we use both the online and offline lists?;
						if ( listBoxCurrentItem + numItemsVisible > OnlineSortItemCount )
						{
							// Get the values to figure out how to access both the online and offline-
							// -sorted listboxes to make it look as one whole.

							/*** First get the online items...then the offline items next ***/
							onLineCountNeeded = OnlineSortItemCount - listBoxCurrentItem;
							offLineCountNeeded = numItemsVisible - onLineCountNeeded;

							for ( int i = listBoxCurrentItem; i < listBoxCurrentItem + onLineCountNeeded; i++ )
							{
								try
								{	
									tmpItemSort = listBoxOnlineSort->getListboxItemFromIndex( i );
								}
								catch(...)
								{
									cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
										"(1) listBoxOnlineSort->getListboxItemFromIndex();" << endl;
		
									return false;
								}
		
								// Create the text item and set its text;
								item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

								// Set the text color;
								myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
								item->setTextColours(myColor);

								// Set the brush;
								item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
		
								// Add the item;
								listBox->addItem( item );	
							}


							/*** Get the offline items ***/
							for ( int i = 0; i < offLineCountNeeded; i++ )
							{
								try
								{	
									tmpItemSort = listBoxOfflineSort->getListboxItemFromIndex( i );
								}
								catch(...)
								{
									cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
										"(2) listBoxOfflineSort->getListboxItemFromIndex();" << endl;
		
									return false;
								}
		
								// Create the text item and set its text;
								item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

								// Set the text color;
								myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
								item->setTextColours(myColor);

								// Set the brush;
								item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
		
								// Add the item;
								listBox->addItem( item );	
							}
						}
						// Do we use just the online list?;
						else if ( listBoxCurrentItem + numItemsVisible <= OnlineSortItemCount )
						{
							onLineCountNeeded = listBoxCurrentItem + numItemsVisible;

							for ( int i = listBoxCurrentItem; i < onLineCountNeeded; i++ )
							{
								try
								{	
									tmpItemSort = listBoxOnlineSort->getListboxItemFromIndex( i );
								}
								catch(...)
								{
									cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
										"(3) listBoxOnlineSort->getListboxItemFromIndex();" << endl;
		
									return false;
								}
		
								// Create the text item and set its text;
								item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

								// Set the text color;
								myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
								item->setTextColours(myColor);

								// Set the brush;
								item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
		
								// Add the item;
								listBox->addItem( item );
							}
						}
					}
					else if ( listBoxCurrentItem >= OnlineSortItemCount )	// Do we start on the offline listbox?;
					{
						/*** Just use the offline list ***/
						offLineCountNeeded = listBoxCurrentItem - OnlineSortItemCount;

						for ( int i = offLineCountNeeded; i < (offLineCountNeeded + numItemsVisible); i++ )
						{
							try
							{	
								tmpItemSort = listBoxOfflineSort->getListboxItemFromIndex( i );
							}
							catch(...)
							{
								cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
									"(4)listBoxOfflineSort->getListboxItemFromIndex();" << endl;
	
								return false;
							}
	
							// Create the text item and set its text;
							item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );
	
							// Set the text color;
							myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
							item->setTextColours(myColor);

							// Set the brush;
							item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
	
							// Add the item;
							listBox->addItem( item );
						}
					}
				}
			}
			// Did we decrease the index and go up the listbox?;
			else if ( previousSliderValue < currentValue )
			{
				if ( tmpVal >= closestPercentIndex || (type == ENUM_TOP_PUSH_BUTTON) )
				{
					// return if we have already seen the bottom of the listbox items; 
					if ( listBoxCurrentItem <= 0 )
					{
						return false;
					}


					// clear the listBox;
					listBox->resetList();


					#ifdef INGAMEUIBUDDYLIST_DEBUG
						cout << "DECREASE INSIDE CHECK" << endl;
						cout << endl;
					#endif


					// Important to let everything know we decreased the index and went up -
					// - the listbox;
					listBoxCurrentItem--;
					

					// If the slider is moved very fast up all the way it... doesn't go -
					// to index 0. Its some kinda cegui bug. Whell we can check if -
					// the slider is up all the way and if it is then set the listBoxcurrentItem -
					// to index 0;
					if ( tmpVal >= 100.0F )
					{
						listBoxCurrentItem = 0;
					}



					if ( listBoxCurrentItem < OnlineSortItemCount )		// Do we start on the online listbox?;
					{
						// Do we use both the online and offline lists?;
						if ( listBoxCurrentItem + numItemsVisible > OnlineSortItemCount )
						{
							// Get the values to figure out how to access both the online and offline-
							// -sorted listboxes to make it look as one whole.

							/*** First get the online items...then the offline items next ***/
							onLineCountNeeded = OnlineSortItemCount - listBoxCurrentItem;
							offLineCountNeeded = numItemsVisible - onLineCountNeeded;

							for ( int i = listBoxCurrentItem; i < listBoxCurrentItem + onLineCountNeeded; i++ )
							{
								try
								{	
									tmpItemSort = listBoxOnlineSort->getListboxItemFromIndex( i );
								}
								catch(...)
								{
									cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
										"(1) listBoxOnlineSort->getListboxItemFromIndex();" << endl;
		
									return false;
								}
		
								// Create the text item and set its text;
								item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

								// Set the text color;
								myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
								item->setTextColours(myColor);

								// Set the brush;
								item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
		
								// Add the item;
								listBox->addItem( item );	
							}


							/*** Get the offline items ***/
							for ( int i = 0; i < offLineCountNeeded; i++ )
							{
								try
								{	
									tmpItemSort = listBoxOfflineSort->getListboxItemFromIndex( i );
								}
								catch(...)
								{
									cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
										"(2) listBoxOfflineSort->getListboxItemFromIndex();" << endl;
		
									return false;
								}
		
								// Create the text item and set its text;
								item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

								// Set the text color;
								myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
								item->setTextColours(myColor);

								// Set the brush;
								item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
		
								// Add the item;
								listBox->addItem( item );	
							}
						}
						// Do we use just the online list?;
						else if ( listBoxCurrentItem + numItemsVisible <= OnlineSortItemCount )
						{
							onLineCountNeeded = listBoxCurrentItem + numItemsVisible;

							for ( int i = listBoxCurrentItem; i < onLineCountNeeded; i++ )
							{
								try
								{	
									tmpItemSort = listBoxOnlineSort->getListboxItemFromIndex( i );
								}
								catch(...)
								{
									cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
										"(3) listBoxOnlineSort->getListboxItemFromIndex();" << endl;
		
									return false;
								}
		
								// Create the text item and set its text;
								item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );

								// Set the text color;
								myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
								item->setTextColours(myColor);

								// Set the brush;
								item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
		
								// Add the item;
								listBox->addItem( item );
							}
						}
					}
					else if ( listBoxCurrentItem >= OnlineSortItemCount )	// Do we start on the offline listbox?;
					{
						/*** Just use the offline list ***/
						offLineCountNeeded = listBoxCurrentItem - OnlineSortItemCount;

						for ( int i = offLineCountNeeded; i < (offLineCountNeeded + numItemsVisible); i++ )
						{
							try
							{	
								tmpItemSort = listBoxOfflineSort->getListboxItemFromIndex( i );
							}
							catch(...)
							{
								cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): Error in: " << 
									"(4)listBoxOfflineSort->getListboxItemFromIndex();" << endl;
	
								return false;
							}
	
							// Create the text item and set its text;
							item = new CEGUI::ListboxTextItem( tmpItemSort->getText() );
	
							// Set the text color;
							myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
							item->setTextColours(myColor);

							// Set the brush;
							item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
	
							// Add the item;
							listBox->addItem( item );
						}
					}
				}
			}
				

			// Save the previous Value;
			previousSliderValue = currentValue;
		}
		catch( ... )
		{
			cout << "InGameUIBuddyList::event_slider_thumbPositionChange(): An unknown error occurred." << endl;
			return false;
		}



		return true;
	}

	bool InGameUIBuddyList::event_add_button(const CEGUI::EventArgs& pEventArgs)
	{
		/*** Show the dialog window to add a name ***/
		menuAddName->show();
		menuAddNameEditbox->activate();

		return true;
	}

	bool InGameUIBuddyList::event_edit_button(const CEGUI::EventArgs& pEventArgs)
	{
		return true;
	}

	bool InGameUIBuddyList::event_remove_button(const CEGUI::EventArgs& pEventArgs)
	{
		CEGUI::String ceStr;
		CEGUI::ListboxItem* tmpItem;


		// Get the text from the editbox;
		// If there is no item selected than it causes an exception.
		// Catch the exception so the program doesn't exit;
		try
		{
			// If we don't have atleast 1 selection than return false;
			if ( listBox->getSelectedCount() < 1 )
			{
				return false;
			}

			tmpItem = listBox->getFirstSelectedItem();
		}
		catch ( ... )
		{
			#ifdef INGAMEUIBUDDYLIST_DEBUG
				cout << "InGameUIBuddyList::event_remove_button():" <<
					" Error getting listBox->getFirstSelectedItem()->getText();" << endl;
			#endif

			return false;
		}


		ceStr = tmpItem->getText();
		string s( ceStr.c_str() );

		// If the string is blank then hide the window and return false;
		if ( s == "" )
		{
			// hide the window now;
			menuRemoveName->hide();	

			return false;
		}


		// Show the dialog window to remove a name;
		menuRemoveName->show();
		menuRemoveName->activate();
		
		// Disable input to the editbox;
		menuRemoveNameEditbox->setEnabled(false);

		// Set the editbox text to the selected name;
		menuRemoveNameEditbox->setText(s);


		return true;
	}



	/**************************/
	/*** ADD NAME FUNCTIONS ***/
	/**************************/

	bool InGameUIBuddyList::event_menuAddName_ButtonNo(const CEGUI::EventArgs& pEventArgs)
	{
		// hide the menuAddName window because we chose to not add a buddy;
		menuAddName->hide();

		// clear the editbox;
		menuAddNameEditbox->setText("");

		return true;
	}

	bool InGameUIBuddyList::event_menuAddName_ButtonYes(const CEGUI::EventArgs& pEventArgs)
	{
		// Get the text from the editbox;
		CEGUI::String ceStr = menuAddNameEditbox->getText();
		string s( ceStr.c_str() );


		// If the string is blank then hide the window and return false;
		if ( s == "" )
		{
			// hide the window now;
			menuAddName->hide();	

			return false;
		}


		// Call the function to add the name;
		// For now we set the state to always offline;
		addPlayer(s, 0);

		
		// hide the window now;
		menuAddName->hide();

		// clear the editbox;
		menuAddNameEditbox->setText("");

		// Refresh the buddy list by setting the slider at the top and the index to 0;
		refreshBuddyList();


		return true;
	}




	/*****************************/
	/*** REMOVE NAME FUNCTIONS ***/
	/*****************************/

	bool InGameUIBuddyList::event_menuRemoveName_ButtonNo(const CEGUI::EventArgs& pEventArgs)
	{
		// hide the menuAddName window because we chose to not add a buddy;
		menuRemoveName->hide();

		// clear the editbox;
		menuRemoveNameEditbox->setText("");

		return true;
	}

	bool InGameUIBuddyList::event_menuRemoveName_ButtonYes(const CEGUI::EventArgs& pEventArgs)
	{
		CEGUI::String ceStr;
		CEGUI::ListboxItem* tmpItem;

		// Get the text from the editbox;
		// If there is no item selected than it causes an exception.
		// Catch the exception so the program doesn't exit;
		try
		{
			// If we don't have atleast 1 selection than return false;
			if ( listBox->getSelectedCount() < 1 )
			{
				return false;
			}

			tmpItem = listBox->getFirstSelectedItem();
		}
		catch ( ... )
		{
			#ifdef INGAMEUIBUDDYLIST_DEBUG
				cout << "InGameUIBuddyList::event_menuRemoveName_ButtonYes():" <<
					" Error getting listBox->getFirstSelectedItem()->getText();" << endl;
			#endif

			return false;
		}


		ceStr = tmpItem->getText();
		string s( ceStr.c_str() );

	
		// If the string is blank then hide the window and return false;
		if ( s == "" )
		{
			// hide the window now;
			menuRemoveName->hide();	

			return false;
		}

		// Call the function to Remove the name;
		// For now we set the state to always offline;
		removePlayer(s);

		
		// hide the window now;
		menuRemoveName->hide();

		// clear the editbox;
		menuRemoveNameEditbox->setText("");

		// Refresh the buddy list by setting the slider at the top and the index to 0;
		refreshBuddyList();


		return true;
	}


	bool InGameUIBuddyList::testme_loadListBox()
	{
		vector<string> s;

		// clear the listBoxes;
		listBox->resetList();
		listBoxOnlineSort->resetList();
		listBoxOfflineSort->resetList();


		s.push_back("hoy");
		s.push_back("foy");
		s.push_back("doy");
		s.push_back("goy");
		s.push_back("eoy");
		s.push_back("boy");
		s.push_back("coy");
		s.push_back("aoy");

		s.push_back("kjjlkljklj");
		s.push_back("jklggfhjf");
		s.push_back("fggddyt");
		s.push_back("dhfdghdgd");
		s.push_back("fadghfdgghdyfdoy");
		s.push_back("trtr");
		s.push_back("dghdhd");
		

		s.push_back("jh");
		s.push_back("jkgh");
		s.push_back("hghjgdoy");
		s.push_back("ggggfoy");
		s.push_back("ehggoy");
		s.push_back("bgjghjhjoy");
		s.push_back("chghghjoy");
		s.push_back("akhjkhoy");

		s.push_back("kjjftfghlkljklj");
		s.push_back("jkghfghflggfhjf");
		s.push_back("fgdhdugddyt");
		s.push_back("dhfuydghdgd");
		s.push_back("fafgfgdghfdgghdyfdoy");
		s.push_back("gfgf");
		s.push_back("dgfgffhdhd");
		s.push_back("ghjgfjgfghf");


		addPlayer(s[0], 1);
		addPlayer(s[1], 1);
		addPlayer(s[2], 1);
		addPlayer(s[3], 1);
		addPlayer(s[4], 1);
		addPlayer(s[5], 1);
		addPlayer(s[6], 1);
		addPlayer(s[7], 1);

		addPlayer(s[8], 1);
		addPlayer(s[9], 1);

		addPlayer(s[10], 0);
		addPlayer(s[11], 0);
		addPlayer(s[12], 0);
		addPlayer(s[13], 0);
		addPlayer(s[14], 0);
		
		addPlayer(s[15], 0);
		addPlayer(s[16], 0);
		addPlayer(s[17], 0);
		addPlayer(s[18], 0);
		addPlayer(s[19], 0);
	/***
		addPlayer(s[20], 0);
		addPlayer(s[21], 1);
		addPlayer(s[22], 1);

		addPlayer(s[23], 0);
		addPlayer(s[24], 1);
		addPlayer(s[25], 0);
		addPlayer(s[26], 1);
		addPlayer(s[27], 0);
		addPlayer(s[28], 0);
		addPlayer(s[29], 1);
	***/

		refreshBuddyList();


		return true;
	}

	bool InGameUIBuddyList::testme_leftclick_online (const CEGUI::EventArgs& pEventArgs)
	{
		const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);


		if(mouseEventArgs.button == CEGUI::LeftButton)
		{
			CEGUI::ListboxItem* tmpItem;
	
			try
			{
				tmpItem = listBox->getFirstSelectedItem();
				if ( tmpItem != NULL )		// Check that there is a item selected;
				{
					string str( tmpItem->getText().c_str() );
	
					setPlayerOnline( str );
				}
			}
			catch(...)
			{
				cout << "InGameUIBuddyList::testme_leftclick_online(): An unknown error occured." << endl;
			}
		}

		
		#ifdef INGAMEUIBUDDYLIST_DEBUG
			CEGUI::Scrollbar* listboxScrollbar = listBox->getVertScrollbar();
			cout << "scrollPOS= " << listboxScrollbar->getScrollPosition() << endl;
		#endif

		return true;
	}

	bool InGameUIBuddyList::testme_remove_and_offline(const CEGUI::EventArgs& pEventArgs)
	{
		const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);


		if(mouseEventArgs.button == CEGUI::MiddleButton)
		{
			CEGUI::ListboxItem* tmpItem;
	
			try
			{
				tmpItem = listBox->getFirstSelectedItem();
				if ( tmpItem != NULL )		// Check that there is a item selected;
				{
					string str( tmpItem->getText().c_str() );
	
					removePlayer( str );
				}
			}
			catch(...)
			{
				cout << "InGameUIBuddyList::testme_remove_and_offline(): An unknown error occured. " << endl;
			}
		}
		else if(mouseEventArgs.button == CEGUI::RightButton)
		{
			CEGUI::ListboxItem* tmpItem;
	
			try
			{
				tmpItem = listBox->getFirstSelectedItem();
				if ( tmpItem != NULL )		// Check that there is a item selected;
				{
					string str( tmpItem->getText().c_str() );
	
					setPlayerOffline( str );
				}
			}
			catch(...)
			{
				cout << "InGameUIBuddyList::testme_remove_and_offline(): An unknown error occured. " << endl;
			}
		}

		return true;
	}

	void InGameUIBuddyList::removePlayer(const string &name)
	{
		CEGUI::ListboxItem* tmpItem;

		try
		{
			// If listBox has the name then remove it;
			//tmpItem = listBox->findItemWithText( name, NULL);
			//if ( tmpItem != NULL)	listBox->removeItem( tmpItem );
				
	
			// If listBoxOnlineSort has the name then remove it;
			tmpItem = listBoxOnlineSort->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBoxOnlineSort->removeItem( tmpItem );
			
	
			// If listBoxOfflineSort has the name then remove it;
			tmpItem = listBoxOfflineSort->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBoxOfflineSort->removeItem( tmpItem );


			// Refresh the buddylist;
			refreshBuddyList();
		}
		catch( ... )
		{
			cout << "InGameUIBuddyList::removePlayer(): An unknown error occured." << endl;
		}
	}

	bool InGameUIBuddyList::addPlayer(const string &name, const bool state)	// state = 0 for offline and 1 for online;
	{
		unsigned int count = 0;
		CEGUI::ListboxItem* tmpItem;
		int i = 0;

		try
		{
			item = new CEGUI::ListboxTextItem( name );
		
	
			// If we have a duplicate than return false;
			if ( listBox->findItemWithText( name, NULL) != NULL)	return false;
			if ( listBoxOnlineSort->findItemWithText( name, NULL) != NULL)	return false;
			if ( listBoxOfflineSort->findItemWithText( name, NULL) != NULL)	return false;
	
			// If the person is online we add an item to listBoxOnlineSort else their offline and -
			// - we add an item to listBoxOfflineSort;
			if ( state )	// Is the person online?;
			{
				listBoxOnlineSort->addItem( item );
			}
			else		// The person is not online;
			{
				listBoxOfflineSort->addItem( item );
			}
	
			// Refresh the buddylist;
			refreshBuddyList();	
		}
		catch(...)
		{
			cout << "InGameUIBuddyList::addPlayer(): An unknown error Occured." << endl;

			return false;
		}



		return true;
	}


	bool InGameUIBuddyList::setPlayerOnline(const string &name)
	{
		unsigned int count = 0;
		CEGUI::ListboxItem* tmpItem;
		int i = 0;
		

		try
		{
			/*** Remove the name from every listbox ***/
	
			// If listBox has the name then remove it;
			tmpItem = listBox->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBox->removeItem( tmpItem );
				
	
			// If listBoxOnlineSort has the name then remove it;
			tmpItem = listBoxOnlineSort->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBoxOnlineSort->removeItem( tmpItem );
			
	
			// If listBoxOfflineSort has the name then remove it;
			tmpItem = listBoxOfflineSort->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBoxOfflineSort->removeItem( tmpItem );
	
	
	
			// clear the listBox;
			listBox->resetList();
	
	
			/***  Add the name to listBoxOnlineSort, set the text color for online and the brush ***/
			item = new CEGUI::ListboxTextItem( name );
			myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
			item->setTextColours(myColor);
			item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
			listBoxOnlineSort->addItem( item );
	
	
			/*** Add the listBoxOnlineSort sorted text to listBox, along with the colored online text ***/
	
			count = listBoxOnlineSort->getItemCount();
			
			for (i=0; i < count; i++)
			{
				try
				{
					tmpItem = listBoxOnlineSort->getListboxItemFromIndex(i);
				}
				catch(...)
				{
					cout << "InGameUIBuddyList::setPlayerOnline(): Error in: listBoxOnlineSort->getListboxItemFromIndex(0);" << endl;
					return false;
				}

				item = new CEGUI::ListboxTextItem( tmpItem->getText() );
	
				// Set the text color;
				myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
				item->setTextColours(myColor);
				
				// Add the item to the listBox;
				listBox->addItem( item );
	
				item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
			}
			
	
			/*** Add the listBoxOfflineSort sorted text to listBox, along with the colored offline text ***/
	
			count = listBoxOfflineSort->getItemCount();
			
			for (i=0; i < count; i++)
			{
				try
				{
					tmpItem = listBoxOfflineSort->getListboxItemFromIndex(i);
				}
				catch(...)
				{
					cout << "InGameUIBuddyList::setPlayerOnline(): Error in: listBoxOfflineSort->getListboxItemFromIndex(0);" << endl;
					return false;
				}

				item = new CEGUI::ListboxTextItem( tmpItem->getText() );
	
				// Set the text color;
				myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
				item->setTextColours(myColor);
				
				// Add the item to the listBox;
				listBox->addItem( item );
	
				item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
			}
		}
		catch(...)
		{
			cout << "InGameUIBuddyList::setPlayerOnline(): An unknown error Occured." << endl;

			return false;
		}

		return true;
	}

	bool InGameUIBuddyList::setPlayerOffline(const string &name)
	{
		unsigned int count = 0;
		CEGUI::ListboxItem* tmpItem;
		int i = 0;
		

		try
		{
			/*** Remove the name from every listbox ***/
	
			// If listBox has the name then remove it;
			tmpItem = listBox->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBox->removeItem( tmpItem );
				
	
			// If listBoxOnlineSort has the name then remove it;
			tmpItem = listBoxOnlineSort->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBoxOnlineSort->removeItem( tmpItem );
			
	
			// If listBoxOfflineSort has the name then remove it;
			tmpItem = listBoxOfflineSort->findItemWithText( name, NULL);
			if ( tmpItem != NULL)	listBoxOfflineSort->removeItem( tmpItem );
	
	
	
			// clear the listBox;
			listBox->resetList();
	
	
			/*** Add the listBoxOnlineSort sorted text to listBox, along with the colored online text ***/
	
			count = listBoxOnlineSort->getItemCount();
			
			for (i=0; i < count; i++)
			{
				try
				{
					tmpItem = listBoxOnlineSort->getListboxItemFromIndex(i);
				}
				catch(...)
				{
					cout << "InGameUIBuddyList::setPlayerOffline(): Error in: listBoxOnlineSort->getListboxItemFromIndex(0);" << endl;
					return false;
				}

				item = new CEGUI::ListboxTextItem( tmpItem->getText() );
	
				// Set the text color;
				myColor.setRGB( textColorOnline[0], textColorOnline[1], textColorOnline[2] );
				item->setTextColours(myColor);
				
				// Add the item to the listBox;
				listBox->addItem( item );
	
				item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
			}
			
	

			/***  Add the name to listBoxOfflineSort, set the text color for online and the brush ***/
			item = new CEGUI::ListboxTextItem( name );
			myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
			item->setTextColours(myColor);
			item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
			listBoxOfflineSort->addItem( item );



			/*** Add the listBoxOfflineSort sorted text to listBox, along with the colored offline text ***/
	
			count = listBoxOfflineSort->getItemCount();
			
			for (i=0; i < count; i++)
			{
				try
				{
					tmpItem = listBoxOfflineSort->getListboxItemFromIndex(i);
				}
				catch(...)
				{
					cout << "InGameUIBuddyList::setPlayerOffline(): Error in: listBoxOfflineSort->getListboxItemFromIndex(0);" << endl;
					return false;
				}

				item = new CEGUI::ListboxTextItem( tmpItem->getText() );
	
				// Set the text color;
				myColor.setRGB( textColorOffline[0], textColorOffline[1], textColorOffline[2] );
				item->setTextColours(myColor);
				
				// Add the item to the listBox;
				listBox->addItem( item );
	
				item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );
			}
		}
		catch(...)
		{
			cout << "InGameUIBuddyList::setPlayerOffline(): An unknown error Occured." << endl;

			return false;
		}

		return true;
	}

bool InGameUIBuddyList::addChildWindow( CEGUI::SchemeManager* schemeManager, CEGUI::ImagesetManager* imgSetManager, 
				CEGUI::WindowManager* winManager, CEGUI::Window* window, bool bLayoutSuccess)
{
	// Set the class pointers;
	windowManager = winManager;
	sheet = window;
	imageSetManager = imgSetManager;
	schemeMgr = schemeManager;


	// Set Add, edit, remove button properties;
	float buttonXPos = 0.065;
	float buttonYpos = 0.085;
	float buttonCushion = 0.025;
	float buttonXsize = 0.24;
	float buttonYsize = 0.60;



	try
	{
		// If bLayoutSuccess is false then we create hardcoded settings and events. 
		// Else if its true then the layout succeded in loading, we just have to make events for it; 
		if ( bLayoutSuccess == false )
		{
			/**************************/
			/*** Create the widgets ***/
			/**************************/
	
	
			// This sets the buddylist width and height;
			// The frameWindow is determines the width and the sideImage determines the height;
			float main_width = 0.15F;
			float main_height = 0.2F;
	
	
			/***  Connect the frame to the topImage ***/
			float frameWindow_x_size = 0.15F;
			float frameWindow_y_size = 0.3F;
			float frameWindow_x_pos = 0.8F;
			float frameWindow_y_pos = 0.05F;
	
			float topImage_x_size = frameWindow_x_size;
			float topImage_y_size = 0.04F;
			float topImage_x_pos = frameWindow_x_pos;
			float topImage_y_pos = frameWindow_y_pos + frameWindow_y_size;
		
	
			/*** later, Connect the sideImage to the topImage ***/
			float sideImage_x_size = frameWindow_x_size;
			float sideImage_y_size = main_height;
	
			/*** later, Connect the bottomImage to the sideImage ***/
			float bottomImage_x_size = frameWindow_x_size;
			float bottomImage_y_size = 0.05F;
	
		




			/*** The listBox settings ***/
			float listBox_x_size =0.7F;
			float listBox_x_pos = 0.15F;
			float listBox_y_pos = 0.15F;


			/*** The slider push button settings ***/
			float pushButton_x_size = 0.1F;
			float pushButton_y_size = 0.1F;
			float pushBotton_x_pos = 0.85F;
			float top_pushBotton_y_pos = 0.3F;
			float bottom_pushBotton_y_pos = 0.75F;
	
	
			/*** The slider settings ***/
			float slider_x_size = 0.075F;
			float slider_y_size = 0.76F;
			float slider_x_pos = pushBotton_x_pos;
			float slider_y_pos = 0.0F;
	

	
			/***************************************/
			/*** Make the Frame title bar window ***/
			/***************************************/
			frameWindow = (CEGUI::FrameWindow*) winManager->createWindow(
				"TaharezLook/FrameWindow", "InGameUI_InGameUIBuddyList_BuddyListWindow");
			frameWindow->setSize(CEGUI::UVector2(CEGUI::UDim(frameWindow_x_size, 0), CEGUI::UDim(frameWindow_y_size, 0)));
			frameWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(frameWindow_x_pos, 0), CEGUI::UDim(frameWindow_y_pos, 0) ) );
			frameWindow->setText("Buddy List");
	
	

	

	
			int titleBarHeight =  frameWindow->getTitlebar()->getPixelRect().getHeight();
			//int frameHeight = frameWindow->getPixelRect().getHeight() - frameWindow->getTitlebar()->getPixelRect().getHeight();

			// listbox;
			listBox = (CEGUI::Listbox *) winManager->createWindow(
				"TaharezLook/listbox_transparent_background_buddyList_invisibleNoEventsScrollbar", "InGameUI_InGameUIBuddyList_listBox");

			//listBox = (CEGUI::Listbox *) winManager->createWindow(
			//	"TaharezLook/Listbox", "InGameUI_InGameUIBuddyList_listBox");


			//listBox->setSize(CEGUI::UVector2(CEGUI::UDim( listBox_x_size, 0), CEGUI::UDim(1, 0) ) );
			//listBox->setPosition( CEGUI::UVector2(CEGUI::UDim(listBox_x_pos, 0), CEGUI::UDim(0.3, 0) ) );
	
	
			// Position a window to pixel coordinates (10,20)
			listBox->setPosition( CEGUI::UVector2( CEGUI::UDim( 0, 4), CEGUI::UDim( 0, titleBarHeight ) ) );
			
			// Set window size to 150 x 200 pixels
			listBox->setSize( CEGUI::UVector2( CEGUI::UDim( 0, 100 ), CEGUI::UDim( 0, 99) ) ); 






			// listBoxOnlineSort;
			listBoxOnlineSort = (CEGUI::Listbox *) winManager->createWindow("TaharezLook/listbox_transparent_background_scrollbar1",
					"InGameUI_InGameUIBuddyList_listBoxOnlineSort");
			listBoxOnlineSort->setSize(CEGUI::UVector2(CEGUI::UDim( 0.87, 0), CEGUI::UDim(0.7, 0) ) );
			listBoxOnlineSort->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.155, 0) ) );
	
	
			// listBoxOfflineSort;
			listBoxOfflineSort = (CEGUI::Listbox *) winManager->createWindow("TaharezLook/listbox_transparent_background_scrollbar1",
					"InGameUI_InGameUIBuddyList_listBoxOfflineSort");
			listBoxOfflineSort->setSize(CEGUI::UVector2(CEGUI::UDim( 0.87, 0), CEGUI::UDim(0.7, 0) ) );
			listBoxOfflineSort->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.155, 0) ) );
	
	
			// slider;
			slider = (CEGUI::Slider *)winManager->createWindow("TaharezLook/buddyList_Slider",
					"InGameUI_InGameUIBuddyList_buddyList_slider");
			slider->setSize(CEGUI::UVector2(CEGUI::UDim( slider_x_size, 0), CEGUI::UDim(slider_y_size, 0) ) );
			slider->setPosition( CEGUI::UVector2(CEGUI::UDim(slider_x_pos, 0), CEGUI::UDim(slider_y_pos, 0) ) );
	
	
	
			// The scrollbar top and bottom push buttons	
			slider_scrollbar_pushButton_top = (CEGUI::PushButton*) winManager->createWindow(
				"TaharezLook/buddyList_scrollBar_pushButton_top", "InGameUI_InGameUIBuddyList_ScrollBar_PushButton_Top");
			slider_scrollbar_pushButton_top->setSize(CEGUI::UVector2(CEGUI::UDim(pushButton_x_size, 0), CEGUI::UDim(pushButton_y_size, 0)));
			slider_scrollbar_pushButton_top->setPosition( CEGUI::UVector2(CEGUI::UDim(pushBotton_x_pos, 0), CEGUI::UDim(top_pushBotton_y_pos, 0) ) );



			slider_scrollbar_pushButton_bottom = (CEGUI::PushButton*) winManager->createWindow(
				"TaharezLook/buddyList_scrollBar_pushButton_bottom", "InGameUI_InGameUIBuddyList_ScrollBar_PushButton_Bottom");
			slider_scrollbar_pushButton_bottom->setSize(CEGUI::UVector2(CEGUI::UDim(pushButton_x_size, 0), CEGUI::UDim(pushButton_y_size, 0)));
			slider_scrollbar_pushButton_bottom->setPosition( CEGUI::UVector2(CEGUI::UDim(pushBotton_x_pos, 0), CEGUI::UDim(bottom_pushBotton_y_pos, 0) ) );
	
	
	
			// buttonAdd;
			buttonAdd = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_buttonAdd");
			buttonAdd->setText("Add");
			buttonAdd->setSize(CEGUI::UVector2(CEGUI::UDim(buttonXsize, 0), CEGUI::UDim(buttonYsize, 0)));
			buttonAdd->setPosition( CEGUI::UVector2(CEGUI::UDim(buttonXPos, 0), CEGUI::UDim(buttonYpos, 0) ) );
			
			buttonXPos += buttonXsize + buttonCushion;
	
			// buttonEdit;
			buttonEdit = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_buttonEdit");
			buttonEdit->setText("Edit");
			buttonEdit->setSize(CEGUI::UVector2(CEGUI::UDim(buttonXsize, 0), CEGUI::UDim(buttonYsize, 0)));
			buttonEdit->setPosition( CEGUI::UVector2(CEGUI::UDim(buttonXPos, 0), CEGUI::UDim(buttonYpos, 0) ) );
	
			buttonXPos += buttonXsize + buttonCushion;
			buttonXsize += 0.1;
	
			// buttonRemove;
			buttonRemove = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_buttonRemove");
			buttonRemove->setText("Remove");
			buttonRemove->setSize(CEGUI::UVector2(CEGUI::UDim(buttonXsize, 0), CEGUI::UDim(buttonYsize, 0)));
			buttonRemove->setPosition( CEGUI::UVector2(CEGUI::UDim(buttonXPos, 0), CEGUI::UDim(buttonYpos, 0) ) );
			
	
			/***  Create the menuAddName for adding names to the listBox ***/ 
			menuAddName = (CEGUI::FrameWindow*) winManager->createWindow("TaharezLook/FrameWindow", "InGameUI_InGameUIBuddyList_menuAddName");
			menuAddName->setSize(CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.15, 0)));
			menuAddName->setText("Add Name:");
			// Set the position to the middle of the screen;
			menuAddName->setPosition( CEGUI::UVector2(CEGUI::UDim(0.38, 0), CEGUI::UDim(0.35, 0) ) );	
	
			// Create a editbox to input the name;
			menuAddNameEditbox = (CEGUI::Editbox *) winManager->createWindow("TaharezLook/Editbox", "InGameUI_InGameUIBuddyList_menuAddNameEditbox");
			menuAddNameEditbox->setSize(CEGUI::UVector2(CEGUI::UDim(0.8, 0), CEGUI::UDim(0.24, 0)));
			menuAddNameEditbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.3, 0) ) );
	
			// Create a "yes" button;
			menuAddName_ButtonYes = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_menuAddName_ButtonYes");
			menuAddName_ButtonYes->setText("Yes");
			menuAddName_ButtonYes->setSize(CEGUI::UVector2(CEGUI::UDim(0.35, 0), CEGUI::UDim(0.2, 0)));
			menuAddName_ButtonYes->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.65, 0) ) );
	
			// Create a "No" button;
			menuAddName_ButtonNo = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_menuAddName_ButtonNo");
			menuAddName_ButtonNo->setText("No");
			menuAddName_ButtonNo->setSize(CEGUI::UVector2(CEGUI::UDim(0.35, 0), CEGUI::UDim(0.2, 0)));
			menuAddName_ButtonNo->setPosition( CEGUI::UVector2(CEGUI::UDim(0.55, 0), CEGUI::UDim(0.65, 0) ) );
	
	
	
			/***  Create the menuRemoveName for removing names from the listBox ***/ 
			menuRemoveName = (CEGUI::FrameWindow*) winManager->createWindow("TaharezLook/FrameWindow", "InGameUI_InGameUIBuddyList_menuRemoveName");
			menuRemoveName->setSize(CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.15, 0)));
			menuRemoveName->setText("Remove Name:");
			// Set the position to the middle of the screen;
			menuRemoveName->setPosition( CEGUI::UVector2(CEGUI::UDim(0.38, 0), CEGUI::UDim(0.35, 0) ) );	
	
			// Create a editbox to input the name;
			menuRemoveNameEditbox = (CEGUI::Editbox *) winManager->createWindow("TaharezLook/Editbox",
					"InGameUI_InGameUIBuddyList_menuRemoveNameEditbox");
			menuRemoveNameEditbox->setSize(CEGUI::UVector2(CEGUI::UDim(0.8, 0), CEGUI::UDim(0.24, 0)));
			menuRemoveNameEditbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.3, 0) ) );
	
			// Create a "yes" button;
			menuRemoveName_ButtonYes = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_menuRemoveName_ButtonYes");
			menuRemoveName_ButtonYes->setText("Yes");
			menuRemoveName_ButtonYes->setSize(CEGUI::UVector2(CEGUI::UDim(0.35, 0), CEGUI::UDim(0.2, 0)));
			menuRemoveName_ButtonYes->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.65, 0) ) );
	
			// Create a "No" button;
			menuRemoveName_ButtonNo = (CEGUI::PushButton*) winManager->createWindow("TaharezLook/Button", "InGameUI_InGameUIBuddyList_menuRemoveName_ButtonNo");
			menuRemoveName_ButtonNo->setText("No");
			menuRemoveName_ButtonNo->setSize(CEGUI::UVector2(CEGUI::UDim(0.35, 0), CEGUI::UDim(0.2, 0)));
			menuRemoveName_ButtonNo->setPosition( CEGUI::UVector2(CEGUI::UDim(0.55, 0), CEGUI::UDim(0.65, 0) ) );
	
	
			/*****************/
			/*** For Tests ***/
			/*****************/
			listBox->setSortingEnabled(false);
			listBoxOnlineSort->setSortingEnabled(true);
			listBoxOfflineSort->setSortingEnabled(true);
			CEGUI::EventArgs pEventArgs;
			testme_loadListBox();
	
	
			/****************/
			/*** Events   ***/
			/****************/
	
	
			//))))))frameWindow->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(
					///// &InGameUIBuddyList::testme_remove_and_offline, this ) );
			//))))))frameWindow->subscribeEvent( CEGUI::Window::EventMouseDoubleClick, CEGUI::Event::Subscriber(
					///// &InGameUIBuddyList::testme_leftclick_online, this ) );
			//frameWindow->subscribeEvent( CEGUI::Window::EventMouseDoubleClick, CEGUI::Event::Subscriber(
					////// &InGameUIBuddyList::testme_mousewheel, this ) );
	
	


			frameWindow->subscribeEvent(CEGUI::FrameWindow::EventSized, Event::Subscriber(
				&InGameUIBuddyList::refreshBuddyList, this));
			



	
			/*** For changing the listbox ***/
			slider->subscribeEvent(  CEGUI::Slider::EventValueChanged, CEGUI::Event::Subscriber(
				&InGameUIBuddyList::event_slider_thumbPositionChange, this ) );
	
	
			slider_scrollbar_pushButton_top->subscribeEvent(CEGUI::PushButton::EventMouseClick, Event::Subscriber(
				&InGameUIBuddyList::event_PushButton_top, this));
	
			slider_scrollbar_pushButton_bottom->subscribeEvent(CEGUI::PushButton::EventMouseClick, Event::Subscriber(
				&InGameUIBuddyList::event_PushButton_bottom, this));
	
	
	

	
			/*** Windows To add and remove names ***/
			buttonAdd->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
					&InGameUIBuddyList::event_add_button, this ) );
			buttonEdit->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
					&InGameUIBuddyList::event_edit_button, this ) );
			buttonRemove->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
					&InGameUIBuddyList::event_remove_button, this ) );
	
			// To add a name;
			menuAddName_ButtonNo->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
					&InGameUIBuddyList::event_menuAddName_ButtonNo, this ) );
			menuAddName_ButtonYes->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
					&InGameUIBuddyList::event_menuAddName_ButtonYes, this ) );
	
			// To remove the name;
			menuRemoveName_ButtonNo->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
					&InGameUIBuddyList::event_menuRemoveName_ButtonNo, this ) );
			menuRemoveName_ButtonYes->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
					&InGameUIBuddyList::event_menuRemoveName_ButtonYes, this ) );

	
	
	
			/****************/
			/*** Settings ***/
			/****************/	
	
			////slider_scrollbar_pushButton_top->setYPosition( listBox->getYPosition() + slider_scrollbar_pushButton_top->getWidth() );

			// Set the slider position;
			slider->setYPosition( slider_scrollbar_pushButton_top->getYPosition() + slider_scrollbar_pushButton_top->getHeight() );
			slider->setHeight( (slider_scrollbar_pushButton_bottom->getYPosition() - slider_scrollbar_pushButton_bottom->getHeight())
 					-slider_scrollbar_pushButton_top->getYPosition() );


			// Hide the Name Add, edit and remove windows;
			menuAddName->hide();
			menuRemoveName->hide();
	
			// Disable sorting for listBox but allow sorting for the other listboxes;
			listBox->setSortingEnabled(false);
			listBoxOnlineSort->setSortingEnabled(true);
			listBoxOfflineSort->setSortingEnabled(true);
	
			// Set the listbox slider to be at the top and its click step;
			slider->setCurrentValue(1.0f);
			slider->setClickStep(0.1f);
	
	
			/************************/
			/***  Add the windows ***/
			/************************/
	
	
			// Attach the listbox and slider to the side image;
			////buddyListBgImageSides->addChildWindow( slider_scrollbar_pushButton_top );
			////buddyListBgImageSides->addChildWindow( slider_scrollbar_pushButton_bottom );
			///buddyListBgImageSides->addChildWindow( slider );
			/////buddyListBgImageSides->addChildWindow( listBox );
	
			// Attach the buttons to the bottom image;
			////buddyListBgImageBottom->addChildWindow( buttonAdd );
			////buddyListBgImageBottom->addChildWindow( buttonEdit );
			/////buddyListBgImageBottom->addChildWindow( buttonRemove );
	
			// Create the window to Add a name;
			menuAddName->addChildWindow( menuAddNameEditbox );
			menuAddName->addChildWindow( menuAddName_ButtonYes );
			menuAddName->addChildWindow( menuAddName_ButtonNo );
	
			// Create the window to Remove a name;
			menuRemoveName->addChildWindow( menuRemoveNameEditbox );
			menuRemoveName->addChildWindow( menuRemoveName_ButtonYes );
			menuRemoveName->addChildWindow( menuRemoveName_ButtonNo );	
	
	
			// Attack the top, side , bottom, resizer and mover images to the main window;
			////window->addChildWindow( buddyListBgImageTop );
			/////window->addChildWindow( buddyListBgImageSides );
			///window->addChildWindow( buddyListBgImageBottom );
			//////window->addChildWindow( imageResizer );
			/////window->addChildWindow( mouseButtonUpTracker );
	
			// Attach the Name Add and Remove windows to the main window;
			///window->addChildWindow( menuAddName );
			////window->addChildWindow( menuRemoveName );
	


			// Attach the listbox and slider to the side image;
			////buddyListBgImageSides->addChildWindow( slider_scrollbar_pushButton_top );
			////buddyListBgImageSides->addChildWindow( slider_scrollbar_pushButton_bottom );
			///buddyListBgImageSides->addChildWindow( slider );
			/////buddyListBgImageSides->addChildWindow( listBox );

			
			frameWindow->addChildWindow( slider );
			frameWindow->addChildWindow( slider_scrollbar_pushButton_top );
			frameWindow->addChildWindow( slider_scrollbar_pushButton_bottom );
			frameWindow->addChildWindow( listBox );

			// Attach the frame Window which everything depends on;
			window->addChildWindow( frameWindow );
		}
		else		// the layout was loaded successfully lets just make events;
		{
	
			/*** We create the online and offline listbox because no layout is needed for invisible things ***/

			// listBoxOnlineSort;
			listBoxOnlineSort = (CEGUI::Listbox *) winManager->createWindow("TaharezLook/listbox_transparent_background_scrollbar1",
					"InGameUI_InGameUIBuddyList_listBoxOnlineSort");
			listBoxOnlineSort->setSize(CEGUI::UVector2(CEGUI::UDim( 0.87, 0), CEGUI::UDim(0.7, 0) ) );
			listBoxOnlineSort->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.155, 0) ) );
	
	
			// listBoxOfflineSort;
			listBoxOfflineSort = (CEGUI::Listbox *) winManager->createWindow("TaharezLook/listbox_transparent_background_scrollbar1",
					"InGameUI_InGameUIBuddyList_listBoxOfflineSort");
			listBoxOfflineSort->setSize(CEGUI::UVector2(CEGUI::UDim( 0.87, 0), CEGUI::UDim(0.7, 0) ) );
			listBoxOfflineSort->setPosition( CEGUI::UVector2(CEGUI::UDim(0.1, 0), CEGUI::UDim(0.155, 0) ) );
	
	
			/*****************************************/
			/*** Get the pointers to the windows   ***/
			/*****************************************/
			frameWindow = (CEGUI::FrameWindow*) winManager->getWindow("InGameUI_InGameUIBuddyList_BuddyListWindow");

			// listbox;
			listBox = (CEGUI::Listbox *) winManager->getWindow("InGameUI_InGameUIBuddyList_listBox");

			// slider;
			slider = (CEGUI::Slider *)winManager->getWindow("InGameUI_InGameUIBuddyList_buddyList_slider");
			// The scrollbar top and bottom push buttons	
			slider_scrollbar_pushButton_top = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_ScrollBar_PushButton_Top");
			slider_scrollbar_pushButton_bottom = (CEGUI::PushButton*) winManager->getWindow(
				"InGameUI_InGameUIBuddyList_ScrollBar_PushButton_Bottom");
	

			// buttonAdd;
			buttonAdd = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_buttonAdd");
			// buttonEdit;
			buttonEdit = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_buttonEdit");
			// buttonRemove;
			buttonRemove = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_buttonRemove");
			
	
			/***  Create the menuAddName for adding names to the listBox ***/ 
			menuAddName = (CEGUI::FrameWindow*) winManager->getWindow("InGameUI_InGameUIBuddyList_menuAddName");
			// Create a editbox to input the name;
			menuAddNameEditbox = (CEGUI::Editbox *) winManager->getWindow("InGameUI_InGameUIBuddyList_menuAddNameEditbox");
			// Create a "yes" button;
			menuAddName_ButtonYes = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_menuAddName_ButtonYes");
			// Create a "No" button;
			menuAddName_ButtonNo = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_menuAddName_ButtonNo");
	
	
			/***  Create the menuRemoveName for removing names from the listBox ***/ 
			menuRemoveName = (CEGUI::FrameWindow*) winManager->getWindow("InGameUI_InGameUIBuddyList_menuRemoveName");
			// Create a editbox to input the name;
			menuRemoveNameEditbox = (CEGUI::Editbox *) winManager->getWindow("InGameUI_InGameUIBuddyList_menuRemoveNameEditbox");
			// Create a "yes" button;
			menuRemoveName_ButtonYes = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_menuRemoveName_ButtonYes");
			// Create a "No" button;
			menuRemoveName_ButtonNo = (CEGUI::PushButton*) winManager->getWindow("InGameUI_InGameUIBuddyList_menuRemoveName_ButtonNo");
	
			
			/****************/
			/*** TESTS   ***/
			/****************/
	
			listBox->setSortingEnabled(false);
			listBoxOnlineSort->setSortingEnabled(true);
			listBoxOfflineSort->setSortingEnabled(true);
			CEGUI::EventArgs pEventArgs;
			testme_loadListBox();
	
	
			/****************/
			/*** Events   ***/
			/****************/
	
	


	
	
			/****************/
			/*** Settings ***/
			/****************/
			

			// Hide the Name Add, edit and remove windows;
			menuAddName->hide();
			menuRemoveName->hide();
	
			// Disable sorting for listBox but allow sorting for the other listboxes;
			listBox->setSortingEnabled(false);
			listBoxOnlineSort->setSortingEnabled(true);
			listBoxOfflineSort->setSortingEnabled(true);
	
			// Set the listbox slider to be at the top and its click step;
			slider->setCurrentValue(1.0f);
			slider->setClickStep(0.1f);
		}
	}
	catch( ... )
	{
		cout << "InGameUIBuddyList::addChildWindow(): An unknown error occured" << endl;
		return false;
	}

	return true;
}


 
