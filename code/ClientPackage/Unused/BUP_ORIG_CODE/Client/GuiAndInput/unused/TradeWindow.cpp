/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TradeWindow.h"
 

InGameUITradeWindow::InGameUITradeWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	
	mTradeWindow = windowManager.createWindow("TaharezLook/FrameWindow", "InGameUI_TradeWindow");
	mTradeWindow->setSize(CEGUI::USize(CEGUI::UDim(0.15f, 0.0f), CEGUI::UDim(0.15f, 0.0f)));
	sheet->addChild( mTradeWindow );
}

InGameUITradeWindow::~InGameUITradeWindow()
{

}
