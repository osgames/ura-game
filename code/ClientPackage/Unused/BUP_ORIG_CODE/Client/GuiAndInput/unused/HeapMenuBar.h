/*
Project Ura, an open source MMORPG.
Copyright (C) 2008  Denny Kunker

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef IN_GAME_UI_MENUBAR_H
#define IN_GAME_UI_MENUBAR_H



#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>


#include "MenuBarImageState.h"
#include "InGameUIMenuBarImageData.h"

// stores the file paths for the layouts;
#include "InGameUILayoutPaths.h"


class InGameUI;

using namespace std;
using namespace CEGUI;



#define IMAGE_TYPE_NORMAL	0
#define IMAGE_TYPE_HOVER	1
#define IMAGE_TYPE_PRESSED	2
#define IMAGE_TYPE_ACTIVE	3



/*************************************************************************************
There are four functions that deal with how the menubar images change and these are:

*bool event_mouseHover_image(const CEGUI::EventArgs& pEventArgs);
*bool event_imageLeftPress_toFront(const CEGUI::EventArgs& pEventArgs);
*bool event_LeftButtonUp(const CEGUI::EventArgs& pEventArgs);
*bool event_mouseLeave_image(const CEGUI::EventArgs& pEventArgs);


mouseHover - changes the image to a hover image whenever the cursor hovers the button;
leftPress_toFront - changes the image to pressed when the image is pressed;

LeftButtonUp - calls the function pObject->event_menubar_toggleButton(i, [true or false]).
  which checks if the window is visible or not, and depending on its outcome calls
  setImageActive() or setImageNormal() which changes the states, or the image depending on keyboard or mouse bool.
  When the cursor leaves the image it calls the function mouseLeave_image() which then sets the
  Image depending on its state.

mouseLeave_image - sets the Image depending on its state.

#######################################################################################

When the mouse button isn't released under the button and is held until it leaves the image button, then
Problems can occur. Such as the image toggling when its not suppose to. "leftMouseBtnReleased" variable checks if
The user released the mouse button under the image button. If not then we restore the image back
to its previous state;

leftMousebtnRelease deals with the fuctions:
*bool event_imageLeftPress_toFront(const CEGUI::EventArgs& pEventArgs);
*bool event_LeftButtonUp(const CEGUI::EventArgs& pEventArgs);
*bool event_mouseLeave_image(const CEGUI::EventArgs& pEventArgs);
setImageActive(const unsigned int button, const bool keyboard)
setImageNormal(const unsigned int button, const bool keyboard)

****************************************************************************************/


class InGameUIMenuBar
{
protected:
	vector<unsigned int> mNumImageButtons;
	vector<float> mImageResizerYSpace;

	vector<string> mImageWindowName;
	vector<string> mImagesetName;

	// Image mover;
	CEGUI::Imageset* mImgMover;
	CEGUI::Window* mMenubarImageMover;	

	// Image Resizer;
	CEGUI::Imageset* mImgResizer;
	CEGUI::Window* mMenubarImageResizer;

	// For window moving
	CEGUI::Window* mMovingWindow;
	vector<CEGUI::Event::Connection> mMovingEvent;
	vector<CEGUI::Vector2> mMousePosInWindow;

	// Create important objects to store data;
	vector<InGameUIMenuBarImageData> mMenuBarImageData;
	vector<MenuBarImageState> mVimageStates;		// The #defines are associated with this. EX: #define IMAGE_TYPE_NORMAL
	vector<string> mVimageTypes;

	// Store the pointers to the window and imagesets;
	vector<CEGUI::Window*> mVpWindow;
	vector<CEGUI::Imageset*> mVpImageSet;


	// InGameUI pointer to call other classes;
	InGameUI *mPobject;

	// There is a bug where when the resizer is being dragged it touches the last image -
	// - and makes it change the images to a hover image. We will stop the image -
	// - from changing to the hover image, with this variable;
	// This also makes sure when the windows are active and prevents the mouse from -
	// - moving the camera;
	vector<bool> mWindowBeingDragged;

	/**********
		When the mouse button isn't released under the button and is held until it leaves the image button, then
		Problems can occur. Such as the image toggling when its not suppose to. "leftMouseBtnReleased" variable checks if
		The user released the mouse button under the image button. If not then we restore the image back
		to its previous state;
	**********/
	vector<bool> mLeftMouseBtnReleased;


	/******************************************************************************/
	/*** These functions handles the mouse over and mouse leaving of the images ***/
	/******************************************************************************/
	bool event_mouseHover_image(const CEGUI::EventArgs& pEventArgs);
	bool event_mouseLeave_image(const CEGUI::EventArgs& pEventArgs);

	/**********************************************************************************************/
	/*** This function handles left button image presses and sets all the menbar windows on top ***/
	/**********************************************************************************************/

	// Note mouse button presses toggle windows here;
	bool event_imageLeftPress_toFront(const CEGUI::EventArgs& pEventArgs);

	/***************************************************/
	/*** This function handles left button up events ***/
	/***************************************************/
	bool event_LeftButtonUp(const CEGUI::EventArgs& pEventArgs);

	/*********************************************************************************/
	/*** THESE TWO FUNCTIONS ARE FOR MOVING THE MENUBAR WINDOW WITH THE IMAGEMOVER ***/
	/*********************************************************************************/
	bool event_imageMover_onLeftPress(const CEGUI::EventArgs& pEventArgs);
	bool event_imageMover_onWindowMove(const CEGUI::EventArgs& pEventArgs);

	/*************************************************************************************/
	/*** THESE TWO FUNCTIONS ARE FOR RESIZING THE MENUBAR WINDOW WITH THE IMAGERESIZER ***/
	/*************************************************************************************/
	bool event_imageSizer_onLeftPress(const CEGUI::EventArgs& pEventArgs);

	// This function moves all the windows together;
	bool event_imageSizer_onWindowMove(const CEGUI::EventArgs& pEventArgs);

public:
	InGameUIMenuBar();
	void prepareToQuit();

	void toggle();	
	bool isVisible();

	void setImageActive(const unsigned int button, const bool keyboard);
	void setImageNormal(const unsigned int button, const bool keyboard);

	bool isActive();
	void deactivate();
	bool addChildWindow(InGameUI *gameUI, const unsigned int numButtons,
		vector<unsigned int>* vActiveButton, vector<string>* vToolTip );
};





////////#define HEAPMENUBAR_DEBUG


// The point is to put as much stuff on the heap as possible. And to delete things when not needed or toggled;
class HeapMenuBar
{
protected:
	InGameUIMenuBar* mMenubar;
public:
	HeapMenuBar();
	~HeapMenuBar();
	void prepareToQuit();

	void toggle(InGameUI *gameUI, const unsigned int numButtons,
		vector<unsigned int>* vActiveButton, vector<string>* vToolTip );

	bool isVisible();
	bool isActive();
	void deactivate();

	void setImageActive(const unsigned int button, const bool keyboard);
	void setImageNormal(const unsigned int button, const bool keyboard);
};




#endif

