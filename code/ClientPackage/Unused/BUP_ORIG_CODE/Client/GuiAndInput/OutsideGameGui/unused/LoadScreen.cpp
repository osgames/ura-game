/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LoadScreen.h"
#include "CEGUI/CEGUI.h"
#include "Ogre.h"
#include "Ogre3dManager.h"
#include "CeguiManager.h"

#define DEF_LOADSCREEN_MAINWINDOW_NAME		"LoadScreen_loadScreenWindow"
#define DEF_LOADSCREEN_BGIMAGE_FILENAME		"LoadBackground.jpg"
#define DEF_LOADSCREEN_BGIMAGESET_NAME		"LoadScreen_bgImage"




LoadScreen::LoadScreen()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();
	CEGUI::Window* const rootWindow = ceguiManager->getRootWindow();

	CEGUI::Window* loadScreenWindow = NULL;

	  
	// Load an image to use as a background //
	imageManager->addFromImageFile(DEF_LOADSCREEN_BGIMAGESET_NAME, DEF_LOADSCREEN_BGIMAGE_FILENAME);

	// Create the main window //
	loadScreenWindow = windowManager->createWindow("TaharezLook/StaticImage", DEF_LOADSCREEN_MAINWINDOW_NAME);
	loadScreenWindow->setSize(CEGUI::USize(CEGUI::UDim(1.0, 0), CEGUI::UDim(1.0, 0)));
	loadScreenWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0, 0), CEGUI::UDim(0.0, 0)));
	loadScreenWindow->setProperty("Image", DEF_LOADSCREEN_BGIMAGESET_NAME);


	// Add the child window //
	rootWindow->addChild(loadScreenWindow);

	   
	// FIXME IMP: Have Ogre render one frame - needed to show the loading screen //
	// This has been causing crashes //
	//ogre3dManager->getRoot()->renderOneFrame();
}
 
LoadScreen::~LoadScreen()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();

	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::Window* tmpWindow = sheet->getChild(DEF_LOADSCREEN_MAINWINDOW_NAME);
	 
	windowManager->destroyWindow(tmpWindow);

	// Destroy the imageset and the texture //
	imageManager->destroy(DEF_LOADSCREEN_BGIMAGESET_NAME);
	imageManager->destroyImageCollection(DEF_LOADSCREEN_BGIMAGESET_NAME);
}
