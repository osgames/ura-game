/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ServerMenu.h"
#include "CeguiManager.h"
#include "Network.h"
#include "NetworkStructures.h"
#include "ClientStateManager.h"


#define DEF_SERVERMENU_WINDOW_NAME		"ServerMenu_WindowName"
#define DEF_SERVERMENU_BGIMAGESET_NAME		"ServerMenu_bgImage"
#define DEF_SERVERMENU_BGIMAGE_FILENAME		"MenuBackground.jpg"


using namespace std;



ServerMenu::ServerMenu()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();
	CEGUI::Window* const parentWindow = ceguiManager->getRootWindow();

	Network* const network = Network::getSingletonPtr();

	CEGUI::Window* mainServerMenu;




	/// Load an image to use as a background ///
	CEGUI::ImageManager::getSingleton().addFromImageFile(DEF_SERVERMENU_BGIMAGESET_NAME, DEF_SERVERMENU_BGIMAGE_FILENAME);



	// mainServerMenu;
	mainServerMenu = windowManager->createWindow("TaharezLook/StaticImage", DEF_SERVERMENU_WINDOW_NAME);
	mainServerMenu->setSize(CEGUI::USize(CEGUI::UDim(1.0, 0), CEGUI::UDim(1.0, 0)));
	mainServerMenu->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0, 0), CEGUI::UDim(0.0, 0)));


	// mServer_listbox;
	mServer_listbox = (CEGUI::Listbox *)windowManager->createWindow("TaharezLook/Listbox", "ServerMenu_mServer_listbox");
	mServer_listbox->setSize(CEGUI::USize(CEGUI::UDim(0.50, 0), CEGUI::UDim(0.50, 0)));
	mServer_listbox->setPosition( CEGUI::UVector2(CEGUI::UDim(0.25, 0), CEGUI::UDim(0.25, 0) ) );


	// mBack;
	mBack = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "ServerMenu_mBack") );
	mBack->setText("Back");
	mBack->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mBack->setPosition( CEGUI::UVector2(CEGUI::UDim(0.7, 0), CEGUI::UDim(0.8, 0) ) );
	

	// mConnect;
	mConnect = static_cast<CEGUI::PushButton*>( windowManager->createWindow("TaharezLook/Button", "ServerMenu_mConnect") );
	mConnect->setText("Connect");
	mConnect->setSize(CEGUI::USize(CEGUI::UDim(0.15, 0), CEGUI::UDim(0.05, 0)));
	mConnect->setPosition( CEGUI::UVector2(CEGUI::UDim(0.425, 0), CEGUI::UDim(0.8, 0) ) );


	 
	/*** Feel the listbox with the server names ***/
	CEGUI::ListboxTextItem *item = new CEGUI::ListboxTextItem( "Server Name                              Status" );
	item->setDisabled( true );
	mServer_listbox->addItem( item );
	list.push_back( item );

	CEGUI::ListboxTextItem *item1 = new CEGUI::ListboxTextItem( "                                               " );
	item1->setDisabled( true );
	mServer_listbox->addItem( item1 );
	list.push_back( item1 );

	if (network)
	{
		for( int indx = 0; indx < network->server_list.size(); indx++ )
		{
			CEGUI::ListboxTextItem *item = new CEGUI::ListboxTextItem( network->server_list[indx]->server_name );

			// Set the selection image //
			const CEGUI::Image* selectImage = &CEGUI::ImageManager::getSingleton().get("TaharezLook/TextSelectionBrush");
			item->setSelectionBrushImage(selectImage);
			 
			mServer_listbox->addItem( item );
			list.push_back( item );
		}
	
		// If the server list size is greater than '0' than select the first server.
		// The Index starts at '2' because there are two unused indexes above it.
		if ( network->server_list.size() > 0 )
		{
			mServer_listbox->setItemSelectState (size_t (2), true);
		}
	}


	/*************************/
	/*** Background Images ***/
	/*************************/
	mainServerMenu->setProperty("Image", DEF_SERVERMENU_BGIMAGESET_NAME);


	/**************/
	/*** Events ***/
	/**************/
	mBack->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &ServerMenu::event_back, this ) );
	mConnect->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &ServerMenu::event_connect, this ) );


	/***********************/
	/*** Add the windows ***/
	/***********************/
	mainServerMenu->addChild(mServer_listbox);
	mainServerMenu->addChild(mBack);
	mainServerMenu->addChild(mConnect);

	parentWindow->addChild(mainServerMenu);
}
 
ServerMenu::~ServerMenu()
{
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();
	CEGUI::WindowManager* const windowManager = ceguiManager->getWindowManager();
	CEGUI::ImageManager* const imageManager = ceguiManager->getImagesetManager();


	mServer_listbox->resetList();


	CEGUI::Window* sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
	CEGUI::Window* tmpWindow = sheet->getChild(DEF_SERVERMENU_WINDOW_NAME);
	  
	windowManager->destroyWindow(tmpWindow);

	// Destroy the imageset and the texture //
	imageManager->destroy(DEF_SERVERMENU_BGIMAGESET_NAME);
	imageManager->destroyImageCollection(DEF_SERVERMENU_BGIMAGESET_NAME);
}

bool ServerMenu::event_back(const CEGUI::EventArgs &e)
{
	return sendEscape();
}

bool ServerMenu::event_connect(const CEGUI::EventArgs &e)
{	
	return sendEnter();
}

bool ServerMenu::sendEnter()
{
	Network* const network = Network::getSingletonPtr();


	if (network)
	{
		// If a Servers available then use the listbox selected Server;
		if( mServer_listbox->getFirstSelectedItem() != NULL )
		{
			for( int indx = 0; indx < network->server_list.size(); indx++ )
			{
				if( strcmp( network->server_list[indx]->server_name.c_str(), mServer_listbox->getFirstSelectedItem()->getText().c_str() ) == 0 )
				{
					network->connectToServer( network->server_list[indx]->server_ip_address );
				}
			}
		}
	}

	 
	return true;
}

// Handle Escape key;
bool ServerMenu::sendEscape()
{
	ClientStateManager::getSingletonPtr()->setState( AS_MAIN_MENU );	
	return true;
}