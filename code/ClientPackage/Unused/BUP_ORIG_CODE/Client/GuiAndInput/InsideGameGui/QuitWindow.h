/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef __QUITWINDOW_H__
#define __QUITWINDOW_H__


/****************/
/*** INCLUDES ***/
/****************/
#include <CEGUI/CEGUI.h>


/***************/
/*** DEFINES ***/
/***************/
#define DEF_QUITWINDOW_MAINWINDOW_NAME	"QuitWindow_MainWindow"

 

class QuitWindow
{
protected:
	CEGUI::FrameWindow* mMainWindow;

	void createWindow();
	void destroyWindow();

	bool event_noQuit(const CEGUI::EventArgs &e);
	bool event_quit(const CEGUI::EventArgs &e);
public:
	QuitWindow();
	~QuitWindow();

	bool isActive() const;
	void toggle();
}; 


#endif

