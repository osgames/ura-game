/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "QuitWindow.h"
#include "ClientStateManager.h"




QuitWindow::QuitWindow()
{
	mMainWindow = NULL;
}
 
QuitWindow::~QuitWindow()
{
	this->destroyWindow();
}
 


bool QuitWindow::isActive() const
{
	return mMainWindow;
}



void QuitWindow::toggle()
{
	if (mMainWindow)
	{
		this->destroyWindow();
	}
	else
	{
		this->createWindow();
	}
}
 


void QuitWindow::destroyWindow()
{
	CEGUI::WindowManager::getSingleton().destroyWindow(mMainWindow);
	mMainWindow = NULL;
}


void QuitWindow::createWindow()
{
	CEGUI::Window* const sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();	
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::Window* mainChildWindow = NULL;
	CEGUI::Window* quitMenuLabel = NULL;
	CEGUI::PushButton* quitMenu_button_yes = NULL;
	CEGUI::PushButton* quitMenu_button_no = NULL;


	// Quit Menu
	mMainWindow = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", DEF_QUITWINDOW_MAINWINDOW_NAME);
	mMainWindow->setSize(CEGUI::USize(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.3, 0)));
	mMainWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.3, 0), CEGUI::UDim(0.25, 0) ) );

	// Main Child Window of which everything is attached to;
	//mainChildWindow = windowManager.createWindow("TaharezLook/Listbox_woodframe","QuitWindow_mainChildWindow");
	mainChildWindow = windowManager.createWindow("TaharezLook/Listbox","QuitWindow_mainChildWindow");
	mainChildWindow->setSize(CEGUI::USize(CEGUI::UDim(0.93f, 0.0f), CEGUI::UDim(0.77f, 0.0f))); 
	mainChildWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.035f, 0.0f), CEGUI::UDim(0.15f, 0.0f))); 

	// Quit menu button yes;
	quitMenu_button_yes = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "QuitWindow_quitMenu_button_yes");
	quitMenu_button_yes->setText("Yes");
	quitMenu_button_yes->setSize(CEGUI::USize(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.15, 0)));
	quitMenu_button_yes->setPosition( CEGUI::UVector2(CEGUI::UDim(0.07, 0), CEGUI::UDim(0.7, 0) ) );


	// Quit menu button no;
	quitMenu_button_no = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "QuitWindow_quitMenu_button_no");
	quitMenu_button_no->setText("No");
	quitMenu_button_no->setSize(CEGUI::USize(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.15, 0)));
	quitMenu_button_no->setPosition( CEGUI::UVector2(CEGUI::UDim(0.53, 0), CEGUI::UDim(0.7, 0) ) );


	// Quit menu label;
	quitMenuLabel = (CEGUI::Window *) windowManager.createWindow("TaharezLook/StaticText", "QuitWindow_quitMenuLabel");
	quitMenuLabel->setSize(CEGUI::USize(CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
	quitMenuLabel->setText("Are you sure you want to quit?");
	// Disable frame and background;
	quitMenuLabel->setProperty("FrameEnabled", "false");
	quitMenuLabel->setProperty("BackgroundEnabled", "false");
	// Make the label text Vertically and Horizontally Centered
	quitMenuLabel->setProperty("VertFormatting", "VertCentred");
	quitMenuLabel->setProperty("HorzFormatting",  "HorzCentred");
	// Disable the label so it doesn't steal the events;
	quitMenuLabel->disable();


	/// Add the events ///
	quitMenu_button_yes->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&QuitWindow::event_quit, this ) );	
	quitMenu_button_no->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&QuitWindow::event_noQuit, this ) );


	/*** Add the windows ***/
	mainChildWindow->addChild( quitMenuLabel );
	mainChildWindow->addChild( quitMenu_button_yes );
	mainChildWindow->addChild( quitMenu_button_no );

	mMainWindow->addChild( mainChildWindow );
	sheet->addChild( mMainWindow );
}




bool QuitWindow::event_noQuit(const CEGUI::EventArgs &e)
{	
	this->destroyWindow();
	return true;
}

bool QuitWindow::event_quit(const CEGUI::EventArgs &e)
{
	// Destroy the window - if i don't the quit window stays open//
	this->destroyWindow();

	// Change the state //
	ClientStateManager::getSingletonPtr()->setState(AS_MAIN_MENU);

	return true;
}

