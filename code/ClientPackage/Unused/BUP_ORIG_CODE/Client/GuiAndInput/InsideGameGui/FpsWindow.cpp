/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FpsWindow.h"
#include "CeguiManager.h"
#include "Ogre3dManager.h"


FpsWindow::FpsWindow()
{ 
	CeguiManager* const ceguiManager = CeguiManager::getSingletonPtr();


	mTmpString = "";
	mTmpFloat = 0.0f;

	mLabel = static_cast<CEGUI::Window*>( ceguiManager->createWindow("TaharezLook/StaticText", "FpsWindow_mLabel") );
	mLabel->setSize(CEGUI::USize(CEGUI::UDim(0.5, 0), CEGUI::UDim(0.03, 0)));
	mLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(0.025, 0), CEGUI::UDim(0.95, 0) ) );
	// Disable frame and background;
	mLabel->setProperty("FrameEnabled", "false");
	mLabel->setProperty("BackgroundEnabled", "false");
	// Disable the label so it doesn't steal the events;
	mLabel->disable();

	/// Add the child to the root window ///
	ceguiManager->getRootWindow()->addChild(mLabel);
}

FpsWindow::~FpsWindow()
{
	CeguiManager::getSingletonPtr()->destroyWindow(mLabel);
}

 
void FpsWindow::update()
{
	mTmpFloat = Ogre3dManager::getSingletonPtr()->getRenderWindow()->getLastFPS();
	mTmpString = "FPS: ";
	mTmpString += Ogre::StringConverter::toString(mTmpFloat);
	mLabel->setText(mTmpString);
}

