/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __INSIDEGAMEGUI_H__
#define __INSIDEGAMEGUI_H__


#include <CEGUI/CEGUI.h>

#include "InputManager.h"
#include "QuitWindow.h"
#include "FpsWindow.h"
#include "ChatWindow.h"
///// FIXME: Fix these files later
//////#include "InGameUIInventoryWindow.h"
//////#include "HeapPlayerInteractionWindow.h"
//////#include "TradeWindow.h"
//////#include "HeapNpcDialogueWindow.h"



class InsideGameGui
{
private:
	InputKeyCollector* mInputKeyCollector;

	QuitWindow* mQuitWindow;
	FpsWindow* mFpsWindow;
//	ChatWindow* mChatWindow;
	// Get Rid of the auto pointers //
	////std::auto_ptr<InGameUIInventoryWindow> mInventoryWindow;
	////std::auto_ptr<HeapPlayerInteractionWindow> mHeapPlayerInteractionWindow;
	////std::auto_ptr<InGameUITradeWindow> mTradeWindow;
	////std::auto_ptr<HeapNpcDialogueWindow> mHeapNpcDialogueWindow;
public:
	InsideGameGui();
	~InsideGameGui();

	bool isWindowActive() const;
	
	void updateInput();
	void updateData();
	  
	 
	//QuitWindow* const getQuitWindow() const
	//{
	//	return mQuitWindow;
	//}

	//std::auto_ptr<HeapPlayerInteractionWindow> getHeapPlayerInteractionWindow()
	//{
	//	return mHeapPlayerInteractionWindow;
	//}

	//std::auto_ptr<InGameUIInventoryWindow> getInventoryWindow()
	//{
	//	return mInventoryWindow;
	//}

	//std::auto_ptr<HeapNpcDialogueWindow> getHeapNpcDialogueWindow()
	//{
	//	return mHeapNpcDialogueWindow;
	//}


	// Chat window //
	void receive_say_chat( std::string &message, std::string &player );
};

#endif
