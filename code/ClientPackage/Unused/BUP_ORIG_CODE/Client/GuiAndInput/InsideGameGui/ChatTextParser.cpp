/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ChatTextParser.h"

//#include "SelfPlayerCharacter.h"
#include "Game.h"
#include "ClientStateManager.h"

#include <iostream>
#include <Ogre.h>
#include <CEGUI/CEGUI.h>






void ChatTextParser::appendWords( std::string *tmpBufferOne, const std::vector<std::string> &vStr, const int &numWords)
{
	try
	{
		// i = 1 because we always skip the first word or tag. Such as "/command" is skipped;
		for ( int i = 1; i < numWords; i++ )
		{
			tmpBufferOne->append( vStr[i] );

			// If were not on the last word then append a " ";
			if ( (i + 1) < numWords )
			{
				tmpBufferOne->append( " " );
			}
		}
	}
	catch( ... )
	{
		std::cout << "ChatTextParser::appendWords() Error: an unknown Exception occured!" << std::endl;
	}
}

std::string ChatTextParser::handleText(std::string &s)
{
	unsigned int maxLen = 0;
	unsigned int numWords = 0;
	unsigned int counter = 0;

	std::string tmpBufferOne = "";
	std::string tmpBufferTwo = "";
	std::vector<std::string> vStr;


	try
	{
		// Get the max length of the string.
		maxLen = s.length();
		
		// Take out the blank spaces and just keep the words in a vector array;
		// Also count the number of words.
		while ( counter < maxLen )
		{
			while ( s[counter] == ' ' &&  counter < maxLen )
			{
				//std::cout << "SPACE" << s[counter] << std::endl;
				counter++;
			}
		
			// Clear the tmpStr;
			tmpBufferTwo = "";
			
			while ( s[counter] != ' ' && counter < maxLen )
			{
				//std::cout << "NOSPACE" << s[counter] << std::endl;
				tmpBufferTwo.append( s, counter, 1 );

				counter++;
			}
		
			//std::cout << "tmpBufferTwo=" << tmpBufferTwo << std::endl;
			vStr.push_back(tmpBufferTwo);
			numWords++;
		}


		/*** If no Words and just spaces, then return true ***/
		if ( numWords <= 0 )
		{
			return "";
		}


		/************************/
		/*** Handle the words ***/
		/************************/


		/*************************************************************************************************************
		// "/command";
		if ( vStr[0] == "/command" )
		{
			// If the numWords is bigger than 1 than we append all the words and send them to the server.
			if ( numWords > 1 )
			{
				// This function puts the words together and adds the spaces;
				appendWords( &tmpBufferOne, vStr, numWords );
				//std::cout << "TOTAL: " << tmpBufferOne << std::endl;
				
				// Send a command message.
				Network::getSingletonPtr()->sendCommand( tmpBufferOne );
			}
		}
		else if ( vStr[0] == "/modelhelp" )
		{
			std::string s = "/fog [on/off]\n \
					/echo [text]\n \
					/model [name] [model file name]\n \
					/modelRm [name]\n \
					/modelSize [name] [x] [y] [z]\n \
					/modelPlayerSize  [x] [y] [z]\n \
					/modelLight [name] [x] [y] [z]\n \
					/modelLightAmbient [x] [y] [z]\n \
					/modelPos [name] [x/y/z] [value]\n \
					/modelYaw [name] [value]\n \
					/modelRoll [name] [value]";

			return s;
		}
		else if ( vStr[0] == "/echo" )
		{
			// This function puts the words together and adds the spaces;
			appendWords( &tmpBufferOne, vStr, numWords );

			return tmpBufferOne;
		}
		else if ( vStr[0] == "/fog" )
		{
			if (vStr[1] == "on" )
			{
				Ogre::SceneManager *scene_manager;
				scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				scene_manager->setFog(Ogre::FOG_EXP, Ogre::ColourValue::White, 0.0002);
				return "/fog is on.";
			}
			else if (vStr[1] == "off" )
			{
				Ogre::SceneManager *scene_manager;
				scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				scene_manager->setFog(Ogre::FOG_NONE, Ogre::ColourValue::White, 0.0002);
				return "/fog is off.";
			}
		}
		else if ( vStr[0] == "/model" )		// Add a model;
		{
			if ( numWords == 3 )
			{
				Ogre::SceneManager *scene_manager;
				//Ogre::Entity *mEntity;
				//Ogre::SceneNode *mSceneNode;
				Game *game;

				scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				
				game = ClientStateManager::getSingletonPtr()->getGame();
				if( game )
				{
					try
					{
						//game->getPlayer()->setPosition( TempPosition );
						Ogre::Entity* thisEntity = scene_manager->createEntity( vStr[1], vStr[2] + ".mesh");
						Ogre::SceneNode* thisSceneNode = scene_manager->getRootSceneNode()->createChildSceneNode(
							game->getPlayer()->getPosition(), game->getPlayer()->getTargetOrientation()  );
						// game->getPlayer()->getPlayerOrientation()
						thisSceneNode->attachObject(thisEntity);
						thisSceneNode->scale(5, 5, 5);
					}
					catch(...)
					{
						std::cout << "ChatTextParser::handleText(): /model error" << std::endl;
						return "";
					}

					return "/model is set.";
				}
			}
		}
		else if ( vStr[0] == "/modelRm" )	// Remove a model;
		{
			if ( numWords == 2 )
			{
				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				Ogre::Entity* thisEntity = scene_manager->getEntity( vStr[1] );

				Ogre::SceneNode* parent = thisEntity->getParentSceneNode();
				parent->detachObject(thisEntity);
				scene_manager->destroyEntity( vStr[1] );
				
				return "/modelRM: Model is removed.";
			}
		}
		else if ( vStr[0] == "/modelSize" )	// resize a model;
		{
			if ( numWords == 5 )
			{
				CEGUI::PropertyHelper ph;

				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				Ogre::Entity* thisEntity = scene_manager->getEntity( vStr[1] );

				Ogre::SceneNode* parent = thisEntity->getParentSceneNode();
				parent->scale( ph.stringToFloat(vStr[2]), ph.stringToFloat(vStr[3]), ph.stringToFloat(vStr[4]) );

				return "/modelSize: Model is resized.";
			}
		}
		else if ( vStr[0] == "/modelPlayerSize" )	// resize the player;
		{
			if ( numWords == 4 )
			{
				CEGUI::PropertyHelper ph;

				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				Ogre::Entity* thisEntity = scene_manager->getEntity( "client_player_Entity" );

				Ogre::SceneNode* parent = thisEntity->getParentSceneNode();
				parent->scale( ph.stringToFloat(vStr[1]), ph.stringToFloat(vStr[2]), ph.stringToFloat(vStr[3]) );

				return "/modelPlayerSize: Player is resized.";
			}
		}
		else if ( vStr[0] == "/modelLight" )	// Add a light;
		{
			if ( numWords == 5 )
			{
				CEGUI::PropertyHelper ph;
				Game *game;

				game = ClientStateManager::getSingletonPtr()->getGame();
				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				
				Ogre::Light* myLight = scene_manager->createLight( vStr[1] );
				myLight->setType(Ogre::Light::LT_POINT);
				myLight->setPosition(game->getPlayer()->getPosition().x, game->getPlayer()->getPosition().y,
					game->getPlayer()->getPosition().z);
				myLight->setDiffuseColour( ph.stringToFloat(vStr[2]), ph.stringToFloat(vStr[3]), ph.stringToFloat(vStr[4]) );
				myLight->setSpecularColour( ph.stringToFloat(vStr[2]), ph.stringToFloat(vStr[3]), ph.stringToFloat(vStr[4]) );

				return "/modelLight: A light was added.";
			}
		}
		else if ( vStr[0] == "/modelLightAmbient" )	// Change the Ambient light;
		{
			if ( numWords == 4 )
			{
				CEGUI::PropertyHelper ph;
				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();

				scene_manager->setAmbientLight(Ogre::ColourValue(ph.stringToFloat(vStr[1]), ph.stringToFloat(vStr[2]),
					ph.stringToFloat(vStr[3])));
	
				return "/modelLightAmbient: Ambient light changed.";
			}	
		}
		else if ( vStr[0] == "/modelPos" )	// Change the model xyz position;
		{
			if ( numWords == 4 )
			{
				CEGUI::PropertyHelper ph;
				const float i = ph.stringToFloat( vStr[3] );

				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				Ogre::Entity* thisEntity = scene_manager->getEntity( vStr[1] );
				Ogre::SceneNode* parent = thisEntity->getParentSceneNode();

				if ( vStr[2] == "x" )
				{
					parent->setPosition( parent->getPosition().x + i, parent->getPosition().y, parent->getPosition().z );
				}
				else if ( vStr[2] == "y" )
				{
					parent->setPosition( parent->getPosition().x, parent->getPosition().y + i, parent->getPosition().z );
				}
				else if ( vStr[2] == "z" )
				{
					parent->setPosition( parent->getPosition().x, parent->getPosition().y, parent->getPosition().z + i );
				}

				return "/modelPos: Model is moved.";
			}
		}
		else if ( vStr[0] == "/modelYaw" )	// rotate the model;
		{
			if ( numWords == 3 )
			{
				CEGUI::PropertyHelper ph;
				const float i = ph.stringToFloat( vStr[2] );

				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				Ogre::Entity* thisEntity = scene_manager->getEntity( vStr[1] );
				Ogre::SceneNode* parent = thisEntity->getParentSceneNode();

				Ogre::Radian r(i);
				parent->yaw(r, Ogre::Node::TS_LOCAL);

				return "/modelRotate: Model Yawed.";
			}
		}
		else if ( vStr[0] == "/modelRoll" )	// Roll the model;
		{
			if ( numWords == 3 )
			{
				CEGUI::PropertyHelper ph;
				const float i = ph.stringToFloat( vStr[2] );

				Ogre::SceneManager *scene_manager = Ogre3dManager::getSingleton().getSceneManager();
				Ogre::Entity* thisEntity = scene_manager->getEntity( vStr[1] );
				Ogre::SceneNode* parent = thisEntity->getParentSceneNode();

				Ogre::Radian r(i);
				parent->roll(r, Ogre::Node::TS_LOCAL);

				return "/modelRoll: Model Rolled.";
			}
		}
		else if ( vStr[0] == "/trade" )	// Initiate a trade with another player;
		{
		}





		else	// This is chat and not a command;	
		{
			// Send chat message.
			Network::getSingletonPtr()->sendChatMessage( s );
		}
	}
	catch( std::string str )
	{
		std::cout << "Exception Raised: " << str << std::endl;
		return "";
	}
	catch( ... )
	{
		std::cout << "ChatTextParser::handleText(): Unknown exception." << std::endl;
		return "";
	}

	return "";
	*****************************************************************************************/



	/**************************************/
	/*** Send chat message only for now ***/
	/**************************************/
		Network::getSingletonPtr()->sendChatMessage( s );
		return "";
	}
	catch( std::string str )
	{
		std::cout << "Exception Raised: " << str << std::endl;
		return "";
	}
	catch( ... )
	{
		std::cout << "ChatTextParser::handleText(): Unknown exception." << std::endl;
		return "";
	}
}



