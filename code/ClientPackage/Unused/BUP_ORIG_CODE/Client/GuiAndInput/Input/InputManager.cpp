#include "InputManager.h"



template<> InputManager* Ogre::Singleton<InputManager>::msSingleton = NULL;
InputManager* const InputManager::getSingletonPtr()
{
	return msSingleton;
}


 
InputManager::InputManager()
{
	mOisInput = new OisInput();
}

InputManager::~InputManager()
{
	if (mOisInput)
	{
		delete mOisInput;
		mOisInput = NULL;
	}
}



void InputManager::update(const float timeSinceLastFrame) const
{
	mOisInput->update(timeSinceLastFrame);
}

InputKeyCollector* const InputManager::getInputKeyCollector() const
{
	return mOisInput->getInputKeyCollector();
}

InputMouseCollector* const InputManager::getMouseKeyCollector() const
{
	return mOisInput->getMouseKeyCollector();
}