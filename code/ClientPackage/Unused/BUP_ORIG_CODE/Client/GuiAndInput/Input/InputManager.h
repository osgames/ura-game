#ifndef __INPUTMANAGER_H__
#define __INPUTMANAGER_H__


#include "OisInput.h"
#include <Ogre.h>	// For public Ogre::Singleton<InputManager>

 
class InputManager : public Ogre::Singleton<InputManager>
{
protected:
	OisInput* mOisInput;
public:
	InputManager();
	~InputManager();

	static InputManager* const getSingletonPtr();

	// Updating all events - Must be called in the main loop //
	void update(const float timeSinceLastFrame) const;
	
	InputKeyCollector* const getInputKeyCollector() const;
	InputMouseCollector* const getMouseKeyCollector() const;
};


#endif