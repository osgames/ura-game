/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __INPUTKEYCOLLECTOR_H__
#define __INPUTKEYCOLLECTOR_H__


#include <OIS/OIS.h>
 

// Define the max amount of keys registered simultaneously //
#define DEF_INPUTKEYCOLLECTOR_MAX_KEYS	3



class InputKeyCollector
{
public:
	// Use the same ois enum value keys.
	// IC = Input Collector. 
	// We can use the same name for the mouse for example IC_MOUSE_LEFT
	enum KEY
	{
		IC_KEY_A = OIS::KC_A,
		IC_KEY_B = OIS::KC_B,
		IC_KEY_C = OIS::KC_C,
		IC_KEY_D = OIS::KC_D,
		IC_KEY_E = OIS::KC_E,
		IC_KEY_F = OIS::KC_F,
		IC_KEY_G = OIS::KC_G,
		IC_KEY_H = OIS::KC_H,
		IC_KEY_I = OIS::KC_I,
		IC_KEY_J = OIS::KC_J,
		IC_KEY_K = OIS::KC_K,
		IC_KEY_L = OIS::KC_L,
		IC_KEY_M = OIS::KC_M,
		IC_KEY_N = OIS::KC_N,
		IC_KEY_O = OIS::KC_O,
		IC_KEY_P = OIS::KC_P,
		IC_KEY_Q = OIS::KC_Q,
		IC_KEY_R = OIS::KC_R,
		IC_KEY_S = OIS::KC_S,
		IC_KEY_T = OIS::KC_T,
		IC_KEY_U = OIS::KC_U,
		IC_KEY_V = OIS::KC_V,
		IC_KEY_W = OIS::KC_W,
		IC_KEY_X = OIS::KC_X,
		IC_KEY_Y = OIS::KC_Y,
		IC_KEY_Z = OIS::KC_Z,
		
		IC_KEY_RETURN = OIS::KC_RETURN,
		IC_KEY_ESCAPE = OIS::KC_ESCAPE,
		IC_KEY_TAB = OIS::KC_TAB
	};
protected:
	KEY mKeyPressMovement[DEF_INPUTKEYCOLLECTOR_MAX_KEYS];
	int mKeyPressCount;
public:
	InputKeyCollector();

	bool setKeyPressed(KEY key);
	void setKeyReleased(KEY key);
	 
	int getKeyPressedCount();
	void releaseAllKeys();

	KEY getkey(int index);
	
	bool isKeyPressed1(KEY key);
	bool isKeyPressed2(KEY key1, KEY key2);
	bool isKeyPressed3(KEY key1, KEY key2, KEY key3);
};
 


#endif