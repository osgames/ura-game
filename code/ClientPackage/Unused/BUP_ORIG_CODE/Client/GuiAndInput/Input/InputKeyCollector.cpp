#include "InputKeyCollector.h"



InputKeyCollector::InputKeyCollector()
{
	// We don't have any keys down at the moment //
	mKeyPressCount = 0;
}

bool InputKeyCollector::setKeyPressed(InputKeyCollector::KEY key)
{
	// Only DEF_INPUTKEYCOLLECTOR_MAX_KEYS keys are needed to move;
	if (mKeyPressCount >= DEF_INPUTKEYCOLLECTOR_MAX_KEYS)
	{
		return false;
	}

	mKeyPressMovement[mKeyPressCount] = key;
	mKeyPressCount++;				// Increment the index mKeyPressCount;

	return true;
}

void InputKeyCollector::setKeyReleased(InputKeyCollector::KEY key)
{
	int i = 0;

	// Make sure we wont' get a value of 0 or below. That would cause array corruption;
	if ( mKeyPressCount < 1 )
	{
		return;
	}

	// Find the position of the release key, in the array;
	for (i = 0; i < mKeyPressCount; i++)
	{
		if ( mKeyPressMovement[i] == key)
		{
			break;
		}
	}

	// Give it the value at the end of the array;
	mKeyPressMovement[i] = mKeyPressMovement[ mKeyPressCount - 1 ];

	// Reduce the array count by 1;
	mKeyPressCount--;
}

int InputKeyCollector::getKeyPressedCount()
{
	if ( mKeyPressCount < 0 )
	{
		mKeyPressCount = 0;
	}

	return mKeyPressCount;
}

void InputKeyCollector::releaseAllKeys()
{
	mKeyPressCount = 0;
}
 
InputKeyCollector::KEY InputKeyCollector::getkey(int index)
{
	// If the index is beyond its limits.....return a halt movement;
	//if (index < 0 || index >= DEF_INPUTKEYCOLLECTOR_MAX_KEYS)
	//{
	//	return INPUT_KEY_COLLECTOR_KEY_NONE;
	//}

	return mKeyPressMovement[index];
}

bool InputKeyCollector::isKeyPressed1(InputKeyCollector::KEY key)
{
	for (int i = 0; i < mKeyPressCount; i++) {
	if ( mKeyPressMovement[i] == key )
	    return true;
	}
	return false;
}

bool InputKeyCollector::isKeyPressed2(InputKeyCollector::KEY key1, InputKeyCollector::KEY key2)
{
	const int gmSize = 2;
	InputKeyCollector::KEY gmArray[gmSize];
	int countSuccesses = 0;

	gmArray[0] = key1;
	gmArray[1] = key2;

	// Check if all the keys are in the array;
	for (int i = 0; i < gmSize; i++)
	{
		for (int j = 0; j < mKeyPressCount; j++)
		{
			if ( mKeyPressMovement[j] == gmArray[i])
			{
				countSuccesses++;		// We found a match;
				break;
			}
		}
	}

	// Check if we got all the keys;
	if (countSuccesses == gmSize)
	{
		return true;
	}

	return false;
}

bool InputKeyCollector::isKeyPressed3(InputKeyCollector::KEY key1, InputKeyCollector::KEY key2, InputKeyCollector::KEY key3)
{
	const int gmSize = 3;
	InputKeyCollector::KEY gmArray[gmSize];
	int countSuccesses = 0;

	gmArray[0] = key1;
	gmArray[1] = key2;
	gmArray[2] = key3;

	// Check if all the keys are in the array;
	for (int i = 0; i < gmSize; i++)
	{
		for (int j = 0; j < mKeyPressCount; j++)
		{
			if ( mKeyPressMovement[j] == gmArray[i])
			{
				countSuccesses++;		// We found a match;
				break;
			}
		}
	}

	// Check if we got all the keys;
	if (countSuccesses == gmSize)
	{
		return true;
	}

	return false;
}



