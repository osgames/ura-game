#include "InputMouseCollector.h"


InputMouseCollector::InputMouseCollector()
{
	mbLeftButtonPressed = false;
	mbRightButtonPressed = false;
	mbMoving = false;

	mMoveRelativeX = 0;
	mMoveRelativeY = 0;
}


/////////////////////
/// SET FUNCTIONS ///
/////////////////////
void InputMouseCollector::setLeftButtonPressed()
{
	mbLeftButtonPressed = true;
}

void InputMouseCollector::setRightButtonPressed()
{
	mbRightButtonPressed = true;
}

void InputMouseCollector::setLeftButtonReleased()
{
	mbLeftButtonPressed = false;
}

void InputMouseCollector::setRightButtonReleased()
{
	mbRightButtonPressed = false;
}

void InputMouseCollector::setNotMoving()
{
	mbMoving = false;
}

void InputMouseCollector::setMoving(const int relativeX, const int relativeY)
{
	mbMoving = true;
	mMoveRelativeX = relativeX;
	mMoveRelativeY = relativeY;
}


///////////////////////
/// CHECK FUNCTIONS ///
///////////////////////
bool InputMouseCollector::isButtonPressed() const
{
	return (mbLeftButtonPressed || mbRightButtonPressed);
}

bool InputMouseCollector::isLeftButtonPressed() const
{
	return mbLeftButtonPressed;
}

bool InputMouseCollector::isRightButtonPressed() const
{
	return mbRightButtonPressed;
}

bool InputMouseCollector::isMoving() const
{
	return mbMoving;
}


/////////////////////
/// GET FUNCTIONS ///
/////////////////////
int InputMouseCollector::getMoveRelativeX() const
{
	return mMoveRelativeX;
}


int InputMouseCollector::getMoveRelativeY() const
{
	return mMoveRelativeY;
}