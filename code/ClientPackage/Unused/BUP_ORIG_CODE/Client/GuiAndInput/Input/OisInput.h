/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009 Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __OISINPUT_H__
#define __OISINPUT_H__


#include <OIS/OIS.h>
#include <CEGUI/CEGUI.h>
#include "InputKeyCollector.h"
#include "InputMouseCollector.h"


class OisInput : public OIS::KeyListener, public OIS::MouseListener
{
protected:
	OIS::InputManager* mOIS_InputManager;
	OIS::Keyboard* mOIS_keyboard;
	OIS::Mouse* mOIS_Mouse;

	CEGUI::GUIContext* mGuiContext;

	// A keyboard input collector //
	InputKeyCollector* mInputKeyCollector;

	// A Mouse input collector //
	InputMouseCollector* mInputMouseCollector;


	CEGUI::MouseButton _convertToCeguiMouseButton(OIS::MouseButtonID buttonID) const;

	virtual bool keyPressed (const OIS::KeyEvent& arg);
	virtual bool keyReleased (const OIS::KeyEvent& arg);

	virtual bool mouseMoved (const OIS::MouseEvent& arg);
	virtual bool mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
	virtual bool mouseReleased (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
public:
	OisInput ();
	~OisInput();

	OIS::Keyboard* const getKeyboard() const;
	OIS::Mouse* const getMouse() const;

	void update(const float timeSinceLastFrame) const;
	InputKeyCollector* const getInputKeyCollector() const;
	InputMouseCollector* const getMouseKeyCollector() const;
};
 
#endif
