/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Ogre3dManager.h"

// For the Client Window Caption Text //
#include "ClientGlobals.h"



template<> Ogre3dManager* Ogre::Singleton<Ogre3dManager>::msSingleton = 0;
Ogre3dManager* const Ogre3dManager::getSingletonPtr()
{
	return msSingleton;
}



Ogre3dManager::Ogre3dManager()
{
	mRoot = new Ogre::Root();


	if( !mRoot->restoreConfig() )
	{
		if( !mRoot->showConfigDialog() )
		{
			return;
		}
	}
	

	
	/*** Set the Window Caption Text and create the scene Manager ***/
	mRenderWindow = mRoot->initialise(true, DEF_CLIENTGLOBALS_CAPTION_TEXT);
 	mSceneManager = mRoot->createSceneManager( Ogre::ST_GENERIC, "sceneManager" );


	/*** Create the main camera and set its default properties ***/
	mCamera = mSceneManager->createCamera( "camera" );
	mCamera->setPosition( Ogre::Vector3( 0.0f,0.0f,0.0f ) );
	mCamera->lookAt( Ogre::Vector3( 0.0f,0.0f,0.0f ) );
	mCamera->setNearClipDistance( 5.0f );


	/*** Add the viewport and set the background color ***/
	mViewport = mRenderWindow->addViewport( mCamera );
	mViewport->setBackgroundColour( Ogre::ColourValue( 0.0f,0.0f,0.0f ) );


	/*** Set the camera default aspect ratio ***/
	mCamera->setAspectRatio( Ogre::Real( mViewport->getActualWidth() ) 
		/ Ogre::Real( mViewport->getActualHeight() ) );


	/*** Set the default Ambient light ***/
	mSceneManager->setAmbientLight( Ogre::ColourValue( 0.0f, 0.0f, 0.0f, 0.0f ) );


	/*** Create the Ogre3d resource manager which loads the resources from the resources file ***/
	mOgre3dResourceManager = new Ogre3dResourceManager;


	/*** Create the log manager ***/
	mOgre3dLogManager = new Ogre3dLogManager;
} 

Ogre3dManager::~Ogre3dManager()
{
	if (mRoot)
	{
		delete mRoot;
		mRoot = NULL;
	}

	if (mOgre3dResourceManager)
	{
		delete mOgre3dResourceManager;
		mOgre3dResourceManager = NULL;
	}

	if (mOgre3dLogManager)
	{
		delete mOgre3dLogManager;
		mOgre3dLogManager = NULL;
	}
}

 

Ogre::Root* const Ogre3dManager::getRoot() const
{
	return mRoot;
}

Ogre::SceneManager* const Ogre3dManager::getSceneManager() const
{
	return mSceneManager;
}

Ogre::RenderWindow* const Ogre3dManager::getRenderWindow() const
{
	return mRenderWindow;
}

Ogre::Camera* const Ogre3dManager::getCamera() const
{
	return mCamera;
}


Ogre3dLogManager* const Ogre3dManager::getOgre3dLogManager() const
{
	return mOgre3dLogManager;
}

Ogre::Viewport* const Ogre3dManager::getViewport() const
{
	return mViewport;
}

 
 
void Ogre3dManager::startRendering() const
{
	if (mRoot)
	{
		try
		{
			mRoot->startRendering();
		}
		catch( Ogre::Exception& e )
		{
			mOgre3dLogManager->logErrorMessage(
				"Ogre3dManager::startRendering()", e.getFullDescription().c_str() );
		}
	}
}

void Ogre3dManager::addFrameListener(Ogre::FrameListener* const frameListener) const
{
	if (mRoot)
	{
		mRoot->addFrameListener(frameListener);
	}
}