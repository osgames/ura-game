/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SoundManager.h"
#include "Sound.h"
#include "ClientGlobals.h"



template<> SoundManager* Ogre::Singleton<SoundManager>::msSingleton = NULL;
SoundManager* SoundManager::getSingletonPtr()
{
	return msSingleton;
}

 

SoundManager::SoundManager()
{
	// Initialize OpenAL and clear the error bit.
    alutInit(0, 0);
    alGetError();

	// Set the resource directory //
	SoundManager::setResourcesDirectory(DEF_CLIENTGLOBALS_SOUNDMANAGER_RESOURCE_DIR);

	setPlayerPosition( Ogre::Vector3(0.0, 0.0, 0.0) );
	setPlayerVelocity( Ogre::Vector3(0.0, 0.0, 0.0) );
	setPlayerOrientation( Ogre::Quaternion() );
}

SoundManager::~SoundManager()
{
	for( std::list<Sound *>::iterator it = mSoundList.begin(); it != mSoundList.end(); it++ )
	{
		delete (*it);
	}
	mSoundList.clear();

	alutExit();
}


void SoundManager::setPlayerPosition( Ogre::Vector3 pos )
{
	mListenerPos[0] = pos.x;
	mListenerPos[1] = pos.y;
	mListenerPos[2] = pos.z;
	alListenerfv( AL_POSITION, mListenerPos );
}

void SoundManager::setPlayerVelocity( Ogre::Vector3 vel )
{
	mListenerVel[0] = vel.x;
	mListenerVel[1] = vel.y;
	mListenerVel[2] = vel.z;
	alListenerfv( AL_VELOCITY, mListenerVel );
}

void SoundManager::setPlayerOrientation(Ogre::Quaternion quat)
{
	mOrientation = quat;

	// Orientation of the listener : look at then look up
	Ogre::Vector3 axis = Ogre::Vector3::ZERO;
	axis.x = mOrientation.getYaw().valueRadians();
	axis.y = mOrientation.getPitch().valueRadians();
	axis.z = mOrientation.getRoll().valueRadians();

	// Set the direction
	ALfloat dir[] = { axis.x, axis.y, axis.z };

	alListenerfv( AL_ORIENTATION, dir );
}

Sound* SoundManager::createSound( std::string sound_file )
{
	Sound *sound = new Sound( mResourcesDirectory + sound_file );
	mSoundList.push_back( sound );
	return sound;
}

bool SoundManager::destroySound( Sound *s )
{
	if( s )
	{
		for( std::list<Sound *>::iterator it = mSoundList.begin(); it != mSoundList.end(); it++ )
		{
			if( (*it) == s )
			{
				it = mSoundList.erase( it );
				delete s;
				return true;
			}
		}
	}
	return false;
}


void SoundManager::setResourcesDirectory( std::string dir )
{
	if( dir[dir.length()-1] != 47 && dir[dir.length()-1] != 92 )
		dir = dir + char(47);
	mResourcesDirectory = dir;
}

Sound *SoundManager::getSound( std::string sound_file )
{
	for( std::list<Sound *>::iterator it = mSoundList.begin(); it != mSoundList.end(); it++ )
	{
		if( (*it)->getFile() == (mResourcesDirectory + sound_file) )
		{
			return (*it);
		}
	}

	return 0;
}
