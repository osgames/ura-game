/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ClientStateManager.h"

#include "Ogre3dManager.h"
#include "CeguiManager.h"
#include "InputManager.h"
#include "SoundManager.h"

#include "Game.h"
#include "MainMenu.h"
#include "ServerMenu.h"
#include "CharacterMenu.h"




template<> ClientStateManager* Ogre::Singleton<ClientStateManager>::msSingleton = NULL;
ClientStateManager* const ClientStateManager::getSingletonPtr()
{
	return msSingleton;
}


 

ClientStateManager::ClientStateManager() : 
	mMainMenu(NULL), 
	mServerMenu(NULL), 
	mCharacterMenu(NULL),  
	mGame(NULL)
{
	/// Create all the main starter Objects ///
	new Ogre3dManager();
	new CeguiManager();
	new InputManager();
	new SoundManager();
	new Network();
	 

	/// Initialize the current state to None ///
	mCurrentState = AvailableStates::AS_NONE;

	/// Change the state to the main menu ///
	ClientStateManager::setState(AS_MAIN_MENU);

	/// Create the main loop: This should always be at the bottom ///
	mClientLoop = new ClientLoop();
}

ClientStateManager::~ClientStateManager()
{
	Ogre3dManager* ogre3dManager = Ogre3dManager::getSingletonPtr();
	CeguiManager* ceguiManager = CeguiManager::getSingletonPtr();
	InputManager* inputManager = InputManager::getSingletonPtr();
	SoundManager* soundManager = SoundManager::getSingletonPtr();
	Network* network = Network::getSingletonPtr();


	////////////////////////////////////
	/// Destroy the changing objects ///
	////////////////////////////////////
	this->_destroyDynamicObjects();

	////////////////////////////
	/// Free Everything else ///
	////////////////////////////
	if (ogre3dManager)
	{
		delete ogre3dManager;
		ogre3dManager = NULL;
	}

	if (ceguiManager)
	{
		delete ceguiManager;
		ceguiManager = NULL;
	}

	if (inputManager)
	{
		delete inputManager;
		inputManager = NULL;
	}

	if (soundManager)
	{
		delete soundManager;
		soundManager = NULL;
	}

	if (network)
	{
		delete network;
		network = NULL;
	}
	
	if (mClientLoop)
	{
		delete mClientLoop;
		mClientLoop = NULL;
	}
}
  

void ClientStateManager::_destroyDynamicObjects()
{
	if (mGame)
	{
		delete mGame; 
		mGame = NULL;
	}
	
	if (mCharacterMenu)
	{
		delete mCharacterMenu; 
		mCharacterMenu = NULL;
	}
	  
	if (mServerMenu)
	{
		delete mServerMenu; 
		mServerMenu = NULL;
	}

	if (mMainMenu)
	{
		delete mMainMenu; 
		mMainMenu = NULL;
	}
}



void ClientStateManager::setState(AvailableStates state)
{
	if (mCurrentState != state)
	{
		// Destroy All changable Objects //
		this->_destroyDynamicObjects();
	
		// Create the objects in the new state //
		switch (state)
		{
			case AS_MAIN_MENU:
			{
				mMainMenu = new MainMenu();
				break;
			}
			case AS_SERVER_MENU:
			{
				mServerMenu = new ServerMenu();
				break;
			}
			case AS_CHARACTER_MENU:
			{
				mCharacterMenu = new CharacterMenu();
				break;
			}
			case AS_GAME:
			{
				mGame = new Game();
				break;
			}
			case AS_QUIT:
			{
				// Destory this object //
				ClientStateManager::~ClientStateManager();
			}
		}

		mCurrentState = state;
	}
}
 
  

AvailableStates ClientStateManager::getCurrentState() const
{
	return mCurrentState;
}




MainMenu* const ClientStateManager::getMainMenu() const
{
	return mMainMenu;
}

ServerMenu* const ClientStateManager::getServerMenu() const
{
	return mServerMenu;
}

CharacterMenu* const ClientStateManager::getCharacterMenu() const
{
	return mCharacterMenu;
}

Game* const ClientStateManager::getGame() const
{
	return mGame;
} 