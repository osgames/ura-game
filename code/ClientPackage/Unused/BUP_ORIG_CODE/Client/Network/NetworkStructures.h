/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __NETWORK_STRUCTURES_H__
#define __NETWORK_STRUCTURES_H__

#include <vector>
#include <string.h>
#include <Ogre.h>

class Server
{
public:
	// Server information.
	std::string server_name;
	std::string server_ip_address;
	bool server_status;
};

class Character
{
public:
	std::string name;
	std::string login_username;

	float money;
};

/*
 * Class for pulling player information off of the network.
 * See CPlayer class for finalized players.
 */
class Player
{
public:
	std::string name;

	Ogre::Quaternion orientation;
	Ogre::Vector3 position;

	bool player_character;
};

#endif