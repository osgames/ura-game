/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __NETWORK_H__
#define __NETWORK_H__

#include <Ogre.h>
#include <RakPeer.h>
#include <RakNetworkFactory.h>
#include "../Dependencies/NetworkMessages.h"
#include "NonePlayerCharacter.h"
#include "Game.h"
#include "ClientGlobals.h"	// For the port

class Server;
class Character;
class Player;

 


class Network : public Ogre::FrameListener, public Ogre::Singleton<Network>
{
private:
	Packet *mPacket;
	bool connected, connect_notified;
	//int server_port;
	std::string login_username, login_password;
	Ogre::Timer *mTimer;
	Ogre::Timer *mConnectionTimer;
	std::string master_server_ip;
	bool logged_in;
	Ogre::Timer *mTimeoutTimer;
	Ogre::Timer *mSilenceTimer;
	int iSilenceLength;

	bool frameStarted( const Ogre::FrameEvent &evt );
	bool frameEnded( const Ogre::FrameEvent &evt );

	void sendLoginInfo();
	void parseServerList();
	void parseCharacterList();

	bool bCancelMasterServerConnect;
	
	// Flag to let the network manager know to keep track of a timeout.
	bool bConnectingToMasterServer;

	// Player is intentionally quitting if this is true.
	bool bQuitting;

	// Checks all client-side things that can be handled by executing a command.
	void checkCommandProcedures( std::string cmd );
public:
	Network();
	~Network();

	std::vector<Server *> server_list;
	std::vector<Character *> character_list;
	std::vector<Player *> player_list;
	SystemAddress game_server_address;
	RakPeerInterface *mPeer;

	static Network* const getSingletonPtr();

	bool isConnected();
	void connectToMasterServer();
	void realConnectMasterServer( std::string server_name );
	void connectToServer( std::string _ip );
	void disconnect();

	void insert_login_info( std::string username, std::string password );
	void joinConnectedServer( std::string character_name );
	void emptyPlayerList();
	void sendOrientationUpdate( Ogre::Quaternion orientation );

	// Basically ignores notifications from the RakNet switch (i.e. "Disconnected from server.").
	// @param ms: the number of milliseconds until notifications are no longer silent.
	void silenceNotifications( int ms );

	/*
	 * Sends specified Type ID to the server.  Allows for basic one-way communication from outside classes.
	 */
	void sendIdToServer( int id );

	void sendPositionToServer( Ogre::Vector3 pos );

	void sendChatMessage( std::string message );

	void sendCommand( std::string cmd );

	void request_NPC_interaction( std::string npc_name );

	void request_Full_Inventory_Snapshot();

	void parseMovementChange( std::string character_name, short movement_type );


	void testNPCQuery(); // ONLY TEMPORARY
	NonePlayerCharacter* const npcQuery(std::string &name);

	// Attempts to cancel connection with master server.
	void cancelMasterServerConnect();

	// Called if the client is ready to quit, so it won't show disconnect messages.
	// Should be called again when the client is disconnected successfully.
	void clientQuit( bool q );
};

#endif