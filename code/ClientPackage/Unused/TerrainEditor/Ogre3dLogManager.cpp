/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "Ogre3dLogManager.h"


using namespace std;



Ogre3dLogManager::Ogre3dLogManager()
{

}

Ogre3dLogManager::~Ogre3dLogManager()
{

}



void Ogre3dLogManager::logMessage(const string &str)
{
	Ogre::LogManager* const logManager = Ogre::LogManager::getSingletonPtr();

	logManager->logMessage(str);
}

void Ogre3dLogManager::logMessage(const string &functionName, const string &str)
{
	Ogre::LogManager* const logManager = Ogre::LogManager::getSingletonPtr();
	string tmp;


	tmp = functionName + ": " +  str + ".";

	logManager->logMessage(tmp);
}

void Ogre3dLogManager::logErrorMessage(const string &functionName, const string &errorMessage)
{
	Ogre::LogManager* const logManager = Ogre::LogManager::getSingletonPtr();
	string tmp;


	tmp = "ERROR: " + functionName + ": " +  errorMessage + ".";

	logManager->logMessage(tmp);
}

void Ogre3dLogManager::logErrorMemoryMessage(const string &functionName, const string &variableName)
{
	Ogre::LogManager* const logManager = Ogre::LogManager::getSingletonPtr();
	string tmp;


	tmp = "ERROR: " + functionName + ": Could not allocate memory for '" +  variableName + "'.";

	logManager->logMessage(tmp);
}

