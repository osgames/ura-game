/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "CustomSceneNodeManipulator.h"
#include "Ogre3dManager.h"
#include "CustomSceneNodeManager.h"
#include "EditManager.h"
#include "RayCastManager.h"


using namespace std;
using namespace Ogre;


CustomSceneNodeSelectionTracker::CustomSceneNodeSelectionTracker()
{

}

CustomSceneNodeSelectionTracker::~CustomSceneNodeSelectionTracker()
{

}


void CustomSceneNodeSelectionTracker::insert(Ogre::SceneNode* const sn)
{
	mObjectSelectedList.push_back(sn);
	mStdSetSelectedSceneNodes.insert(sn);
}

void CustomSceneNodeSelectionTracker::erase(Ogre::SceneNode* const sn)
{
	/*********************************************************************************************************************
		NOTE: This is the only function that makes me think I should use std::map instead of std::set.
		For now this function is inefficient, but it doesn't seem to matter.....maybe later i'll change it to std::Map.
	***********************************************************************************************************************/

	std::set<Ogre::SceneNode*>::iterator it;

	for (unsigned long i = 0; i < mObjectSelectedList.size(); i++)
	{
		// If the pointer exists in our list than erase it;
		if ( sn == mObjectSelectedList[i] )
		{
			// Erase the SceneNode from the the std::set;
			it = mStdSetSelectedSceneNodes.find(sn);
			mStdSetSelectedSceneNodes.erase(it);

			// Erase this scene node from the list;
			mObjectSelectedList.erase( mObjectSelectedList.begin() + i );
		}
	}
}

void CustomSceneNodeSelectionTracker::clear()
{
	mObjectSelectedList.clear();
	mStdSetSelectedSceneNodes.clear();
}

bool CustomSceneNodeSelectionTracker::exist(Ogre::SceneNode* const sn)
{
	if ( mStdSetSelectedSceneNodes.find(sn) != mStdSetSelectedSceneNodes.end() )
	{
		return true;
	}

	return false;
}

unsigned long CustomSceneNodeSelectionTracker::getSize()
{
	return mObjectSelectedList.size();
}

bool CustomSceneNodeSelectionTracker::get(unsigned long index, std::string &sceneNodeName)
{
	sceneNodeName = mObjectSelectedList[index]->getName();
	return true;
}













CustomSceneNodeManipulator::CustomSceneNodeManipulator()
{
	mEntityNodeTracker = new CustomSceneNodeSelectionTracker;
	mLightNodeTracker = new CustomSceneNodeSelectionTracker;
	mCurrentNodeTracker = mEntityNodeTracker;
	mMode = ENTITY;
}

CustomSceneNodeManipulator::~CustomSceneNodeManipulator()
{
	if (mEntityNodeTracker)
	{
		delete mEntityNodeTracker;
		mEntityNodeTracker = 0;
	}

	if (mLightNodeTracker)
	{
		delete mLightNodeTracker;
		mLightNodeTracker = 0;
	}
}


void CustomSceneNodeManipulator::select(vector<Ogre::SceneNode*> *pList)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	/*** Turn off the bounding boxes for the old Selected ***/
	for ( unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, false);
		}
	}


	/*** Clear the list ***/
	CustomSceneNodeManipulator::emptySelected();


	/*** Insert the new selected and Turn on the bounding boxes ***/
	for ( unsigned long i = 0; i < pList->size(); i++)
	{
		mCurrentNodeTracker->insert( (*pList)[i] );

		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			tmpSceneNode->showBoundingBox(true);
		}
	}
}

void CustomSceneNodeManipulator::select(Ogre::SceneNode* const sn)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;



	if (sn)
	{
		// If we don't have the pointer already than push back the scenenode and show the bounding box;
		if ( !mCurrentNodeTracker->exist(sn) )
		{
			mCurrentNodeTracker->insert(sn);

			// Show the bounding boxes;
			for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
			{
				if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
				{
					tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
					customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true);
				}
			}
		}
	}	
}

void CustomSceneNodeManipulator::deselect(Ogre::SceneNode* const sn)
{
	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();


	if (sn)
	{
		// If the pointer exists in our list, than turn off the bounding box and erase it;
		if ( mCurrentNodeTracker->exist(sn) )
		{
			// Turn off the bounding box for this scene node;
			customSceneNodeManager->showBoundingBoxToBottom(sn, false);

			// Erase the scenenode;
			mCurrentNodeTracker->erase(sn);
		}
	}
}


void CustomSceneNodeManipulator::copySelected()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();

	Ogre::SceneNode* tmpSceneNode = 0;
	Ogre::SceneNode* oldSceneNode = 0;
	std::string tmpSceneNodeName;
	std::string copiedSceneNodeName;
	std::vector<Ogre::SceneNode*> vNewSceneNodes;



	// Copy the selected scenenodes and store the copied scenenodes pointers in vNewSceneNodes;
	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);

			// Get the original top SceneNode;
			oldSceneNode = customSceneNodeManager->getTopSceneNode(tmpSceneNode);
	
			// Make a copy of the original scenenode;
			if ( customSceneNodeManager->copySceneNode(oldSceneNode, copiedSceneNodeName) )
			{
				// Get a pointer to the new copied SceneNode;
				vNewSceneNodes.push_back( sceneManager->getSceneNode(copiedSceneNodeName) );
			}
		}
	}

	
	// deselect the old Selected list, so that we can select the newly copied Scenenodes;
	CustomSceneNodeManipulator::deselect();


	for (unsigned long i = 0; i < vNewSceneNodes.size(); i++)
	{
		CustomSceneNodeManipulator::select( vNewSceneNodes[i] );
	}
}


void CustomSceneNodeManipulator::emptySelected()
{
	mCurrentNodeTracker->clear();
}

void CustomSceneNodeManipulator::deselect()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;


	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, false);
		}
	}


	CustomSceneNodeManipulator::emptySelected();
}

void CustomSceneNodeManipulator::deleteSelected()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	
	Ogre::SceneNode* tmpSceneNode = 0;
	Ogre::SceneNode* topSceneNode = 0;

	std::string tmpSceneNodeName;



	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);

	
			// Check if the scenenode exists before we delete it;
			if ( sceneManager->hasSceneNode(tmpSceneNodeName) )
			{
				// In order to save time we don't want to turn the bounding boxes off before we delete the nodes.
				// Instead, we get the topnode then delete what needs to be deleted, afterwards we use the top node
				// to turn off the bounding boxes for whats left;
				topSceneNode = customSceneNodeManager->getTopSceneNode(tmpSceneNode);
	

				// Delete all the entities and objects first;
				customSceneNodeManager->destroyMovableObjectsToBottom(tmpSceneNode);
	

				// Remove and destroy all the children and then destroy the sceneNode;
				tmpSceneNode->removeAndDestroyAllChildren();
				sceneManager->destroySceneNode(tmpSceneNodeName);
	
				// Check if the scenenode exist before we turn off the bounding box.
				// The reason is because the parent sometimes gets deleted;
				if ( sceneManager->hasSceneNode(topSceneNode->getName()) )
				{
					customSceneNodeManager->showBoundingBoxTopToBottom(topSceneNode, false);
				}
			}
		}
	}


	CustomSceneNodeManipulator::emptySelected();
}

void CustomSceneNodeManipulator::yaw(Ogre::Real degree)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;


	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true);
			tmpSceneNode->yaw(Ogre::Radian(Ogre::Degree(degree)), Ogre::Node::TS_LOCAL);
		}
	}	
}

void CustomSceneNodeManipulator::roll(Ogre::Real degree)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true );
			tmpSceneNode->roll(Ogre::Radian(Ogre::Degree(degree)), Ogre::Node::TS_LOCAL);
		}
	}	
}

void CustomSceneNodeManipulator::pitch(Ogre::Real degree)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true);
			tmpSceneNode->pitch(Ogre::Radian(Ogre::Degree(degree)), Ogre::Node::TS_LOCAL);	
		}
	}	
}


void CustomSceneNodeManipulator::translateX(Ogre::Real degree)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true);
			tmpSceneNode->translate(Ogre::Vector3(degree,0.0f,0.0f), Ogre::Node::TS_LOCAL);
		}
	}
}

void CustomSceneNodeManipulator::translateY(Ogre::Real degree)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true);
			tmpSceneNode->translate(Ogre::Vector3(0.0f,degree,0.0f), Ogre::Node::TS_LOCAL);
		}
	}
}

void CustomSceneNodeManipulator::translateZ(Ogre::Real degree)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			customSceneNodeManager->showBoundingBoxTopToBottom(tmpSceneNode, true);
			tmpSceneNode->translate(Ogre::Vector3(0.0f,0.0f,degree), Ogre::Node::TS_LOCAL);
		}
	}
}

void CustomSceneNodeManipulator::showBoundingBox(bool visibility) 
{
  Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
  Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
  
  Ogre::SceneNode* tmpSceneNode = 0;
  std::string tmpSceneNodeName;
  
  for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)  {
    if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) ) {
	  tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
	  tmpSceneNode->showBoundingBox(visibility);
    }
  }
}

void CustomSceneNodeManipulator::scale(Ogre::Real x, Ogre::Real y, Ogre::Real z)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	Ogre::SceneNode* tmpSceneNode = 0;
	std::string tmpSceneNodeName;

	for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
	{
		if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
		{
			tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
			tmpSceneNode->scale(x, y, z);
		}
	}
}


void CustomSceneNodeManipulator::attach(Ogre::SceneNode* const parent)
{
	/****************************************************************
	if (parent)
	{
		unsigned long selectSize =  mObjectSelectedList.size();


		if (selectSize > 0)
		{
			Ogre::Vector3 childPosition;
			Ogre::Vector3 parentPosition;
			Ogre::Vector3 newPosition;
			bool bSuccess = true;

			// Turn off the selected bounding boxes.
			for (unsigned long i = 0; i < selectSize; i++)
			{
				customSceneNodeManager->showBoundingBoxTopToBottom( mObjectSelectedList[i], false );
			}

			// When a Scenenode above tries to get attached to a lower already connected scenenode,
			//  the game will crash. We stop that from happening with a check function.
			// Then we turn off the bounding boxes and clear the selected vector;
			// Else if we try to attach to a selected node than it will crash....so we stop that..
			// and clear everything;
			for (unsigned long i = 0; i < selectSize; i++)
			{
				if ( customSceneNodeManager->isAboveSceneNode(mObjectSelectedList[i], parent) )
				{
					bSuccess = false;
				}
				else if ( parent == mObjectSelectedList[i] )		// Don't allow to attach to a selected node;
				{
					bSuccess = false;
				}
			}


			if (bSuccess)
			{
				// Store the Parent position;
				parentPosition = parent->_getDerivedPosition();

				//// Attach the sceneNodes and set their position ////
				for (unsigned long i = 0; i < selectSize; i++)
				{
					//////
					/////	We want to attach all the scene nodes to the parent node node.
					////	Also what we want to do is remember the world positions and change
					////	them back after a scene node change. The reason is because when
					////	the node is changed the positions get changed to be relative to the parents;
					/////


					// Store the child node position;
					childPosition = mObjectSelectedList[i]->_getDerivedPosition();


					// Calculate the new position by subtracting the
					// child world position from the parent world position;
					newPosition = childPosition - parentPosition;


					mObjectSelectedList[i]->getParent()->removeChild(mObjectSelectedList[i]);

					// If there's an error then clean up and return;
					try
					{
						// Keep its direction safe;
						mObjectSelectedList[i]->setInheritOrientation(false);

						// Add the child;
						parent->addChild(mObjectSelectedList[i]);

						// Turn it back on;
						mObjectSelectedList[i]->setInheritOrientation(true);
					}
					catch(...)
					{
						cout << "ModelEditorMainTab::mouseLeftButtonDown():"
							<< "mRadioEditModeAttachNode->isSelected():"
							<< "ERROR: an unknown error ocurred while attaching entities."
							<< endl;

						// There was an error so break the loop;
						break;
					}

					// The last selected object before the attach doesn't show the bounding box.
					// So lets show it;
					mObjectSelectedList.at(selectSize - 1)->showBoundingBox( true );

					// Set the new position;
					mObjectSelectedList[i]->setPosition(newPosition);
				}
			}


			// Turn off the bounding box for them;
			customSceneNodeManager->showBoundingBoxTopToBottom( parent, false );

			// Clear the vector;
			mObjectSelectedList.clear();

			// Clear the std::set;
			mStdSetSelectedSceneNodes.clear();
		}
	}
	************************************************************/
}

void CustomSceneNodeManipulator::detach()
{
	/********************************************
	// Turn off the bounding box, remove and erase it;
	for (unsigned long i = 0; i < mObjectSelectedList.size(); i++)
	{
		// Turn off the bounding box for them;
		customSceneNodeManager->showBoundingBoxTopToBottom( mObjectSelectedList[i], false );

		// remove the scenenodes;
		customSceneNodeManager->removeAllSceneNodeToBottom_putToRoot(mObjectSelectedList[i]);

		// Erase the SceneNode from the the std::set;
		mStdSetSelectedSceneNodes.erase( mObjectSelectedList[i] );

		// Erase this scene node from the list;
		mObjectSelectedList.erase( mObjectSelectedList.begin() + i );
	}
	*****************************************/
}

void CustomSceneNodeManipulator::drag()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();

	RayCastManager* const raycastManager = EditManager::getSingletonPtr()->getRayCastManager();
	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();

	Ogre::SceneNode* tmpSceneNode = 0;
	Ogre::SceneNode* topSceneNode = 0;
	std::string tmpSceneNodeName;

	Ogre::Vector3 cursorPos;
	Ogre::Vector3 distanceFromMouse;
	Ogre::Vector3 sceneNodePos;

	Ogre::Vector3 raycastNormal;
	Ogre::Vector3 raycastResult;

	Ogre::Real height = static_cast<Ogre::Real>(0.0);



	// We Get the top SceneNode if there is one. Then we get the first scenenodes x,z position and its position from the mouse.
	// We use that Position to relate all the other scenenodes to it. We add that position to the other scenenodes.
	if ( raycastManager->getPositionAtCursor(cursorPos) )
	{
		// Make sure we have atleast 1 scenenode selected;
		if ( mCurrentNodeTracker->getSize() > 0 )
		{
			// Get the first Scenenode in index 0 and than get the top scenenode of index 0;
			if ( mCurrentNodeTracker->get(0, tmpSceneNodeName) )
			{
				tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
				topSceneNode = customSceneNodeManager->getTopSceneNode(tmpSceneNode);
	
				if (topSceneNode)
				{
					distanceFromMouse = topSceneNode->getPosition();
					distanceFromMouse.x = cursorPos.x - distanceFromMouse.x;
					distanceFromMouse.z = cursorPos.z - distanceFromMouse.z;
			
					for (unsigned long i = 0; i < mCurrentNodeTracker->getSize(); i++)
					{
						if ( mCurrentNodeTracker->get(i, tmpSceneNodeName) )
						{
							tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
							topSceneNode = customSceneNodeManager->getTopSceneNode(tmpSceneNode);
					
							if (topSceneNode)
							{
								sceneNodePos = topSceneNode->getPosition();	
								
								sceneNodePos.x += distanceFromMouse.x;
								sceneNodePos.z += distanceFromMouse.z;
					
				
								if ( raycastManager->getHeightAtPosition(sceneNodePos.x, sceneNodePos.z, height) )
								{
									sceneNodePos.y = height;	
									topSceneNode->setPosition(sceneNodePos);	
								}
							}
						}
					}	
				}
			}
		}
	}
}

void CustomSceneNodeManipulator::setMode(Mode mode) {
  /*******************************************************************
   *  This switches the manipulation mode to either entity or light. *
   ******************************************************************/
  mMode = mode;
  this->showBoundingBox(false);
  if (mode == CustomSceneNodeManipulator::LIGHT)
    mCurrentNodeTracker = mLightNodeTracker;
  else
    mCurrentNodeTracker = mEntityNodeTracker;
  this->showBoundingBox(true);
}
  

unsigned long CustomSceneNodeManipulator::getSize() const {
  /********************************************************************
   * Gets the size of the set of scene nodes currently being tracked. *
   *******************************************************************/
  return mCurrentNodeTracker->getSize();
}

const Ogre::Light* CustomSceneNodeManipulator::getLight() const {
  /*****************************************************************
   * Returns a pointer to a random light in the set of light nodes.*
   ****************************************************************/
  assert( getSize() > 0 );
  assert( mMode == LIGHT );

  std::string tmpSceneNodeName;
  Ogre::SceneNode* tmpSceneNode = 0;
  Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
  Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
  Ogre::MovableObject* tmpObject;

  mCurrentNodeTracker->get(0, tmpSceneNodeName);
  tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
  for ( int i = 0; i < tmpSceneNode->numAttachedObjects(); ++i ) {
    tmpObject = tmpSceneNode->getAttachedObject(i);
    if ( tmpObject->getMovableType() == "Light")
      return (Ogre::Light*) tmpObject;
  }
  // the function should never reach here.
  return NULL;
}

const std::vector<Ogre::Light*> CustomSceneNodeManipulator::getLights() const {
    /*****************************************************************
   * Returns a pointer to a random light in the set of light nodes.*
   ****************************************************************/
  assert( getSize() > 0 );
  assert( mMode == LIGHT );

  std::string tmpSceneNodeName;
  Ogre::SceneNode* tmpSceneNode = 0;
  Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
  Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
  Ogre::MovableObject* tmpObject;
  std::vector<Ogre::Light*> lights;

  for (int j = 0; j < getSize(); ++j) {
    mCurrentNodeTracker->get(j, tmpSceneNodeName);
    tmpSceneNode = sceneManager->getSceneNode(tmpSceneNodeName);
    for ( int i = 0; i < tmpSceneNode->numAttachedObjects(); ++i ) {
      tmpObject = tmpSceneNode->getAttachedObject(i);
      if ( tmpObject->getMovableType() == "Light") {
	lights.push_back( (Ogre::Light*) tmpObject );
	continue;
      }
    }
  }
  // the function should never reach here.
  return lights;
}
