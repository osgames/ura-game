/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CEGUI_MANAGER_H
#define CEGUI_MANAGER_H


#include <string>
#include <OIS.h>
#include <CEGUI.h>
#include <Ogre.h>
#include <OgreCEGUIRenderer.h>

#include "CeguiCursorManager.h"



class CeguiManager : public Ogre::Singleton<CeguiManager>
{
protected:
	CEGUI::OgreCEGUIRenderer* mOgreCeguiRenderer;
	CEGUI::System* mSystem;
	CEGUI::Window* mParentWindow;

	CeguiCursorManager* mCeguiCursorManager;
public:
	CeguiManager();
	~CeguiManager();

	static CeguiManager* const getSingletonPtr();

	CEGUI::System* const getSystem() const;
	CeguiCursorManager* const getCeguiCursorManager() const;
	CEGUI::Window* const getParentWindow() const;

	std::string getImagePropertyString(const std::string& name) const;
};


#endif 

