/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "TerrainManager.h"
#include "EditManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "CameraManager.h"
#include "CeguiManager.h"
#include "CeguiCursorManager.h"
#include "WorldEditorProperties.h"


using namespace std;



TerrainManager::TerrainManager() : mTerrain(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "TerrainManager::TerrainManager(...)";
	string strErrorWordName;


	/*** Create the sun light ***/
	mSunLight = sceneManager->createLight("Sun");
	mSunLight->setType(Ogre::Light::LT_DIRECTIONAL);
	mSunLight->setDirection(-0.808863, -0.488935, -0.326625);


	/*** Allocate memory ***/
	mSplatManager = new SplatManager;

	mMapWidth = new Ogre::Real;
	mMapHeight = new Ogre::Real;
	mBrushPosition = new Ogre::Vector3;
	mBrushSize = new Ogre::Vector2;
	mTerrainFlattenHeight = new Ogre::Real;


	/*** Check for memory allocation error ***/
	if (!mSplatManager)
	{
		strErrorWordName = "mSplatManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}


	if (!mMapWidth)
	{
		strErrorWordName = "mMapWidth";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mMapHeight)
	{
		strErrorWordName = "mMapHeight";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mBrushPosition)
	{
		strErrorWordName = "mBrushPosition";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mBrushSize)
	{
		strErrorWordName = "mBrushSize";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mTerrainFlattenHeight)
	{
		strErrorWordName = "mTerrainFlattenHeight";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}


	/*** Set the terrain default Flatten height ***/
	*mTerrainFlattenHeight = 0;
}

TerrainManager::~TerrainManager()
{
	if (mSplatManager)
	{
		delete mSplatManager;
		mSplatManager = NULL;
	}

	if (mTerrain)
	{
		// Destroy all the brushes;
		mTerrain->destroyAllBrushes();

		delete mTerrain;
		mTerrain = NULL;
	}


	if (mMapWidth)
	{
		delete mMapWidth;
		mMapWidth = NULL;
	}

	if (mMapHeight)
	{
		delete mMapHeight;
		mMapHeight = NULL;
	}

	if (mBrushPosition)
	{
		delete mBrushPosition;
		mBrushPosition = NULL;
	}

	if (mBrushSize)
	{
		delete mBrushSize;
		mBrushSize = NULL;
	}

	if (mTerrainFlattenHeight)
	{
		delete mTerrainFlattenHeight;
		mTerrainFlattenHeight = NULL;
	}
}



void TerrainManager::onFrameStart(unsigned long* const timeSinceLastFrame)
{
	if (mTerrain)
	{
		mTerrain->onFrameStart(*timeSinceLastFrame);
	}
}

void TerrainManager::onFrameEnd(unsigned long* const timeSinceLastFrame)
{
	if (mTerrain)
	{
		mTerrain->onFrameEnd(*timeSinceLastFrame);
	}
}




SPT::Terrain* TerrainManager::getTerrain()
{
	return mTerrain;
}

SplatManager* const TerrainManager::getSplatManager()
{
	return mSplatManager;
}




void TerrainManager::setNewTerrain(Ogre::Real width, Ogre::Real maxHeight)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();
	Ogre::Camera* const camera = cameraManager->getCamera();


	// Make sure we don't try to create another SPT::Terrain when the previous one wasn't deleted;
	if (mTerrain)
	{
		delete mTerrain;
		mTerrain = NULL;
	}


	if (!mTerrain)
	{
		/*
		***	Setup the new SPT::Terrain.
		 */


		// Set the width and height;
		*mMapWidth = width;
		*mMapHeight = maxHeight;


		/*** Setup the terrain ***/
		mTerrain = new SPT::Terrain(camera, sceneManager);

		// image, nothing, X and Z width, height;
		mTerrain->quickSetup(TERRAIN_DEFAULT_HEIGHTMAP_FILENAME, 1025, SPLATMAP_DEFAULT_MATERIALNAME, *mMapWidth, *mMapHeight);


		#ifndef DISABLE_SPT_LIGHTMAP_CODE
			// Create the Atmosphere;
			mTerrain->createAtmosphere(	637800,638640,
							-0.985148,
							95.0213,
							640000.0,
							Ogre::Vector3(2.9427e-006, 9.4954e-006, 1.40597e-005),
							Ogre::Vector3(9.43648e-005, 3.36762e-005, 6.59358e-006),
							"SkySphere",512,256);
			mTerrain->createLightmapper("Lightmapper","SPT_Comp", 2048, 2049);
			mTerrain->setAutoUpdateLightmap(false);
			mTerrain->setLightDirection( mSunLight->getDirection() );
		#endif


		/*** Hide the spt brush ***/
		TerrainManager::showBrush(false);



		/*** Setup the SplatManager ***/
		if (mSplatManager)
		{
			mSplatManager->setup(*mMapWidth, 2048, 2048, SPLATMAP_DEFAULT_MATERIALNAME);
		}
	}
}

bool TerrainManager::setHeightMapData(string &data, unsigned long long &dataSize)
{
	Ogre::StringConverter strConverter;


	SPT::Heightmap* const hMap = mTerrain->getHeightmap();
	SPT::HEIGHTMAPTYPE* const pHeightMap = hMap->getData();
	SPT::HEIGHTMAPTYPE* hMapHeight;

	string heightMapDataStr;
	unsigned long long heightMapDataStrSize = data.size();
	Ogre::Real tmpVal = 0.0;

	unsigned long long strIndex = 0;
	long long counter = 0;
	string tmpStr;



	if (heightMapDataStrSize != dataSize)
	{
		return false;
	}


	while (strIndex < heightMapDataStrSize)
	{
		while ( strIndex < heightMapDataStrSize && data.at(strIndex) != ',' )
		{
			tmpStr += data.at(strIndex);

			strIndex++;
		}

		// increment pass the comma;
		strIndex++;


		// convert the string to a real than to SPT::HEIGHTMAPTYPE;
		tmpVal = (SPT::HEIGHTMAPTYPE) strConverter.parseReal(tmpStr);

		hMapHeight = &tmpVal;
		pHeightMap[counter] = *hMapHeight;

		counter++;

		tmpStr.clear();
	}


	// We have to call the update the heightmap function;
	mTerrain->updateHeightmap();


	// This code can sometimes cause problems with 3d cards, so allow it to be disabled;
	// Update the lightmap;
	#ifndef DISABLE_SPT_LIGHTMAP_CODE
		mTerrain->updateLightmap();
	#endif


	return true;
}

void TerrainManager::updateLightMap()
{
	if (mTerrain)
	{
		// This code can sometimes cause problems with 3d cards, so allow it to be disabled;
		// Update the lightmap;
		#ifndef DISABLE_SPT_LIGHTMAP_CODE
			mTerrain->updateLightmap();
		#endif
	}
}



bool TerrainManager::getTerrainLoaded() const
{
	return mTerrain;
}

Ogre::Real TerrainManager::getWidth() const
{
	return *mMapWidth;
}

Ogre::Real TerrainManager::getHeight() const
{
	return *mMapHeight;
}



void TerrainManager::setBrushSize(Ogre::Vector2 size)
{
	*mBrushSize = size;
}

void TerrainManager::setBrush( std::string brush_name )
{
	if (mTerrain)
	{
		mTerrain->destroyAllBrushes();
		mBrush = mTerrain->createBrush(brush_name);
		mTerrain->initializeBrushDecal( mBrush->getTexName(), *mBrushSize );
	}
}



void TerrainManager::setBrushPositionToCursor()
{
	if (mTerrain)
	{
		CameraManager* const camManager = EditManager::getSingletonPtr()->getCameraManager();
		CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();
		Ogre::Vector2 cursorPos;


		if ( ceguiCursorManager->getCursorScreenPosition(cursorPos) )
		{
			/*
			 * We use the getRayHeightOptimized() function instead of the getRayHeight() function.
			 * The reason being the getRayHeight() function becomes slow when the mouse
			 * Cursor isn't over the terrain. So we used a special function to deal with that.
			 */

			mTerrain->getRayHeightOptimized(camManager->getCameraSceneNode()->getPosition(),
				camManager->getCamera()->getCameraToViewportRay(cursorPos.x, cursorPos.y), *mBrushPosition);

			mTerrain->setBrushPosition(*mBrushPosition);
		}
	}
}

Ogre::Vector3 TerrainManager::getBrushPosition()
{
	return *mBrushPosition;
}

bool TerrainManager::getBrushVisible()
{
	if (mTerrain)
	{
		return mTerrain->getBrushVisible();
	}

	return false;
}

void TerrainManager::showBrush(bool show)
{
	if (mTerrain)
	{
		mTerrain->setBrushVisible(show);
	}
}

void TerrainManager::displaceTerrain(Ogre::Real intensity)
{
	if (mTerrain)
	{
		mTerrain->addTerrainModifier(SPT::BrushDisplacement(mBrush, *mBrushPosition, mBrushSize->x, mBrushSize->y, intensity));
	}
}

void TerrainManager::setTerrainFlattenHeightToCursor()
{
	CameraManager* const camManager = EditManager::getSingletonPtr()->getCameraManager();
	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();
	Ogre::Vector2 cursorPos;
	Ogre::Vector3 pos;


	if (mTerrain)
	{
		if ( ceguiCursorManager->getCursorScreenPosition(cursorPos) )
		{
			if( mTerrain->getRayHeight(camManager->getCamera()->getCameraToViewportRay(cursorPos.x, cursorPos.y), pos) )
			{
				*mTerrainFlattenHeight = pos.y;
			}
		}
	}
}

void TerrainManager::flattenTerrain()
{
	if (mTerrain)
	{
		mBrushPosition->y = *mTerrainFlattenHeight;

		mTerrain->addTerrainModifier( SPT::BrushDisplacement(mBrush, *mBrushPosition, mBrushSize->x, mBrushSize->y,
			static_cast<unsigned long>(0)) );
	}
}

void TerrainManager::flattenTerrainToBottom()
{
	if (mTerrain)
	{
		mTerrain->addTerrainModifier( SPT::BrushDisplacement(mBrush, *mBrushPosition, mBrushSize->x, mBrushSize->y,
			static_cast<unsigned long>(2)) );
	}
}

void TerrainManager::flattenTerrainToCenter()
{
	if (mTerrain)
	{
		mTerrain->addTerrainModifier( SPT::BrushDisplacement(mBrush, *mBrushPosition, mBrushSize->x, mBrushSize->y,
			static_cast<unsigned long>(1)) );
	}
}

