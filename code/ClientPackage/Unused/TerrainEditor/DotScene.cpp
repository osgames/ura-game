/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



/*------------------------------------------------------------------------------
*  DotScene.cpp
*
*  This file provides some methods for the parsing of a .scene file, with
*  support for userData.
*------------------------------------------------------------------------------*/

#include <string>
#include <boost/lexical_cast.hpp>

#include "DotScene.h"
#include "WorldEditorProperties.h"
#include "EditManager.h"
#include "GUIManager.h"
#include "GUI.h"

#include "GameObject.h"

#include "TerrainManager.h"
#include "SplatManager.h"


#include "TerrainMenu.h"
//#include "TerrainMenuSplatTab.h"


static Light* LoadLight( TiXmlElement *XMLLight, SceneManager *mSceneMgr , std::string& lightName)
{
	TiXmlElement *XMLDiffuse, *XMLSpecular, *XMLAttentuation, *XMLPosition;

	// Create a light (point | directional | spot | radPoint)
	lightName = XMLLight->Attribute("name");
	Light* l = mSceneMgr->createLight( lightName );
	if( !XMLLight->Attribute("type") || Ogre::String(XMLLight->Attribute("type")) == "point" )
		l->setType( Light::LT_POINT );
	else if( Ogre::String(XMLLight->Attribute("type")) == "directional")
		l->setType( Light::LT_DIRECTIONAL );
	else if( Ogre::String(XMLLight->Attribute("type")) == "spot")
		l->setType( Light::LT_SPOTLIGHT );
	else if( Ogre::String(XMLLight->Attribute("type")) == "radPoint")
		l->setType( Light::LT_POINT );

	XMLDiffuse = XMLLight->FirstChildElement("colourDiffuse");
	if( XMLDiffuse ){
		ColourValue Diffuse;
		Diffuse.r = Ogre::StringConverter::parseReal( XMLDiffuse->Attribute("r") );
		Diffuse.g = Ogre::StringConverter::parseReal( XMLDiffuse->Attribute("g") );
		Diffuse.b = Ogre::StringConverter::parseReal( XMLDiffuse->Attribute("b") );
		Diffuse.a = 1;
		l->setDiffuseColour(Diffuse);
	}
	XMLSpecular = XMLLight->FirstChildElement("colourSpecular");
	if( XMLSpecular ){
		ColourValue Specular;
		Specular.r = Ogre::StringConverter::parseReal( XMLSpecular->Attribute("r") );
		Specular.g = Ogre::StringConverter::parseReal( XMLSpecular->Attribute("g") );
		Specular.b = Ogre::StringConverter::parseReal( XMLSpecular->Attribute("b") );
		Specular.a = 1;
		l->setSpecularColour(Specular);
	}

	XMLAttentuation = XMLLight->FirstChildElement("lightAttenuation");
	if( XMLAttentuation )
	{
		//get defaults incase not all values specified
		Real range, constant, linear, quadratic;
		range = l->getAttenuationRange();
		constant = l->getAttenuationConstant();
		linear = l->getAttenuationLinear();
		quadratic = l->getAttenuationQuadric();

		if( XMLAttentuation->Attribute("range") )
			range = StringConverter::parseReal( XMLAttentuation->Attribute("range") );
		if( XMLAttentuation->Attribute("constant") )
			constant = StringConverter::parseReal( XMLAttentuation->Attribute("constant") );
		if( XMLAttentuation->Attribute("linear") )
			linear = StringConverter::parseReal( XMLAttentuation->Attribute("linear") );
		if( XMLAttentuation->Attribute("quadratic") )
			quadratic = StringConverter::parseReal( XMLAttentuation->Attribute("quadratic") );

		l->setAttenuation( range, constant, linear, quadratic );
	}

	XMLPosition = XMLLight->FirstChildElement("position");
	if( XMLPosition ) {
		Ogre::Vector3 p = Ogre::Vector3(0,0,0);
		if( XMLPosition->Attribute("x") )
			p.x = StringConverter::parseReal( XMLPosition->Attribute("x") );
		if( XMLPosition->Attribute("y") )
			p.y = StringConverter::parseReal( XMLPosition->Attribute("y") );
		if( XMLPosition->Attribute("z") )
			p.z = StringConverter::parseReal( XMLPosition->Attribute("z") );

		l->setPosition( p );
	}

	//castShadows      (true | false) "true"
	l->setCastShadows( true );
	if( XMLLight->Attribute("castShadows") )
		if( Ogre::String(XMLLight->Attribute("castShadows")) == "false" )
			l->setCastShadows( false );

	//visible         (true | false) "true"
	l->setVisible( true );
	if( XMLLight->Attribute("visible") )
		if( Ogre::String(XMLLight->Attribute("visible")) == "false" )
			l->setVisible( false );

	return l;
}

bool CDotScene::parseDotScene( std::vector<Ogre::SceneNode*>* pList, const Ogre::String &SceneName,
		const Ogre::String& groupName, SceneManager *yourSceneMgr, SceneNode *pAttachNode, Ogre::String sPrependNode )
{
	// set up shared object values
	m_sPrependNode = sPrependNode;
	mSceneMgr = yourSceneMgr;

	TiXmlDocument   *XMLDoc;
	TiXmlElement   *XMLRoot, *XMLNodes;

	try
	{
		DataStreamPtr pStream = ResourceGroupManager::getSingleton().
			openResource( SceneName, groupName );

		Ogre::String data = pStream->getAsString();
		// Open the .scene File
		XMLDoc = new TiXmlDocument();
		XMLDoc->Parse( data.c_str() );
		pStream->close();
		pStream.setNull();

		if( XMLDoc->Error() )
		{
			//We'll just log, and continue on gracefully
			LogManager::getSingleton().logMessage("[dotSceneLoader] The TiXmlDocument reported an error");
			delete XMLDoc;
			return false;
		}
	}
	catch(...)
	{
		//We'll just log, and continue on gracefully
		LogManager::getSingleton().logMessage("[dotSceneLoader] Error creating TiXmlDocument");
		delete XMLDoc;
		return false;
	}

	// Validate the File
	XMLRoot = XMLDoc->RootElement();
	if( Ogre::String( XMLRoot->Value()) != "scene"  ) {
		LogManager::getSingleton().logMessage( "[dotSceneLoader]Error: Invalid .scene File. Missing <scene>" );
		delete XMLDoc;
		return false;
	}

	// figure out where to attach any nodes we create
	if(pAttachNode == NULL)
	{
		pAttachNode = mSceneMgr->getRootSceneNode();
	}

	/*** Handle the nodes data ***/
	XMLNodes = XMLRoot->FirstChildElement( "nodes" );

	// Read in the scene nodes
	if( XMLNodes )
	{
		processNode(pList, XMLNodes->FirstChildElement( "node" ), pAttachNode);
	}

	// Close the XML File
	delete XMLDoc;


	return true;
}

bool CDotScene::parseMapDotScene( std::string &mapName, Ogre::Real &mapWidth, Ogre::Real &mapHeight,
		std::string &mapHeightData, unsigned long long &mapHeightDataSize, std::vector<Ogre::SceneNode*>* pList,
		const Ogre::String &SceneName, const Ogre::String& groupName, SceneManager *yourSceneMgr,
		SceneNode *pAttachNode, Ogre::String sPrependNode )
{
	// set up shared object values
	m_sPrependNode = sPrependNode;
	mSceneMgr = yourSceneMgr;

	TiXmlDocument   *XMLDoc;
	TiXmlElement   *XMLRoot, *XMLMap, *XMLNodes;

	try
	{
		DataStreamPtr pStream = ResourceGroupManager::getSingleton().
			openResource( SceneName, groupName );

		Ogre::String data = pStream->getAsString();
		// Open the .scene File
		XMLDoc = new TiXmlDocument();
		XMLDoc->Parse( data.c_str() );
		pStream->close();
		pStream.setNull();

		if( XMLDoc->Error() )
		{
			//We'll just log, and continue on gracefully
			LogManager::getSingleton().logMessage("[dotSceneLoader] The TiXmlDocument reported an error");
			delete XMLDoc;
			return false;
		}
	}
	catch(...)
	{
		//We'll just log, and continue on gracefully
		LogManager::getSingleton().logMessage("[dotSceneLoader] Error creating TiXmlDocument");
		delete XMLDoc;
		return false;
	}

	// Validate the File
	XMLRoot = XMLDoc->RootElement();
	if( Ogre::String( XMLRoot->Value()) != "scene"  ) {
		LogManager::getSingleton().logMessage( "[dotSceneLoader]Error: Invalid .scene File. Missing <scene>" );
		delete XMLDoc;
		return false;
	}

	// figure out where to attach any nodes we create
	if(pAttachNode == NULL)
	{
		pAttachNode = mSceneMgr->getRootSceneNode();
	}


	/*** Handle the nodes data ***/
	XMLNodes = XMLRoot->FirstChildElement( "nodes" );

	// Read in the scene nodes
	if( XMLNodes )
	{
		processNode(pList, XMLNodes->FirstChildElement( "node" ), pAttachNode);
	}


	/*** Handle the Terrain data ***/
	XMLMap = XMLRoot->FirstChildElement( "terrains" );

	// Read in the scene nodes
	if( XMLMap )
	{
		// return false if there was an error reading the Terrain data;
		if ( processTerrain(XMLMap->FirstChildElement( "terrain" ), mapName, mapWidth, mapHeight,
			mapHeightData, mapHeightDataSize) == false )
		{
			return false;
		}
	}




	// Close the XML File
	delete XMLDoc;


	return true;
}

bool CDotScene::processTerrain(TiXmlElement *XMLNode, std::string &mapName, Ogre::Real &mapWidth,
		Ogre::Real &mapHeight, std::string &mapHeightData, unsigned long long &mapHeightDataSize)
{
	EditManager* const editManager = EditManager::getSingletonPtr();
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();

	TerrainManager* const terrainManager = editManager->getTerrainManager();
	SplatManager* const splatManager = terrainManager->getSplatManager();

	TerrainMenuSplatTab* const terrrainMenuSplatTab = gui->getTerrainMenu()->getTerrainMenuSplatTab();

	StringConverter sc;
	Ogre::String str;

	TiXmlElement* XMLwidth;
	TiXmlElement* XMLheight;
	TiXmlElement* XMLheightMapData;
	TiXmlElement* XMLheightMapDataSize;
	TiXmlElement* XMLSplatMapFileName;
	TiXmlElement* XMLSplatMapTexture;
	TiXmlElement* XMLSplatMapRegularScales;
	TiXmlElement* XMLSplatMapDetailScales;

	vector<string> vSplatMapTextures;
	Ogre::Vector4 vSplatRegularScales;
	Ogre::Vector4 vSplatDetailScales;




	if( XMLNode )
	{
		/****************/
		/*** Map name ***/
		/****************/

		mapName = XMLNode->Attribute("name");

		if (mapName == "")
		{
			cout << "ERROR void CDotScene::processTerrain(...): No map name." << endl;
			return false;
		}


		/*****************/
		/*** Map width ***/
		/*****************/

		XMLwidth = XMLNode->FirstChildElement("width");

		if (XMLwidth)
		{
			str = XMLwidth->Attribute("w");
			mapWidth = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No Terrain width." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): Terrain width is not a valid number." << endl;
				return false;
			}


			// If the number isn't bigger than zero than we have a problem;
			if ( mapWidth <= 0 )
			{
				cout << "ERROR void CDotScene::processTerrain(...): Terrain width is zero or less." << endl;
				return false;
			}
		}
		else
		{
				cout << "ERROR void CDotScene::processTerrain(...): No Terrain width." << endl;
				return false;
		}


		/**********************/
		/*** Terrain height ***/
		/**********************/

		XMLheight = XMLNode->FirstChildElement("maxheight");

		if (XMLheight)
		{
			str = XMLheight->Attribute("mh");
			mapHeight = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No Terrain height." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): Terrain height is not a valid number." << endl;
				return false;
			}


			// If the number isn't bigger than zero than we have a problem;
			if ( mapHeight <= 0 )
			{
				cout << "ERROR void CDotScene::processTerrain(...): Terrain height is zero or less." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No Terrain height." << endl;
			return false;
		}


		/**********************/
		/*** Heightmap Data ***/
		/**********************/

		XMLheightMapData = XMLNode->FirstChildElement("heightMapData");

		if (XMLheightMapData)
		{
			mapHeightData = XMLheightMapData->Attribute("data");

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No Terrain height data." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No Terrain height data.." << endl;
			return false;
		}


		/***************************/
		/*** Heightmap Data Size ***/
		/***************************/

		XMLheightMapDataSize = XMLNode->FirstChildElement("heightMapDataSize");

		if (XMLheightMapDataSize)
		{
			str = XMLheightMapDataSize->Attribute("size");


			// Convert the string to a long long;
			mapHeightDataSize = boost::lexical_cast<unsigned long long>(str);


			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No Terrain mapHeightDataSize ." << endl;
				return false;
			}

			// If the number isn't bigger than zero than we have a problem;
			if ( mapHeightDataSize <= 0 )
			{
				cout << "ERROR void CDotScene::processTerrain(...): Terrain mapHeightDataSize is zero or less." << endl;
				return false;
			}
		}
		else
		{
				cout << "ERROR void CDotScene::processTerrain(...): No Terrain mapHeightDataSize." << endl;
				return false;
		}


		/***************************/
		/*** Splat map file name ***/
		/***************************/

		XMLSplatMapFileName = XMLNode->FirstChildElement("splatMapFileName");

		if (XMLSplatMapFileName)
		{
			str = XMLSplatMapFileName->Attribute("fileName");

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No SplatMap file name." << endl;
				return false;
			}
			else
			{
				splatManager->setSplatMapFileName(str);
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No Terrain height." << endl;
			return false;
		}


		/**********************************/
		/*** Set the Splat map Textures ***/
		/**********************************/

		/* texture1 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture1");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No splatTexture1 file name." << endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}

		/* texture2 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture2");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No splatTexture2 file name." << endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}

		/* texture3 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture3");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No splatTexture3 file name." << endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}

		/* texture4 */
		XMLSplatMapTexture = XMLNode->FirstChildElement("splatTexture4");

		if (XMLSplatMapTexture)
		{
			str = XMLSplatMapTexture->Attribute("fileName");

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): No splatTexture4 file name." << endl;
				return false;
			}
			else
			{
				vSplatMapTextures.push_back(str);
			}
		}


		/*********************************/
		/*** Set All 4  Splat textures ***/
		/*********************************/

		// If we have all four textures than set the SplatManager textures;
		splatManager->setTextures( vSplatMapTextures.at(0), vSplatMapTextures.at(1),
			vSplatMapTextures.at(2), vSplatMapTextures.at(3) );


		/*********************************************************************/
		/*** Reset the scrollpane sample texture images to the ones loaded ***/
		/*********************************************************************/
		terrrainMenuSplatTab->setSampleTexture(0, string(SPLATMAP_SAMPLETEXTURE_FILENAME_STARTPART + vSplatMapTextures.at(0)) );
		terrrainMenuSplatTab->setSampleTexture(1, string(SPLATMAP_SAMPLETEXTURE_FILENAME_STARTPART + vSplatMapTextures.at(1)) );
		terrrainMenuSplatTab->setSampleTexture(2, string(SPLATMAP_SAMPLETEXTURE_FILENAME_STARTPART + vSplatMapTextures.at(2)) );
		terrrainMenuSplatTab->setSampleTexture(3, string(SPLATMAP_SAMPLETEXTURE_FILENAME_STARTPART + vSplatMapTextures.at(3)) );


		/************************************/
		/*** Set the Splat Regular scales ***/
		/************************************/

		// splatRegularScales1;
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales1");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.x = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales1." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales1 is not a valid number." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales1." << endl;
			return false;
		}


		// splatRegularScales2
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales2");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.y = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales2." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales2 is not a valid number." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales2." << endl;
			return false;
		}


		// splatRegularScales3
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales3");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.z = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales3." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales3 is not a valid number." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales3." << endl;
			return false;
		}


		// splatRegularScales4
		XMLSplatMapRegularScales = XMLNode->FirstChildElement("splatRegularScales4");
		if (XMLSplatMapRegularScales)
		{
			str = XMLSplatMapRegularScales->Attribute("RealVal");
			vSplatRegularScales.w = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales4." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatRegularScales4 is not a valid number." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No splatRegularScales4." << endl;
			return false;
		}


		// Now Set the Regular splat scales;
		splatManager->setSplatScales(vSplatRegularScales);



		/***********************************/
		/*** Set the Splat detail scales ***/
		/***********************************/

		// splatDetailScales1;
		XMLSplatMapDetailScales = XMLNode->FirstChildElement("splatDetailScales1");
		if (XMLSplatMapDetailScales)
		{
			str = XMLSplatMapDetailScales->Attribute("RealVal");
			vSplatDetailScales.x = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales1." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales1 is not a valid number." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No splatDetailScales1." << endl;
			return false;
		}


		// splatDetailScales2
		XMLSplatMapDetailScales = XMLNode->FirstChildElement("splatDetailScales2");
		if (XMLSplatMapDetailScales)
		{
			str = XMLSplatMapDetailScales->Attribute("RealVal");
			vSplatDetailScales.y = sc.parseReal(str);

			if (str == "")
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales2." << endl;
				return false;
			}
			else if ( !sc.isNumber(str) )
			{
				cout << "ERROR void CDotScene::processTerrain(...): splatDetailScales2 is not a valid number." << endl;
				return false;
			}
		}
		else
		{
			cout << "ERROR void CDotScene::processTerrain(...): No splatDetailScales2." << endl;
			return false;
		}


		// Now Set the Detail splat scales;
		splatManager->setDetailScales(vSplatDetailScales);
	}
	else
	{
		cout << "ERROR void CDotScene::processTerrain(...): No Terrain tag." << endl;
		return false;
	}


	return true;
}

void CDotScene::processNode(std::vector<Ogre::SceneNode*>* pList, TiXmlElement *XMLNode, SceneNode *pAttach)
{
	TiXmlElement *XMLPosition, *XMLRotation, *XMLRoll, *XMLPitch, *XMLScale,
			*XMLEntity, *XMLEntityLOD,
			*XMLBillboardSet,  *XMLLight,
			*XMLUserData;

	while( XMLNode )
	{
		// Process the current node
		// Grab the name of the node
		Ogre::String NodeName = XMLNode->Attribute("name");
		// First create the new scene node
		//SceneNode* NewNode = pAttach->createChildSceneNode( /*m_sPrependNode + NodeName*/ );
		SceneNode* NewNode = pAttach->createChildSceneNode( NodeName );
		Ogre::Vector3 TempVec;
		Ogre::String TempValue;



		/*
		***	NOTE: The order of the scaling, pitching and ect is important.
		***	Here the order should be: Scale, position, roll, pitch, rotation;
		 */

		// Scale it.
		XMLScale = XMLNode->FirstChildElement("scale");
		if( XMLScale ){
			TempValue = XMLScale->Attribute("x");
			TempVec.x = StringConverter::parseReal(TempValue);
			TempValue = XMLScale->Attribute("y");
			TempVec.y = StringConverter::parseReal(TempValue);
			TempValue = XMLScale->Attribute("z");
			TempVec.z = StringConverter::parseReal(TempValue);

			try
			{
				NewNode->scale( TempVec );
			}
			catch(...)
			{
				cout << "void CDotScene::processNode(...): ERROR: An error occurred while scaling." << endl;
			}
		}

		// Now position it...
		XMLPosition = XMLNode->FirstChildElement("position");
		if( XMLPosition ){
			TempValue = XMLPosition->Attribute("x");
			TempVec.x = StringConverter::parseReal(TempValue);
			TempValue = XMLPosition->Attribute("y");
			TempVec.y = StringConverter::parseReal(TempValue);
			TempValue = XMLPosition->Attribute("z");
			TempVec.z = StringConverter::parseReal(TempValue);
			NewNode->setPosition( TempVec );
		}

		// Roll;
		XMLRoll = XMLNode->FirstChildElement("roll");
		if( XMLRoll ){
			Ogre::Degree tempDegree;
			TempValue = XMLRoll->Attribute("degree");
			tempDegree = StringConverter::parseReal(TempValue);
			NewNode->roll(Ogre::Radian(Ogre::Degree(tempDegree)), Ogre::Node::TS_WORLD);
		}

		// Pitch;
		XMLPitch = XMLNode->FirstChildElement("pitch");
		if( XMLPitch ){
			Ogre::Degree tempDegree;
			TempValue = XMLPitch->Attribute("degree");
			tempDegree = StringConverter::parseReal(TempValue);
			NewNode->pitch(Ogre::Radian(Ogre::Degree(tempDegree)), Ogre::Node::TS_WORLD);
		}

		// Rotate it...
		XMLRotation = XMLNode->FirstChildElement("rotation");
		if( XMLRotation ){
			Quaternion TempQuat;
			TempValue = XMLRotation->Attribute("qx");
			TempQuat.x = StringConverter::parseReal(TempValue);
			TempValue = XMLRotation->Attribute("qy");
			TempQuat.y = StringConverter::parseReal(TempValue);
			TempValue = XMLRotation->Attribute("qz");
			TempQuat.z = StringConverter::parseReal(TempValue);
			TempValue = XMLRotation->Attribute("qw");
			TempQuat.w = StringConverter::parseReal(TempValue);
			NewNode->setOrientation( TempQuat );
		}


		XMLLight = XMLNode->FirstChildElement( "light" );
		if( XMLLight ) {
		  std::string lightName;
		  NewNode->attachObject( LoadLight( XMLLight, mSceneMgr, lightName ) );
		  //attaches the terrain specific mesh to the node.
		  NewNode->attachObject( mSceneMgr->createEntity(DEF_PARSE_LIGHT_MESH_NAME_START_STRING + 
								 lightName, LIGHTING_MESH_NAME) );
		}

		// Check for an Entity
		XMLEntity = XMLNode->FirstChildElement("entity");
		if( XMLEntity )
		{
			Ogre::String EntityName, EntityMeshFilename;
			EntityName = XMLEntity->Attribute( "name" );
			EntityMeshFilename = XMLEntity->Attribute( "meshFile" );

			// Create entity
			Entity* NewEntity = mSceneMgr->createEntity(EntityName, EntityMeshFilename);

                        //castShadows      (true | false) "true"
                        NewEntity->setCastShadows( true );
                        if( XMLEntity->Attribute("castShadows") )
                           if( Ogre::String(XMLEntity->Attribute("castShadows")) == "false" )
                              NewEntity->setCastShadows( false );

                        NewNode->attachObject( NewEntity );

			//GameObject *game_object = mEditMgr->addGameObject();
			/*game_object->setAttribute( "NodeName", NewNode->getName() );
			game_object->setAttribute( "Position_X", Ogre::StringConverter::toString(NewNode->getPosition().x) );
			game_object->setAttribute( "Position_Y", Ogre::StringConverter::toString(NewNode->getPosition().y) );
			game_object->setAttribute( "Position_Z", Ogre::StringConverter::toString(NewNode->getPosition().z) );
			game_object->setAttribute( "Orientation_W", Ogre::StringConverter::toString(NewNode->getOrientation().w) );
			game_object->setAttribute( "Orientation_X", Ogre::StringConverter::toString(NewNode->getOrientation().x) );
			game_object->setAttribute( "Orientation_Y", Ogre::StringConverter::toString(NewNode->getOrientation().y) );
			game_object->setAttribute( "Orientation_Z", Ogre::StringConverter::toString(NewNode->getOrientation().z) );
			game_object->setAttribute( "*/
			//game_object->setSceneNode( NewNode );
			//mEditMgr->addObject( NewNode );

			/*****************************************/
			/*** Store the Scene Nodes in a vector ***/
			/*****************************************/
			pList->push_back(NewNode);

		}

		XMLBillboardSet = XMLNode->FirstChildElement( "billboardSet" );
		if( XMLBillboardSet )
		{
			Ogre::String TempValue;

			BillboardSet* bSet = mSceneMgr->createBillboardSet( NewNode->getName() );

			BillboardType Type;
			TempValue = XMLBillboardSet->Attribute( "type" );
			if( TempValue == "orientedCommon" )
				Type = BBT_ORIENTED_COMMON;
			else if( TempValue == "orientedSelf" )
				Type = BBT_ORIENTED_SELF;
			else Type = BBT_POINT;

			BillboardOrigin Origin;
			TempValue = XMLBillboardSet->Attribute( "type" );
			if( TempValue == "bottom_left" )
				Origin = BBO_BOTTOM_LEFT;
			else if( TempValue == "bottom_center" )
				Origin = BBO_BOTTOM_CENTER;
			else if( TempValue == "bottomRight"  )
				Origin = BBO_BOTTOM_RIGHT;
			else if( TempValue == "left" )
				Origin = BBO_CENTER_LEFT;
			else if( TempValue == "right" )
				Origin = BBO_CENTER_RIGHT;
			else if( TempValue == "topLeft" )
				Origin = BBO_TOP_LEFT;
			else if( TempValue == "topCenter" )
				Origin = BBO_TOP_CENTER;
			else if( TempValue == "topRight" )
				Origin = BBO_TOP_RIGHT;
			else
				Origin = BBO_CENTER;

			bSet->setBillboardType( Type );
			bSet->setBillboardOrigin( Origin );


			TempValue = XMLBillboardSet->Attribute( "name" );
			bSet->setMaterialName( TempValue );

			int width, height;
			width = (int) StringConverter::parseReal( XMLBillboardSet->Attribute( "width" ) );
			height = (int) StringConverter::parseReal( XMLBillboardSet->Attribute( "height" ) );
			bSet->setDefaultDimensions( width, height );
			bSet->setVisible( true );
			NewNode->attachObject( bSet );

			TiXmlElement *XMLBillboard;

			XMLBillboard = XMLBillboardSet->FirstChildElement( "billboard" );

			while( XMLBillboard )
			{
				Billboard *b;
				// TempValue;
				TempVec = Ogre::Vector3( 0, 0, 0 );
				ColourValue TempColour(1,1,1,1);

				XMLPosition = XMLBillboard->FirstChildElement( "position" );
				if( XMLPosition ){
					TempValue = XMLPosition->Attribute("x");
					TempVec.x = StringConverter::parseReal(TempValue);
					TempValue = XMLPosition->Attribute("y");
					TempVec.y = StringConverter::parseReal(TempValue);
					TempValue = XMLPosition->Attribute("z");
					TempVec.z = StringConverter::parseReal(TempValue);
				}

				TiXmlElement* XMLColour = XMLBillboard->FirstChildElement( "colourDiffuse" );
				if( XMLColour ){
					TempValue = XMLColour->Attribute("r");
					TempColour.r = StringConverter::parseReal(TempValue);
					TempValue = XMLColour->Attribute("g");
					TempColour.g = StringConverter::parseReal(TempValue);
					TempValue = XMLColour->Attribute("b");
					TempColour.b = StringConverter::parseReal(TempValue);
				}

				b = bSet->createBillboard( TempVec, TempColour);

				XMLBillboard = XMLBillboard->NextSiblingElement( "billboard" );
			}
		}

		XMLUserData = XMLNode->FirstChildElement( "userData" );
		if ( XMLUserData )
		{
			TiXmlElement *XMLProperty;
			XMLProperty = XMLUserData->FirstChildElement("property");
			while ( XMLProperty )
			{
				Ogre::String first = NewNode->getName();
				Ogre::String second = XMLProperty->Attribute("name");
				Ogre::String third = XMLProperty->Attribute("data");
				Ogre::String type = XMLProperty->Attribute("type");
				nodeProperty newProp(first,second,third,type);
				nodeProperties.push_back(newProp);
				XMLProperty = XMLProperty->NextSiblingElement("property");
			}
		}
//-->		/*
		/*		Added by @lpha_Max ! :-)
		*		"Global" vars :
		*			NewNode --> Node where attach the object...
		*			mSceneMgr --> SceneManager
		*/
		TiXmlElement* XMLCamera = XMLNode->FirstChildElement( "camera" );
		if ( XMLCamera )
		{
			Ogre::String CameraName = XMLCamera->Attribute( "name" );
			Camera *cam = mSceneMgr->createCamera(CameraName);
			NewNode->attachObject(cam);
		}

		TiXmlElement* XMLParticle = XMLNode->FirstChildElement( "particleSystem" );
		if ( XMLParticle )
		{
			Ogre::String ParticleName = XMLParticle->Attribute( "name" );
			Ogre::String ParticleFile = XMLParticle->Attribute( "file" );
			ParticleSystem* ParticleS = mSceneMgr->createParticleSystem(ParticleName, ParticleFile);
			NewNode->attachObject(ParticleS);
                }
//<--
		TiXmlElement * ChildXMLNode;
		ChildXMLNode = XMLNode->FirstChildElement( "node" );
		if(ChildXMLNode)
			processNode(pList, ChildXMLNode, NewNode);	// recurse to do all my children

		XMLNode = XMLNode->NextSiblingElement( "node" ); // process my next sibling
	}
}

Ogre::String CDotScene::getProperty(Ogre::String ndNm, Ogre::String prop)
{
	for ( unsigned int i = 0 ; i < nodeProperties.size(); i++ )
	{
		if ( nodeProperties[i].nodeName == ndNm && nodeProperties[i].propertyNm == prop )
		{
			return nodeProperties[i].valueName;
		}
	}
	return " ";
}



