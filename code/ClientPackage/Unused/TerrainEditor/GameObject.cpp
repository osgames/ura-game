/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GameObject.h"

GameObject::GameObject( std::string name )
{
	sName = name;
	mSceneNode = 0;
}

GameObject::~GameObject()
{
}

void GameObject::setName( std::string name )
{
	sName = name;
}

std::string GameObject::getName()
{
	return sName;
}

void GameObject::setAttribute( std::string name, std::string value )
{
	for( unsigned int indx = 0; indx < vAttributeName.size(); indx++ )
	{
		if( vAttributeName[indx] == name )
		{
			vAttributeValue[indx] = value;
		}
	}

	vAttributeName.push_back( name );
	vAttributeValue.push_back( value );
}

std::string GameObject::getAttribute( std::string name )
{
	for( unsigned int indx = 0; indx < vAttributeName.size(); indx++ )
	{
		if( vAttributeName[indx] == name )
		{
			return vAttributeValue[indx];
		}
	}

	return ""; // It should never get to this point.
}

void GameObject::setSceneNode( Ogre::SceneNode *sn )
{
	mSceneNode = sn;
}

Ogre::SceneNode *GameObject::getSceneNode()
{
	return mSceneNode;
}

