/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CAMERA_MANAGER_H
#define CAMERA_MANAGER_H

#include <vector>
#include <string>
#include <Ogre.h>



class CameraManager
{
protected:
	Ogre::Camera* mCamera;
	Ogre::SceneNode* mCameraSceneNode;
	Ogre::Viewport* mViewport;

	Ogre::Real mCameraSceneNodeMoveDegree;
	Ogre::Real mCameraMoveDegree;
	Ogre::Vector3 mTmpVector3Position;

	Ogre::Degree mCameraSceneNodeYaw;
	Ogre::Degree mCameraSceneNodePitch;
	Ogre::Degree mCameraSceneNodeRoll;
	Ogre::Vector3 mCameraSceneNodeTranslateVector;

	Ogre::Degree mCameraYaw;
	Ogre::Degree mCameraPitch;
	Ogre::Degree mCameraRoll;
	Ogre::Vector3 mCameraTranslateVector;
public:
	CameraManager();
	~CameraManager();

	Ogre::Camera* const getCamera();
	Ogre::SceneNode* const getCameraSceneNode();

	void resetDefaultPosition();
	void setCameraMoveDegree(Ogre::Real val);
	Ogre::Real getCameraMoveDegree() const;
	void setCameraSceneNodeMoveDegree(Ogre::Real val);
	Ogre::Real getCameraSceneNodeMoveDegree() const;
	

	/*************************/
	/*** Update the Camera ***/
	/*************************/
	void update(unsigned long* const timeSinceLastFrame);


	/******************/
	/*** The camera ***/
	/******************/
	void cameraYaw(Ogre::Real val);
	void cameraPitch(Ogre::Real val);

	void cameraMoveForward();
	void cameraMoveBackward();
	void cameraMoveLeft();
	void cameraMoveRight();
	void cameraYawLeft();
	void cameraYawRight();
	void cameraPitchForward();
	void cameraPitchBackward();
	void cameraRollLeft();
	void cameraRollRight();
	void cameraRaise();
	void cameraLower();


	/****************************/
	/*** The camera SceneNode ***/
	/****************************/
	void cameraSceneNodeYaw(Ogre::Real val);
	void cameraSceneNodePitch(Ogre::Real val);

	void cameraSceneNodeMoveForward();
	void cameraSceneNodeMoveBackward();
	void cameraSceneNodeMoveLeft();
	void cameraSceneNodeMoveRight();
	void cameraSceneNodeYawLeft();
	void cameraSceneNodeYawRight();
	void cameraSceneNodePitchForward();
	void cameraSceneNodePitchBackward();
	void cameraSceneNodeRollLeft();
	void cameraSceneNodeRollRight();
	void cameraSceneNodeRaise();
	void cameraSceneNodeLower();
};


#endif





