/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CameraManager.h"
#include "Ogre3dManager.h"


using namespace std;



CameraManager::CameraManager() :
	mCameraSceneNodeMoveDegree(10), mCameraMoveDegree(10),
	mCameraSceneNodeYaw(0), mCameraSceneNodePitch(0), mCameraSceneNodeRoll(0),
	mCameraSceneNodeTranslateVector(Ogre::Vector3::ZERO),
	mCameraYaw(0), mCameraPitch(0), mCameraRoll(0), mCameraTranslateVector(Ogre::Vector3::ZERO)
{

	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::RenderWindow* renderWindow = ogre3dManager->getRenderWindow();
	Ogre::SceneManager* sceneManager = ogre3dManager->getSceneManager();


	/*** Create the sceneNode for the camera to get attached to and set its position and orientation ***/
	mCameraSceneNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mCameraSceneNode->setOrientation(Ogre::Quaternion(0,0,0,1));	// Were looking down the positive z Axis;
	mCameraSceneNode->setPosition(0, 0, 0);


	/*** Create the main camera, set its nearclipDistance and Attach it to the sceneNode ***/
	mCamera = sceneManager->createCamera("camera");
	mCamera->setNearClipDistance(5.0f);
	mCameraSceneNode->attachObject(mCamera);


	/*** Make the camera face the same direction as the cam SceneNode is, which is the positive z Axis ***/
	mCamera->lookAt( Ogre::Vector3(0.0f, 0.0f, 1.0f) );


	/*** Add the viewport and set the background color ***/
	mViewport = renderWindow->addViewport( mCamera );
	mViewport->setBackgroundColour( Ogre::ColourValue( 0.0f,0.0f,0.0f ) );


	/*** Set the camera default aspect ratio ***/
	mCamera->setAspectRatio( Ogre::Real( mViewport->getActualWidth() ) 
		/ Ogre::Real( mViewport->getActualHeight() ) );


	/*** Set the default Ambient light ***/
	//	sceneManager->setAmbientLight( Ogre::ColourValue( 1.0f, 1.0f, 1.0f, 1.0f ) );
	sceneManager->setAmbientLight( Ogre::ColourValue( 0.1f, 0.1f, 0.1f) );
} 

CameraManager::~CameraManager()
{

} 




Ogre::Camera* const CameraManager::getCamera()
{
	return mCamera;
}

Ogre::SceneNode* const CameraManager::getCameraSceneNode()
{
	return mCameraSceneNode;
}

void CameraManager::resetDefaultPosition()
{
	// We reset the Scenenode and camera position to default, which is looking down the positive z Axis;
	mCameraSceneNode->setOrientation(Ogre::Quaternion(0,0,0,1));	// Were looking down the positive z Axis;
	mCameraSceneNode->setPosition(0, 0, 0);

	mCamera->lookAt( Ogre::Vector3(0.0f, 0.0f, 1.0f) );
}

void CameraManager::setCameraMoveDegree(Ogre::Real val)
{
	mCameraMoveDegree = val;
}

Ogre::Real CameraManager::getCameraMoveDegree() const
{
	return mCameraMoveDegree;
}

void CameraManager::setCameraSceneNodeMoveDegree(Ogre::Real val)
{
	mCameraSceneNodeMoveDegree = val;
}

Ogre::Real CameraManager::getCameraSceneNodeMoveDegree() const
{
	return mCameraSceneNodeMoveDegree;
}



void CameraManager::update(unsigned long* const timeSinceLastFrame)
{
	/*** Handle the camera sceneNode ***/
	mCameraSceneNode->yaw( mCameraSceneNodeYaw * (*timeSinceLastFrame) );
	mCameraSceneNode->pitch( mCameraSceneNodePitch * (*timeSinceLastFrame) );
	mCameraSceneNode->roll( mCameraSceneNodeRoll * (*timeSinceLastFrame) );
	mCameraSceneNode->translate( mCameraSceneNodeTranslateVector * (*timeSinceLastFrame) );

	mCameraSceneNodeYaw = static_cast<Ogre::Real>(0);
	mCameraSceneNodePitch = static_cast<Ogre::Real>(0);
	mCameraSceneNodeRoll = static_cast<Ogre::Real>(0);
	mCameraSceneNodeTranslateVector = Ogre::Vector3::ZERO;


	/*** Handle the camera ***/
	mCamera->yaw( mCameraYaw * (*timeSinceLastFrame) );
	mCamera->pitch( mCameraPitch * (*timeSinceLastFrame) );
	mCamera->roll( mCameraRoll * (*timeSinceLastFrame) );
	mCamera->moveRelative( mCameraTranslateVector * (*timeSinceLastFrame) );

	mCameraYaw = static_cast<Ogre::Real>(0);
	mCameraPitch = static_cast<Ogre::Real>(0);
	mCameraRoll = static_cast<Ogre::Real>(0);
	mCameraTranslateVector = Ogre::Vector3::ZERO;
}


/******************/
/*** The camera ***/
/******************/

void CameraManager::cameraYaw(Ogre::Real val)
{
	mCameraYaw += Ogre::Degree(val);
}

void CameraManager::cameraPitch(Ogre::Real val)
{
	mCameraPitch += Ogre::Degree(val);
}


void CameraManager::cameraMoveForward()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = mCameraMoveDegree;

	mCameraTranslateVector += (mCamera->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraMoveBackward()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = -mCameraMoveDegree;

	mCameraTranslateVector += (mCamera->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraMoveLeft()
{
	mTmpVector3Position.x = -mCameraMoveDegree;
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraTranslateVector += (mCamera->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraMoveRight()
{
	mTmpVector3Position.x = mCameraMoveDegree;
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraTranslateVector += (mCamera->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraRaise()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = -mCameraMoveDegree;
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraTranslateVector += (mCamera->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraLower()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = mCameraMoveDegree;
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraTranslateVector += (mCamera->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraYawLeft()
{
	mCameraYaw += Ogre::Degree(-mCameraMoveDegree);
}

void CameraManager::cameraYawRight()
{
	mCameraYaw += Ogre::Degree(mCameraMoveDegree);
}

void CameraManager::cameraPitchForward()
{
	mCameraPitch += Ogre::Degree(mCameraMoveDegree);
}

void CameraManager::cameraPitchBackward()
{
	mCameraPitch += Ogre::Degree(-mCameraMoveDegree);
}

void CameraManager::cameraRollLeft()
{
	mCameraRoll += Ogre::Degree(-mCameraMoveDegree);
}

void CameraManager::cameraRollRight()
{
	mCameraRoll += Ogre::Degree(mCameraMoveDegree);
}




/****************************/
/*** The camera SceneNode ***/
/****************************/

void CameraManager::cameraSceneNodeYaw(Ogre::Real val)
{
	mCameraSceneNodeYaw += Ogre::Degree(val);
}

void CameraManager::cameraSceneNodePitch(Ogre::Real val)
{
	mCameraSceneNodePitch += Ogre::Degree(val);
}



void CameraManager::cameraSceneNodeMoveForward()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = mCameraSceneNodeMoveDegree;

	mCameraSceneNodeTranslateVector += (mCameraSceneNode->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraSceneNodeMoveBackward()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = -mCameraSceneNodeMoveDegree;

	mCameraSceneNodeTranslateVector += (mCameraSceneNode->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraSceneNodeMoveLeft()
{
	mTmpVector3Position.x = -mCameraSceneNodeMoveDegree;
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraSceneNodeTranslateVector += (mCameraSceneNode->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraSceneNodeMoveRight()
{
	mTmpVector3Position.x = mCameraSceneNodeMoveDegree;
	mTmpVector3Position.y = static_cast<Ogre::Real>(0);
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraSceneNodeTranslateVector += (mCameraSceneNode->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraSceneNodeRaise()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = -mCameraSceneNodeMoveDegree;
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraSceneNodeTranslateVector += (mCameraSceneNode->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraSceneNodeLower()
{
	mTmpVector3Position.x = static_cast<Ogre::Real>(0);
	mTmpVector3Position.y = mCameraSceneNodeMoveDegree;
	mTmpVector3Position.z = static_cast<Ogre::Real>(0);

	mCameraSceneNodeTranslateVector += (mCameraSceneNode->getOrientation() * mTmpVector3Position);
}

void CameraManager::cameraSceneNodeYawLeft()
{
	mCameraSceneNodeYaw += Ogre::Degree(-mCameraSceneNodeMoveDegree);
}

void CameraManager::cameraSceneNodeYawRight()
{
	mCameraSceneNodeYaw += Ogre::Degree(mCameraSceneNodeMoveDegree);
}

void CameraManager::cameraSceneNodePitchForward()
{
	mCameraSceneNodePitch += Ogre::Degree(mCameraSceneNodeMoveDegree);
}

void CameraManager::cameraSceneNodePitchBackward()
{
	mCameraSceneNodePitch += Ogre::Degree(-mCameraSceneNodeMoveDegree);
}

void CameraManager::cameraSceneNodeRollLeft()
{
	mCameraSceneNodeRoll += Ogre::Degree(-mCameraSceneNodeMoveDegree);
}

void CameraManager::cameraSceneNodeRollRight()
{
	mCameraSceneNodeRoll += Ogre::Degree(mCameraSceneNodeMoveDegree);
}







