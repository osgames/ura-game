/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ModelMenu.h"


using namespace CEGUI;
using namespace std;


ModelMenu::ModelMenu()
{
	/*** Allocate heap memory for these objects ***/
	mModelEditorMainTab = new ModelEditorMainTab;
	mModelMenuLightingTab = new ModelMenuLightingTab;
	//mModelMenuDotScene = new ModelMenuDotScene(mModelEditorMainTab);


	if (!mModelEditorMainTab)
	{
		cout << "ERROR: ModelMenu::ModelMenu(...): mModelEditorMainTab could not allocate heap memory";
	}

	if (!mModelMenuLightingTab)
	{
		cout << "ERROR: ModelMenu::ModelMenu(...): mModelMenuLightingTab could not allocate heap memory";
	}

	/*********
	if (!mModelMenuDotScene)
	{
		cout << "ERROR: ModelMenu::ModelMenu(...): mModelMenuDotScene could not allocate heap memory";
		return false;
	}
	**********/
}

ModelMenu::~ModelMenu()
{
	/*** Delete the heap memory ***/
	if ( mModelEditorMainTab )
	{
		delete mModelEditorMainTab;
		mModelEditorMainTab = NULL;
	}

	/*** Delete the heap memory ***/
	if ( mModelMenuLightingTab )
	{
		delete mModelMenuLightingTab;
		mModelMenuLightingTab = NULL;
	}

	/************
	if ( mModelMenuDotScene )
	{
		delete mModelMenuDotScene;
		mModelMenuDotScene = NULL;
	}
	*************/
}

void ModelMenu::prepareToQuit()
{
	/*** Destroy the cegui window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mObjectMenu);	
}

void ModelMenu::toggle()
{
	if ( mObjectMenu->isVisible() == false )
	{
		/*
		*** When the Main window is reshown we want the scrollbars like "Camera Rate" and "Mouse Rate"
		***  to readjust. Also we want to update anything we call the updateWindowShown() function;
		 */
		mModelEditorMainTab->updateWindowShown();
		mModelMenuLightingTab->updateWindowShown();

		mObjectMenu->show();
		mObjectMenu->activate();	
	}
	else if( mObjectMenu->isVisible() == true )
	{
		mObjectMenu->hide();	
	}
}

bool ModelMenu::isVisible()
{
	return mObjectMenu->isVisible();
}

bool ModelMenu::addChildWindow(bool showWindow = true)
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	float objectMenu_x_size = 0.24F;
	float objectMenu_y_size = 1.0F;
	float ObjectMenu_x_pos = 1.0F - objectMenu_x_size;


	/*** Create the main window ***/
	mObjectMenu = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "ModelMenu_mObjectMenu");
	mObjectMenu->setPosition( CEGUI::UVector2(CEGUI::UDim(ObjectMenu_x_pos, 0.0F), CEGUI::UDim(0.0F, 0.0F) ) );
	mObjectMenu->setSize(CEGUI::UVector2(CEGUI::UDim(objectMenu_x_size, 0.0F), CEGUI::UDim(objectMenu_y_size, 0.0F)));
	mObjectMenu->setText("Model Editor");


	/*** Create the tab control ***/
	mTabControl = static_cast<TabControl*>(windowManager.createWindow("TaharezLook/TabControl", "ModelMenu_mTabControl"));
	mTabControl->setTabHeight(UDim(0.03f, 9.05f));
	mTabControl->setPosition( CEGUI::UVector2(CEGUI::UDim(0.06, 0.0F), CEGUI::UDim(0.04F, 0.0F) ) );
	mTabControl->setSize(CEGUI::UVector2(CEGUI::UDim(0.88F, 0.0F), CEGUI::UDim(0.944F, 0.0F)));
	

	/*************************************************/
	/*** Creat the Main window and sub Tab Control ***/
	/*************************************************/
	CEGUI::Window* MainModelWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage","ModelMenu_MainModelWindow");
	MainModelWindow->setText("Model");
	MainModelWindow->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	MainModelWindow->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 

	// Create the sub tab control;
	mSubTabControl = static_cast<TabControl*>(windowManager.createWindow("TaharezLook/TabControl", "ModelMenu_mSubTabControl"));
	mSubTabControl->setTabHeight(UDim(0.03f, 9.05f));
	mSubTabControl->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f)));
	mSubTabControl->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 



	/****************************************************************/
	/*** Create the child windows and add the windows to the tab ****/
	/****************************************************************/

	// Create the ModelEditorMainTab and attach it to the sheet;
	if ( mModelEditorMainTab )
	{
		mModelEditorMainTab->addChildWindow();
		mSubTabControl->addTab( mModelEditorMainTab->getWindow() );
	}

	// Create the ModelMenuLightingTab and attach it to the sheet;
	if ( mModelMenuLightingTab )
	{
		mModelMenuLightingTab->addChildWindow();
		mSubTabControl->addTab( mModelMenuLightingTab->getWindow() );
	}


	/****************************************
	if ( mModelMenuDotScene )
	{
		mModelMenuDotScene->addChildWindow();
		mSubTabControl->addTab( mModelMenuDotScene->getWindow() );
	}
	*****************************************/


	/************************/
	/*** Set the Settings ***/
	/************************/

	// Determine if we need to hide the window;
	if ( !showWindow )
	{
		mObjectMenu->hide();		
	}

	/**************************/
	/*** Attach the windows ***/
	/**************************/
	MainModelWindow->addChildWindow( mSubTabControl );
	mTabControl->addChildWindow( MainModelWindow );

	// Add the mTabControl to the TerrainMenu;
	mObjectMenu->addChildWindow( mTabControl );


	// Add the mTerrainMenu window to the sheet;
	sheet->addChildWindow( mObjectMenu );


	return true;
} 

ModelEditorMainTab* const ModelMenu::getModelEditorMainTab()
{
	return mModelEditorMainTab;
}

ModelMenuLightingTab* const ModelMenu::getModelMenuLightingTab()
{
	return mModelMenuLightingTab;
}

