/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAP_MENUBAR_H
#define HEAP_MENUBAR_H


/****************/
/*** INCLUDES ***/
/****************/
#include <CEGUI/CEGUI.h>
#include <string>
#include <iostream>
#include <vector>

#include "MenuBarDataType.h"


/***************/
/*** DEFINES ***/
/***************/
#define MENUBAR_DEBUG
#define HEAP_MENUBAR_DEBUG



class MenuBar
{
protected:
	CEGUI::Window* menuBar;
	std::vector<MenuBarDataType>* mVdataTypes;
	std::vector<CEGUI::Window*> mParentMenuItems;
	std::vector<CEGUI::Window*> mChildMenuItems;

	bool event_menu_toggleButton(const CEGUI::EventArgs& pEventArgs);
public:
	MenuBar();
	~MenuBar();

	CEGUI::Window* const getWindow() const;
	bool addChildWindow(std::vector<MenuBarDataType> *vDataTypes);

	bool enableParent(std::string &name);
	bool disableParent(std::string &name);

	bool enableChild(std::string &name);
	bool disableChild(std::string &name);
}; 


class HeapMenuBar
{
protected:
	MenuBar *mMenubarWindow;
public:
	HeapMenuBar();
	~HeapMenuBar();
	
	void toggle(std::vector<MenuBarDataType> *vDataTypes);
	CEGUI::Window* const getWindow() const;
	bool isVisible() const;

	bool enableParent(std::string &name);
	bool disableParent(std::string &name);

	bool enableChild(std::string &name);
	bool disableChild(std::string &name);
}; 


#endif

