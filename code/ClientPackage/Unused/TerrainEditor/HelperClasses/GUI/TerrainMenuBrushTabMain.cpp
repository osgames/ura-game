/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TerrainMenuBrushTabMain.h"
#include "WorldEditorProperties.h"
#include "EditManager.h"
#include "CameraManager.h"
#include "TerrainManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace CEGUI;
using namespace std;



TerrainMenuBrushTabMain::TerrainMenuBrushTabMain()
{
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();


	// The scrollbar starts at 0 so we have to add +1;
	mMaxIntensity = 101;
	mMaxScale = 15001;


	mIntensityIncrease = 20;
	mIntensityDecrease = 20;

	mBrush_x_scale = 500;
	mBrush_y_scale = 500;

	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();

	// This variable can goto 0 in the scrollbar;
	mAutoRepeatRate = 200;


	/*** Allocate heap memory for the scrollpane object ***/
	mScrollPane = new CustomScrollPane;
}

TerrainMenuBrushTabMain::~TerrainMenuBrushTabMain()
{
	/*** Free the heap memory ***/
	if ( mScrollPane )
	{
		delete mScrollPane;
		mScrollPane = NULL;
	}

	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);

	// Destroy the brushes;
	for (unsigned long i = 0; i < mVimgSetBrushes.size(); i++)
	{	
		CEGUI::ImagesetManager::getSingleton().destroyImageset( mVimgSetBrushes.at(i) );
	}
}

CEGUI::Window* TerrainMenuBrushTabMain::getWindow()
{
	return mParentwin;
}

bool TerrainMenuBrushTabMain::addChildWindow()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool TerrainMenuBrushTabMain::addChildWindow()";
	string strLogMessage;

	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();
	
	CEGUI::PropertyHelper phelper;
	AccessFileSystem fileSystem;

	CEGUI::Window* scrollPaneWindow;
	CEGUI::Window* buttonReloadBrush;
	CEGUI::Window* frameBelowRefreshButton;
	CEGUI::Window* frameBelowDecreaseScrollbar;
	CEGUI::Window* frameBelow_AutoRepeat_Scrollbar;

	/*** Set the scrollbar variables ***/
	string strScrollpaneName = "TerrainMenuBrushTabMain_XXuniqueScrollpaneXX.";
	string strScrollImageSet = "basLook";
	string strScrollImageName = "bas_Color_blue";
	
	const long childWindow_x_pos = 9; 
	const long childWindow_x_rightSpace = 24;
	const long childWindow_y_pos = 25;
	const long childWindow_y_size = 50; 
	const long childWindow_y_space = 5;

	const long highligther_y_topSpace = -2; 
	const long highligther_y_bottomSpace = -15;

	string classBrushName = "TerrainMenuBrushTabMain_Brush_";
	string brushPath = FILEPATH_OF_SPT_BRUSHES;
	string brushFileFilter = ".png";

	/************************/
	/*** Load the brushes ***/
	/************************/

	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(brushPath) )
	{
		strLogMessage = "Could not find brush directory";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}

	fileSystem.getRecursiveFilesWithExtension(brushPath, &mVbrushNames, &brushFileFilter);

	for (unsigned long i = 0; i < mVbrushNames.size(); i++)
	{
		try
		{
			if ( imageSetManager.isImagesetPresent(classBrushName + mVbrushNames[i]) == false )
			{
				mVimgSetBrushes.push_back( imageSetManager.createImagesetFromImageFile(classBrushName + mVbrushNames[i], mVbrushNames[i]) );

				strLogMessage = "Successfully Loaded brush: " + mVbrushNames[i];
				ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
			}
		}
		catch(...)
		{
			strLogMessage = "Could not load brushes";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}

	}


	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","TerrainMenuBrushTab_Main_mParentwin");
	mParentwin->setText("Main");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 

	mParentwin->subscribeEvent( CEGUI::Window::EventSized, CEGUI::Event::Subscriber(
		&TerrainMenuBrushTabMain::event_parent_resize, this ) );

	mParentwin->subscribeEvent( CEGUI::Window::EventShown, CEGUI::Event::Subscriber(
		&TerrainMenuBrushTabMain::event_windowVisible, this ) );

	/******************************************************/
	/*** Create a child window to add the scrollpane to ***/
	/******************************************************/
	scrollPaneWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", "TerrainMenuBrushTab_Main_scrollPaneWindow");
	scrollPaneWindow->setProperty( "Image", "set:TaharezLook image:colorWhite" );
	

	/*************************************/
	/*** create reload brushes button  ***/
	/*************************************/
	buttonReloadBrush = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button", "TerrainMenuBrushTab_Main_buttonReloadBrush");
	buttonReloadBrush->setText("Reload Brushes");


	/**************************************************************/
	/***  Create the wood frame beneath the reload brush button ***/
	/**************************************************************/
	frameBelowRefreshButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuBrushTab_Main_frameBelowRefreshButton");
	frameBelowRefreshButton->setProperty("FrameEnabled", "False");
	frameBelowRefreshButton->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/******************************************************************/
	/*** Create the mLabelFlattenMode static text and radio buttons ***/
	/******************************************************************/

	// Create the static text
	mLabelFlattenMode = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTab_Main_mLabelFlattenMode");
	mLabelFlattenMode->setProperty("FrameEnabled", "False");
	mLabelFlattenMode->setProperty("BackgroundEnabled", "false");

	// Disable
	mLabelFlattenMode->disable();
	// Center the text;
	mLabelFlattenMode->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabelFlattenMode->setProperty("VertFormatting", "TopAligned"); 
	mLabelFlattenMode->setText( "Flatten Mode:" );


	mRadioFlattenPosition = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "TerrainMenuBrushTab_Main_mRadioFlattenPosition"));
	mRadioFlattenPosition->setGroupID(1);
	mRadioFlattenPosition->setText("Position");

	mRadioFlattenCenter = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "TerrainMenuBrushTab_Main_mRadioFlattenCenter"));
	mRadioFlattenCenter->setGroupID(1);
	mRadioFlattenCenter->setText("Center");

	mRadioFlattenBottom = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "TerrainMenuBrushTab_Main_mRadioFlattenBottom"));
	mRadioFlattenBottom->setGroupID(1);
	mRadioFlattenBottom->setText("Bottom");


	/**************************************************************/
	/***  Create the wood frame beneath the mRadioFlattenBottom ***/
	/**************************************************************/
	mFrameBelow_mRadioFlattenBottom = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuBrushTab_Main_mFrameBelow_mRadioFlattenBottom");
	mFrameBelow_mRadioFlattenBottom->setProperty("FrameEnabled", "False");
	mFrameBelow_mRadioFlattenBottom->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/************************************************************/
	/*** Create the terrain increase Scrollbar and static text***/
	/************************************************************/
	mTerrainIncreaseScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTab_Main_mTerrainIncreaseScrollbar");
	mTerrainIncreaseScrollbar->setDocumentSize(mMaxIntensity);
	mTerrainIncreaseScrollbar->setScrollPosition(mIntensityIncrease);
	mTerrainIncreaseScrollbar->setPageSize(1);
	mTerrainIncreaseScrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mTerrainIncreaseScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMain::event_mTerrainIncreaseScrollbar_change, this));


	// Create the static text
	mTerrainIncreaseText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTab_Main_mTerrainIncreaseText");
	mTerrainIncreaseText->setProperty("FrameEnabled", "False");
	mTerrainIncreaseText->setProperty("BackgroundEnabled", "false");

	// Disable the mTerrainIncreaseText;
	mTerrainIncreaseText->disable();
	// Set the text color;
	//mTerrainIncreaseText->setProperty("TextColours","tl:FF0000FF tr:FF0000FF bl:FF0000FF br:FF0000FF");
	//mTerrainIncreaseText->setProperty("TextColours","tl:FF000000 tr:FF000000 bl:FF000000 br:FF000000");
	// Center the text;
	mTerrainIncreaseText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mTerrainIncreaseText->setProperty("VertFormatting", "TopAligned"); 
	mTerrainIncreaseText->setText( "Terrain Increase: " + phelper.uintToString((unsigned int) mIntensityIncrease) );


	/************************************************************/
	/*** Create the terrain Decrease Scrollbar and static text***/
	/************************************************************/
	mTerrainDecreaseScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTab_Main_mTerrainDecreaseScrollbar");
	mTerrainDecreaseScrollbar->setDocumentSize(mMaxIntensity);
	mTerrainDecreaseScrollbar->setScrollPosition(mIntensityDecrease);
	mTerrainDecreaseScrollbar->setPageSize(1);
	mTerrainDecreaseScrollbar->setStepSize(1);
	

	// Set the scrollbar event to change the text;
	mTerrainDecreaseScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMain::event_mTerrainDecreaseScrollbar_change, this));

	
	// Create the static text
	mTerrainDecreaseText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTab_Main_mTerrainDecreaseText");
	mTerrainDecreaseText->setProperty("FrameEnabled", "False");
	mTerrainDecreaseText->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mTerrainDecreaseText->disable();
	// Center the text;
	mTerrainDecreaseText->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mTerrainDecreaseText->setProperty("VertFormatting", "TopAligned"); 
	mTerrainDecreaseText->setText( "Terrain Decrease: -" + phelper.uintToString( (unsigned int) mIntensityDecrease ) );


	/*************************************************************/
	/***  Create the wood frame beneath the Decrease Scrollbar ***/
	/*************************************************************/
	frameBelowDecreaseScrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", "TerrainMenuBrushTab_Main_frameBelowDecreaseScrollbar");
	frameBelowDecreaseScrollbar->setProperty("FrameEnabled", "False");
	frameBelowDecreaseScrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );
	

	/**********************************************************/
	/*** Create the Brush x scale scrollbar and static text ***/
	/**********************************************************/
	mBrush_x_scale_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTab_Main_mBrush_x_scale_Scrollbar");
	mBrush_x_scale_Scrollbar->setDocumentSize(mMaxScale);
	mBrush_x_scale_Scrollbar->setScrollPosition(mBrush_x_scale);
	mBrush_x_scale_Scrollbar->setPageSize(1);
	mBrush_x_scale_Scrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mBrush_x_scale_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMain::event_mBrush_x_scale_Scrollbar_change, this));

	
	// Create the static text
	mBrush_x_scale_Text = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTab_Main_mBrush_x_scale_Text");
	mBrush_x_scale_Text->setProperty("FrameEnabled", "False");
	mBrush_x_scale_Text->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mBrush_x_scale_Text->disable();
	// Center the text;
	mBrush_x_scale_Text->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mBrush_x_scale_Text->setProperty("VertFormatting", "TopAligned");
	mBrush_x_scale_Text->setText( "Brush Scale X: " + phelper.uintToString( (unsigned int) mBrush_x_scale) );


	/**********************************************************/
	/*** Create the Brush y scale scrollbar and static text ***/
	/**********************************************************/
	mBrush_y_scale_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTab_Main_mBrush_y_scale_Scrollbar");
	mBrush_y_scale_Scrollbar->setDocumentSize(mMaxScale);
	mBrush_y_scale_Scrollbar->setScrollPosition(mBrush_y_scale);
	mBrush_y_scale_Scrollbar->setPageSize(1);
	mBrush_y_scale_Scrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mBrush_y_scale_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMain::event_mBrush_y_scale_Scrollbar_change, this));

	
	// Create the static text
	mBrush_y_scale_Text = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTab_Main_mBrush_y_scale_Text");
	mBrush_y_scale_Text->setProperty("FrameEnabled", "False");
	mBrush_y_scale_Text->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mBrush_y_scale_Text->disable();
	// Center the text;
	mBrush_y_scale_Text->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mBrush_y_scale_Text->setProperty("VertFormatting", "TopAligned");
	mBrush_y_scale_Text->setText( "Brush Scale Y: " + phelper.uintToString( (unsigned int) mBrush_y_scale ) );


	/****************************************************************/
	/***  Create the wood frame beneath the brushYScale_Scrollbar ***/
	/****************************************************************/
	mFrameBelow_brushYScale_Scrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuBrushTab_Main_mFrameBelow_brushYScale_Scrollbar");
	mFrameBelow_brushYScale_Scrollbar->setProperty("FrameEnabled", "False");
	mFrameBelow_brushYScale_Scrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************************************************/
	/*** Create the mCamDegree_Scrollbar scrollbar and mCamDegreeLabel text label ***/
	/********************************************************************************/
	mCamDegree_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTabMain_mCamDegree_Scrollbar");
	mCamDegree_Scrollbar->setDocumentSize(201);
	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);
	mCamDegree_Scrollbar->setPageSize(1);
	mCamDegree_Scrollbar->setStepSize(0.001f);

	// Set the scrollbar event to change the text;
	mCamDegree_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMain::event_mCamDegree_Scrollbar_Scrollbar_change, this));

	
	// Create the static text
	mCamDegreeLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTabMain_mCamDegreeLabel");
	mCamDegreeLabel->setProperty("FrameEnabled", "False");
	mCamDegreeLabel->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mCamDegreeLabel->disable();
	// Center the text;
	mCamDegreeLabel->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mCamDegreeLabel->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mCamDegreeLabel->setText( "Camera Rate: " + phelper.floatToString(mCameraDegree) );


	/**********************************************************/
	/*** Create the autoRepeat scrollbar and static text ***/
	/**********************************************************/
	mAutoRepeat_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTab_Main_mBrushAutoRepeat_Scrollbar");
	mAutoRepeat_Scrollbar->setDocumentSize(201);
	mAutoRepeat_Scrollbar->setScrollPosition(mAutoRepeatRate);
	mAutoRepeat_Scrollbar->setPageSize(1);
	mAutoRepeat_Scrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mAutoRepeat_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMain::event_mAutoRepeatRate_Scrollbar_change, this));

	
	// Create the static text
	mAutoRepeat_Text = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTab_Main_mBrushAutoRepeat_Text");
	mAutoRepeat_Text->setProperty("FrameEnabled", "False");
	mAutoRepeat_Text->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mAutoRepeat_Text->disable();
	// Center the text;
	mAutoRepeat_Text->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mAutoRepeat_Text->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mAutoRepeat_Text->setText( "Mouse Rate: " + phelper.uintToString( (unsigned int) mAutoRepeatRate ) );


	/***************************************************************/
	/***  Create the wood frame beneath the autoRepeat Scrollbar ***/
	/***************************************************************/
	frameBelow_AutoRepeat_Scrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuBrushTab_Main_frameBelow_AutoRepeat_Scrollbar");
	frameBelow_AutoRepeat_Scrollbar->setProperty("FrameEnabled", "False");
	frameBelow_AutoRepeat_Scrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/***************************************************/
	/*** Create the windows to add to the scrollpane ***/
	/***************************************************/
	for ( unsigned long i = 0; i < mVbrushNames.size(); i++ )
	{
		// Add another scrollpanewindow
		mVScrollPaneWindows.push_back(NULL);

		/*** Creat the parent window ***/
		mVScrollPaneWindows.at(i) = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
			"TerrainMenuBrushTab_Main_scrollpane_window_Parent_image_" + mVbrushNames[i] );
		mVScrollPaneWindows.at(i)->setProperty("FrameEnabled", "False");
		mVScrollPaneWindows.at(i)->setProperty("BackgroundEnabled", "false");


		/*** Set the mouse down event for the windows, so that we can know when to change the brush ***/
		/*** Note: Make sure to use the buttonup event so that the scrollpane auto selector doens't get confused ***/
		mVScrollPaneWindows.at(i)->subscribeEvent( CEGUI::Window::EventMouseButtonUp, 
			CEGUI::Event::Subscriber( &TerrainMenuBrushTabMain::event_brushChange, this ) );


		/*** Create the image ***/
		CEGUI:: Window* scrollpaneImage = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
			"TerrainMenuBrushTab_Main_scrollpane_window_child_image_" + mVbrushNames[i]);
		scrollpaneImage->setProperty("FrameEnabled", "False");
		scrollpaneImage->setProperty("BackgroundEnabled", "false");
		scrollpaneImage->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.6F, 0.0F)));
		scrollpaneImage->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F) ));

		/*** Set the brush image ***/
		string strSetImage = "set:" + classBrushName + mVbrushNames[i] + " image:full_image";
		scrollpaneImage->setProperty( "Image", strSetImage );


		/*** Create the text window ***/
		CEGUI:: Window* scrollpaneText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
			"TerrainMenuBrushTab_Main_scrollpane_window_child_text" + mVbrushNames[i]);
		scrollpaneText->setProperty("FrameEnabled", "False");
		scrollpaneText->setProperty("BackgroundEnabled", "false");
		scrollpaneText->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.3F, 0.0F)));
		scrollpaneText->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.61F, 0.0F) ));

		// Disable the scrollpaneText;
		scrollpaneText->disable();
		// Set the text color;
		scrollpaneText->setProperty("TextColours","tl:FF0000FF tr:FF0000FF bl:FF0000FF br:FF0000FF");
		// Center the text;
/////////	scrollpaneText->setProperty("HorzFormatting", "HorzCentred"); 
		// Set the text;
		scrollpaneText->setText( mVbrushNames[i] );


		/*** Add the child windows to the parent window ***/
		mVScrollPaneWindows.at(i)->addChildWindow( scrollpaneImage );
		mVScrollPaneWindows.at(i)->addChildWindow( scrollpaneText );
	}


	/*********************************************************************/
	/*** Setup the scrollpane and add the  windows to the child window ***/
	/*********************************************************************/
	if (mScrollPane)
	{
		mScrollPane->set(scrollPaneWindow, &mVScrollPaneWindows, &strScrollpaneName, &strScrollImageSet, &strScrollImageName, false,
			childWindow_x_pos, childWindow_x_rightSpace,
			childWindow_y_pos, childWindow_y_size, childWindow_y_space,
			highligther_y_topSpace, highligther_y_bottomSpace);
	}
	else
	{
		strLogMessage = "mScrollPane";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

		return false;
	}	


	/*************************************************************************/
	/*** Setup the BrushTab that contains everything on it and its subtabs ***/
	/*************************************************************************/


	/*** Setup the "Main" Tab sizes and positions ***/
	
	float frame_x_pos = 0.00f;
	float frame_x_size = 1.0f;
	float frame_y_size = 0.018f;
	float frame_bottom_padding = 0.004f;
	float frame_top_padding = 0.008f;

	float text_x_pos = 0.07f;
	float text_y_size = 0.06f;

	float scrollbar_top_y_padding = 0.031f;
	float scrollbar_x_pos = 0.07f;
	float scrollbar_x_size = 0.86f;
	float scrollbar_y_size = 0.027f;


	float mScrollPaneWindow_x_pos = 0.195f;
	float mScrollPaneWindow_y_pos = 0.038f;
	float mScrollPaneWindow_x_size = 0.6f;
	float mScrollPaneWindow_y_size = 0.28f;	

	float mButtonReloadBrush_y_pos = mScrollPaneWindow_y_pos + mScrollPaneWindow_y_size + 0.021f;
	float mButtonReloadBrush_x_size = mScrollPaneWindow_x_size + 0.01;
	float mButtonReloadBrush_y_size = 0.035F;	

	float radio_y_size = 0.025f;


	// The y position is different because of the frame;
	float mFrameBelowRefreshButton_y_pos = mButtonReloadBrush_y_pos + mButtonReloadBrush_y_size + 0.005f;


	float mLabelFlattenMode_y_pos = mFrameBelowRefreshButton_y_pos + frame_y_size + frame_bottom_padding;
	float mRadioFlattenPosition_y_pos = mLabelFlattenMode_y_pos + 0.04f;
	float mRadioFlattenCenter_y_pos = mRadioFlattenPosition_y_pos + radio_y_size + 0.007f;
	float mRadioFlattenBottom_y_pos = mRadioFlattenCenter_y_pos + radio_y_size + 0.007f;

	float mFrameBelow_mRadioFlattenBottom_y_pos = mRadioFlattenBottom_y_pos + radio_y_size + 0.008f;


	float mTerrainIncreaseText_y_pos = mFrameBelow_mRadioFlattenBottom_y_pos + frame_y_size + frame_bottom_padding;
	float mTerrainIncreaseScrollbar_y_pos = mTerrainIncreaseText_y_pos + 0.031f;

	float mTerrainDecreaseText_y_pos = mTerrainIncreaseScrollbar_y_pos + scrollbar_top_y_padding;
	float mTerrainDecreaseScrollbar_y_pos = mTerrainDecreaseText_y_pos + scrollbar_top_y_padding;


	float mFrameBelowDecreaseScrollbar_y_pos = mTerrainDecreaseScrollbar_y_pos + scrollbar_y_size + frame_top_padding;

	// The y position is different because of the frame;
	float mBrush_x_scale_Text_y_pos = mFrameBelowDecreaseScrollbar_y_pos + frame_y_size + frame_bottom_padding;
	float mBrush_x_scale_Scrollbar_y_pos = mBrush_x_scale_Text_y_pos + scrollbar_top_y_padding;

	float mBrush_y_scale_Text_y_pos = mBrush_x_scale_Scrollbar_y_pos + scrollbar_top_y_padding;
	float mBrush_y_scale_Scrollbar_y_pos = mBrush_y_scale_Text_y_pos + scrollbar_top_y_padding;


	float mFrameBelow_brushYScale_Scrollbar_y_pos = mBrush_y_scale_Scrollbar_y_pos + scrollbar_y_size + frame_top_padding;


	// The y position is different because of the frame;
	float mCamDegreeLabel_y_pos = mFrameBelow_brushYScale_Scrollbar_y_pos + frame_y_size + frame_bottom_padding;
	float mCamDegree_Scrollbar_y_pos = mCamDegreeLabel_y_pos + scrollbar_top_y_padding;


	float mAutoRepeat_Text_y_pos = mCamDegree_Scrollbar_y_pos + scrollbar_top_y_padding;
	float mAutoRepeat_Scrollbar_y_pos = mAutoRepeat_Text_y_pos + scrollbar_top_y_padding;



	/*********************************************************************/
	/*** Set the size and positions of the windows for the "Main" tab ****/
	/*********************************************************************/


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	scrollPaneWindow->setPosition(UVector2(UDim(mScrollPaneWindow_x_pos, 0.0f), UDim(mScrollPaneWindow_y_pos, 0.0f)));
	scrollPaneWindow->setSize(UVector2(UDim(mScrollPaneWindow_x_size, 0.0f), UDim(mScrollPaneWindow_y_size, 0.0f))); 

	buttonReloadBrush->setPosition(UVector2(UDim(mScrollPaneWindow_x_pos, 0.0f), UDim(mButtonReloadBrush_y_pos, 0.0f)));
	buttonReloadBrush->setSize(UVector2(UDim(mButtonReloadBrush_x_size, 0.0f), UDim(mButtonReloadBrush_y_size, 0.0f))); 

	// Frame;
	frameBelowRefreshButton->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowRefreshButton_y_pos, 0.0f)));
	frameBelowRefreshButton->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	// Flatten label and radio buttons;
	mLabelFlattenMode->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mLabelFlattenMode_y_pos, 0.0f)));
	mLabelFlattenMode->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mRadioFlattenPosition->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mRadioFlattenPosition_y_pos, 0.0f)));
	mRadioFlattenPosition->setSize(UVector2(UDim(1.0f, 0.0f), UDim(radio_y_size, 0.0f)));

	mRadioFlattenCenter->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mRadioFlattenCenter_y_pos, 0.0f)));
	mRadioFlattenCenter->setSize(UVector2(UDim(1.0f, 0.0f), UDim(radio_y_size, 0.0f)));

	mRadioFlattenBottom->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mRadioFlattenBottom_y_pos, 0.0f)));
	mRadioFlattenBottom->setSize(UVector2(UDim(1.0f, 0.0f), UDim(radio_y_size, 0.0f)));

	mFrameBelow_mRadioFlattenBottom->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelow_mRadioFlattenBottom_y_pos, 0.0f)));
	mFrameBelow_mRadioFlattenBottom->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 


	// Terrain increase and decrease scrollbars;
	mTerrainIncreaseText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mTerrainIncreaseText_y_pos, 0.0f)));
	mTerrainIncreaseText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 
	mTerrainIncreaseScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mTerrainIncreaseScrollbar_y_pos, 0.0f)));
	mTerrainIncreaseScrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 

	mTerrainDecreaseText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mTerrainDecreaseText_y_pos, 0.0f)));
	mTerrainDecreaseText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 
	mTerrainDecreaseScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mTerrainDecreaseScrollbar_y_pos, 0.0f)));
	mTerrainDecreaseScrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	// Frame;
	frameBelowDecreaseScrollbar->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowDecreaseScrollbar_y_pos, 0.0f)));
	frameBelowDecreaseScrollbar->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 


	// Scale X, y, z;
	mBrush_x_scale_Text->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mBrush_x_scale_Text_y_pos, 0.0f)));
	mBrush_x_scale_Text->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mBrush_x_scale_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mBrush_x_scale_Scrollbar_y_pos, 0.0f)));
	mBrush_x_scale_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 

	mBrush_y_scale_Text->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mBrush_y_scale_Text_y_pos, 0.0f)));
	mBrush_y_scale_Text->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mBrush_y_scale_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mBrush_y_scale_Scrollbar_y_pos, 0.0f)));
	mBrush_y_scale_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	// Frame;
	mFrameBelow_brushYScale_Scrollbar->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelow_brushYScale_Scrollbar_y_pos, 0.0f)));
	mFrameBelow_brushYScale_Scrollbar->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 

	// Camera Degree;
	mCamDegreeLabel->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mCamDegreeLabel_y_pos, 0.0f)));
	mCamDegreeLabel->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mCamDegree_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mCamDegree_Scrollbar_y_pos, 0.0f)));
	mCamDegree_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 

	// Mouse Auto-repeat rate;
	mAutoRepeat_Text->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mAutoRepeat_Text_y_pos, 0.0f)));
	mAutoRepeat_Text->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mAutoRepeat_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mAutoRepeat_Scrollbar_y_pos, 0.0f)));
	mAutoRepeat_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	/***********************/
	/*** Handle settings ***/
	/***********************/

	// Select the first flatten radio button;
	mRadioFlattenPosition->setSelected(true);


	/*****************************/
	/*** Add the child Windows ***/
	/*****************************/
	mParentwin->addChildWindow( scrollPaneWindow );
	mParentwin->addChildWindow( buttonReloadBrush );
	mParentwin->addChildWindow( frameBelowRefreshButton );
	mParentwin->addChildWindow( mLabelFlattenMode );
	mParentwin->addChildWindow( mRadioFlattenPosition );
	mParentwin->addChildWindow( mRadioFlattenCenter );
	mParentwin->addChildWindow( mRadioFlattenBottom );
	mParentwin->addChildWindow( mFrameBelow_mRadioFlattenBottom );
	mParentwin->addChildWindow( mTerrainIncreaseText );
	mParentwin->addChildWindow( mTerrainIncreaseScrollbar );
	mParentwin->addChildWindow( mTerrainDecreaseText );
	mParentwin->addChildWindow( mTerrainDecreaseScrollbar );
	mParentwin->addChildWindow( frameBelowDecreaseScrollbar );
	mParentwin->addChildWindow( mBrush_x_scale_Text );
	mParentwin->addChildWindow( mBrush_x_scale_Scrollbar );
	mParentwin->addChildWindow( mBrush_y_scale_Text );
	mParentwin->addChildWindow( mBrush_y_scale_Scrollbar );
	mParentwin->addChildWindow( mFrameBelow_brushYScale_Scrollbar );
	mParentwin->addChildWindow( mCamDegreeLabel );
	mParentwin->addChildWindow( mCamDegree_Scrollbar );
	mParentwin->addChildWindow( mAutoRepeat_Text );
	mParentwin->addChildWindow( mAutoRepeat_Scrollbar );


	/*** Select the first brush and show it on startup ***/
	brushChange();


	return true;
} 

void TerrainMenuBrushTabMain::updateWindowShown()
{
	// Refresh the mouse rate;
	TerrainMenuBrushTabMain::_setMouseRate();

	// Refresh the scrollbars;
	TerrainMenuBrushTabMain::_reAdjustScrollbars();
}

inline void TerrainMenuBrushTabMain::_setMouseRate()
{
	// Set the mouse auto-repeat rate;
	CEGUI::System::getSingleton().getGUISheet()->setAutoRepeatRate(mAutoRepeatRate);
}

void TerrainMenuBrushTabMain::_reAdjustScrollbars()
{
	string tmpstr;
	CEGUI::PropertyHelper phelper;
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	// Set the Camera move Degree rate ***/
	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();
	
	/*** Set the camera rate scrollbar and label ***/
	tmpstr = "Camera Rate: ";

	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);

	tmpstr += phelper.floatToString(mCameraDegree).c_str();

	// Set the text;
	mCamDegreeLabel->setText( tmpstr );
}

void TerrainMenuBrushTabMain::brushChange()
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	unsigned long windowNumber = 0;

	Ogre::Vector2 screenPos(
	CEGUI::MouseCursor::getSingleton().getPosition().d_x / (float)sheet->getPixelRect().getWidth(),
	CEGUI::MouseCursor::getSingleton().getPosition().d_y / (float)sheet->getPixelRect().getHeight()
	);


	if ( mScrollPane )
	{
		windowNumber = mScrollPane->getSelectedWindow();
	}

	string& tmp = mVbrushNames[windowNumber];


	// Set the brush size;
	terrainManager->setBrushSize( Ogre::Vector2( (Ogre::Real) mBrush_x_scale, (Ogre::Real) mBrush_y_scale ) );

	// Set the brush;
	terrainManager->setBrush( tmp );

	// Set the brush position - The brush wont show unless we set its position;
	terrainManager->setBrushPositionToCursor();
}

void TerrainMenuBrushTabMain::terrainRaise()
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();

	terrainManager->displaceTerrain( mIntensityIncrease );	
}

void TerrainMenuBrushTabMain::terrainLower()
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();

	// Note: Do not forget to add the negative sign because the scrollbar just uses positive numbers;
	terrainManager->displaceTerrain( -mIntensityDecrease );	
}

void TerrainMenuBrushTabMain::terrainFlatten()
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();


	if ( mRadioFlattenPosition->isSelected() )
	{
		terrainManager->flattenTerrain();
	}
	else if ( mRadioFlattenCenter->isSelected() )
	{
		terrainManager->flattenTerrainToCenter();
	}
	else if ( mRadioFlattenBottom->isSelected() )
	{
		terrainManager->flattenTerrainToBottom();
	}
}

unsigned long TerrainMenuBrushTabMain::getBrushCount()
{
	if ( mScrollPane )
	{
		return mScrollPane->getWindowCount();
	}

	return 0;
}

unsigned long TerrainMenuBrushTabMain::getSelectedBrush()
{
	if ( mScrollPane )
	{
		return mScrollPane->getSelectedWindow();
	}

	return 0;
}

void TerrainMenuBrushTabMain::setBrush(const unsigned long number)
{
	// highlight the brush image and set the number;
	if ( mScrollPane )
	{
		mScrollPane->setWindow(number);
	}
	else
	{
		mScrollPane->setWindow(0);
	}

	// Change the brush;
	brushChange();
}

void TerrainMenuBrushTabMain::updateTerrainBrush()
{
  this->brushChange();
}

void TerrainMenuBrushTabMain::setDisplaceDocumentSize(const unsigned long number)
{
	mMaxIntensity = number;

	mIntensityIncrease = 0;
	mIntensityDecrease = 0;

	mTerrainIncreaseScrollbar->setDocumentSize(mMaxIntensity);
	mTerrainIncreaseScrollbar->setScrollPosition(mIntensityIncrease);

	mTerrainDecreaseScrollbar->setDocumentSize(mMaxIntensity);
	mTerrainDecreaseScrollbar->setScrollPosition(mIntensityDecrease);
}

void TerrainMenuBrushTabMain::setScaleDocumentSize(const unsigned long number)
{
	mMaxScale = number;

	mBrush_x_scale = 0;
	mBrush_y_scale = 0;

	mBrush_x_scale_Scrollbar->setDocumentSize(mMaxScale);
	mBrush_x_scale_Scrollbar->setScrollPosition(mBrush_x_scale);

	mBrush_y_scale_Scrollbar->setDocumentSize(mMaxScale);
	mBrush_y_scale_Scrollbar->setScrollPosition(mBrush_y_scale);
}


bool TerrainMenuBrushTabMain::event_windowVisible(const CEGUI::EventArgs& pEventArgs)
{
	TerrainMenuBrushTabMain::updateWindowShown();

	return true;
}

bool TerrainMenuBrushTabMain::event_parent_resize(const CEGUI::EventArgs& pEventArgs)
{
	if ( mScrollPane )
	{
		mScrollPane->resize();
	}

	return true;
}

bool TerrainMenuBrushTabMain::event_brushChange(const CEGUI::EventArgs& pEventArgs)
{
	brushChange();

	return true;
}

bool TerrainMenuBrushTabMain::event_mTerrainIncreaseScrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Terrain Increase: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mTerrainIncreaseScrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '1';
	// if the scroll pos is at 0 then we set the scroll position to 1;
	if (scrollPos < 1 )
	{
		scrollPos++;
		 mTerrainIncreaseScrollbar->setScrollPosition(1);
	}

	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mTerrainIncreaseText->setText( tmpstr ); 

	// Set the class member;
	mIntensityIncrease = scrollPos;

	// Handle a property change;
	brushChange();


	return true;
}

bool TerrainMenuBrushTabMain::event_mTerrainDecreaseScrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Terrain Decrease: -";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos =  mTerrainDecreaseScrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '1';
	// if the scroll pos is at 0 then we set the scroll position to 1;
	if (scrollPos < 1)
	{
		scrollPos++;
		 mTerrainDecreaseScrollbar->setScrollPosition(1);
	}

	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mTerrainDecreaseText->setText( tmpstr ); 

	// Set the class member;
	mIntensityDecrease = scrollPos;

	// Handle a property change;
	brushChange();


	return true;
}


bool TerrainMenuBrushTabMain::event_mBrush_x_scale_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Brush Scale X: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real xscale = mBrush_x_scale_Scrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '1';
	// if the scroll pos is at 0 then we set the scroll position to 1;
	if (xscale < 1)
	{
		xscale++;
		 mBrush_x_scale_Scrollbar->setScrollPosition(1);
	}

	tmpstr += phelper.uintToString( xscale ).c_str();

	// Set the text;
	mBrush_x_scale_Text->setText( tmpstr ); 

	// Set the class member;
	mBrush_x_scale = xscale;

	// Handle a property change;
	brushChange();

	return true;
}

bool TerrainMenuBrushTabMain::event_mBrush_y_scale_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Brush Scale Y: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real yscale = mBrush_y_scale_Scrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '1';
	// if the scroll pos is at 0 then we set the scroll position to 1;
	if (yscale < 1)
	{
		yscale++;
		 mBrush_y_scale_Scrollbar->setScrollPosition(1);
	}

	tmpstr += phelper.uintToString( yscale ).c_str();

	// Set the text;
	mBrush_y_scale_Text->setText( tmpstr ); 

	// Set the class member;
	mBrush_y_scale = yscale;

	// Handle a property change;
	brushChange();

	return true;
}

bool TerrainMenuBrushTabMain::event_mCamDegree_Scrollbar_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Camera Rate: ";
	CEGUI::PropertyHelper phelper;
	float scrollPos = mCamDegree_Scrollbar->getScrollPosition();
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();


	// Since the scrollbar starts at '0', we want it to never decrease passed '0.001';
	// if the scroll pos is at 0 then we set the scroll position to 0.001;
	if (scrollPos < 0.001f)
	{
		scrollPos = 0.001f;
		mCamDegree_Scrollbar->setScrollPosition(scrollPos);
	}

	tmpstr += phelper.floatToString(scrollPos).c_str();

	// Set the text;
	mCamDegreeLabel->setText( tmpstr ); 

	// Set the class member;
	mCameraDegree = scrollPos;


	// Set the camera sceneNode and camera degree;
	cameraManager->setCameraSceneNodeMoveDegree(mCameraDegree);
	cameraManager->setCameraMoveDegree(mCameraDegree);

	return true;
}

bool TerrainMenuBrushTabMain::event_mAutoRepeatRate_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Mouse Rate: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mAutoRepeat_Scrollbar->getScrollPosition();

	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mAutoRepeat_Text->setText( tmpstr ); 

	// Set the class member;
	mAutoRepeatRate = scrollPos;

	// Set the mouse auto-repeat rate;
	_setMouseRate();

	return true;
}


