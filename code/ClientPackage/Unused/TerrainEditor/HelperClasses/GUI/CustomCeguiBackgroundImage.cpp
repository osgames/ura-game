/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CustomCeguiBackgroundImage.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"

#define IMAGESET_NAME	"CustomCeguiBackgroundImage_load"
#define IMAGE_PROPERTY	"set:CustomCeguiBackgroundImage_load image:full_image"


using namespace std;
using namespace CEGUI;



CustomCeguiBackgroundImage::CustomCeguiBackgroundImage()
{
	mImageSet = NULL;
}

CustomCeguiBackgroundImage::~CustomCeguiBackgroundImage()
{
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();

	// Only continue if the heap pointer is allocated;
	// If we continue than delete the image set if it exists and delete the heap pointer;
	if (mImageSet != NULL)
	{
		// IF the imageset is present, then delete it;
		if ( imageSetManager.isImagesetPresent(IMAGESET_NAME) )
		{
			CEGUI::ImagesetManager::getSingleton().destroyImageset( *mImageSet );
		}

		// Delete the heap pointer;
		delete mImageSet;

		// Set the pointer back to NULL;
		mImageSet = NULL;
	}
}

void CustomCeguiBackgroundImage::load(CEGUI::Window *window, string &fileName)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void CustomCeguiBackgroundImage::load(...)";
	string strLogMessage;

	if (mImageSet == NULL)
	{
		mImageSet = new CEGUI::Imageset*;
		CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();
	
		try
		{
			// Make sure the image isn't already loaded before we load it;
			if ( imageSetManager.isImagesetPresent(IMAGESET_NAME) == false )
			{
				*mImageSet = imageSetManager.createImagesetFromImageFile(IMAGESET_NAME, fileName);
			}

			// Set the window image property;
			window->setProperty("Image", IMAGE_PROPERTY);
			 //mSheet->setImage("MainMenu_BackgroundImage", "full_image");
		}
		catch( ... )
		{
			strLogMessage = "Could not Create the imageset";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}
	}
	else
	{
			strLogMessage = "Trying to create a imageset When an image already exists";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
	}
}

void CustomCeguiBackgroundImage::unload(CEGUI::Window *window)
{
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();

	// Only continue if the heap pointer is allocated;
	// If we continue than delete the image set if it exists and delete the heap pointer;
	if (mImageSet != NULL)
	{
		// IF the imageset is present, then delete it;
		if ( imageSetManager.isImagesetPresent(IMAGESET_NAME) )
		{
			
			window->setProperty("Image", "");
			CEGUI::ImagesetManager::getSingleton().destroyImageset( *mImageSet );
		}

		// Delete the heap pointer;
		delete mImageSet;
		mImageSet = NULL;
	}
}



