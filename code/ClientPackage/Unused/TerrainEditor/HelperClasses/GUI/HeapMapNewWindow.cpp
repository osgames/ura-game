/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapMapNewWindow.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "GUIManager.h"
#include "GUI.h"
#include "ModelMenu.h"
#include "CustomSceneNodeManager.h"
#include "CustomSceneNodeManipulator.h"



using namespace std;



HeapMapNewWindow::HeapMapNewWindow() : mNewMapWindow(NULL)
{

}

HeapMapNewWindow::~HeapMapNewWindow()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapMapNewWindow::~HeapMapNewWindow()";
	string strLogMessage;

	if (mNewMapWindow)	
	{
		delete mNewMapWindow;
		mNewMapWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted MapNewWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

CEGUI::Window* const HeapMapNewWindow::getWindow() const
{
	if (mNewMapWindow)
	{
		return mNewMapWindow->getWindow();
	}

	return NULL;
}

void HeapMapNewWindow::toggle()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapMapNewWindow::toggle()";
	string strLogMessage;


	if ( mNewMapWindow )	
	{
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		delete mNewMapWindow;
		mNewMapWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted MapNewWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mNewMapWindow = new MapNewWindow;
		
		if ( mNewMapWindow == NULL )
		{ 
			strLogMessage = "mNewMapWindow";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mNewMapWindow->addChildWindow();

		strLogMessage = "created MapNewWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

bool HeapMapNewWindow::isVisible() const
{
	if (mNewMapWindow)
	{
		return mNewMapWindow->getWindow()->isVisible();
	}

	return false;
}







MapNewWindow::MapNewWindow() :	mLoadMapWin(NULL),
				mMessagebox(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "MapNewWindow::MapNewWindow()";
	string strLogMessage;


	/*** Alocate heap memory for the GuiMessageBox ***/
	mMessagebox = new GUIMessageBox;

	if ( mMessagebox == NULL)
	{
		strLogMessage = "mMessagebox";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}
}

MapNewWindow::~MapNewWindow()
{
	if ( mMessagebox != NULL )
	{
		delete mMessagebox;
		mMessagebox = NULL;
	}

	/*** Destroy the window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mLoadMapWin);
}


CEGUI::Window* const MapNewWindow::getWindow() const
{
	return mLoadMapWin;
}

bool MapNewWindow::addChildWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MapNewWindow::addChildWindow()";
	string strLogMessage;


	try
	{
		/******************************/
		/*** Create the main window ***/
		/******************************/
		mLoadMapWin = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "MapNewWindow::mLoadMapWin");
		//mLoadMapWin->setPosition( CEGUI::UVector2(CEGUI::UDim(0.3, 0.0F), CEGUI::UDim(0.3F, 0.0F) ) );
		mLoadMapWin->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0.0F), CEGUI::UDim(0.4, 0.0F)));
		mLoadMapWin->setText("  Map Settings");

		mLoadMapWin->setHorizontalAlignment(CEGUI::HA_CENTRE);
		mLoadMapWin->setVerticalAlignment(CEGUI::VA_CENTRE);


		/************************************/
		/*** Create the main Child Window ***/
		/************************************/
		mMainChildwindow =  windowManager.createWindow("TaharezLook/Listbox_woodframe","MapNewWindow::mMainChildwindow");
		mMainChildwindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.036, 0.0F), CEGUI::UDim(0.12, 0.0F) ) );
		mMainChildwindow->setSize(CEGUI::UVector2(CEGUI::UDim(0.924, 0.0F), CEGUI::UDim(0.825, 0.0F)));


		/****************************************************/
		/*** Create the StaticText name Label and EditBox ***/
		/****************************************************/

		// Name Label;
		mNameLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
			"MapNewWindow::mNameLabel");
		mNameLabel->setProperty("FrameEnabled", "False");
		mNameLabel->setProperty("BackgroundEnabled", "false");
		mNameLabel->setText("Name:");	// Set the text;
		mNameLabel->disable();

		// Name Editbox;
		mNameEditbox = (CEGUI::Editbox*) windowManager.createWindow("TaharezLook/Editbox",
			"MapNewWindow::mNameEditbox");

		// Set the Default name;
		mNameEditbox->setText("MapTest");

		/*****************************************************/
		/*** Create the StaticText Width Label and EditBox ***/
		/*****************************************************/

		// Width Label;
		mWidthLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
			"MapNewWindow::mWidthLabel");
		mWidthLabel->setProperty("FrameEnabled", "False");
		mWidthLabel->setProperty("BackgroundEnabled", "false");
		mWidthLabel->setText("Size:");	// Set the text;
		mWidthLabel->disable();

		// Width Editbox;
		mWidthEditbox = (CEGUI::Editbox*) windowManager.createWindow("TaharezLook/Editbox",
			"MapNewWindow::mWidthEditbox");
		
		// Set the Default Width;
		mWidthEditbox->setText("50000");

		/******************************************************/
		/*** Create the StaticText Height Label and EditBox ***/
		/******************************************************/

		// Width Label;
		mHeightLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
			"MapNewWindow::mHeightLabel");
		mHeightLabel->setProperty("FrameEnabled", "False");
		mHeightLabel->setProperty("BackgroundEnabled", "false");
		mHeightLabel->setText("Max Height:");	// Set the text;
		mHeightLabel->disable();

		// Width Editbox;
		mHeightEditbox = (CEGUI::Editbox*) windowManager.createWindow("TaharezLook/Editbox",
			"MapNewWindow::mHeightEditbox");

		// Set the Default Max Height;
		mHeightEditbox->setText("1000");

		/***************************************/
		/*** Create the Ok and Cancel Button ***/
		/***************************************/

		mOkButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "MapNewWindow::mOkButton");
		mOkButton->setText("Ok");

		mCancelButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "MapNewWindow::mCancelButton");
		mCancelButton->setText("Cancel");


		/**********************************/
		/*** Set the size and positions ***/
		/**********************************/

		float label_x_pos = 0.1F;
		float editbox_x_pos = 0.4F;

		float button_x_size = 0.35f;
		float button_y_size = 0.09f;
		float button_y_pos = 0.8f;

		float mOkButton_x_pos = label_x_pos;
		float mCancelButton_x_pos = 0.55f;


		mNameLabel->setSize(CEGUI::UVector2(CEGUI::UDim(0.3F, 0.0F), CEGUI::UDim(0.3F, 0.0F)));
		mNameLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(label_x_pos, 0.0F), CEGUI::UDim(0.05F, 0.0F) ));

		mNameEditbox->setSize(CEGUI::UVector2(CEGUI::UDim(0.5F, 0.0F), CEGUI::UDim(0.11F, 0.0F)));
		mNameEditbox->setPosition( CEGUI::UVector2(CEGUI::UDim(editbox_x_pos, 0.0F), CEGUI::UDim(0.16F, 0.0F) ));


		mWidthLabel->setSize(CEGUI::UVector2(CEGUI::UDim(0.3F, 0.0F), CEGUI::UDim(0.3F, 0.0F)));
		mWidthLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(label_x_pos, 0.0F), CEGUI::UDim(0.25F, 0.0F) ));

		mWidthEditbox->setSize(CEGUI::UVector2(CEGUI::UDim(0.5F, 0.0F), CEGUI::UDim(0.11F, 0.0F)));
		mWidthEditbox->setPosition( CEGUI::UVector2(CEGUI::UDim(editbox_x_pos, 0.0F), CEGUI::UDim(0.35F, 0.0F) ));


		mHeightLabel->setSize(CEGUI::UVector2(CEGUI::UDim(0.3F, 0.0F), CEGUI::UDim(0.3F, 0.0F)));
		mHeightLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(label_x_pos, 0.0F), CEGUI::UDim(0.44F, 0.0F) ));

		mHeightEditbox->setSize(CEGUI::UVector2(CEGUI::UDim(0.5F, 0.0F), CEGUI::UDim(0.11F, 0.0F)));
		mHeightEditbox->setPosition( CEGUI::UVector2(CEGUI::UDim(editbox_x_pos, 0.0F), CEGUI::UDim(0.54F, 0.0F) ));


		mOkButton->setSize(CEGUI::UVector2(CEGUI::UDim(button_x_size, 0), CEGUI::UDim(button_y_size, 0)));
		mOkButton->setPosition( CEGUI::UVector2(CEGUI::UDim(mOkButton_x_pos, 0), CEGUI::UDim(button_y_pos, 0) ) );

		mCancelButton->setSize(CEGUI::UVector2(CEGUI::UDim(button_x_size, 0), CEGUI::UDim(button_y_size, 0)));
		mCancelButton->setPosition( CEGUI::UVector2(CEGUI::UDim(mCancelButton_x_pos, 0), CEGUI::UDim(button_y_pos, 0) ) );


		/***********************************************************/
		/*** Create the GuiMessageBox and attach it to the sheet ***/
		/***********************************************************/
		mMessagebox->addWindow();


		/**************/
		/*** Events ***/
		/**************/

		mOkButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
				&MapNewWindow::event_button_ok_click, this ) );
		
		mCancelButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
				&MapNewWindow::event_mCancelButton_click, this ) );

		/* Prevent special key acton while in the editboxes */
		mNameEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber( 
			&MapNewWindow::event_editBox_activated, this ) );

		mWidthEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber( 
			&MapNewWindow::event_editBox_activated, this ) );

		mHeightEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber( 
			&MapNewWindow::event_editBox_activated, this ) );


		/********************/
		/*** The Settings ***/
		/********************/
	
		// Only allow this window to be accessed until the ok button is clicked;
		mLoadMapWin->setModalState(true);


		/************************************/
		/*** Add the Windows to the sheet ***/
		/************************************/
		mMainChildwindow->addChildWindow(mNameLabel);
		mMainChildwindow->addChildWindow(mNameEditbox);

		mMainChildwindow->addChildWindow(mWidthLabel);
		mMainChildwindow->addChildWindow(mWidthEditbox);

		mMainChildwindow->addChildWindow(mHeightLabel);
		mMainChildwindow->addChildWindow(mHeightEditbox);

		mMainChildwindow->addChildWindow(mOkButton);
		mMainChildwindow->addChildWindow(mCancelButton);

		mLoadMapWin->addChildWindow(mMainChildwindow);
		sheet->addChildWindow(mLoadMapWin);
	}
	catch(...)
	{
		strLogMessage = "An unknown error occured";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}


	return true;
} 



bool MapNewWindow::event_button_ok_click(const CEGUI::EventArgs &e)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = customSceneNodeManager->getCustomSceneNodeManipulator();

	Ogre::StringConverter sc;

	string strName = mNameEditbox->getText().c_str();
	string strWidth = mWidthEditbox->getText().c_str();
	string strMaxHeight = mHeightEditbox->getText().c_str();

	Ogre::Real width = 0.0;
	Ogre::Real maxHeight = 0.0;



	/*** Determine if the editboxes are empty ***/
	if ( strName.size() == 0 || strWidth.size() == 0 || strMaxHeight.size() == 0 )
	{
		string title = "ERROR";
		string functionName = "bool MapNewWindow::event_button_ok_click(...)";
		string errorMsg = "An Editbox is empty.";

		mMessagebox->show( &title, &functionName, &errorMsg );
		return false;
	}
	

	/*** Determine if we are getting numbers like we should be getting  ***/
	if ( sc.isNumber(strWidth) )
	{
		
	}
	else
	{
		string title = "ERROR";
		string functionName = "bool MapNewWindow::event_button_ok_click(...)";
		string errorMsg = "Width Editbox has an invalid number.";

		mMessagebox->show( &title, &functionName, &errorMsg );
		return false;
	}	

	if ( sc.isNumber(strMaxHeight) )
	{
		
	}
	else
	{
		string title = "ERROR";
		string functionName = "bool MapNewWindow::event_button_ok_click(...)";
		string errorMsg = "Max Height Editbox has an invalid number.";

		mMessagebox->show( &title, &functionName, &errorMsg );
		return false;
	}


	/*** Determine that the max Height is bigger than 0 and max width is bigger than 100 ***/
	width = sc.parseReal(strWidth);
	maxHeight = sc.parseReal(strMaxHeight);

	if ( width > 100.0 )
	{
	
	}
	else
	{
		string title = "ERROR";
		string functionName = "bool MapNewWindow::event_button_ok_click(...)";
		string errorMsg = "Width must be bigger than 100.";

		mMessagebox->show( &title, &functionName, &errorMsg );
		return false;
	}

	if ( maxHeight > 0.0 )
	{
	
	}
	else
	{
		string title = "ERROR";
		string functionName = "bool MapNewWindow::event_button_ok_click(...)";
		string errorMsg = "Max Height must be bigger than 0.";

		mMessagebox->show( &title, &functionName, &errorMsg );
		return false;
	}



	/************************************************************/
	/*** If we got here then all the editbox variables are ok ***/
	/************************************************************/


	/*** First Destroy the scene ***/
	try
	{
		customSceneNodeManager->destroyAllSceneNodesAndMovableObjects();
	}
	catch(...)
	{
		return false;
	}


	/*** We clear the selected list to make sure we have no left over pointers ***/
	customSceneNodeManipulator->emptySelected();


	/*** Handle closing the window ***/

	// Set the windows to invisible and allow the other windows to be accessed again;
	mLoadMapWin->setModalState(false);	// Allow the other windows to be accessed again;

	// We set the GUI windows to not busy;
	guiManager->setEnabledCommandKeys(true);

	gui->event_NewMapWindowClose(true, strName, width, maxHeight);

	
	return true;
}

bool MapNewWindow::event_mCancelButton_click(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();

	/*** Allow the other windows to be accessed again ***/
	mLoadMapWin->setModalState(false);	// Allow the other windows to be accessed again;

	// We set the GUI windows to not busy;
	guiManager->setEnabledCommandKeys(true);

	/*** Inform the GUI that the window was closed and that the cancel button was hit ***/
	gui->event_NewMapWindowClose(false, "", 0, 0);

	return true;
}


/*********************************************************************************************/
/*** These two events make sure that we aren't using special keys when typing regular text ***/
/*********************************************************************************************/

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool MapNewWindow::event_editBox_activated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mNameEditbox->isActive() || mWidthEditbox->isActive() || mHeightEditbox->isActive() )
	{
		// We set the GUI windows to busy;
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		// We set the GUI windows to not busy;
		guiManager->setEnabledCommandKeys(true);
	}

	return true;	
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool MapNewWindow::event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();

	if ( mNameEditbox->isActive() || mWidthEditbox->isActive() || mHeightEditbox->isActive() )
	{
		// We set the GUI windows to busy;
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		// We set the GUI windows to not busy;
		guiManager->setEnabledCommandKeys(true);
	}

	return true;	
}

