/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapQuitWindow.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "EditManager.h"
#include "GUIManager.h"
#include "GUI.h"


using namespace std;
using namespace CEGUI;


HeapQuitWindow::HeapQuitWindow() : mQuitWindow(NULL)
{

}

HeapQuitWindow::~HeapQuitWindow()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapQuitWindow::~HeapQuitWindow()";
	string strLogMessage;


	if (mQuitWindow)
	{
		strLogMessage = "Deleted mQuitWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);

		delete mQuitWindow;
		mQuitWindow = NULL;
	}
}

void HeapQuitWindow::toggle()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapQuitWindow::toggle(...)";
	string strLogMessage;


	/*** If visible, then hide and delete the window and object. Else we create the object ***/

	if (mQuitWindow)
	{
		/*** Delete the object and set the pointer to NULL ***/
		delete mQuitWindow;
		mQuitWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted mQuitWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mQuitWindow = new QuitWindow;

		if (!mQuitWindow)
		{
			strLogMessage = "mQuitWindow";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mQuitWindow->addChildWindow();

		strLogMessage = "Created mQuitWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

bool HeapQuitWindow::isVisible()
{
	if (mQuitWindow)
	{
		return mQuitWindow->getWindow()->isVisible();
	}

	return false;
}

CEGUI::Window* const HeapQuitWindow::getWindow() const
{
	if (mQuitWindow)
	{
		return mQuitWindow->getWindow();
	}

	return NULL;
}






QuitWindow::QuitWindow()
{

}

QuitWindow::~QuitWindow()
{
	/*** Destroy the cegui window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(DEF_QUITWINDOW_MAINWINDOW_NAME);
}

void QuitWindow::toggle() const
{
	CEGUI::Window* const mainWindow = CEGUI::WindowManager::getSingleton().getWindow(DEF_QUITWINDOW_MAINWINDOW_NAME);

	if ( mainWindow->isVisible() == false )
	{
		mainWindow->show();
		mainWindow->activate();
	}
	else if( mainWindow->isVisible() == true )
	{
		mainWindow->hide();
	}
}

CEGUI::Window* const QuitWindow::getWindow() const
{
	return CEGUI::WindowManager::getSingleton().getWindow(DEF_QUITWINDOW_MAINWINDOW_NAME);
}

bool QuitWindow::addChildWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::FrameWindow* mainWindow = NULL;
	CEGUI::Window* mainChildWindow = NULL;
	CEGUI::Window* quitMenuLabel = NULL;
	CEGUI::PushButton* quitMenu_button_yes = NULL;
	CEGUI::PushButton* quitMenu_button_no = NULL;


	// Quit Menu
	mainWindow = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", DEF_QUITWINDOW_MAINWINDOW_NAME);
	mainWindow->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.3, 0)));
	mainWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0.3, 0), CEGUI::UDim(0.25, 0) ) );

	// Main Child Window of which everything is attached to;
	mainChildWindow = windowManager.createWindow("TaharezLook/Listbox_woodframe","QuitWindow_mainChildWindow");
	mainChildWindow->setSize(UVector2(UDim(0.93f, 0.0f), UDim(0.77f, 0.0f))); 
	mainChildWindow->setPosition(UVector2(UDim(0.035f, 0.0f), UDim(0.15f, 0.0f))); 

	// Quit menu button yes;
	quitMenu_button_yes = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "QuitWindow_quitMenu_button_yes");
	quitMenu_button_yes->setText("Yes");
	quitMenu_button_yes->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.15, 0)));
	quitMenu_button_yes->setPosition( CEGUI::UVector2(CEGUI::UDim(0.07, 0), CEGUI::UDim(0.7, 0) ) );
	quitMenu_button_yes->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&QuitWindow::event_quit, this ) );

	// Quit menu button no;
	quitMenu_button_no = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "QuitWindow_quitMenu_button_no");
	quitMenu_button_no->setText("No");
	quitMenu_button_no->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.15, 0)));
	quitMenu_button_no->setPosition( CEGUI::UVector2(CEGUI::UDim(0.53, 0), CEGUI::UDim(0.7, 0) ) );
	quitMenu_button_no->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&QuitWindow::event_noQuit, this ) );

	// Quit menu label;
	quitMenuLabel = (CEGUI::Window *) windowManager.createWindow("TaharezLook/StaticText", "QuitWindow_quitMenuLabel");
	quitMenuLabel->setSize(CEGUI::UVector2(CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
	quitMenuLabel->setText("Are you sure you want to quit?");
	// Disable frame and background;
	quitMenuLabel->setProperty("FrameEnabled", "false");
	quitMenuLabel->setProperty("BackgroundEnabled", "false");
	// Make the label text Vertically and Horizontally Centered
	quitMenuLabel->setProperty("VertFormatting", "VertCentred");
	quitMenuLabel->setProperty("HorzFormatting",  "HorzCentred");
	// Disable the label so it doesn't steal the events;
	quitMenuLabel->disable();


	/*** Add the windows ***/
	mainChildWindow->addChildWindow( quitMenuLabel );
	mainChildWindow->addChildWindow( quitMenu_button_yes );
	mainChildWindow->addChildWindow( quitMenu_button_no );

	mainWindow->addChildWindow( mainChildWindow );
	sheet->addChildWindow( mainWindow );


	return true;
}

bool QuitWindow::event_noQuit(const CEGUI::EventArgs &e)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();
	HeapQuitWindow* const heapQuitWindow = gui->getHeapQuitWindow();

	if (heapQuitWindow)
	{
		heapQuitWindow->toggle();
	}


	return true;
}

bool QuitWindow::event_quit(const CEGUI::EventArgs &e)
{
	EditManager* const editManager = EditManager::getSingletonPtr();
	editManager->quit();


	return true;
}

