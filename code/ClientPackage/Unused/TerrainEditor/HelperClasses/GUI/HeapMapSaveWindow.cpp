/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapMapSaveWindow.h"
#include "WorldEditorProperties.h"
#include "AccessFileSystem.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "EditManager.h"
#include "GUIManager.h"
#include "GUI.h"
#include "ModelMenu.h"
#include "ModelEditorMainTab.h"


using namespace std;
using namespace CEGUI;
using namespace Ogre;

MapSaveWindow::MapSaveWindow() :	mParentwin(NULL),
					mGuiMessageBox(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "MapSaveWindow::MapSaveWindow()";
	string strLogMessage;


	/*** allocate heap memory for the gui messagebox ***/
	mGuiMessageBox = new GUIMessageBox;

	if (mGuiMessageBox == NULL)
	{
		strLogMessage = "mGuiMessageBox";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}
}

MapSaveWindow::~MapSaveWindow()
{
	if ( mGuiMessageBox )
	{
		delete mGuiMessageBox;
		mGuiMessageBox = NULL;
	}

	// Destroy the cegui window;
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);	
}

CEGUI::Window* const MapSaveWindow::getWindow()
{
	return mParentwin;
}

bool MapSaveWindow::addChildWindow()
{
	CEGUI::Window* const sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	


	// Create the GuiMessageBox and add it to the sheet;
	if ( mGuiMessageBox )
	{
		mGuiMessageBox->addWindow();
	}


	/******************************/
	/*** Create the main window ***/
	/******************************/
	mParentwin = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "MapSaveWindow_mParentwin");
	mParentwin->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0.0F), CEGUI::UDim(0.8, 0.0F)));
	mParentwin->setText("  Save Map");


	/************************************/
	/*** Create the main Child Window ***/
	/************************************/
	mMainChildwin = windowManager.createWindow("TaharezLook/Listbox_woodframe","MapSaveWindow_mMainChildwin");
	mMainChildwin->setPosition( CEGUI::UVector2(CEGUI::UDim(0.036, 0.0F), CEGUI::UDim(0.055, 0.0F) ) );
	mMainChildwin->setSize(CEGUI::UVector2(CEGUI::UDim(0.924, 0.0F), CEGUI::UDim(0.92, 0.0F)));


	/**********************************************************/
	/*** Create the mListbox to select what files to handle ***/
	/**********************************************************/

	// the listbox;
	mListbox = (CEGUI::Listbox *) windowManager.createWindow("TaharezLook/Listbox_woodframe","MapSaveWindow_mMeshListbox");


	/*********************************/
	/*** Create the filename Label ***/
	/*********************************/

	// Name Label;
	mFileNameLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"MapSaveWindow_mFileNameLabel");
	mFileNameLabel->setProperty("FrameEnabled", "False");
	mFileNameLabel->setProperty("BackgroundEnabled", "false");
	mFileNameLabel->setText("File Name:");	// Set the text;
	mFileNameLabel->disable();



	/**********************************************************/
	/*** Create the Editbox to optionally insert a filename ***/
	/**********************************************************/

	// Create the edit box;
	mNameEditbox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "MapSaveWindow_mNameEditbox");


	/************************************************/
	/*** Create the reload, ok and cancel buttons ***/
	/************************************************/

	// The reload button;
	mReloadButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "MapSaveWindow_mReloadButton");
	mReloadButton->setText("Reload List");

	// The okay button;
	mOkButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "MapSaveWindow_mOkButton");
	mOkButton->setText("Ok");

	// The Cancel button;
	mCancelButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "MapSaveWindow_mCancelButton");
	mCancelButton->setText("Cancel");


	/**********************************/
	/*** Set the size and positions ***/
	/**********************************/

	float mListbox_x_pos = 0.1f;
	float mListbox_y_pos = 0.06f;
	float mListbox_x_size = 0.8f;
	float mListbox_y_size = 0.63f;

	float mFileNameLabel_y_size = 0.04f;
	float editbox_y_size = 0.04f;
	float button_y_size = 0.04f;
	float button_x_size = 0.35f;


	float mReloadButton_y_pos = mListbox_y_pos + mListbox_y_size + 0.03f;
	float mFileNameLabel_y_pos = mReloadButton_y_pos + button_y_size + 0.025f;
	float mNameEditbox_y_pos =  mFileNameLabel_y_pos + mFileNameLabel_y_size;
	float mOkButton_y_pos =  mNameEditbox_y_pos + editbox_y_size + 0.03f + 0.01;

	float mCancelButton_x_pos = mListbox_x_pos + button_x_size + 0.05f + 0.05f;



	mListbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mListbox_y_pos, 0.0f))); 
	mListbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(mListbox_y_size, 0.0f))); 

	// We want the X position to be auto centered and the Y position to be custom;
	mReloadButton->setYPosition( UDim(mReloadButton_y_pos, 0.0f) ); 
	mReloadButton->setSize(UVector2(UDim(button_x_size, 0.0f), UDim(button_y_size, 0.0f))); 

	mFileNameLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(mListbox_x_pos, 0.0F), CEGUI::UDim(mFileNameLabel_y_pos, 0.0F) ));
	mFileNameLabel->setSize(CEGUI::UVector2(CEGUI::UDim(mListbox_x_size, 0.0F), CEGUI::UDim(mFileNameLabel_y_size, 0.0F)));

	mNameEditbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mNameEditbox_y_pos, 0.0f))); 
	mNameEditbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(editbox_y_size, 0.0f))); 

	mOkButton->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mOkButton_y_pos, 0.0f))); 
	mOkButton->setSize(UVector2(UDim(button_x_size, 0.0f), UDim(button_y_size, 0.0f))); 

	mCancelButton->setPosition(UVector2(UDim(mCancelButton_x_pos, 0.0f), UDim(mOkButton_y_pos, 0.0f))); 
	mCancelButton->setSize(UVector2(UDim(button_x_size, 0.0f), UDim(button_y_size, 0.0f))); 



	/*************************/
	/*** Handle the events ***/
	/*************************/


	// Handle the listbox selection change;
	mListbox->subscribeEvent( CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber( 
		&MapSaveWindow::event_listBoxTextToEditBox, this ) );

	// Handle the listbox reload with the reload button;
	mReloadButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&MapSaveWindow::event_reloadListbox, this ) );

	mNameEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber( 
			&MapSaveWindow::event_editBox_activated, this ) );

	mNameEditbox->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber( 
			&MapSaveWindow::event_editBox_deactivated, this ) );

	mOkButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&MapSaveWindow::event_mOkButton_click, this ) );

	mCancelButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&MapSaveWindow::event_mCancelButton_click, this ) );


	/************************/
	/*** Set the Settings ***/
	/************************/

	// We want main window to be auto aligned vertically and horiz;
	mParentwin->setHorizontalAlignment(CEGUI::HA_CENTRE);
	mParentwin->setVerticalAlignment(CEGUI::VA_CENTRE);

	// We want the X position to be auto centered and the Y position to be custom;
	mReloadButton->setHorizontalAlignment(CEGUI::HA_CENTRE);


	// Only allow this window to be accessed until the ok button is clicked;
	mParentwin->setModalState(true);

	// Turn on listbox Sorting;
	mListbox->setSortingEnabled(true);

	// Load the listbox on startup;
	_loadMapListBox();

	/************************************************/
	/*** Attach the windows to the parent window ***/
	/************************************************/
	mMainChildwin->addChildWindow( mListbox );
	mMainChildwin->addChildWindow( mFileNameLabel );	
	mMainChildwin->addChildWindow( mNameEditbox );
	mMainChildwin->addChildWindow( mReloadButton );
	mMainChildwin->addChildWindow( mOkButton );
	mMainChildwin->addChildWindow( mCancelButton );

	mParentwin->addChildWindow( mMainChildwin );
	sheet->addChildWindow( mParentwin );

	return true;
}


bool MapSaveWindow::_loadMapListBox()
{
	AccessFileSystem fileSystem;
	string vFilePath = FILEPATH_OF_MAPS;
	string vFileFilter = ".map";

	vector<string> vFileNames;
	CEGUI::ListboxTextItem* item = NULL;



	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(vFilePath) )
	{
		string title = "ERROR";
		string functionName = "bool MapSaveWindow::_loadMapListBox()";
		string errorMsg = "Could not find directory.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}


		return false;
	}


	/*** We load the listbox with all the files in this directory that have the extension ".map" ***/
	fileSystem.getFilesWithExtension(vFilePath, &vFileNames, &vFileFilter);


	// Create the text item and set its text;
	for (unsigned int i = 0; i < vFileNames.size(); i++)
	{
		item = new CEGUI::ListboxTextItem( vFileNames.at(i) );			// Create the textitem;
		item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );	// Set the brush;
		mListbox->addItem( item );						// Add the item;
	}


	return true;
}


// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool MapSaveWindow::event_editBox_activated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mNameEditbox->isActive() )
	{
		// We set the GUI windows to not busy;
		guiManager->setEnabledCommandKeys(true);
	}
	else
	{
		// We set the GUI windows to busy;
		guiManager->setEnabledCommandKeys(false);
	}

	return true;	
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool MapSaveWindow::event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mNameEditbox->isActive() )
	{
		// We set the GUI windows to not busy;
		guiManager->setEnabledCommandKeys(true);
	}
	else
	{
		// We set the GUI windows to busy;
		guiManager->setEnabledCommandKeys(false);
	}


	return true;	
}

bool MapSaveWindow::event_listBoxTextToEditBox(const CEGUI::EventArgs& pEventArgs)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MapSaveWindow::event_listBoxTextToEditBox(...)";
	string strLogMessage;

	CEGUI::ListboxItem* item;
	
	string str;
	string tmp;
	unsigned int size = 0;


	/*** Get the text from the selected listbox item ***/
	try
	{	
		item = mListbox->getFirstSelectedItem();
	}
	catch(...)
	{
		strLogMessage = "Error in: mListbox->getFirstSelectedItem()";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);


		return false;
	}

	// If the item isn't NULL, than there is something selected.
	if ( item != NULL )
	{
		/*** Put the selected text into a string and Remove the ".map" part from the string. ***/ 
		str = item->getText().c_str();
		size = str.size();
	
		// We check if the string has ".map". By checking if we have more than 4 characters, we do just that;
		if (size > 4)
		{
			str.erase(size - 4, 4);

			// Put the string into the editbox;
			mNameEditbox->setText( str );

			return true;
		}
		else
		{
			return false;
		}
	}


	return false;
}

bool MapSaveWindow::event_reloadListbox(const CEGUI::EventArgs& pEventArgs)
{
	// First Clear the listbox;
	mListbox->resetList();

	// Now load the listbox;
	return _loadMapListBox();
}





bool MapSaveWindow::event_mOkButton_click(const CEGUI::EventArgs& pEventArgs)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	EditManager* const editMgr = EditManager::getSingletonPtr();
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();

	Ogre::SceneManager* const sceneMgr = ogre3dManager->getSceneManager();
	std::vector<Ogre::SceneNode*> objectList;
	bool check = true;

	// Get the text from the editbox;
	string fileName = mNameEditbox->getText().c_str();



	// If the file name editbox is empty than give a message box error and return;
	if ( fileName == "" )
	{
		string title = "ERROR";
		string functionName = "void MapSaveWindow::event_mOkButton_click(...)";
		string errorMsg = "The filename editbox is empty.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}

		
		return false;
	}


	// Get the sceneNodes that we want to get and ignore what should be ignored;
	_getSceneNodesWithException( sceneMgr->getRootSceneNode(), &objectList );

	
	// Attach the ".map" extension to the filename. This way the user doesn't have to type it every time;
	fileName += ".map";	

	// Save the map file and check for error;
	check = editMgr->saveMap(FILEPATH_OF_MAPS, fileName, &objectList);

	if ( !check )
	{
		string title = "ERROR";
		string functionName = "void MapSaveWindow::event_mOkButton_click(...)";
		string errorMsg = "An error occured while saving the map file. Possibly invalid file path.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}

		
		return false;
	}
	else
	{
		/*** Allow the other windows to be accessed again ***/
		mParentwin->setModalState(false);	// Allow the other windows to be accessed again;
	
		/*** Enable the command keys ***/
		guiManager->setEnabledCommandKeys(true);

		/*** Inform the GUI that the window was closed on success ***/
		gui->event_SaveMapWindowClose(true);
	}


	return true;
}

bool MapSaveWindow::event_mCancelButton_click(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();


	/*** Allow the other windows to be accessed again ***/
	mParentwin->setModalState(false);	// Allow the other windows to be accessed again;

	/*** Enable the command keys ***/
	guiManager->setEnabledCommandKeys(true);

	/*** Inform the GUI that the window was closed and that the cancel button was hit ***/
	gui->event_SaveMapWindowClose(false);


	return true;
}

void MapSaveWindow::_getSceneNodesWithException(Ogre::SceneNode *n, vector<Ogre::SceneNode*> *include)
{
	string tmp;

	tmp = n->getName();


	// We use "sn_" in the beginning of our scenenodes to determine which ones we want to save;	
	if ( tmp.compare(0,3,"sn_") == 0 )
	{
		include->push_back(n);	
	}
	
	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	while(object_it.hasMoreElements())
	{
		object_it.getNext();
	}

	while(node_it.hasMoreElements())
	{
		_getSceneNodesWithException( (Ogre::SceneNode*) node_it.getNext(), include );
	}
}






HeapMapSaveWindow::HeapMapSaveWindow() : mMapSaveWindow(NULL)
{

}

HeapMapSaveWindow::~HeapMapSaveWindow()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapMapSaveWindow::~HeapMapSaveWindow()";
	string strLogMessage;


	if ( mMapSaveWindow )	
	{ 
		delete mMapSaveWindow;
		mMapSaveWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted mMapSaveWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

CEGUI::Window* const HeapMapSaveWindow::getWindow()
{
	if ( mMapSaveWindow )
	{
		return mMapSaveWindow->getWindow();
	}
	
	return NULL;
}

void HeapMapSaveWindow::toggle()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapMapSaveWindow::toggle()";
	string strLogMessage;


	/*** If visible, then hide and delete the window and object. Else we create the object ***/
	if ( mMapSaveWindow )	
	{
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		delete mMapSaveWindow;
		mMapSaveWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted mMapSaveWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mMapSaveWindow = new MapSaveWindow;
		
		if ( mMapSaveWindow == NULL )
		{ 
			strLogMessage = "mMapSaveWindow";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mMapSaveWindow->addChildWindow();

		strLogMessage = "created mMapSaveWindow";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

bool HeapMapSaveWindow::isVisible() const
{
	if (mMapSaveWindow)
	{
		return mMapSaveWindow->getWindow()->isVisible();
	}

	return false;
}
