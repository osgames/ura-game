/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ModelMenuImportDotSceneTab.h"
#include "WorldEditorProperties.h"
#include "CustomSceneNodeManager.h"
#include "CustomSceneNodeManipulator.h"

using namespace CEGUI;
using namespace Ogre;
using namespace std;


ModelMenuImportDotSceneTab::ModelMenuImportDotSceneTab(EditManager *editMgr) : mEditManager(editMgr)
{
	mGuiMessageBox = new GUIMessageBox;
}

ModelMenuImportDotSceneTab::~ModelMenuImportDotSceneTab()
{
	if (mGuiMessageBox)
	{
		delete mGuiMessageBox;
		mGuiMessageBox = 0;
	}
}

CEGUI::Window* ModelMenuImportDotSceneTab::getWindow()
{
	return mParentwin;
}

bool ModelMenuImportDotSceneTab::addChildWindow()
{
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	
	// Create the GuiMessageBox and add it to the sheet;
	if ( mGuiMessageBox )
	{
		mGuiMessageBox->addWindow();
	}
	else
	{
		cout << "ERROR: bool ModelMenuImportDotSceneTab::addChildWindow(): mGuiMessageBox could not allocate heap memory";
		cout << endl;
		return false;
	}


	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelMenuImportDotSceneTab_mParentwin");
	mParentwin->setText("Import");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 


	/**********************************************************/
	/*** Create the mListbox to select what files to import ***/
	/**********************************************************/

	// the listbox;
	mListbox = (CEGUI::Listbox *) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelMenuImportDotSceneTab_mMeshListbox");


	/********************************/
	/*** Create the reload button ***/
	/*********************************/

	// The reload button;
	mReloadButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "ModelMenuImportDotSceneTab_mReloadButton");
	mReloadButton->setText("Reload List");


	/****************************/
	/*** Create the ok button ***/
	/****************************/

	// The okay button
	mOkButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "ModelMenuImportDotSceneTab_mOkButton");
	mOkButton->setText("Ok");


	/**********************************/
	/*** Set the size and positions ***/
	/**********************************/

	float mListbox_x_pos = 0.1f;
	float mListbox_y_pos = 0.03f;
	float mListbox_x_size = 0.8f;
	float mListbox_y_size = 0.8f;

	float button_y_size = 0.04f;


	float mReloadButton_y_pos = mListbox_y_pos + mListbox_y_size + 0.03f;
	float mOkButton_y_pos =  mReloadButton_y_pos + button_y_size + 0.03f;


	mListbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mListbox_y_pos, 0.0f))); 
	mListbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(mListbox_y_size, 0.0f))); 

	mReloadButton->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mReloadButton_y_pos, 0.0f))); 
	mReloadButton->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(button_y_size, 0.0f))); 

	mOkButton->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mOkButton_y_pos, 0.0f))); 
	mOkButton->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(button_y_size, 0.0f))); 


	/*************************/
	/*** Handle the events ***/
	/*************************/

	// Handle the listbox reload with the reload button;
	mReloadButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&ModelMenuImportDotSceneTab::event_reloadListbox, this ) );


	// Handle the Ok button;
	mOkButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&ModelMenuImportDotSceneTab::event_loadDotSceneFile, this ) );


	/************************/
	/*** Set the Settigns ***/
	/************************/

	// Turn on listbox Sorting;
	mListbox->setSortingEnabled(true);

	// Load the listbox on startup;
	_loadDotSceneListBox();

	/************************************************/
	/*** Attach the windows to the parent window ***/
	/************************************************/
	mParentwin->addChildWindow( mListbox );	
	mParentwin->addChildWindow( mReloadButton );	
	mParentwin->addChildWindow( mOkButton );


	return true;
}


bool ModelMenuImportDotSceneTab::_loadDotSceneListBox()
{
	AccessFileSystem fileSystem;
	string vFilePath = FILEPATH_OF_MODELS;
	string vFileFilter = ".xml";

	vector<string> vFileNames;
	CEGUI::ListboxTextItem* item = NULL;


	
	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(vFilePath) )
	{
		string title = "ERROR";
		string functionName = "ModelMenuImportDotSceneTab::loadListBox()";
		string errorMsg = "Could not find model directory.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}
		else
		{
			cout << "ERROR: bool ModelMenuImportDotSceneTab::_loadDotSceneListBox(): mGuiMessageBox could not allocate heap memory";
			cout << endl;
			return false;
		}

		return false;
	}


	/*** We load the listbox with all the files in this directory that have the extension ".xml" ***/
	fileSystem.getFullPathRecursiveFilesWithExtension(vFilePath, &vFileNames, &vFileFilter);


	// Create the text item and set its text;
	for (unsigned int i = 0; i < vFileNames.size(); i++)
	{
		item = new CEGUI::ListboxTextItem( vFileNames.at(i) );			// Create the textitem;
		item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );	// Set the brush;
		mListbox->addItem( item );						// Add the item;
	}

	return true;
}



bool ModelMenuImportDotSceneTab::event_reloadListbox(const CEGUI::EventArgs& pEventArgs)
{
	// First Clear the listbox;
	mListbox->resetList();

	// Now load the listbox;
	return _loadDotSceneListBox();
}

bool ModelMenuImportDotSceneTab::event_loadDotSceneFile(const CEGUI::EventArgs& pEventArgs)
{
	string fileName;
	CEGUI::ListboxItem* item;
	std::vector<Ogre::SceneNode*> objectList;

	CustomSceneNodeManager* const customSceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = customSceneNodeManager->getCustomSceneNodeManipulator();


	try
	{	
		item = mListbox->getFirstSelectedItem();
	}
	catch(...)
	{
		cout << "ModelMenuImportDotSceneTab::event_loadDotSceneFile(): Error in: " << 
			"mListbox->getListboxItemFromIndex()." << endl;

		return false;
	}

	// If the item returns NULL than there was nothing selected and we return;
	if ( item == NULL )
	{
		return false;
	}


	// Get the selected listbox item string;
	fileName = item->getText().c_str();


	/*** Import the dotscene File ***/
	try
	{
		mEditManager->importDotScene(&objectList, fileName);
	}
	catch(...) 
	{
		string title = "ERROR";
		string functionName = "ModelMenuImportDotSceneTab::event_loadDotSceneFile()";
		string errorMsg = "Could not import DotScene file. Possibly models or sceneNodes with the same name exist.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}
		else
		{
			cout << "ERROR: bool ModelMenuImportDotSceneTab::event_loadDotSceneFile(const CEGUI::EventArgs& pEventArgs): "
				<< "mGuiMessageBox could not allocate heap memory" << endl;
			return false;
		}

		// We select the imported that made it through and delete them;
		customSceneNodeManipulator->select(&objectList);
		customSceneNodeManipulator->deleteSelected();


		return false;
	}


	/*** Select the imported and turn the boundingbox on ***/
	customSceneNodeManipulator->select(&objectList);


	return true;
}


