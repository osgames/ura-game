/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRAINMENU_SPLATTAB_H
#define TERRAINMENU_SPLATTAB_H


#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

#include "EditManager.h"
#include "CustomScrollPane.h"
#include "AccessFileSystem.h"
#include "TerrainMenuSplatTabARGBWin.h"
#include "HeapTerrainMenuSplatTabScalesWin.h"
#include "HeapTerrainMenuSplatTabFileDialog.h"

#define TERRAINMENUSPLATTAB_UNIQUE_BRUSHNAME_START "TerrainMenuSplatTab_UniqueBrushName_"
#define TERRAINMENUSPLATTAB_UNIQUE_TEXTURENAME_START "TerrainMenuSplatTab_UniqueTextureName_"



class TerrainMenuSplatTab
{
protected:
	CustomScrollPane* mScrollPane;
	CustomScrollPane* mTextureScrollPane;

	TerrainMenuSplatTabARGBWin* mTerrainMenuSplatTabARGBWin;
	HeapTerrainMenuSplatTabScalesWin* mHeapTerrainMenuSplatTabScalesWin;
	HeapTerrainMenuSplatTabFileDialog* mHeapTerrainMenuSplatTabFileDialog;

	CEGUI::Window* mParentwin;

	/*** The brush scrollpane variables ***/
	std::vector<std::string> mVbrushNames;
	std::vector <CEGUI::Window*> mVbrushScrollPaneWindows;

	/*** The texture scrollpane variables ***/
	std::vector<std::string> mVsampleTextureNames;
	std::vector<std::string> mVterrainTextureNames;
	std::vector <CEGUI::Window*> mVtextureScrollPaneWindows;
	std::vector <CEGUI::Window*> mVtextureScrollpaneImage;

	/*** The static text variables ***/
	Ogre::Real mMaxScale;
	Ogre::Real mBrush_x_scale;
	Ogre::Real mBrush_y_scale;
    static const Ogre::Real X_SCALE_TO_BRUSH;
    static const Ogre::Real Y_SCALE_TO_BRUSH;
	float mCameraDegree;
	unsigned long mAutoRepeatRate;
	

	/*** The Windows to add ***/
	CEGUI::Window* mBrush_x_scale_Text;
	CEGUI::Scrollbar* mBrush_x_scale_Scrollbar;

	CEGUI::Window* mBrush_y_scale_Text;
	CEGUI::Scrollbar* mBrush_y_scale_Scrollbar;

	CEGUI::Window* mFrameBelow_brushYScale_Scrollbar;

	CEGUI::Window* mCamDegreeLabel;
	CEGUI::Scrollbar* mCamDegree_Scrollbar;

	CEGUI::Window* mAutoRepeat_Text;
	CEGUI::Scrollbar* mAutoRepeat_Scrollbar;

	CEGUI::PushButton* mButtonARGB;
	CEGUI::PushButton* mButtonSplatScale;

	CEGUI::Checkbox* mirrorX_checkbox;
	CEGUI::Checkbox* mirrorY_checkbox;

	void _setMouseRate();
	void _reAdjustScrollbars();
	void _brushChange();
	/*** The events ***/
	bool event_windowVisible(const CEGUI::EventArgs &pEventArgs);
	bool event_windowHidden(const CEGUI::EventArgs &pEventArgs);	
	bool event_parent_resize(const CEGUI::EventArgs &pEventArgs);
	bool event_brushChange(const CEGUI::EventArgs &pEventArgs);
	bool event_TextureChange(const CEGUI::EventArgs &pEventArgs);
	bool event_mHeapTerrainMenuSplatTabFileDialog_Show(const CEGUI::EventArgs &pEventArgs);
	bool event_mBrush_x_scale_Scrollbar_change(const CEGUI::EventArgs &pEventArgs);
	bool event_mBrush_y_scale_Scrollbar_change(const CEGUI::EventArgs &pEventArgs);
	bool event_mCamDegree_Scrollbar_Scrollbar_change(const CEGUI::EventArgs &pEventArgs);
	bool event_mAutoRepeatRate_Scrollbar_change(const CEGUI::EventArgs &pEventArgs);

	bool event_mButtonARGB_click(const CEGUI::EventArgs &pEventArgs);
	bool event_terrainMenuSplatTabScalesWin_click(const CEGUI::EventArgs &pEventArgs);
public:
	TerrainMenuSplatTab();
	~TerrainMenuSplatTab();

	bool isVisible();
	CEGUI::Window* getWindow();
	bool addChildWindow();

	void _loadBrushesAndTextures();

	void updateWindowShown();
	void updateWindowHidden();

	void splatTerrain();

	std::vector<std::string>* const getSampleTextures()
	{
		return &mVsampleTextureNames;
	}

	unsigned long getBrushCount();
	unsigned long getSelectedBrush();
	void setBrush(const unsigned long number);
	void updateTerrainBrush();

	unsigned long getTextureCount();
	unsigned long getSelectedTexture();
	void setSelectedTexture(const unsigned long number);
	void setSampleTexture(const unsigned long number, std::string imageName);
}; 


#endif

 
