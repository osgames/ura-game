/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GuiKeyAutoRepeat.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace std;
using namespace OIS;



GuiKeyAutoRepeat::GuiKeyAutoRepeat()
{

}

GuiKeyAutoRepeat::~GuiKeyAutoRepeat()
{

}


bool GuiKeyAutoRepeat::hasKey(unsigned int key)
{
	map<unsigned int, CustomTimerType*>::iterator it = mMap.find(key);
	

	if ( it != mMap.end() )
	{
		CustomTimerType* tmpTimerType = it->second;


		if ( tmpTimerType )
		{
			if ( !tmpTimerType->isCalledAlready() )
			{
				// Make it known that this key was already called;
				tmpTimerType->setCalledAlready(true);

				// Reset the timer;
				tmpTimerType->resetTimer();

				return true;
			} 
			else
			{
				if ( tmpTimerType->getMilliseconds() > 150 )
				{
					// Reset the timer;
					tmpTimerType->resetTimer();

					return true;
				}
			}
		}
		else
		{
			Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
			Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
			const string strLogFunctionName = "bool GuiKeyAutoRepeat::hasKey(...)";
			string strLogMessage;

			strLogMessage = "Could not find 'tmpTimerType'";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}
	}


	return false;
}

void GuiKeyAutoRepeat::addKey(unsigned int key)
{
	map<unsigned int, CustomTimerType*>::iterator it = mMap.find(key);


	// If the key isn't already in the map than add it;
	if ( it == mMap.end() )
	{
		// Add a new timer;
		mTimerType = new CustomTimerType;

		// Only insert a pair if There was no mTimerType allocation error;
		if (mTimerType)
		{
			// Insert the key and a pointer to the mTimerType;
			mMap.insert( pair<unsigned int, CustomTimerType*>(key, mTimerType) );
		}
	}
}

void GuiKeyAutoRepeat::removeKey(unsigned int key)
{
	map<unsigned int, CustomTimerType*>::iterator it = mMap.find(key);


	if ( it != mMap.end() )
	{
		CustomTimerType* tmpTimerType = it->second;


		// Delete the timer if it exists;
		if ( tmpTimerType )
		{
			delete tmpTimerType;
			tmpTimerType = NULL;
		}
		else
		{
			Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
			Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
			const string strLogFunctionName = "bool GuiKeyAutoRepeat::removeKey(...)";
			string strLogMessage;

			strLogMessage = "Could not delete 'tmpTimerType'";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}


		// Erase the key;
		mMap.erase(it);
	}
	else
	{
		Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
		Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
		const string strLogFunctionName = "bool GuiKeyAutoRepeat::removeKey(...)";
		string strLogMessage;

		strLogMessage = "Could not find std::map key " + it->first;
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
	}
}


