/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TerrainMenuSplatTab.h"
#include "WorldEditorProperties.h"
#include "CameraManager.h"
#include "SplatManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace CEGUI;
using namespace std;

const Ogre::Real TerrainMenuSplatTab::X_SCALE_TO_BRUSH = 320;
const Ogre::Real TerrainMenuSplatTab::Y_SCALE_TO_BRUSH = 320;

TerrainMenuSplatTab::TerrainMenuSplatTab() : 
						mHeapTerrainMenuSplatTabFileDialog(NULL),
						mHeapTerrainMenuSplatTabScalesWin(NULL)
{
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	// The scrollbar starts at 0 so we have to add +1;
	mMaxScale = 101;

	mBrush_x_scale = 10;
	mBrush_y_scale = 10;

	mCameraDegree =  cameraManager->getCameraSceneNodeMoveDegree();

	// This variable can goto 0 in the scrollbar;
	mAutoRepeatRate = 200;


	/*** Allocate heap memory for the windows ***/
	mScrollPane = new CustomScrollPane;
	mTextureScrollPane = new CustomScrollPane;
	mTerrainMenuSplatTabARGBWin = new TerrainMenuSplatTabARGBWin;
}

TerrainMenuSplatTab::~TerrainMenuSplatTab()
{
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();
	string imageSetName;

	/*** Free the heap memory ***/
	if ( mScrollPane )
	{
		delete mScrollPane;
		mScrollPane = NULL;
	}

	if ( mTextureScrollPane )
	{
		delete mTextureScrollPane;
		mTextureScrollPane = NULL;
	}

	if ( mTerrainMenuSplatTabARGBWin )
	{
		delete mTerrainMenuSplatTabARGBWin;
		mTerrainMenuSplatTabARGBWin = NULL;
	}

	if ( mHeapTerrainMenuSplatTabScalesWin )
	{
		delete mHeapTerrainMenuSplatTabScalesWin;
		mHeapTerrainMenuSplatTabScalesWin = NULL;
	}

	if (mHeapTerrainMenuSplatTabFileDialog)
	{
		delete mHeapTerrainMenuSplatTabFileDialog;
		mHeapTerrainMenuSplatTabFileDialog = NULL;
	}



	/*** Destroy the windows ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);



	/*** Destroy the imagesets ***/

	// Destroy the brush imageSets;
	for (unsigned long i = 0; i < mVbrushNames.size(); i++)
	{	
		imageSetName = TERRAINMENUSPLATTAB_UNIQUE_BRUSHNAME_START + mVbrushNames.at(i);

		if ( imageSetManager.isImagesetPresent(imageSetName) )
		{
			imageSetManager.destroyImageset(imageSetName);
		}
	}


	// Destroy the sample textures;
	for (unsigned long i = 0; i < mVsampleTextureNames.size(); i++)
	{	
		imageSetName = TERRAINMENUSPLATTAB_UNIQUE_TEXTURENAME_START + mVsampleTextureNames.at(i);

		if ( imageSetManager.isImagesetPresent(imageSetName) )
		{
			imageSetManager.destroyImageset(imageSetName);
		}
	}
}

CEGUI::Window* TerrainMenuSplatTab::getWindow()
{
	return mParentwin;
}

bool TerrainMenuSplatTab::addChildWindow()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strLogFunctionName = "bool TerrainMenuSplatTab::addChildWindow()";
	string strLogMessage;

	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();
	
	CEGUI::PropertyHelper phelper;
	AccessFileSystem fileSystem;

	CEGUI::Window* brushes_label;	
	CEGUI::Window* scrollPaneWindow;
	CEGUI::Window* buttonReloadBrush;
	CEGUI::Window* frameBelowRefreshButton;
	CEGUI::Window* textures_label;
	CEGUI::Window* scrollPaneTextureWindow;
	CEGUI::Window* frameBelowDecreaseScrollbar;
	CEGUI::Window* frameBelow_AutoRepeat_Scrollbar;

	/*** Set the scrollbar variables ***/
	string strScrollpaneName = "TerrainMenuSplatTab_XXuniqueScrollpaneXX.";
	string strTextureScrollpaneName = "TerrainMenuSplatTextureTab_XXuniqueScrollpaneXX.";
	string strScrollImageSet = "basLook";
	string strScrollImageName = "bas_Color_blue";
	
	const long childWindow_x_pos = 9; 
	const long childWindow_x_rightSpace = 24;
	const long childWindow_y_pos = 25;
	const long childWindow_y_size = 50; 
	const long childWindow_y_space = 5;

	const long highligther_y_topSpace = -2; 
	const long highligther_y_bottomSpace = -15;

	string uniqueBrushStartName = TERRAINMENUSPLATTAB_UNIQUE_BRUSHNAME_START;
	string uniqueTextureStartName = TERRAINMENUSPLATTAB_UNIQUE_TEXTURENAME_START;
	string brushPath = FILEPATH_OF_SPLAT_BRUSH;
	string sampleTexturePath = FILEPATH_OF_SPLAT_SAMPLEBRUSH;
	string brushFileFilter = ".png";
	string textureFileFilter = ".png";

	vector<string> vTmpTextureFileNames;


	/************************/
	/*** Load the brushes ***/
	/************************/

	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(brushPath) )
	{
		strLogMessage = "Could not find brush directory";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}

	fileSystem.getRecursiveFilesWithExtension(brushPath, &mVbrushNames, &brushFileFilter);

	for (unsigned long i = 0; i < mVbrushNames.size(); i++)
	{
		try
		{
			if ( imageSetManager.isImagesetPresent(uniqueBrushStartName + mVbrushNames[i]) == false )
			{
				imageSetManager.createImagesetFromImageFile(uniqueBrushStartName + mVbrushNames[i], mVbrushNames[i]);

				strLogMessage = "Successfully Loaded brush: " + mVbrushNames[i];
				ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
			}
		}
		catch(...)
		{
			strLogMessage = "Could not load brushes";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}

	}


	/*******************************/
	/*** Load the Splat Textures ***/
	/*******************************/

	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(sampleTexturePath) )
	{
		strLogMessage = "Could not find sampleTexturePath directory";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}

	// Get the file names recursively;
	fileSystem.getRecursiveFilesWithExtension(sampleTexturePath, &vTmpTextureFileNames, &textureFileFilter);


	// We only can use 4 textures, so just get the first 4;
	for (unsigned long i = 0; i < 4; i++)
	{
		mVsampleTextureNames.push_back( vTmpTextureFileNames.at(i) );
	}
	

	// Create the imageSets for the textures;
	for (unsigned long i = 0; i < mVsampleTextureNames.size(); i++)
	{
		try
		{
			if ( imageSetManager.isImagesetPresent(uniqueTextureStartName + mVsampleTextureNames[i]) == false )
			{
				imageSetManager.createImagesetFromImageFile(
					uniqueTextureStartName + mVsampleTextureNames[i], mVsampleTextureNames[i]);

				strLogMessage = "Successfully Loaded textures: " + mVsampleTextureNames[i];
				ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
			}
		}
		catch(...)
		{
			strLogMessage = "Could not load textures";
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}

	}


	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","TerrainMenuSplatTab_Main_mParentwin");
	mParentwin->setText("Splat");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 

	mParentwin->subscribeEvent( CEGUI::Window::EventSized, CEGUI::Event::Subscriber(
		&TerrainMenuSplatTab::event_parent_resize, this ) );

	mParentwin->subscribeEvent( CEGUI::Window::EventShown, CEGUI::Event::Subscriber(
		&TerrainMenuSplatTab::event_windowVisible, this ) );

	mParentwin->subscribeEvent( CEGUI::Window::EventHidden, CEGUI::Event::Subscriber(
		&TerrainMenuSplatTab::event_windowHidden, this ) );


	/********************************************/
	/*** create the brushes_label staticText  ***/
	/********************************************/
	// Create the static text
	brushes_label = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTab_brushes_label");
	brushes_label->setText("Brushes:");
	brushes_label->setProperty("FrameEnabled", "False");
	brushes_label->setProperty("BackgroundEnabled", "false");

	brushes_label->setProperty("HorzFormatting", "HorzCentred"); 

	// Disable the window;
	brushes_label->disable();

	/******************************************************/
	/*** Create a child window to add the scrollpane to ***/
	/******************************************************/
	scrollPaneWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", "TerrainMenuSplatTab_Main_scrollPaneWindow");
	scrollPaneWindow->setProperty( "Image", "set:TaharezLook image:colorWhite" );
	

	/*************************************/
	/*** create reload brushes button  ***/
	/*************************************/
	buttonReloadBrush = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button", "TerrainMenuSplatTab_Main_buttonReloadBrush");
	buttonReloadBrush->setText("Reload Brushes");


	/************************************************************/
	/***  Create the wood frame beneath the buttonReloadBrush ***/
	/************************************************************/
	frameBelowRefreshButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", "TerrainMenuSplatTab_Main_frameBelowRefreshButton");
	frameBelowRefreshButton->setProperty("FrameEnabled", "False");
	frameBelowRefreshButton->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************/
	/*** create the textures_label staticText  ***/
	/********************************************/
	// Create the static text
	textures_label = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTab_textures_label");
	textures_label->setText("Textures:");
	textures_label->setProperty("FrameEnabled", "False");
	textures_label->setProperty("BackgroundEnabled", "false");

	textures_label->setProperty("HorzFormatting", "HorzCentred"); 

	// Disable the window;
	textures_label->disable();


	/******************************************************/
	/*** Create a child window to add the scrollpane to ***/
	/******************************************************/
	scrollPaneTextureWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", "TerrainMenuSplatTab_Main_scrollPaneTextureWindow");
	scrollPaneTextureWindow->setProperty( "Image", "set:TaharezLook image:colorWhite" );


	/*************************************************************/
	/***  Create the wood frame beneath the Decrease Scrollbar ***/
	/*************************************************************/
	frameBelowDecreaseScrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", "TerrainMenuSplatTab_Main_frameBelowDecreaseScrollbar");
	frameBelowDecreaseScrollbar->setProperty("FrameEnabled", "False");
	frameBelowDecreaseScrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );
	

	/**********************************************************/
	/*** Create the Brush x scale scrollbar and static text ***/
	/**********************************************************/
	mBrush_x_scale_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTab_Main_mBrush_x_scale_Scrollbar");
	mBrush_x_scale_Scrollbar->setDocumentSize(mMaxScale);
	mBrush_x_scale_Scrollbar->setScrollPosition(mBrush_x_scale);
	mBrush_x_scale_Scrollbar->setPageSize(1);
	mBrush_x_scale_Scrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mBrush_x_scale_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTab::event_mBrush_x_scale_Scrollbar_change, this));

	
	// Create the static text
	mBrush_x_scale_Text = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTab_Main_mBrush_x_scale_Text");
	mBrush_x_scale_Text->setProperty("FrameEnabled", "False");
	mBrush_x_scale_Text->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mBrush_x_scale_Text->disable();
	// Center the text;
	mBrush_x_scale_Text->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mBrush_x_scale_Text->setProperty("VertFormatting", "TopAligned");
	mBrush_x_scale_Text->setText( "Brush Scale X: " + phelper.uintToString( (unsigned int) mBrush_x_scale) );


	/**********************************************************/
	/*** Create the Brush y scale scrollbar and static text ***/
	/**********************************************************/
	mBrush_y_scale_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTab_Main_mBrush_y_scale_Scrollbar");
	mBrush_y_scale_Scrollbar->setDocumentSize(mMaxScale);
	mBrush_y_scale_Scrollbar->setScrollPosition(mBrush_y_scale);
	mBrush_y_scale_Scrollbar->setPageSize(1);
	mBrush_y_scale_Scrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mBrush_y_scale_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTab::event_mBrush_y_scale_Scrollbar_change, this));

	
	// Create the static text
	mBrush_y_scale_Text = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTab_Main_mBrush_y_scale_Text");
	mBrush_y_scale_Text->setProperty("FrameEnabled", "False");
	mBrush_y_scale_Text->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mBrush_y_scale_Text->disable();
	// Center the text;
	mBrush_y_scale_Text->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mBrush_y_scale_Text->setProperty("VertFormatting", "TopAligned");
	mBrush_y_scale_Text->setText( "Brush Scale Y: " + phelper.uintToString( (unsigned int) mBrush_y_scale ) );


	/****************************************************************/
	/***  Create the wood frame beneath the brushYScale_Scrollbar ***/
	/****************************************************************/
	mFrameBelow_brushYScale_Scrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuSplatTab_Main_mFrameBelow_brushYScale_Scrollbar");
	mFrameBelow_brushYScale_Scrollbar->setProperty("FrameEnabled", "False");
	mFrameBelow_brushYScale_Scrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************************************************/
	/*** Create the mCamDegree_Scrollbar scrollbar and mCamDegreeLabel text label ***/
	/********************************************************************************/
	mCamDegree_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTab_mCamDegree_Scrollbar");
	mCamDegree_Scrollbar->setDocumentSize(201);
	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);
	mCamDegree_Scrollbar->setPageSize(1);
	mCamDegree_Scrollbar->setStepSize(0.001f);

	// Set the scrollbar event to change the text;
	mCamDegree_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTab::event_mCamDegree_Scrollbar_Scrollbar_change, this));

	
	// Create the static text
	mCamDegreeLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTab_mCamDegreeLabel");
	mCamDegreeLabel->setProperty("FrameEnabled", "False");
	mCamDegreeLabel->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mCamDegreeLabel->disable();
	// Center the text;
	mCamDegreeLabel->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mCamDegreeLabel->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mCamDegreeLabel->setText( "Camera Rate: " + phelper.floatToString(mCameraDegree) );


	/**********************************************************/
	/*** Create the autoRepeat scrollbar and static text ***/
	/**********************************************************/
	mAutoRepeat_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTab_Main_mBrushAutoRepeat_Scrollbar");
	mAutoRepeat_Scrollbar->setDocumentSize(201);
	mAutoRepeat_Scrollbar->setScrollPosition(mAutoRepeatRate);
	mAutoRepeat_Scrollbar->setPageSize(1);
	mAutoRepeat_Scrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mAutoRepeat_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTab::event_mAutoRepeatRate_Scrollbar_change, this));

	
	// Create the static text
	mAutoRepeat_Text = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTab_Main_mBrushAutoRepeat_Text");
	mAutoRepeat_Text->setProperty("FrameEnabled", "False");
	mAutoRepeat_Text->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mAutoRepeat_Text->disable();
	// Center the text;
	mAutoRepeat_Text->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mAutoRepeat_Text->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mAutoRepeat_Text->setText( "Mouse Rate: " + phelper.uintToString( (unsigned int) mAutoRepeatRate ) );


	/***************************************************************/
	/***  Create the wood frame beneath the autoRepeat Scrollbar ***/
	/***************************************************************/
	frameBelow_AutoRepeat_Scrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuSplatTab_Main_frameBelow_AutoRepeat_Scrollbar");
	frameBelow_AutoRepeat_Scrollbar->setProperty("FrameEnabled", "False");
	frameBelow_AutoRepeat_Scrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/*********************************************************/
	/*** Create the windows to add to the Brush scrollpane ***/
	/*********************************************************/
	for ( unsigned long i = 0; i < mVbrushNames.size(); i++ )
	{
		// Add another scrollpanewindow
		mVbrushScrollPaneWindows.push_back(NULL);

		/*** Create the parent window ***/
		//mVbrushScrollPaneWindows.at(i) = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		//	"TerrainMenuSplatTab_Main_scrollpane_window_Parent_image_" + mVbrushNames[i] );
		mVbrushScrollPaneWindows.at(i) = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage");
		mVbrushScrollPaneWindows.at(i)->setProperty("FrameEnabled", "False");
		mVbrushScrollPaneWindows.at(i)->setProperty("BackgroundEnabled", "false");


		/*** Set the mouse down event for the windows, so that we can know when to change the brush ***/
		/*** Note: Make sure to use the buttonup event so that the scrollpane auto selector doesn't get confused ***/
		mVbrushScrollPaneWindows.at(i)->subscribeEvent( CEGUI::Window::EventMouseButtonUp, 
			CEGUI::Event::Subscriber( &TerrainMenuSplatTab::event_brushChange, this ) );


		/*** Create the image ***/
		//CEGUI:: Window* scrollpaneImage = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		//	"TerrainMenuSplatTab_Main_scrollpane_window_child_image_" + mVbrushNames[i]);
		CEGUI:: Window* scrollpaneImage = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage");
		scrollpaneImage->setProperty("FrameEnabled", "False");
		scrollpaneImage->setProperty("BackgroundEnabled", "false");
		scrollpaneImage->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.6F, 0.0F)));
		scrollpaneImage->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F) ));

		/*** Set the brush image ***/
		string strSetImage = "set:" + uniqueBrushStartName + mVbrushNames[i] + " image:full_image";
		scrollpaneImage->setProperty( "Image", strSetImage );


		/*** Create the text window ***/
		//CEGUI:: Window* scrollpaneText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		//	"TerrainMenuSplatTab_Main_scrollpane_window_child_text" + mVbrushNames[i]);
		CEGUI:: Window* scrollpaneText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText");
		scrollpaneText->setProperty("FrameEnabled", "False");
		scrollpaneText->setProperty("BackgroundEnabled", "false");
		scrollpaneText->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.3F, 0.0F)));
		scrollpaneText->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.61F, 0.0F) ));

		// Disable the scrollpaneText;
		scrollpaneText->disable();
		// Set the text color;
		scrollpaneText->setProperty("TextColours","tl:FF0000FF tr:FF0000FF bl:FF0000FF br:FF0000FF");
		// Center the text;
/////////	scrollpaneText->setProperty("HorzFormatting", "HorzCentred"); 
		// Set the text;
		scrollpaneText->setText( mVbrushNames[i] );


		/*** Add the child windows to the parent window ***/
		mVbrushScrollPaneWindows.at(i)->addChildWindow( scrollpaneImage );
		mVbrushScrollPaneWindows.at(i)->addChildWindow( scrollpaneText );
	}

	/***********************************************************/
	/*** Create the windows to add to the Texture scrollpane ***/
	/***********************************************************/
	for ( unsigned long i = 0; i < mVsampleTextureNames.size(); i++ )
	{
		// Add another scrollpanewindow
		mVtextureScrollPaneWindows.push_back(NULL);

		/*** Creat the parent window ***/
		//mVbrushScrollPaneWindows.at(i) = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		//	"TerrainMenuSplatTab_Main_scrollpane_window_Parent_image_" + mVbrushNames[i] );
		mVtextureScrollPaneWindows.at(i) = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage");
		mVtextureScrollPaneWindows.at(i)->setProperty("FrameEnabled", "False");
		mVtextureScrollPaneWindows.at(i)->setProperty("BackgroundEnabled", "false");


		/*** Set the mouse down event for the windows, so that we can know when to change the brush ***/
		/*** Note: Make sure to use the buttonup event so that the scrollpane auto selector doens't get confused ***/
		mVtextureScrollPaneWindows.at(i)->subscribeEvent( CEGUI::Window::EventMouseButtonUp, 
			CEGUI::Event::Subscriber( &TerrainMenuSplatTab::event_TextureChange, this ) );


		/*** Set the double click event for the windows, so that we can dynamically change the textures with a file dialog ***/
		mVtextureScrollPaneWindows.at(i)->subscribeEvent( CEGUI::Window::EventMouseDoubleClick, 
			CEGUI::Event::Subscriber( &TerrainMenuSplatTab::event_mHeapTerrainMenuSplatTabFileDialog_Show, this ) );		


		/*** Create the image ***/
		//CEGUI:: Window* scrollpaneImage = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		//	"TerrainMenuSplatTab_Main_scrollpane_window_child_image_" + mVbrushNames[i]);
		CEGUI:: Window* scrollpaneImage = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage");
		scrollpaneImage->setProperty("FrameEnabled", "False");
		scrollpaneImage->setProperty("BackgroundEnabled", "false");
		scrollpaneImage->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.6F, 0.0F)));
		scrollpaneImage->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.0F, 0.0F) ));

		/*** Set the brush image ***/
		string strSetImage = "set:" + uniqueTextureStartName + mVsampleTextureNames[i] + " image:full_image";
		scrollpaneImage->setProperty( "Image", strSetImage );
		mVtextureScrollpaneImage.push_back(scrollpaneImage);

		/*** Create the text window ***/
		//CEGUI:: Window* scrollpaneText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		//	"TerrainMenuSplatTab_Main_scrollpane_window_child_text" + mVbrushNames[i]);
		CEGUI:: Window* scrollpaneText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText");
		scrollpaneText->setProperty("FrameEnabled", "False");
		scrollpaneText->setProperty("BackgroundEnabled", "false");
		scrollpaneText->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.3F, 0.0F)));
		scrollpaneText->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), CEGUI::UDim(0.61F, 0.0F) ));

		// Disable the scrollpaneText;
		scrollpaneText->disable();
		// Set the text color;
		scrollpaneText->setProperty("TextColours","tl:FF0000FF tr:FF0000FF bl:FF0000FF br:FF0000FF");
		// Center the text;
/////////	scrollpaneText->setProperty("HorzFormatting", "HorzCentred"); 
		// Set the text;
		scrollpaneText->setText( mVsampleTextureNames[i] );


		/*** Add the child windows to the parent window ***/
		mVtextureScrollPaneWindows.at(i)->addChildWindow( scrollpaneImage );
		mVtextureScrollPaneWindows.at(i)->addChildWindow( scrollpaneText );
	}


	/*********************************************************************/
	/*** Setup the scrollpane and add the  windows to the child window ***/
	/*********************************************************************/
	if ( mScrollPane )
	{
		mScrollPane->set(scrollPaneWindow, &mVbrushScrollPaneWindows, &strScrollpaneName, &strScrollImageSet, &strScrollImageName, false,
			childWindow_x_pos, childWindow_x_rightSpace,
			childWindow_y_pos, childWindow_y_size, childWindow_y_space,
			highligther_y_topSpace, highligther_y_bottomSpace);
	}
	
	/*****************************************************************************/
	/*** Setup the mTextureScrollPane and add the  windows to the child window ***/
	/*****************************************************************************/
	if ( mTextureScrollPane )
	{
		mTextureScrollPane->set(scrollPaneTextureWindow, &mVtextureScrollPaneWindows, &strTextureScrollpaneName, &strScrollImageSet, &strScrollImageName,
			false,
			childWindow_x_pos, childWindow_x_rightSpace,
			childWindow_y_pos, childWindow_y_size, childWindow_y_space,
			highligther_y_topSpace, highligther_y_bottomSpace);
	}


	/***********************************************************/
	/*** Setup the mButtonARGB and mButtonSplatScale  button ***/
	/***********************************************************/
	mButtonARGB = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button",
		"TerrainMenuSplatTabARGBWin_mButtonARGB");
	mButtonARGB->setText("argb");
	
	mButtonARGB->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&TerrainMenuSplatTab::event_mButtonARGB_click, this ) );


	mButtonSplatScale = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button",
		"TerrainMenuSplatTabARGBWin_mButtonSplatScale");
	mButtonSplatScale->setText("Scale");

	mButtonSplatScale->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&TerrainMenuSplatTab::event_terrainMenuSplatTabScalesWin_click, this ) );


	/*******************************************/
	/*** The mirrorX and mirrorY check boxes ***/
	/*******************************************/
	mirrorX_checkbox = static_cast<CEGUI::Checkbox *>( windowManager.createWindow("TaharezLook/Checkbox", "MainMenu_mirrorX_checkbox") );
	mirrorX_checkbox->setText("flipX");

	mirrorY_checkbox = static_cast<CEGUI::Checkbox *>( windowManager.createWindow("TaharezLook/Checkbox", "MainMenu_mirrorY_checkbox") );
	mirrorY_checkbox->setText("flipY");


	/**********************************************************************/
	/*** Set the size and positions of the windows for the "Splat" tab ****/
	/**********************************************************************/

	
	float frame_x_pos = 0.00f;
	float frame_x_size = 1.0f;
	float frame_y_size = 0.018f;
	float frame_bottom_padding = 0.004f;
	float frame_top_padding = 0.008f;

	float text_x_pos = 0.07f;
	float text_y_size = 0.06f;

	float scrollbar_top_y_padding = 0.031f;
	float scrollbar_x_pos = 0.07f;
	float scrollbar_x_size = 0.86f;
	float scrollbar_y_size = 0.027f;


	float brushes_label_y_pos = 0.002f;

	float mScrollPaneWindow_x_pos = 0.07f;
	float mScrollPaneWindow_y_pos = brushes_label_y_pos + 0.05f;
	float mScrollPaneWindow_x_size = 0.6f;
	float mScrollPaneWindow_y_size = 0.27f;	
	float scrollPaneTextureWindow_x_pos = mScrollPaneWindow_x_pos;

	float mTextureScrollPane_y_size = 0.248f;	

	float mButtonReloadBrush_y_pos = mScrollPaneWindow_y_pos + mScrollPaneWindow_y_size + 0.010f;
	float mButtonReloadBrush_x_size = mScrollPaneWindow_x_size + 0.01;
	float mButtonReloadBrush_y_size = 0.035F;	



	float frameBelowRefreshButton_y_pos = mButtonReloadBrush_y_pos + mButtonReloadBrush_y_size + 0.011f;
	float textures_label_y_pos = frameBelowRefreshButton_y_pos + 0.005f;
	float mTextureScrollPane_y_pos = textures_label_y_pos + 0.05f;

	float mFrameBelowDecreaseScrollbar_y_pos = mTextureScrollPane_y_pos + mTextureScrollPane_y_size + 0.010f;

	// The y position is different because of the frame;
	float mBrush_x_scale_Text_y_pos = mFrameBelowDecreaseScrollbar_y_pos + frame_y_size + frame_bottom_padding;
	float mBrush_x_scale_Scrollbar_y_pos = mBrush_x_scale_Text_y_pos + scrollbar_top_y_padding;

	float mBrush_y_scale_Text_y_pos = mBrush_x_scale_Scrollbar_y_pos + scrollbar_top_y_padding;
	float mBrush_y_scale_Scrollbar_y_pos = mBrush_y_scale_Text_y_pos + scrollbar_top_y_padding;


	float mFrameBelow_brushYScale_Scrollbar_y_pos = mBrush_y_scale_Scrollbar_y_pos + scrollbar_y_size + frame_top_padding;


	// The y position is different because of the frame;
	float mCamDegreeLabel_y_pos = mFrameBelow_brushYScale_Scrollbar_y_pos + frame_y_size + frame_bottom_padding;
	float mCamDegree_Scrollbar_y_pos = mCamDegreeLabel_y_pos + scrollbar_top_y_padding;


	float mAutoRepeat_Text_y_pos = mCamDegree_Scrollbar_y_pos + scrollbar_top_y_padding;
	float mAutoRepeat_Scrollbar_y_pos = mAutoRepeat_Text_y_pos + scrollbar_top_y_padding;


	float mButtonARGB_x_pos = scrollPaneTextureWindow_x_pos + mScrollPaneWindow_x_size + 0.01f;
	float mButtonARGB_y_pos = mTextureScrollPane_y_pos;
	float mButtonARGB_x_size = 0.25f;
	float mButtonARGB_y_size = 0.04f;

	float mButtonSplatScale_x_pos = scrollPaneTextureWindow_x_pos + mScrollPaneWindow_x_size + 0.01f;
	float mButtonSplatScale_y_pos = mButtonARGB_y_pos + mButtonARGB_y_size + 0.02f;
	float mButtonSplatScale_x_size = 0.25f;
	float mButtonSplatScale_y_size = 0.04f;


	float checkbox_x_size = 1.0f;
	float checkbox_y_size = 0.03f;

	float mirrorX_checkbox_x_pos = mScrollPaneWindow_x_pos + mScrollPaneWindow_x_size;
	float mirrorX_checkbox_y_pos = mScrollPaneWindow_y_pos;

	float mirrorY_checkbox_y_pos = mirrorX_checkbox_y_pos + checkbox_y_size + 0.005f;


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	// Set the brushes label;
	brushes_label->setYPosition(UDim(brushes_label_y_pos, 0.0f));
	brushes_label->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	scrollPaneWindow->setPosition(UVector2(UDim(mScrollPaneWindow_x_pos, 0.0f), UDim(mScrollPaneWindow_y_pos, 0.0f)));
	scrollPaneWindow->setSize(UVector2(UDim(mScrollPaneWindow_x_size, 0.0f), UDim(mScrollPaneWindow_y_size, 0.0f))); 

	buttonReloadBrush->setPosition(UVector2(UDim(mScrollPaneWindow_x_pos, 0.0f), UDim(mButtonReloadBrush_y_pos, 0.0f)));
	buttonReloadBrush->setSize(UVector2(UDim(mButtonReloadBrush_x_size, 0.0f), UDim(mButtonReloadBrush_y_size, 0.0f))); 

	frameBelowRefreshButton->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(frameBelowRefreshButton_y_pos, 0.0f)));
	frameBelowRefreshButton->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	// Set the textures label;
	textures_label->setYPosition(UDim(textures_label_y_pos, 0.0f));
	textures_label->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 


	scrollPaneTextureWindow->setPosition(UVector2(UDim(scrollPaneTextureWindow_x_pos, 0.0f), UDim(mTextureScrollPane_y_pos, 0.0f)));
	scrollPaneTextureWindow->setSize(UVector2(UDim(mScrollPaneWindow_x_size, 0.0f), UDim(mTextureScrollPane_y_size, 0.0f))); 


	// Frame;
	frameBelowDecreaseScrollbar->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowDecreaseScrollbar_y_pos, 0.0f)));
	frameBelowDecreaseScrollbar->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 


	// Scale X, y, z;
	mBrush_x_scale_Text->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mBrush_x_scale_Text_y_pos, 0.0f)));
	mBrush_x_scale_Text->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mBrush_x_scale_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mBrush_x_scale_Scrollbar_y_pos, 0.0f)));
	mBrush_x_scale_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 

	mBrush_y_scale_Text->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mBrush_y_scale_Text_y_pos, 0.0f)));
	mBrush_y_scale_Text->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mBrush_y_scale_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mBrush_y_scale_Scrollbar_y_pos, 0.0f)));
	mBrush_y_scale_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	// Frame;
	mFrameBelow_brushYScale_Scrollbar->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelow_brushYScale_Scrollbar_y_pos, 0.0f)));
	mFrameBelow_brushYScale_Scrollbar->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f))); 

	// Camera Degree;
	mCamDegreeLabel->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mCamDegreeLabel_y_pos, 0.0f)));
	mCamDegreeLabel->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mCamDegree_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mCamDegree_Scrollbar_y_pos, 0.0f)));
	mCamDegree_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 

	// Mouse Auto-repeat rate;
	mAutoRepeat_Text->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mAutoRepeat_Text_y_pos, 0.0f)));
	mAutoRepeat_Text->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mAutoRepeat_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mAutoRepeat_Scrollbar_y_pos, 0.0f)));
	mAutoRepeat_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	/*** The ARGB and Scales options buttons ***/
	mButtonARGB->setPosition(UVector2(UDim(mButtonARGB_x_pos, 0.0f), UDim(mButtonARGB_y_pos, 0.0f)));
	mButtonARGB->setSize(UVector2(UDim(mButtonARGB_x_size, 0.0f), UDim(mButtonARGB_y_size, 0.0f))); 

	mButtonSplatScale->setPosition(UVector2(UDim(mButtonSplatScale_x_pos, 0.0f), UDim(mButtonSplatScale_y_pos, 0.0f)));
	mButtonSplatScale->setSize(UVector2(UDim(mButtonSplatScale_x_size, 0.0f), UDim(mButtonSplatScale_y_size, 0.0f))); 


	/*** The mirrorX and mirrorY check boxes ***/
	mirrorX_checkbox->setSize(CEGUI::UVector2(CEGUI::UDim(checkbox_x_size, 0), CEGUI::UDim(checkbox_y_size, 0)));
	mirrorX_checkbox->setPosition( CEGUI::UVector2(CEGUI::UDim(mirrorX_checkbox_x_pos, 0), CEGUI::UDim(mirrorX_checkbox_y_pos, 0) ) );

	mirrorY_checkbox->setSize(CEGUI::UVector2(CEGUI::UDim(checkbox_x_size, 0), CEGUI::UDim(checkbox_y_size, 0)));
	mirrorY_checkbox->setPosition( CEGUI::UVector2(CEGUI::UDim(mirrorX_checkbox_x_pos, 0), CEGUI::UDim(mirrorY_checkbox_y_pos, 0) ) );


	/***********************/
	/*** Attach the tabs ***/
	/***********************/

	// Attach everything needed on the "Main" tab;
	mParentwin->addChildWindow( brushes_label );
	mParentwin->addChildWindow( scrollPaneWindow );
	mParentwin->addChildWindow( buttonReloadBrush );
	mParentwin->addChildWindow( frameBelowRefreshButton );
	mParentwin->addChildWindow( textures_label );
	mParentwin->addChildWindow( scrollPaneTextureWindow );
	mParentwin->addChildWindow( frameBelowDecreaseScrollbar );
	mParentwin->addChildWindow( mBrush_x_scale_Text );
	mParentwin->addChildWindow( mBrush_x_scale_Scrollbar );
	mParentwin->addChildWindow( mBrush_y_scale_Text );
	mParentwin->addChildWindow( mBrush_y_scale_Scrollbar );
	mParentwin->addChildWindow( mFrameBelow_brushYScale_Scrollbar );
	mParentwin->addChildWindow( mCamDegreeLabel );
	mParentwin->addChildWindow( mCamDegree_Scrollbar );
	mParentwin->addChildWindow( mAutoRepeat_Text );
	mParentwin->addChildWindow( mAutoRepeat_Scrollbar );

	mParentwin->addChildWindow( mButtonARGB );
	mParentwin->addChildWindow( mButtonSplatScale );

	mParentwin->addChildWindow( mirrorX_checkbox );
	mParentwin->addChildWindow( mirrorY_checkbox );


	/*******************************/
	/*** Create the Side Windows ***/
	/*******************************/
	mTerrainMenuSplatTabARGBWin->addWindow();



	_loadBrushesAndTextures();

	return true;
} 

void TerrainMenuSplatTab::updateWindowShown()
{
	// Refresh the mouse rate;
	TerrainMenuSplatTab::_setMouseRate();

	// Refresh the scrollbars;
	TerrainMenuSplatTab::_reAdjustScrollbars();
}

void TerrainMenuSplatTab::updateWindowHidden()
{
	if (mTerrainMenuSplatTabARGBWin)
	{
		if ( mTerrainMenuSplatTabARGBWin->isVisible() )
		{
			mTerrainMenuSplatTabARGBWin->hide();	
		}
	}

	if (mHeapTerrainMenuSplatTabScalesWin)
	{
		delete mHeapTerrainMenuSplatTabScalesWin;
		mHeapTerrainMenuSplatTabScalesWin = NULL;
	}

	if (mHeapTerrainMenuSplatTabFileDialog)
	{
		delete mHeapTerrainMenuSplatTabFileDialog;
		mHeapTerrainMenuSplatTabFileDialog = NULL;
	}
}

inline void TerrainMenuSplatTab::_setMouseRate()
{
	// Set the mouse auto-repeat rate;
	CEGUI::System::getSingleton().getGUISheet()->setAutoRepeatRate(mAutoRepeatRate);
}

void TerrainMenuSplatTab::_reAdjustScrollbars()
{
	string tmpstr;
	CEGUI::PropertyHelper phelper;
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	// Get the Camera move Degree rate ***/
	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();
	
	/*** Set the camera rate scrollbar and label ***/
	tmpstr = "Camera Rate: ";

	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);

	tmpstr += phelper.floatToString(mCameraDegree).c_str();

	// Set the text;
	mCamDegreeLabel->setText(tmpstr);
}

void TerrainMenuSplatTab::_loadBrushesAndTextures()
{
	/*
	*** We can't use the same listbox texture for the terrain texture.
	*** The problem is, if we use the listbox sample texture than it gets resized and makes the
	*** terrain look grainy and bad. So we use two textures, one for the sample and one for the terrain.
	 */

	/*
	*** We make two images the normal material texture and the sample one. The sample image should start with "sample_" and the regular one
	*** is just something.jpg. So if we load "sample_something.jpg", we then try to load "something.jpg" as the terrain material.
	 */


	SplatManager* const splatManager = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager();
	string tmpString;


	/*** Clear the mVterrainTextureNames ***/
	mVterrainTextureNames.clear();


	/*** Set the splat brushes and Set the current Splat brush to the first one  ***/
	if ( splatManager->getBrushCount() == 0 )
	{
		/*** Set the splat brushes ***/
		for (unsigned long i = 0; i < mVbrushNames.size(); i++)
		{
			splatManager->addBrush( mVbrushNames.at(i) );
		}
	
	
		/*** Set the current Splat brush to the first one ***/
		splatManager->setCurrentBrush(0);
	}


	/*** If the texture starts with "sample_" then take it out because were using a texture without the "sample_" ***/
	for (unsigned long i = 0; i < mVsampleTextureNames.size(); i++)
	{
		tmpString = mVsampleTextureNames.at(i);

		// Try to get the substr "sample_" to see if its there;
		tmpString = mVsampleTextureNames.at(i).substr(0, 7);
		

		if (tmpString == SPLATMAP_SAMPLETEXTURE_FILENAME_STARTPART)
		{
			mVterrainTextureNames.push_back( mVsampleTextureNames.at(i).substr(7) );
		}
	}


	/*** Set the four splat textures ***/
	splatManager->setTextures( mVterrainTextureNames.at(0), mVterrainTextureNames.at(1), mVterrainTextureNames.at(2),
		mVterrainTextureNames.at(3) );


	/*** Set the current splat texture to the first one ***/
	splatManager->setCurrentTexture(0);


	// We reset the ARGB window and its values, scrollbars and ect;
	// The reason is were loading new brushes so that needs a reset;
	if (mTerrainMenuSplatTabARGBWin)
	{
		mTerrainMenuSplatTabARGBWin->reset();
	}
}

void TerrainMenuSplatTab::setSampleTexture(const unsigned long number, string imageName)
{
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();

	string strImageSetName;
	string strSetImage;



	/*** Clear all the scrollpane images ***/
	for (unsigned long i = 0; i < mVsampleTextureNames.size(); i++)
	{
		mVtextureScrollpaneImage.at(i)->setProperty( "Image", "");
	}



	/*** Delete all the imageSets if they exist ***/
	for (unsigned long i = 0; i < mVsampleTextureNames.size(); i++)
	{
		if ( imageSetManager.isImagesetPresent(strImageSetName) )
		{
			CEGUI::ImagesetManager::getSingleton().destroyImageset(strImageSetName);
		}
	}


	/*** Set the new File name to the selected scrollpane image index ***/
	mVsampleTextureNames.at(number) = imageName;


	/*** Create the new ImageSets and set the scrollPane Imanges ***/
	for (unsigned long i = 0; i < mVsampleTextureNames.size(); i++)
	{
		strImageSetName = TERRAINMENUSPLATTAB_UNIQUE_TEXTURENAME_START + mVsampleTextureNames.at(i);

		if (imageSetManager.isImagesetPresent(strImageSetName) == false)
		{
			imageSetManager.createImagesetFromImageFile( strImageSetName, mVsampleTextureNames.at(i) );
		}

		strSetImage = "set:" + strImageSetName + " image:full_image";
		mVtextureScrollpaneImage.at(i)->setProperty("Image", strSetImage);		
	}
}

void TerrainMenuSplatTab::splatTerrain()
{
	if (mTerrainMenuSplatTabARGBWin)
	{
		const vector<Ogre::Vector4>* const mvARGB = mTerrainMenuSplatTabARGBWin->getARGB();
		unsigned long index =  mTextureScrollPane->getSelectedWindow();
		SplatManager* const splatManager = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager();
	
		if (mvARGB)
		{
			splatManager->brush(mBrush_x_scale * X_SCALE_TO_BRUSH, 
					    mBrush_y_scale * Y_SCALE_TO_BRUSH,
				mirrorX_checkbox->isSelected(), mirrorY_checkbox->isSelected(),
				mvARGB->at(index).x, mvARGB->at(index).y,
				mvARGB->at(index).z, mvARGB->at(index).w);
		}
	}	
}

unsigned long TerrainMenuSplatTab::getBrushCount()
{
	if ( mScrollPane )
	{
		return mScrollPane->getWindowCount();
	}

	return 0;
}

unsigned long TerrainMenuSplatTab::getSelectedBrush()
{
	if ( mScrollPane )
	{
		return mScrollPane->getSelectedWindow();
	}

	return 0;
}

void TerrainMenuSplatTab::setBrush(const unsigned long number)
{
	SplatManager* const splatManager = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager();

	// highlight the brush image and set the number;
	if ( mScrollPane )
	{
		// Set the selected brush image number;
		mScrollPane->setWindow(number);

		// Get the current Splat brush number using the selected window number;
		splatManager->setCurrentBrush( mScrollPane->getSelectedWindow() );
		this->_brushChange();
	}
}

void TerrainMenuSplatTab::updateTerrainBrush()
{
  this->_brushChange();
}

void TerrainMenuSplatTab::_brushChange()
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	unsigned long windowNumber = 0;

	Ogre::Vector2 screenPos(
	CEGUI::MouseCursor::getSingleton().getPosition().d_x / (float)sheet->getPixelRect().getWidth(),
	CEGUI::MouseCursor::getSingleton().getPosition().d_y / (float)sheet->getPixelRect().getHeight()
	);


	if ( mScrollPane )
	{
		windowNumber = mScrollPane->getSelectedWindow();
	}

	std::string* tmp = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager()->getCurrentBrush();


	// Set the brush size;
	terrainManager->setBrushSize( Ogre::Vector2( mBrush_x_scale * X_SCALE_TO_BRUSH, 
						     mBrush_y_scale * X_SCALE_TO_BRUSH ) );

	// Set the brush;
	terrainManager->setBrush( *tmp );

	// Set the brush position - The brush wont show unless we set its position;
	terrainManager->setBrushPositionToCursor();
}

unsigned long TerrainMenuSplatTab::getTextureCount()
{
	if ( mTextureScrollPane )
	{
		return mTextureScrollPane->getWindowCount();
	}

	return 0;
}

unsigned long TerrainMenuSplatTab::getSelectedTexture()
{
	if ( mTextureScrollPane )
	{
		return mTextureScrollPane->getSelectedWindow();
	}

	return 0;
}

void TerrainMenuSplatTab::setSelectedTexture(const unsigned long number)
{
	SplatManager* const splatManager = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager();

	// highlight the brush image and set the number;
	if ( mTextureScrollPane )
	{
		// Set the selected brush image number;
		mTextureScrollPane->setWindow(number);

		// Get the current Splat brush number using the selected window number;
		splatManager->setCurrentTexture( mTextureScrollPane->getSelectedWindow() );

		// When we change the texture and the ARGB window is open, we want to,
		//  set the ARGB window to show the settings for the current texture;
		if (mTerrainMenuSplatTabARGBWin)
		{
			if ( mTerrainMenuSplatTabARGBWin->isVisible() )
			{
				mTerrainMenuSplatTabARGBWin->show( mTextureScrollPane->getSelectedWindow() );
			}
		}
	}
}

bool TerrainMenuSplatTab::event_windowVisible(const CEGUI::EventArgs& pEventArgs)
{
	TerrainMenuSplatTab::updateWindowShown();


	return true;
}

bool TerrainMenuSplatTab::event_windowHidden(const CEGUI::EventArgs &pEventArgs)
{
	TerrainMenuSplatTab::updateWindowHidden();

	return true;
}

bool TerrainMenuSplatTab::event_parent_resize(const CEGUI::EventArgs &pEventArgs)
{
	/*** Call the functions to resize the scrollpane and and windows inside of it ***/
	if ( mScrollPane )
	{
		mScrollPane->resize();
	}

	if ( mTextureScrollPane )
	{
		mTextureScrollPane->resize();
	}


	return true;
}

bool TerrainMenuSplatTab::event_brushChange(const CEGUI::EventArgs& pEventArgs)
{
	SplatManager* const splatManager = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager();

	if (mScrollPane)
	{
		splatManager->setCurrentBrush( mScrollPane->getSelectedWindow() );
	}


	return true;
}

bool TerrainMenuSplatTab::event_TextureChange(const CEGUI::EventArgs& pEventArgs)
{
	SplatManager* const splatManager = EditManager::getSingletonPtr()->getTerrainManager()->getSplatManager();

	if (mTextureScrollPane)
	{
		splatManager->setCurrentTexture( mTextureScrollPane->getSelectedWindow() );

		// When we change the texture and the ARGB window is open, we want to,
		//  set the ARGB window to show the settings for the current texture;
		if (mTerrainMenuSplatTabARGBWin)
		{
			if ( mTerrainMenuSplatTabARGBWin->isVisible() )
			{
				mTerrainMenuSplatTabARGBWin->show( mTextureScrollPane->getSelectedWindow() );
			}
		}
	}


	return true;
}


bool TerrainMenuSplatTab::event_mHeapTerrainMenuSplatTabFileDialog_Show(const CEGUI::EventArgs &pEventArgs)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strLogFunctionName = "bool TerrainMenuSplatTab::event_mHeapTerrainMenuSplatTabFileDialog_Show(...)";
	string strLogMessage;


	if (!mHeapTerrainMenuSplatTabFileDialog)
	{
		mHeapTerrainMenuSplatTabFileDialog = new HeapTerrainMenuSplatTabFileDialog;

		if (!mHeapTerrainMenuSplatTabFileDialog)
		{
			strLogMessage = "mHeapTerrainMenuSplatTabFileDialog";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
		}
		else
		{
			mHeapTerrainMenuSplatTabFileDialog->toggle();
		}	
	}
	else
	{
		// The window may just be hidden so we don't have to toggle, just show it;
		mHeapTerrainMenuSplatTabFileDialog->getWindow()->show();
	}


	return true;
}

bool TerrainMenuSplatTab::event_mBrush_x_scale_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Brush Scale X: ";
	CEGUI::PropertyHelper phelper;
	const unsigned long xscale = mBrush_x_scale_Scrollbar->getScrollPosition();


	tmpstr += phelper.uintToString( xscale ).c_str();

	// Set the text;
	mBrush_x_scale_Text->setText( tmpstr ); 

	// Set the class member;
	mBrush_x_scale = xscale;


	return true;
}

bool TerrainMenuSplatTab::event_mBrush_y_scale_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Brush Scale Y: ";
	CEGUI::PropertyHelper phelper;
	const unsigned long yscale = mBrush_y_scale_Scrollbar->getScrollPosition();


	tmpstr += phelper.uintToString( yscale ).c_str();

	// Set the text;
	mBrush_y_scale_Text->setText( tmpstr ); 

	// Set the class member;
	mBrush_y_scale = yscale;

	return true;
}

bool TerrainMenuSplatTab::event_mCamDegree_Scrollbar_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Camera Rate: ";
	CEGUI::PropertyHelper phelper;
	float scrollPos = mCamDegree_Scrollbar->getScrollPosition();
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	// Since the scrollbar starts at '0', we want it to never decrease passed '0.001';
	// if the scroll pos is at 0 then we set the scroll position to 0.001;
	if (scrollPos < 0.001f)
	{
		scrollPos = 0.001f;
		 mCamDegree_Scrollbar->setScrollPosition(scrollPos);
	}

	tmpstr += phelper.floatToString(scrollPos).c_str();

	// Set the text;
	mCamDegreeLabel->setText( tmpstr ); 

	// Set the class member;
	mCameraDegree = scrollPos;


	// Set the camera sceneNode and camera degree;
	cameraManager->setCameraSceneNodeMoveDegree(mCameraDegree);
	cameraManager->setCameraMoveDegree(mCameraDegree);


	return true;
}

bool TerrainMenuSplatTab::event_mAutoRepeatRate_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Mouse Rate: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mAutoRepeat_Scrollbar->getScrollPosition();

	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mAutoRepeat_Text->setText( tmpstr ); 

	// Set the class member;
	mAutoRepeatRate = scrollPos;

	// Set the mouse auto-repeat rate;
	_setMouseRate();

	return true;
}

 
bool TerrainMenuSplatTab::event_mButtonARGB_click(const CEGUI::EventArgs &pEventArgs)
{
	if (mTerrainMenuSplatTabARGBWin)
	{
		if ( mTerrainMenuSplatTabARGBWin->isVisible() )
		{
			mTerrainMenuSplatTabARGBWin->hide();	
		}
		else
		{
			mTerrainMenuSplatTabARGBWin->show( mTextureScrollPane->getSelectedWindow() );
		}
	}

	return true;
}

bool TerrainMenuSplatTab::event_terrainMenuSplatTabScalesWin_click(const CEGUI::EventArgs &pEventArgs)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strLogFunctionName = "bool TerrainMenuSplatTab::event_terrainMenuSplatTabScalesWin_click(...)";
	string strLogMessage;


	if (!mHeapTerrainMenuSplatTabScalesWin)
	{
		mHeapTerrainMenuSplatTabScalesWin = new HeapTerrainMenuSplatTabScalesWin;

		if (!mHeapTerrainMenuSplatTabScalesWin)
		{
			strLogMessage = "mHeapTerrainMenuSplatTabScalesWin";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
		}
		else
		{
			mHeapTerrainMenuSplatTabScalesWin->toggle();
		}	
	}
	else
	{
		// The window may just be hidden so we don't have to toggle, just show it;
		mHeapTerrainMenuSplatTabScalesWin->getWindow()->show();
	}


	return true;
}

