/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TerrainMenuSplatTabARGBWin.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace std;
using namespace CEGUI;



TerrainMenuSplatTabARGBWin::TerrainMenuSplatTabARGBWin() : mTextureNumber(0)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "TerrainMenuSplatTabARGBWin::TerrainMenuSplatTabARGBWin()";
	string strErrorWordName;


	/*** Allocate memory for 'mvARGB' and set its ARGB contents ***/
	mvARGB = new vector<Ogre::Vector4>;

	if (!mvARGB)
	{
		strErrorWordName = "mvARGB";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	// Push back the Ogre::Vector4 containing the ARGB colors;
	mvARGB->push_back( Ogre::Vector4(DEF_MAX_COLOR_VALUE,0,0,0) );
	mvARGB->push_back( Ogre::Vector4(0,DEF_MAX_COLOR_VALUE,0,0) );
	mvARGB->push_back( Ogre::Vector4(0,0,DEF_MAX_COLOR_VALUE,0) );
	mvARGB->push_back( Ogre::Vector4(0,0,0,DEF_MAX_COLOR_VALUE) );
}

TerrainMenuSplatTabARGBWin::~TerrainMenuSplatTabARGBWin()
{
	/*** Destroy the window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mMainWindow);


	if ( mvARGB )
	{
		delete mvARGB;
		mvARGB = NULL;
	}
}

bool TerrainMenuSplatTabARGBWin::addWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::PropertyHelper phelper;



	/*********************************************/
	/*** The Main window and the Parent Window ***/
	/*********************************************/
	mMainWindow = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "TerrainMenuSplatTabARGBWin_mMainWindow");
	mMainWindow->setText("argb");
	mMainWindow->setVerticalAlignment(CEGUI::VA_CENTRE);
	mMainWindow->setHorizontalAlignment(CEGUI::HA_CENTRE);


	CEGUI::Window* parentWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe",
		"TerrainMenuSplatTabARGBWin_parentWindow");


	/**********************************/
	/*** Alplha label and scrollbar ***/
	/**********************************/
	mLabalAlpha = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabARGBWin_mLabalAlpha");
	mLabalAlpha->setProperty("FrameEnabled", "False");
	mLabalAlpha->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalAlpha->disable();
	// Center the text;
	mLabalAlpha->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mLabalAlpha->setProperty("VertFormatting", "TopAligned");
	mLabalAlpha->setText( "Alpha: " + phelper.uintToString( static_cast<unsigned int>(0)) );


	mLabalAlpha_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabARGBWin_mLabalAlpha_Scrollbar");
	mLabalAlpha_Scrollbar->setDocumentSize(DEF_MAX_COLOR_VALUE + 1);
	mLabalAlpha_Scrollbar->setScrollPosition(0);
	mLabalAlpha_Scrollbar->setPageSize(1);
	mLabalAlpha_Scrollbar->setStepSize(1);


	/*******************************/
	/*** Red label and scrollbar ***/
	/*******************************/
	mLabalRed = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabARGBWin_mLabalRed");
	mLabalRed->setProperty("FrameEnabled", "False");
	mLabalRed->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalRed->disable();
	// Center the text;
	mLabalRed->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mLabalRed->setProperty("VertFormatting", "TopAligned");
	mLabalRed->setText( "Red: " + phelper.uintToString( static_cast<unsigned int>(0)) );


	mLabalRed_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabARGBWin_mLabalRed_Scrollbar");
	mLabalRed_Scrollbar->setDocumentSize(DEF_MAX_COLOR_VALUE + 1);
	mLabalRed_Scrollbar->setScrollPosition(0);
	mLabalRed_Scrollbar->setPageSize(1);
	mLabalRed_Scrollbar->setStepSize(1);


	/*********************************/
	/*** Green label and scrollbar ***/
	/*********************************/
	mLabalGreen = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabARGBWin_mLabalGreen");
	mLabalGreen->setProperty("FrameEnabled", "False");
	mLabalGreen->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalGreen->disable();
	// Center the text;
	mLabalGreen->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mLabalGreen->setProperty("VertFormatting", "TopAligned");
	mLabalGreen->setText( "Green: " + phelper.uintToString( static_cast<unsigned int>(0)) );


	mLabalGreen_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabARGBWin_mLabalGreen_Scrollbar");
	mLabalGreen_Scrollbar->setDocumentSize(DEF_MAX_COLOR_VALUE + 1);
	mLabalGreen_Scrollbar->setScrollPosition(0);
	mLabalGreen_Scrollbar->setPageSize(1);
	mLabalGreen_Scrollbar->setStepSize(1);


	/********************************/
	/*** Blue label and scrollbar ***/
	/********************************/
	mLabalBlue = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabARGBWin_mLabalBlue");
	mLabalBlue->setProperty("FrameEnabled", "False");
	mLabalBlue->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalBlue->disable();
	// Center the text;
	mLabalBlue->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mLabalBlue->setProperty("VertFormatting", "TopAligned");
	mLabalBlue->setText( "Blue: " + phelper.uintToString( static_cast<unsigned int>(0)) );


	mLabalBlue_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabARGBWin_mLabalBlue_Scrollbar");
	mLabalBlue_Scrollbar->setDocumentSize(DEF_MAX_COLOR_VALUE + 1);
	mLabalBlue_Scrollbar->setScrollPosition(0);
	mLabalBlue_Scrollbar->setPageSize(1);
	mLabalBlue_Scrollbar->setStepSize(1);


	/**************************************/
	/*** Default and Close push Buttons ***/
	/**************************************/
	mButtonDefault = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button",
		"TerrainMenuSplatTabARGBWin_mButtonDefault");
	mButtonDefault->setText("Defaults");
	mButtonDefault->setHorizontalAlignment(CEGUI::HA_CENTRE);


	mButtonClose = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button",
		"TerrainMenuSplatTabARGBWin_mButtonClose");
	mButtonClose->setText("Close");
	mButtonClose->setHorizontalAlignment(CEGUI::HA_CENTRE);


	/******************/
	/*** The Events ***/
	/******************/

	mLabalAlpha_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabARGBWin::event_Scrollbar_change, this));

	mLabalRed_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabARGBWin::event_Scrollbar_change, this));

	mLabalGreen_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabARGBWin::event_Scrollbar_change, this));

	mLabalBlue_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabARGBWin::event_Scrollbar_change, this));


	mButtonDefault->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&TerrainMenuSplatTabARGBWin::event_mButtonDefault_click, this ) );

	mButtonClose->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&TerrainMenuSplatTabARGBWin::event_mButtonClose_click, this ) );

	/******************************/
	/*** The Size and Positions ***/
	/******************************/

	const float mMainWindow_x_size = 0.2f;
	const float mMainWindow_y_size = 0.5f;	
	
	const float parentWindow_x_size = 0.935F;
	const float parentWindow_y_size = 0.9F;
	const float parentWindow_x_pos = 0.03;
	const float parentWindow_y_pos = 0.08F;

	const float start_x_pos = 0.07f;

	const float label_top_y_padding = 0.011f;
	const float label_x_size = 1.0f;
	const float label_y_size = 0.06f;

	const float scrollbar_top_y_padding = 0.011f;
	const float scrollbar_x_size = 0.86f;
	const float scrollbar_y_size = 0.047f;

	const float mButtonDefault_x_size = scrollbar_x_size;
	const float mButtonDefault_y_size = 0.075F;

	const float mButtonClose_x_size = scrollbar_x_size;
	const float mButtonClose_y_size = 0.085F;




	const float mLabalAlpha_y_pos = 0.04f;
	const float mLabalAlpha_Scrollbar_y_pos = mLabalAlpha_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalRed_y_pos = mLabalAlpha_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mLabalRed_Scrollbar_y_pos = mLabalRed_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalGreen_y_pos = mLabalRed_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mLabalGreen_Scrollbar_y_pos = mLabalGreen_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalBlue_y_pos = mLabalGreen_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mLabalBlue_Scrollbar_y_pos = mLabalBlue_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mButtonDefault_y_pos = mLabalBlue_Scrollbar_y_pos + scrollbar_y_size + 0.1f;
	const float mButtonClose_y_pos = 1.0f - 0.14f;

	/*** Main window and parent window ***/
	mMainWindow->setSize(CEGUI::UVector2(CEGUI::UDim(mMainWindow_x_size, 0), CEGUI::UDim(mMainWindow_y_size, 0)));

	parentWindow->setSize(UVector2(UDim(parentWindow_x_size, 0.0f), UDim(parentWindow_y_size, 0.0f))); 
	parentWindow->setPosition(UVector2(UDim(parentWindow_x_pos, 0.0f), UDim(parentWindow_y_pos, 0.0f))); 


	/*** Alplha label and scrollbar ***/
	mLabalAlpha->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f))); 
	mLabalAlpha->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalAlpha_y_pos, 0.0f))); 

	mLabalAlpha_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 
	mLabalAlpha_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalAlpha_Scrollbar_y_pos, 0.0f))); 


	/*** Red label and scrollbar ***/
	mLabalRed->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f))); 
	mLabalRed->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalRed_y_pos, 0.0f))); 

	mLabalRed_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 
	mLabalRed_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalRed_Scrollbar_y_pos, 0.0f))); 


	/*** Green label and scrollbar ***/
	mLabalGreen->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f))); 
	mLabalGreen->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalGreen_y_pos, 0.0f))); 

	mLabalGreen_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 
	mLabalGreen_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalGreen_Scrollbar_y_pos, 0.0f))); 


	/*** Blue label and scrollbar ***/
	mLabalBlue->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f))); 
	mLabalBlue->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalBlue_y_pos, 0.0f))); 

	mLabalBlue_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 
	mLabalBlue_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalBlue_Scrollbar_y_pos, 0.0f))); 


	/*** Default and Close push Buttons ***/
	mButtonDefault->setSize(CEGUI::UVector2(CEGUI::UDim(mButtonDefault_x_size, 0), CEGUI::UDim(mButtonDefault_y_size, 0)));
	mButtonDefault->setYPosition( CEGUI::UDim(mButtonDefault_y_pos, 0) );

	mButtonClose->setSize(CEGUI::UVector2(CEGUI::UDim(mButtonClose_x_size, 0), CEGUI::UDim(mButtonClose_y_size, 0)));
	mButtonClose->setYPosition( CEGUI::UDim(mButtonClose_y_pos, 0) );



	/********************/
	/*** The Settings ***/
	/********************/

	mMainWindow->setVisible(false);


	/*
	*** After scrollbars are added with AddChildWindow() the event_scrollbar_change() function is called.
	*** What we want to do is setup the default scrollbar positions before the scrollbar window is added.
	*** This way when the event_scrollbar_change() getScrollPosition() is called, it can get the default scrollbar values.
	 */
	TerrainMenuSplatTabARGBWin::reset();




	/***********************/
	/*** Add the windows ***/
	/***********************/
	parentWindow->addChildWindow(mLabalAlpha);
	parentWindow->addChildWindow(mLabalAlpha_Scrollbar);

	parentWindow->addChildWindow(mLabalRed);	
	parentWindow->addChildWindow(mLabalRed_Scrollbar);

	parentWindow->addChildWindow(mLabalGreen);
	parentWindow->addChildWindow(mLabalGreen_Scrollbar);

	parentWindow->addChildWindow(mLabalBlue);
	parentWindow->addChildWindow(mLabalBlue_Scrollbar);

	parentWindow->addChildWindow(mButtonDefault);
	parentWindow->addChildWindow(mButtonClose);

	mMainWindow->addChildWindow(parentWindow);
	sheet->addChildWindow(mMainWindow);

	
	return true;
} 

void TerrainMenuSplatTabARGBWin::show(const unsigned long textureNumber)
{
	// Set the texture number;
	mTextureNumber = textureNumber;


	// Update the scrollbars;
	if ( mvARGB->size() > mTextureNumber)
	{
		// Mute the scrollbars so we don't call the change event;
		mLabalAlpha_Scrollbar->setMutedState(true);
		mLabalRed_Scrollbar->setMutedState(true);
		mLabalGreen_Scrollbar->setMutedState(true);
		mLabalBlue_Scrollbar->setMutedState(true);

		// Set the new scrollposition;
		mLabalAlpha_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).x );
		mLabalRed_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).y );
		mLabalGreen_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).z );
		mLabalBlue_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).w );

		// Unmute the scrollbars;
		mLabalAlpha_Scrollbar->setMutedState(false);
		mLabalRed_Scrollbar->setMutedState(false);
		mLabalGreen_Scrollbar->setMutedState(false);
		mLabalBlue_Scrollbar->setMutedState(false);


		/*** Update the scrollbars text ***/
		TerrainMenuSplatTabARGBWin::_updateScrollbarsText();
	}


	// Show the window;
	mMainWindow->setVisible(true);
}

void TerrainMenuSplatTabARGBWin::hide()
{
	mMainWindow->setVisible(false);
}

bool TerrainMenuSplatTabARGBWin::isVisible()
{
	return mMainWindow->isVisible();
}

void TerrainMenuSplatTabARGBWin::reset()
{
	mTextureNumber = 0;

	if ( mvARGB->size() == 4 )
	{
		// Reset the Ogre::Vector4 containing the ARGB colors;
		mvARGB->at(0) = Ogre::Vector4(DEF_MAX_COLOR_VALUE,0,0,0);
		mvARGB->at(1) = Ogre::Vector4(0,DEF_MAX_COLOR_VALUE,0,0);
		mvARGB->at(2) = Ogre::Vector4(0,0,DEF_MAX_COLOR_VALUE,0);
		mvARGB->at(3) = Ogre::Vector4(0,0,0,DEF_MAX_COLOR_VALUE);
	
		
		// Mute the scrollbars so we don't call the change event;
		mLabalAlpha_Scrollbar->setMutedState(true);
		mLabalRed_Scrollbar->setMutedState(true);
		mLabalGreen_Scrollbar->setMutedState(true);
		mLabalBlue_Scrollbar->setMutedState(true);

		// Set the new scrollposition;
		mLabalAlpha_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).x );
		mLabalRed_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).y );
		mLabalGreen_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).z );
		mLabalBlue_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).w );
	
		// Unmute the scrollbars;
		mLabalAlpha_Scrollbar->setMutedState(false);
		mLabalRed_Scrollbar->setMutedState(false);
		mLabalGreen_Scrollbar->setMutedState(false);
		mLabalBlue_Scrollbar->setMutedState(false);	


		TerrainMenuSplatTabARGBWin::_updateScrollbarsText();
	}
}

const vector<Ogre::Vector4>* const TerrainMenuSplatTabARGBWin::getARGB()
{
	return mvARGB;
}

bool TerrainMenuSplatTabARGBWin::event_mButtonDefault_click(const CEGUI::EventArgs &pEventArgs)
{
	if ( mvARGB->size() > mTextureNumber)
	{
		mvARGB->at(mTextureNumber).x = 0;
		mvARGB->at(mTextureNumber).y = 0;
		mvARGB->at(mTextureNumber).z = 0;
		mvARGB->at(mTextureNumber).w = 0;

		switch(mTextureNumber)
		{
			case 0:
				{
					mvARGB->at(mTextureNumber).x = DEF_MAX_COLOR_VALUE;
					break;
				}
			case 1:
				{
					mvARGB->at(mTextureNumber).y = DEF_MAX_COLOR_VALUE;
					break;
				}
			case 2:
				{
					mvARGB->at(mTextureNumber).z = DEF_MAX_COLOR_VALUE;
					break;
				}
			case 3:
				{
					mvARGB->at(mTextureNumber).w = DEF_MAX_COLOR_VALUE;
					break;
				}
		}


		// Mute the scrollbars so we don't call the change event;
		mLabalAlpha_Scrollbar->setMutedState(true);
		mLabalRed_Scrollbar->setMutedState(true);
		mLabalGreen_Scrollbar->setMutedState(true);
		mLabalBlue_Scrollbar->setMutedState(true);

		// Set the new scrollposition;
		mLabalAlpha_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).x );
		mLabalRed_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).y );
		mLabalGreen_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).z );
		mLabalBlue_Scrollbar->setScrollPosition( mvARGB->at(mTextureNumber).w );

		// Unmute the scrollbars;
		mLabalAlpha_Scrollbar->setMutedState(false);
		mLabalRed_Scrollbar->setMutedState(false);
		mLabalGreen_Scrollbar->setMutedState(false);
		mLabalBlue_Scrollbar->setMutedState(false);


		/*** Update the scrollbars text ***/
		TerrainMenuSplatTabARGBWin::_updateScrollbarsText();
	}


	return true;
}

bool TerrainMenuSplatTabARGBWin::event_mButtonClose_click(const CEGUI::EventArgs &pEventArgs)
{
	mMainWindow->setVisible(false);
	return true;
}

bool TerrainMenuSplatTabARGBWin::event_Scrollbar_change(const CEGUI::EventArgs &pEventArgs)
{
	TerrainMenuSplatTabARGBWin::_updateScrollbarsText();
	return true;
}

void TerrainMenuSplatTabARGBWin::_updateScrollbarsText()
{
	CEGUI::PropertyHelper phelper;

	string tmpColorAlphaName = 	"Alpha: ";
	string tmpColorRedName =	"Red: ";
	string tmpColorGreenName = 	"Green: ";
	string tmpColorBlueName = 	"Blue: ";	


	const unsigned long tmpColorAlpha = static_cast<const unsigned long>( mLabalAlpha_Scrollbar->getScrollPosition() );
	const unsigned long tmpColorRed = static_cast<const unsigned long>( mLabalRed_Scrollbar->getScrollPosition() );
	const unsigned long tmpColorGreen = static_cast<const unsigned long>( mLabalGreen_Scrollbar->getScrollPosition() );
	const unsigned long tmpColorBlue = static_cast<const unsigned long>( mLabalBlue_Scrollbar->getScrollPosition() );


	tmpColorAlphaName.append( phelper.uintToString(tmpColorAlpha).c_str() );
	tmpColorRedName.append( phelper.uintToString(tmpColorRed).c_str() );
	tmpColorGreenName.append( phelper.uintToString(tmpColorGreen).c_str() );
	tmpColorBlueName.append( phelper.uintToString(tmpColorBlue).c_str() );

	mLabalAlpha->setText( tmpColorAlphaName ); 
	mLabalRed->setText( tmpColorRedName ); 
	mLabalGreen->setText( tmpColorGreenName ); 
	mLabalBlue->setText( tmpColorBlueName ); 


	if ( mvARGB->size() > mTextureNumber)
	{
		mvARGB->at(mTextureNumber).x = tmpColorAlpha;
		mvARGB->at(mTextureNumber).y = tmpColorRed;
		mvARGB->at(mTextureNumber).z = tmpColorGreen;
		mvARGB->at(mTextureNumber).w = tmpColorBlue;	
	}
}


