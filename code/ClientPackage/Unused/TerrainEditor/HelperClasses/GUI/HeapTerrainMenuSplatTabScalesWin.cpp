/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapTerrainMenuSplatTabScalesWin.h"
#include "SplatManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


#define DEF_MAX_SPLAT_SCALE 2000

#define DEF_SPLAT_SCALE_INDEX_X 0
#define DEF_SPLAT_SCALE_INDEX_Y 1
#define DEF_SPLAT_SCALE_INDEX_Z 2
#define DEF_SPLAT_SCALE_INDEX_W 3

#define DEF_MAX_DETAIL_SCALE 20000


using namespace std;
using namespace CEGUI;



HeapTerrainMenuSplatTabScalesWin::HeapTerrainMenuSplatTabScalesWin() : mTerrainMenuSplatTabScalesWin(NULL)
{

}

HeapTerrainMenuSplatTabScalesWin::~HeapTerrainMenuSplatTabScalesWin()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapTerrainMenuSplatTabScalesWin::~HeapTerrainMenuSplatTabScalesWin()";
	string strLogMessage;

	if (mTerrainMenuSplatTabScalesWin)
	{
		delete mTerrainMenuSplatTabScalesWin;
		mTerrainMenuSplatTabScalesWin = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted TerrainMenuSplatTabScalesWin";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

CEGUI::Window* const HeapTerrainMenuSplatTabScalesWin::getWindow() const
{
	if (mTerrainMenuSplatTabScalesWin)
	{
		return mTerrainMenuSplatTabScalesWin->getWindow();
	}

	return NULL;
}

void HeapTerrainMenuSplatTabScalesWin::toggle()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapTerrainMenuSplatTabScalesWin::toggle()";
	string strLogMessage;


	if ( mTerrainMenuSplatTabScalesWin )
	{
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		delete mTerrainMenuSplatTabScalesWin;
		mTerrainMenuSplatTabScalesWin = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted TerrainMenuSplatTabScalesWin";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mTerrainMenuSplatTabScalesWin = new TerrainMenuSplatTabScalesWin;

		if ( mTerrainMenuSplatTabScalesWin == NULL )
		{
			strLogMessage = "mTerrainMenuSplatTabScalesWin";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mTerrainMenuSplatTabScalesWin->addWindow();

		strLogMessage = "created TerrainMenuSplatTabScalesWin";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

bool HeapTerrainMenuSplatTabScalesWin::isVisible() const
{
	if (mTerrainMenuSplatTabScalesWin)
	{
		return mTerrainMenuSplatTabScalesWin->getWindow()->isVisible();
	}

	return false;
}







TerrainMenuSplatTabScalesWin::TerrainMenuSplatTabScalesWin()
{
	EditManager* const editManager = EditManager::getSingletonPtr();
	SplatManager* const splatManager = editManager->getTerrainManager()->getSplatManager();



	/*** Set the Detail and splat scales ***/
	mSplatScales = splatManager->getSplatScales();
	mDetailScales = splatManager->getDetailScales();
}

TerrainMenuSplatTabScalesWin::~TerrainMenuSplatTabScalesWin()
{
	/*** Destroy the window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mMainWindow);
}

CEGUI::Window* const TerrainMenuSplatTabScalesWin::getWindow() const
{
	return mMainWindow;
}

bool TerrainMenuSplatTabScalesWin::addWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::PropertyHelper phelper;



	/*********************************************/
	/*** The Main window and the Parent Window ***/
	/*********************************************/
	mMainWindow = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "TerrainMenuSplatTabScalesWin_mMainWindow");
	mMainWindow->setText("Texture Scales");
	mMainWindow->setVerticalAlignment(CEGUI::VA_CENTRE);
	mMainWindow->setHorizontalAlignment(CEGUI::HA_CENTRE);


	CEGUI::Window* parentWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe",
		"TerrainMenuSplatTabScalesWin_parentWindow");


	/*********************************/
	/*** mLabalSplatTextures label ***/
	/*********************************/
	mLabalSplatTextures = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalSplatTextures");
	mLabalSplatTextures->setProperty("FrameEnabled", "False");
	mLabalSplatTextures->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalSplatTextures->disable();
	// Center the text;
	mLabalSplatTextures->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalSplatTextures->setProperty("VertFormatting", "TopAligned");
	mLabalSplatTextures->setText("Splat Textures");


	/*********************************************/
	/*** mLabalSplatScaleX label and scrollbar ***/
	/*********************************************/
	mLabalSplatScaleX = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalSplatScaleX");
	mLabalSplatScaleX->setProperty("FrameEnabled", "False");
	mLabalSplatScaleX->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalSplatScaleX->disable();
	// Center the text;
	mLabalSplatScaleX->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalSplatScaleX->setProperty("VertFormatting", "TopAligned");
	mLabalSplatScaleX->setText( "1: " + phelper.uintToString(static_cast<unsigned int>(mSplatScales.x)) );


	mSplatScaleX_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabScalesWin_mSplatScaleX_Scrollbar");
	mSplatScaleX_Scrollbar->setDocumentSize(DEF_MAX_SPLAT_SCALE + 1);
	mSplatScaleX_Scrollbar->setScrollPosition( static_cast<unsigned int>(mSplatScales.x) );
	mSplatScaleX_Scrollbar->setPageSize(1);
	mSplatScaleX_Scrollbar->setStepSize(1);


	/*********************************************/
	/*** mLabalSplatScaleY label and scrollbar ***/
	/*********************************************/
	mLabalSplatScaleY = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalSplatScaleY");
	mLabalSplatScaleY->setProperty("FrameEnabled", "False");
	mLabalSplatScaleY->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalSplatScaleY->disable();
	// Center the text;
	mLabalSplatScaleY->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalSplatScaleY->setProperty("VertFormatting", "TopAligned");
	mLabalSplatScaleY->setText( "2: " + phelper.uintToString(static_cast<unsigned int>(mSplatScales.y)) );


	mSplatScaleY_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabScalesWin_mSplatScaleY_Scrollbar");
	mSplatScaleY_Scrollbar->setDocumentSize(DEF_MAX_SPLAT_SCALE + 1);
	mSplatScaleY_Scrollbar->setScrollPosition( static_cast<unsigned int>(mSplatScales.y) );
	mSplatScaleY_Scrollbar->setPageSize(1);
	mSplatScaleY_Scrollbar->setStepSize(1);


	/*********************************************/
	/*** mLabalSplatScaleZ label and scrollbar ***/
	/*********************************************/
	mLabalSplatScaleZ = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalSplatScaleZ");
	mLabalSplatScaleZ->setProperty("FrameEnabled", "False");
	mLabalSplatScaleZ->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalSplatScaleZ->disable();
	// Center the text;
	mLabalSplatScaleZ->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalSplatScaleZ->setProperty("VertFormatting", "TopAligned");
	mLabalSplatScaleZ->setText( "3: " + phelper.uintToString( static_cast<unsigned int>(mSplatScales.z)) );


	mSplatScaleZ_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabScalesWin_mSplatScaleZ_Scrollbar");
	mSplatScaleZ_Scrollbar->setDocumentSize(DEF_MAX_SPLAT_SCALE + 1);
	mSplatScaleZ_Scrollbar->setScrollPosition( static_cast<unsigned int>(mSplatScales.z) );
	mSplatScaleZ_Scrollbar->setPageSize(1);
	mSplatScaleZ_Scrollbar->setStepSize(1);


	/*********************************************/
	/*** mLabalSplatScaleW label and scrollbar ***/
	/*********************************************/
	mLabalSplatScaleW = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalSplatScaleW");
	mLabalSplatScaleW->setProperty("FrameEnabled", "False");
	mLabalSplatScaleW->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalSplatScaleW->disable();
	// Center the text;
	mLabalSplatScaleW->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalSplatScaleW->setProperty("VertFormatting", "TopAligned");
	mLabalSplatScaleW->setText( "4: " + phelper.uintToString(static_cast<unsigned int>(mSplatScales.w)) );


	mSplatScaleW_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabScalesWin_mSplatScaleW_Scrollbar");
	mSplatScaleW_Scrollbar->setDocumentSize(DEF_MAX_SPLAT_SCALE + 1);
	mSplatScaleW_Scrollbar->setScrollPosition( static_cast<unsigned int>(mSplatScales.w) );
	mSplatScaleW_Scrollbar->setPageSize(1);
	mSplatScaleW_Scrollbar->setStepSize(1);


	/*************************************/
	/***  Create the wood frame mFrame ***/
	/*************************************/
	mFrame = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuSplatTabScalesWin_mFrame");
	mFrame->setProperty("FrameEnabled", "False");
	mFrame->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/*********************************/
	/*** mLabalDetailTextures label ***/
	/*********************************/
	mLabalDetailTextures = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalDetailTextures");
	mLabalDetailTextures->setProperty("FrameEnabled", "False");
	mLabalDetailTextures->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalDetailTextures->disable();
	// Center the text;
	mLabalDetailTextures->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalDetailTextures->setProperty("VertFormatting", "TopAligned");
	mLabalDetailTextures->setText("Detail Textures");


	/**********************************************/
	/*** mLabalDetailScaleX label and scrollbar ***/
	/**********************************************/
	mLabalDetailScaleX = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalDetailScaleX");
	mLabalDetailScaleX->setProperty("FrameEnabled", "False");
	mLabalDetailScaleX->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalDetailScaleX->disable();
	// Center the text;
	mLabalDetailScaleX->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalDetailScaleX->setProperty("VertFormatting", "TopAligned");
	mLabalDetailScaleX->setText( "1: " + phelper.uintToString( static_cast<unsigned int>(mDetailScales.x)) );


	mDetailScaleX_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabScalesWin_mDetailScaleX_Scrollbar");
	mDetailScaleX_Scrollbar->setDocumentSize(DEF_MAX_DETAIL_SCALE + 1);
	mDetailScaleX_Scrollbar->setScrollPosition( static_cast<unsigned int>(mDetailScales.x) );
	mDetailScaleX_Scrollbar->setPageSize(1);
	mDetailScaleX_Scrollbar->setStepSize(1);


	/**********************************************/
	/*** mLabalDetailScaleY label and scrollbar ***/
	/**********************************************/
	mLabalDetailScaleY = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabScalesWin_mLabalDetailScaleY");
	mLabalDetailScaleY->setProperty("FrameEnabled", "False");
	mLabalDetailScaleY->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mLabalDetailScaleY->disable();
	// Center the text;
	mLabalDetailScaleY->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mLabalDetailScaleY->setProperty("VertFormatting", "TopAligned");
	mLabalDetailScaleY->setText( "2: " + phelper.uintToString( static_cast<unsigned int>(mDetailScales.y)) );


	mDetailScaleY_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuSplatTabScalesWin_mDetailScaleY_Scrollbar");
	mDetailScaleY_Scrollbar->setDocumentSize(DEF_MAX_DETAIL_SCALE + 1);
	mDetailScaleY_Scrollbar->setScrollPosition( static_cast<unsigned int>(mDetailScales.y) );
	mDetailScaleY_Scrollbar->setPageSize(1);
	mDetailScaleY_Scrollbar->setStepSize(1);


	/*************************************/
	/***  Create the wood frame mFrame2 ***/
	/*************************************/
	mFrame2 = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuSplatTabScalesWin_mFrame2");
	mFrame2->setProperty("FrameEnabled", "False");
	mFrame2->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/**************************************/
	/*** Default and Close push Buttons ***/
	/**************************************/
	mButtonClose = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button",
		"TerrainMenuSplatTabScalesWin_mButtonClose");
	mButtonClose->setText("Close");
	mButtonClose->setHorizontalAlignment(CEGUI::HA_CENTRE);


	/******************/
	/*** The Events ***/
	/******************/


	mSplatScaleX_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabScalesWin::event_Scrollbar_change, this));

	mSplatScaleY_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabScalesWin::event_Scrollbar_change, this));

	mSplatScaleZ_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabScalesWin::event_Scrollbar_change, this));

	mSplatScaleW_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabScalesWin::event_Scrollbar_change, this));


	mDetailScaleX_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabScalesWin::event_Scrollbar_change, this));

	mDetailScaleY_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuSplatTabScalesWin::event_Scrollbar_change, this));


	mButtonClose->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&TerrainMenuSplatTabScalesWin::event_mButtonClose_click, this ) );


	/******************************/
	/*** The Size and Positions ***/
	/******************************/

	const float mMainWindow_x_size = 0.2f;
	const float mMainWindow_y_size = 0.6f;

	const float parentWindow_x_size = 0.935F;
	const float parentWindow_y_size = 0.92F;
	const float parentWindow_x_pos = 0.03;
	const float parentWindow_y_pos = 0.07F;


	const float start_x_pos = 0.07f;


	const float label_top_y_padding = 0.005f;
	const float label_x_size = 1.0f;
	const float label_y_size = 0.043f;

	const float scrollbar_top_y_padding = 0.005f;
	const float scrollbar_x_size = 0.86f;
	const float scrollbar_y_size = 0.047f;

	const float mFrame_y_size = 0.02f;

	const float mButtonClose_x_size = scrollbar_x_size;
	const float mButtonClose_y_size = 0.071F;



	const float mLabalSplatTextures_y_pos = 0.04f;

	const float mLabalSplatScaleX_y_pos = mLabalSplatTextures_y_pos + label_y_size + 0.03f;
	const float mSplatScaleX_Scrollbar_y_pos = mLabalSplatScaleX_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalSplatScaleY_y_pos = mSplatScaleX_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mSplatScaleY_Scrollbar_y_pos = mLabalSplatScaleY_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalSplatScaleZ_y_pos = mSplatScaleY_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mSplatScaleZ_Scrollbar_y_pos = mLabalSplatScaleZ_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalSplatScaleW_y_pos = mSplatScaleZ_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mSplatScaleW_Scrollbar_y_pos = mLabalSplatScaleW_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mFrame_y_pos = mSplatScaleW_Scrollbar_y_pos + scrollbar_y_size + 0.01f;

	const float mLabalDetailTextures_y_pos = mFrame_y_pos + mFrame_y_size + label_top_y_padding;

	const float mLabalDetailScaleX_y_pos = mLabalDetailTextures_y_pos + label_y_size + 0.03f;
	const float mDetailScaleX_Scrollbar_y_pos = mLabalDetailScaleX_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mLabalDetailScaleY_y_pos = mDetailScaleX_Scrollbar_y_pos + scrollbar_y_size + label_top_y_padding;
	const float mDetailScaleY_Scrollbar_y_pos = mLabalDetailScaleY_y_pos + label_y_size + scrollbar_top_y_padding;

	const float mFrame2_y_pos = mDetailScaleY_Scrollbar_y_pos + scrollbar_y_size + 0.01f;

	const float mButtonClose_y_pos = 1.0f - 0.12f;



	/*** Main window and parent window ***/
	mMainWindow->setSize(CEGUI::UVector2(CEGUI::UDim(mMainWindow_x_size, 0), CEGUI::UDim(mMainWindow_y_size, 0)));

	parentWindow->setSize(UVector2(UDim(parentWindow_x_size, 0.0f), UDim(parentWindow_y_size, 0.0f)));
	parentWindow->setPosition(UVector2(UDim(parentWindow_x_pos, 0.0f), UDim(parentWindow_y_pos, 0.0f)));


	/*** mLabalSplatTextures label ***/
	mLabalSplatTextures->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalSplatTextures->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalSplatTextures_y_pos, 0.0f)));


	/*** Alplha label and scrollbar ***/
	mLabalSplatScaleX->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalSplatScaleX->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalSplatScaleX_y_pos, 0.0f)));

	mSplatScaleX_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));
	mSplatScaleX_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mSplatScaleX_Scrollbar_y_pos, 0.0f)));


	/*** Red label and scrollbar ***/
	mLabalSplatScaleY->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalSplatScaleY->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalSplatScaleY_y_pos, 0.0f)));

	mSplatScaleY_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));
	mSplatScaleY_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mSplatScaleY_Scrollbar_y_pos, 0.0f)));


	/*** Green label and scrollbar ***/
	mLabalSplatScaleZ->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalSplatScaleZ->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalSplatScaleZ_y_pos, 0.0f)));

	mSplatScaleZ_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));
	mSplatScaleZ_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mSplatScaleZ_Scrollbar_y_pos, 0.0f)));


	/*** Blue label and scrollbar ***/
	mLabalSplatScaleW->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalSplatScaleW->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalSplatScaleW_y_pos, 0.0f)));

	mSplatScaleW_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));
	mSplatScaleW_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mSplatScaleW_Scrollbar_y_pos, 0.0f)));


	/*** mFrame ***/
	mFrame->setSize(UVector2(UDim(1.0f, 0.0f), UDim(mFrame_y_size, 0.0f)));
	mFrame->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mFrame_y_pos, 0.0f)));


	/*** mLabalDetailTextures label ***/
	mLabalDetailTextures->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalDetailTextures->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalDetailTextures_y_pos, 0.0f)));


	/*** mLabalDetailScaleX label and scrollbar ***/
	mLabalDetailScaleX->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalDetailScaleX->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalDetailScaleX_y_pos, 0.0f)));

	mDetailScaleX_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));
	mDetailScaleX_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mDetailScaleX_Scrollbar_y_pos, 0.0f)));


	/*** mLabalDetailScaleY label and scrollbar ***/
	mLabalDetailScaleY->setSize(UVector2(UDim(label_x_size, 0.0f), UDim(label_y_size, 0.0f)));
	mLabalDetailScaleY->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mLabalDetailScaleY_y_pos, 0.0f)));

	mDetailScaleY_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));
	mDetailScaleY_Scrollbar->setPosition(UVector2(UDim(start_x_pos, 0.0f), UDim(mDetailScaleY_Scrollbar_y_pos, 0.0f)));


	/*** mFrame2 ***/
	mFrame2->setSize(UVector2(UDim(1.0f, 0.0f), UDim(mFrame_y_size, 0.0f)));
	mFrame2->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mFrame2_y_pos, 0.0f)));


	/*** Close push Button ***/
	mButtonClose->setSize(CEGUI::UVector2(CEGUI::UDim(mButtonClose_x_size, 0), CEGUI::UDim(mButtonClose_y_size, 0)));
	mButtonClose->setYPosition( CEGUI::UDim(mButtonClose_y_pos, 0) );



	/********************/
	/*** The Settings ***/
	/********************/




	/***********************/
	/*** Add the windows ***/
	/***********************/

	parentWindow->addChildWindow(mLabalSplatTextures);

	parentWindow->addChildWindow(mLabalSplatScaleX);
	parentWindow->addChildWindow(mSplatScaleX_Scrollbar);

	parentWindow->addChildWindow(mLabalSplatScaleY);
	parentWindow->addChildWindow(mSplatScaleY_Scrollbar);

	parentWindow->addChildWindow(mLabalSplatScaleZ);
	parentWindow->addChildWindow(mSplatScaleZ_Scrollbar);

	parentWindow->addChildWindow(mLabalSplatScaleW);
	parentWindow->addChildWindow(mSplatScaleW_Scrollbar);

	parentWindow->addChildWindow(mFrame);

	parentWindow->addChildWindow(mLabalDetailTextures);

	parentWindow->addChildWindow(mLabalDetailScaleX);
	parentWindow->addChildWindow(mDetailScaleX_Scrollbar);

	parentWindow->addChildWindow(mLabalDetailScaleY);
	parentWindow->addChildWindow(mDetailScaleY_Scrollbar);

	parentWindow->addChildWindow(mFrame2);

	parentWindow->addChildWindow(mButtonClose);




	mMainWindow->addChildWindow(parentWindow);
	sheet->addChildWindow(mMainWindow);


	return true;
}

bool TerrainMenuSplatTabScalesWin::event_mButtonClose_click(const CEGUI::EventArgs &pEventArgs)
{
	mMainWindow->setVisible(false);

	return true;
}

bool TerrainMenuSplatTabScalesWin::event_Scrollbar_change(const CEGUI::EventArgs &pEventArgs)
{
	TerrainMenuSplatTabScalesWin::_updateScrollbars();

	return true;
}

void TerrainMenuSplatTabScalesWin::_updateScrollbars()
{
	EditManager* const editManager = EditManager::getSingletonPtr();
	TerrainManager* const terrainManager = editManager->getTerrainManager();
	SplatManager* splatManager = NULL;

	CEGUI::PropertyHelper phelper;

	string strScaleSplatX = "1: ";
	string strScaleSplatY =	"2: ";
	string strScaleSplatZ = "3: ";
	string strScaleSplatW = "4: ";

	string strScaleDetailX = "1: ";
	string strScaleDetailY = "2: ";


	if (terrainManager)
	{
		splatManager = terrainManager->getSplatManager();
	
		/***************************************/
		/*** Handle the texture splat update ***/
		/***************************************/
	
		// Get the scroll positions;
		const unsigned long tmpScaleSplatX = static_cast<const unsigned long>( mSplatScaleX_Scrollbar->getScrollPosition() );
		const unsigned long tmpScaleSplatY = static_cast<const unsigned long>( mSplatScaleY_Scrollbar->getScrollPosition() );
		const unsigned long tmpScaleSplatZ = static_cast<const unsigned long>( mSplatScaleZ_Scrollbar->getScrollPosition() );
		const unsigned long tmpScaleSplatW = static_cast<const unsigned long>( mSplatScaleW_Scrollbar->getScrollPosition() );
	
		// Append the scroll position to the string;
		strScaleSplatX.append( phelper.uintToString(tmpScaleSplatX).c_str() );
		strScaleSplatY.append( phelper.uintToString(tmpScaleSplatY).c_str() );
		strScaleSplatZ.append( phelper.uintToString(tmpScaleSplatZ).c_str() );
		strScaleSplatW.append( phelper.uintToString(tmpScaleSplatW).c_str() );
	
		// Set the label text;
		mLabalSplatScaleX->setText( strScaleSplatX );
		mLabalSplatScaleY->setText( strScaleSplatY );
		mLabalSplatScaleZ->setText( strScaleSplatZ );
		mLabalSplatScaleW->setText( strScaleSplatW );
	
		// Store the scrollbar values;
		mSplatScales.x = static_cast<float>(tmpScaleSplatX);
		mSplatScales.y = static_cast<float>(tmpScaleSplatY);
		mSplatScales.z = static_cast<float>(tmpScaleSplatZ);
		mSplatScales.w = static_cast<float>(tmpScaleSplatW);
	
	
		/****************************************/
		/*** Handle the texture Detail update ***/
		/****************************************/
	
		// Get the scroll positions;
		const unsigned long tmpScaleDetailX = static_cast<const unsigned long>( mDetailScaleX_Scrollbar->getScrollPosition() );
		const unsigned long tmpScaleDetailY = static_cast<const unsigned long>( mDetailScaleY_Scrollbar->getScrollPosition() );
	
	
		// Append the scroll position to the string;
		strScaleDetailX.append( phelper.uintToString(tmpScaleDetailX).c_str() );
		strScaleDetailY.append( phelper.uintToString(tmpScaleDetailY).c_str() );
	
	
		// Set the label text;
		mLabalDetailScaleX->setText( strScaleDetailX );
		mLabalDetailScaleY->setText( strScaleDetailY );
	
	
		// Store the scrollbar values;
		mDetailScales.x = static_cast<float>(tmpScaleDetailX);
		mDetailScales.y = static_cast<float>(tmpScaleDetailY);
	
	
		/************************************/
		/*** Set the SplatManagers scales ***/
		/************************************/
		splatManager->setSplatScales(mSplatScales);
		splatManager->setDetailScales(mDetailScales);
	}
}


