/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TERRAINMENU_H
#define TERRAINMENU_H


#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

#include "TerrainMenuBrushTabMain.h"
#include "TerrainMenuSplatTab.h"
#include "TerrainMenuBrushTabMiscTab.h"



class TerrainMenu
{
protected:
	CEGUI::FrameWindow* mTerrainMenu;
	CEGUI::TabControl* mTabControl;
	CEGUI::TabControl* mSubTabControl;

	TerrainMenuBrushTabMain* mTerrainMenuBrushTabMain;
	TerrainMenuSplatTab* mTerrainMenuSplatTab;
	TerrainMenuBrushTabMiscTab* mTerrainMenuBrushTabMiscTab;
public:
	TerrainMenu();
	~TerrainMenu();

	void toggle();
	bool isVisible();
	bool addChildWindow(bool showWindow);

	TerrainMenuBrushTabMain* const getTerrainMenuBrushTabMain();
	TerrainMenuSplatTab* const getTerrainMenuSplatTab();
}; 

#endif


