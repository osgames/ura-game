/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TerrainMenuBrushTabMiscTab.h"
#include "TerrainMenuBrushTabMain.h"


using namespace CEGUI;
using namespace std;



TerrainMenuBrushTabMiscTab::TerrainMenuBrushTabMiscTab()
{
	/*** Subtract the needed number by 1. The reason is because the scrollbar starts at 0 ***/
	mMaxIntensity = 100001;
	mMaxScale = 100001;

	mIntensity = 100;
	mScale = 15000;
}

void TerrainMenuBrushTabMiscTab::prepareToQuit()
{
	/*** Destroy the windows ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);
}

CEGUI::Window* TerrainMenuBrushTabMiscTab::getWindow()
{
	return mParentwin;
}

bool TerrainMenuBrushTabMiscTab::addChildWindow(TerrainMenuBrushTabMain *mainBrushTab)
{
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	CEGUI::PropertyHelper phelper;

	// Set the member pointer;
	mMainBrushTab = mainBrushTab;


	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","TerrainMenuBrushTabMiscTab_mParentwin");
	mParentwin->setText("Misc");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 


	/****************************************************************/
	/*** Create the terrain Max Displace Scrollbar and static text***/
	/****************************************************************/
	mMaxIntensityScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTabMiscTab_mMaxIntensityScrollbar");
	mMaxIntensityScrollbar->setDocumentSize(mMaxIntensity);
	mMaxIntensityScrollbar->setScrollPosition(mIntensity);
	mMaxIntensityScrollbar->setPageSize(1);
	mMaxIntensityScrollbar->setStepSize(1);
	

	// Set the scrollbar event to change the text;
	mMaxIntensityScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMiscTab::event_mMaxIntensityScrollbar_change, this));

	
	// Create the static text
	mMaxIntensityText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTabMiscTab_mMaxIntensityText");
	mMaxIntensityText->setProperty("FrameEnabled", "False");
	mMaxIntensityText->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mMaxIntensityText->disable();
	// Center the text;
	mMaxIntensityText->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mMaxIntensityText->setProperty("VertFormatting", "TopAligned"); 
	mMaxIntensityText->setText( "Max Displacement: " + phelper.uintToString( (unsigned int) mIntensity) );


	/*****************************************************/
	/*** Create the Max Scale Scrollbar and static text***/
	/*****************************************************/
	mMaxScaleScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"TerrainMenuBrushTabMiscTab_mMaxScaleScrollbar");
	mMaxScaleScrollbar->setDocumentSize(mMaxScale);
	mMaxScaleScrollbar->setScrollPosition(mScale);
	mMaxScaleScrollbar->setPageSize(1);
	mMaxScaleScrollbar->setStepSize(1);
	

	// Set the scrollbar event to change the text;
	mMaxScaleScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&TerrainMenuBrushTabMiscTab::event_mMaxScaleScrollbar_change, this));

	
	// Create the static text
	mMaxScaleText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuBrushTabMiscTab_mMaxScaleText");
	mMaxScaleText->setProperty("FrameEnabled", "False");
	mMaxScaleText->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mMaxScaleText->disable();
	// Center the text;
	mMaxScaleText->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mMaxScaleText->setProperty("VertFormatting", "TopAligned"); 
	mMaxScaleText->setText( "Max Brush Scale: " + phelper.uintToString( (unsigned int) mScale) );


	/*************************************************************/
	/***  Create the wood frame beneath the mMaxScaleScrollbar ***/
	/*************************************************************/
	mFrameBelowMaxBrushScaleScrollbar = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"TerrainMenuBrushTabMiscTab_mFrameBelowMaxBrushScaleScrollbar");
	mFrameBelowMaxBrushScaleScrollbar->setProperty("FrameEnabled", "False");
	mFrameBelowMaxBrushScaleScrollbar->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/*************************************/
	/*** create reload brushes button  ***/
	/*************************************/
	mApplyButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button", "TerrainMenuBrushTabMiscTab_mApplyButton");
	mApplyButton->setText("Apply");
	mApplyButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( &TerrainMenuBrushTabMiscTab::event_apply, this ) );


	/**********************************/
	/*** Set the size and positions ***/
	/**********************************/

	
	vector <float> start_y_pos(1, 0.021f);

	vector <float> frame_x_pos(1, 0.0f);
	vector <float> frame_x_size(1, 1.0f);
	vector <float> frame_y_size(1, 0.018f);
	vector <float> frame_bottom_padding(1, 0.004f);
	vector <float> frame_top_padding(1, 0.008f);

	vector <float> text_x_pos(1, 0.07f);
	vector <float> text_y_size(1, 0.06f);

	vector <float> scrollbar_top_y_padding(1, 0.031f);
	vector <float> scrollbar_x_pos(1, 0.07f);
	vector <float> scrollbar_x_size(1, 0.86f);
	vector <float> scrollbar_y_size(1, 0.027f);


	vector <float> mMaxIntensityText_y_pos( 1, start_y_pos.at(0) );
	vector <float> mMaxIntensityScrollbar_y_pos( 1, mMaxIntensityText_y_pos.at(0) + scrollbar_top_y_padding.at(0) );

	vector <float> mMaxScaleText_y_pos( 1, mMaxIntensityScrollbar_y_pos.at(0) + scrollbar_top_y_padding.at(0) );
	vector <float> mMaxScaleScrollbar_y_pos( 1, mMaxScaleText_y_pos.at(0) + scrollbar_top_y_padding.at(0) );


	vector <float> mFrameBelowMaxBrushScaleScrollbar_y_pos(1, mMaxScaleScrollbar_y_pos.at(0) + scrollbar_y_size.at(0) + frame_top_padding.at(0));


	vector <float> mApplyButton_y_pos(1, 0.93f);
	vector <float> mApplyButton_y_size(1, 0.04f);



	mMaxIntensityText->setPosition(UVector2(UDim(text_x_pos.at(0), 0.0f), UDim(mMaxIntensityText_y_pos.at(0), 0.0f)));
	mMaxIntensityText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size.at(0), 0.0f))); 

	mMaxIntensityScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos.at(0), 0.0f), UDim(mMaxIntensityScrollbar_y_pos.at(0), 0.0f)));
	mMaxIntensityScrollbar->setSize(UVector2(UDim(scrollbar_x_size.at(0), 0.0f), UDim(scrollbar_y_size.at(0), 0.0f))); 

	mMaxScaleText->setPosition(UVector2(UDim(text_x_pos.at(0), 0.0f), UDim(mMaxScaleText_y_pos.at(0), 0.0f)));
	mMaxScaleText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size.at(0), 0.0f))); 
	
	mMaxScaleScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos.at(0), 0.0f), UDim(mMaxScaleScrollbar_y_pos.at(0), 0.0f)));
	mMaxScaleScrollbar->setSize(UVector2(UDim(scrollbar_x_size.at(0), 0.0f), UDim(scrollbar_y_size.at(0), 0.0f))); 


	// Frame;
	mFrameBelowMaxBrushScaleScrollbar->setPosition(UVector2(UDim(frame_x_pos.at(0), 0.0f), UDim(mFrameBelowMaxBrushScaleScrollbar_y_pos.at(0), 0.0f)));
	mFrameBelowMaxBrushScaleScrollbar->setSize(UVector2(UDim(frame_x_size.at(0), 0.0f), UDim(frame_y_size.at(0), 0.0f))); 


	mApplyButton->setPosition(UVector2(UDim(scrollbar_x_pos.at(0), 0.0f), UDim(mApplyButton_y_pos.at(0), 0.0f)));
	mApplyButton->setSize(UVector2(UDim(scrollbar_x_size.at(0) + 0.01, 0.0f), UDim(mApplyButton_y_size.at(0), 0.0f))); 



	/***********************************************/
	/*** Attach the windows to the parent window ***/
	/***********************************************/
	mParentwin->addChildWindow( mMaxIntensityText );
	mParentwin->addChildWindow( mMaxIntensityScrollbar );

	mParentwin->addChildWindow( mMaxScaleText );
	mParentwin->addChildWindow( mMaxScaleScrollbar );

	mParentwin->addChildWindow( mFrameBelowMaxBrushScaleScrollbar );

	mParentwin->addChildWindow( mApplyButton );


	return true;
} 

bool TerrainMenuBrushTabMiscTab::event_mMaxIntensityScrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Max Displacement: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mMaxIntensityScrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '1';
	// if the scroll pos is at 0 then we set the scroll position to 1;
	if (scrollPos < 1 )
	{
		scrollPos++;
		 mMaxIntensityScrollbar->setScrollPosition(1);
	}

	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mMaxIntensityText->setText( tmpstr ); 

	// Set the class member;
	mIntensity = scrollPos;

	return true;
}

bool TerrainMenuBrushTabMiscTab::event_mMaxScaleScrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Max Brush Scale: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mMaxScaleScrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '1';
	// if the scroll pos is at 0 then we set the scroll position to 1;
	if (scrollPos < 1 )
	{
		scrollPos++;
		 mMaxScaleScrollbar->setScrollPosition(1);
	}

	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mMaxScaleText->setText( tmpstr ); 

	// Set the class member;
	mScale = scrollPos;

	return true;
}

bool TerrainMenuBrushTabMiscTab::event_apply(const CEGUI::EventArgs& pEventArgs)
{
	// We need to add +1 to the numbers because the scrollbar starts at 0;
	mMainBrushTab->setDisplaceDocumentSize(mIntensity + 1);
	mMainBrushTab->setScaleDocumentSize(mScale + 1);

	return true;
}


