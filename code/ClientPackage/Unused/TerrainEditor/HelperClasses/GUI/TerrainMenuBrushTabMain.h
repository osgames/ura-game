/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRAINMENU_BRUSHTAB_H
#define TERRAINMENU_BRUSHTAB_H


#include <vector>
#include <string>
#include <iostream>

#include <CEGUI/CEGUI.h>
#include <Ogre.h>

#include "CustomScrollPane.h"
#include "AccessFileSystem.h"



class TerrainMenuBrushTabMain
{
protected:
	CustomScrollPane* mScrollPane;

	CEGUI::Window* mParentwin;

	std::vector<std::string> mVbrushNames;
	std::vector<CEGUI::Imageset*> mVimgSetBrushes;
	std::vector <CEGUI::Window*> mVScrollPaneWindows;

	/*** The static text variables ***/
	Ogre::Real mMaxIntensity;	
	Ogre::Real mMaxScale;

	Ogre::Real mIntensityIncrease;
	Ogre::Real mIntensityDecrease;	// we add the minus sign when the functions are called;

	Ogre::Real mBrush_x_scale;
	Ogre::Real mBrush_y_scale;

	float mCameraDegree;
	unsigned long mAutoRepeatRate;
	

	/*** The Windows to add ***/
	CEGUI::Window* mLabelFlattenMode;
	CEGUI::RadioButton* mRadioFlattenPosition;
	CEGUI::RadioButton* mRadioFlattenCenter;
	CEGUI::RadioButton* mRadioFlattenBottom;

	CEGUI::Window* mFrameBelow_mRadioFlattenBottom;

	CEGUI::Window* mTerrainIncreaseText;
	CEGUI::Scrollbar* mTerrainIncreaseScrollbar;

	CEGUI::Window* mTerrainDecreaseText;	
	CEGUI::Scrollbar* mTerrainDecreaseScrollbar;

	CEGUI::Window* mBrush_x_scale_Text;
	CEGUI::Scrollbar* mBrush_x_scale_Scrollbar;

	CEGUI::Window* mBrush_y_scale_Text;
	CEGUI::Scrollbar* mBrush_y_scale_Scrollbar;

	CEGUI::Window* mFrameBelow_brushYScale_Scrollbar;

	CEGUI::Window* mCamDegreeLabel;
	CEGUI::Scrollbar* mCamDegree_Scrollbar;

	CEGUI::Window* mAutoRepeat_Text;
	CEGUI::Scrollbar* mAutoRepeat_Scrollbar;


	void _setMouseRate();
	void _reAdjustScrollbars();

	// The brush change function;
	void brushChange();	// This is called when a property needs changing;

	/*** The events ***/
	bool event_windowVisible(const CEGUI::EventArgs& pEventArgs);
	bool event_parent_resize(const CEGUI::EventArgs& pEventArgs);
	bool event_brushChange(const CEGUI::EventArgs& pEventArgs);
	bool event_mTerrainIncreaseScrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mTerrainDecreaseScrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mBrush_x_scale_Scrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mBrush_y_scale_Scrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mCamDegree_Scrollbar_Scrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mAutoRepeatRate_Scrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mUndoChanges_Scrollbar_change(const CEGUI::EventArgs& pEventArgs);
public:
	TerrainMenuBrushTabMain();
	~TerrainMenuBrushTabMain();

	bool isVisible();
	CEGUI::Window* getWindow();
	bool addChildWindow();

	void updateWindowShown();

	void terrainRaise();
	void terrainLower();
	void terrainFlatten();

	unsigned long getBrushCount();
	unsigned long getSelectedBrush();
	void setBrush(const unsigned long number);	
	void updateTerrainBrush();

	void setDisplaceDocumentSize(const unsigned long number);
	void setScaleDocumentSize(const unsigned long number);
}; 


#endif

 
