/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ModelMenuDotScene.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


ModelMenuDotScene::ModelMenuDotScene(EditManager *editMgr, ModelEditorMainTab *modelEditorMainTab)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool ModelMenuDotScene::addChildWindow()";
	string strLogMessage;


	/*** Allocate heap memory for these objects ***/
	mImportDotSceneTab = new ModelMenuImportDotSceneTab(editMgr);
	////mExportDotSceneTab = new ModelMenuExportDotSceneTab(editMgr);


	if (!mImportDotSceneTab)
	{
		strLogMessage = "mImportDotSceneTab";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	/*******
	if (!mExportDotSceneTab)
	{
		strLogMessage = "mExportDotSceneTab";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}
	*********/
}

ModelMenuDotScene::~ModelMenuDotScene()
{
	/*** Free the heap memory ***/
	if ( mImportDotSceneTab )
	{
		delete mImportDotSceneTab;
	}

	/***********
	if ( mExportDotSceneTab )
	{
		delete mExportDotSceneTab;
	}
	************/
}

CEGUI::Window* const ModelMenuDotScene::getWindow()
{
	return mParentwin;
}

/*** We Make a window that has two tabs for dotscene importing and exporting. We get that window and attach it to ModelMenu ***/
bool ModelMenuDotScene::addChildWindow()
{
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	TabControl* tabControl;


	/******************************/
	/*** Create the tab control ***/
	/******************************/
	tabControl = static_cast<TabControl*>(windowManager.createWindow("TaharezLook/TabControl", "ModelMenuDotScene_mTabControl"));
	tabControl->setTabHeight(UDim(0.03f, 9.05f));
	tabControl->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	tabControl->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f)));
	

	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage","ModelMenuDotScene_Main_mParentwin");
	mParentwin->setText("DotScene");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f)));


	/*********************************/
	/*** Create the child windows ****/
	/*********************************/

	if (mImportDotSceneTab)
	{
		mImportDotSceneTab->addChildWindow();
	}

	
	/*****************	
	if (mExportDotSceneTab)
	{
		mExportDotSceneTab->addChildWindow();
	}
	*****************/

	/********************/
	/*** Add the tabs ***/
	/********************/

	if (mImportDotSceneTab)
	{
		tabControl->addTab( mImportDotSceneTab->getWindow() );
	}

	/****************************************************************************** 
	// We take Take out the export DotScene Tab for now because of file creation issues. 
	// Later We might put it back.	
	if ( mExportDotSceneTab )
	{
		tabControl->addTab( mExportDotSceneTab->getWindow() );	
	}

	*******************************************************************************/


	/**************************/
	/*** Attach the windows ***/
	/**************************/

	// Add the tabControl to the ModelMenuDotScene;
	mParentwin->addChildWindow(tabControl);


	return true;
} 




