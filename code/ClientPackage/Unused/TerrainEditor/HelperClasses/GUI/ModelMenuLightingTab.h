/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MODEL_MENU_LIGHTING_TAB_H
#define MODEL_MENU_LIGHTING_TAB_H


/* 
*** 	NOTE: When Left clicking and adding a new model, sometimes the mouse up event isn't recieved.
***	 One reason could be from the long loading time of the model and the lowered fps.
 */


/****************/
/*** INCLUDES ***/
/****************/
#include <CEGUI/CEGUI.h>
#include <Ogre.h>
#include <vector>
#include <string>
#include <iostream>
#include <set>

#include "EditManager.h"
#include "CustomScrollPane.h"
#include "AccessFileSystem.h"
#include "GUIMessageBox.h"


/***************/
/*** DEFINES ***/
/***************/
#define MOUSE_RATE_ADD_INDEX 		0
#define MOUSE_RATE_EDIT_INDEX 		1



class ModelMenuLightingTab
{
protected:
	GUIMessageBox* mGuiMessageBox;
	EditManager* mEditManager;


	/*** This windows pointer ***/
	CEGUI::Window* mParentwin;


	/*** The static text variables and mouse rates ***/
	float mModelMoveDegree;
	std::vector<unsigned int> mVecMouseRate;
	float mCameraDegree;

	/*** The Windows to add ***/
	/* All Mode */
	CEGUI::Window* mLightingModeText;
	CEGUI::RadioButton* mRadioAddLighting;
	CEGUI::RadioButton* mRadioEditLighting;
	CEGUI::Window* mFrameBelowRadioDeleteLighting;

	CEGUI::Window* mMouseRateText;
	CEGUI::Scrollbar* mMouseRateScrollbar;

	/* Add Mode */

	CEGUI::Window* mAddLightingLabel;
	CEGUI::Editbox* mAddLightingEditboxName;
	CEGUI::RadioButton* mRadioAddLightingName;
	CEGUI::RadioButton* mRadioAddLightingAutoName;
	CEGUI::Window* mFrameBelowLightingAutoName;

	CEGUI::Window* mCamDegreeLabel;
	CEGUI::Scrollbar* mCamDegree_Scrollbar;

	/* Edit Mode */
	CEGUI::Window* mEditModeText;
	CEGUI::RadioButton* mRadioEditModeSelect;
	CEGUI::RadioButton* mRadioEditModeDeselect;
	CEGUI::RadioButton* mRadioDeleteLighting;
	CEGUI::RadioButton* mRadioCopy;
	CEGUI::RadioButton* mRadioX;
	CEGUI::RadioButton* mRadioY;
	CEGUI::RadioButton* mRadioZ;
	CEGUI::RadioButton* mRadioDrag;
	CEGUI::Window* mDiffuseColorText;
	CEGUI::Editbox* mDiffuseColorEditboxX;
	CEGUI::Editbox* mDiffuseColorEditboxY;
	CEGUI::Editbox* mDiffuseColorEditboxZ;
	CEGUI::Window* mSpecularColorText;
	CEGUI::Editbox* mSpecularColorEditboxX;
	CEGUI::Editbox* mSpecularColorEditboxY;
	CEGUI::Editbox* mSpecularColorEditboxZ;
	CEGUI::Window* mAttenuationText;
	CEGUI::Editbox* mAttenuationRangeEditbox;
	CEGUI::Editbox* mAttenuationConstantEditbox;
	CEGUI::Editbox* mAttenuationLinearEditbox;
	CEGUI::Window* mLightingApplyButton;

	CEGUI::Window* mFrameBelowTextSceneNodeName;

	CEGUI::Window* mScaleText;
	CEGUI::Editbox* mScaleEditboxX;
	CEGUI::Editbox* mScaleEditboxY;
	CEGUI::Editbox* mScaleEditboxZ;
	CEGUI::Window* mScaleApplyButton;

	CEGUI::Window* mFrameBelowApplyButton;

	CEGUI::Window* mModelMoveDegreeText;
	CEGUI::Scrollbar* mModelMoveDegreeScrollbar;

	void _setGuiMouseRate();
	void _setMouseRate();
	void _reAdjustScrollbars();

	void _radio_addModel_selected();
	void _radio_EditModel_selected();

	void updateLightingInformation();
	void eraseLightingInformation();
	void copyLightingInformation(const Ogre::Light* light);

	bool parseEditboxes(const std::vector<CEGUI::Editbox*>& editboxes, 
			    std::vector<Ogre::Real>& values) const;
	bool parseAndVerifyEditboxes(const std::vector<CEGUI::Editbox*>& editboxes, 
				     std::vector<Ogre::Real>& values);
	bool parseAndVerifyColorEditboxes(const std::vector<CEGUI::Editbox*>& editboxes, 
					  std::vector<Ogre::Real>& values);
	bool setDiffuseColor();
	bool setSpecularColor();
	bool setAttenuation();

	/*** The events ***/
	bool event_windowVisible(const CEGUI::EventArgs& pEventArgs);
	bool event_radio_modelMode_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mModelMoveDegreeScrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mCamDegree_Scrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mMouseRateScrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_scaleApply(const CEGUI::EventArgs& pEventArgs);
	bool event_lightingApply(const CEGUI::EventArgs& pEventArgs);

	// Prevent keyboard actions while in the editboxes;
	bool event_editBox_activated(const CEGUI::EventArgs& pEventArgs);
	bool event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs);
	bool event_sheet_mouseDown(const CEGUI::EventArgs& pEventArgs);



public:
	ModelMenuLightingTab();
	~ModelMenuLightingTab();

	bool addChildWindow();
	CEGUI::Window* const getWindow() const;
	bool isVisible();

	void updateWindowShown();

	void mouseLeftButtonDown();
	void mouseRightButtonDown();
};

#endif




