/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapMenuBar.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "GUIManager.h"
#include "GUI.h"


using namespace std;
using namespace CEGUI;


HeapMenuBar::HeapMenuBar() : mMenubarWindow(NULL)
{

}

HeapMenuBar::~HeapMenuBar()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapMenuBar::~HeapMenuBar()";
	string strLogMessage;


	if (mMenubarWindow)	
	{ 
		strLogMessage = "Deleted MenuBar";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);

		delete mMenubarWindow; 
		mMenubarWindow = NULL;
	}
}

void HeapMenuBar::toggle(vector <MenuBarDataType> *vDataTypes)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapMenuBar::toggle(...)";
	string strLogMessage;


	/*** If visible, then hide and delete the window and object. Else we create the object ***/

	if ( mMenubarWindow )	
	{
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		delete mMenubarWindow;
		mMenubarWindow = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted MenuBar";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mMenubarWindow = new MenuBar;
		
		if (!mMenubarWindow)
		{ 
			strLogMessage = "mMenubarWindow";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mMenubarWindow->addChildWindow(vDataTypes);

		strLogMessage = "Created MenuBar";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

CEGUI::Window* const HeapMenuBar::getWindow() const
{
	if (mMenubarWindow)
	{
		return mMenubarWindow->getWindow();
	}

	return NULL;
}

bool HeapMenuBar::isVisible() const
{
	if ( mMenubarWindow )
	{
		return mMenubarWindow->getWindow()->isVisible();
	}

	return false;
}

bool HeapMenuBar::enableParent(string &name)
{
	 return mMenubarWindow->enableParent(name);
}

bool HeapMenuBar::disableParent(string &name)
{
	 return mMenubarWindow->disableParent(name);
}

bool HeapMenuBar::enableChild(string &name)
{
	 return mMenubarWindow->enableChild(name);
}

bool HeapMenuBar::disableChild(string &name)
{
	 return mMenubarWindow->disableChild(name);
}






MenuBar::MenuBar()
{

}

MenuBar::~MenuBar()
{
	/*** Destroy the window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(menuBar);
}

CEGUI::Window* const MenuBar::getWindow() const
{
	return menuBar;
}

bool MenuBar::addChildWindow(vector <MenuBarDataType> *vDataTypes)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MenuBar::addChildWindow(...)";
	string strLogMessage;

	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::Window* popupContainer;
	CEGUI::Window* popup;

	unsigned int itemCounter = 0;





	
	try
	{
		/**************************************************/
		/*** Create the Menubar and set it to the sheet ***/
		/**************************************************/
		menuBar = windowManager.createWindow("TaharezLook/Menubar", "MenuBar::menuBar");
		menuBar->setArea(UDim(0,0),UDim(0,0),UDim(0.15,0), UDim(0.04,0) );
		menuBar->setAlwaysOnTop(true); // we want the menu on top
	
		// Add the Menubar to the sheet;
		sheet->addChildWindow(menuBar);
	
	
		/*** Add the items and text to the menubar ***/
	
		// First we loop Through the parents. Then we loop through the children. 
		// The itemCounter keeps incrementing and doesn't notice the for loop.
		// Thats important because only the children were added to the vector;
	
		for (unsigned int i = 0; i < vDataTypes->size(); i++)
		{
			popupContainer = windowManager.createWindow("TaharezLook/MenuItem");
			popupContainer->setText( *(vDataTypes->at(i).getParentName()) );
			menuBar->addChildWindow(popupContainer);
	
			// Create a popup and add it;
			popup = windowManager.createWindow("TaharezLook/PopupMenu");
			popupContainer->addChildWindow( popup );
	
			mParentMenuItems.push_back(popupContainer);

			// Add the child items;
			for (unsigned int j = 0; j < ( vDataTypes->at(i).getChildSize() ); j++)
			{
				// Add the vector item, set the text and event, and update the counter;
				mChildMenuItems.push_back( windowManager.createWindow("TaharezLook/MenuItem") );
	
				mChildMenuItems.at(itemCounter)->setText( *(vDataTypes->at(i).getChildName(j)) );
				mChildMenuItems.at(itemCounter)->subscribeEvent(CEGUI::MenuItem::EventClicked,
					Event::Subscriber(&MenuBar::event_menu_toggleButton, this));
	
				// Attach the menuItem to the popup;
				popup->addChildWindow( mChildMenuItems.at(itemCounter) );
	
				// Increase the counter;
				itemCounter++;
			}
		}
	
		// Set the datatypes member;
		mVdataTypes = vDataTypes;
	}
	catch(...)
	{
		strLogMessage = "An unknown error occurred";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}

	return true;
} 


bool MenuBar::event_menu_toggleButton(const CEGUI::EventArgs& pEventArgs)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MenuBar::event_menu_toggleButton(...)";
	string strLogMessage;

	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();

	CEGUI::Window* window = NULL;
	const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
	window = windowEventArgs.window;

	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int itemCounter = 0;



	// We compare pointers by simulating how the menuitems were added. First we loop
	// Through the parents. Then we loop through the children. 
	// The itemCounter keeps incrementing and doesn't notice the for loop.
	// Thats important because only the children were added to the vector;

	try
	{
		for (i = 0; i < mVdataTypes->size(); i++)
		{
			#ifdef MENUBAR_DEBUG
				cout << "parent: " << i << endl;
			#endif
	
			// Add the items;
			for (j = 0; j < ( mVdataTypes->at(i).getChildSize() ); j++)
			{
				#ifdef MENUBAR_DEBUG
					cout << "Child: " << itemCounter << endl;
				#endif
	
				// The itemCounter keeps incrementing and doesn't notice the for loop;
				if ( window == mChildMenuItems.at(itemCounter) )
				{
					#ifdef MENUBAR_DEBUG
						cout << "found a match: " << *(mVdataTypes->at(i).getChildName(j)) << endl << endl;
					#endif

					// Let the GUI know which window to toggle;
					gui->event_menu_toggleButton( mVdataTypes->at(i).getChildNumber(j) );


					return true;
				}
	
				itemCounter++;
			}
		}
	}
	catch(...)
	{
		strLogMessage = "An unknown error occurred";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
	}

	return false;
}


bool MenuBar::enableParent(string &name)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MenuBar::enableParent(...)";
	string strLogMessage;

	CEGUI::Window* window = NULL;
	string* parentName = NULL;


	try
	{
		for (unsigned int i = 0; i < mVdataTypes->size(); i++)
		{
			#ifdef MENUBAR_DEBUG
				cout << "parent: " << i << endl;
			#endif
	
			parentName = mVdataTypes->at(i).getParentName();

			// If we find a name match then we have the pointer. So enable the window and return true;
			if ( *parentName == name )
			{
				window = mParentMenuItems.at(i);

				window->enable();

				return true;
			}
		}
	}
	catch(...)
	{
		strLogMessage = "An unknown error occurred";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}

	return false;
}

bool MenuBar::disableParent(string &name)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MenuBar::disableParent(...)";
	string strLogMessage;

	CEGUI::Window* window = NULL;
	string* parentName = NULL;



	try
	{
		for (unsigned int i = 0; i < mVdataTypes->size(); i++)
		{
			#ifdef MENUBAR_DEBUG
				cout << "parent: " << i << endl;
			#endif
	
			parentName = mVdataTypes->at(i).getParentName();

			// If we find a name match then we have the pointer. So disable the window and return true;
			if ( *parentName == name )
			{
				window = mParentMenuItems.at(i);

				window->disable();

				return true;
			}
		}
	}
	catch(...)
	{
		strLogMessage = "An unknown error occurred";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);

		return false;
	}

	return false;
}

bool MenuBar::enableChild(string &name)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MenuBar::enableChild(...)";
	string strLogMessage;

	CEGUI::Window* window = NULL;

	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int itemCounter = 0;

	string* childName = NULL;


	// We compare pointers by simulating how the menuitems were added. First we loop
	// Through the parents. Then we loop through the children. 
	// The itemCounter keeps incrementing and doesn't notice the for loop.
	// Thats important because only the children were added to the vector;

	try
	{
		for (i = 0; i < mVdataTypes->size(); i++)
		{
			#ifdef MENUBAR_DEBUG
				cout << "parent: " << i << endl;
			#endif
	
		
			// Add the items;
			for (j = 0; j < ( mVdataTypes->at(i).getChildSize() ); j++)
			{
				#ifdef MENUBAR_DEBUG
					cout << "Child: " << itemCounter << endl;
				#endif
	
				childName = mVdataTypes->at(i).getChildName(j);

				if (name == *childName)
				{
					window = mChildMenuItems.at(itemCounter);
					window->enable();

					return true;
				}

				itemCounter++;
			}	
		}
	}
	catch(...)
	{
		strLogMessage = "An unknown error occurred";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
	}

	return false;

}

bool MenuBar::disableChild(string &name)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool MenuBar::disableChild(...)";
	string strLogMessage;

	CEGUI::Window* window = NULL;

	unsigned int i = 0;
	unsigned int j = 0;
	unsigned int itemCounter = 0;

	string* childName = NULL;


	// We compare pointers by simulating how the menuitems were added. First we loop
	// Through the parents. Then we loop through the children. 
	// The itemCounter keeps incrementing and doesn't notice the for loop.
	// Thats important because only the children were added to the vector;

	try
	{
		for (i = 0; i < mVdataTypes->size(); i++)
		{
			#ifdef MENUBAR_DEBUG
				cout << "parent: " << i << endl;
			#endif
	
		
			// Add the items;
			for (j = 0; j < ( mVdataTypes->at(i).getChildSize() ); j++)
			{
				#ifdef MENUBAR_DEBUG
					cout << "Child: " << itemCounter << endl;
				#endif
	
				childName = mVdataTypes->at(i).getChildName(j);

				if (name == *childName)
				{
					window = mChildMenuItems.at(itemCounter);
					window->disable();

					return true;
				}

				itemCounter++;
			}	
		}
	}
	catch(...)
	{
		strLogMessage = "An unknown error occurred";
		ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
	}

	return false;
}


