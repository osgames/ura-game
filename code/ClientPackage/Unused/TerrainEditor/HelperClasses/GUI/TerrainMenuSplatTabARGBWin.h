/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRAIN_MENU_SPLAT_TAB_ARGB_WIN_H
#define TERRAIN_MENU_SPLAT_TAB_ARGB_WIN_H


#include <CEGUI/CEGUI.h>
#include <Ogre.h>
#include <string>
#include <iostream>

#define DEF_MAX_COLOR_VALUE 255



class TerrainMenuSplatTabARGBWin
{
protected:
	CEGUI::FrameWindow* mMainWindow;

	CEGUI::Window* mLabalAlpha;
	CEGUI::Scrollbar* mLabalAlpha_Scrollbar;

	CEGUI::Window* mLabalRed;
	CEGUI::Scrollbar* mLabalRed_Scrollbar;

	CEGUI::Window* mLabalGreen;
	CEGUI::Scrollbar* mLabalGreen_Scrollbar;

	CEGUI::Window* mLabalBlue;
	CEGUI::Scrollbar* mLabalBlue_Scrollbar;

	CEGUI::PushButton* mButtonDefault;
	CEGUI::PushButton* mButtonClose;

	std::vector<Ogre::Vector4>* mvARGB;
	unsigned long mTextureNumber;

	void _updateScrollbarsText();

	bool event_Scrollbar_change(const CEGUI::EventArgs &pEventArgs);
	bool event_mButtonDefault_click(const CEGUI::EventArgs &pEventArgs);
	bool event_mButtonClose_click(const CEGUI::EventArgs &pEventArgs);
public:
	TerrainMenuSplatTabARGBWin();
	~TerrainMenuSplatTabARGBWin();
	bool addWindow();

	void show(const unsigned long textureNumber);
	void hide();
	bool isVisible();

	void reset();
	const std::vector<Ogre::Vector4>* const getARGB();
}; 


#endif

