/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MODELMENU_H
#define MODELMENU_H


#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

#include "EditManager.h"
#include "ModelEditorMainTab.h"
#include "ModelMenuLightingTab.h"
//#include "ModelMenuDotScene.h"



class ModelMenu
{
protected:
	CEGUI::FrameWindow* mObjectMenu;
	CEGUI::TabControl* mTabControl;
	CEGUI::TabControl* mSubTabControl;

	ModelEditorMainTab* mModelEditorMainTab;
	ModelMenuLightingTab* mModelMenuLightingTab;
	//ModelMenuDotScene* mModelMenuDotScene;
public:
	ModelMenu();
	~ModelMenu();
	void prepareToQuit();

	void toggle();
	bool isVisible();
	bool addChildWindow(bool showWindow);

	ModelEditorMainTab* const getModelEditorMainTab();
	ModelMenuLightingTab* const getModelMenuLightingTab();
}; 

#endif



