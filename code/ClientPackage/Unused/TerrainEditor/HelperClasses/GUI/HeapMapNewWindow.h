/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAPMAPNEWWINDOW_H
#define HEAPMAPNEWWINDOW_H



/****************/
/*** INCLUDES ***/
/****************/
#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

#include <GUIMessageBox.h>



class MapNewWindow
{
protected:
	GUIMessageBox* mMessagebox;

	CEGUI::FrameWindow* mLoadMapWin;
	CEGUI::Window* mMainChildwindow;

	CEGUI::Window* mNameLabel;
	CEGUI::Editbox* mNameEditbox;

	CEGUI::Window* mWidthLabel;
	CEGUI::Editbox* mWidthEditbox;

	CEGUI::Window* mHeightLabel;
	CEGUI::Editbox* mHeightEditbox;

	CEGUI::PushButton* mOkButton;
	CEGUI::PushButton* mCancelButton;

	bool event_button_ok_click(const CEGUI::EventArgs &e);
	bool event_mCancelButton_click(const CEGUI::EventArgs &pEventArgs);

	/* These two events make sure that we aren't using special keys when typing regular text */
	bool event_editBox_activated(const CEGUI::EventArgs &pEventArgs);
	bool event_editBox_deactivated(const CEGUI::EventArgs &pEventArgs);
public:
	MapNewWindow();
	~MapNewWindow();

	CEGUI::Window* const getWindow() const;
	bool addChildWindow();
}; 


class HeapMapNewWindow
{
protected:
	MapNewWindow* mNewMapWindow;
public:
	HeapMapNewWindow();
	~HeapMapNewWindow();
	
	CEGUI::Window* const getWindow() const;
	void toggle();
	bool isVisible() const;
}; 


#endif

