/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TerrainMenu.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace CEGUI;
using namespace std;



TerrainMenu::TerrainMenu()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "bool TerrainMenuBrushTabMain::addChildWindow()";
	string strLogMessage;


	mTerrainMenuBrushTabMain = new TerrainMenuBrushTabMain;
	mTerrainMenuSplatTab = new TerrainMenuSplatTab;
	mTerrainMenuBrushTabMiscTab = new TerrainMenuBrushTabMiscTab;	


	if (!mTerrainMenuBrushTabMain)
	{
		strLogMessage = "mTerrainMenuBrushTabMain";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mTerrainMenuSplatTab)
	{
		strLogMessage = "mTerrainMenuSplatTab";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mTerrainMenuBrushTabMiscTab)
	{
		strLogMessage = "mTerrainMenuBrushTabMiscTab";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);;
	}
}

TerrainMenu::~TerrainMenu()
{
	if (mTerrainMenuBrushTabMain)
	{
		delete mTerrainMenuBrushTabMain;
		mTerrainMenuBrushTabMain = NULL;
	}

	if (mTerrainMenuSplatTab)
	{
		delete mTerrainMenuSplatTab;
		mTerrainMenuSplatTab = NULL;
	}

	if (mTerrainMenuBrushTabMiscTab)
	{
		delete mTerrainMenuBrushTabMiscTab;
		mTerrainMenuBrushTabMiscTab = NULL;
	}


	/*** Destroy the cegui window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mTerrainMenu);
}

void TerrainMenu::toggle()
{
	if ( mTerrainMenu->isVisible() == false )
	{
		/*
		*** When the Main window is reshown we want the scrollbars like "Camera Rate" and "Mouse Rate"
		***  to readjust. Also we want to update anything we call the updateWindowShown() function;
		 */
		mTerrainMenuBrushTabMain->updateWindowShown();
		mTerrainMenuSplatTab->updateWindowShown();


		/*** Show and activate the mTerrainMenu window ***/
		mTerrainMenu->show();
		mTerrainMenu->activate();	
	}
	else if( mTerrainMenu->isVisible() == true )
	{
		mTerrainMenuSplatTab->updateWindowHidden();

		mTerrainMenu->hide();	
	}
}

bool TerrainMenu::isVisible()
{
	return mTerrainMenu->isVisible();
}

bool TerrainMenu::addChildWindow(bool showWindow = true)
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	float terrainMenu_x_size = 0.21F;
	float terrainMenu_y_size = 1.0F;
	float terrainMenu_x_pos = 1.0F - terrainMenu_x_size;


	/*** Create the main terrain window ***/
	mTerrainMenu = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "TerrainMenu_terrainMenu");
	mTerrainMenu->setPosition( CEGUI::UVector2(CEGUI::UDim(terrainMenu_x_pos, 0.0F), CEGUI::UDim(0.0F, 0.0F) ) );
	mTerrainMenu->setSize(CEGUI::UVector2(CEGUI::UDim(terrainMenu_x_size, 0.0F), CEGUI::UDim(terrainMenu_y_size, 0.0F)));
	mTerrainMenu->setText("Terrain Editor");


	/*** Create the tab control ***/
	mTabControl = static_cast<TabControl*>(windowManager.createWindow("TaharezLook/TabControl", "TerrainMenu_mTabControl"));
	mTabControl->setTabHeight(UDim(0.03f, 9.05f));
	mTabControl->setPosition( CEGUI::UVector2(CEGUI::UDim(0.06, 0.0F), CEGUI::UDim(0.04F, 0.0F) ) );
	mTabControl->setSize(CEGUI::UVector2(CEGUI::UDim(0.88F, 0.0F), CEGUI::UDim(0.944F, 0.0F)));




	CEGUI::Window* MainBrushWindow = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage","TerrainMenu_MainBrushWindow");
	MainBrushWindow->setText("Brush");
	MainBrushWindow->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	MainBrushWindow->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 



	// Create the sub tab control;
	mSubTabControl = static_cast<TabControl*>(windowManager.createWindow("TaharezLook/TabControl", "TerrainMenu_mSubTabControl"));
	mSubTabControl->setTabHeight(UDim(0.03f, 9.05f));
	mSubTabControl->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f)));
	mSubTabControl->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 


	/****************************************************************/
	/*** Create the child windows and add the windows to the tab ****/
	/****************************************************************/

	// Create the mTerrainMenuBrushTabMain tab on the mSubTabControl;
	if ( mTerrainMenuBrushTabMain )
	{
		mTerrainMenuBrushTabMain->addChildWindow();
		mSubTabControl->addTab( mTerrainMenuBrushTabMain->getWindow() );
	}

	// Create the mTerrainMenuBrushTabMain tab on the mSubTabControl;
	if ( mTerrainMenuSplatTab )
	{
		mTerrainMenuSplatTab->addChildWindow();
		mSubTabControl->addTab( mTerrainMenuSplatTab->getWindow() );
	}

	// Create the mTerrainMenuBrushTabMiscTab tab on the mSubTabControl;
	if ( mTerrainMenuBrushTabMiscTab )
	{
		mTerrainMenuBrushTabMiscTab->addChildWindow(mTerrainMenuBrushTabMain);
		mSubTabControl->addTab( mTerrainMenuBrushTabMiscTab->getWindow() );
	}



	/************************/
	/*** Set the Settings ***/
	/************************/

	// Determine if we need to hide the window;
	if ( !showWindow )
	{
		mTerrainMenu->hide();
	}


	/**************************/
	/*** Attach the windows ***/
	/**************************/
	MainBrushWindow->addChildWindow( mSubTabControl );
	mTabControl->addChildWindow( MainBrushWindow );


	// Add the mTabControl to the TerrainMenu;
	mTerrainMenu->addChildWindow( mTabControl );


	// Add the mTerrainMenu window to the sheet;
	sheet->addChildWindow( mTerrainMenu );


	return true;
} 

TerrainMenuBrushTabMain* const TerrainMenu::getTerrainMenuBrushTabMain()
{
	return mTerrainMenuBrushTabMain;
}

TerrainMenuSplatTab* const TerrainMenu::getTerrainMenuSplatTab()
{
	return mTerrainMenuSplatTab;
}

