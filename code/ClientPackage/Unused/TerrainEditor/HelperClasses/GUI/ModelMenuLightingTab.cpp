/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ModelMenuLightingTab.h"
#include "WorldEditorProperties.h"
#include "Ogre3dManager.h"
#include "CeguiManager.h"
#include "CameraManager.h"
#include "CustomSceneNodeManager.h"
#include "CustomSceneNodeManipulator.h"
#include "RayCastManager.h"
#include "GUIManager.h"
#include "EditManager.h"
#include <cstdio>

using namespace std;
using namespace CEGUI;
using namespace Ogre;


ModelMenuLightingTab::ModelMenuLightingTab()
{
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	mGuiMessageBox = NULL;
	mModelMoveDegree = 1.0f;


	/*** Alocate heap memory for the GuiMessageBox ***/
	mGuiMessageBox = new GUIMessageBox;

	if ( !mGuiMessageBox )
	{
		string tmp = "ERROR: ModelMenuLightingTab::ModelMenuLightingTab(EditManager *editMgr): mGuiMessageBox could not allocate heap memory";
		Ogre::LogManager::getSingleton().logMessage( tmp );
	}


	/***
		Push back the starting 2 vectors of mouse rate: Add and edit.
		Edit is slower to due dragging needing to me slower.
 	***/
	mVecMouseRate.push_back(150);
	mVecMouseRate.push_back(80);


	/*** get the Camera move Degree rate ***/
	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();

}

ModelMenuLightingTab::~ModelMenuLightingTab()
{
	if (mGuiMessageBox)
	{
		delete mGuiMessageBox;
		mGuiMessageBox = 0;
	}


	/*** Destroy the windows ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);
}

CEGUI::Window* const ModelMenuLightingTab::getWindow() const
{
	return mParentwin;
}

bool ModelMenuLightingTab::addChildWindow()
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::PropertyHelper phelper;

	// Create the GuiMessageBox and attach it to the sheet;
	mGuiMessageBox->addWindow();



	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelMenuLightingTab_Main_mParentwin");
	mParentwin->setText("Lighting");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f)));
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f)));

	mParentwin->subscribeEvent( CEGUI::Window::EventShown, CEGUI::Event::Subscriber(
		&ModelMenuLightingTab::event_windowVisible, this ) );


	/****************************************************************************************/
	/*** create the mLightingModeText text, mesh Add, mesh edit and mesh delete radio buttons ***/
	/****************************************************************************************/

	/*** Create the static text ***/
	mLightingModeText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mLightingModeText");
	mLightingModeText->setProperty("FrameEnabled", "False");
	mLightingModeText->setProperty("BackgroundEnabled", "false");

	// Disable the mLightingModeText;
	mLightingModeText->disable();
	// Center the text;
	mLightingModeText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mLightingModeText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mLightingModeText->setText( "Lighting Mode:" );


	mRadioAddLighting = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioAddLighting"));
	mRadioAddLighting->setGroupID(1);
	mRadioAddLighting->setText("Add");

	mRadioEditLighting = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioEditLighting"));
	mRadioEditLighting->setGroupID(1);
	mRadioEditLighting->setText("Edit");


	/*****************************************************************/
	/***  Create the wood frame beneath the mRadioDeleteLighting radio ***/
	/*****************************************************************/
	mFrameBelowRadioDeleteLighting = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelMenuLightingTab_mFrameBelowRadioDeleteLighting");
	mFrameBelowRadioDeleteLighting->setProperty("FrameEnabled", "False");
	mFrameBelowRadioDeleteLighting->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/**************************************************************************************/
	/*** Create the mEditModeText text and radios rotate, roll, pitch, x, y, z and drag ***/
	/**************************************************************************************/

	/*** Create the static text ***/
	mEditModeText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mEditModeText");
	mEditModeText->setProperty("FrameEnabled", "False");
	mEditModeText->setProperty("BackgroundEnabled", "false");

	// Disable the mEditModeText;
	mEditModeText->disable();
	// Center the text;
	mEditModeText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mEditModeText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mEditModeText->setText( "Edit Mode:" );


	/*** select and deselect radios ***/
	mRadioEditModeSelect = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelMenuLightingTab_mRadioEditModeSelect"));
	mRadioEditModeSelect->setGroupID(2);
	mRadioEditModeSelect->setText("Select");

	mRadioEditModeDeselect = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelMenuLightingTab_mRadioEditModeDeselect"));
	mRadioEditModeDeselect->setGroupID(2);
	mRadioEditModeDeselect->setText("Deselect");


	/*** Delete Lighting Radio Button ***/
	mRadioDeleteLighting = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioDeleteLighting"));
	mRadioDeleteLighting->setGroupID(2);
	mRadioDeleteLighting->setText("Delete");

	/*** Copy Radio Button ***/
	mRadioCopy = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioCopy"));
	mRadioCopy->setGroupID(2);
	mRadioCopy->setText("Copy");

	/*** Radios x, y and z ***/
	mRadioX = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioX"));
	mRadioX->setGroupID(2);
	mRadioX->setText("X");

	mRadioY = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioY"));
	mRadioY->setGroupID(2);
	mRadioY->setText("Y");

	mRadioZ = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioZ"));
	mRadioZ->setGroupID(2);
	mRadioZ->setText("Z");

	/*** radio drag ***/
	mRadioDrag = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelMenuLightingTab_mRadioDrag"));
	mRadioDrag->setGroupID(2);
	mRadioDrag->setText("Drag");


	/*** Create the static text ***/
	mDiffuseColorText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mDiffuseColorText");
	mDiffuseColorText->setProperty("FrameEnabled", "False");
	mDiffuseColorText->setProperty("BackgroundEnabled", "false");

	// Disable the mDiffuseColorText;
	mDiffuseColorText->disable();
	// Center the text;
	mDiffuseColorText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mDiffuseColorText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mDiffuseColorText->setText( "Diffuse Color RGB" );

	mDiffuseColorEditboxX = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mDiffuseColorEditboxX");
	mDiffuseColorEditboxY = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mDiffuseColorEditboxY");
	mDiffuseColorEditboxZ = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mDiffuseColorEditboxZ");


	/*** Create the static text ***/
	mSpecularColorText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mSpecularColorText");
	mSpecularColorText->setProperty("FrameEnabled", "False");
	mSpecularColorText->setProperty("BackgroundEnabled", "false");

	// Disable the mSpecularColorText;
	mSpecularColorText->disable();
	// Center the text;
	mSpecularColorText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mSpecularColorText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mSpecularColorText->setText( "Specular Color RGB" );

	mSpecularColorEditboxX = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mSpecularColorEditboxX");
	mSpecularColorEditboxY = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mSpecularColorEditboxY");
	mSpecularColorEditboxZ = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mSpecularColorEditboxZ");

	/*** Create the static text ***/
	mAttenuationText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mAttenuationText");
	mAttenuationText->setProperty("FrameEnabled", "False");
	mAttenuationText->setProperty("BackgroundEnabled", "false");

	// Disable the mAttenuationText;
	mAttenuationText->disable();
	// Center the text;
	mAttenuationText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mAttenuationText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mAttenuationText->setText( "Atten.: Range, Const., Linear" );

	mAttenuationRangeEditbox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mAttenuationRangeEditbox");
	mAttenuationConstantEditbox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mAttenuationConstantEditbox");
	mAttenuationLinearEditbox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mAttenuationLinearEditbox");
	mLightingApplyButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button", "ModelMenuLightingTab_mLightingApplyButton");
	mLightingApplyButton->setText("Apply Lighting");
	/*************************************************************************************/
	/*** create the text scenenode name and frame below the mFrameBelowTextSceneNodeName radio ***/
	/**************************************************************************************/


	/*** Create the frame ***/
	mFrameBelowTextSceneNodeName = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelMenuLightingTab_mFrameBelowRadioDrag");
	mFrameBelowTextSceneNodeName->setProperty("FrameEnabled", "False");
	mFrameBelowTextSceneNodeName->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************************************************/
	/*** create the Scale text, the editboxes for x, y and z and the apply button ***/
	/********************************************************************************/

	/*** Create the static text ***/
	mScaleText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mScaleText");
	mScaleText->setProperty("FrameEnabled", "False");
	mScaleText->setProperty("BackgroundEnabled", "false");

	// Disable the mScaleText;
	mScaleText->disable();
	// Center the text;
	mScaleText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mScaleText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mScaleText->setText( "Scale Percent:  x, y, z" );


	mScaleEditboxX = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mScaleEditboxX");
	mScaleEditboxY = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mScaleEditboxY");
	mScaleEditboxZ = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mScaleEditboxZ");
	mScaleApplyButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button", "ModelMenuLightingTab_mScaleApplyButton");
	mScaleApplyButton->setText("Ok");

	/************************************************************/
	/***  Create the wood frame beneath the mScaleApplyButton ***/
	/************************************************************/
	mFrameBelowApplyButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelMenuLightingTab_mFrameBelowApplyButton");
	mFrameBelowApplyButton->setProperty("FrameEnabled", "False");
	mFrameBelowApplyButton->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/*************************************************/
	/*** create the Move degree text and scrollbar ***/
	/*************************************************/

	/*** Create the static text ***/
	mModelMoveDegreeText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mModelMoveDegreeText");
	mModelMoveDegreeText->setProperty("FrameEnabled", "False");
	mModelMoveDegreeText->setProperty("BackgroundEnabled", "false");

	// Disable the mMouseRateText;
	mModelMoveDegreeText->disable();
	// Center the text;
	mModelMoveDegreeText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mModelMoveDegreeText->setProperty("VertFormatting", "TopAligned");
	mModelMoveDegreeText->setText( "Model Rate: " + phelper.floatToString(mModelMoveDegree) );


	mModelMoveDegreeScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"ModelMenuLightingTab_Main_mMoveDegreeScrollbar");
	mModelMoveDegreeScrollbar->setDocumentSize(201);
	mModelMoveDegreeScrollbar->setScrollPosition(mModelMoveDegree);
	mModelMoveDegreeScrollbar->setPageSize(1);
	mModelMoveDegreeScrollbar->setStepSize(0.001f);

	// Set the scrollbar event to change the text;
	mModelMoveDegreeScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&ModelMenuLightingTab::event_mModelMoveDegreeScrollbar_change, this));

	/***************************************************************/
	/*** create the mouse rate text and the mouse rate scrollbar ***/
	/***************************************************************/

	/*** Create the static text ***/
	mMouseRateText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_ mMouseRateText");
	mMouseRateText->setProperty("FrameEnabled", "False");
	mMouseRateText->setProperty("BackgroundEnabled", "false");

	// Disable the mMouseRateText;
	mMouseRateText->disable();
	// Center the text;
	mMouseRateText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mMouseRateText->setProperty("VertFormatting", "TopAligned");
	mMouseRateText->setText( "Mouse Rate: " + phelper.uintToString(mVecMouseRate.at(0)) );


	mMouseRateScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"ModelMenuLightingTab_Main_mMouseRateScrollbar");
	mMouseRateScrollbar->setDocumentSize(201);
	mMouseRateScrollbar->setScrollPosition( mVecMouseRate.at(0) );
	mMouseRateScrollbar->setPageSize(1);
	mMouseRateScrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mMouseRateScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&ModelMenuLightingTab::event_mMouseRateScrollbar_change, this));

	/******************************************************************************/
	/*** Create the mAddLightingLabel, Editbox, Radios "name" "auto name" and frame ***/
	/******************************************************************************/

	// mAddLightingLabel Label;
	mAddLightingLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mAddLightingLabel");
	mAddLightingLabel->setProperty("FrameEnabled", "False");
	mAddLightingLabel->setProperty("BackgroundEnabled", "false");
	mAddLightingLabel->setText("Model Name:");	// Set the text;
	mAddLightingLabel->disable();

	// Create the edit box;
	mAddLightingEditboxName = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuLightingTab_mAddLightingEditboxName");

	// Radio name;
	mRadioAddLightingName = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelMenuLightingTab_mRadioAddLightingName"));
	mRadioAddLightingName->setGroupID(3);
	mRadioAddLightingName->setText("Name");

	// Radio auto name;
	mRadioAddLightingAutoName = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelMenuLightingTab_mRadioAddLightingAutoName"));
	mRadioAddLightingAutoName->setGroupID(3);
	mRadioAddLightingAutoName->setText("Auto Name");

	// Frame;
	mFrameBelowLightingAutoName = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelMenuLightingTab_mFrameBelowLightingAutoName");
	mFrameBelowLightingAutoName->setProperty("FrameEnabled", "False");
	mFrameBelowLightingAutoName->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************************************************/
	/*** Create the mCamDegree_Scrollbar scrollbar and mCamDegreeLabel text label ***/
	/********************************************************************************/
	mCamDegree_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"ModelMenuLightingTab_mCamDegree_Scrollbar");
	mCamDegree_Scrollbar->setDocumentSize(201);
	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);
	mCamDegree_Scrollbar->setPageSize(1);
	mCamDegree_Scrollbar->setStepSize(0.001f);

	// Set the scrollbar event to change the text;
	mCamDegree_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&ModelMenuLightingTab::event_mCamDegree_Scrollbar_change, this));

	
	// Create the static text
	mCamDegreeLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelMenuLightingTab_mCamDegreeLabel");
	mCamDegreeLabel->setProperty("FrameEnabled", "False");
	mCamDegreeLabel->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mCamDegreeLabel->disable();
	// Center the text;
	mCamDegreeLabel->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mCamDegreeLabel->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mCamDegreeLabel->setText( "Camera Rate: " + phelper.floatToString(mCameraDegree) );



	/*************************************/
	/*** Setup the sizes and positions ***/
	/*************************************/

	// Frame stuff;
	float frame_x_pos = 0.00f;
	float frame_x_size = 1.0f;
	float frame_y_size = 0.018f;
	float frame_bottom_padding = 0.019f;
	float frame_top_padding = 0.05f;

	// text stuff
	float text_top_padding = 0.035f;
	float text_x_pos = 0.07f;
	float text_y_size = 0.06f;

	// Scrollbar stuff
	float scrollbar_x_pos = 0.07f;
	float scrollbar_x_size = 0.86f;
	float scrollbar_y_size = 0.027f;

	// radio Stuff;
	float radio_y_padding = 0.04f;
	float radio_y_size = 0.05f;
	float radio_x_pos = 0.07f;

	// editbox Stuff:
	float editbox_y_padding = 0.035f;

	// scaling stuf;
	float mScaleEditbox_x_size = 0.23f;
	float mScaleEditbox_y_size = 0.04f;
	float mScaleEditbox_x_padding = 0.05f;

	float mScaleEditboxX_x_pos = 0.1f;
	float mScaleEditboxY_x_pos = mScaleEditboxX_x_pos + mScaleEditbox_x_size + mScaleEditbox_x_padding;
	float mScaleEditboxZ_x_pos = mScaleEditboxY_x_pos + mScaleEditbox_x_size + mScaleEditbox_x_padding;

	float mScaleApplyButton_x_size = 0.805f;
	float mScaleApplyButton_y_size = 0.04f;
	float mScaleApplyButton_x_pos = mScaleEditboxX_x_pos;

	float mLightingModeText_y_pos = 0.02f;
	float mRadioAddLighting_y_pos = mLightingModeText_y_pos + 0.03f;

	float mFrameBelowRadioDeleteLighting_y_pos = mRadioAddLighting_y_pos + frame_top_padding;

	// Lighting listbox;
	float mLightingListbox_x_pos = 0.1f;
	float mLightingListbox_y_pos = mFrameBelowRadioDeleteLighting_y_pos + 0.038f;
	float mLightingListbox_x_size = 0.8f;
	float mLightingListbox_y_size = 0.46f;

	// Lighting listbox refresh button;
	float mAddLightingRefreshListboxButton_y_pos = mLightingListbox_y_pos + mLightingListbox_y_size + 0.015f;
	float mFrameBelowRefreshListboxButton_y_pos = mAddLightingRefreshListboxButton_y_pos + 0.04f +  0.015f;

	float mEditModeText_y_pos = mFrameBelowRadioDeleteLighting_y_pos + 0.025f;
	float mRadioEditModeSelect_y_pos = mEditModeText_y_pos + 0.025f;
	float mRadioDeleteLighting_y_pos = mRadioEditModeSelect_y_pos + radio_y_padding;
	float mRadioX_y_pos = mRadioDeleteLighting_y_pos + radio_y_padding;
	float mRadioDrag_y_pos = mRadioX_y_pos + radio_y_padding;
	float mDiffuseColorText_y_pos = mRadioDrag_y_pos + radio_y_padding;
	float mDiffuseColorEditboxX_y_pos = mDiffuseColorText_y_pos + editbox_y_padding;
	float mSpecularColorText_y_pos = mDiffuseColorEditboxX_y_pos + radio_y_padding;
	float mSpecularColorEditboxX_y_pos = mSpecularColorText_y_pos + editbox_y_padding;
	float mAttenuationText_y_pos = mSpecularColorEditboxX_y_pos + radio_y_padding;
	float mAttenuationRangeEditbox_y_pos = mAttenuationText_y_pos + editbox_y_padding;
	float mLightingApplyButton_y_pos = mAttenuationRangeEditbox_y_pos + radio_y_padding + 0.01f;

	float mTextSceneNodeName_y_pos = 0.545f;
	float mFrameBelowTextSceneNodeName_y_pos = mTextSceneNodeName_y_pos + 0.05f;

	float mScaleText_y_pos = mFrameBelowTextSceneNodeName_y_pos + frame_bottom_padding;
	float mScaleEditboxX_y_pos = mScaleText_y_pos + text_top_padding;
	float mScaleApplyButton_y_pos = mScaleEditboxX_y_pos + mScaleEditbox_y_size + 0.01f;

	float mFrameBelowApplyButton_y_pos = mScaleApplyButton_y_pos + frame_top_padding;

	// Move degree;
	float mMoveDegreeText_y_pos = mFrameBelowApplyButton_y_pos + 0.025f;
	float mMoveDegreeScrollbar_y_pos = mMoveDegreeText_y_pos + text_top_padding;

	// Mouse Rate;
	float mMouseRateText_y_pos = 0.906f;
	float mMouseRateScrollbar_y_pos = mMouseRateText_y_pos + text_top_padding;

	// Add Mode - naming;
	float mAddLightingLabel_y_pos = mFrameBelowRefreshListboxButton_y_pos + 0.020f;
	float mAddLightingEditboxName_y_pos = mAddLightingLabel_y_pos + 0.04f;
	float mRadioAddLightingName_y_pos = mAddLightingEditboxName_y_pos + radio_y_padding;
	float mFrameBelowRadioAddLightingName_y_pos = mRadioAddLightingName_y_pos + 0.048f;

	// Add Mode - Cam Degree;
	float mCamDegreeLabel_y_pos = mFrameBelowRadioAddLightingName_y_pos + 0.025f;
	float mCamDegree_Scrollbar_y_pos = mCamDegreeLabel_y_pos + text_top_padding;


	float tmpXPosition = 0.0f;




	/*** All mode ***/
	mLightingModeText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mLightingModeText_y_pos, 0.0f)));
	mLightingModeText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));

	mRadioAddLighting->setPosition(UVector2(UDim(0.25f, 0.0f), UDim(mRadioAddLighting_y_pos, 0.0f)));
	mRadioAddLighting->setSize(UVector2(UDim(0.25f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioAddLighting->getXPosition().d_scale + mRadioAddLighting->getWidth().d_scale + 0.02f;
	mRadioEditLighting->setPosition(UVector2(UDim(tmpXPosition, 0.0f),UDim(mRadioAddLighting_y_pos, 0.0f)));
	mRadioEditLighting->setSize(UVector2(UDim(0.23f, 0.0f), UDim(radio_y_size, 0.0f)));

	// Frame;
	mFrameBelowRadioDeleteLighting->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowRadioDeleteLighting_y_pos, 0.0f)));
	mFrameBelowRadioDeleteLighting->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Mouse rate
	mMouseRateText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mMouseRateText_y_pos, 0.0f)));
	mMouseRateText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mMouseRateScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mMouseRateScrollbar_y_pos, 0.0f)));
	mMouseRateScrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));


	// Add Mode - naming;
	mAddLightingLabel->setPosition(UVector2(UDim(mLightingListbox_x_pos, 0.0f), UDim(mAddLightingLabel_y_pos, 0.0f)));
	mAddLightingLabel->setSize(UVector2(UDim(mLightingListbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mAddLightingEditboxName->setPosition(UVector2(UDim(mLightingListbox_x_pos, 0.0f), UDim(mAddLightingEditboxName_y_pos, 0.0f)));
	mAddLightingEditboxName->setSize(UVector2(UDim(mLightingListbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mRadioAddLightingName->setPosition(UVector2(UDim(mLightingListbox_x_pos, 0.0f), UDim(mRadioAddLightingName_y_pos, 0.0f)));
	mRadioAddLightingName->setSize(UVector2(UDim(0.32f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioAddLightingName->getXPosition().d_scale + mRadioAddLightingName->getWidth().d_scale + 0.02f;
	mRadioAddLightingAutoName->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioAddLightingName_y_pos, 0.0f)));
	mRadioAddLightingAutoName->setSize(UVector2(UDim(0.7f, 0.0f), UDim(radio_y_size, 0.0f)));

	mFrameBelowLightingAutoName->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowRadioAddLightingName_y_pos, 0.0f)));
	mFrameBelowLightingAutoName->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Camera Degree;
	mCamDegreeLabel->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mCamDegreeLabel_y_pos, 0.0f)));
	mCamDegreeLabel->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mCamDegree_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mCamDegree_Scrollbar_y_pos, 0.0f)));
	mCamDegree_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	/*** Edit mode ***/
	mEditModeText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mEditModeText_y_pos, 0.0f)));
	mEditModeText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mRadioEditModeSelect->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioEditModeSelect_y_pos, 0.0f)));
	mRadioEditModeSelect->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioEditModeSelect->getXPosition().d_scale + mRadioEditModeSelect->getWidth().d_scale + 0.02f;
	mRadioEditModeDeselect->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioEditModeSelect_y_pos, 0.0f)));
	mRadioEditModeDeselect->setSize(UVector2(UDim(0.4f, 0.0f), UDim(radio_y_size, 0.0f)));

	mRadioDeleteLighting->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioDeleteLighting_y_pos, 0.0f)));
	mRadioDeleteLighting->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioDeleteLighting->getXPosition().d_scale + mRadioDeleteLighting->getWidth().d_scale + 0.02f;
	mRadioCopy->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioDeleteLighting_y_pos, 0.0f)));
	mRadioCopy->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));

	mRadioX->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioX_y_pos, 0.0f)));
	mRadioX->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioX->getXPosition().d_scale + mRadioX->getWidth().d_scale + 0.02f;
	mRadioY->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioX_y_pos, 0.0f)));
	mRadioY->setSize(UVector2(UDim(0.23f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioY->getXPosition().d_scale + mRadioY->getWidth().d_scale + 0.02f;
	mRadioZ->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioX_y_pos, 0.0f)));
	mRadioZ->setSize(UVector2(UDim(0.25f, 0.0f), UDim(radio_y_size, 0.0f)));
	mRadioDrag->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioDrag_y_pos, 0.0f)));
	mRadioDrag->setSize(UVector2(UDim(0.25f, 0.0f), UDim(radio_y_size, 0.0f)));
	mDiffuseColorText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mDiffuseColorText_y_pos, 0.0f)));
	mDiffuseColorText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mDiffuseColorEditboxX->setPosition(UVector2(UDim(mScaleEditboxX_x_pos, 0.0f), UDim(mDiffuseColorEditboxX_y_pos, 0.0f)));
	mDiffuseColorEditboxX->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mDiffuseColorEditboxY->setPosition(UVector2(UDim(mScaleEditboxY_x_pos, 0.0f), UDim(mDiffuseColorEditboxX_y_pos, 0.0f)));
	mDiffuseColorEditboxY->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mDiffuseColorEditboxZ->setPosition(UVector2(UDim(mScaleEditboxZ_x_pos, 0.0f), UDim(mDiffuseColorEditboxX_y_pos, 0.0f)));
	mDiffuseColorEditboxZ->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mSpecularColorText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mSpecularColorText_y_pos, 0.0f)));
	mSpecularColorText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mSpecularColorEditboxX->setPosition(UVector2(UDim(mScaleEditboxX_x_pos, 0.0f), UDim(mSpecularColorEditboxX_y_pos, 0.0f)));
	mSpecularColorEditboxX->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mSpecularColorEditboxY->setPosition(UVector2(UDim(mScaleEditboxY_x_pos, 0.0f), UDim(mSpecularColorEditboxX_y_pos, 0.0f)));
	mSpecularColorEditboxY->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mSpecularColorEditboxZ->setPosition(UVector2(UDim(mScaleEditboxZ_x_pos, 0.0f), UDim(mSpecularColorEditboxX_y_pos, 0.0f)));
	mSpecularColorEditboxZ->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));

	mAttenuationText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mAttenuationText_y_pos, 0.0f)));
	mAttenuationText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mAttenuationRangeEditbox->setPosition(UVector2(UDim(mScaleEditboxX_x_pos, 0.0f), UDim(mAttenuationRangeEditbox_y_pos, 0.0f)));
	mAttenuationRangeEditbox->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mAttenuationConstantEditbox->setPosition(UVector2(UDim(mScaleEditboxY_x_pos, 0.0f), UDim(mAttenuationRangeEditbox_y_pos, 0.0f)));
	mAttenuationConstantEditbox->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mAttenuationLinearEditbox->setPosition(UVector2(UDim(mScaleEditboxZ_x_pos, 0.0f), UDim(mAttenuationRangeEditbox_y_pos, 0.0f)));
	mAttenuationLinearEditbox->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));

	mLightingApplyButton->setPosition(UVector2(UDim(mScaleApplyButton_x_pos, 0.0f), UDim(mLightingApplyButton_y_pos, 0.0f)));
	mLightingApplyButton->setSize(UVector2(UDim(mScaleApplyButton_x_size, 0.0f), UDim(mScaleApplyButton_y_size, 0.0f)));

	//Frame;
	mFrameBelowTextSceneNodeName->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowTextSceneNodeName_y_pos, 0.0f)));
	mFrameBelowTextSceneNodeName->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	mScaleText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mScaleText_y_pos, 0.0f)));
	mScaleText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mScaleEditboxX->setPosition(UVector2(UDim(mScaleEditboxX_x_pos, 0.0f), UDim(mScaleEditboxX_y_pos, 0.0f)));
	mScaleEditboxX->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mScaleEditboxY->setPosition(UVector2(UDim(mScaleEditboxY_x_pos, 0.0f), UDim(mScaleEditboxX_y_pos, 0.0f)));
	mScaleEditboxY->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mScaleEditboxZ->setPosition(UVector2(UDim(mScaleEditboxZ_x_pos, 0.0f), UDim(mScaleEditboxX_y_pos, 0.0f)));
	mScaleEditboxZ->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mScaleApplyButton->setPosition(UVector2(UDim(mScaleApplyButton_x_pos, 0.0f), UDim(mScaleApplyButton_y_pos, 0.0f)));
	mScaleApplyButton->setSize(UVector2(UDim(mScaleApplyButton_x_size, 0.0f), UDim(mScaleApplyButton_y_size, 0.0f)));

	// Frame;
	mFrameBelowApplyButton->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowApplyButton_y_pos, 0.0f)));
	mFrameBelowApplyButton->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Move Degree;
	mModelMoveDegreeText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mMoveDegreeText_y_pos, 0.0f)));
	mModelMoveDegreeText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mModelMoveDegreeScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mMoveDegreeScrollbar_y_pos, 0.0f)));
	mModelMoveDegreeScrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));


	/*************************/
	/*** Handle the events ***/
	/*************************/

	/*** Model Mode ***/
	mRadioAddLighting->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelMenuLightingTab::event_radio_modelMode_change, this));

	mRadioEditLighting->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelMenuLightingTab::event_radio_modelMode_change, this));

	mRadioDeleteLighting->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelMenuLightingTab::event_radio_modelMode_change, this));

	mRadioCopy->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelMenuLightingTab::event_radio_modelMode_change, this));


	/*** Scale apply ***/
	mScaleApplyButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_scaleApply, this ) );


	/*** Prevent keyboard acton while in the editboxes ***/
	mAddLightingEditboxName->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mAddLightingEditboxName->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	mScaleEditboxX->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mScaleEditboxX->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	mScaleEditboxY->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mScaleEditboxY->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	mScaleEditboxZ->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mScaleEditboxZ->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	mDiffuseColorEditboxX->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mDiffuseColorEditboxX->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );
	mDiffuseColorEditboxY->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mDiffuseColorEditboxY->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );
	mDiffuseColorEditboxZ->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mDiffuseColorEditboxZ->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	mSpecularColorEditboxX->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mSpecularColorEditboxX->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );
	mSpecularColorEditboxY->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mSpecularColorEditboxY->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );
	mSpecularColorEditboxZ->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mSpecularColorEditboxZ->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	mAttenuationRangeEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mAttenuationRangeEditbox->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );
	mAttenuationConstantEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mAttenuationConstantEditbox->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );
	mAttenuationConstantEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_activated, this ) );
	mAttenuationConstantEditbox->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_editBox_deactivated, this ) );

	/*** Lighting apply ***/
	mLightingApplyButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_lightingApply, this ) );

		// When we click on the main sheet we want to deactivate the editbox;
	sheet->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(
			&ModelMenuLightingTab::event_sheet_mouseDown, this ) );

	/***********************/
	/*** Handle settings ***/
	/***********************/

	// Set the scale editboxes to have the text of 100;
	mScaleEditboxX->setText("100");
	mScaleEditboxY->setText("100");
	mScaleEditboxZ->setText("100");

	// Select the first Lighting mode radio which is Add;
	mRadioAddLighting->setSelected(true);

	// Set the radio position to select;
	mRadioEditModeSelect->setSelected(true);

	// Select the Add mode "name" radio;
	mRadioAddLightingName->setSelected(true);


	/***********************/
	/*** Attach the tabs ***/
	/***********************/


	/*** All mode ***/
	mParentwin->addChildWindow( mLightingModeText );
	mParentwin->addChildWindow( mRadioAddLighting );
	mParentwin->addChildWindow( mRadioEditLighting );
	mParentwin->addChildWindow( mRadioDeleteLighting );
	mParentwin->addChildWindow( mRadioCopy );

	mParentwin->addChildWindow( mMouseRateText );
	mParentwin->addChildWindow( mMouseRateScrollbar );


	// Add Mode - naming;
	mParentwin->addChildWindow( mAddLightingLabel );
	mParentwin->addChildWindow( mAddLightingEditboxName );
	mParentwin->addChildWindow( mRadioAddLightingName );
	mParentwin->addChildWindow( mRadioAddLightingAutoName );
	mParentwin->addChildWindow( mFrameBelowLightingAutoName );

	// Camera Degree move rate;
	mParentwin->addChildWindow( mCamDegreeLabel );
	mParentwin->addChildWindow( mCamDegree_Scrollbar );


	/*** Edit mode ***/
	mParentwin->addChildWindow( mFrameBelowRadioDeleteLighting );

	mParentwin->addChildWindow( mEditModeText );
	mParentwin->addChildWindow( mRadioEditModeSelect );
	mParentwin->addChildWindow( mRadioEditModeDeselect );
	mParentwin->addChildWindow( mRadioX );
	mParentwin->addChildWindow( mRadioY );
	mParentwin->addChildWindow( mRadioZ );
	mParentwin->addChildWindow( mRadioDrag );
	mParentwin->addChildWindow( mDiffuseColorText );
	mParentwin->addChildWindow( mDiffuseColorEditboxX );
	mParentwin->addChildWindow( mDiffuseColorEditboxY );
	mParentwin->addChildWindow( mDiffuseColorEditboxZ );
	mParentwin->addChildWindow( mSpecularColorText );
	mParentwin->addChildWindow( mSpecularColorEditboxX );
	mParentwin->addChildWindow( mSpecularColorEditboxY );
	mParentwin->addChildWindow( mSpecularColorEditboxZ );
	mParentwin->addChildWindow( mAttenuationText );
	mParentwin->addChildWindow( mAttenuationRangeEditbox );
	mParentwin->addChildWindow( mAttenuationConstantEditbox );
	mParentwin->addChildWindow( mAttenuationLinearEditbox );
	mParentwin->addChildWindow( mLightingApplyButton );

	mParentwin->addChildWindow( mFrameBelowTextSceneNodeName );

	mParentwin->addChildWindow( mScaleText );
	mParentwin->addChildWindow( mScaleEditboxX );
	mParentwin->addChildWindow( mScaleEditboxY );
	mParentwin->addChildWindow( mScaleEditboxZ );
	mParentwin->addChildWindow( mScaleApplyButton );

	mParentwin->addChildWindow( mFrameBelowApplyButton );

	mParentwin->addChildWindow( mModelMoveDegreeText );
	mParentwin->addChildWindow( mModelMoveDegreeScrollbar );


	return true;
}



void ModelMenuLightingTab::mouseLeftButtonDown()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	EditManager* const editManager = EditManager::getSingletonPtr();
	RayCastManager* const rayCastManager = editManager->getRayCastManager();
	string sceneNodeName;

	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::SceneNode* sceneNode = NULL;

	CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();

	// Make sure that the mode is set to lighting. This should already be the case but there's no harm in verifying.
	customSceneNodeManipulator->setMode(CustomSceneNodeManipulator::LIGHT);

	// Get the picked SceneNode;
	if (rayCastManager)
	{
		// If there's a sceneNode at the cursor than get it and set it to sceneNode, else set 
		//  sceneNode to NULL;
		if (rayCastManager->getLightSceneNodeNameAtCursor(sceneNodeName) )
		{
			// If the sceneNode name doesn't exist than a exception is thrown.
			// We catch the exception and set the sceneNode to NULL;
			try
			{	
				sceneNode = ogre3dManager->getSceneManager()->getSceneNode(sceneNodeName);
			}
			catch(...)
			{
				sceneNode = NULL;
			}
		}
		else	
		{
			sceneNode = NULL;
		}
	}



	// Addding meshes means the sceneNode may be NULL sometimes;
	if ( mRadioAddLighting->isSelected() )
	{
		Ogre::SceneNode* tmpNode = 0;
		string meshFileName;
		string sceneNodeName;
		string lightName;
		string lightMeshName;

		meshFileName = LIGHTING_MESH_NAME;


		/*** Handle if we auto name or name the scene node and entity ourselves ***/
		if ( mRadioAddLightingAutoName->isSelected() )
		{
			sceneNodeManager->getFreeAutoNameSceneNode(sceneNodeName);
			sceneNodeManager->getFreeAutoNameLight(lightName, lightMeshName);
		}
		else if ( mRadioAddLightingName->isSelected() )
		{
			sceneNodeName = mAddLightingEditboxName->getText().c_str();
			lightName = sceneNodeName;


			// Make the editbox lose focus ***/
			mAddLightingEditboxName->deactivate();

			// Clear the editbox text;
			mAddLightingEditboxName->setText("");



			// If there's no text in the editbox, than return;
			if ( sceneNodeName.empty() )
			{
				string title = "ERROR";
				string functionName = "void ModelMenuLightingTab::mouseLeftButtonDown()";
				string errorMsg = "You did not enter a model name.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return;
			}


			// Create the SceneNode, If the SceneNode already exists than return;
			if ( !sceneNodeManager->getFreeSceneNode(sceneNodeName) )
			{
				string title = "ERROR";
				string functionName = "void ModelMenuLightingTab::mouseLeftButtonDown()";
				string errorMsg = "The model name already exists.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return;
			}
			

			// Create the Light, If the Light already exists than return;
			if ( !sceneNodeManager->getFreeLight(lightName, lightMeshName) )
			{
				string title = "ERROR";
				string functionName = "void ModelMenuLightingTab::mouseLeftButtonDown()";
				string errorMsg = "The entity name already exists.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return;
			}
		}


		/*** Attach the entity to the scenenode ***/
		sceneNodeManager->attachLightToSceneNode(sceneNodeName, lightName, lightMeshName);

		/*** Get the scenenode by its name and then position it at the cursor ***/
		tmpNode = sceneManager->getSceneNode(sceneNodeName);		
		rayCastManager->setSceneNodeAtCursor(tmpNode);

		/*** Select the SceneNode that was just created ***/
		customSceneNodeManipulator->select(tmpNode);
	}
	else if ( mRadioEditLighting->isSelected() )
	{
		if ( mRadioEditModeSelect->isSelected() )
		{
			customSceneNodeManipulator->select(sceneNode);
		}
		else if ( mRadioEditModeDeselect->isSelected() )
		{
			customSceneNodeManipulator->deselect(sceneNode);
		}
		else if ( mRadioX->isSelected() )		
		{
			customSceneNodeManipulator->translateX(mModelMoveDegree);
		}
		else if ( mRadioY->isSelected() )		
		{
			customSceneNodeManipulator->translateY(mModelMoveDegree);
		}
		else if ( mRadioZ->isSelected() )		
		{
			customSceneNodeManipulator->translateZ(mModelMoveDegree);
		}
		else if ( mRadioDrag->isSelected() )
		{
			customSceneNodeManipulator->drag();	
		}
		else if ( mRadioDeleteLighting->isSelected() )
		{
			customSceneNodeManipulator->deleteSelected();
		}
		else if ( mRadioCopy->isSelected() )
		{
			customSceneNodeManipulator->copySelected();
		}
	}

	// Update the lighting edit boxes.
	// Technically this should only need to happen when nodes are added selected and deselected.
	// Until it proves to be a speed problem it will be left here, so as not to introduce bugs caused
	// by copying and pasting a bunch of code.
	this->updateLightingInformation();
}




void ModelMenuLightingTab::mouseRightButtonDown()
{
	CustomSceneNodeManipulator* const customSceneNodeManipulator = CustomSceneNodeManager::getSingletonPtr()->getCustomSceneNodeManipulator();



	if ( mRadioEditLighting->isSelected() )
	{
		// If right click when either the "select" or "deselect" radio is selected, than clear the list;
		if ( mRadioEditModeSelect->isSelected() )
		{
			customSceneNodeManipulator->deselect();
		}
		else if ( mRadioEditModeDeselect->isSelected() )
		{
			customSceneNodeManipulator->deselect();
		}
		else if ( mRadioX->isSelected() )		
		{
			customSceneNodeManipulator->translateX(-mModelMoveDegree);
		}
		else if ( mRadioY->isSelected() )		
		{
			customSceneNodeManipulator->translateY(-mModelMoveDegree);
		}
		else if ( mRadioZ->isSelected() )		
		{
			customSceneNodeManipulator->translateZ(-mModelMoveDegree);
		}
	}
}

void ModelMenuLightingTab::updateWindowShown()
{
	// Refresh the mouse rate;
	ModelMenuLightingTab::_setGuiMouseRate();
	ModelMenuLightingTab::_setMouseRate();

	// Refresh the scrollbars;
	ModelMenuLightingTab::_reAdjustScrollbars();
}

void ModelMenuLightingTab::_reAdjustScrollbars()
{
	string tmpstr;
	CEGUI::PropertyHelper phelper;
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();


	/*** Set the camera rate scrollbar and label ***/
	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();

	tmpstr = "Camera Rate: ";

	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);

	tmpstr += phelper.floatToString(mCameraDegree).c_str();

	// Set the text;
	mCamDegreeLabel->setText(tmpstr);
}

void ModelMenuLightingTab::_setMouseRate()
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();


	if ( mRadioAddLighting->isSelected() )
	{
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_ADD_INDEX));
	}
	else if ( mRadioEditLighting->isSelected() )
	{
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX));
	}
}

void ModelMenuLightingTab::_setGuiMouseRate()
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();
	string tmpstr = "Mouse Rate: ";
	CEGUI::PropertyHelper phelper;


	if ( mRadioAddLighting->isSelected() )
	{
		tmpstr += phelper.uintToString( mVecMouseRate.at(MOUSE_RATE_ADD_INDEX) ).c_str();
		mMouseRateScrollbar->setScrollPosition( mVecMouseRate.at(MOUSE_RATE_ADD_INDEX) );
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_ADD_INDEX));
	}
	else if ( mRadioEditLighting->isSelected() )
	{
		tmpstr += phelper.uintToString( mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX) ).c_str();
		mMouseRateScrollbar->setScrollPosition( mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX) );
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX));
	}


	// Set the text;
	mMouseRateText->setText( tmpstr );
}

bool ModelMenuLightingTab::event_windowVisible(const CEGUI::EventArgs& pEventArgs)
{
  // set the Node Manipulator mode to light.
	CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();
	customSceneNodeManipulator->setMode(CustomSceneNodeManipulator::LIGHT);
	
	ModelMenuLightingTab::updateWindowShown();

	return true;
}

void ModelMenuLightingTab::updateLightingInformation()
{
  /******************************************************************************
   * Updates the state of the lighting editboxes to reflect the current lights. *
   *****************************************************************************/
	CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();
	if ( customSceneNodeManipulator->getSize() == 0) {
	  this->eraseLightingInformation();
	} else {
	  this->copyLightingInformation( customSceneNodeManipulator->getLight() );
	}
}

void ModelMenuLightingTab::eraseLightingInformation() {
  // Removes text from all the lighting editboxes
  mDiffuseColorEditboxX->setText("");
  mDiffuseColorEditboxY->setText("");
  mDiffuseColorEditboxZ->setText("");

  mSpecularColorEditboxX->setText("");
  mSpecularColorEditboxY->setText("");
  mSpecularColorEditboxZ->setText("");

  mAttenuationRangeEditbox->setText("");
  mAttenuationConstantEditbox->setText("");
  mAttenuationLinearEditbox->setText("");
}

void ModelMenuLightingTab::copyLightingInformation(const Ogre::Light* light) {
  // Attaches all of the lighting information from the light object to the editboxes.+

  CEGUI::PropertyHelper phelper;
  const Ogre::ColourValue diffuseColor = light->getDiffuseColour();
  const Ogre::ColourValue specularColor = light->getSpecularColour();
  const Ogre::Real attenuationRange = light->getAttenuationRange();
  const Ogre::Real attenuationConstant = light->getAttenuationConstant();
  const Ogre::Real attenuationLinear = light->getAttenuationLinear();

  mDiffuseColorEditboxX->setText(phelper.floatToString(diffuseColor.r));
  mDiffuseColorEditboxY->setText(phelper.floatToString(diffuseColor.g));
  mDiffuseColorEditboxZ->setText(phelper.floatToString(diffuseColor.b));

  mSpecularColorEditboxX->setText(phelper.floatToString(specularColor.r));
  mSpecularColorEditboxY->setText(phelper.floatToString(specularColor.g));
  mSpecularColorEditboxZ->setText(phelper.floatToString(specularColor.b));

  mAttenuationRangeEditbox->setText(phelper.floatToString(attenuationRange));
  mAttenuationConstantEditbox->setText(phelper.floatToString(attenuationConstant));
  mAttenuationLinearEditbox->setText(phelper.floatToString(attenuationLinear));
}

bool ModelMenuLightingTab::parseEditboxes(const std::vector<CEGUI::Editbox*>& editboxes, 
					  std::vector<Ogre::Real>& values) const {
  // returns false if any of the editboxes contains a string that cannot be converted to a floating
  // point number.

  string title = "ERROR";
  string functionName = "void ModelMenuLightingTab::parseEditboxes()";
  string errorMsg = "You must input a real number.";
  
  Ogre::Real tempReal;
  for (unsigned int i = 0; i < editboxes.size(); ++i) {
    if ( sscanf(editboxes[i]->getText().c_str(), "%f", &tempReal) == EOF) {
      mGuiMessageBox->show( &title, &functionName, &errorMsg );
      return false;
    } else {
      values.push_back(tempReal);
    }
  }
  return true;
}
 
bool ModelMenuLightingTab::parseAndVerifyEditboxes(const std::vector<CEGUI::Editbox*>& editboxes, 
						   std::vector<Ogre::Real>& values) {
  // Parse all of the editboxes and also verifies that there is at least one light node selected.
  // This function automatically sets all of the editboxes to a null string in case there 
  // is some error.

  CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
  CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();
  if ( customSceneNodeManipulator->getSize() == 0 ) {
    string title = "ERROR";
    string functionName = "void ModelMenuLightingTab::parseAndVerifyEditboxes()";
    string errorMsg = "There are no lights to edit.";
    mGuiMessageBox->show( &title, &functionName, &errorMsg );
    this->updateLightingInformation();
    return false;
  }

  if( !parseEditboxes(editboxes, values) ) {
    this->updateLightingInformation();
    return false;
  }

  return true;
}

bool ModelMenuLightingTab::parseAndVerifyColorEditboxes(const std::vector<CEGUI::Editbox*>& editboxes, 
							std::vector<Ogre::Real>& values) {
  // Parses all the text in the editboxes and verifies that they all represent numbers between 0 and 1
  if ( !parseAndVerifyEditboxes( editboxes, values ) )
    return false;
  string title = "ERROR";
  string functionName = "void ModelMenuLightingTab::parseAndVerifyEditboxes()";
  string errorMsg = "Color values must be between 0 and 1.";
  for ( unsigned int i = 0; i < values.size(); ++i ) {
    if (values.at(i) < 0 || values.at(i) > 1) {
      mGuiMessageBox->show( &title, &functionName, &errorMsg );
      this->updateLightingInformation();
      return false;
    }
  }
  return true;
}

bool ModelMenuLightingTab::event_mModelMoveDegreeScrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Model Rate: ";
	CEGUI::PropertyHelper phelper;
	float scrollPos = mModelMoveDegreeScrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '0.001';
	// if the scroll pos is at 0 then we set the scroll position to 0.001;
	if (scrollPos < 0.001f)
	{
		scrollPos = 0.001f;
		 mModelMoveDegreeScrollbar->setScrollPosition(scrollPos);
	}

	tmpstr += phelper.floatToString( scrollPos ).c_str();

	// Set the text;
	mModelMoveDegreeText->setText( tmpstr );

	// Set the class member;
	mModelMoveDegree = scrollPos;


	return true;
}

bool ModelMenuLightingTab::event_mCamDegree_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Camera Rate: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mCamDegree_Scrollbar->getScrollPosition();
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	// Since the scrollbar starts at '0', we want it to never decrease passed '0.001';
	// if the scroll pos is at 0 then we set the scroll position to 0.001;
	if (scrollPos < 0.001f)
	{
		scrollPos = 0.001f;
		mCamDegree_Scrollbar->setScrollPosition(scrollPos);
	}

	tmpstr += phelper.floatToString(scrollPos).c_str();

	// Set the text;
	mCamDegreeLabel->setText(tmpstr); 

	// Set the class member;
	mCameraDegree = scrollPos;


	// Set the camera sceneNode and camera degree;
	cameraManager->setCameraSceneNodeMoveDegree(mCameraDegree);
	cameraManager->setCameraMoveDegree(mCameraDegree);


	return true;
}

bool ModelMenuLightingTab::event_mMouseRateScrollbar_change(const CEGUI::EventArgs& e)
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();
	string tmpstr = "Mouse Rate: ";
	CEGUI::PropertyHelper phelper;
	const unsigned int scrollPos = mMouseRateScrollbar->getScrollPosition();


	if ( mRadioAddLighting->isSelected() )
	{
		mVecMouseRate.at(MOUSE_RATE_ADD_INDEX) =  scrollPos;
	}
	else if ( mRadioEditLighting->isSelected() )
	{
		mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX) =  scrollPos;
	}


	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mMouseRateText->setText( tmpstr );

	// Set the mouse rate;
	sheet->setAutoRepeatRate(scrollPos);


	return true;
}

void ModelMenuLightingTab::_radio_addModel_selected()
{
	/*** Set the mouse rate to be very slow ***/
	_setGuiMouseRate();

	/*** Handle what should and shouldn't be visible ***/
	mAddLightingLabel->setVisible(true);
	mAddLightingEditboxName->setVisible(true);
	mRadioAddLightingName->setVisible(true);
	mRadioAddLightingAutoName->setVisible(true);
	mFrameBelowLightingAutoName->setVisible(true);

	mEditModeText->setVisible(false);
	mRadioEditModeSelect->setVisible(false);
	mRadioEditModeDeselect->setVisible(false);
	mRadioDeleteLighting->setVisible(false);
	mRadioCopy->setVisible(false);
	mRadioX->setVisible(false);
	mRadioY->setVisible(false);
	mRadioZ->setVisible(false);
	mRadioDrag->setVisible(false);
	mDiffuseColorText->setVisible(false);
	mDiffuseColorEditboxX->setVisible(false);
	mDiffuseColorEditboxY->setVisible(false);
	mDiffuseColorEditboxZ->setVisible(false);
	mSpecularColorText->setVisible(false);
	mSpecularColorEditboxX->setVisible(false);
	mSpecularColorEditboxY->setVisible(false);
	mSpecularColorEditboxZ->setVisible(false);
	mAttenuationText->setVisible(false);
	mAttenuationRangeEditbox->setVisible(false);
	mAttenuationConstantEditbox->setVisible(false);
	mAttenuationLinearEditbox->setVisible(false);
	mLightingApplyButton->setVisible(false);

	mFrameBelowTextSceneNodeName->setVisible(false);

	mScaleText->setVisible(false);
	mScaleEditboxX->setVisible(false);
	mScaleEditboxY->setVisible(false);
	mScaleEditboxZ->setVisible(false);
	mScaleApplyButton->setVisible(false);
	mFrameBelowApplyButton->setVisible(false);

	mModelMoveDegreeText->setVisible(false);
	mModelMoveDegreeScrollbar->setVisible(false);
}

void ModelMenuLightingTab::_radio_EditModel_selected()
{
	/*** Set the mouse rate to be very fast ***/
	_setGuiMouseRate();

	/*** Handle what should and shouldn't be visible ***/
	mAddLightingLabel->setVisible(false);
	mAddLightingEditboxName->setVisible(false);
	mRadioAddLightingName->setVisible(false);
	mRadioAddLightingAutoName->setVisible(false);
	mFrameBelowLightingAutoName->setVisible(false);

	mEditModeText->setVisible(true);
	mRadioEditModeSelect->setVisible(true);
	mRadioEditModeDeselect->setVisible(true);
	mRadioDeleteLighting->setVisible(true);
	mRadioCopy->setVisible(true);
	mRadioX->setVisible(true);
	mRadioY->setVisible(true);
	mRadioZ->setVisible(true);
	mRadioDrag->setVisible(true);
	mDiffuseColorText->setVisible(true);
	mDiffuseColorEditboxX->setVisible(true);
	mDiffuseColorEditboxY->setVisible(true);
	mDiffuseColorEditboxZ->setVisible(true);
	mSpecularColorText->setVisible(true);
	mSpecularColorEditboxX->setVisible(true);
	mSpecularColorEditboxY->setVisible(true);
	mSpecularColorEditboxZ->setVisible(true);
	mAttenuationText->setVisible(true);
	mAttenuationRangeEditbox->setVisible(true);
	mAttenuationConstantEditbox->setVisible(true);
	mAttenuationLinearEditbox->setVisible(true);
	mLightingApplyButton->setVisible(true);

	mFrameBelowTextSceneNodeName->setVisible(true);

	mScaleText->setVisible(true);
	mScaleEditboxX->setVisible(true);
	mScaleEditboxY->setVisible(true);
	mScaleEditboxZ->setVisible(true);
	mScaleApplyButton->setVisible(true);
	mFrameBelowApplyButton->setVisible(true);

	mModelMoveDegreeText->setVisible(true);
	mModelMoveDegreeScrollbar->setVisible(true);

	// upadate the lighting editbox states.
	this->updateLightingInformation();
}

bool ModelMenuLightingTab::event_radio_modelMode_change(const CEGUI::EventArgs& pEventArgs)
{
	if ( mRadioAddLighting->isSelected() )
	{
		_radio_addModel_selected();
	}
	else if ( mRadioEditLighting->isSelected() )
	{
		_radio_EditModel_selected();
	}


	return true;

}

bool ModelMenuLightingTab::event_scaleApply(const CEGUI::EventArgs& pEventArgs)
{
	CustomSceneNodeManipulator* const customSceneNodeManipulator = CustomSceneNodeManager::getSingletonPtr()->getCustomSceneNodeManipulator();

	Ogre::StringConverter sc;

	string str_scale_x = mScaleEditboxX->getText().c_str();
	string str_scale_y = mScaleEditboxY->getText().c_str();
	string str_scale_z = mScaleEditboxZ->getText().c_str();

	Ogre::Real x = 0.0;
	Ogre::Real y = 0.0;
	Ogre::Real z = 0.0;


	if ( str_scale_x == "" || str_scale_y == "" || str_scale_z == "" )
	{
		string title = "ERROR";
		string functionName = "bool ModelMenuLightingTab::event_scaleApply(...)";
		string errorMsg = "A Scale editbox is blank.";

		mGuiMessageBox->show( &title, &functionName, &errorMsg );

		return false;
	}
	else
	{
		// Check if the editbox texts have valid numbers;
		if ( sc.isNumber(str_scale_x) && sc.isNumber(str_scale_y) && sc.isNumber(str_scale_z) )
		{
			x = sc.parseReal(str_scale_x);
			y = sc.parseReal(str_scale_y);
			z = sc.parseReal(str_scale_z);


			// When scaling 1 = normal so we get the number and multiple it by 0.01 and then add +1.
			// That way we get the scaling percent.
			// For increase scaling we want it to go from 0 to 100 and for decreasing -99.9 to 0.
			// The reason we don't want -100 is because that decreases the model all the way.
			if ( (x > -100.0 && x <= 100.0) && (y > -100.0 && y <= 100.0) && (z > -100.0 && z <= 100.0) )
			{
				try
				{
					// When scaling 1 = normal so we get the number and multiple it by 0.01 and then add +1.
					// That way we get the scaling percent.
					x = (x * 0.01f) + 1;
					y = (y * 0.01f) + 1;
					z = (z * 0.01f) + 1;

					/*** Scale the sceneNodes ***/
					customSceneNodeManipulator->scale(x,y,z);
				}
				catch(...)
				{
					string title = "ERROR";
					string functionName = "bool ModelMenuLightingTab::event_scaleApply(...)";
					string errorMsg = "An error occurred while scaling. Possibly a problem with the scale numbers.";

					mGuiMessageBox->show( &title, &functionName, &errorMsg );

					return false;
				}
			}
			else
			{
				string title = "ERROR";
				string functionName = "bool ModelMenuLightingTab::event_scaleApply(...)";
				string errorMsg = "The scale percent numbers must be bigger than -100 and no bigger than 100.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return false;
			}
		}
		else
		{
			string title = "ERROR";
			string functionName = "bool ModelMenuLightingTab::event_scaleApply(...)";
			string errorMsg = "The Scale Editboxes don't have valid numbers.";

			mGuiMessageBox->show( &title, &functionName, &errorMsg );

			return false;
		}
	}


	return true;
}

bool ModelMenuLightingTab::event_lightingApply(const CEGUI::EventArgs& pEventArgs)
{
  // Applies the changes to the lighting settings to the selected lights.
  if (this->setDiffuseColor())
    if (this->setSpecularColor())
      if (this->setAttenuation())
	return true;
  this->updateLightingInformation();
  return false;
}

bool ModelMenuLightingTab::setDiffuseColor() 
{
  //Changes the diffuse color of the selected lights.
  CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
  CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();  
  //verify that we are in edit mode.
  if ( mEditModeText->isVisible() ) {
    std::vector<CEGUI::Editbox*> diffuseEditboxes;
    std::vector<Ogre::Real> colorValues;
    diffuseEditboxes.push_back( mDiffuseColorEditboxX );
    diffuseEditboxes.push_back( mDiffuseColorEditboxY );
    diffuseEditboxes.push_back( mDiffuseColorEditboxZ );
    if ( !parseAndVerifyColorEditboxes( diffuseEditboxes, colorValues ) ) {
      return false;
    } else {
      std::vector<Ogre::Light*> lights = customSceneNodeManipulator->getLights();
      for (unsigned int i = 0; i < lights.size(); ++i) {
	std::cout << "Setting Color: (" << colorValues.at(0) << ", " << 
	  colorValues.at(1) << ", " << colorValues.at(2) << ")" << std::endl;;
	lights[i]->setDiffuseColour( colorValues.at(0), colorValues.at(1), colorValues.at(2) );
      }
      return true;
    }
  }
  return true;
}

bool ModelMenuLightingTab::setSpecularColor() 
{
  //Changes the specular color of the selected lights.
  CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
  CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();  
  //verify that we are in edit mode.
  if ( mEditModeText->isVisible() ) {
    std::vector<CEGUI::Editbox*> specularEditboxes;
    std::vector<Ogre::Real> colorValues;
    specularEditboxes.push_back( mSpecularColorEditboxX );
    specularEditboxes.push_back( mSpecularColorEditboxY );
    specularEditboxes.push_back( mSpecularColorEditboxZ );
    if ( !parseAndVerifyColorEditboxes( specularEditboxes, colorValues ) ) {
      return false;
    } else {
      std::vector<Ogre::Light*> lights = customSceneNodeManipulator->getLights();
      for (unsigned int i = 0; i < lights.size(); ++i) {
	std::cout << "Setting Specular Color: (" << colorValues.at(0) << ", " << 
	  colorValues.at(1) << ", " << colorValues.at(2) << ")" << std::endl;;
	lights[i]->setSpecularColour( colorValues.at(0), colorValues.at(1), colorValues.at(2) );
      }
      return true;
    }
  }
  return true;
}

bool ModelMenuLightingTab::setAttenuation() 
{
  //Changes the diffuse color of the selected lights.
  CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
  CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();  
  //verify that we are in edit mode.
  if ( mEditModeText->isVisible() ) {
    std::vector<CEGUI::Editbox*> attenuationEditboxes;
    std::vector<Ogre::Real> attenuationValues;
    attenuationEditboxes.push_back( mAttenuationRangeEditbox );
    attenuationEditboxes.push_back( mAttenuationConstantEditbox );
    attenuationEditboxes.push_back( mAttenuationLinearEditbox );
    if ( !parseAndVerifyEditboxes( attenuationEditboxes, attenuationValues ) ) {
      return false;
    } else {
      std::vector<Ogre::Light*> lights = customSceneNodeManipulator->getLights();
      for (unsigned int i = 0; i < lights.size(); ++i) {
	std::cout << "Setting Color: (" << attenuationValues.at(0) << ", " << 
	  attenuationValues.at(1) << ", " << attenuationValues.at(2) << ")" << std::endl;;
	// for now we will set the quadratic value to zero (the normal default.) their simply isn't room in the ui
	lights[i]->setAttenuation( attenuationValues.at(0), attenuationValues.at(1), attenuationValues.at(2), 0 );
      }
      return true;
    }
  }
  return true;
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool ModelMenuLightingTab::event_editBox_activated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mAddLightingEditboxName->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else if ( mScaleEditboxX->isActive() || mScaleEditboxY->isActive() || mScaleEditboxZ->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		guiManager->setEnabledCommandKeys(true);
	}


	return true;
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool ModelMenuLightingTab::event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mAddLightingEditboxName->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else if ( mScaleEditboxX->isActive() || mScaleEditboxY->isActive() || mScaleEditboxZ->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		guiManager->setEnabledCommandKeys(true);
	}


	return true;
}

bool ModelMenuLightingTab::event_sheet_mouseDown(const CEGUI::EventArgs& pEventArgs)
{
	// Deactivate the editboxes;
	mAddLightingEditboxName->deactivate();

	mScaleEditboxX->deactivate();
	mScaleEditboxY->deactivate();
	mScaleEditboxZ->deactivate();

	mDiffuseColorEditboxX->deactivate();
	mDiffuseColorEditboxY->deactivate();
	mDiffuseColorEditboxZ->deactivate();

	mSpecularColorEditboxX->deactivate();
	mSpecularColorEditboxY->deactivate();
	mSpecularColorEditboxZ->deactivate();

	mAttenuationRangeEditbox->deactivate();
	mAttenuationConstantEditbox->deactivate();
	mAttenuationLinearEditbox->deactivate();
	return true;
}




