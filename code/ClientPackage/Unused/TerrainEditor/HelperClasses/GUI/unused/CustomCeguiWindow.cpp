/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "CustomCeguiWindow.h"



CustomCeguiWindow::CustomCeguiWindow()
{
	mWindow = NULL;
}

CustomCeguiWindow::~CustomCeguiWindow()
{
/****************
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();

	// Only continue if the heap pointer is allocated;
	// If we continue than delete the image set if it exists and delete the heap pointer;
	if (mImageSet != NULL)
	{
		// IF the imageset is present, then delete it;
		if ( imageSetManager.isImagesetPresent(IMAGESET_NAME) )
		{
			CEGUI::ImagesetManager::getSingleton().destroyImageset( *mImageSet );
		}

		// Delete the heap pointer;
		delete mImageSet;

		// Set the pointer back to NULL;
		mImageSet = NULL;
	}
****************/

}

void CustomCeguiWindow::load(CEGUI::Window *window, string &windowName)
{
	if (mWindow == NULL)
	{
		mWindow = new CEGUI::Window(windowName, windowName);

		//mWindow = CEGUI::WindowManager::getSingleton().createWindow(windowName, windowName);

		
		//cout << "CustomCeguiWindow::load(...): Error creating the imageset." << endl;
	}
	else
	{
		cout << "ERROR: CustomCeguiWindow::load(...): Trying to create a imageset When an image already exists."
			<< endl;
	}

}

void CustomCeguiWindow::unLoad()
{
/*******
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();

	// Only continue if the heap pointer is allocated;
	// If we continue than delete the image set if it exists and delete the heap pointer;
	if (mImageSet != NULL)
	{
		// IF the imageset is present, then delete it;
		if ( imageSetManager.isImagesetPresent(IMAGESET_NAME) )
		{
			CEGUI::ImagesetManager::getSingleton().destroyImageset( *mImageSet );
		}

		// Delete the heap pointer;
		delete mImageSet;

		// Set the pointer back to NULL;
		mImageSet = NULL;
	}
********/





}
