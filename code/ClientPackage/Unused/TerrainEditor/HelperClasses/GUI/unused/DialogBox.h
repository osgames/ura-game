/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DIALOGBOX_H
#define DIALOGBOX_H 


#include <CEGUI/CEGUI.h>
#include <Ogre.h>
#include <vector>
#include <string>
#include <iostream>
#include <CustomCeguiTools.h>

#include <EditManager.h>
#include "SingletonGUIWindowStatus.h"
#include "AccessFileSystem.h"
#include "GUIMessageBox.h"
#include "ModelEditorMainTab.h"

#include "CustomWindow2.h"

#include "AccessFileSystem.h"


/***************/
/*** CLASSES ***/
/***************/
//class GUI;


using namespace CEGUI;
using namespace Ogre;
using namespace std;

#define RELOAD_IMAGE_FILENAME "reload.png"
#define DIR_UP_IMAGE_FILENAME "up.png"
#define FOLDER_IMAGE_FILENAME "folder.png"
#define FILE_IMAGE_FILENAME "file.png"

#define DIALOGBOX_MRELOADIMAGE_IMAGESETNAME "DialogBox_mReloadImage_imageSet"
#define DIALOGBOX_MUPIMAGE_IMAGESETNAME "DialogBox_mUpImage_imageSet"
#define DIALOGBOX_FOLDERIMAGE_IMAGESETNAME "DialogBox_FolderImage_imageSet"
#define DIALOGBOX_FILEIMAGE_IMAGESETNAME "DialogBox_FileImage_imageSet"

#define DEF_ICON_TYPE_FOLDER true
#define DEF_ICON_TYPE_FILE false

/******
struct CustomWindow2
{
	Window* one;
	Window* two;
};
*****/

class DialogBox
{
protected:
	Window* mParentwin;
	Window* mMainChildwin;

	Window* mSheet;

	Window* mReloadImage;
	Window* mUpImage;
	CEGUI::Editbox* mEditboxCurrentDir;

	Window* mScrollablePaneParent;	
	ScrollablePane* mScrollablePane;

	unsigned long mNumOfIcons;
	unsigned long mNumberOfWindows;

	vector<string> mVLabelText;
	vector<bool> mViconType;

	CustomWindow2 mVcustomWindow2;

	string mCurrentDir;

	std::map<string, unsigned long> mWindowNameMap;

	AccessFileSystem mFileSystem;

	vector<unsigned long> mVselectedIconIndexes;

	bool event_mReloadButton_click(const CEGUI::EventArgs& pEventArgs);
	bool event_mUpImage_click(const CEGUI::EventArgs& pEventArgs);

	bool event_folderFile_doubleClick(const CEGUI::EventArgs& pEventArgs);
	bool event_folderFile_Click(const CEGUI::EventArgs& pEventArgs);


	void _setStaticTextRelativeExtent(Window* win);
	void _getLargestStringRowIndex(const vector<string> &vString, vector<unsigned long> &vIndex,
		const unsigned long &numOfRows, const unsigned long &iconsPerRow);

	void _createImagesAndLabels();
	void _setupImagesAndLabels();

public:
	DialogBox();
	~DialogBox();

	CEGUI::Window* getWindow();
	bool addChildWindow();	

	void reload();
	void moveUpDir();
};



#endif

