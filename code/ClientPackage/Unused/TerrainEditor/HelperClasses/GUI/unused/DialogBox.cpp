/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapMapSaveWindow.h"
#include <GUI.h>









DialogBox::DialogBox()
{
	mParentwin = NULL;

	mNumberOfWindows = 0;
	mNumOfIcons = 0;
}

DialogBox::~DialogBox()
{
	// Destroy the cegui window;
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);	
}

CEGUI::Window* DialogBox::getWindow()
{
	return mParentwin;
}

bool DialogBox::addChildWindow()
{
	mSheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	CEGUI::ImagesetManager& imageSetManager = CEGUI::ImagesetManager::getSingleton();
		
	CustomCeguiTools customCeguiTools;




	/******************************/
	/*** Create the main window ***/
	/******************************/
	mParentwin = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "DialogBox::mParentwin");
	mParentwin->setSize(CEGUI::UVector2(CEGUI::UDim(0.6, 0.0F), CEGUI::UDim(0.6, 0.0F)));
	mParentwin->setText("  Open File");


	/************************************/
	/*** Create the main Child Window ***/
	/************************************/
	mMainChildwin = windowManager.createWindow("TaharezLook/Listbox_woodframe","DialogBox::mMainChildwin");
	mMainChildwin->setPosition( CEGUI::UVector2(CEGUI::UDim(0.036, 0.0F), CEGUI::UDim(0.055, 0.0F) ) );
	mMainChildwin->setSize(CEGUI::UVector2(CEGUI::UDim(0.924, 0.0F), CEGUI::UDim(0.92, 0.0F)));


	/*******************************/
	/*** Create the mReloadImage ***/
	/*******************************/
	mReloadImage = static_cast<CEGUI::Window*>( windowManager.createWindow("TaharezLook/StaticImage", "DialogBox::mReloadImage") );
	// Disable the image frame and background;
	mReloadImage->setProperty("FrameEnabled", "false");
	mReloadImage->setProperty("BackgroundEnabled", "false");


	/***************************/
	/*** Create the mUpImage ***/
	/***************************/
	mUpImage = static_cast<CEGUI::Window*>( windowManager.createWindow("TaharezLook/StaticImage", "DialogBox::mUpImage") );
	// Disable the image frame and background;
	mUpImage->setProperty("FrameEnabled", "false");
	mUpImage->setProperty("BackgroundEnabled", "false");


	/*************************************/
	/*** Create the mEditboxCurrentDir ***/
	/*************************************/
	mEditboxCurrentDir = static_cast<CEGUI::Editbox*>( windowManager.createWindow("TaharezLook/Editbox", "DialogBox::mEditboxCurrentDir") );


	/************************************************************/
	/*** Create the mScrollablePaneParent and mScrollablePane ***/
	/************************************************************/
//	mScrollablePaneParent = windowManager.createWindow("TaharezLook/Listbox_woodframe","DialogBox::mScrollablePaneParent");

	mScrollablePaneParent = windowManager.createWindow("TaharezLook/StaticImage","DialogBox::mScrollablePaneParent");

	mScrollablePane = static_cast<ScrollablePane*>( windowManager.createWindow("TaharezLook/ScrollablePane", "DialogBox::mScrollablePane") );


	/**************************/
	/*** Load the imagesets ***/
	/**************************/
	try
	{
		if ( imageSetManager.isImagesetPresent(DIALOGBOX_MRELOADIMAGE_IMAGESETNAME) == false )
		{
			imageSetManager.createImagesetFromImageFile(DIALOGBOX_MRELOADIMAGE_IMAGESETNAME, RELOAD_IMAGE_FILENAME);
		}

		if ( imageSetManager.isImagesetPresent(DIALOGBOX_MUPIMAGE_IMAGESETNAME) == false )
		{
			imageSetManager.createImagesetFromImageFile(DIALOGBOX_MUPIMAGE_IMAGESETNAME, DIR_UP_IMAGE_FILENAME);
		}

		if ( imageSetManager.isImagesetPresent(DIALOGBOX_FOLDERIMAGE_IMAGESETNAME) == false )
		{
			imageSetManager.createImagesetFromImageFile(DIALOGBOX_FOLDERIMAGE_IMAGESETNAME, FOLDER_IMAGE_FILENAME);
		}

		if ( imageSetManager.isImagesetPresent(DIALOGBOX_FILEIMAGE_IMAGESETNAME) == false )
		{
			imageSetManager.createImagesetFromImageFile(DIALOGBOX_FILEIMAGE_IMAGESETNAME, FILE_IMAGE_FILENAME);
		}
	}
	catch(...)
	{
		cout << "DialogBox::addChildWindow(): Error creating the imagesets" << endl;
		return false;
	}


	/****************/
	/*** Settings ***/
	/****************/
	mReloadImage->setProperty( "Image", customCeguiTools.ceguiSetImageProperty(string(DIALOGBOX_MRELOADIMAGE_IMAGESETNAME)) );
	mUpImage->setProperty( "Image", customCeguiTools.ceguiSetImageProperty(string(DIALOGBOX_MUPIMAGE_IMAGESETNAME)) );

	// Don't allow the scrollablepane to be auto sized;
	mScrollablePane->setContentPaneAutoSized(false);
	

	/**********************************/
	/*** Set the size and positions ***/
	/**********************************/
	float mReloadImage_x_size = 0.05;
	float mReloadImage_y_size = 0.06;
	float mReloadImage_x_pos = 0.03;
	float mReloadImage_y_pos = 0.04;

	float mUpImage_x_size = mReloadImage_x_size;
	float mUpImage_y_size = mReloadImage_y_size;
	float mUpImage_x_pos = mReloadImage_x_pos + mReloadImage_x_size + 0.02;
	float mUpImage_y_pos = mReloadImage_y_pos;

	float mEditboxCurrentDir_x_size = 0.78;
	float mEditboxCurrentDir_y_size = mReloadImage_y_size;
	float mEditboxCurrentDir_x_pos = mUpImage_x_pos + mUpImage_x_size + 0.03;
	float mEditboxCurrentDir_y_pos = mReloadImage_y_pos;

	float mScrollablePaneParent_x_size = 0.93;
	float mScrollablePaneParent_y_size = 0.7;
	float mScrollablePaneParent_x_pos = mReloadImage_x_pos;
	float mScrollablePaneParent_y_pos = 0.13;


	mReloadImage->setPosition( CEGUI::UVector2(CEGUI::UDim(mReloadImage_x_pos, 0.0F), CEGUI::UDim(mReloadImage_y_pos, 0.0F) ));
	mReloadImage->setSize(UVector2(UDim(mReloadImage_x_size, 0.0f), UDim(mReloadImage_y_size, 0.0f))); 

	mUpImage->setPosition( CEGUI::UVector2(CEGUI::UDim(mUpImage_x_pos, 0.0F), CEGUI::UDim(mUpImage_y_pos, 0.0F) ));
	mUpImage->setSize(UVector2(UDim(mUpImage_x_size, 0.0f), UDim(mUpImage_y_size, 0.0f))); 

	mEditboxCurrentDir->setPosition( CEGUI::UVector2(CEGUI::UDim(mEditboxCurrentDir_x_pos, 0.0F), CEGUI::UDim(mEditboxCurrentDir_y_pos, 0.0F) ));
	mEditboxCurrentDir->setSize(UVector2(UDim(mEditboxCurrentDir_x_size, 0.0f), UDim(mEditboxCurrentDir_y_size, 0.0f))); 



	mScrollablePane->setPosition( CEGUI::UVector2(CEGUI::UDim(0, 0.0F), CEGUI::UDim(0, 0.0F) ));
	mScrollablePane->setSize(UVector2(UDim(1, 0.0f), UDim(1, 0.0f))); 

	mScrollablePaneParent->setPosition( CEGUI::UVector2(CEGUI::UDim(mScrollablePaneParent_x_pos, 0.0F),
		 CEGUI::UDim(mScrollablePaneParent_y_pos, 0.0F) ));
	mScrollablePaneParent->setSize(UVector2(UDim(mScrollablePaneParent_x_size, 0.0f),
		 UDim(mScrollablePaneParent_y_size, 0.0f))); 



	/*************************/
	/*** Handle the events ***/
	/*************************/

	mReloadImage->subscribeEvent( CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber( 
			&DialogBox::event_mReloadButton_click, this ) );

	mUpImage->subscribeEvent( CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber( 
			&DialogBox::event_mUpImage_click, this ) );

	/************************/
	/*** Set the Settings ***/
	/************************/

	// Only allow this window to be accessed until the ok button is clicked;
	//mParentwin->setModalState(true);


	/************************************************/
	/*** Attach the windows to the parent window ***/
	/************************************************/
	mMainChildwin->addChildWindow(mReloadImage);
	mMainChildwin->addChildWindow(mUpImage);
	mMainChildwin->addChildWindow(mEditboxCurrentDir);

	// Attach the scrollablepane to a parent window first
	mScrollablePaneParent->addChildWindow(mScrollablePane);

	// Now attach the scrollablepain parent to the main child window;
	mMainChildwin->addChildWindow(mScrollablePaneParent);


	mParentwin->addChildWindow(mMainChildwin);
	mSheet->addChildWindow(mParentwin);




	/*******************/
	/*** Handle CODE ***/
	/*******************/

/*
	mNumOfIcons = 10;

	if (mNumOfIcons > 0)
	{
		DialogBox::_createImagesAndLabels();
	}

//	mCurrentDir = "/home/bittorrent/Desktop/testfolder/1/2/3/4/5/6";

*/

	/*** Set the current directory to the path the editor was run from ***/
	if ( mFileSystem.getInitialPath(&mCurrentDir) )
	{

	}
	else
	{
		mCurrentDir = "/";
	}


	/***  Fill the window with the folders and icons by reloading ***/
	DialogBox::reload();



	return true;
}

void DialogBox::_getLargestStringRowIndex(const vector<string> &vString, vector<unsigned long> &vIndex,
	const unsigned long &numOfRows, const unsigned long &iconsPerRow)
{
	unsigned long tmpSize = 0;
	unsigned long tmpStore = 0;
	
	unsigned long iconTmpCount = 0;
	unsigned long largestIndexPerRow = 0;

	for(unsigned long i = 0; i < numOfRows; i++)
	{
		tmpStore = vString.at(iconTmpCount).size();
		tmpSize = tmpStore;
		largestIndexPerRow = iconTmpCount;

		for (unsigned long j = 0; j < iconsPerRow; j++)
		{
			tmpStore = vString.at(iconTmpCount).size();

			if (tmpStore > tmpSize)
			{
				tmpSize = tmpStore;

				largestIndexPerRow = iconTmpCount;
			}


			++iconTmpCount;

			if (iconTmpCount >= mNumOfIcons)
			{
				i = numOfRows;
				break;
			}
		}


		vIndex.push_back(largestIndexPerRow);
	}
}


void DialogBox::_setupImagesAndLabels()
{
	CustomCeguiTools customCeguiTools;

	float tmpVal = 0.0f;

	unsigned long  iconRowHeightPos = 0;
	unsigned long  imageRowXPos = 0;
	unsigned long  labelRowXPos = 0;
	unsigned long tmpXpos = 0;
	const unsigned long row_x_pos_extraBuffer = 20;

	CEGUI::Font* pFont = mVcustomWindow2.getTwo(0)->getFont();
	const unsigned long  iconHeight = 30;
	const unsigned long  iconWidth = 30;
	unsigned long   labelWidth = 0;
	unsigned long largestLabelIndex = 0;

	unsigned long scrollablePanePixelWidth = 0;
	unsigned long scrollablePanePixelHeight = mScrollablePaneParent->getPixelSize().d_height;

	// Get iconsPerRow;
	tmpVal = ( (static_cast<float>(scrollablePanePixelHeight)  / static_cast<float>(iconHeight)) );
	unsigned long iconsPerRow = tmpVal;

	// We get the number of rows. We use 0.5f to round up the integer;
	tmpVal  = (static_cast<float>(mNumOfIcons) / iconsPerRow);
	// If the float val is bigger than the int value than we need at add a +1 for another row;
	const unsigned long numOfRows = ( tmpVal > static_cast<unsigned long>(tmpVal) ) ? ++tmpVal : tmpVal;

	unsigned long iconTmpCount = 0;
	vector<unsigned long> vIndex;






	/*** If the number of icons is better than icons per row, then set iconsPerRow to numOficons ***/
	if (mNumOfIcons < iconsPerRow)
	{
		iconsPerRow = mNumOfIcons;
	}


	/*** Get the indexes with the largest string and determine their rows.....for later use ****/
	DialogBox::_getLargestStringRowIndex(mVLabelText, vIndex, numOfRows, iconsPerRow);



	/*** Set the images, labels and their text and their positions ***/
	for(unsigned long i = 0; i < numOfRows; i++)
	{
		largestLabelIndex = vIndex[i];
		labelWidth = pFont->getTextExtent ( mVLabelText.at(largestLabelIndex) );



		// We don't want the buffer to be added on the first row;
		if (i > static_cast<unsigned long>(0) )
		{
			tmpXpos += (row_x_pos_extraBuffer);
		}


		imageRowXPos = tmpXpos;
		
		tmpXpos += iconWidth;
		
		labelRowXPos = tmpXpos;
		
		tmpXpos += labelWidth;
			

		for (unsigned long j = 0; j < iconsPerRow; j++)
		{
			iconRowHeightPos = iconHeight * static_cast<unsigned long>(j);
	

			/*** Set the icon image ***/
			if (mViconType.at(iconTmpCount) == DEF_ICON_TYPE_FOLDER)
			{
				mVcustomWindow2.getOne(iconTmpCount)->setProperty( "Image",
					customCeguiTools.ceguiSetImageProperty(string(DIALOGBOX_FOLDERIMAGE_IMAGESETNAME)) );
			}
			else if (mViconType.at(iconTmpCount) == DEF_ICON_TYPE_FILE)
			{
				mVcustomWindow2.getOne(iconTmpCount)->setProperty( "Image",
					customCeguiTools.ceguiSetImageProperty(string(DIALOGBOX_FILEIMAGE_IMAGESETNAME)) );
			}


			/*** Set the image Position and size ***/
			mVcustomWindow2.getOne(iconTmpCount)->setPosition( CEGUI::UVector2(CEGUI::UDim(0, imageRowXPos),
				CEGUI::UDim(0, iconRowHeightPos) ));
			mVcustomWindow2.getOne(iconTmpCount)->setSize(UVector2(UDim(0,iconWidth), UDim(0, iconHeight))); 
	
	
			/*** Set the label position and size after it ***/
			mVcustomWindow2.getTwo(iconTmpCount)->setPosition( CEGUI::UVector2(CEGUI::UDim(0, labelRowXPos), CEGUI::UDim(0, iconRowHeightPos) ));
			mVcustomWindow2.getTwo(iconTmpCount)->setSize(UVector2(UDim(0, labelWidth), UDim(0, iconHeight))); 


			/*** Set the label text ***/
			mVcustomWindow2.getTwo(iconTmpCount)->setText( mVLabelText.at(iconTmpCount) );	



			/*** If we reached the max numofIcons count than break from the loops ***/
			++iconTmpCount;

			if (iconTmpCount >= mNumOfIcons)
			{
				i = numOfRows;
				break;
			}
		}
	}


	/*** Set the scrollablePaneArea. To do that we use the last icon position + its width to get the pixelWidth ***/
	scrollablePanePixelWidth = mVcustomWindow2.getTwo(mNumOfIcons - 1)->getXPosition().d_offset + 
			mVcustomWindow2.getTwo(mNumOfIcons - 1)->getWidth().d_offset + 5;
	mScrollablePane->setContentPaneArea( CEGUI::Rect(0,0, scrollablePanePixelWidth, 0) );
}

bool DialogBox::event_mReloadButton_click(const CEGUI::EventArgs& pEventArgs)
{
	DialogBox::reload();
	
	return true;	
}

bool DialogBox::event_mUpImage_click(const CEGUI::EventArgs& pEventArgs)
{
	DialogBox::moveUpDir();

	return true;	
}

void DialogBox::_createImagesAndLabels()
{
	/* Set the number of icons */
	mNumOfIcons = mVLabelText.size();

	const unsigned long numOfNewWindowsNeeded = mNumOfIcons - mNumberOfWindows;
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	string windowName;



	/*
	 *	We use a pool of already created windows. If we need more than make more and add them to the pool.
	 *	If we Need less than make the extras invisible and possibly disable them. 
	 */
	if (mNumOfIcons == mNumberOfWindows)
	{
		cout << "mNumOfIcons == mNumberOfWindows" << endl;

		/*
		 *	We have the same of amount of icon images and windows. What we want is to set the text,
		 *	 setvisible and enable the image and label for them.
		 */
		for (unsigned long i = 0; i < mNumberOfWindows; i++)
		{
			// set visible and enable the image;
			mVcustomWindow2.getOne(i)->setVisible(true);
			mVcustomWindow2.getOne(i)->setEnabled(true);
	
			// set visible and enable the label;
			mVcustomWindow2.getTwo(i)->setVisible(true);
			mVcustomWindow2.getTwo(i)->setEnabled(true);		
		}
	}
	else if (mNumOfIcons == 0)
	{
		cout << "mNumOfIcons == 0" << endl;

		/*
		 *	We have the same amount of icon images and windows. What we want is to
		 *	 set invisible, disable the image and label for them.
		 */
		for (unsigned long i = 0; i < mNumberOfWindows; i++)
		{
			// set invisible and disable the image;
			mVcustomWindow2.getOne(i)->setVisible(false);
			mVcustomWindow2.getOne(i)->setEnabled(false);
	
			// set invisible and disable the label;
			mVcustomWindow2.getTwo(i)->setVisible(false);
			mVcustomWindow2.getTwo(i)->setEnabled(false);
		}
	}
	else if (mNumOfIcons > mNumberOfWindows)
	{
		cout << "mNumOfIcons > mNumberOfWindows" << endl;

		/*
		 *	We might already have some windows created from last time. What we want is to set the text,
		 *	 setvisible and enable the image and label for them.
		 */
		for (unsigned long i = 0; i < mNumberOfWindows; i++)
		{
			// set visible and enable the image;
			mVcustomWindow2.getOne(i)->setVisible(true);
			mVcustomWindow2.getOne(i)->setEnabled(true);
	
			// set visible and enable the image;
			mVcustomWindow2.getTwo(i)->setVisible(true);
			mVcustomWindow2.getTwo(i)->setEnabled(true);		
		}

		/*** Create the new needed windows ***/
		for (unsigned long i = mNumberOfWindows; i < mNumOfIcons; i++)
		{
			// push back the vector to create a new object;
			mVcustomWindow2.add(NULL,NULL);
	

			// Create the image;
			mVcustomWindow2.setOne(i, windowManager.createWindow("TaharezLook/StaticImage"));
			mVcustomWindow2.getOne(i)->setProperty("FrameEnabled", "false");
			mVcustomWindow2.getOne(i)->setProperty("BackgroundEnabled", "false");
		
			// Create the label;
			mVcustomWindow2.setTwo(i, windowManager.createWindow("TaharezLook/StaticText"));
			mVcustomWindow2.getTwo(i)->setProperty("FrameEnabled", "False");
			mVcustomWindow2.getTwo(i)->setProperty("BackgroundEnabled", "false");
	
			// Add the windows to the scrollable pane;
			mScrollablePane->addChildWindow(mVcustomWindow2.getOne(i));
			mScrollablePane->addChildWindow(mVcustomWindow2.getTwo(i));


			/*
			*** Store the Window name and pointer in a std::map.
			*** Later we can figure out what icon was clicked with this;
			 */
			windowName = mVcustomWindow2.getOne(i)->getName().c_str();
			mWindowNameMap.insert( pair<string, unsigned long>(windowName, i) );

			windowName = mVcustomWindow2.getTwo(i)->getName().c_str();
			mWindowNameMap.insert( pair<string, unsigned long>(windowName, i) );


			/*** Set the events for double clicks ***/
			mVcustomWindow2.getOne(i)->subscribeEvent( CEGUI::Window::EventMouseDoubleClick, CEGUI::Event::Subscriber( 
				&DialogBox::event_folderFile_doubleClick, this ) );

			mVcustomWindow2.getTwo(i)->subscribeEvent( CEGUI::Window::EventMouseDoubleClick, CEGUI::Event::Subscriber( 
				&DialogBox::event_folderFile_doubleClick, this ) );


			/*** Set the events for single clicks ***/
			mVcustomWindow2.getOne(i)->subscribeEvent( CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber( 
				&DialogBox::event_folderFile_Click, this ) );

			mVcustomWindow2.getTwo(i)->subscribeEvent( CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber( 
				&DialogBox::event_folderFile_Click, this ) );
		}


		// We now have more windows to add them to our class member variable;
		mNumberOfWindows += numOfNewWindowsNeeded;
	}
	else if (mNumOfIcons < mNumberOfWindows)
	{
		cout << "mNumOfIcons < mNumberOfWindows" << endl;

		/*
		 *	We have more windows than needed, so Enable and set visible all the windows up to the 
		 *	  number of mNumOfIcons. Also set the text.
		 */
		for (unsigned long i = 0; i < mNumOfIcons; i++)
		{
			// set invisible and disable the image;
			mVcustomWindow2.getOne(i)->setVisible(true);
			mVcustomWindow2.getOne(i)->setEnabled(true);
	
			// set invisible and disable the label;
			mVcustomWindow2.getTwo(i)->setVisible(true);
			mVcustomWindow2.getTwo(i)->setEnabled(true);
		}

		/*
		 *	We have more windows than needed so hide and disable the un-needed ones
		 *	 after the mNumOfIcons.
		 */
		for (unsigned long i = mNumOfIcons; i < mNumberOfWindows; i++)
		{
			// set invisible and disable the image;
			mVcustomWindow2.getOne(i)->setVisible(false);
			mVcustomWindow2.getOne(i)->setEnabled(false);
	
			// set invisible and disable the label;
			mVcustomWindow2.getTwo(i)->setVisible(false);
			mVcustomWindow2.getTwo(i)->setEnabled(false);		
		}
	}



	// Make sure we have icons before we do anything;
	if (mNumOfIcons > 0)
	{
		_setupImagesAndLabels();
	}
}


void DialogBox::reload()
{
	unsigned long tmpCount = 0;

	
	// Set all labels text colors back to white;
	for (unsigned long i = 0; i < mVselectedIconIndexes.size(); i++)
	{
		// Reset all the selected labels text colors back to white;
		mVcustomWindow2.getTwo(mVselectedIconIndexes.at(i))->setProperty("TextColours",
			"tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF"); 
	}


	// Clear the vectors and map;
	mVselectedIconIndexes.clear();
	mVLabelText.clear();
	mViconType.clear();


	// Set the editbox current directory text;
	mEditboxCurrentDir->setText(mCurrentDir);


	// Get the current directions and feel the string vector with them;
	mFileSystem.getDirectories(mCurrentDir, &mVLabelText);


	// Set the indexes for the icon type folder and set the text color to white;
	for (unsigned int i = 0; i < mVLabelText.size(); i++)
	{
		mViconType.push_back(DEF_ICON_TYPE_FOLDER); 
	}



	// Store the folder icon count and use it so we know when the file icons start;
	tmpCount = mVLabelText.size();


	// Get the files and put them in the string vector;
	mFileSystem.getFiles(mCurrentDir, &mVLabelText);

	
	// Set the indexes for the icon type file;
	for (unsigned int i = tmpCount; i < mVLabelText.size(); i++)
	{
		mViconType.push_back(DEF_ICON_TYPE_FILE);
	}	
	


	_createImagesAndLabels();
}

void DialogBox::moveUpDir()
{
	string tmpResult;
	unsigned long tmpCount = 0;
	bool check = 0;
	



	/*** Set all labels text colors back to white ***/
	for (unsigned long i = 0; i < mVselectedIconIndexes.size(); i++)
	{
		// Reset all the selected labels text colors back to white;
		mVcustomWindow2.getTwo(mVselectedIconIndexes.at(i))->setProperty("TextColours",
			"tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF"); 
	}


	/*** Clear the vectors and map***/
	mVselectedIconIndexes.clear();
	mVLabelText.clear();
	mViconType.clear();


	/*** Check if we can move up a directory. Store the result ***/
	check = mFileSystem.getCanMoveUpDir(&mCurrentDir, &tmpResult);

	

	if (!check)
	{
		cout << "ERROR: DialogBox::reload(...): Couldn't go up one directory." << endl;
	}
	else
	{
		// Store the current directory result;
		mCurrentDir = tmpResult;


		// Set the editbox current directory text;
		mEditboxCurrentDir->setText(mCurrentDir);


		// Get the current directions and feel the string vector with them;
		mFileSystem.getDirectories(mCurrentDir, &mVLabelText);


		// Set the indexes for the icon type folder;
		for (unsigned int i = 0; i < mVLabelText.size(); i++)
		{
			mViconType.push_back(DEF_ICON_TYPE_FOLDER);
		}


		// Store the folder icon count and use it so we know when the file icons start;
		tmpCount = mVLabelText.size();
	

		// Get the files and put them in the string vector;
		mFileSystem.getFiles(mCurrentDir, &mVLabelText);

		
		// Set the indexes for the icon type file;
		for (unsigned int i = tmpCount; i < mVLabelText.size(); i++)
		{
			mViconType.push_back(DEF_ICON_TYPE_FILE);
		}	
	}


	_createImagesAndLabels();
}



bool DialogBox::event_folderFile_doubleClick(const CEGUI::EventArgs& pEventArgs)
{
	CEGUI::Window* window = static_cast<const WindowEventArgs&>(pEventArgs).window;
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);

	string windowName = window->getName().c_str();
	map<string, unsigned long>::iterator it = mWindowNameMap.find(windowName);

	string folderName;
	unsigned long windowNumber = 0;
	string tmpResult;
	bool check = false;

	bool foundDuplicate = false;



	if ( it != mWindowNameMap.end() )
	{
		windowNumber = it->second;


		if (mouseEventArgs.button == CEGUI::LeftButton)
		{
			if ( mViconType.at(windowNumber) == DEF_ICON_TYPE_FOLDER )
			{
				// Get the foldername by using windowNumber as the indexof mVLabelText;
				folderName = mVLabelText.at(windowNumber);
	
				check = mFileSystem.getCanMoveIntoDir(&mCurrentDir, &folderName, &tmpResult);
	
				if (check)
				{
					mCurrentDir = tmpResult;
	
					// Set all labels text colors back to white;
					for (unsigned long i = 0; i < mVselectedIconIndexes.size(); i++)
					{
						// Reset all the selected labels text colors back to white;
						mVcustomWindow2.getTwo(mVselectedIconIndexes.at(i))->setProperty("TextColours",
							"tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF"); 
					}

					// Fill the window with the folders and icons by reloading;
					DialogBox::reload();
				}
	
			}
		}
		// Multiple selection - So keep turning the labels text color blue and push back the vector windowNumber;
		else  if (mouseEventArgs.button == CEGUI::RightButton)
		{
			cout << "RIGHT button num= " << windowNumber << endl;

			// Make sure were not adding duplicate window numbers;
			for (unsigned long i = 0; i < mVselectedIconIndexes.size(); i++)
			{
				if ( mVselectedIconIndexes.at(i) == windowNumber )
				{
					foundDuplicate = true;

					cout << "DUPLICATE FOUND "  << endl;

					break;
				}
			}

			if (!foundDuplicate)
			{
				// Set the new labels text color to blue;
				mVcustomWindow2.getTwo(windowNumber)->setProperty("TextColours", "tl:FF0000FF tr:FF0000FF bl:FF0000FF br:FF0000FF");
	
				// Add the new selected label to the vector;
				mVselectedIconIndexes.push_back(windowNumber);
			}
		}
	}
	else
	{
		cout << "ERROR: bool DialogBox::event_folderFile_doubleClick(): Could not find std::map key " 
			<< it->first << endl;
	}




	cout << " mVselectedIconIndexes.size() ="  << mVselectedIconIndexes.size() << endl;


	return true;
}

bool DialogBox::event_folderFile_Click(const CEGUI::EventArgs& pEventArgs)
{
	CEGUI::Window* window = static_cast<const WindowEventArgs&>(pEventArgs).window;
	const MouseEventArgs mouseEventArgs = static_cast<const MouseEventArgs&>(pEventArgs);

	string windowName = window->getName().c_str();
	map<string, unsigned long>::iterator it = mWindowNameMap.find(windowName);

	unsigned long windowNumber = 0;

	

	if ( it != mWindowNameMap.end() )
	{
		windowNumber = it->second;


		if (mouseEventArgs.button == CEGUI::LeftButton)
		{
			if (mVselectedIconIndexes.size() > 0)
			{
				for (unsigned long i = 0; i < mVselectedIconIndexes.size(); i++)
				{
					// Reset all the selected labels text colors back to white;
					mVcustomWindow2.getTwo(mVselectedIconIndexes.at(i))->setProperty("TextColours",
						"tl:FFFFFFFF tr:FFFFFFFF bl:FFFFFFFF br:FFFFFFFF"); 
				}
	
				// Clear the vector;
				mVselectedIconIndexes.clear();
			}
	
			// Set the new labels text color to blue;
			mVcustomWindow2.getTwo(windowNumber)->setProperty("TextColours", "tl:FF0000FF tr:FF0000FF bl:FF0000FF br:FF0000FF");	

			// Add the new selected label to the vector;
			mVselectedIconIndexes.push_back(windowNumber);
		}
	}
	else
	{
		cout << "ERROR: bool DialogBox::event_folderFile_Click(): Could not find std::map key " 
			<< it->first << endl;
	}


	cout << " mVselectedIconIndexes.size() ="  << mVselectedIconIndexes.size() << endl;


	return true;
}


