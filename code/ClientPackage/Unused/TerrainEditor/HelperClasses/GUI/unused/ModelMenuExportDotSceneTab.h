/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MODELMENUEXPORTDOTSCENETAB_H
#define MODELMENUEXPORTDOTSCENETAB_H 


#include <CEGUI/CEGUI.h>
#include <Ogre.h>
#include <vector>
#include <string>
#include <iostream>

#include <EditManager.h>
#include "SingletonGUIWindowStatus.h"
#include "AccessFileSystem.h"
#include "GUIMessageBox.h"

using namespace CEGUI;
using namespace Ogre;
using namespace std;

class ModelMenuExportDotSceneTab
{
protected:
	GUIMessageBox* mGuiMessageBox;

	CEGUI::Window* mParentwin;
	CEGUI::Listbox* mListbox;
	CEGUI::Editbox* mNameEditbox;
	CEGUI::PushButton* mReloadButton;
	CEGUI::PushButton* mOkButton;

	CEGUI::Window* mSheet;

	EditManager *mEditManager;

	bool _loadDotSceneListBox();
	void _exportDotScene(string* name);
	void _getSceneNodesWithException(Ogre::SceneNode *n, vector<Ogre::SceneNode*> *include);

	bool event_listBoxTextToEditBox(const CEGUI::EventArgs& pEventArgs);
	bool event_reloadListbox(const CEGUI::EventArgs& pEventArgs);
	bool event_exportDotSceneFile(const CEGUI::EventArgs& pEventArgs);

	// Prevent keyboard actons while in the editboxes;
	bool event_editBox_activated(const CEGUI::EventArgs& pEventArgs);
	bool event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs);
	bool event_sheet_mouseDown(const CEGUI::EventArgs& pEventArgs);
public:
	ModelMenuExportDotSceneTab(EditManager *editMgr);
	~ModelMenuExportDotSceneTab();

	CEGUI::Window* getWindow();
	bool addChildWindow();	
};


#endif