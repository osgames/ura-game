/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ModelMenuExportDotSceneTab.h"



ModelMenuExportDotSceneTab::ModelMenuExportDotSceneTab(EditManager *editMgr) : mEditManager(editMgr)
{
	mGuiMessageBox = NULL;

	/*** allocate heap memory for the gui messagebox ***/
	mGuiMessageBox = new GUIMessageBox;
}

ModelMenuExportDotSceneTab::~ModelMenuExportDotSceneTab()
{
	if ( mGuiMessageBox )
	{
		delete mGuiMessageBox;
	}
}

CEGUI::Window* ModelMenuExportDotSceneTab::getWindow()
{
	return mParentwin;
}

bool ModelMenuExportDotSceneTab::addChildWindow()
{
	mSheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	
	// Create the GuiMessageBox and add it to the sheet;
	if ( mGuiMessageBox )
	{
		mGuiMessageBox->addWindow();
	}
	else
	{
		cout << "ERROR: bool ModelMenuExportDotSceneTab::addChildWindow(): mGuiMessageBox could not allocate heap memory";
		return false;
	}


	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelMenuExportDotSceneTab_mParentwin");
	mParentwin->setText("Export");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f))); 
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f))); 


	/**********************************************************/
	/*** Create the mListbox to select what files to import ***/
	/**********************************************************/

	// the listbox;
	mListbox = (CEGUI::Listbox *) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelMenuExportDotSceneTab_mMeshListbox");


	/**********************************************************/
	/*** Create the Editbox to optionally insert a filename ***/
	/**********************************************************/

	// Create the edit box;
	mNameEditbox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelMenuExportDotSceneTab_mNameEditbox");


	/****************************************/
	/*** Create the reload and ok buttons ***/
	/****************************************/

	// The reload button;
	mReloadButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "ModelMenuExportDotSceneTab_mReloadButton");
	mReloadButton->setText("Reload List");

	// The okay button
	mOkButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "ModelMenuExportDotSceneTab_mOkButton");
	mOkButton->setText("Ok");

	/**********************************/
	/*** Set the size and positions ***/
	/**********************************/

	float mListbox_x_pos = 0.1f;
	float mListbox_y_pos = 0.03f;
	float mListbox_x_size = 0.8f;
	float mListbox_y_size = 0.73f;

	float editbox_y_size = 0.04f;
	float button_y_size = 0.04f;

	float mNameEditbox_y_pos = mListbox_y_pos + mListbox_y_size + 0.03f;
	float mReloadButton_y_pos = mNameEditbox_y_pos + editbox_y_size +  0.03f;
	float mOkButton_y_pos =  mReloadButton_y_pos + button_y_size + 0.03f;


	mListbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mListbox_y_pos, 0.0f))); 
	mListbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(mListbox_y_size, 0.0f))); 

	mNameEditbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mNameEditbox_y_pos, 0.0f))); 
	mNameEditbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(editbox_y_size, 0.0f))); 

	mReloadButton->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mReloadButton_y_pos, 0.0f))); 
	mReloadButton->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(button_y_size, 0.0f))); 

	mOkButton->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mOkButton_y_pos, 0.0f))); 
	mOkButton->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(button_y_size, 0.0f))); 


	/*************************/
	/*** Handle the events ***/
	/*************************/

	// Handle the listbox selection change;
	mListbox->subscribeEvent( CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber( 
		&ModelMenuExportDotSceneTab::event_listBoxTextToEditBox, this ) );

	// Handle the listbox reload with the reload button;
	mReloadButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&ModelMenuExportDotSceneTab::event_reloadListbox, this ) );


	// Handle the Ok button;
	mOkButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&ModelMenuExportDotSceneTab::event_exportDotSceneFile, this ) );

	// Handle putting text into editbox without using special keys;
	mNameEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber( 
			&ModelMenuExportDotSceneTab::event_editBox_activated, this ) );

	mNameEditbox->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber( 
			&ModelMenuExportDotSceneTab::event_editBox_deactivated, this ) );

	// When we click on the main sheet we want to deactivate the editbox;
	mSheet->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber( 
			&ModelMenuExportDotSceneTab::event_sheet_mouseDown, this ) );
	
	/************************/
	/*** Set the Settigns ***/
	/************************/

	// Turn on listbox Sorting;
	mListbox->setSortingEnabled(true);

	// Load the listbox on startup;
	_loadDotSceneListBox();

	/************************************************/
	/*** Attach the windows to the parent window ***/
	/************************************************/
	mParentwin->addChildWindow( mListbox );	
	mParentwin->addChildWindow( mNameEditbox );
	mParentwin->addChildWindow( mReloadButton );
	mParentwin->addChildWindow( mOkButton );



	return true;
}


bool ModelMenuExportDotSceneTab::_loadDotSceneListBox()
{

	AccessFileSystem fileSystem;
	string vFilePath = "../../../resources/models";
	string vFileFilter = ".xml";

	vector<string> vFileNames;
	CEGUI::ListboxTextItem* item = NULL;



	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(vFilePath) )
	{
		string title = "ERROR";
		string functionName = "bool ModelMenuExportDotSceneTab::_loadDotSceneListBox()";
		string errorMsg = "Could not find model directory.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}
		else
		{
			cout << "ERROR: bool ModelMenuExportDotSceneTab::_loadDotSceneListBox(): mGuiMessageBox could not allocate heap memory";
			return false;
		}

		return false;
	}


	/*** We load the listbox with all the files in this directory that have the extension ".xml" ***/
	fileSystem.getFullPathRecursiveFilesWithExtension(vFilePath, &vFileNames, &vFileFilter);


	// Create the text item and set its text;
	for (unsigned int i = 0; i < vFileNames.size(); i++)
	{
		item = new CEGUI::ListboxTextItem( vFileNames.at(i) );			// Create the textitem;
		item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );	// Set the brush;
		mListbox->addItem( item );						// Add the item;
	}


	return true;
}


void ModelMenuExportDotSceneTab::_exportDotScene(string* name)
{
	bool check = true;

	Ogre::SceneManager* mSceneMgr = Ogre::Root::getSingleton().getSceneManager( "scene_manager" );
	vector<Ogre::SceneNode*> include;

	// Get the sceneNodes that we want to get and ignore what should be ignored;
	_getSceneNodesWithException( mSceneMgr->getRootSceneNode(), &include );
	
	/*** Export the DotScene ***/
	check = mEditManager->exportDotScene( *name, &include );

	if ( !check )
	{
		string title = "ERROR";
		string functionName = "void ModelMenuExportDotSceneTab::_exportDotScene(string* name)";
		string errorMsg = "An error occured while exporting the DotScene file. Possibly an invalid path was used.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}
		else
		{
			cout << "ERROR: void ModelMenuExportDotSceneTab::_exportDotScene(string* name): mGuiMessageBox could not allocate heap memory";
			return;
		}
	}
} 

void ModelMenuExportDotSceneTab::_getSceneNodesWithException(Ogre::SceneNode *n, vector<Ogre::SceneNode*> *include)
{
	string tmp;

	tmp = n->getName();


	// We use "sn_" in the beginning of our scenenodes to determine which ones we want to save;	
	if ( tmp.compare(0,3,"sn_") == 0 )
	{
		include->push_back(n);	
	}

	
	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	while(object_it.hasMoreElements())
	{
		object_it.getNext();
	}

	while(node_it.hasMoreElements())
	{
		_getSceneNodesWithException( (Ogre::SceneNode*) node_it.getNext(), include );
	}
}




// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool ModelMenuExportDotSceneTab::event_editBox_activated(const CEGUI::EventArgs& pEventArgs)
{
	if ( mNameEditbox->isActive() )
	{
		// We set the GUI windows to not busy;
		SingletonGUIWindowStatus::busy = true;
	}
	else
	{
		// We set the GUI windows to busy;
		SingletonGUIWindowStatus::busy = false;
	}

	return true;	
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool ModelMenuExportDotSceneTab::event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs)
{
	if ( mNameEditbox->isActive() )
	{
		// We set the GUI windows to not busy;
		SingletonGUIWindowStatus::busy = true;
	}
	else
	{
		// We set the GUI windows to busy;
		SingletonGUIWindowStatus::busy = false;
	}

	return true;	
}

bool ModelMenuExportDotSceneTab::event_sheet_mouseDown(const CEGUI::EventArgs& pEventArgs)
{
	mNameEditbox->deactivate();

	return true;
}

bool ModelMenuExportDotSceneTab::event_listBoxTextToEditBox(const CEGUI::EventArgs& pEventArgs)
{
	CEGUI::ListboxItem* item;
	

	try
	{	
		item = mListbox->getFirstSelectedItem();
	}
	catch(...)
	{
		cout << "bool ModelMenuExportDotSceneTab::event_listBoxTextToEditBox(...): Error in: " << 
			"mListbox->getListboxItemFromIndex()." << endl;

		return false;
	}

	// If the item returns NULL than there was nothing selected and we return;
	if ( item == NULL )
	{
		return false;
	}


	// Get the selected listbox item string and put it into the editbox;
	mNameEditbox->setText( item->getText() );

	return true;
}

bool ModelMenuExportDotSceneTab::event_reloadListbox(const CEGUI::EventArgs& pEventArgs)
{
	// First Clear the listbox;
	mListbox->resetList();

	// Now load the listbox;
	return _loadDotSceneListBox();
}

bool ModelMenuExportDotSceneTab::event_exportDotSceneFile(const CEGUI::EventArgs& pEventArgs)
{
	// Get the text from the editbox;
	string fileName = mNameEditbox->getText().c_str();


	if ( fileName == "" )
	{
		return false;
	}


	/*** Export the dotscene File ***/
	try
	{
		_exportDotScene(&fileName);
	}
	catch(...) 
	{
		return false;
	}


	return true;
}
 



