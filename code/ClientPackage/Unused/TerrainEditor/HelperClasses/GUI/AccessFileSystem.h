/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef ACCESSFILESYSTEM
#define ACCESSFILESYSTEM


#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/path.hpp>

//using namespace boost::filesystem;


class AccessFileSystem
{
public:
	bool getDirectoryExist(const boost::filesystem::path &directory);
	void getRecursiveDirectoriesWithFullPath(const boost::filesystem::path &directory, std::vector<std::string> *vStr);
	void getDirectories(const boost::filesystem::path &directory, std::vector<std::string> *vStr);
	void getFiles(const boost::filesystem::path & directory, std::vector<std::string> *vStr);
	void getRecursiveFiles(const boost::filesystem::path & directory, std::vector<std::string> *vFiles);
	void getFilesWithName(const boost::filesystem::path & directory, std::vector<std::string> *vFiles, std::string* strException);
	void getRecursiveFilesWithName(const boost::filesystem::path & directory, std::vector<std::string> *vFiles, std::string* strException);

	bool getHasMatchExtension(std::string* str1, std::string *str2);
	void getFilesWithExtension(const boost::filesystem::path & directory, std::vector<std::string> *vStr, std::string *strExtension);
	void getRecursiveFilesWithExtension(const boost::filesystem::path & directory, std::vector<std::string> *vFiles, std::string *strExtension);
	void getFullPathRecursiveFilesWithExtension(const boost::filesystem::path & directory, std::vector<std::string> *vFiles, std::string *strExtension);

	bool getCanMoveUpDir(std::string *str, std::string *result);
	bool getCanMoveIntoDir(std::string *currentDir, std::string *folderName, std::string *result);

	bool getInitialPath(std::string *str);
};



#endif


