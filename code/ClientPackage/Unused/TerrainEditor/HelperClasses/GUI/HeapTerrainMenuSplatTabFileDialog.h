/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HEAPTERRAINMENUSPLATTAB_FILEDIALOG_H
#define HEAPTERRAINMENUSPLATTAB_FILEDIALOG_H 


#include <CEGUI/CEGUI.h>
#include <Ogre.h>
#include <vector>
#include <string>
#include <iostream>

#include "WorldEditorProperties.h"
#include "EditManager.h"
#include "AccessFileSystem.h"
#include "GUIMessageBox.h"
#include "ModelEditorMainTab.h"



class TerrainMenuSplatTabFileDialog
{
protected:
	GUIMessageBox* mGuiMessageBox;

	CEGUI::Window* mParentwin;
	CEGUI::Window* mMainChildwin;

	CEGUI::Listbox* mListbox;
	CEGUI::Window* mFileNameLabel;
	CEGUI::Editbox* mNameEditbox;
	CEGUI::PushButton* mReloadButton;
	CEGUI::PushButton* mOkButton;
	CEGUI::PushButton* mCancelButton;

	bool _loadMapListBox();

	bool event_mOkButton_click(const CEGUI::EventArgs& pEventArgs);
	bool event_mCancelButton_click(const CEGUI::EventArgs& pEventArgs);

	bool event_listBoxTextToEditBox(const CEGUI::EventArgs& pEventArgs);
	bool event_reloadListbox(const CEGUI::EventArgs& pEventArgs);

	// Prevent keyboard actons while in the editboxes;
	bool event_editBox_activated(const CEGUI::EventArgs& pEventArgs);
	bool event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs);
public:
	TerrainMenuSplatTabFileDialog();
	~TerrainMenuSplatTabFileDialog();

	CEGUI::Window* const getWindow() const;
	bool addChildWindow();	
};


class HeapTerrainMenuSplatTabFileDialog
{
protected:
	TerrainMenuSplatTabFileDialog* mTerrainMenuSplatTabFileDialog;
public:
	HeapTerrainMenuSplatTabFileDialog();
	~HeapTerrainMenuSplatTabFileDialog();
	
	CEGUI::Window* const getWindow() const;
	void toggle();
	bool isVisible() const;
}; 



#endif

