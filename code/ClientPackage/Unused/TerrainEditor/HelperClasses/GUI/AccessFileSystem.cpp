/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "AccessFileSystem.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace boost::filesystem;
using namespace std;



bool AccessFileSystem::getDirectoryExist(const path & directory)
{
	try
	{
		if( exists( directory ) )
		{
			return true;
		}
	}
	catch(...)
	{
		return false;
	}

	return false;
}

void AccessFileSystem::getRecursiveDirectoriesWithFullPath(const path & directory, vector<string> *vStr)
{
	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( is_directory( *iter ) )
      			{
				// Store the string;
				vStr->push_back( iter->path().native_directory_string() );

				// Call the function resursively;
				getRecursiveDirectoriesWithFullPath(*iter, vStr);
      			}
  		}
	}
}

void AccessFileSystem::getDirectories(const path & directory, vector<string> *vStr)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void AccessFileSystem::getDirectories(...)";
	string strLogMessage;

	directory_iterator end_iter;


	if( exists( directory ) )
	{
		try
		{
			for ( directory_iterator dir_itr( directory ); dir_itr != end_iter; ++dir_itr )
			{
				try
				{
					if ( is_directory(dir_itr->status()) )
					{
						vStr->push_back( dir_itr->path().filename() );
					}
			
				}
				catch (const std::exception &ex)
				{
					strLogMessage = dir_itr->path().filename() + " " + ex.what();
					ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
				}
			}
		}
		catch(const std::exception &ex)
		{
			strLogMessage = ex.what();
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}
	}
}

void AccessFileSystem::getFiles(const path & directory, vector<string> *vStr)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void AccessFileSystem::getFiles(...)";
	string strLogMessage;

	directory_iterator end_iter;


	if( exists( directory ) )
	{
		try
		{
			for ( directory_iterator dir_itr( directory ); dir_itr != end_iter; ++dir_itr )
			{
				try
				{
					if ( !is_directory(dir_itr->status()) )
					{
						vStr->push_back( dir_itr->path().filename() );
					}
			
				}
				catch (const std::exception &ex)
				{
					strLogMessage = dir_itr->path().filename() + " " + ex.what();
					ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
				}
			}
		}
		catch(const std::exception &ex)
		{
			strLogMessage = ex.what();
			ogre3dLogManager->logErrorMessage(strLogFunctionName, strLogMessage);
		}
	}
}

void AccessFileSystem::getRecursiveFiles(const path & directory, vector<string> *vFiles)
{
	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( is_directory( *iter ) )
      			{
				getRecursiveFiles(*iter, vFiles);
      			}
      			else
			{
				/*** Get the file name ***/
				vFiles->push_back( iter->leaf() );
			}
  		}
	}
}

void AccessFileSystem::getFilesWithName(const path & directory, vector<string> *vFiles, string* strException)
{
	size_t found = 0;
	string tmp;

	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( !is_directory( *iter ) )
      			{
				// Get the string;
				tmp = iter->leaf();

				// found out if the string contrains what we dont' want it to have;
				found = tmp.find( *strException );

				// If the string doesn't have want we don't want than keep it;
				if (found != string::npos)
				{
					/*** Get the file name ***/
					vFiles->push_back(tmp);
				}
      			}
  		}
	}
}

void AccessFileSystem::getRecursiveFilesWithName(const path & directory, vector<string> *vFiles, string* strException)
{
	size_t found = 0;
	string tmp;

	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( is_directory( *iter ) )
      			{
				getRecursiveFilesWithName(*iter, vFiles, strException);
      			}
      			else
			{
				// Get the string;
				tmp = iter->leaf();

				// found out if the string contrains what we dont' want it to have;
				found = tmp.find( *strException );

				// If the string doesn't have want we don't want than keep it;
				if (found != string::npos)
				{
					/*** Get the file name ***/
					vFiles->push_back(tmp);
				}
			}
  		}
	}
}



bool AccessFileSystem::getHasMatchExtension(string* str1, string *str2)
{
	unsigned int str1_size = str1->size();
	unsigned int str2_size = str2->size();
	unsigned int position = 0;


	if ( str1_size <= str2_size )
	{
		position = str2_size - str1_size;

		//cout << "Position=" << position << endl;
		//cout << "str1_size=" << str1_size << endl;
		//cout << "str2_size=" << str2_size << endl;

		if ( str2->compare(position, str1_size, *str1) == 0 )
		{
			return true;
		}

	}

	return false;
}

void AccessFileSystem::getFilesWithExtension(const path & directory, vector<string> *vStr, string *strExtension)
{
	string tmp;

	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( !is_directory( *iter ) )
      			{
				// Get the string;
				tmp = iter->leaf();

				// Check if the string has the extension we want;
				if ( getHasMatchExtension(strExtension, &tmp) )
				{
					/*** Get the file name ***/
					vStr->push_back(tmp);
				}
      			}
  		}
	}
}

void AccessFileSystem::getRecursiveFilesWithExtension(const path & directory, vector<string> *vFiles, string *strExtension)
{
	string tmp;

	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( is_directory( *iter ) )
      			{
				getRecursiveFilesWithExtension(*iter, vFiles, strExtension);
      			}
      			else
			{
				// Get the string;
				tmp = iter->leaf();

				// Check if the string has the extension we want;
				if ( getHasMatchExtension(strExtension, &tmp) )
				{
					/*** Get the file name ***/
					vFiles->push_back(tmp);
				}
			}
  		}
	}
}

void AccessFileSystem::getFullPathRecursiveFilesWithExtension(const path & directory, vector<string> *vFiles, string *strExtension)
{
	string tmp;

	if( exists( directory ) )
	{
		directory_iterator end;
		for( directory_iterator iter(directory); iter != end; ++iter )
		{
			if ( is_directory( *iter ) )
      			{
				getFullPathRecursiveFilesWithExtension(*iter, vFiles, strExtension);
      			}
      			else
			{
				// Get the string;
				tmp = iter->leaf();

				// Check if the string has the extension we want;
				if ( getHasMatchExtension(strExtension, &tmp) )
				{
					/*** Get the file name ***/
					vFiles->push_back( iter->path().native_directory_string() );
				}
			}
  		}
	}
}


bool AccessFileSystem::getCanMoveUpDir(string *str, string *result)
{
	const unsigned long tmpStrSize = str->size();
	unsigned endIndex = 0;

	string tmpStr;	


	if (tmpStrSize > 0)
	{
		for (unsigned long i = tmpStrSize - 1; i >= 0; i--)
		{
			if (str->at(i) == '/')
			{
				// The first character may be '/'. If it is then we keep going;
				if (i == 0)
				{
					tmpStr = "/";
					endIndex = 1;
		
					break;
				}
				else if (i != (tmpStrSize-1) )
				{
					endIndex = i;

					break;
				}
			}
		}


		// Copy the string str from 0 to endIndex where the '/' was seen;
		tmpStr = str->substr(0, endIndex);

		// Check if the directory exists and return true if it does;
		if ( AccessFileSystem::getDirectoryExist(tmpStr) )
		{
			// Set the result;
			*result = tmpStr;

			return true;
		}
	}
	

	return false;
}

bool AccessFileSystem::getCanMoveIntoDir(string *currentDir, string *folderName, string *result)
{
	const unsigned long tmpStrSize = currentDir->size();
	string tmpStr;	


	if (tmpStrSize > 0)
	{
		// If the string doesn't have a '/' at the end and the string 'folderName'
		//   Doesn't have a '/' at index 0 then append a '/' to currentDir string;
		if (currentDir->at(tmpStrSize - 1) != '/' && folderName->at(0) != '/')
		{
			currentDir->append("/");
		}

		// Append the foldername to the string;
		currentDir->append(*folderName);

		// Put the contents into a regular string to use in the function below;
		tmpStr = *currentDir;

		// Check if the directory exists and return true if it does;
		if ( AccessFileSystem::getDirectoryExist(tmpStr) )
		{
			// set the result;
			*result = tmpStr;

			return true;
		}
	}


	return false;
}


bool AccessFileSystem::getInitialPath(string *str)
{
	path full_path( initial_path<path>() );


	if ( AccessFileSystem::getDirectoryExist(full_path) )
	{
		*str = full_path.native_directory_string();

		return true;
	}


	return false;
}

