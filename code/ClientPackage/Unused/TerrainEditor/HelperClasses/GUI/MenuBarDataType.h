/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MENUBAR_DATATYPE_H
#define MENUBAR_DATATYPE_H

#include <vector>
#include <string>
#include <iostream>



class MenuBarDataType
{
protected:
	std::string mParentName;
	std::vector <std::string> mVchildName;
	std::vector <unsigned int> mVchildNumber;
public:
	void setParent(std::string parentName);
	void setChild(std::string childName, unsigned int number);
	std::string* getParentName();
	unsigned int getChildSize();
	std::string* getChildName(unsigned int number);
	unsigned int getChildNumber(unsigned int number);
};
 

#endif

