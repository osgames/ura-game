/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CustomScrollPane.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace CEGUI;
using namespace std;



CustomScrollPane::CustomScrollPane()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "CustomScrollPane::CustomScrollPane()";
	string strLogMessage;


	/*** Allocate the heap memory ***/
	mChildWindow_x_pos = new long;
	mChildWindow_x_rightSpace = new long;
	mChildWindow_y_pos = new long;
	mChildWindow_y_size = new long;
	mChildWindow_y_space = new long;
	mHighligther_y_topSpace = new long;
	mHighligther_y_bottomSpace = new long;

	mBStretchedImage = new bool;


	/*** Check for heap memory allocation errors ***/
	if (!mChildWindow_x_pos)
	{
		strLogMessage = "mChildWindow_x_pos";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mChildWindow_x_rightSpace)
	{
		strLogMessage = "mChildWindow_x_rightSpace";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mChildWindow_y_pos)
	{
		strLogMessage = "mChildWindow_y_pos";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mChildWindow_y_size)
	{
		strLogMessage = "mChildWindow_y_size";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mChildWindow_y_space)
	{
		strLogMessage = "mChildWindow_y_space";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}


	if (!mHighligther_y_topSpace)
	{
		strLogMessage = "mHighligther_y_topSpace";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mHighligther_y_bottomSpace)
	{
		strLogMessage = "mHighligther_y_bottomSpace";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}

	if (!mBStretchedImage)
	{
		strLogMessage = "mBStretchedImage";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}
}

CustomScrollPane::~CustomScrollPane()
{
	/*** Free the heap memory ***/

	if (mChildWindow_x_pos)
	{
		delete mChildWindow_x_pos;
		mChildWindow_x_pos = NULL;
	}

	if (mChildWindow_x_rightSpace)
	{
		delete mChildWindow_x_rightSpace;
		mChildWindow_x_rightSpace = NULL;
	}

	if (mChildWindow_y_pos)
	{
		delete mChildWindow_y_pos;
		mChildWindow_y_pos = NULL;
	}

	if (mChildWindow_y_size)
	{
		delete mChildWindow_y_size;
		mChildWindow_y_size = NULL;
	}

	if (mChildWindow_y_space)
	{
		delete mChildWindow_y_space;
		mChildWindow_y_space = NULL;
	}


	if (mHighligther_y_topSpace)
	{
		delete mHighligther_y_topSpace;
		mHighligther_y_topSpace = NULL;
	}

	if (mHighligther_y_bottomSpace)
	{
		delete mHighligther_y_bottomSpace;
		mHighligther_y_bottomSpace = NULL;
	}

	if (mBStretchedImage)
	{
		delete mBStretchedImage;
		mBStretchedImage = NULL;
	}
}

void CustomScrollPane::set(CEGUI::Window *parentWindow, vector<CEGUI::Window*> *childWindows, 
	string *scrollpaneName, string *imageSet, string *imageName, bool stretchImage,
	long childWindow_x_pos, long childWindow_x_rightSpace, 
	long childWindow_y_pos, long childWindow_y_size, long childWindow_y_space, 
	long highligther_y_topSpace, long highligther_y_bottomSpace)	
{
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	long childWindow_x_size = 0;
	unsigned long scrollPane_y_size = 0;

	// Variables to highlight the first window if it exists;
	long highligther_y_pos = 0;
	long highligther_y_size = 0;

	// Combine the string to make the whole image property set string;
	string imageStrCombine = "set:" + *imageSet + " image:" + *imageName;


	/*** Set all the variables ***/

	// store the parent window;
	mParentWindow = parentWindow;

	// store the child windows;
	mVChildWindows = childWindows;


	// Store the windows x and y positions;
	*mChildWindow_x_pos = childWindow_x_pos;
	*mChildWindow_y_pos = childWindow_y_pos;

	// Store the y Size;
	*mChildWindow_y_size = childWindow_y_size;

	// Store the windows x and y spaces apart;
	*mChildWindow_x_rightSpace = childWindow_x_rightSpace;
	*mChildWindow_y_space = childWindow_y_space;

	// Store the Highlighter variables;
	*mHighligther_y_topSpace = highligther_y_topSpace;
	*mHighligther_y_bottomSpace = highligther_y_bottomSpace;


	// Store the value that determines if the windows are stretched or centered;
	*mBStretchedImage = stretchImage;


	// We set the first window as the selected window;
	mSelectedWindow.push_back(0);


	/*************************************/
	/*** Create the image high-lighter ***/
	/*************************************/
	mImageHighlighter = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage", *scrollpaneName + "_imagehighlighter");
	mImageHighlighter->setProperty("FrameEnabled", "False");
	mImageHighlighter->setProperty("BackgroundEnabled", "false");
	mImageHighlighter->setProperty( "Image", imageStrCombine );
	// Hide and disable the mImageHighlighter window;
	mImageHighlighter->disable();
	mImageHighlighter->hide();


	/***************************/
	/*** Create a ScrollPane ***/
	/***************************/

	// Calculate the scrollpane y size;
	scrollPane_y_size = (*mChildWindow_y_size + *mChildWindow_y_space) * (mVChildWindows->size() + 1);

	mScrollPane = (CEGUI::ScrollablePane*) windowManager.createWindow("TaharezLook/ScrollablePane", *scrollpaneName);
	mScrollPane->setArea( URect( UDim(0,0),UDim(0,0), UDim(1,0),UDim(1,0) ) );
	mScrollPane->setContentPaneAutoSized(false);
	mScrollPane->setContentPaneArea(Rect(0,0,0,scrollPane_y_size));

	// Get the main parentWindow pixel width and subtract the window x right space;
	childWindow_x_size = mParentWindow->getPixelRect().getWidth() - *mChildWindow_x_rightSpace;


	/*** Set the mousedown events for the windows. This way we can call a function to highlight them ***/
	for ( unsigned long i = 0; i < mVChildWindows->size(); i++ )
	{
		mVChildWindows->at(i)->subscribeEvent( CEGUI::Window::EventMouseButtonDown, 
			CEGUI::Event::Subscriber( &CustomScrollPane::event_windowClicked, this ) );
	}

	/*** Set the size and positions of the child windows ***/
	for ( unsigned long i = 0; i < mVChildWindows->size(); i++ )
	{
		mVChildWindows->at(i)->setSize(CEGUI::UVector2(CEGUI::UDim(0, childWindow_x_size), 
			CEGUI::UDim(0, *mChildWindow_y_size)));
		mVChildWindows->at(i)->setPosition( CEGUI::UVector2(CEGUI::UDim(0, *mChildWindow_x_pos), 
			CEGUI::UDim(0, *mChildWindow_y_pos) ) );

		*mChildWindow_y_pos += *mChildWindow_y_size + *mChildWindow_y_space;
	}


	/**************************/
	/*** Attach the Windows ***/
	/**************************/

	// Add the childwindows to the mScrollPane;
	for ( unsigned long i = 0; i < mVChildWindows->size(); i++ )
	{
		mScrollPane->addChildWindow( mVChildWindows->at(i) );
	}

	// Attach the mImageHighlighter to the mScrollPane;
	mScrollPane->addChildWindow( mImageHighlighter );

	// Attach the mScrollPane to the mParentWindow;
	mParentWindow->addChildWindow( mScrollPane );

	
	/**********************************************/
	/*** Highlight the first image if it exists ***/
	/**********************************************/
	if ( mVChildWindows->size() )
	{
		highligther_y_pos = mVChildWindows->at(0)->getYPosition().d_offset + *mHighligther_y_topSpace;
		highligther_y_size = *mChildWindow_y_size + *mHighligther_y_bottomSpace;
	
		// Show the mImageHighlighter window if its invisible;
		if ( !mImageHighlighter->isVisible() )
		{
			mImageHighlighter->show();
		}
	

		// Set the Size and position of the mImageHighlighter window;
		mImageHighlighter->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.0F, highligther_y_size)));
		mImageHighlighter->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), 
			CEGUI::UDim(0.0F, highligther_y_pos) ) );

	
		// Move the childwindow to the front so that the highlighter wont block it;
		mVChildWindows->at(0)->moveToFront();
	}
}

// Here we handle how the windows react on resize. if mBStretchedImage == true then stretch the image;
// Else if mBStretchedImage == false, then we just center the image and don't resize it;
// We also have to resize the ImageHighlighter here;
void CustomScrollPane::resize()
{
	if ( *mBStretchedImage )
	{
		/*** Stretch the windows on resize ***/

		const long window_x_size = mParentWindow->getPixelRect().getWidth() - *mChildWindow_x_rightSpace;

		for ( unsigned long i = 0; i < mVChildWindows->size(); i++ )
		{
			mVChildWindows->at(i)->setSize(CEGUI::UVector2(CEGUI::UDim(0.0F, window_x_size),
				CEGUI::UDim(0.0F, *mChildWindow_y_size)));
		}
	}
	else
	{
		/*** Center the windows on resize and keep the same size ***/

		const long window_x_pos = (mParentWindow->getPixelRect().getWidth() / 2) - ( *mChildWindow_y_size / 2) ;

		// resize all the windows x size. The y size stays absolute and non changing;
		for ( unsigned long i = 0; i < mVChildWindows->size(); i++ )
		{
			mVChildWindows->at(i)->setXPosition( CEGUI::UDim(0, window_x_pos) );
			mVChildWindows->at(i)->setWidth( CEGUI::UDim(0, *mChildWindow_y_size) );
		}
	}	
}

/*** When a window is clicked we make an image to highlight it ***/
bool CustomScrollPane::event_windowClicked(const CEGUI::EventArgs &pEventArgs)
{
	CEGUI::Window* window = NULL;
	const WindowEventArgs windowEventArgs = static_cast<const WindowEventArgs&>(pEventArgs);
	window = windowEventArgs.window;
	
	const long highligther_y_pos = window->getYPosition().d_offset + *mHighligther_y_topSpace;
	const long highligther_y_size = *mChildWindow_y_size + *mHighligther_y_bottomSpace;


	// Set the Size and position of the mImageHighlighter window;
	mImageHighlighter->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.0F, highligther_y_size)));
	mImageHighlighter->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0.0F), 
		CEGUI::UDim(0.0F, highligther_y_pos) ) );


	// move the window to front and on top of the mImageHighlighter;
	window->moveToFront();


	// Store the selected window variable;
	for (unsigned long i = 0; i < mVChildWindows->size(); i++)
	{
		// Compare the window pointers to know what window number it is;
		if ( mVChildWindows->at(i) == window )
		{
			mSelectedWindow.at(0) = i;
		}
	}

	return true;
}

/*** Highlight the first image if it exists and store the selected window variable ***/
bool CustomScrollPane::setWindow(const long windowNumber)
{
	long highligther_y_pos = 0;
	long highligther_y_size = 0;
	const unsigned long childWindowSize = mVChildWindows->size();

	// Make sure the imageNumber is within bounds before highlighting it;
	if ( windowNumber >= 0 && windowNumber < childWindowSize )
	{
		highligther_y_pos = mVChildWindows->at(windowNumber)->getYPosition().d_offset + *mHighligther_y_topSpace;
		highligther_y_size = *mChildWindow_y_size + *mHighligther_y_bottomSpace;
	
		// Show the mImageHighlighter window if its invisible;
		if ( !mImageHighlighter->isVisible() )
		{
			mImageHighlighter->show();
		}
	

		// Set the Size and position of the mImageHighlighter window;
		mImageHighlighter->setSize(CEGUI::UVector2(CEGUI::UDim(1.0F, 0.0F), CEGUI::UDim(0.0F, highligther_y_size)));
		mImageHighlighter->setPosition( CEGUI::UVector2(CEGUI::UDim(0.0F, 0), 
			CEGUI::UDim(0.0F, highligther_y_pos) ) );
	

		// Move the childwindow to the front so that the highlighter wont block it;
		mVChildWindows->at(windowNumber)->moveToFront();

		// Store the selected window variable;
		mSelectedWindow.at(0) = windowNumber;
	}

	return true;
}

unsigned long CustomScrollPane::getSelectedWindow()
{
	return mSelectedWindow.at(0);
}

unsigned long CustomScrollPane::getWindowCount()
{
	return mVChildWindows->size();
}

