/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TERRAINMENU_BRUSHTAB_MISCTAB_H
#define TERRAINMENU_BRUSHTAB_MISCTAB_H


#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

#include <EditManager.h>


class TerrainMenuBrushTabMain;



class TerrainMenuBrushTabMiscTab
{
protected:
	TerrainMenuBrushTabMain* mMainBrushTab;

	EditManager* mEditManager;


	unsigned long mMaxIntensity;
	unsigned long mIntensity;

	unsigned long mMaxScale;
	unsigned long mScale;

	/*** The Windows to add ***/
	CEGUI::Window* mParentwin;

	CEGUI::Window* mMaxIntensityText;
	CEGUI::Scrollbar* mMaxIntensityScrollbar;

	CEGUI::Window* mMaxScaleText;
	CEGUI::Scrollbar* mMaxScaleScrollbar;

	CEGUI::Window* mFrameBelowMaxBrushScaleScrollbar;

	CEGUI::Window* mApplyButton;

	bool event_mMaxIntensityScrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_mMaxScaleScrollbar_change(const CEGUI::EventArgs& pEventArgs);
	bool event_apply(const CEGUI::EventArgs& pEventArgs);
public:
	TerrainMenuBrushTabMiscTab();
	void prepareToQuit();

	CEGUI::Window* getWindow();
	bool addChildWindow(TerrainMenuBrushTabMain *mainBrushTab);	
}; 


#endif


