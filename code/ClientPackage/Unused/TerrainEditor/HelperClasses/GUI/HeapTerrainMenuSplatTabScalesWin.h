/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HEAP_TERRAIN_MENU_SPLAT_TAB_SCALES_WIN_H
#define HEAP_TERRAIN_MENU_SPLAT_TAB_SCALES_WIN_H


#include <CEGUI/CEGUI.h>
#include <Ogre.h>
#include <string>
#include <iostream>

#include "EditManager.h"



class TerrainMenuSplatTabScalesWin
{
protected:
	CEGUI::FrameWindow* mMainWindow;

	CEGUI::Window* mLabalSplatTextures;

	CEGUI::Window* mLabalSplatScaleX;
	CEGUI::Scrollbar* mSplatScaleX_Scrollbar;

	CEGUI::Window* mLabalSplatScaleY;
	CEGUI::Scrollbar* mSplatScaleY_Scrollbar;

	CEGUI::Window* mLabalSplatScaleZ;
	CEGUI::Scrollbar* mSplatScaleZ_Scrollbar;

	CEGUI::Window* mLabalSplatScaleW;
	CEGUI::Scrollbar* mSplatScaleW_Scrollbar;

	CEGUI::Window* mFrame;

	CEGUI::Window* mLabalDetailTextures;

	CEGUI::Window* mLabalDetailScaleX;
	CEGUI::Scrollbar* mDetailScaleX_Scrollbar;

	CEGUI::Window* mLabalDetailScaleY;
	CEGUI::Scrollbar* mDetailScaleY_Scrollbar;

	CEGUI::Window* mFrame2;

	CEGUI::PushButton* mButtonClose;

	Ogre::Vector4 mSplatScales;
	Ogre::Vector4 mDetailScales;

	void _updateScrollbars();

	bool event_Scrollbar_change(const CEGUI::EventArgs &pEventArgs);
	bool event_mButtonDefault_click(const CEGUI::EventArgs &pEventArgs);
	bool event_mButtonClose_click(const CEGUI::EventArgs &pEventArgs);
public:
	TerrainMenuSplatTabScalesWin();
	~TerrainMenuSplatTabScalesWin();

	CEGUI::Window* const getWindow() const;
	bool addWindow();

	void reset();
	const std::vector<Ogre::Vector4>* const getARGB();
}; 




class HeapTerrainMenuSplatTabScalesWin
{
protected:
	TerrainMenuSplatTabScalesWin* mTerrainMenuSplatTabScalesWin;
public:
	HeapTerrainMenuSplatTabScalesWin();
	~HeapTerrainMenuSplatTabScalesWin();
	
	CEGUI::Window* const getWindow() const;
	void toggle();
	bool isVisible() const;
}; 




#endif

