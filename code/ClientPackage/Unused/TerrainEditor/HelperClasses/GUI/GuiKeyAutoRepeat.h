/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIKEYAUTOREPEAT_H
#define GUIKEYAUTOREPEAT_H


/****************/
/*** INCLUDES ***/
/****************/
#include <vector>
#include <string>
#include <iostream>
#include <set>

#include <OIS.h>
#include <Ogre.h>



class CustomTimerType
{
protected:
	Ogre::Timer* mTimer;
	bool* mCalledAlready;
public:
	CustomTimerType()
	{
		mCalledAlready = new bool;
		mTimer = new Ogre::Timer;

		if ( !mCalledAlready )
		{
			std::cout << "ERROR: CustomTimerType::CustomTimerType(): Could not allocate memory for 'mCalledAlready'."
				<< std::endl;
		}

		if ( !mTimer )
		{
			std::cout << "ERROR: CustomTimerType::CustomTimerType(): Could not allocate memory for 'mTimer'."
				<< std::endl;
		}		


		// Set calledAlready to false so that it shows we haven't used any keys yet;
		*mCalledAlready = false;
	}

	~CustomTimerType()
	{
		if ( mCalledAlready )
		{
			delete mCalledAlready;
		}

		if ( mTimer )
		{
			delete mTimer;
		}		
	}

	inline void setCalledAlready(bool bCalledAlready)
	{
		*mCalledAlready = bCalledAlready;
	}

	inline bool isCalledAlready()
	{
		return *mCalledAlready;
	}

	inline void resetTimer()
	{
		mTimer->reset();
	}

	inline unsigned long getMilliseconds()
	{
		return mTimer->getMilliseconds();
	}
};


class GuiKeyAutoRepeat
{
protected:
	std::map<unsigned int, CustomTimerType*> mMap;
	CustomTimerType* mTimerType;
public:
	GuiKeyAutoRepeat();
	~GuiKeyAutoRepeat();

void keys();

	bool hasKey(unsigned int key);
	void addKey(unsigned int key);
	void removeKey(unsigned int key);
};


#endif


