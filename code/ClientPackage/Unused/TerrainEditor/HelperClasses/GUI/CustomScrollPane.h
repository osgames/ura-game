/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CUSTOMSCROLLPANE_H
#define CUSTOMSCROLLPANE_H

#include <CEGUI/CEGUI.h>
#include <vector>
#include <string>
#include <iostream>



class CustomScrollPane
{
protected:
	CEGUI::Window* mParentWindow;
	CEGUI::ScrollablePane* mScrollPane;
	CEGUI::Window* mImageHighlighter;
	std::vector <CEGUI::Window*> *mVChildWindows;

	long* mChildWindow_x_pos;
	long* mChildWindow_x_rightSpace;
	long* mChildWindow_y_pos;
	long* mChildWindow_y_size;
	long* mChildWindow_y_space;

	long* mHighligther_y_topSpace;
	long* mHighligther_y_bottomSpace;

	// Store the value that determines if the windows are stretched or centered;
	bool* mBStretchedImage;

	// Stores the selected window value;
	std::vector <unsigned long> mSelectedWindow;
public:
	CustomScrollPane();
	~CustomScrollPane();

	// Note: when we talk about spaces were talking about how many spaces away from that part of the border;
	void set(CEGUI::Window *parentWindow, std::vector<CEGUI::Window*> *childWindows, 
		std::string *scrollpaneName, std::string *imageSet, std::string *imageName, bool stretchImage,
		long childWindow_x_pos, long childWindow_x_rightSpace, 
		long childWindow_y_pos, long childWindow_y_size, long childWindow_y_space,
		long highligther_y_topSpace, long highligther_y_bottomSpace);

	// Here we handle how the windows react on resize. if mBStretchedImage == true then stretch the image;
	// Else if mBStretchedImage == false, then we just center the image and don't resize it;
	// We also have to resize the ImageHighlighter here;
	void resize();

	bool setWindow(const long windowNumber);					// Select the window number;
	unsigned long getSelectedWindow();					// Get the selected window number
	unsigned long getWindowCount();	

	bool event_windowClicked(const CEGUI::EventArgs &pEventArgs);	// Highlight the window when its clicked;
}; 


#endif
