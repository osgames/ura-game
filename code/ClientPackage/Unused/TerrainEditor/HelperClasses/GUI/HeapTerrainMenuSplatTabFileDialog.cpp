/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HeapTerrainMenuSplatTabFileDialog.h"
#include "WorldEditorProperties.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "GUIManager.h"
#include "GUI.h"
#include "TerrainMenu.h"
#include "TerrainMenuSplatTab.h"
#include "SplatManager.h"
#include "ModelMenu.h"
#include "ModelEditorMainTab.h"


using namespace Ogre;
using namespace CEGUI;
using namespace std;



TerrainMenuSplatTabFileDialog::TerrainMenuSplatTabFileDialog() :
					mParentwin(NULL),
					mGuiMessageBox(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "TerrainMenuSplatTabFileDialog::TerrainMenuSplatTabFileDialog()";
	string strLogMessage;


	/*** allocate heap memory for the gui messagebox ***/
	mGuiMessageBox = new GUIMessageBox;

	if (mGuiMessageBox == NULL)
	{
		strLogMessage = "mGuiMessageBox";
		ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);
	}
}

TerrainMenuSplatTabFileDialog::~TerrainMenuSplatTabFileDialog()
{
	if ( mGuiMessageBox )
	{
		delete mGuiMessageBox;
		mGuiMessageBox = NULL;
	}

	// Destroy the cegui window;
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);	
}

CEGUI::Window* const TerrainMenuSplatTabFileDialog::getWindow() const
{
	return mParentwin;
}

bool TerrainMenuSplatTabFileDialog::addChildWindow()
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();

	CEGUI::Window* const sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();
	


	// Create the GuiMessageBox and add it to the sheet;
	if ( mGuiMessageBox )
	{
		mGuiMessageBox->addWindow();
	}


	/******************************/
	/*** Create the main window ***/
	/******************************/
	mParentwin = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow", "TerrainMenuSplatTabFileDialog_mParentwin");
	mParentwin->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0.0F), CEGUI::UDim(0.8, 0.0F)));
	mParentwin->setText("  Change Texture");


	/************************************/
	/*** Create the main Child Window ***/
	/************************************/
	mMainChildwin = windowManager.createWindow("TaharezLook/Listbox_woodframe","TerrainMenuSplatTabFileDialog_mMainChildwin");
	mMainChildwin->setPosition( CEGUI::UVector2(CEGUI::UDim(0.036, 0.0F), CEGUI::UDim(0.055, 0.0F) ) );
	mMainChildwin->setSize(CEGUI::UVector2(CEGUI::UDim(0.924, 0.0F), CEGUI::UDim(0.92, 0.0F)));


	/**********************************************************/
	/*** Create the mListbox to select what files to handle ***/
	/**********************************************************/

	// the listbox;
	mListbox = (CEGUI::Listbox *) windowManager.createWindow("TaharezLook/Listbox_woodframe","TerrainMenuSplatTabFileDialog_mMeshListbox");


	/*********************************/
	/*** Create the filename Label ***/
	/*********************************/

	// Name Label;
	mFileNameLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"TerrainMenuSplatTabFileDialog_mFileNameLabel");
	mFileNameLabel->setProperty("FrameEnabled", "False");
	mFileNameLabel->setProperty("BackgroundEnabled", "false");
	mFileNameLabel->setText("File Name:");	// Set the text;
	mFileNameLabel->disable();



	/**********************************************************/
	/*** Create the Editbox to optionally insert a filename ***/
	/**********************************************************/

	// Create the edit box;
	mNameEditbox = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "TerrainMenuSplatTabFileDialog_mNameEditbox");


	/************************************************/
	/*** Create the reload, ok and cancel buttons ***/
	/************************************************/

	// The reload button;
	mReloadButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "TerrainMenuSplatTabFileDialog_mReloadButton");
	mReloadButton->setText("Reload List");

	// The okay button;
	mOkButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "TerrainMenuSplatTabFileDialog_mOkButton");
	mOkButton->setText("Ok");

	// The Cancel button;
	mCancelButton = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button", "TerrainMenuSplatTabFileDialog_mCancelButton");
	mCancelButton->setText("Cancel");


	/**********************************/
	/*** Set the size and positions ***/
	/**********************************/

	float mListbox_x_pos = 0.1f;
	float mListbox_y_pos = 0.06f;
	float mListbox_x_size = 0.8f;
	float mListbox_y_size = 0.63f;

	float mFileNameLabel_y_size = 0.04f;
	float editbox_y_size = 0.04f;
	float button_y_size = 0.04f;
	float button_x_size = 0.35f;


	float mReloadButton_y_pos = mListbox_y_pos + mListbox_y_size + 0.03f;
	float mFileNameLabel_y_pos = mReloadButton_y_pos + button_y_size + 0.025f;
	float mNameEditbox_y_pos =  mFileNameLabel_y_pos + mFileNameLabel_y_size;
	float mOkButton_y_pos =  mNameEditbox_y_pos + editbox_y_size + 0.03f + 0.01;

	float mCancelButton_x_pos = mListbox_x_pos + button_x_size + 0.05f + 0.05f;



	mListbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mListbox_y_pos, 0.0f))); 
	mListbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(mListbox_y_size, 0.0f))); 

	// We want the X position to be auto centered and the Y position to be custom;
	mReloadButton->setYPosition( UDim(mReloadButton_y_pos, 0.0f) ); 
	mReloadButton->setSize(UVector2(UDim(button_x_size, 0.0f), UDim(button_y_size, 0.0f))); 

	mFileNameLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(mListbox_x_pos, 0.0F), CEGUI::UDim(mFileNameLabel_y_pos, 0.0F) ));
	mFileNameLabel->setSize(CEGUI::UVector2(CEGUI::UDim(mListbox_x_size, 0.0F), CEGUI::UDim(mFileNameLabel_y_size, 0.0F)));

	mNameEditbox->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mNameEditbox_y_pos, 0.0f))); 
	mNameEditbox->setSize(UVector2(UDim(mListbox_x_size, 0.0f), UDim(editbox_y_size, 0.0f))); 

	mOkButton->setPosition(UVector2(UDim(mListbox_x_pos, 0.0f), UDim(mOkButton_y_pos, 0.0f))); 
	mOkButton->setSize(UVector2(UDim(button_x_size, 0.0f), UDim(button_y_size, 0.0f))); 

	mCancelButton->setPosition(UVector2(UDim(mCancelButton_x_pos, 0.0f), UDim(mOkButton_y_pos, 0.0f))); 
	mCancelButton->setSize(UVector2(UDim(button_x_size, 0.0f), UDim(button_y_size, 0.0f))); 



	/*************************/
	/*** Handle the events ***/
	/*************************/


	// Handle the listbox selection change;
	mListbox->subscribeEvent( CEGUI::Listbox::EventSelectionChanged, CEGUI::Event::Subscriber( 
		&TerrainMenuSplatTabFileDialog::event_listBoxTextToEditBox, this ) );

	// Handle the listbox reload with the reload button;
	mReloadButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&TerrainMenuSplatTabFileDialog::event_reloadListbox, this ) );

	mNameEditbox->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber( 
			&TerrainMenuSplatTabFileDialog::event_editBox_activated, this ) );

	mNameEditbox->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber( 
			&TerrainMenuSplatTabFileDialog::event_editBox_deactivated, this ) );

	mOkButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&TerrainMenuSplatTabFileDialog::event_mOkButton_click, this ) );

	mCancelButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber( 
			&TerrainMenuSplatTabFileDialog::event_mCancelButton_click, this ) );


	/************************/
	/*** Set the Settings ***/
	/************************/

	// We want main window to be auto aligned vertically and horiz;
	mParentwin->setHorizontalAlignment(CEGUI::HA_CENTRE);
	mParentwin->setVerticalAlignment(CEGUI::VA_CENTRE);

	// We want the X position to be auto centered and the Y position to be custom;
	mReloadButton->setHorizontalAlignment(CEGUI::HA_CENTRE);


	// Only allow this window to be accessed until the ok button is clicked;
	mParentwin->setModalState(true);

	// The editbox should be disabled so that no one can edit it manually;
	mNameEditbox->disable();

	// Turn on listbox Sorting;
	mListbox->setSortingEnabled(true);

	// Load the listbox on startup;
	_loadMapListBox();

	/************************************************/
	/*** Attach the windows to the parent window ***/
	/************************************************/
	mMainChildwin->addChildWindow( mListbox );
	mMainChildwin->addChildWindow( mFileNameLabel );	
	mMainChildwin->addChildWindow( mNameEditbox );
	mMainChildwin->addChildWindow( mReloadButton );
	mMainChildwin->addChildWindow( mOkButton );
	mMainChildwin->addChildWindow( mCancelButton );

	mParentwin->addChildWindow(mMainChildwin);
	sheet->addChildWindow(mParentwin);

	return true;
}


bool TerrainMenuSplatTabFileDialog::_loadMapListBox()
{

	AccessFileSystem fileSystem;
	string vFilePath = FILEPATH_OF_SPLAT_SAMPLEBRUSH;

	vector<string> vFileNames;
	CEGUI::ListboxTextItem* item = NULL;



	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(vFilePath) )
	{
		string title = "ERROR";
		string functionName = "bool TerrainMenuSplatTabFileDialog::_loadMapListBox()";
		string errorMsg = "Could not find directory.";

		if ( mGuiMessageBox )
		{
			mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
		}


		return false;
	}


	/*** We load the listbox with all the files in this directory ***/
	fileSystem.getRecursiveFiles(vFilePath, &vFileNames);


	// Create the text item and set its text;
	for (unsigned int i = 0; i < vFileNames.size(); i++)
	{
		item = new CEGUI::ListboxTextItem( vFileNames.at(i) );			// Create the textitem;
		item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );	// Set the brush;
		mListbox->addItem( item );						// Add the item;
	}


	return true;
}


// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool TerrainMenuSplatTabFileDialog::event_editBox_activated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mNameEditbox->isActive() )
	{
		// We set the GUI windows to busy;
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		// We set the GUI windows to not busy;
		guiManager->setEnabledCommandKeys(true);
	}

	return true;	
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool TerrainMenuSplatTabFileDialog::event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mNameEditbox->isActive() )
	{
		// We set the GUI windows to busy;
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		// We set the GUI windows to not busy;
		guiManager->setEnabledCommandKeys(true);
	}

	return true;	
}

bool TerrainMenuSplatTabFileDialog::event_listBoxTextToEditBox(const CEGUI::EventArgs& pEventArgs)
{
	CEGUI::ListboxItem* item;
	

	try
	{	
		item = mListbox->getFirstSelectedItem();
	}
	catch(...)
	{
		cout << "bool TerrainMenuSplatTabFileDialog::event_listBoxTextToEditBox(...): Error in: " << 
			"mListbox->getListboxItemFromIndex()." << endl;

		return false;
	}

	// If the item returns NULL than there was nothing selected and we return;
	if ( item == NULL )
	{
		return false;
	}


	// Get the selected listbox item string and put it into the editbox;
	mNameEditbox->setText( item->getText() );


	return true;
}

bool TerrainMenuSplatTabFileDialog::event_reloadListbox(const CEGUI::EventArgs& pEventArgs)
{
	// First Clear the listbox;
	mListbox->resetList();

	// Since the listbox is clear, we will clear the editbox as well;
	mNameEditbox->setText("");


	// Now load the listbox;
	return _loadMapListBox();
}





bool TerrainMenuSplatTabFileDialog::event_mOkButton_click(const CEGUI::EventArgs& pEventArgs)
{
	EditManager* const editManager = EditManager::getSingletonPtr();
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	GUI* const gui = guiManager->getGUI();
	TerrainMenuSplatTab* const terrainMenuSplatTab = gui->getTerrainMenu()->getTerrainMenuSplatTab();

	string fileName;
	CEGUI::ListboxItem* item;

	const unsigned long selectedTextureNumber = terrainMenuSplatTab->getSelectedTexture();
	vector<string>* vTextures = terrainMenuSplatTab->getSampleTextures();
	const unsigned long vTexturesSize = vTextures->size();




	/*** Try to get the text from the listbox or editbox ***/
	try
	{	
		item = mListbox->getFirstSelectedItem();
	}
	catch(...)
	{
		cout << "TerrainMenuSplatTabFileDialog::event_mOkButton_click(): Error in: " << 
			"mListbox->getListboxItemFromIndex()." << endl;

		return false;
	}

	// If the item returns NULL than there was nothing selected and we return;
	if ( item == NULL )
	{
		return false;
	}


	// Get the selected listbox item string;
	fileName = item->getText().c_str();


	if (vTexturesSize == 4)
	{
		terrainMenuSplatTab->setSampleTexture(selectedTextureNumber, fileName);

		terrainMenuSplatTab->_loadBrushesAndTextures();
		
		terrainMenuSplatTab->setSelectedTexture(selectedTextureNumber);	
	}	
	

/*********
		if ( check == false)
		{
			string title = "ERROR";
			string functionName = "TerrainMenuSplatTabFileDialog::event_mOkButton_click()";
			string errorMsg = "Could not import map file. Possible corrupted map file.";
	
			if ( mGuiMessageBox )
			{
				mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
			}

			// Destroy the scene, just to make sure in case something like models made it through;
			modelEditorMainTab->destroyScene();

			return false;
		}
***********/


	/*** Allow the other windows to be accessed again ***/
	mParentwin->setModalState(false);	// Allow the other windows to be accessed again;

	/*** We set the GUI windows to not busy ***/
	guiManager->setEnabledCommandKeys(true);

	/*** Hide the window ***/
	mParentwin->hide();


	return true;
}

bool TerrainMenuSplatTabFileDialog::event_mCancelButton_click(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	/*** Allow the other windows to be accessed again ***/
	mParentwin->setModalState(false);	// Allow the other windows to be accessed again;

	/*** We set the GUI windows to not busy ***/
	guiManager->setEnabledCommandKeys(true);

	/*** Hide the window ***/
	mParentwin->hide();


	return true;
}





HeapTerrainMenuSplatTabFileDialog::HeapTerrainMenuSplatTabFileDialog() : mTerrainMenuSplatTabFileDialog(NULL)
{

}

HeapTerrainMenuSplatTabFileDialog::~HeapTerrainMenuSplatTabFileDialog()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "HeapTerrainMenuSplatTabFileDialog::~HeapTerrainMenuSplatTabFileDialog()";
	string strLogMessage;

	if (mTerrainMenuSplatTabFileDialog)	
	{ 
		delete mTerrainMenuSplatTabFileDialog;
		mTerrainMenuSplatTabFileDialog = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted mTerrainMenuSplatTabFileDialog";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

CEGUI::Window* const HeapTerrainMenuSplatTabFileDialog::getWindow() const
{
	if ( mTerrainMenuSplatTabFileDialog )
	{
		return mTerrainMenuSplatTabFileDialog->getWindow();
	}
	
	return NULL;
}

void HeapTerrainMenuSplatTabFileDialog::toggle()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	const string strLogFunctionName = "void HeapTerrainMenuSplatTabFileDialog::toggle()";
	string strLogMessage;


	/*** If visible, then hide and delete the window and object. Else we create the object ***/
	if ( mTerrainMenuSplatTabFileDialog )	
	{
		/*** Prepare to quit, Delete the object and set the pointer to NULL ***/
		delete mTerrainMenuSplatTabFileDialog;
		mTerrainMenuSplatTabFileDialog = NULL;		// Let us know the object was deleted;

		strLogMessage = "Deleted TerrainMenuSplatTabFileDialog";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
	else
	{
		/*** Recreate the window ***/
		mTerrainMenuSplatTabFileDialog = new TerrainMenuSplatTabFileDialog;
		
		if ( mTerrainMenuSplatTabFileDialog == NULL )
		{ 
			strLogMessage = "TerrainMenuSplatTabFileDialog";
			ogre3dLogManager->logErrorMemoryMessage(strLogFunctionName, strLogMessage);

			// Don't allow us to continue further;
			return;
		}

		mTerrainMenuSplatTabFileDialog->addChildWindow();

		strLogMessage = "created TerrainMenuSplatTabFileDialog";
		ogre3dLogManager->logMessage(strLogFunctionName, strLogMessage);
	}
}

bool HeapTerrainMenuSplatTabFileDialog::isVisible() const
{
	if (mTerrainMenuSplatTabFileDialog)
	{
		return mTerrainMenuSplatTabFileDialog->getWindow()->isVisible();
	}

	return false;
}


