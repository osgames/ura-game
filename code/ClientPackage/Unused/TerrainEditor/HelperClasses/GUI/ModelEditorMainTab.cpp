/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ModelEditorMainTab.h"
#include "WorldEditorProperties.h"
#include "Ogre3dManager.h"
#include "CeguiManager.h"
#include "CameraManager.h"
#include "CustomSceneNodeManager.h"
#include "CustomSceneNodeManipulator.h"
#include "RayCastManager.h"
#include "GUIManager.h"
#include "EditManager.h"


using namespace std;
using namespace CEGUI;
using namespace Ogre;



ModelEditorMainTab::ModelEditorMainTab()
{
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	mGuiMessageBox = NULL;
	mModelMoveDegree = 1.0f;


	/*** Alocate heap memory for the GuiMessageBox ***/
	mGuiMessageBox = new GUIMessageBox;

	if ( !mGuiMessageBox )
	{
		string tmp = "ERROR: ModelEditorMainTab::ModelEditorMainTab(EditManager *editMgr): mGuiMessageBox could not allocate heap memory";
		Ogre::LogManager::getSingleton().logMessage( tmp );
	}


	/***
		Push back the starting 2 vectors of mouse rate: Add and edit.
		Edit is slower to due dragging needing to me slower.
 	***/
	mVecMouseRate.push_back(150);
	mVecMouseRate.push_back(80);


	/*** get the Camera move Degree rate ***/
	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();


	/*** Recursively add all the locations in the path we choose. This way we can load any model starting from the base directory ***/
	AccessFileSystem fileSystem;
	string startDirectoryPath = FILEPATH_OF_MODELS;
	vector<string> vDirectoryPaths;

	fileSystem.getRecursiveDirectoriesWithFullPath(startDirectoryPath, &vDirectoryPaths);

	for (unsigned int i = 0; i < vDirectoryPaths.size(); i++)
	{
		if ( !fileSystem.getDirectoryExist( vDirectoryPaths.at(i) ) )	// Check if the directory exists;
		{
			cout << "ModelEditorMainTab::ModelEditorMainTab(EditManager *editMgr): ERROR: Could not access directory." << endl;
		}
		else	// The directory exists so lets try to add it;
		{
			try
			{
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation ( vDirectoryPaths.at(i), "FileSystem", "General");
			}
			catch(...)
			{
				cout << "ModelEditorMainTab::ModelEditorMainTab(EditManager *editMgr):"
					<< "ERROR: adding directory to resource group." << endl;
			}
		}
	}
}

ModelEditorMainTab::~ModelEditorMainTab()
{
	if (mGuiMessageBox)
	{
		delete mGuiMessageBox;
		mGuiMessageBox = 0;
	}


	/*** Destroy the windows ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mParentwin);
}

CEGUI::Window* const ModelEditorMainTab::getWindow() const
{
	return mParentwin;
}

bool ModelEditorMainTab::addChildWindow()
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	CEGUI::PropertyHelper phelper;

	// Create the GuiMessageBox and attach it to the sheet;
	mGuiMessageBox->addWindow();



	/********************************/
	/*** Create the parent window ***/
	/********************************/
	mParentwin = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelEditorMainTab_Main_mParentwin");
	mParentwin->setText("Main");
	mParentwin->setSize(UVector2(UDim(1.0f, 0.0f), UDim(1.0f, 0.0f)));
	mParentwin->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(0.0f, 0.0f)));

	mParentwin->subscribeEvent( CEGUI::Window::EventShown, CEGUI::Event::Subscriber(
		&ModelEditorMainTab::event_windowVisible, this ) );

	/****************************************************************************/
	/*** Create the mMeshListbox to select meshes from and its refresh button ***/
	/****************************************************************************/

	// the listbox;
	mMeshListbox = (CEGUI::Listbox *) windowManager.createWindow("TaharezLook/Listbox_woodframe","ModelEditorMainTab_mMeshListbox");

	// the refresh button;
	mAddMeshRefreshListboxButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button",
		"ModelEditorMainTab_mAddMeshRefreshListboxButton");
	mAddMeshRefreshListboxButton->setText("Reload");
	mAddMeshRefreshListboxButton->subscribeEvent( CEGUI::Window::EventShown, CEGUI::Event::Subscriber(
		&ModelEditorMainTab::event_refreshMesh, this ) );

	// The frame;
	mFrameBelowRefreshListboxButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelEditorMainTab_mFrameBelowRefreshListboxButton");
	mFrameBelowRefreshListboxButton->setProperty("FrameEnabled", "False");
	mFrameBelowRefreshListboxButton->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/****************************************************************************************/
	/*** create the mMeshModeText text, mesh Add, mesh edit and mesh delete radio buttons ***/
	/****************************************************************************************/

	/*** Create the static text ***/
	mMeshModeText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mMeshModeText");
	mMeshModeText->setProperty("FrameEnabled", "False");
	mMeshModeText->setProperty("BackgroundEnabled", "false");

	// Disable the mMeshModeText;
	mMeshModeText->disable();
	// Center the text;
	mMeshModeText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mMeshModeText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mMeshModeText->setText( "Model Mode:" );


	mRadioAddMesh = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioAddMesh"));
	mRadioAddMesh->setGroupID(1);
	mRadioAddMesh->setText("Add");

	mRadioEditMesh = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioEditMesh"));
	mRadioEditMesh->setGroupID(1);
	mRadioEditMesh->setText("Edit");


	/*****************************************************************/
	/***  Create the wood frame beneath the mRadioDeleteMesh radio ***/
	/*****************************************************************/
	mFrameBelowRadioDeleteMesh = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelEditorMainTab_mFrameBelowRadioDeleteMesh");
	mFrameBelowRadioDeleteMesh->setProperty("FrameEnabled", "False");
	mFrameBelowRadioDeleteMesh->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/**************************************************************************************/
	/*** Create the mEditModeText text and radios rotate, roll, pitch, x, y, z and drag ***/
	/**************************************************************************************/

	/*** Create the static text ***/
	mEditModeText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mEditModeText");
	mEditModeText->setProperty("FrameEnabled", "False");
	mEditModeText->setProperty("BackgroundEnabled", "false");

	// Disable the mEditModeText;
	mEditModeText->disable();
	// Center the text;
	mEditModeText->setProperty("HorzFormatting", "HorzCentred");
	// Put the text at the top;
	mEditModeText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mEditModeText->setText( "Edit Mode:" );


	/*** select and deselect radios ***/
	mRadioEditModeSelect = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelEditorMainTab_mRadioEditModeSelect"));
	mRadioEditModeSelect->setGroupID(2);
	mRadioEditModeSelect->setText("Select");

	mRadioEditModeDeselect = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelEditorMainTab_mRadioEditModeDeselect"));
	mRadioEditModeDeselect->setGroupID(2);
	mRadioEditModeDeselect->setText("Deselect");


	/*** Attach and dettach node ***/
	mRadioEditModeAttachNode = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelEditorMainTab_mRadioEditModeAttachNode"));
	mRadioEditModeAttachNode->setGroupID(2);
	mRadioEditModeAttachNode->setText("Attach");

	mRadioEditModeDettachNode = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelEditorMainTab_mRadioEditModeDettachNode"));
	mRadioEditModeDettachNode->setGroupID(2);
	mRadioEditModeDettachNode->setText("Dettach");


	/*** Delete Mesh Radio Button ***/
	mRadioDeleteMesh = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioDeleteMesh"));
	mRadioDeleteMesh->setGroupID(2);
	mRadioDeleteMesh->setText("Delete");

	/*** Copy Radio Button ***/
	mRadioCopy = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioCopy"));
	mRadioCopy->setGroupID(2);
	mRadioCopy->setText("Copy");

	/*** Radios rotate, roll and pitch ***/
	mRadioRotate = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioRotate"));
	mRadioRotate->setGroupID(2);
	mRadioRotate->setText("Rotate");

	mRadioRoll = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTabb_mRadioRoll"));
	mRadioRoll->setGroupID(2);
	mRadioRoll->setText("Roll");

	mRadioPitch = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioPitch"));
	mRadioPitch->setGroupID(2);
	mRadioPitch->setText("Pitch");


	/*** Radios x, y and z ***/
	mRadioX = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioX"));
	mRadioX->setGroupID(2);
	mRadioX->setText("X");

	mRadioY = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioY"));
	mRadioY->setGroupID(2);
	mRadioY->setText("Y");

	mRadioZ = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioZ"));
	mRadioZ->setGroupID(2);
	mRadioZ->setText("Z");

	/*** radio drag ***/
	mRadioDrag = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton", "ModelEditorMainTab_mRadioDrag"));
	mRadioDrag->setGroupID(2);
	mRadioDrag->setText("Drag");


	/*************************************************************************************/
	/*** create the text scenenode name and frame below the mFrameBelowTextSceneNodeName radio ***/
	/**************************************************************************************/

	/*** Create the static text ***/
	mTextSceneNodeName = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mTextSceneNodeName");
	mTextSceneNodeName->setProperty("FrameEnabled", "False");
	mTextSceneNodeName->setProperty("BackgroundEnabled", "false");

	// Disable the editbox;
	mTextSceneNodeName->disable();

	// Set the text;
	mTextSceneNodeName->setText( "Model Name:" );


	/*** Create the frame ***/
	mFrameBelowTextSceneNodeName = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelEditorMainTab_mFrameBelowRadioDrag");
	mFrameBelowTextSceneNodeName->setProperty("FrameEnabled", "False");
	mFrameBelowTextSceneNodeName->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************************************************/
	/*** create the Scale text, the editboxes for x, y and z and the apply button ***/
	/********************************************************************************/

	/*** Create the static text ***/
	mScaleText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mScaleText");
	mScaleText->setProperty("FrameEnabled", "False");
	mScaleText->setProperty("BackgroundEnabled", "false");

	// Disable the mScaleText;
	mScaleText->disable();
	// Center the text;
	mScaleText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mScaleText->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mScaleText->setText( "Scale Percent:  x, y, z" );


	mScaleEditboxX = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelEditorMainTab_mScaleEditboxX");
	mScaleEditboxY = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelEditorMainTab_mScaleEditboxY");
	mScaleEditboxZ = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelEditorMainTab_mScaleEditboxZ");
	mScaleApplyButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/Button", "ModelEditorMainTab_mScaleApplyButton");
	mScaleApplyButton->setText("Ok");

	/************************************************************/
	/***  Create the wood frame beneath the mScaleApplyButton ***/
	/************************************************************/
	mFrameBelowApplyButton = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelEditorMainTab_mFrameBelowApplyButton");
	mFrameBelowApplyButton->setProperty("FrameEnabled", "False");
	mFrameBelowApplyButton->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/*************************************************/
	/*** create the Move degree text and scrollbar ***/
	/*************************************************/

	/*** Create the static text ***/
	mModelMoveDegreeText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mModelMoveDegreeText");
	mModelMoveDegreeText->setProperty("FrameEnabled", "False");
	mModelMoveDegreeText->setProperty("BackgroundEnabled", "false");

	// Disable the mMouseRateText;
	mModelMoveDegreeText->disable();
	// Center the text;
	mModelMoveDegreeText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mModelMoveDegreeText->setProperty("VertFormatting", "TopAligned");
	mModelMoveDegreeText->setText( "Model Rate: " + phelper.floatToString(mModelMoveDegree) );


	mModelMoveDegreeScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"ModelEditorMainTab_Main_mMoveDegreeScrollbar");
	mModelMoveDegreeScrollbar->setDocumentSize(201);
	mModelMoveDegreeScrollbar->setScrollPosition(mModelMoveDegree);
	mModelMoveDegreeScrollbar->setPageSize(1);
	mModelMoveDegreeScrollbar->setStepSize(0.001f);

	// Set the scrollbar event to change the text;
	mModelMoveDegreeScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&ModelEditorMainTab::event_mModelMoveDegreeScrollbar_change, this));

	/***************************************************************/
	/*** create the mouse rate text and the mouse rate scrollbar ***/
	/***************************************************************/

	/*** Create the static text ***/
	mMouseRateText = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_ mMouseRateText");
	mMouseRateText->setProperty("FrameEnabled", "False");
	mMouseRateText->setProperty("BackgroundEnabled", "false");

	// Disable the mMouseRateText;
	mMouseRateText->disable();
	// Center the text;
	mMouseRateText->setProperty("HorzFormatting", "LeftAligned");
	// Put the text at the top;
	mMouseRateText->setProperty("VertFormatting", "TopAligned");
	mMouseRateText->setText( "Mouse Rate: " + phelper.uintToString(mVecMouseRate.at(0)) );


	mMouseRateScrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"ModelEditorMainTab_Main_mMouseRateScrollbar");
	mMouseRateScrollbar->setDocumentSize(201);
	mMouseRateScrollbar->setScrollPosition( mVecMouseRate.at(0) );
	mMouseRateScrollbar->setPageSize(1);
	mMouseRateScrollbar->setStepSize(1);

	// Set the scrollbar event to change the text;
	mMouseRateScrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&ModelEditorMainTab::event_mMouseRateScrollbar_change, this));

	/******************************************************************************/
	/*** Create the mAddMeshLabel, Editbox, Radios "name" "auto name" and frame ***/
	/******************************************************************************/

	// mAddMeshLabel Label;
	mAddMeshLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mAddMeshLabel");
	mAddMeshLabel->setProperty("FrameEnabled", "False");
	mAddMeshLabel->setProperty("BackgroundEnabled", "false");
	mAddMeshLabel->setText("Model Name:");	// Set the text;
	mAddMeshLabel->disable();

	// Create the edit box;
	mAddMeshEditboxName = (CEGUI::Editbox *) windowManager.createWindow("TaharezLook/Editbox", "ModelEditorMainTab_mAddMeshEditboxName");

	// Radio name;
	mRadioAddMeshName = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelEditorMainTab_mRadioAddMeshName"));
	mRadioAddMeshName->setGroupID(3);
	mRadioAddMeshName->setText("Name");

	// Radio auto name;
	mRadioAddMeshAutoName = static_cast<RadioButton*>(windowManager.createWindow("TaharezLook/RadioButton",
		"ModelEditorMainTab_mRadioAddMeshAutoName"));
	mRadioAddMeshAutoName->setGroupID(3);
	mRadioAddMeshAutoName->setText("Auto Name");

	// Frame;
	mFrameBelowMeshAutoName = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticImage",
		"ModelEditorMainTab_mFrameBelowMeshAutoName");
	mFrameBelowMeshAutoName->setProperty("FrameEnabled", "False");
	mFrameBelowMeshAutoName->setProperty( "Image", "set:bas_skin_wood image:bas_skin_wood_Titlebar_BigWoodFrameTop_Middle" );


	/********************************************************************************/
	/*** Create the mCamDegree_Scrollbar scrollbar and mCamDegreeLabel text label ***/
	/********************************************************************************/
	mCamDegree_Scrollbar = (CEGUI::Scrollbar*) windowManager.createWindow("TaharezLook/HorizontalScrollbar",
		"ModelEditorMainTab_mCamDegree_Scrollbar");
	mCamDegree_Scrollbar->setDocumentSize(201);
	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);
	mCamDegree_Scrollbar->setPageSize(1);
	mCamDegree_Scrollbar->setStepSize(0.001f);

	// Set the scrollbar event to change the text;
	mCamDegree_Scrollbar->subscribeEvent(CEGUI::Scrollbar::EventScrollPositionChanged,
		Event::Subscriber(&ModelEditorMainTab::event_mCamDegree_Scrollbar_change, this));

	
	// Create the static text
	mCamDegreeLabel = (CEGUI::Window*) windowManager.createWindow("TaharezLook/StaticText",
		"ModelEditorMainTab_mCamDegreeLabel");
	mCamDegreeLabel->setProperty("FrameEnabled", "False");
	mCamDegreeLabel->setProperty("BackgroundEnabled", "false");

	// Disable the window;
	mCamDegreeLabel->disable();
	// Center the text;
	mCamDegreeLabel->setProperty("HorzFormatting", "LeftAligned"); 
	// Put the text at the top;
	mCamDegreeLabel->setProperty("VertFormatting", "TopAligned");
	// Set the text;
	mCamDegreeLabel->setText( "Camera Rate: " + phelper.floatToString(mCameraDegree) );



	/*************************************/
	/*** Setup the sizes and positions ***/
	/*************************************/

	// Frame stuff;
	float frame_x_pos = 0.00f;
	float frame_x_size = 1.0f;
	float frame_y_size = 0.018f;
	float frame_bottom_padding = 0.019f;
	float frame_top_padding = 0.05f;

	// text stuff
	float text_top_padding = 0.035f;
	float text_x_pos = 0.07f;
	float text_y_size = 0.06f;

	// Scrollbar stuff
	float scrollbar_x_pos = 0.07f;
	float scrollbar_x_size = 0.86f;
	float scrollbar_y_size = 0.027f;

	// radio Stuff;
	float radio_y_padding = 0.04f;
	float radio_y_size = 0.05f;
	float radio_x_pos = 0.07f;

	// scaling stuf;
	float mScaleEditbox_x_size = 0.23f;
	float mScaleEditbox_y_size = 0.04f;
	float mScaleEditbox_x_padding = 0.05f;

	float mScaleEditboxX_x_pos = 0.1f;
	float mScaleEditboxY_x_pos = mScaleEditboxX_x_pos + mScaleEditbox_x_size + mScaleEditbox_x_padding;
	float mScaleEditboxZ_x_pos = mScaleEditboxY_x_pos + mScaleEditbox_x_size + mScaleEditbox_x_padding;

	float mScaleApplyButton_x_size = 0.805f;
	float mScaleApplyButton_y_size = 0.04f;
	float mScaleApplyButton_x_pos = mScaleEditboxX_x_pos;

	float mMeshModeText_y_pos = 0.02f;
	float mRadioAddMesh_y_pos = mMeshModeText_y_pos + 0.03f;

	float mFrameBelowRadioDeleteMesh_y_pos = mRadioAddMesh_y_pos + frame_top_padding;

	// Mesh listbox;
	float mMeshListbox_x_pos = 0.1f;
	float mMeshListbox_y_pos = mFrameBelowRadioDeleteMesh_y_pos + 0.038f;
	float mMeshListbox_x_size = 0.8f;
	float mMeshListbox_y_size = 0.46f;

	// Mesh listbox refresh button;
	float mAddMeshRefreshListboxButton_y_pos = mMeshListbox_y_pos + mMeshListbox_y_size + 0.015f;
	float mFrameBelowRefreshListboxButton_y_pos = mAddMeshRefreshListboxButton_y_pos + 0.04f +  0.015f;

	float mEditModeText_y_pos = mFrameBelowRadioDeleteMesh_y_pos + 0.025f;
	float mRadioEditModeSelect_y_pos = mEditModeText_y_pos + 0.025f;
	float mRadioEditModeAttachNode_y_pos = mRadioEditModeSelect_y_pos + radio_y_padding;
	float mRadioDeleteMesh_y_pos = mRadioEditModeAttachNode_y_pos + radio_y_padding;
	float mRadioRotate_y_pos = mRadioDeleteMesh_y_pos + radio_y_padding;
	float mRadioX_y_pos = mRadioRotate_y_pos + radio_y_padding;
	float mRadioDrag_y_pos = mRadioX_y_pos + radio_y_padding;

	float mTextSceneNodeName_y_pos = 0.545f;
	float mFrameBelowTextSceneNodeName_y_pos = mTextSceneNodeName_y_pos + 0.05f;

	float mScaleText_y_pos = mFrameBelowTextSceneNodeName_y_pos + frame_bottom_padding;
	float mScaleEditboxX_y_pos = mScaleText_y_pos + text_top_padding;
	float mScaleApplyButton_y_pos = mScaleEditboxX_y_pos + mScaleEditbox_y_size + 0.01f;

	float mFrameBelowApplyButton_y_pos = mScaleApplyButton_y_pos + frame_top_padding;

	// Move degree;
	float mMoveDegreeText_y_pos = mFrameBelowApplyButton_y_pos + 0.025f;
	float mMoveDegreeScrollbar_y_pos = mMoveDegreeText_y_pos + text_top_padding;

	// Mouse Rate;
	float mMouseRateText_y_pos = 0.906f;
	float mMouseRateScrollbar_y_pos = mMouseRateText_y_pos + text_top_padding;

	// Add Mode - naming;
	float mAddMeshLabel_y_pos = mFrameBelowRefreshListboxButton_y_pos + 0.020f;
	float mAddMeshEditboxName_y_pos = mAddMeshLabel_y_pos + 0.04f;
	float mRadioAddMeshName_y_pos = mAddMeshEditboxName_y_pos + radio_y_padding;
	float mFrameBelowRadioAddMeshName_y_pos = mRadioAddMeshName_y_pos + 0.048f;

	// Add Mode - Cam Degree;
	float mCamDegreeLabel_y_pos = mFrameBelowRadioAddMeshName_y_pos + 0.025f;
	float mCamDegree_Scrollbar_y_pos = mCamDegreeLabel_y_pos + text_top_padding;


	float tmpXPosition = 0.0f;




	/*** All mode ***/
	mMeshModeText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mMeshModeText_y_pos, 0.0f)));
	mMeshModeText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));

	mRadioAddMesh->setPosition(UVector2(UDim(0.25f, 0.0f), UDim(mRadioAddMesh_y_pos, 0.0f)));
	mRadioAddMesh->setSize(UVector2(UDim(0.25f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioAddMesh->getXPosition().d_scale + mRadioAddMesh->getWidth().d_scale + 0.02f;
	mRadioEditMesh->setPosition(UVector2(UDim(tmpXPosition, 0.0f),UDim(mRadioAddMesh_y_pos, 0.0f)));
	mRadioEditMesh->setSize(UVector2(UDim(0.23f, 0.0f), UDim(radio_y_size, 0.0f)));

	// Frame;
	mFrameBelowRadioDeleteMesh->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowRadioDeleteMesh_y_pos, 0.0f)));
	mFrameBelowRadioDeleteMesh->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Mouse rate
	mMouseRateText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mMouseRateText_y_pos, 0.0f)));
	mMouseRateText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mMouseRateScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mMouseRateScrollbar_y_pos, 0.0f)));
	mMouseRateScrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));


	/*** Add mode ***/
	mMeshListbox->setPosition(UVector2(UDim(mMeshListbox_x_pos, 0.0f), UDim(mMeshListbox_y_pos, 0.0f)));
	mMeshListbox->setSize(UVector2(UDim(mMeshListbox_x_size, 0.0f), UDim(mMeshListbox_y_size, 0.0f)));
	mAddMeshRefreshListboxButton->setPosition(UVector2(UDim(mMeshListbox_x_pos, 0.0f), UDim(mAddMeshRefreshListboxButton_y_pos, 0.0f)));
	mAddMeshRefreshListboxButton->setSize(UVector2(UDim(mMeshListbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mFrameBelowRefreshListboxButton->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowRefreshListboxButton_y_pos, 0.0f)));
	mFrameBelowRefreshListboxButton->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Add Mode - naming;
	mAddMeshLabel->setPosition(UVector2(UDim(mMeshListbox_x_pos, 0.0f), UDim(mAddMeshLabel_y_pos, 0.0f)));
	mAddMeshLabel->setSize(UVector2(UDim(mMeshListbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mAddMeshEditboxName->setPosition(UVector2(UDim(mMeshListbox_x_pos, 0.0f), UDim(mAddMeshEditboxName_y_pos, 0.0f)));
	mAddMeshEditboxName->setSize(UVector2(UDim(mMeshListbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mRadioAddMeshName->setPosition(UVector2(UDim(mMeshListbox_x_pos, 0.0f), UDim(mRadioAddMeshName_y_pos, 0.0f)));
	mRadioAddMeshName->setSize(UVector2(UDim(0.32f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioAddMeshName->getXPosition().d_scale + mRadioAddMeshName->getWidth().d_scale + 0.02f;
	mRadioAddMeshAutoName->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioAddMeshName_y_pos, 0.0f)));
	mRadioAddMeshAutoName->setSize(UVector2(UDim(0.7f, 0.0f), UDim(radio_y_size, 0.0f)));

	mFrameBelowMeshAutoName->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowRadioAddMeshName_y_pos, 0.0f)));
	mFrameBelowMeshAutoName->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Camera Degree;
	mCamDegreeLabel->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mCamDegreeLabel_y_pos, 0.0f)));
	mCamDegreeLabel->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f))); 

	mCamDegree_Scrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mCamDegree_Scrollbar_y_pos, 0.0f)));
	mCamDegree_Scrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f))); 


	/*** Edit mode ***/
	mEditModeText->setPosition(UVector2(UDim(0.0f, 0.0f), UDim(mEditModeText_y_pos, 0.0f)));
	mEditModeText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mRadioEditModeSelect->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioEditModeSelect_y_pos, 0.0f)));
	mRadioEditModeSelect->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioEditModeSelect->getXPosition().d_scale + mRadioEditModeSelect->getWidth().d_scale + 0.02f;
	mRadioEditModeDeselect->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioEditModeSelect_y_pos, 0.0f)));
	mRadioEditModeDeselect->setSize(UVector2(UDim(0.4f, 0.0f), UDim(radio_y_size, 0.0f)));
	mRadioEditModeAttachNode->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioEditModeAttachNode_y_pos, 0.0f)));
	mRadioEditModeAttachNode->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioEditModeAttachNode->getXPosition().d_scale + mRadioEditModeAttachNode->getWidth().d_scale + 0.02f;
	mRadioEditModeDettachNode->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioEditModeAttachNode_y_pos, 0.0f)));
	mRadioEditModeDettachNode->setSize(UVector2(UDim(0.4f, 0.0f), UDim(radio_y_size, 0.0f)));

	mRadioDeleteMesh->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioDeleteMesh_y_pos, 0.0f)));
	mRadioDeleteMesh->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioDeleteMesh->getXPosition().d_scale + mRadioDeleteMesh->getWidth().d_scale + 0.02f;
	mRadioCopy->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioDeleteMesh_y_pos, 0.0f)));
	mRadioCopy->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));

	mRadioRotate->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioRotate_y_pos, 0.0f)));
	mRadioRotate->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioRotate->getXPosition().d_scale + mRadioRotate->getWidth().d_scale + 0.02f;
	mRadioRoll->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioRotate_y_pos, 0.0f)));
	mRadioRoll->setSize(UVector2(UDim(0.23, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioRoll->getXPosition().d_scale + mRadioRoll->getWidth().d_scale + 0.02f;
	mRadioPitch->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioRotate_y_pos, 0.0f)));
	mRadioPitch->setSize(UVector2(UDim(1.0f, 0.0f), UDim(radio_y_size, 0.0f)));
	mRadioX->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioX_y_pos, 0.0f)));
	mRadioX->setSize(UVector2(UDim(0.31f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioX->getXPosition().d_scale + mRadioX->getWidth().d_scale + 0.02f;
	mRadioY->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioX_y_pos, 0.0f)));
	mRadioY->setSize(UVector2(UDim(0.23f, 0.0f), UDim(radio_y_size, 0.0f)));
	tmpXPosition = mRadioY->getXPosition().d_scale + mRadioY->getWidth().d_scale + 0.02f;
	mRadioZ->setPosition(UVector2(UDim(tmpXPosition, 0.0f), UDim(mRadioX_y_pos, 0.0f)));
	mRadioZ->setSize(UVector2(UDim(0.25f, 0.0f), UDim(radio_y_size, 0.0f)));
	mRadioDrag->setPosition(UVector2(UDim(radio_x_pos, 0.0f), UDim(mRadioDrag_y_pos, 0.0f)));
	mRadioDrag->setSize(UVector2(UDim(0.25f, 0.0f), UDim(radio_y_size, 0.0f)));

	// Editbox Scenenode name;
	mTextSceneNodeName->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mTextSceneNodeName_y_pos, 0.0f)));
	mTextSceneNodeName->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));

	//Frame;
	mFrameBelowTextSceneNodeName->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowTextSceneNodeName_y_pos, 0.0f)));
	mFrameBelowTextSceneNodeName->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	mScaleText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mScaleText_y_pos, 0.0f)));
	mScaleText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mScaleEditboxX->setPosition(UVector2(UDim(mScaleEditboxX_x_pos, 0.0f), UDim(mScaleEditboxX_y_pos, 0.0f)));
	mScaleEditboxX->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mScaleEditboxY->setPosition(UVector2(UDim(mScaleEditboxY_x_pos, 0.0f), UDim(mScaleEditboxX_y_pos, 0.0f)));
	mScaleEditboxY->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mScaleEditboxZ->setPosition(UVector2(UDim(mScaleEditboxZ_x_pos, 0.0f), UDim(mScaleEditboxX_y_pos, 0.0f)));
	mScaleEditboxZ->setSize(UVector2(UDim(mScaleEditbox_x_size, 0.0f), UDim(mScaleEditbox_y_size, 0.0f)));
	mScaleApplyButton->setPosition(UVector2(UDim(mScaleApplyButton_x_pos, 0.0f), UDim(mScaleApplyButton_y_pos, 0.0f)));
	mScaleApplyButton->setSize(UVector2(UDim(mScaleApplyButton_x_size, 0.0f), UDim(mScaleApplyButton_y_size, 0.0f)));

	// Frame;
	mFrameBelowApplyButton->setPosition(UVector2(UDim(frame_x_pos, 0.0f), UDim(mFrameBelowApplyButton_y_pos, 0.0f)));
	mFrameBelowApplyButton->setSize(UVector2(UDim(frame_x_size, 0.0f), UDim(frame_y_size, 0.0f)));

	// Move Degree;
	mModelMoveDegreeText->setPosition(UVector2(UDim(text_x_pos, 0.0f), UDim(mMoveDegreeText_y_pos, 0.0f)));
	mModelMoveDegreeText->setSize(UVector2(UDim(1.0f, 0.0f), UDim(text_y_size, 0.0f)));
	mModelMoveDegreeScrollbar->setPosition(UVector2(UDim(scrollbar_x_pos, 0.0f), UDim(mMoveDegreeScrollbar_y_pos, 0.0f)));
	mModelMoveDegreeScrollbar->setSize(UVector2(UDim(scrollbar_x_size, 0.0f), UDim(scrollbar_y_size, 0.0f)));


	/***************/
	/*** Listbox ***/
	/***************/

	AccessFileSystem fileSystem;
	string modelFilePath = FILEPATH_OF_MODELS;
	string modelFileFilter = ".mesh";

	vector<string> vMeshFileNames;
	CEGUI::ListboxTextItem* item;


	/*** We load the listbox with all the files in this directory that have the extension ".mesh" ***/

	fileSystem.getRecursiveFilesWithExtension(modelFilePath, &vMeshFileNames, &modelFileFilter);


	// If the directory doesn't exist than give an error message and return;
	if ( !fileSystem.getDirectoryExist(modelFilePath) )
	{
		cout << "bool ModelEditorMainTab::addChildWindow(): ERROR: Could not find model directory." << endl;
		return false;
	}

	// Turn on listbox Sorting;
	mMeshListbox->setSortingEnabled(true);

	// Create the text item and set its text;
	for (unsigned int i = 0; i < vMeshFileNames.size(); i++)
	{
		item = new CEGUI::ListboxTextItem( vMeshFileNames[i] );			// Create the textitem;
		item->setSelectionBrushImage( "TaharezLook", "TextSelectionBrush" );	// Set the brush;
		mMeshListbox->addItem( item );						// Add the item;
	}

	/*************************/
	/*** Handle the events ***/
	/*************************/

	/*** Model Mode ***/
	mRadioAddMesh->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelEditorMainTab::event_radio_modelMode_change, this));

	mRadioEditMesh->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelEditorMainTab::event_radio_modelMode_change, this));

	mRadioDeleteMesh->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelEditorMainTab::event_radio_modelMode_change, this));

	mRadioCopy->subscribeEvent(CEGUI::RadioButton::EventSelectStateChanged,
	Event::Subscriber(&ModelEditorMainTab::event_radio_modelMode_change, this));


	/*** Scale apply ***/
	mScaleApplyButton->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_scaleApply, this ) );


	/*** Prevent keyboard acton while in the editboxes ***/
	mAddMeshEditboxName->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_activated, this ) );
	mAddMeshEditboxName->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_deactivated, this ) );

	mScaleEditboxX->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_activated, this ) );
	mScaleEditboxX->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_deactivated, this ) );

	mScaleEditboxY->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_activated, this ) );
	mScaleEditboxY->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_deactivated, this ) );

	mScaleEditboxZ->subscribeEvent( CEGUI::Window::EventActivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_activated, this ) );
	mScaleEditboxZ->subscribeEvent( CEGUI::Window::EventDeactivated, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_editBox_deactivated, this ) );

		// When we click on the main sheet we want to deactivate the editbox;
	sheet->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber(
			&ModelEditorMainTab::event_sheet_mouseDown, this ) );

	/***********************/
	/*** Handle settings ***/
	/***********************/

	// Set the scale editboxes to have the text of 100;
	mScaleEditboxX->setText("100");
	mScaleEditboxY->setText("100");
	mScaleEditboxZ->setText("100");

	// Select the first Mesh mode radio which is Add;
	mRadioAddMesh->setSelected(true);

	// Set the radio position to select;
	mRadioEditModeSelect->setSelected(true);

	// Select the Add mode "name" radio;
	mRadioAddMeshName->setSelected(true);


	/* NOTE: We disable these two radios for now, because the code needs work */
	mRadioEditModeAttachNode->disable();
	mRadioEditModeDettachNode->disable();


	/***********************/
	/*** Attach the tabs ***/
	/***********************/


	/*** All mode ***/
	mParentwin->addChildWindow( mMeshModeText );
	mParentwin->addChildWindow( mRadioAddMesh );
	mParentwin->addChildWindow( mRadioEditMesh );
	mParentwin->addChildWindow( mRadioDeleteMesh );
	mParentwin->addChildWindow( mRadioCopy );

	mParentwin->addChildWindow( mMouseRateText );
	mParentwin->addChildWindow( mMouseRateScrollbar );


	/*** Add mode ***/
	mParentwin->addChildWindow( mMeshListbox );
	mParentwin->addChildWindow( mAddMeshRefreshListboxButton );
	mParentwin->addChildWindow( mFrameBelowRefreshListboxButton );

	// Add Mode - naming;
	mParentwin->addChildWindow( mAddMeshLabel );
	mParentwin->addChildWindow( mAddMeshEditboxName );
	mParentwin->addChildWindow( mRadioAddMeshName );
	mParentwin->addChildWindow( mRadioAddMeshAutoName );
	mParentwin->addChildWindow( mFrameBelowMeshAutoName );

	// Camera Degree move rate;
	mParentwin->addChildWindow( mCamDegreeLabel );
	mParentwin->addChildWindow( mCamDegree_Scrollbar );


	/*** Edit mode ***/
	mParentwin->addChildWindow( mFrameBelowRadioDeleteMesh );

	mParentwin->addChildWindow( mEditModeText );
	mParentwin->addChildWindow( mRadioEditModeSelect );
	mParentwin->addChildWindow( mRadioEditModeDeselect );
	mParentwin->addChildWindow( mRadioEditModeAttachNode );
	mParentwin->addChildWindow( mRadioEditModeDettachNode );
	mParentwin->addChildWindow( mRadioRotate );
	mParentwin->addChildWindow( mRadioRoll );
	mParentwin->addChildWindow( mRadioPitch );
	mParentwin->addChildWindow( mRadioX );
	mParentwin->addChildWindow( mRadioY );
	mParentwin->addChildWindow( mRadioZ );
	mParentwin->addChildWindow( mRadioDrag );

	mParentwin->addChildWindow( mTextSceneNodeName );
	mParentwin->addChildWindow( mFrameBelowTextSceneNodeName );

	mParentwin->addChildWindow( mScaleText );
	mParentwin->addChildWindow( mScaleEditboxX );
	mParentwin->addChildWindow( mScaleEditboxY );
	mParentwin->addChildWindow( mScaleEditboxZ );
	mParentwin->addChildWindow( mScaleApplyButton );

	mParentwin->addChildWindow( mFrameBelowApplyButton );

	mParentwin->addChildWindow( mModelMoveDegreeText );
	mParentwin->addChildWindow( mModelMoveDegreeScrollbar );


	return true;
}



void ModelEditorMainTab::mouseLeftButtonDown()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	EditManager* const editManager = EditManager::getSingletonPtr();
	RayCastManager* const rayCastManager = editManager->getRayCastManager();
	string sceneNodeName;

	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::SceneNode* sceneNode = NULL;

	CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();


	// Make sure that the mode is set to entity. This should already be the case but there's no harm in verifying.
	customSceneNodeManipulator->setMode(CustomSceneNodeManipulator::ENTITY);

	// Get the picked SceneNode;
	if (rayCastManager)
	{
		// If there's a sceneNode at the cursor than get it and set it to sceneNode, else set 
		//  sceneNode to NULL;
		if (rayCastManager->getSceneNodeNameAtCursor(sceneNodeName) )
		{
			// If the sceneNode name doesn't exist than a exception is thrown.
			// We catch the exception and set the sceneNode to NULL;
			try
			{	
				sceneNode = ogre3dManager->getSceneManager()->getSceneNode(sceneNodeName);
			}
			catch(...)
			{
				sceneNode = NULL;
			}
		}
		else	
		{
			sceneNode = NULL;
		}
	}



	// Adding meshes means the sceneNode may be NULL sometimes;
	if ( mRadioAddMesh->isSelected() )
	{
		Ogre::SceneNode* tmpNode = 0;
		CEGUI::ListboxItem* item = 0;
		string meshFileName;
		string sceneNodeName;
		string entityName;



		try
		{
			item = mMeshListbox->getFirstSelectedItem();
		}
		catch(...)
		{
			cout << "ModelEditorMainTab::mouseLeftButtonDown(): Error in: " <<
				"mMeshListbox->getListboxItemFromIndex();" << endl;

			return;
		}


		// If the item returns NULL than there was nothing selected. Give a message box and return;
		if ( !item )
		{
			string title = "ERROR";
			string functionName = "void ModelEditorMainTab::mouseLeftButtonDown()";
			string errorMsg = "No model in the listbox was selected.";

			mGuiMessageBox->show( &title, &functionName, &errorMsg );

			return;
		}


		// Get the listbox text meshName;
		meshFileName = item->getText().c_str();


		/*** Handle if we auto name or name the scene node and entity ourselves ***/
		if ( mRadioAddMeshAutoName->isSelected() )
		{
			sceneNodeManager->getFreeAutoNameSceneNode(sceneNodeName);
			sceneNodeManager->getFreeAutoNameEntity(entityName, meshFileName);
		}
		else if ( mRadioAddMeshName->isSelected() )
		{
			sceneNodeName = mAddMeshEditboxName->getText().c_str();
			entityName = sceneNodeName;


			// Make the editbox lose focus ***/
			mAddMeshEditboxName->deactivate();

			// Clear the editbox text;
			mAddMeshEditboxName->setText("");



			// If there's no text in the editbox, than return;
			if ( sceneNodeName.empty() )
			{
				string title = "ERROR";
				string functionName = "void ModelEditorMainTab::mouseLeftButtonDown()";
				string errorMsg = "You did not enter a model name.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return;
			}


			// Create the SceneNode, If the SceneNode already exists than return;
			if ( !sceneNodeManager->getFreeSceneNode(sceneNodeName) )
			{
				string title = "ERROR";
				string functionName = "void ModelEditorMainTab::mouseLeftButtonDown()";
				string errorMsg = "The model name already exists.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return;
			}
			

			// Create the Entity, If the Entity already exists than return;
			if ( !sceneNodeManager->getFreeEntity(entityName, meshFileName) )
			{
				string title = "ERROR";
				string functionName = "void ModelEditorMainTab::mouseLeftButtonDown()";
				string errorMsg = "The entity name already exists.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return;
			}
		}


		/*** Attach the entity to the scenenode ***/
		sceneNodeManager->attachEntityToSceneNode(sceneNodeName, entityName);

		/*** Get the scenenode by its name and then position it at the cursor ***/
		tmpNode = sceneManager->getSceneNode(sceneNodeName);		
		rayCastManager->setSceneNodeAtCursor(tmpNode);

		/*** Select the SceneNode that was just created ***/
		customSceneNodeManipulator->select(tmpNode);
	}
	else if ( mRadioEditMesh->isSelected() )
	{
		if ( mRadioEditModeSelect->isSelected() )
		{
			customSceneNodeManipulator->select(sceneNode);
		}
		else if ( mRadioEditModeAttachNode->isSelected() )
		{
			//customSceneNodeManipulator->attach(sceneNode);
		}
		else if ( mRadioEditModeDettachNode->isSelected() )
		{
			//customSceneNodeManipulator->detach();
		}
		else if ( mRadioEditModeDeselect->isSelected() )
		{
			customSceneNodeManipulator->deselect(sceneNode);
		}
		else if ( mRadioRotate->isSelected() )		// Rotate;
		{
			customSceneNodeManipulator->yaw(mModelMoveDegree);
		}
		else if ( mRadioRoll->isSelected() )		// Roll;
		{
			customSceneNodeManipulator->roll(mModelMoveDegree);
		}
		else if ( mRadioPitch->isSelected() )		// Pitch;
		{
			customSceneNodeManipulator->pitch(mModelMoveDegree);
		}
		else if ( mRadioX->isSelected() )		
		{
			customSceneNodeManipulator->translateX(mModelMoveDegree);
		}
		else if ( mRadioY->isSelected() )		
		{
			customSceneNodeManipulator->translateY(mModelMoveDegree);
		}
		else if ( mRadioZ->isSelected() )		
		{
			customSceneNodeManipulator->translateZ(mModelMoveDegree);
		}
		else if ( mRadioDrag->isSelected() )
		{
			customSceneNodeManipulator->drag();	
		}
		else if ( mRadioDeleteMesh->isSelected() )
		{
			customSceneNodeManipulator->deleteSelected();
		}
		else if ( mRadioCopy->isSelected() )
		{
			customSceneNodeManipulator->copySelected();
		}
	}
}




void ModelEditorMainTab::mouseRightButtonDown()
{
	CustomSceneNodeManipulator* const customSceneNodeManipulator = CustomSceneNodeManager::getSingletonPtr()->getCustomSceneNodeManipulator();



	if ( mRadioEditMesh->isSelected() )
	{
		// If right click when either the "select" or "deselect" radio is selected, than clear the list;
		if ( mRadioEditModeSelect->isSelected() )
		{
			customSceneNodeManipulator->deselect();
		}
		else if ( mRadioEditModeDeselect->isSelected() )
		{
			customSceneNodeManipulator->deselect();
		}
		else if ( mRadioRotate->isSelected() )		// Rotate;
		{
			customSceneNodeManipulator->yaw(-mModelMoveDegree);
		}
		else if ( mRadioRoll->isSelected() )		// Roll;
		{
			customSceneNodeManipulator->roll(-mModelMoveDegree);
		}
		else if ( mRadioPitch->isSelected() )		// Pitch;
		{
			customSceneNodeManipulator->pitch(-mModelMoveDegree);
		}
		else if ( mRadioX->isSelected() )		
		{
			customSceneNodeManipulator->translateX(-mModelMoveDegree);
		}
		else if ( mRadioY->isSelected() )		
		{
			customSceneNodeManipulator->translateY(-mModelMoveDegree);
		}
		else if ( mRadioZ->isSelected() )		
		{
			customSceneNodeManipulator->translateZ(-mModelMoveDegree);
		}
	}
}

void ModelEditorMainTab::updateWindowShown()
{
	// Refresh the mouse rate;
	ModelEditorMainTab::_setGuiMouseRate();
	ModelEditorMainTab::_setMouseRate();

	// Refresh the scrollbars;
	ModelEditorMainTab::_reAdjustScrollbars();
}

void ModelEditorMainTab::_reAdjustScrollbars()
{
	string tmpstr;
	CEGUI::PropertyHelper phelper;
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();


	/*** Set the camera rate scrollbar and label ***/
	mCameraDegree = cameraManager->getCameraSceneNodeMoveDegree();

	tmpstr = "Camera Rate: ";

	mCamDegree_Scrollbar->setScrollPosition(mCameraDegree);

	tmpstr += phelper.floatToString(mCameraDegree).c_str();

	// Set the text;
	mCamDegreeLabel->setText(tmpstr);
}

void ModelEditorMainTab::_setMouseRate()
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();


	if ( mRadioAddMesh->isSelected() )
	{
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_ADD_INDEX));
	}
	else if ( mRadioEditMesh->isSelected() )
	{
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX));
	}
}

void ModelEditorMainTab::_setGuiMouseRate()
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();
	string tmpstr = "Mouse Rate: ";
	CEGUI::PropertyHelper phelper;


	if ( mRadioAddMesh->isSelected() )
	{
		tmpstr += phelper.uintToString( mVecMouseRate.at(MOUSE_RATE_ADD_INDEX) ).c_str();
		mMouseRateScrollbar->setScrollPosition( mVecMouseRate.at(MOUSE_RATE_ADD_INDEX) );
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_ADD_INDEX));
	}
	else if ( mRadioEditMesh->isSelected() )
	{
		tmpstr += phelper.uintToString( mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX) ).c_str();
		mMouseRateScrollbar->setScrollPosition( mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX) );
		sheet->setAutoRepeatRate(mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX));
	}


	// Set the text;
	mMouseRateText->setText( tmpstr );
}

bool ModelEditorMainTab::event_windowVisible(const CEGUI::EventArgs& pEventArgs)
{
  // set the Node Manipulator mode to entity.
	CustomSceneNodeManager* const sceneNodeManager = CustomSceneNodeManager::getSingletonPtr();
	CustomSceneNodeManipulator* const customSceneNodeManipulator = sceneNodeManager->getCustomSceneNodeManipulator();
	customSceneNodeManipulator->setMode(CustomSceneNodeManipulator::ENTITY);

	ModelEditorMainTab::updateWindowShown();

	return true;
}

bool ModelEditorMainTab::event_mModelMoveDegreeScrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Model Rate: ";
	CEGUI::PropertyHelper phelper;
	float scrollPos = mModelMoveDegreeScrollbar->getScrollPosition();

	// Since the scrollbar starts at '0', we want it to never decrease passed '0.001';
	// if the scroll pos is at 0 then we set the scroll position to 0.001;
	if (scrollPos < 0.001f)
	{
		scrollPos = 0.001f;
		 mModelMoveDegreeScrollbar->setScrollPosition(scrollPos);
	}

	tmpstr += phelper.floatToString( scrollPos ).c_str();

	// Set the text;
	mModelMoveDegreeText->setText( tmpstr );

	// Set the class member;
	mModelMoveDegree = scrollPos;


	return true;
}

bool ModelEditorMainTab::event_mCamDegree_Scrollbar_change(const CEGUI::EventArgs& pEventArgs)
{
	string tmpstr = "Camera Rate: ";
	CEGUI::PropertyHelper phelper;
	Ogre::Real scrollPos = mCamDegree_Scrollbar->getScrollPosition();
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();

	// Since the scrollbar starts at '0', we want it to never decrease passed '0.001';
	// if the scroll pos is at 0 then we set the scroll position to 0.001;
	if (scrollPos < 0.001f)
	{
		scrollPos = 0.001f;
		mCamDegree_Scrollbar->setScrollPosition(scrollPos);
	}

	tmpstr += phelper.floatToString(scrollPos).c_str();

	// Set the text;
	mCamDegreeLabel->setText(tmpstr); 

	// Set the class member;
	mCameraDegree = scrollPos;


	// Set the camera sceneNode and camera degree;
	cameraManager->setCameraSceneNodeMoveDegree(mCameraDegree);
	cameraManager->setCameraMoveDegree(mCameraDegree);


	return true;
}

bool ModelEditorMainTab::event_mMouseRateScrollbar_change(const CEGUI::EventArgs& e)
{
	CEGUI::Window* const sheet = CeguiManager::getSingleton().getParentWindow();
	string tmpstr = "Mouse Rate: ";
	CEGUI::PropertyHelper phelper;
	const unsigned int scrollPos = mMouseRateScrollbar->getScrollPosition();


	if ( mRadioAddMesh->isSelected() )
	{
		mVecMouseRate.at(MOUSE_RATE_ADD_INDEX) =  scrollPos;
	}
	else if ( mRadioEditMesh->isSelected() )
	{
		mVecMouseRate.at(MOUSE_RATE_EDIT_INDEX) =  scrollPos;
	}


	tmpstr += phelper.uintToString( scrollPos ).c_str();

	// Set the text;
	mMouseRateText->setText( tmpstr );

	// Set the mouse rate;
	sheet->setAutoRepeatRate(scrollPos);


	return true;
}

void ModelEditorMainTab::_radio_addModel_selected()
{
	/*** Set the mouse rate to be very slow ***/
	_setGuiMouseRate();

	/*** Handle what should and shouldn't be visible ***/
	mMeshListbox->setVisible(true);
	mAddMeshRefreshListboxButton->setVisible(true);
	mFrameBelowRefreshListboxButton->setVisible(true);
	mAddMeshLabel->setVisible(true);
	mAddMeshEditboxName->setVisible(true);
	mRadioAddMeshName->setVisible(true);
	mRadioAddMeshAutoName->setVisible(true);
	mFrameBelowMeshAutoName->setVisible(true);

	mEditModeText->setVisible(false);
	mRadioEditModeSelect->setVisible(false);
	mRadioEditModeDeselect->setVisible(false);
	mRadioEditModeAttachNode->setVisible(false);
	mRadioEditModeDettachNode->setVisible(false);
	mRadioDeleteMesh->setVisible(false);
	mRadioCopy->setVisible(false);
	mRadioRotate->setVisible(false);
	mRadioRoll->setVisible(false);
	mRadioPitch->setVisible(false);
	mRadioX->setVisible(false);
	mRadioY->setVisible(false);
	mRadioZ->setVisible(false);
	mRadioDrag->setVisible(false);

	mTextSceneNodeName->setVisible(false);
	mFrameBelowTextSceneNodeName->setVisible(false);

	mScaleText->setVisible(false);
	mScaleEditboxX->setVisible(false);
	mScaleEditboxY->setVisible(false);
	mScaleEditboxZ->setVisible(false);
	mScaleApplyButton->setVisible(false);
	mFrameBelowApplyButton->setVisible(false);

	mModelMoveDegreeText->setVisible(false);
	mModelMoveDegreeScrollbar->setVisible(false);
}

void ModelEditorMainTab::_radio_EditModel_selected()
{
	/*** Set the mouse rate to be very fast ***/
	_setGuiMouseRate();

	/*** Handle what should and shouldn't be visible ***/
	mMeshListbox->setVisible(false);
	mAddMeshRefreshListboxButton->setVisible(false);
	mFrameBelowRefreshListboxButton->setVisible(false);
	mAddMeshLabel->setVisible(false);
	mAddMeshEditboxName->setVisible(false);
	mRadioAddMeshName->setVisible(false);
	mRadioAddMeshAutoName->setVisible(false);
	mFrameBelowMeshAutoName->setVisible(false);

	mEditModeText->setVisible(true);
	mRadioEditModeSelect->setVisible(true);
	mRadioEditModeDeselect->setVisible(true);
	mRadioEditModeAttachNode->setVisible(true);
	mRadioEditModeDettachNode->setVisible(true);
	mRadioDeleteMesh->setVisible(true);
	mRadioCopy->setVisible(true);
	mRadioRotate->setVisible(true);
	mRadioRoll->setVisible(true);
	mRadioPitch->setVisible(true);
	mRadioX->setVisible(true);
	mRadioY->setVisible(true);
	mRadioZ->setVisible(true);
	mRadioDrag->setVisible(true);

	mTextSceneNodeName->setVisible(true);
	mFrameBelowTextSceneNodeName->setVisible(true);

	mScaleText->setVisible(true);
	mScaleEditboxX->setVisible(true);
	mScaleEditboxY->setVisible(true);
	mScaleEditboxZ->setVisible(true);
	mScaleApplyButton->setVisible(true);
	mFrameBelowApplyButton->setVisible(true);

	mModelMoveDegreeText->setVisible(true);
	mModelMoveDegreeScrollbar->setVisible(true);
}

bool ModelEditorMainTab::event_radio_modelMode_change(const CEGUI::EventArgs& pEventArgs)
{
	if ( mRadioAddMesh->isSelected() )
	{
		_radio_addModel_selected();
	}
	else if ( mRadioEditMesh->isSelected() )
	{
		_radio_EditModel_selected();
	}


	return true;

}

bool ModelEditorMainTab::event_scaleApply(const CEGUI::EventArgs& pEventArgs)
{
	CustomSceneNodeManipulator* const customSceneNodeManipulator = CustomSceneNodeManager::getSingletonPtr()->getCustomSceneNodeManipulator();

	Ogre::StringConverter sc;

	string str_scale_x = mScaleEditboxX->getText().c_str();
	string str_scale_y = mScaleEditboxY->getText().c_str();
	string str_scale_z = mScaleEditboxZ->getText().c_str();

	Ogre::Real x = 0.0;
	Ogre::Real y = 0.0;
	Ogre::Real z = 0.0;


	if ( str_scale_x == "" || str_scale_y == "" || str_scale_z == "" )
	{
		string title = "ERROR";
		string functionName = "bool ModelEditorMainTab::event_scaleApply(...)";
		string errorMsg = "A Scale editbox is blank.";

		mGuiMessageBox->show( &title, &functionName, &errorMsg );

		return false;
	}
	else
	{
		// Check if the editbox texts have valid numbers;
		if ( sc.isNumber(str_scale_x) && sc.isNumber(str_scale_y) && sc.isNumber(str_scale_z) )
		{
			x = sc.parseReal(str_scale_x);
			y = sc.parseReal(str_scale_y);
			z = sc.parseReal(str_scale_z);


			// When scaling 1 = normal so we get the number and multiple it by 0.01 and then add +1.
			// That way we get the scaling percent.
			// For increase scaling we want it to go from 0 to 100 and for decreasing -99.9 to 0.
			// The reason we don't want -100 is because that decreases the model all the way.
			if ( (x > -100.0 && x <= 100.0) && (y > -100.0 && y <= 100.0) && (z > -100.0 && z <= 100.0) )
			{
				try
				{
					// When scaling 1 = normal so we get the number and multiple it by 0.01 and then add +1.
					// That way we get the scaling percent.
					x = (x * 0.01f) + 1;
					y = (y * 0.01f) + 1;
					z = (z * 0.01f) + 1;

					/*** Scale the sceneNodes ***/
					customSceneNodeManipulator->scale(x,y,z);
				}
				catch(...)
				{
					string title = "ERROR";
					string functionName = "bool ModelEditorMainTab::event_scaleApply(...)";
					string errorMsg = "An error occurred while scaling. Possibly a problem with the scale numbers.";

					mGuiMessageBox->show( &title, &functionName, &errorMsg );

					return false;
				}
			}
			else
			{
				string title = "ERROR";
				string functionName = "bool ModelEditorMainTab::event_scaleApply(...)";
				string errorMsg = "The scale percent numbers must be bigger than -100 and no bigger than 100.";

				mGuiMessageBox->show( &title, &functionName, &errorMsg );

				return false;
			}
		}
		else
		{
			string title = "ERROR";
			string functionName = "bool ModelEditorMainTab::event_scaleApply(...)";
			string errorMsg = "The Scale Editboxes don't have valid numbers.";

			mGuiMessageBox->show( &title, &functionName, &errorMsg );

			return false;
		}
	}


	return true;
}

bool ModelEditorMainTab::event_refreshMesh(const CEGUI::EventArgs& pEventArgs) {
  std::cout << "Mesh button pressed!" << std::endl;
  return true;
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool ModelEditorMainTab::event_editBox_activated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mAddMeshEditboxName->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else if ( mScaleEditboxX->isActive() || mScaleEditboxY->isActive() || mScaleEditboxZ->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		guiManager->setEnabledCommandKeys(true);
	}


	return true;
}

// Events are not always ordered so we check if anything is active and if so say were busy, else say we are not;
bool ModelEditorMainTab::event_editBox_deactivated(const CEGUI::EventArgs& pEventArgs)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();


	if ( mAddMeshEditboxName->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else if ( mScaleEditboxX->isActive() || mScaleEditboxY->isActive() || mScaleEditboxZ->isActive() )
	{
		guiManager->setEnabledCommandKeys(false);
	}
	else
	{
		guiManager->setEnabledCommandKeys(true);
	}


	return true;
}

bool ModelEditorMainTab::event_sheet_mouseDown(const CEGUI::EventArgs& pEventArgs)
{
	// Deactivate the editboxes;
	mAddMeshEditboxName->deactivate();

	mScaleEditboxX->deactivate();
	mScaleEditboxY->deactivate();
	mScaleEditboxZ->deactivate();

	return true;
}




