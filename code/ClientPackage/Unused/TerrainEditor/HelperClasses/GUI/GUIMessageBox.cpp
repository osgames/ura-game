/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GUIMessageBox.h"
 

using namespace CEGUI;



bool GUIMessageBox::addWindow()
{
	CEGUI::Window* sheet = CEGUI::System::getSingleton().getGUISheet();
	CEGUI::WindowManager& windowManager = CEGUI::WindowManager::getSingleton();

	float label_x_pos = 0.04f;
	float label_y_pos = 0.0f;
	float label_x_size = 0.0f;
	float label_y_size = 0.0f;

	float title_bar_y_size = 0.0f;

	float button_y_pos = 0.75f;
	float button_x_size = 0.4f;
	float button_y_size = 0.12f;



	// window;
	mMainWindow = (CEGUI::FrameWindow*) windowManager.createWindow("TaharezLook/FrameWindow");
	mMainWindow->setSize(CEGUI::UVector2(CEGUI::UDim(0.4, 0), CEGUI::UDim(0.3, 0)));
	mMainWindow->setVerticalAlignment(CEGUI::VA_CENTRE);
	mMainWindow->setHorizontalAlignment(CEGUI::HA_CENTRE);

	// Ok button;
	mButtonOk = (CEGUI::PushButton*) windowManager.createWindow("TaharezLook/Button");
	mButtonOk->setText("Ok");
	mButtonOk->setSize(CEGUI::UVector2(CEGUI::UDim(button_x_size, 0), CEGUI::UDim(button_y_size, 0)));
	mButtonOk->setYPosition( CEGUI::UDim(button_y_pos, 0) );
	mButtonOk->setHorizontalAlignment(CEGUI::HA_CENTRE);

	title_bar_y_size = mMainWindow->getTitlebar()->getSize().d_y.d_scale;
	label_y_pos =  0.2f + title_bar_y_size;
	label_x_size = 1.0f - (label_x_pos + label_x_pos);
	label_y_size = mButtonOk->getPosition().d_y.d_scale - (title_bar_y_size + label_y_pos + 0.05);

	// chat window area;
	mLabel = (CEGUI::MultiLineEditbox *) windowManager.createWindow( "TaharezLook/MultiLineEditbox" );
	mLabel->setReadOnly(true);
	mLabel->setWordWrapping(true);
	mLabel->setSize(CEGUI::UVector2(CEGUI::UDim(label_x_size, 0), CEGUI::UDim(label_y_size, 0)));
	mLabel->setPosition( CEGUI::UVector2(CEGUI::UDim(label_x_pos, 0), CEGUI::UDim(label_y_pos, 0) ) );


	/******************/
	/*** The Events ***/
	/******************/

	mButtonOk->subscribeEvent( CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(
			&GUIMessageBox::event_button_ok, this ) );
	

	/********************/
	/*** The Settings ***/
	/********************/

	mMainWindow->setVisible(false);


	/***********************/
	/*** Add the windows ***/
	/***********************/

	mMainWindow->addChildWindow( mLabel );	
	mMainWindow->addChildWindow( mButtonOk );

	sheet->addChildWindow( mMainWindow );

	
	return true;
} 

GUIMessageBox::~GUIMessageBox()
{
	/*** Destroy the window ***/
	CEGUI::WindowManager::getSingleton().destroyWindow(mMainWindow);
}

void GUIMessageBox::show(std::string *title, std::string *functionName, std::string *message, bool log)
{
	std::string tmp;


	mMainWindow->setVisible(true);

	// Set the titlebar text;
	mMainWindow->getTitlebar()->setText( "    " + *title );

	// Set the label text;
	mLabel->setText( *message );

	// Only allow this window to be accessed until the ok button is clicked;
	mMainWindow->setModalState(true);

	// Combine everything;
	tmp = *title + ":  " + *functionName + ": " + *message;
	
	// Determine if we should log or just message box and cout;
	if ( log )
	{
		// Log the message;
		Ogre::LogManager::getSingleton().logMessage( tmp );
	}
	else
	{
		std::cout << tmp << std::endl;
	}
}


bool GUIMessageBox::event_button_ok(const CEGUI::EventArgs &e)
{
	mMainWindow->setVisible(false);

	// Allow the other windows to be accessed again;
	mMainWindow->setModalState(false);

	return true;
}

