/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef HEAP_QUITWINDOW_H
#define HEAP_QUITWINDOW_H


/****************/
/*** INCLUDES ***/
/****************/
#include <CEGUI/CEGUI.h>
#include <string>
#include <iostream>


/***************/
/*** CLASSES ***/
/***************/
class HeapQuitWindow;


/***************/
/*** DEFINES ***/
/***************/
#define DEF_QUITWINDOW_MAINWINDOW_NAME	"QuitWindow_MainWindow"



class QuitWindow
{
protected:
	bool event_noQuit(const CEGUI::EventArgs &e);
	bool event_quit(const CEGUI::EventArgs &e);
public:
	QuitWindow();
	~QuitWindow();

	bool addChildWindow();
	void toggle() const;
	CEGUI::Window* const getWindow() const;
}; 


class HeapQuitWindow
{
protected:
	QuitWindow* mQuitWindow;
public:
	HeapQuitWindow();
	~HeapQuitWindow();
	
	void toggle();
	bool isVisible();
	CEGUI::Window* const getWindow() const;
}; 


#endif

