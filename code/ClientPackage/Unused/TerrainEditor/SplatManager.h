/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SPLATMANAGER_H
#define SPLATMANAGER_H


/****************/
/*** INCLUDES ***/
/****************/
#include <vector>
#include <string>
#include <iostream>

#include <Terrain.h>

#include <Ogre.h>
#include <OgreTexture.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreTextureManager.h>
#include <OgrePlatform.h>

#include <OgreMaterialManager.h>
#include <OgreEntity.h>
#include <OgreSubEntity.h>
#include <OgreMaterial.h>



/***************/
/*** DEFINES ***/
/***************/
#define DEF_IMAGE_FORMAT PF_A8R8G8B8



class SplatManager
{
protected:
	Ogre::HardwarePixelBufferSharedPtr mSplatMapBuffer;
	Ogre::TexturePtr mSplatMapTexture;

	std::string* mDynamicTextureName;
	std::string* mMaterialName;

	unsigned long* mBufferHeight;
	unsigned long* mBufferWidth;

	Ogre::Vector4* mSplatScales;
	Ogre::Vector4* mDetailScales;

	Ogre::Real* mTerrainLengthWidth;

	std::vector<std::string>* mVbrushNames;
	std::string* mCurrentBrushName;

	std::vector<std::string>* mVtextureNames;
	std::string* mCurrentTextureName;

	std::string* mSplatMapFileName;
public:
	SplatManager();
	~SplatManager();

	void setSplatMapFileName(std::string str);
	std::string getSplatMapFileName();

	void setupNoDynamicTexture();

	void setup(Ogre::Real terrainLengthWidth,
		const unsigned long bufferWidth, const unsigned long bufferHeight,
		std::string materialName);

	void brush(unsigned long brushSizeX, unsigned long brushSizeY,
		const bool mirrorX, const bool mirrorY,
		long altAplhaColor, long altRedColor, long altGreenColor, long altBlueColor);

	void brush(unsigned long brushSizeX, unsigned long brushSizeY,
		const bool mirrorX, const bool mirrorY);

	void splatSinglePixel(const unsigned long &x, const unsigned long &y,
		Ogre::uint32 &int32Color);

	void splatBrushImage(const unsigned long &brushSizeX, const unsigned long &brushSizeY,
		const unsigned long &tmp_y_top, const unsigned long &tmp_y_bottom,
		const unsigned long &tmp_x_left, const unsigned long &tmp_x_right,
		const Ogre::uint32 &int32Color, const bool &mirrorX, const bool &mirrorY);

	void fillTextureWithSingleColor(const Ogre::uint32 &int32Color);

	void saveTexture(std::string fileName);

	// Functions for the brushes;
	void addBrush(std::string name);
	void removeBrush(std::string name);
	unsigned long getBrushCount() const;
	bool setCurrentBrush(const unsigned long num);
	std::string* getCurrentBrush();

	// Functions for the textures;
	void addTexture(std::string name);
	unsigned long getTextureCount() const;
	bool setCurrentTexture(const unsigned long num);
	void setTextures(std::string tex0, std::string tex1, std::string tex2, std::string text3);
	std::vector<std::string>* getTextures();

	Ogre::Vector4 getSplatScales() const;
	Ogre::Vector4 getDetailScales() const;
	void setSplatScales(const Ogre::Vector4 v4);
	void setDetailScales(const Ogre::Vector4 v4);
};




#endif

