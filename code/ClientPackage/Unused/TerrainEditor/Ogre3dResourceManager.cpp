/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Ogre3dResourceManager.h"


Ogre3dResourceManager::Ogre3dResourceManager()
{
	Ogre::ConfigFile cf;

	Ogre::String typeName;
	Ogre::String archName;
	Ogre::ConfigFile::SettingsMultiMap* settings = NULL;

	Ogre::ResourceGroupManager* resourceManager = Ogre::ResourceGroupManager::getSingletonPtr();



	cf.load( "resources.cfg" );


	// Go through all of the file.
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
	while (seci.hasMoreElements())
	{
		Ogre::String secName = seci.peekNextKey();
		settings = seci.getNext();

		for( Ogre::ConfigFile::SettingsMultiMap::iterator i = settings->begin(); i != settings->end(); ++i )
		{
			typeName = i->first;
			archName = i->second;
			resourceManager->addResourceLocation(archName, typeName, secName);
		}
	}


	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
} 

