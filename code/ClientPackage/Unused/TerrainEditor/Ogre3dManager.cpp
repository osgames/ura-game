/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Ogre3dManager.h"



template<> Ogre3dManager* Ogre::Singleton<Ogre3dManager>::ms_Singleton = 0;

Ogre3dManager* const Ogre3dManager::getSingletonPtr()
{
	return ms_Singleton;
}



Ogre3dManager::Ogre3dManager()
{
	mRoot = new Ogre::Root;
	

	if( !mRoot->restoreConfig() )
	{
		if( !mRoot->showConfigDialog() )
		{
			return;
		}
	}


	mRoot->initialise( true, "TrinityReign World Editor (TRWE)" );
	mSceneManager = mRoot->createSceneManager( Ogre::ST_GENERIC, "scene_manager" );


	/*** Create the Ogre3d resource manager which loads the resources from the resources file ***/
	mOgre3dResourceManager = new Ogre3dResourceManager;

	/*** Create the timer manager which is used to store and get the last since last frame ***/
	mOgre3dTimerManager = new Ogre3dTimerManager;

	/*** Create the log manager ***/
	mOgre3dLogManager = new Ogre3dLogManager;
} 

Ogre3dManager::~Ogre3dManager()
{
	if (mRoot)
	{
		delete mRoot;
		mRoot = NULL;
	}

	if (mOgre3dResourceManager)
	{
		delete mOgre3dResourceManager;
		mOgre3dResourceManager = NULL;
	}

	if (mOgre3dTimerManager)
	{
		delete mOgre3dTimerManager;
		mOgre3dTimerManager = NULL;
	}

	if (mOgre3dLogManager)
	{
		delete mOgre3dLogManager;
		mOgre3dLogManager = NULL;
	}
}



Ogre::Root* const Ogre3dManager::getRoot()
{
	return mRoot;
}

Ogre::SceneManager* const Ogre3dManager::getSceneManager()
{
	return mSceneManager;
}

Ogre::RenderWindow* const Ogre3dManager::getRenderWindow()
{
	return mRoot->getAutoCreatedWindow();
}

Ogre3dTimerManager* const Ogre3dManager::getOgre3dTimerManager()
{
	return mOgre3dTimerManager;
}

Ogre3dLogManager* const Ogre3dManager::getOgre3dLogManager()
{
	return mOgre3dLogManager;
}


