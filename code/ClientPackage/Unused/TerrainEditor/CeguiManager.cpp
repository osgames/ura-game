/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CeguiManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace std;


template<> CeguiManager* Ogre::Singleton<CeguiManager>::ms_Singleton = 0;

CeguiManager* const CeguiManager::getSingletonPtr()
{
	return ms_Singleton;
}



CeguiManager::CeguiManager() :	mOgreCeguiRenderer(NULL),
				mSystem(NULL),
				mParentWindow(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::RenderWindow* const renderWindow = ogre3dManager->getRoot()->getAutoCreatedWindow();

	std::string strErrorFunction = "CeguiManager::CeguiManager()";
	std::string strErrorWordNameOrMessage;



	try
	{
		/*** Create the cegui renderer and system. Both don't need to be deleted ***/
		mOgreCeguiRenderer = new CEGUI::OgreCEGUIRenderer(renderWindow, Ogre::RENDER_QUEUE_OVERLAY, false, 3000, sceneManager);
		mSystem = new CEGUI::System(mOgreCeguiRenderer);

		
		/*** Load the Scheme ***/
		CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*)"TaharezLookSkin.scheme");


		/*** Allocate heap memory ***/
		// Note: a scheme Manager and font manager may also be used in the future;
		mCeguiCursorManager = new CeguiCursorManager;


		/*** Set the default font ***/
		mSystem->setDefaultFont((CEGUI::utf8*)"BlueHighway-12");


		/*** There is no root window so lets create one ***/
		mParentWindow = CEGUI::WindowManager::getSingleton().createWindow("DefaultGUISheet", "GUISheet");

		/*** Setup mouse auto-repeat for the root window ***/
		mParentWindow->setMouseAutoRepeatEnabled(true);
		mParentWindow->setAutoRepeatRate(100);

		/*** install this as the root GUI sheet ***/
		mSystem->setGUISheet(mParentWindow);
	}
	catch(CEGUI::Exception e)
	{
		strErrorWordNameOrMessage = e.getMessage().c_str();
		ogre3dLogManager->logErrorMessage(strErrorFunction, strErrorWordNameOrMessage);
	}


	/*** If there was an error allocating memory than log it ***/
	if (!mCeguiCursorManager)
	{
		strErrorWordNameOrMessage = "mCeguiCursorManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordNameOrMessage);
	}
} 

CeguiManager::~CeguiManager()
{
	// Destroy the CeguiCursorManager;
	if (mCeguiCursorManager)
	{
		delete mCeguiCursorManager;
		mCeguiCursorManager = NULL;
	}

	// Destroy the cegui parent window;
	if (mParentWindow)
	{
		CEGUI::WindowManager::getSingleton().destroyWindow(mParentWindow);
	}
}



CEGUI::System* const CeguiManager::getSystem() const
{
	return mSystem;
}


CeguiCursorManager* const CeguiManager::getCeguiCursorManager() const
{
	return mCeguiCursorManager;
}

CEGUI::Window* const CeguiManager::getParentWindow() const
{
	return mParentWindow;
}


std::string CeguiManager::getImagePropertyString(const std::string& name) const
{
	const std::string tmp = "set:" + name + " image:full_image";

	return tmp;
} 

