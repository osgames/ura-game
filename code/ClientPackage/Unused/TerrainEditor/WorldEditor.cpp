/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#include "Ogre3dManager.h"
#include "Ogre3dTimerManager.h"
#include "CeguiManager.h"
#include "EditManager.h"
#include "Input.h"
#include "GUIManager.h"
#include "WorldEditor.h"


using namespace std;



WorldEditor::WorldEditor()
{
	mFrameTimer = new Ogre::Timer();
	mIsRunning = DEF_WORLDEDITOR_RUNNING;


	mOgre3dManager = new Ogre3dManager;
	mCeguiManager = new CeguiManager;

	mEditMgr = new EditManager(this);
	mGUIManager = new GUIManager;
}

WorldEditor::~WorldEditor()
{
	if (mGUIManager)
	{
		delete mGUIManager;
		mGUIManager = NULL;
	}

	if (mEditMgr)
	{
		delete mEditMgr;
		mEditMgr = NULL;
	}

	if (mOgre3dManager)
	{
		delete mOgre3dManager;
		mOgre3dManager = NULL;
	}

	if (mCeguiManager)
	{
		delete mCeguiManager;
		mCeguiManager = NULL;
	}

	if (mFrameTimer)
	{
		delete mFrameTimer;
		mFrameTimer = NULL;
	}
}

void WorldEditor::start()
{
	Ogre::Root* const root = mOgre3dManager->getRoot();
	CEGUI::System* const ceguiSystem = mCeguiManager->getSystem();
	Ogre::WindowEventUtilities windowEventUtilities;

	Input* const input = mGUIManager->getInput();
	Ogre3dTimerManager* const ogre3dTimerManager = mOgre3dManager->getOgre3dTimerManager();
	unsigned long frameTime = 0;

	Ogre::Timer* const rootTimer = root->getTimer();
	const unsigned short defaultFrameRate = 25;	// Default frame rate of game update is 25 FPS;
	const unsigned short maxFrameSkip = 5;
	const unsigned long skipTicks = 1000 / defaultFrameRate;
	unsigned long nextGameTick = rootTimer->getMilliseconds();
 //	float interpolation = 0.0f;




	if (root && ceguiSystem && input && ogre3dTimerManager)
	{
		while(mIsRunning == DEF_WORLDEDITOR_RUNNING)
		{
			unsigned long loops = 0;
		
			// Default frame rate of game update is 25 FPS.
			while ( rootTimer->getMilliseconds() > nextGameTick && loops < maxFrameSkip)
			{
			//	 update_game();

				/*** Update the keyboard ***/
				input->updateKeyBoard();

				// send a gui message Pump;
				windowEventUtilities.messagePump();


				nextGameTick += skipTicks;
				loops++;
			}
	

			/*** Get the interpolation to make movements more smoother with variable fps ***/
		//	interpolation = float(rootTimer->getMilliseconds() + skipTicks - nextGameTick) / float(skipTicks);
		////	display_game(interpolation);


			/*** Update the mouse ***/
			input->updateMouse();


			/*** Store the frame started time, show the frame, store the frame end time and reset the timer ***/
			frameTime = mFrameTimer->getMilliseconds();
			mEditMgr->frameStarted(&frameTime);
			root->renderOneFrame();
			frameTime = mFrameTimer->getMilliseconds();
			mEditMgr->frameEnded(&frameTime);
	
			// store and update the time since last frame;
			ogre3dTimerManager->updateTimeSinceLastFrame(&frameTime);


			// Inject a cegui time pulse which is needed for tooltips and certain things;
			ceguiSystem->injectTimePulse(frameTime);


			// Reseet the frame Timer;
			mFrameTimer->reset();
		}
	}
	else
	{
		if (!root)
		{
			cout << "ERROR: void WorldEditor::start(): Could not get Ogre::Root::getSingletonPtr()."
				<< endl;
		}

		if (!ceguiSystem)
		{
			cout << "ERROR: void WorldEditor::start(): Could not get CEGUI::System::getSingletonPtr()."
				<< endl;
		}

		if (!input)
		{
			cout << "ERROR: void WorldEditor::start(): Could not get Input."
				<< endl;
		}

		if (!ogre3dTimerManager)
		{
			cout << "ERROR: void WorldEditor::start(): Could not get Ogre3dTimerManager."
				<< endl;
		}
	}
}

void WorldEditor::end()
{
	mIsRunning = DEF_WORLDEDITOR_NOT_RUNNING;
}

