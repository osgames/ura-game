/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <string>
#include <vector>
#include <Ogre.h>



class GameObject
{
private:
	std::string sName;
	std::vector<std::string> vAttributeValue;
	std::vector<std::string> vAttributeName;
	Ogre::SceneNode *mSceneNode;
public:
	GameObject( std::string name );
	~GameObject();
	void setName( std::string name );
	std::string getName();
	void setAttribute( std::string name, std::string value );
	std::string getAttribute( std::string name );
	void setSceneNode( Ogre::SceneNode *sn );
	Ogre::SceneNode *getSceneNode();
};

#endif

