/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUI.h"
#include "WorldEditorProperties.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "CeguiManager.h"
#include "CeguiCursorManager.h"
#include "TerrainManager.h"
#include "GUIManager.h"
#include "Input.h"
#include "RayCastManager.h"


/***************/
/*** DEFINES ***/
/***************/


/*** Store the Menubar button ID's ***/
// No Window Open ID;
#define UI_NO_WINDOW_OPEN_ID		0

// Menu "File";
#define UI_FILE_NEWMAP_ID		11
#define UI_FILE_LOADMAP_ID		12
#define UI_FILE_SAVEMAP_ID		13
#define UI_FILE_QUITWINDOW_ID		14

#define UI_FILE_ID_NAME			"File  "
#define UI_FILE_NEWMAP_ID_NAME		"New Map"
#define UI_FILE_LOADMAP_ID_NAME		"Load Map"
#define UI_FILE_SAVEMAP_ID_NAME		"Save Map"
#define UI_FILE_QUITWINDOW_ID_NAME	"Exit  "

// Menu "Edit Mode";
#define UI_EDITMODE_TERRAINMENU_ID	21
#define UI_EDITMODE_MODELMENU_ID	22

#define UI_EDITMODE_ID_NAME		"Edit Mode"
#define UI_EDITMODE_TERRAINMENU_ID_NAME	"Terrain"
#define UI_EDITMODE_MODELMENU_ID_NAME	"Model"


using namespace std;
using namespace CEGUI;


GUI::GUI()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "GUI::GUI()";
	string strErrorMessage;

	CEGUI::Window* const mParentWindow = CeguiManager::getSingletonPtr()->getParentWindow();

	// Set the pointer;
	mEditManager = EditManager::getSingletonPtr();
	mCameraManager = mEditManager->getCameraManager();


	// Set the mouse buttons not down;
	mBmouseLeftButtonDown = false;
	mBmouseMiddleButtonDown = false;
	mBmouseRightButtonDown = false;

	mRootBgImageWindow = NULL;

	// Set the right control key not down;
	mBkeyRightCntrl = false;


	/*** Allocate heap memory for the needed objects ***/
	mHeapMenuBar = new HeapMenuBar;
	mHeapQuitWindow = new HeapQuitWindow;
	mHeapNewMapWindow = new HeapMapNewWindow;
	mHeapLoadMapWin = new HeapMapLoadWindow;
	mHeapSaveWindow = new HeapMapSaveWindow;

	mTerrainMenu = new TerrainMenu;
	mModelMenu = new ModelMenu;

	mGuiMessageBox = new GUIMessageBox;



	/*** Check for heap allocation error ***/
	if (!mTerrainMenu)
	{
		strErrorMessage = "mTerrainMenu";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mModelMenu)
	{
		strErrorMessage = "mModelMenu";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mHeapMenuBar)
	{
		strErrorMessage = "mHeapMenuBar";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mHeapQuitWindow)
	{
		strErrorMessage = "mHeapQuitWindow";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mHeapNewMapWindow)
	{
		strErrorMessage = "mHeapNewMapWindow";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mHeapLoadMapWin)
	{
		strErrorMessage = "mHeapLoadMapWin";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mHeapSaveWindow)
	{
		strErrorMessage = "mHeapSaveWindow";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}
	if (!mGuiMessageBox)
	{
		strErrorMessage = "mGuiMessageBox";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorMessage);
		mEditManager->quit();
	}



	/******************************************/
	/*** Setup the Menubar text and numbers ***/
	/******************************************/
	MenuBarDataType vDataTypeFile;
	MenuBarDataType vDataTypeEditMode;


	vDataTypeFile.setParent(UI_FILE_ID_NAME);
	vDataTypeFile.setChild(UI_FILE_NEWMAP_ID_NAME, UI_FILE_NEWMAP_ID);
	vDataTypeFile.setChild(UI_FILE_LOADMAP_ID_NAME, UI_FILE_LOADMAP_ID);
	vDataTypeFile.setChild(UI_FILE_SAVEMAP_ID_NAME, UI_FILE_SAVEMAP_ID);
	vDataTypeFile.setChild(UI_FILE_QUITWINDOW_ID_NAME, UI_FILE_QUITWINDOW_ID);

	vDataTypeEditMode.setParent(UI_EDITMODE_ID_NAME);
	vDataTypeEditMode.setChild(UI_EDITMODE_TERRAINMENU_ID_NAME, UI_EDITMODE_TERRAINMENU_ID);
	vDataTypeEditMode.setChild(UI_EDITMODE_MODELMENU_ID_NAME, UI_EDITMODE_MODELMENU_ID);


	// Push back the data types;
	mVdataTypes.push_back(vDataTypeFile);
	mVdataTypes.push_back(vDataTypeEditMode);


	/********************************/
	/*** Create the Splash window ***/
	/********************************/
	createBackgroundImagewindow();


	/**********************************/
	/*** Set the main parent events ***/
	/**********************************/

	// Setup a root event for mouse clicks;
	mParentWindow->subscribeEvent( CEGUI::Window::EventMouseButtonDown, CEGUI::Event::Subscriber( &GUI::event_sheet_mouseButtonDown, this ) );
	mParentWindow->subscribeEvent( CEGUI::Window::EventMouseMove, CEGUI::Event::Subscriber( &GUI::event_sheet_mouseMove, this ) );


	/*************************************************************************/
	/*** Only Create the windows/objects that need to be loaded on startup ***/
	/*************************************************************************/
	mHeapMenuBar->toggle(&mVdataTypes);
	mTerrainMenu->addChildWindow(false);	// false means we don't show the window but its created;
	mModelMenu->addChildWindow(false);	// false means we don't show the window but its created;
	mGuiMessageBox->addWindow();

///////	dialogBox.addChildWindow();

	/****************/
	/*** Settings ***/
	/****************/

	/*** Set the gui mode to no window open ***/
	mGuiMode = UI_NO_WINDOW_OPEN_ID;


	/*** Disable some of the menubar options on startup ***/
	string parentNameEditMode = UI_EDITMODE_ID_NAME;
	string childNameSaveMap = UI_FILE_SAVEMAP_ID_NAME;

	mHeapMenuBar->disableParent(parentNameEditMode);

	mHeapMenuBar->disableChild(childNameSaveMap);

}

GUI::~GUI()
{
	/*** Free the heap memory ***/
	if (mHeapQuitWindow) 		{ delete mHeapQuitWindow;	mHeapQuitWindow = NULL; }
	if (mHeapNewMapWindow)		{ delete mHeapNewMapWindow;	mHeapNewMapWindow = NULL; }
	if (mHeapLoadMapWin)		{ delete mHeapLoadMapWin;	mHeapLoadMapWin = NULL; }
	if (mHeapSaveWindow)		{ delete mHeapSaveWindow;	mHeapSaveWindow = NULL; }

	if (mTerrainMenu) 		{ delete mTerrainMenu;		mTerrainMenu = NULL; }
	if (mModelMenu) 		{ delete mModelMenu;		mModelMenu = NULL; }
	if (mHeapMenuBar) 		{ delete mHeapMenuBar;		mHeapMenuBar = NULL; }

	if (mGuiMessageBox) 		{ delete mGuiMessageBox;	mGuiMessageBox = NULL; }
}




void GUI::hideAllEditorWindows()
{
	if ( mTerrainMenu->isVisible() )
	{
		mTerrainMenu->toggle();
	}
	else if ( mModelMenu->isVisible() )
	{
		mModelMenu->toggle();
	}
}

void GUI::setStartupEnabledMenus()
{
	string parentFileEditMode = UI_FILE_ID_NAME;
	string childNameSaveMap = UI_FILE_SAVEMAP_ID_NAME;

	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();



	// If a map was already loaded and the user clicks on "Load map" or "new map, and
	//  they click the cancel button. Then we want to enable the menubar.
	// Else, we restore the menubar to when the editor was first loaded;
	if ( terrainManager->getTerrainLoaded() )
	{
		enableMenubar();
	}
	else
	{
		disableMenubar();

		mHeapMenuBar->enableParent(parentFileEditMode);
		mHeapMenuBar->disableChild(childNameSaveMap);
	}
}

void GUI::enableMenubar()
{
	string parentFileEditMode = UI_FILE_ID_NAME;
	string parentNameEditMode = UI_EDITMODE_ID_NAME;
	string childNameSaveMap = UI_FILE_SAVEMAP_ID_NAME;

	mHeapMenuBar->enableParent(parentFileEditMode);
	mHeapMenuBar->enableParent(parentNameEditMode);

	mHeapMenuBar->enableChild(childNameSaveMap);
}

void GUI::disableMenubar()
{
	string parentFileEditMode = UI_FILE_ID_NAME;
	string parentNameEditMode = UI_EDITMODE_ID_NAME;

	string childNameSaveMap = UI_FILE_SAVEMAP_ID_NAME;

	mHeapMenuBar->disableParent(parentFileEditMode);
	mHeapMenuBar->disableParent(parentNameEditMode);

	mHeapMenuBar->disableChild(childNameSaveMap);
}


void GUI::createBackgroundImagewindow()
{
	CEGUI::Window* const mParentWindow = CeguiManager::getSingletonPtr()->getParentWindow();


	if (!mRootBgImageWindow && mParentWindow)
	{

		string splashFileName = ROOTWINDOW_BGIMAGE_FILENAME;

		mRootBgImageWindow = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage", "GUI_mRootBgImageWindow");
		mRootBgImageWindow->setSize(CEGUI::UVector2(CEGUI::UDim(1, 0), CEGUI::UDim(1, 0)));
		mRootBgImageWindow->setPosition( CEGUI::UVector2(CEGUI::UDim(0, 0), CEGUI::UDim(0, 0) ) );
		mRootBgImageWindow->setProperty("FrameEnabled", "false");
		mRootBgImageWindow->setProperty("BackgroundEnabled", "false");

		// Disable the window from recieving events;
		mRootBgImageWindow->disable();

		// Add the window to the main Parent Window;
		mParentWindow->addChildWindow(mRootBgImageWindow);

		// Load the image;
		mCustomBgImage.load(mRootBgImageWindow, splashFileName);
	}
}

void GUI::destroyBackgroundImagewindow()
{
	if (mRootBgImageWindow)
	{
		mCustomBgImage.unload(mRootBgImageWindow);
		WindowManager::getSingleton().destroyWindow(mRootBgImageWindow);
		mRootBgImageWindow = NULL;
	}
}




bool GUI::event_sheet_mouseButtonDown(const CEGUI::EventArgs& pEventArgs)
{
	/*************************************************/
	/*** Handle the mouse Button down auto repeats ***/
	/*************************************************/

	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();

	ModelEditorMainTab* const modelEditorMainTab = mModelMenu->getModelEditorMainTab();
	ModelMenuLightingTab* const modelMenuLightingTab = mModelMenu->getModelMenuLightingTab();
	TerrainMenuBrushTabMain* const terrainMenuBrushTabMain = mTerrainMenu->getTerrainMenuBrushTabMain();
	TerrainMenuSplatTab* const terrainMenuSplatTab = mTerrainMenu->getTerrainMenuSplatTab();
	
	CEGUI::Window* const modelEditorMainTabWindow = modelEditorMainTab->getWindow();
	CEGUI::Window* const modelMenuLightingTabWindow = modelMenuLightingTab->getWindow();
	CEGUI::Window* const terrainMenuBrushTabMainWindow = terrainMenuBrushTabMain->getWindow();
	CEGUI::Window* const terrainMenuSplatTabWindow = terrainMenuSplatTab->getWindow();



	/***  Return if the cursor is over a child window ***/
	if ( ceguiCursorManager->getCursorOverWidow() )
	{
		return true;
	}


	/*** Handle The mouse buttons depending on their Gui Mode ***/
	if ( mGuiMode == UI_EDITMODE_TERRAINMENU_ID )
	{
		// The left Mouse button increases the terrain, the middle flattens it and the right mouse button decreases it;
		if ( mBmouseLeftButtonDown )
		{
			if ( terrainMenuBrushTabMainWindow->isVisible() )
			{
				terrainMenuBrushTabMain->terrainRaise();
			}
			else if ( terrainMenuSplatTabWindow->isVisible() )
			{
				terrainMenuSplatTab->splatTerrain();
			}
		}
		else if ( mBmouseMiddleButtonDown )
		{
			// We flatten the terrain to bottom if the right control key is down along with the mouse middle button;
			if ( mBkeyRightCntrl )
			{
				if ( terrainMenuBrushTabMainWindow->isVisible() )
				{
					terrainMenuBrushTabMain->terrainFlatten();
				}
			}
		}
		else if ( mBmouseRightButtonDown )
		{
			if ( terrainMenuBrushTabMainWindow->isVisible() )
			{
				terrainMenuBrushTabMain->terrainLower();
			}
		}
	}
	else if ( mGuiMode == UI_EDITMODE_MODELMENU_ID )
	{
		// The left Mouse button increases the terrain, the middle flattens it and the right mouse button decreases it;
		if ( mBmouseLeftButtonDown )
		{
			if ( modelEditorMainTabWindow->isVisible() )
			{
				modelEditorMainTab->mouseLeftButtonDown();
			}
			else if ( modelMenuLightingTabWindow->isVisible() )
			  {
			    modelMenuLightingTab->mouseLeftButtonDown();
			  }
		}
		else if ( mBmouseMiddleButtonDown )
		{

		}
		else if ( mBmouseRightButtonDown )
		{
			if ( modelEditorMainTabWindow->isVisible() )
			{
				modelEditorMainTab->mouseRightButtonDown();
			}
			else if ( modelMenuLightingTabWindow->isVisible() )
			  {
			    modelMenuLightingTab->mouseRightButtonDown();
			  }
		}
	}


	return true;
}

bool GUI::event_sheet_mouseMove(const CEGUI::EventArgs& pEventArgs)
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();

	TerrainMenuBrushTabMain* const terrainMenuBrushTabMain = mTerrainMenu->getTerrainMenuBrushTabMain();
	CEGUI::Window* const terrainMenuBrushTabMainWindow = terrainMenuBrushTabMain->getWindow();
	TerrainMenuSplatTab* const terrainMenuSplatTab = mTerrainMenu->getTerrainMenuSplatTab();
	CEGUI::Window* const terrainMenuSplatTabWindow = terrainMenuSplatTab->getWindow();

	GUIManager* const guiManager = GUIManager::getSingletonPtr();



	/*** Hide the brush and Return if the cursor is over a child window ***/
	if ( ceguiCursorManager->getCursorOverWidow() )
	{
		terrainManager->showBrush(false);
		return true;
	}


	/*** For the terrain editor we show the brush if in the brush main tab, else we hide it ***/
	if ( mGuiMode == UI_EDITMODE_TERRAINMENU_ID )
	{
		if ( terrainMenuBrushTabMainWindow->isVisible() )
		{
		  terrainMenuBrushTabMain->updateTerrainBrush();
		  terrainManager->showBrush(true);
		}
		else if ( terrainMenuSplatTabWindow->isVisible() )
		{
		  terrainMenuSplatTab->updateTerrainBrush();
		  terrainManager->showBrush(true);
		}
		else {
		  terrainManager->showBrush(false);
		}		 
	}



	/*** Set the brush position ***/
	terrainManager->setBrushPositionToCursor();
	

	/*** If the middle mouse button is down and the flatten terrain key isn't down, then we move the camera ***/
	if (mBmouseMiddleButtonDown)
	{
		if ( !mBkeyRightCntrl )
		{
			const OIS::MouseState& mouseState = guiManager->getInput()->getMouse()->getMouseState();
			const Ogre::Real tmpMoveDegree = mCameraManager->getCameraMoveDegree() * static_cast<Ogre::Real>(0.001);

			const Ogre::Real rotateDegree = mouseState.X.rel * tmpMoveDegree;
			const Ogre::Real pitchDegree = -mouseState.Y.rel * tmpMoveDegree;		


			mCameraManager->cameraSceneNodeYaw(rotateDegree);
			mCameraManager->cameraPitch(pitchDegree);
		}
	}


	return true;
}




// NOTE: Later a class will be made to handle the keys;
void GUI::keyPressed(GuiKeyAutoRepeat* const guiKeyAutoRepeat)
{
	GUIManager* const guiManager = GUIManager::getSingletonPtr();
	EditManager* const editManager = EditManager::getSingletonPtr();


	if ( guiManager->getEnabledCommandKeys() )
	{
		if( guiKeyAutoRepeat->hasKey(OIS::KC_ESCAPE) )
		{
			// Toggle the menubar - but make sure to not do it when special windows are open //
			if ( 	( !mHeapNewMapWindow->isVisible() ) && 
				( !mHeapLoadMapWin->isVisible() ) &&
				( !mHeapSaveWindow->isVisible() ) &&
				( !mHeapQuitWindow->isVisible() ) &&
				( editManager->isMapLoaded() ) )
			{
				mHeapMenuBar->toggle(&mVdataTypes);
			}
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_PGUP) )			// Raise the camera;
		{
			mCameraManager->cameraSceneNodeRaise();
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_PGDOWN) )			// Lower the camera;
		{
			mCameraManager->cameraSceneNodeLower();
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_UP) )				// Scroll the camera forward;
		{
			mCameraManager->cameraSceneNodeMoveForward();
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_DOWN) )			// Scroll the camera backward;
		{
			mCameraManager->cameraSceneNodeMoveBackward();
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_LEFT) )			// Scroll the camera left;
		{
			mCameraManager->cameraSceneNodeMoveLeft();
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_RIGHT) )			// Scroll the camera right;
		{
			mCameraManager->cameraSceneNodeMoveRight();
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_RCONTROL) )
		{
			// Inform us if we pressed the right control key;
			mBkeyRightCntrl = true;
		}
		else if( guiKeyAutoRepeat->hasKey(OIS::KC_L) )
		{
			TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
			terrainManager->updateLightMap();

			#include "TerrainManager.h"
			#include "SplatManager.h"

			//SplatManager* const sm =  terrainManager->getSplatManager();
			//sm->setupNoDynamicTexture();

			//sm->setTextures(string(""), string(""), string("") , string(""));
			//sm->setTextures(string("WeirdEye.png"), string("WeirdEye.png"), string("WeirdEye.png") , string("WeirdEye.png"));
		}

	}
}

void GUI::keyReleased(const OIS::KeyEvent* arg)
{
	if( arg->key == OIS::KC_RCONTROL )		// Did we release the right Control key?;
	{
		mBkeyRightCntrl = false;
	}
}




void GUI::mouseWheelForward()
{
	TerrainMenuBrushTabMain* const terrainMenuBrushTabMain = mTerrainMenu->getTerrainMenuBrushTabMain();
	TerrainMenuSplatTab* const terrainMenuSplatTab = mTerrainMenu->getTerrainMenuSplatTab();
	
	CEGUI::Window* const terrainMenuBrushTabMainWindow = terrainMenuBrushTabMain->getWindow();
	CEGUI::Window* const terrainMenuSplatTabWindow = terrainMenuSplatTab->getWindow();

	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();

///////	GuiKeyAutoRepeat* const guiKeyAutoRepeat = GUIManager::getSingletonPtr()->getInput()->getGuiKeyAutoRepeat();


	if ( mGuiMode == UI_EDITMODE_TERRAINMENU_ID )
	{
		if ( terrainMenuBrushTabMainWindow->isVisible() )
		{
			// If we have the Ctrl key down then change brushes;
			if ( mBkeyRightCntrl )
			{
				terrainMenuBrushTabMain->setBrush(terrainMenuBrushTabMain->getSelectedBrush() - 1);
				terrainManager->showBrush(true);
			}

/****************
			// If we have the Ctrl key down then change brushes;
			if( guiKeyAutoRepeat->hasKey(OIS::KC_RCONTROL) )
			{
				terrainMenuBrushTabMain->setBrush(terrainMenuBrushTabMain->getSelectedBrush() - 1);
				terrainManager->showBrush(true);
			}
****************/
		}
		else if ( terrainMenuSplatTabWindow->isVisible() )
		{
			// If we have the Ctrl key down then change the splat brushes;
			if ( mBkeyRightCntrl )
			{
				terrainMenuSplatTab->setBrush(terrainMenuSplatTab->getSelectedBrush() - 1);
			}
		}
	}
}

void GUI::mouseWheelBackward()
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();

	TerrainMenuBrushTabMain* const terrainMenuBrushTabMain = mTerrainMenu->getTerrainMenuBrushTabMain();
	TerrainMenuSplatTab* const terrainMenuSplatTab = mTerrainMenu->getTerrainMenuSplatTab();
	
	CEGUI::Window* const terrainMenuBrushTabMainWindow = terrainMenuBrushTabMain->getWindow();
	CEGUI::Window* const terrainMenuSplatTabWindow = terrainMenuSplatTab->getWindow();



	if ( mGuiMode == UI_EDITMODE_TERRAINMENU_ID )
	{
		if ( terrainMenuBrushTabMainWindow->isVisible() )
		{
			// If we have the Ctrl key down then change brushes;
			if ( mBkeyRightCntrl )
			{
				terrainMenuBrushTabMain->setBrush(terrainMenuBrushTabMain->getSelectedBrush() + 1);
				terrainManager->showBrush(true);
			}
		}
		else if ( terrainMenuSplatTabWindow->isVisible() )
		{
			// If we have the Ctrl key down then change brushes;
			if ( mBkeyRightCntrl )
			{
				terrainMenuSplatTab->setBrush(terrainMenuSplatTab->getSelectedBrush() + 1);
			}
		}
	}
}

// This function only gets called once on mouse down while the event_sheet_mouseButtonDown() and
// mouseButtonRelease() functions get called multiple times.
// This function will let the event_sheet_mouseButtonDown() function know when to start and the mouseButtonRelease
// function will let us know when to stop;
void GUI::mouseButtonPress(OIS::MouseButtonID* const id)
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();



	// Return if the cursor is over a child window;
	if ( ceguiCursorManager->getCursorOverWidow() )
	{
		return;
	}


	// The left Mouse button increases the terrain, the middle flattens it and the right mouse button decreases it;
	if( *id == OIS::MB_Left )
	{
		// Alert us that this mouse button is down and set the others to false;
		mBmouseLeftButtonDown = true;

		// Turn the other buttons off;
		mBmouseMiddleButtonDown = false;
		mBmouseRightButtonDown = false;
	}
	else if( *id == OIS::MB_Middle )
	{
		// Alert us that this mouse button is down and set the others to false;
		mBmouseMiddleButtonDown = true;

		// Turn the other buttons off;
		mBmouseLeftButtonDown = false;
		mBmouseRightButtonDown = false;


		/* 
		***	For now we set the flatten y positon to be the position when first clicked.
		***	Later we need to find a better way to do this, as in some sort of singleMouseButtonPress function.
		 */
	
		terrainManager->setTerrainFlattenHeightToCursor();	
	}
	else if( *id == OIS::MB_Right )
	{
		// Alert us that this mouse button is down and set the others to false;
		mBmouseRightButtonDown = true;

		// Turn the other buttons off;
		mBmouseLeftButtonDown = false;
		mBmouseMiddleButtonDown = false;
	}
}

void GUI::mouseButtonRelease(OIS::MouseButtonID* const id)
{
	// Let us know which mouse button was released and turn all the mouse states to off/released;
	if( *id == OIS::MB_Left )
	{
		mBmouseLeftButtonDown = false;

		mBmouseMiddleButtonDown = false;
		mBmouseRightButtonDown = false;
	}
	else if( *id == OIS::MB_Middle )
	{
		mBmouseMiddleButtonDown = false;

		mBmouseLeftButtonDown = false;
		mBmouseRightButtonDown = false;
	}
	else if( *id == OIS::MB_Right )
	{
		mBmouseRightButtonDown = false;

		mBmouseLeftButtonDown = false;
		mBmouseMiddleButtonDown = false;
	}
}




void GUI::event_menu_toggleButton(const int menubutton)
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();

	
	switch (menubutton)
	{
		case UI_FILE_QUITWINDOW_ID:
			{
				mHeapQuitWindow->toggle();
			}
			break;
		case UI_EDITMODE_TERRAINMENU_ID:
			{
				// If the window isn't visible then hide all the windows, show this ID window and
				//  set the guimode to this ID. Else hide this window and set the guimode to no ID;
				if ( mTerrainMenu->isVisible() )
				{
					mTerrainMenu->toggle();			// Hide the window;
					mGuiMode = UI_NO_WINDOW_OPEN_ID;	// Set no window open;
					cout << "guimode = none" << endl;
				}
				else
				{
					hideAllEditorWindows();			// Hide all windows;
					mTerrainMenu->toggle();			// Show the window;
					mGuiMode = UI_EDITMODE_TERRAINMENU_ID;	// Set the window mode;
					cout << "guimode = terrain" << endl;

					/*
					 * Set the brush to the first one, when the window is toggled.
					 * This helps because when a new map is loaded the windows get hidden,
					 * the SPT::Terrain gets deleted and we need to reset the brush
					 */
					TerrainMenuBrushTabMain* terrainMenuBrushTabMain = mTerrainMenu->getTerrainMenuBrushTabMain();
					terrainMenuBrushTabMain->setBrush(0);
				}
			}
			break;
		case UI_EDITMODE_MODELMENU_ID:
			{
				// If the window isn't visible then hide all the windows, show this ID window and
				//  set the guimode to this ID. Else hide this window and set the guimode to no ID;
				if ( mModelMenu->isVisible() )
				{
					mModelMenu->toggle();			// Hide the window;
					mGuiMode = UI_NO_WINDOW_OPEN_ID;	// Set no window open;
					cout << "guimode = none" << endl;
				}
				else
				{
					hideAllEditorWindows();			// Hide all windows;
					mModelMenu->toggle();			// Show the window;
					mGuiMode = UI_EDITMODE_MODELMENU_ID;	// Set the window mode;
					cout << "guimode = Model" << endl;
				}
			}
			break;
		case UI_FILE_NEWMAP_ID:
			{
				// Hide all the windows;
				hideAllEditorWindows();

				// Disable the  menubar;
				disableMenubar();

				// Create and show the splash screen;
				createBackgroundImagewindow();


				// If the window is open then we close then reopen it. Else just open it;
				if ( mHeapNewMapWindow->isVisible() )
				{
					mHeapNewMapWindow->toggle();
					mHeapNewMapWindow->toggle();
				}
				else
				{
					// Show the window;
					mHeapNewMapWindow->toggle();
				}


				// We now set the GUI mode;
				mGuiMode = UI_FILE_NEWMAP_ID;
			}
			break;
		case UI_FILE_LOADMAP_ID:
			{
				// Hide all the windows;
				hideAllEditorWindows();

				// Disable the  menubar;
				disableMenubar();

				// Create and show the splash screen;
				createBackgroundImagewindow();

				// If the window is open then we close then reopen it. Else just open it;
				if ( mHeapLoadMapWin->isVisible() )
				{
					mHeapLoadMapWin->toggle();
					mHeapLoadMapWin->toggle();
				}
				else
				{
					// Show the window;
					mHeapLoadMapWin->toggle();
				}

				// We now set the GUI mode;
				mGuiMode = UI_FILE_LOADMAP_ID;
			}
			break;
		case UI_FILE_SAVEMAP_ID:
			{
				// Hide all the windows;
				hideAllEditorWindows();

				// Disable the  menubar;
				disableMenubar();

				// Create and show the splash screen;
				createBackgroundImagewindow();

				// If the window is open then we close then reopen it. Else just open it;
				if ( mHeapSaveWindow->isVisible() )
				{
					mHeapSaveWindow->toggle();
					mHeapSaveWindow->toggle();
				}
				else
				{
					// Show the window;
					mHeapSaveWindow->toggle();
				}

				// We now set the GUI mode;
				mGuiMode = UI_FILE_SAVEMAP_ID;
			}
			break;
	}

	terrainManager->showBrush(false);		// Hide the terrain brush;
}

void GUI::event_NewMapWindowClose(bool success, string mapName, Ogre::Real width, Ogre::Real maxHeight)
{
	// If success == true than it means everything was ok and the window ok button was clicked.
	// Else if its false then there was an error or the cancel button was clicked.
	if (success)
	{
		// Set a new map;
		mEditManager->setNewMap(mapName, width, maxHeight);

		// Enable the menubar;
		enableMenubar();
	}
	else
	{
		// The user clicked the cancel button or an error occurred, and we set the menubar back
		//  to its original settings;
		setStartupEnabledMenus();
	}


	// If the window is open then destroy it;
	if ( mHeapNewMapWindow->isVisible() )
	{
		mHeapNewMapWindow->toggle();
	}


	// Destroy the background image if the map was already loaded;
	if ( mEditManager->isMapLoaded() )
	{
		destroyBackgroundImagewindow();
	}
}

void GUI::event_LoadMapWindowClose(bool success, string mapName, Ogre::Real width, Ogre::Real maxHeight, 
					string &mapHeightData, unsigned long long &mapHeightDataSize)
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	bool checkHeightData = true;


	// If success == true than it means everything was ok and the window ok button was clicked.
	// Else if its false then there was an error or the cancel button was clicked.
	if (success)
	{
		// Set a new map;
		mEditManager->setNewMap(mapName, width, maxHeight);

		checkHeightData = terrainManager->setHeightMapData(mapHeightData, mapHeightDataSize);

		// If there's an error with the heightmap data then show a Error messagebox;
		if (!checkHeightData)
		{
			string title = "ERROR";
			string functionName = "void GUI::event_LoadMapWindowClose(...)";
			string errorMsg = "The Terrain Heightmap Data is corrupted.";
	
			if ( mGuiMessageBox )
			{
				mGuiMessageBox->show( &title, &functionName, &errorMsg, true );
			}
		}

		// Enable the menubar;
		enableMenubar();
	}
	else
	{
		// The user clicked the cancel button or an error occurred, and we set the menubar back
		//  to its original settings;
		setStartupEnabledMenus();
	}


	// If the window is open then we destroy it;
	if ( mHeapLoadMapWin->isVisible() )
	{
		mHeapLoadMapWin->toggle();
	}


	// Destroy the background image if the map was already loaded;
	if ( mEditManager->isMapLoaded() )
	{
		destroyBackgroundImagewindow();
	}
}

void GUI::event_SaveMapWindowClose(bool success)
{
	// If success == true than it means everything was ok and the window ok button was clicked.
	// Else if its false then there was an error or the cancel button was clicked.
	if (success)
	{
		// Enable the menubar;
		enableMenubar();
	}
	else
	{
		// The user clicked the cancel button or an error occurred, and we set the menubar back
		//  to its original settings;
		setStartupEnabledMenus();
	}


	// If the window is open then we destroy it;
	if ( mHeapSaveWindow->isVisible() )
	{
		mHeapSaveWindow->toggle();
	}


	// Destroy the background image if the map was already loaded;
	if ( mEditManager->isMapLoaded() )
	{
		destroyBackgroundImagewindow();
	}
}
