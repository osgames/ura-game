/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TERRAIN_MANAGER_H
#define TERRAIN_MANAGER_H


/**********************/
/*** INCLUDE FILES  ***/
/**********************/
#include <vector>
#include <string>
#include <Ogre.h>

#include <Prerequisites.h>
#include <Terrain.h>
#include <BrushDisplacement.h>
#include <Heightmap.h>

#include "SplatManager.h"



class TerrainManager
{
protected:
	SplatManager* mSplatManager;

	SPT::Terrain* mTerrain;
	Ogre::Real* mMapWidth;
	Ogre::Real* mMapHeight;

	Ogre::Light* mSunLight;
	SPT::BrushPtr mBrush;
	Ogre::Vector2* mBrushSize;
	Ogre::Vector3* mBrushPosition;

	Ogre::Real* mTerrainFlattenHeight;
public:
	TerrainManager();
	~TerrainManager();

	void onFrameStart(unsigned long* const timeSinceLastFrame);
	void onFrameEnd(unsigned long* const timeSinceLastFrame);	

	SPT::Terrain* getTerrain();
	SplatManager* const getSplatManager();

	void setNewTerrain(Ogre::Real width, Ogre::Real maxHeight);
	bool setHeightMapData(std::string &data, unsigned long long &dataSize);
	void updateLightMap();

	bool getTerrainLoaded() const;
	Ogre::Real getWidth() const;
	Ogre::Real getHeight() const;

	void setBrushPositionToCursor();
	void setBrushSize(Ogre::Vector2 size);
	void setBrush(std::string brush_name);

	Ogre::Vector3 getBrushPosition();
	bool getBrushVisible();

	void showBrush(bool show);
	void displaceTerrain(Ogre::Real intensity);
	void setTerrainFlattenHeightToCursor();
	void flattenTerrain();
	void flattenTerrainToBottom();
	void flattenTerrainToCenter();
};


#endif 

