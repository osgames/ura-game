/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CUSTOMSCENENODEMANIPULATOR_H
#define CUSTOMSCENENODEMANIPULATOR_H


/****************/
/*** INCLUDES ***/
/****************/
#include <Ogre.h>
#include <vector>
#include <string>
#include <iostream>
#include <set>



class CustomSceneNodeSelectionTracker
{
protected:
	std::vector<Ogre::SceneNode*> mObjectSelectedList;	// This Stores the all selected sceneNodes;
	std::set<Ogre::SceneNode*> mStdSetSelectedSceneNodes;	// STD set for comparing SceneNodes;	
public:
	CustomSceneNodeSelectionTracker();
	~CustomSceneNodeSelectionTracker();

	void insert(Ogre::SceneNode* const sn);
	void erase(Ogre::SceneNode* const sn);
	void clear();
	bool exist(Ogre::SceneNode* const sn);

	unsigned long getSize();
	bool get(unsigned long index, std::string &sceneNodeName);
};



class CustomSceneNodeManipulator
{
 public:
  enum Mode {
    ENTITY,
    LIGHT,
  };

protected:
  CustomSceneNodeSelectionTracker* mEntityNodeTracker;
  CustomSceneNodeSelectionTracker* mLightNodeTracker;
  CustomSceneNodeSelectionTracker* mCurrentNodeTracker;
  Mode mMode;

public:

	CustomSceneNodeManipulator();
	~CustomSceneNodeManipulator();

	void select(std::vector<Ogre::SceneNode*> *pList);
	void select(Ogre::SceneNode* const sn);
	void deselect();
	void deselect(Ogre::SceneNode* const sn);

	void copySelected();

	void emptySelected();
	void deleteSelected();

	void yaw(Ogre::Real degree);
	void roll(Ogre::Real degree);
	void pitch(Ogre::Real degree);

	void translateX(Ogre::Real degree);
	void translateY(Ogre::Real degree);
	void translateZ(Ogre::Real degree);

	void showBoundingBox(bool visibility);

	void scale(Ogre::Real x, Ogre::Real y, Ogre::Real z);

	void attach(Ogre::SceneNode* const parent);
	void detach();

	void drag();
	
	void setMode(Mode mode);
	unsigned long getSize() const;
	const Ogre::Light* getLight() const;
	const std::vector<Ogre::Light*> getLights() const;
};



#endif




