/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RAYCAST_MANAGER_H
#define RAYCAST_MANAGER_H


#include <vector>
#include <string>
#include <Ogre.h>



class RayCastManager
{
protected:
	Ogre::RaySceneQuery* mRaySceneQuery;


	void _getMeshInformation(const Ogre::MeshPtr mesh,
		size_t &vertex_count,
		Ogre::Vector3* &vertices,
		size_t &index_count,
		unsigned long* &indices,
		const Ogre::Vector3 &position,
		const Ogre::Quaternion &orient,
		const Ogre::Vector3 &scale);
public:
	RayCastManager();
	~RayCastManager();

	bool RaycastFromPoint(const Ogre::Vector3 &point, const Ogre::Vector3 &normal, Ogre::Vector3 &result);
	bool RaycastFromPointGetSceneNodeName(const Ogre::Vector3 &point, const Ogre::Vector3 &normal, Ogre::Vector3 &result, std::string &sceneNodeName);
	bool RaycastFromPointGetLightSceneNodeName(const Ogre::Vector3 &point, const Ogre::Vector3 &normal, Ogre::Vector3 &result, std::string &sceneNodeName);

	bool setSceneNodeAtCursor(Ogre::SceneNode* const sn);

	bool getSceneNodeNameAtCursor(std::string& sceneNodeName);
	bool getLightSceneNodeNameAtCursor(std::string& sceneNodeName);
	bool getPositionAtCursor(Ogre::Vector3& position);
	bool getHeightAtPosition(Ogre::Real x, Ogre::Real z, Ogre::Real &result);
	bool getPositionUnderCameraSceneNode(Ogre::Vector3& position);
};


#endif



