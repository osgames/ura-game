/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef EDIT_MANAGER_H
#define EDIT_MANAGER_H


/**********************/
/*** INCLUDE FILES  ***/
/**********************/
#include <vector>
#include <string>
#include <Ogre.h>

#include <Prerequisites.h>
#include <Terrain.h>
#include <BrushDisplacement.h>
#include <Heightmap.h>

#include "CameraManager.h"
#include "CustomSceneNodeManager.h"
#include "TerrainManager.h"
#include "RayCastManager.h"

/***************/
/*** DEFINES ***/
/***************/


/***********************/
/*** INCLUDE CLASSES ***/
/***********************/
class CDotScene;
class WorldEditor;



class EditManager : public Ogre::Singleton<EditManager>
{
private:
	WorldEditor* mWorldEditor;

	CameraManager* mCameraManager;
	CustomSceneNodeManager* mCustomSceneNodeManager;
	TerrainManager* mTerrainManager;
	RayCastManager* mRayCastManager;
	CDotScene* mDotScene;

	std::string mMapName;
	bool mMapLoaded;
	float mTerrainFlattenHeight;
	Ogre::Light* isLightNode(Ogre::SceneNode* sn);
	void saveLightNode(std::ofstream& myfile, const Ogre::Light* light); 

public:
	EditManager(WorldEditor *worldEditor);
	~EditManager();

	static EditManager* const getSingletonPtr();

	void update(unsigned long* const timeSinceLastFrame);
	void frameStarted(unsigned long* const timeSinceLastFrame);	// Update function used by WorldEditor.cpp;
	void frameEnded(unsigned long* const timeSinceLastFrame);	// Update function used by WorldEditor.cpp;
	void quit() const;


	CameraManager* const getCameraManager() const
	{
		return mCameraManager;
	}
	
	TerrainManager* const getTerrainManager() const
	{
		return mTerrainManager;
	}

	RayCastManager* const getRayCastManager() const
	{
		return mRayCastManager;
	}


	bool isMapLoaded() const;
	void setNewMap(std::string mapName, Ogre::Real width, Ogre::Real maxHeight);
	bool saveMap( std::string filepath, std::string file_name, std::vector<Ogre::SceneNode*>* pList );
	
	bool importMapDotScene( std::string &mapName, Ogre::Real &mapWidth, Ogre::Real &mapHeight,
					std::string &mapHeightData, unsigned long long &mapHeightDataSize,
					std::vector<Ogre::SceneNode*>* pList, std::string file_name );

	void importDotScene( std::vector<Ogre::SceneNode*>* pList, std::string file_name );
	bool exportDotScene( std::string file_name, std::vector<Ogre::SceneNode*>* pList);
};


#endif



