/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUI_H
#define GUI_H



/****************/
/*** INCLUDES ***/
/****************/
#include <OIS.h>
#include <CEGUI.h>
#include <vector>
#include <string>
#include <iostream>

#include <Terrain.h>

#include "EditManager.h"
#include "CameraManager.h"
#include "HeapQuitWindow.h"
#include "TerrainMenu.h"
#include "HeapMenuBar.h"
#include "MenuBarDataType.h"
#include "ModelMenu.h"
#include "CustomCeguiBackgroundImage.h"
#include "GUIMessageBox.h"

#include "HeapMapNewWindow.h"
#include "HeapMapLoadWindow.h"
#include "HeapMapSaveWindow.h"

#include "GuiKeyAutoRepeat.h"


//////////////////#include <DialogBox.h>


/***************/
/*** CLASSES ***/
/***************/
class EditManager;



class GUI
{
protected:
	EditManager* mEditManager;
	CameraManager* mCameraManager;

	GUIMessageBox* mGuiMessageBox;
	CEGUI::Window* mRootBgImageWindow;
/////////////////////DialogBox dialogBox;

	HeapMenuBar* mHeapMenuBar;
	TerrainMenu* mTerrainMenu;
	ModelMenu* mModelMenu;

	HeapQuitWindow* mHeapQuitWindow;
	HeapMapNewWindow* mHeapNewMapWindow;
	HeapMapLoadWindow* mHeapLoadMapWin;
	HeapMapSaveWindow* mHeapSaveWindow;


	CustomCeguiBackgroundImage mCustomBgImage;

	std::vector <MenuBarDataType> mVdataTypes;

	// Determines what mouse buttons are down;
	bool mBmouseLeftButtonDown;
	bool mBmouseMiddleButtonDown;
	bool mBmouseRightButtonDown;

	// Determine if the keyboard right control key is down;
	bool mBkeyRightCntrl;

	// Determines which window is open and hence which mode were in;
	unsigned int mGuiMode;

	// The events;
	bool event_sheet_mouseButtonDown(const CEGUI::EventArgs& pEventArgs);
	bool event_sheet_mouseMove(const CEGUI::EventArgs& pEventArgs);
public:
	GUI();
	~GUI();

	void hideAllEditorWindows();

	void createBackgroundImagewindow();
	void destroyBackgroundImagewindow();

	void setStartupEnabledMenus();
	void enableMenubar();
	void disableMenubar();

	void keyPressed(GuiKeyAutoRepeat* const guiKeyAutoRepeat);
	void keyReleased(const OIS::KeyEvent* arg);
	void mouseWheelForward();
	void mouseWheelBackward();
	void mouseButtonPress(OIS::MouseButtonID* const id);
	void mouseButtonRelease(OIS::MouseButtonID* const id);

	void event_menu_toggleButton(const int menubutton);
	void event_NewMapWindowClose(bool success, std::string mapName, Ogre::Real width, Ogre::Real maxHeight);
	void event_LoadMapWindowClose(bool success, std::string mapName, Ogre::Real width, Ogre::Real maxHeight,
						std::string &mapHeightData, unsigned long long &mapHeightDataSize);
	void event_SaveMapWindowClose(bool success);



	HeapQuitWindow* const getHeapQuitWindow() const
	{
		return mHeapQuitWindow;
	}

	ModelMenu* const getModelMenu() const
	{
		return mModelMenu;
	}

	TerrainMenu* const getTerrainMenu() const
	{
		return mTerrainMenu;
	}	
};


#endif

