/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CustomSceneNodeManager.h"
#include "WorldEditorProperties.h"
#include "Ogre3dManager.h"


#include <boost/lexical_cast.hpp>


using namespace std;
using namespace Ogre;


template<> CustomSceneNodeManager* Ogre::Singleton<CustomSceneNodeManager>::ms_Singleton = 0;

CustomSceneNodeManager* const CustomSceneNodeManager::getSingletonPtr()
{
	return ms_Singleton;
}



CustomSceneNodeManager::CustomSceneNodeManager() : mCustomSceneNodeManipulator(0)
{
	mSceneMgr = Ogre3dManager::getSingletonPtr()->getSceneManager();

	mCustomSceneNodeManipulator = new CustomSceneNodeManipulator;
}

CustomSceneNodeManager::~CustomSceneNodeManager()
{
	if (mCustomSceneNodeManipulator)
	{
		delete mCustomSceneNodeManipulator;
		mCustomSceneNodeManipulator = 0;
	}
}



CustomSceneNodeManipulator* const CustomSceneNodeManager::getCustomSceneNodeManipulator() const
{
	return mCustomSceneNodeManipulator;
}


void CustomSceneNodeManager::dumpNodes(Ogre::Node *n, int level)
{
	for(int i = 0; i < level; i++)
	{
		cout << " ";
	}

	cout << "SceneNode: " << n->getName() << std::endl;

	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	Ogre::MovableObject *m;
	while(object_it.hasMoreElements())
	{
		for(int i = 0; i < level + 2; i++)
		{
			cout << " ";
		}

		m = object_it.getNext();
		cout << m->getMovableType() << ": " << m->getName() << std::endl;
	}
	while(node_it.hasMoreElements())
	{
		dumpNodes(node_it.getNext(), level + 2);
	}
}

void CustomSceneNodeManager::dumpNodes(Ogre::Node *n)
{
	cout << std::endl << "Node Hierarchy:" << std::endl;
	dumpNodes(n, 0);
}




void CustomSceneNodeManager::destroyAllSceneNodesAndMovableObjects()
{
	destroyAllEntities();
	destroyAllSceneNodes();
}

void CustomSceneNodeManager::destroyAllEntities()
{
	Ogre::SceneNode *rootNode = mSceneMgr->getRootSceneNode();

	_destroyAllEntities(rootNode);
}

void CustomSceneNodeManager::_destroyAllEntities(Ogre::Node *n)
{
	Ogre::Entity* ent;
	Ogre::MovableObject *m;

	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	string tmp;

	while(object_it.hasMoreElements())
	{
		m = object_it.getNext();

		// Check if the type is a entity and if it is, then delete it;
		if( m->getMovableType() == "Entity" )
		{
			tmp = m->getName();

			// Our entities start with "ent_" that way we know what is what.
			// Only delete the entities that start with "ent_";
			if ( tmp.compare(0,4,"ent_") == 0 )
			{
				ent = mSceneMgr->getEntity( m->getName() );

				m->getParentSceneNode()->detachObject( m->getName() );
				mSceneMgr->destroyEntity( ent );
			}
		}
	}
	while(node_it.hasMoreElements())
	{
		_destroyAllEntities( node_it.getNext() );
	}
}


void CustomSceneNodeManager::destroyAllSceneNodes()
{
	Ogre::SceneNode *rootNode = mSceneMgr->getRootSceneNode();

	_destroyAllSceneNodes(rootNode);
}

void CustomSceneNodeManager::_destroyAllSceneNodes(Ogre::SceneNode *n)
{

	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();
	string tmp;

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		SceneNode* child = (SceneNode*) node_it.getNext();
		tmp = child->getName();

		// Our Scenenodes start with "sn_" that way we know what is what.
		// Only delete the scenenodes that start with "sn_";
		if ( tmp.compare(0,3,"sn_") == 0 )
		{
			mSceneMgr->getRootSceneNode()->removeAndDestroyChild(tmp);
		}
	}
}




void CustomSceneNodeManager::removeAllSceneNodesAndMovableObjects()
{
	removeAllEntities();
	removeAllSceneNodes();
}

void CustomSceneNodeManager::removeAllEntities()
{
	Ogre::SceneNode *rootNode = mSceneMgr->getRootSceneNode();

	_removeAllEntities(rootNode);
}

void CustomSceneNodeManager::_removeAllEntities(Ogre::Node *n)
{
	Ogre::Entity* ent;
	Ogre::MovableObject *m;

	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	string tmp;

	while(object_it.hasMoreElements())
	{
		m = object_it.getNext();

		// Check if the type is a entity and if it is, then delete it;
		if( m->getMovableType() == "Entity" )
		{
			tmp = m->getName();

			// Our entities start with "ent_" that way we know what is what.
			// Only delete the entities that start with "ent_";
			if ( tmp.compare(0,4,"ent_") == 0 )
			{
				ent = mSceneMgr->getEntity( m->getName() );

				m->getParentSceneNode()->detachObject( m->getName() );
			}
		}
	}
	while(node_it.hasMoreElements())
	{
		_removeAllEntities( node_it.getNext() );
	}
}


void CustomSceneNodeManager::removeAllSceneNodes()
{
	Ogre::SceneNode *rootNode = mSceneMgr->getRootSceneNode();

	_removeAllSceneNodes(rootNode);
}

void CustomSceneNodeManager::_removeAllSceneNodes(Ogre::SceneNode *n)
{
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();
	string tmp;

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		SceneNode* child = (SceneNode*) node_it.getNext();
		tmp = child->getName();

		// Our Scenenodes start with "sn_" that way we know what is what.
		// Only delete the scenenodes that start with "sn_";
		if ( tmp.compare(0,3,"sn_") == 0 )
		{
			child->getParent()->removeChild(child);
		}

		_removeAllSceneNodes( child );
	}
}




void CustomSceneNodeManager::destroyMovableObjectsToBottom(Ogre::Node *n)
{
	Ogre::Entity* ent;
	Ogre::MovableObject *m;

	Ogre::SceneNode::ObjectIterator object_it = ((Ogre::SceneNode *)n)->getAttachedObjectIterator();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	while(object_it.hasMoreElements())
	{
		m = object_it.getNext();

		// Check if the type is a entity and if it is, then delete it;
		if( m->getMovableType() == "Entity" || m->getMovableType() == "Light" )
		{
			ent = mSceneMgr->getEntity( m->getName() );

			m->getParentSceneNode()->detachObject( m->getName() );
			mSceneMgr->destroyEntity( ent );
		}
	}
	while(node_it.hasMoreElements())
	{
		destroyMovableObjectsToBottom( node_it.getNext() );
	}
}

void CustomSceneNodeManager::removeAllSceneNodeToBottom(Ogre::SceneNode *n)
{
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	// First do the main scene nodes and on the bottom while loop do the sub nodes;
	n->getParent()->removeChild(n);

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		SceneNode* child = (SceneNode*) node_it.getNext();
		child->getParent()->removeChild(child);

		removeAllSceneNodeToBottom( child );
	}
}

void CustomSceneNodeManager::removeAllSceneNodeToBottom_putToRoot(Ogre::SceneNode *n)
{
	Ogre::SceneNode *rootNode = mSceneMgr->getRootSceneNode();
	Ogre::Vector3 newPosition = n->_getDerivedPosition();
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();
	SceneNode* child;

	// First do the main scene nodes and on the bottom while loop do the sub nodes;
	n->getParent()->removeChild(n);
	rootNode->addChild(n);
	n->setPosition(newPosition);

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		child = (SceneNode*) node_it.getNext();

		newPosition = child->_getDerivedPosition();
		child->getParent()->removeChild(child);
		rootNode->addChild(child);
		child->setPosition(newPosition);

		removeAllSceneNodeToBottom_putToRoot( child );
	}
}

void CustomSceneNodeManager::removeAllSceneNodeToBottom(Ogre::SceneNode *n, Ogre::SceneNode *removeToNode)
{
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();
	Ogre::Vector3 newPosition = n->_getDerivedPosition();
	SceneNode* child;

	// First do the main scene nodes and on the bottom while loop do the sub nodes;
	n->getParent()->removeChild(n);
	removeToNode->addChild(n);
	n->setPosition(newPosition);

	// Handle the subnodes;
	while(node_it.hasMoreElements())
	{
		child = (SceneNode*) node_it.getNext();

		newPosition = child->_getDerivedPosition();
		child->getParent()->removeChild(child);
		removeToNode->addChild(child);
		child->setPosition(newPosition);

		removeAllSceneNodeToBottom( child, removeToNode );
	}
}


void CustomSceneNodeManager::showBoundingBoxToBottom(Ogre::SceneNode *n, bool show)
{
	Ogre::Node::ChildNodeIterator node_it = n->getChildIterator();

	n->showBoundingBox(show);

	while(node_it.hasMoreElements())
	{
		SceneNode* child = (SceneNode*) node_it.getNext();

		child->showBoundingBox(show);

		showBoundingBoxToBottom( child, show );
	}
}

bool CustomSceneNodeManager::isAboveSceneNode(Ogre::SceneNode *parentSceneNode, Ogre::SceneNode *childSceneNode)
{
   Ogre::SceneNode* rootSceneNode = mSceneMgr->getRootSceneNode();


	if ( childSceneNode != rootSceneNode && childSceneNode != parentSceneNode )
	{
	    do
		{
            childSceneNode = childSceneNode->getParentSceneNode();

            // We only continue the loop if 'n' is good;
            if (childSceneNode)
            {
                // The child scene node hasn't hit root yet so its above the parentSceneNode;
                if ( childSceneNode == parentSceneNode )
                {
                    return true;
                }
            }
            else
            {
                // If we got there than an error occurred trying to get the parentSceneNode;
                return false;
            }
		}  while (childSceneNode != rootSceneNode );
	}


	return false;
}

unsigned int CustomSceneNodeManager::getNumJumpsToRootSceneNode(Ogre::SceneNode *n)
{
	unsigned int counter = 0;
    Ogre::SceneNode* rootSceneNode = mSceneMgr->getRootSceneNode();


    while ( n != rootSceneNode )
    {
        n = n->getParentSceneNode();

        // We only continue the loop if 'n' is good;
        if (n)
        {
            counter++;
        }
        else
        {
            // An error occurred so return 0;
            return 0;
        }
    }

    // Successfully return the number of jumps;
    return counter;
}

Ogre::SceneNode* CustomSceneNodeManager::getTopSceneNode(Ogre::SceneNode *n)
{
    Ogre::SceneNode* rootSceneNode = mSceneMgr->getRootSceneNode();
    Ogre::SceneNode* tmp = NULL;


	if ( n == rootSceneNode )
	{
		return n;
	}
	else
    {
		do
		{
            tmp = n;

            n = n->getParentSceneNode();

            // We only continue the loop if 'n' is good;
            if (!n)
            {
                return NULL;
            }
		} while ( n != rootSceneNode );

		n = tmp;
		return n;
    }


	// If we got here than there was an error;
	return NULL;
}

void CustomSceneNodeManager::showBoundingBoxTopToBottom(Ogre::SceneNode *n, bool show)
{
	Ogre::SceneNode* tmp = getTopSceneNode(n);

    if (tmp != NULL)
    {
        showBoundingBoxToBottom(tmp, show);
    }
    else
    {
        cout << "ERROR: void CustomSceneNodeManager::showBoundingBoxTopToBottom(...): "
                << "tmp == NULL." << endl;
    }
}

bool CustomSceneNodeManager::isRootSceneNode(Ogre::SceneNode *n)
{
	if ( n->getName() == "root node" )
	{
		return true;
	}

	return false;
}

string CustomSceneNodeManager::getUniqueSceneNodeName()
{
  string name;
  string endOfName;
  
  unsigned long long counter = 0;
  
  
  do
    {
      name = DEF_PARSE_SCENENODE_NAME_START_STRING; 

      endOfName = boost::lexical_cast<std::string>(counter);
      
      name += endOfName;
      
      ++counter;
    }
  while ( mSceneMgr->hasSceneNode(name) );
  
  return name;
}

string CustomSceneNodeManager::getUniqueEntityName()
{
	string name;
	string endOfName;

	unsigned long long counter = 0;


	do
	{
		name = DEF_PARSE_ENTITY_NAME_START_STRING;
		endOfName = boost::lexical_cast<std::string>(counter);

		name += endOfName;

		++counter;
	}
	while ( mSceneMgr->hasEntity(name) );


	return name;
}

string CustomSceneNodeManager::getUniqueLightName()
{
	string name;
	string endOfName;

	unsigned long long counter = 0;


	do
	{
		name = DEF_PARSE_LIGHT_NAME_START_STRING;
		endOfName = boost::lexical_cast<std::string>(counter);

		name += endOfName;

		++counter;
	}
	while ( mSceneMgr->hasLight(name) );


	return name;
}


bool CustomSceneNodeManager::getSceneNodeMovableObject(Ogre::SceneNode* const sn, string &objectName, const char* type)
{
	Ogre::MovableObject* movableObj = NULL;


	for(unsigned long i = 0; i < sn->numAttachedObjects(); i++ )
	{
		movableObj = sn->getAttachedObject(i);

		if( movableObj->getMovableType() == type );
		{
			objectName = movableObj->getName();
			return true;
		}
	}

	
	return false;
}

bool CustomSceneNodeManager::getSceneNodeEntity(Ogre::SceneNode* const sn, string &entName)
{
  return getSceneNodeMovableObject(sn, entName, "Entity");
}

bool CustomSceneNodeManager::getSceneNodeLight(Ogre::SceneNode* const sn, string &lightName)
{
  return getSceneNodeMovableObject(sn, lightName, "Light");
}

//Scale, position, roll, pitch, rotation;
//tmpSceneNode->roll( Ogre::Degree(sn->getRoll().valueDegrees()) );	
//tmpSceneNode->pitch( Ogre::Degree(sn->getPitch().valueDegrees()) );	


bool CustomSceneNodeManager::copySceneNode(Ogre::SceneNode* const sn, std::string &copiedSceneNodeName)
{
	Ogre::SceneNode* newSceneNode = 0;

	Ogre::Entity* oldEnt = 0;
	Ogre::Entity* newEnt = 0;

	string tmpSceneNodeName;
	string tmpEntityName;
	string tmpEntityMeshFileName;



	if ( CustomSceneNodeManager::getFreeAutoNameSceneNode(tmpSceneNodeName) )
	{
		// Get the New SceneNode;
		newSceneNode = mSceneMgr->getSceneNode(tmpSceneNodeName);

		// Get the old Scennodes Entity, its mesh and attach copies to the new scenenode copy;
		if ( CustomSceneNodeManager::getSceneNodeEntity(sn, tmpEntityName) )
		{
			// Get the old Scene Node and its Mesh File Name;
			oldEnt = mSceneMgr->getEntity(tmpEntityName);
			tmpEntityMeshFileName = oldEnt->getMesh()->getName();	
	

			// Create the new entity, Attach this entity to the sceneNode and set its position and orientation;
			if ( CustomSceneNodeManager::getFreeAutoNameEntity(tmpEntityName, tmpEntityMeshFileName) )
			{
				// Get the new sceneNode and attach the new entity to it;
				newEnt = mSceneMgr->getEntity(tmpEntityName);
				newSceneNode->attachObject(newEnt);
	
				// Set the new Scenenodes scale, pos and orientation;
				newSceneNode->scale( sn->getScale() );
				newSceneNode->setPosition( sn->_getDerivedPosition() );		
				newSceneNode->setOrientation( sn->_getDerivedOrientation() );

				copiedSceneNodeName = newSceneNode->getName();

				return true;
			}		
		}	
	}
	

	return false;
}


bool CustomSceneNodeManager::getFreeAutoNameSceneNode(string &name)
{
	name = CustomSceneNodeManager::getUniqueSceneNodeName();


	try
	{
		mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
	}
	catch(...)
	{
		return false;
	}


	return true;
}

bool CustomSceneNodeManager::getFreeAutoNameEntity(string &name, string meshName)
{
	name = CustomSceneNodeManager::getUniqueEntityName();


	try
	{
		mSceneMgr->createEntity(name, meshName);	
	}
	catch(...)
	{
		return false;
	}


	return true;
}

bool CustomSceneNodeManager::getFreeAutoNameLight(string &lightName, string &lightMeshName)
{
	lightName = CustomSceneNodeManager::getUniqueLightName();
	lightMeshName = DEF_PARSE_LIGHT_MESH_NAME_START_STRING + lightName;

	try
	{
		mSceneMgr->createLight(lightName);	
		std::cout << "Light Name: " << lightName << std::endl;
		mSceneMgr->createEntity(lightMeshName, LIGHTING_MESH_NAME);
	}
	catch(...)
	{
		return false;
	}


	return true;
}

bool CustomSceneNodeManager::getFreeSceneNode(string &name) const
{
	string tmpName = DEF_PARSE_SCENENODE_NAME_START_STRING + name;

	try
	{
		mSceneMgr->getRootSceneNode()->createChildSceneNode(tmpName);
		
		name = tmpName;
	}
	catch(...)
	{
		return false;
	}


	return true;
}

bool CustomSceneNodeManager::getFreeEntity(string &name, string meshName) const
{
	string tmpName = DEF_PARSE_ENTITY_NAME_START_STRING + name;

	try
	{
		mSceneMgr->createEntity(tmpName, meshName);	

		name = tmpName;		
	}
	catch(...)
	{
		return false;
	}


	return true;
}

bool CustomSceneNodeManager::getFreeLight(string &lightName, string &lightMeshName) const
{
	string tmpName = DEF_PARSE_LIGHT_NAME_START_STRING + lightName;
	lightMeshName = DEF_PARSE_LIGHT_MESH_NAME_START_STRING + lightName;

	try
	{
		mSceneMgr->createLight(tmpName);	
		mSceneMgr->createEntity(lightMeshName, LIGHTING_MESH_NAME);

		lightName = tmpName;		
	}
	catch(...)
	{
		return false;
	}


	return true;
}

bool CustomSceneNodeManager::attachEntityToSceneNode(const std::string sceneNodeName, const std::string entityName) const
{
	Ogre::SceneNode* sn = 0;
	Ogre::Entity* ent = 0;


	try
	{
		sn = mSceneMgr->getSceneNode(sceneNodeName);
		ent = mSceneMgr->getEntity(entityName);			
	}
	catch(...)
	{
		return false;
	}

	return attachEntityToSceneNode(sn, ent);
}

bool CustomSceneNodeManager::attachEntityToSceneNode(Ogre::SceneNode* const sn, Ogre::Entity* const ent) const
{
  return attachObjectToSceneNode(sn, ent);
}

bool CustomSceneNodeManager::attachLightToSceneNode(const std::string sceneNodeName, const std::string lightName, const std::string lightMeshName) const
{
	Ogre::SceneNode* sn = 0;
	Ogre::Light* light = 0;
	Ogre::Entity* lightMesh = 0;


	try
	{
		sn = mSceneMgr->getSceneNode(sceneNodeName);
		light = mSceneMgr->getLight(lightName);			
		lightMesh = mSceneMgr->getEntity(lightMeshName);
	}
	catch(...)
	{
		return false;
	}

	return attachLightToSceneNode(sn, light) && attachEntityToSceneNode(sn, lightMesh);
}

bool CustomSceneNodeManager::attachLightToSceneNode(Ogre::SceneNode* const sn, Ogre::Light* const light) const
{
  return attachObjectToSceneNode(sn, light);
}

bool CustomSceneNodeManager::attachObjectToSceneNode(Ogre::SceneNode* const sn, Ogre::MovableObject* const object) const
{
	try
	{
		sn->attachObject(object);
	}
	catch(...)
	{
		return false;
	}

	
	return true;
}

