/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/********************/
/*** THE INCLUDES ***/
/********************/
#include "EditManager.h"

#include <iostream>
#include <fstream>

// SPT headers;
#include <RectFlatten.h>
#include <RectFlattenToCenter.h>

#include "WorldEditor.h"
#include "WorldEditorProperties.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "DotScene.h"
#include "SplatManager.h"


/*******************/
/*** THE DEFINES ***/
/*******************/
#define DEF_GAME_EDITOR_MODE


using namespace std;
using namespace Ogre;



template<> EditManager* Ogre::Singleton<EditManager>::ms_Singleton = 0;

Ogre::Light* EditManager::isLightNode(Ogre::SceneNode* sn)
{
  // returns the light Node if one is found, otherwise null;
 for (unsigned long i = 0; i < sn->numAttachedObjects(); ++i ) {
    if (sn->getAttachedObject(i)->getMovableType() == "Light")
      return (Ogre::Light*) sn->getAttachedObject(i);
  }
 return NULL;
}

void EditManager::saveLightNode(std::ofstream& myfile, const Ogre::Light* light) {
  //Saves the light node in the appropriate xml format.
  myfile << "        <light name=\"";
  myfile << light->getName();
  myfile << "\" type=\"point\" castShadows=\"";
  myfile << Ogre::StringConverter::toString(light->getCastShadows());
  myfile << "\" visible=\"";
  myfile << Ogre::StringConverter::toString(light->isVisible());
  myfile << "\">\n";
  Ogre::ColourValue diffuse = light->getDiffuseColour();
  myfile << "          <colourDiffuse r=\"";
  myfile << Ogre::StringConverter::toString(diffuse.r);
  myfile << "\" g=\"";
  myfile << Ogre::StringConverter::toString(diffuse.g);
  myfile << "\" b=\"";
  myfile << Ogre::StringConverter::toString(diffuse.b);
  myfile << "\" />\n";
  Ogre::ColourValue specular = light->getSpecularColour();
  myfile << "          <colourSpecular r=\"";
  myfile << Ogre::StringConverter::toString(specular.r);
  myfile << "\" g=\"";
  myfile << Ogre::StringConverter::toString(specular.g);
  myfile << "\" b=\"";
  myfile << Ogre::StringConverter::toString(specular.b);
  myfile << "\" />\n";
  myfile << "          <lightAttenuation range=\"";
  myfile << Ogre::StringConverter::toString(light->getAttenuationRange());
  myfile << "\" constant=\"";
  myfile << Ogre::StringConverter::toString(light->getAttenuationConstant());
  myfile << "\" linear=\"";
  myfile << Ogre::StringConverter::toString(light->getAttenuationLinear());
  myfile << "\" quadratic=\"";
  myfile << Ogre::StringConverter::toString(light->getAttenuationQuadric());
  myfile << "\" />\n";
  Ogre::Vector3 position = light->getPosition();
  myfile << "          <position x=\"";
  myfile << Ogre::StringConverter::toString(position.x);
  myfile << "\" y=\"";
  myfile << Ogre::StringConverter::toString(position.y);
  myfile << "\" z=\"";
  myfile << Ogre::StringConverter::toString(position.z);
  myfile << "\" />\n";
  myfile << "        </light>\n";
}
  
  

  

EditManager* const EditManager::getSingletonPtr(void)
{
	return ms_Singleton;
}



EditManager::EditManager( WorldEditor *worldEditor ) : mWorldEditor(worldEditor),
	mCameraManager(NULL), mCustomSceneNodeManager(NULL), mTerrainManager(NULL), mRayCastManager(NULL), mDotScene(NULL),
	mMapLoaded(false)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "EditManager::EditManager(...)";
	string strErrorWordName;


	// Allocate memory for the objects;
	mCameraManager = new CameraManager;
	mCustomSceneNodeManager = new CustomSceneNodeManager;
	mTerrainManager = new TerrainManager;
	mRayCastManager = new RayCastManager;
	mDotScene = new CDotScene;


	// Set the terrain Flatten height;
	mTerrainFlattenHeight = 0.0f;



	/*** Set the Camera and its sceneNodes default movement degree rate ***/
	mCameraManager->setCameraSceneNodeMoveDegree(50);
	mCameraManager->setCameraMoveDegree(50);


	/*** Check for memory allocation error ***/
	if (!mCameraManager)
	{
		strErrorWordName = "mCameraManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mCustomSceneNodeManager)
	{
		strErrorWordName = "mCustomSceneNodeManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mRayCastManager)
	{
		strErrorWordName = "mRayCastManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mTerrainManager)
	{
		strErrorWordName = "mTerrainManager";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mDotScene)
	{
		strErrorWordName = "mDotScene";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}
}

EditManager::~EditManager()
{
	if (mDotScene)
	{
		delete mDotScene;
		mDotScene = NULL;
	}

	if (mRayCastManager)
	{
		delete mRayCastManager;
		mRayCastManager = NULL;
	}

	if (mTerrainManager)
	{
		delete mTerrainManager;
		mTerrainManager = NULL;
	}

	if (mCameraManager)
	{
		delete mCameraManager;
		mCameraManager = NULL;
	}

	if (mCustomSceneNodeManager)
	{
		delete mCustomSceneNodeManager;
		mCustomSceneNodeManager = NULL;
	}
}

void EditManager::quit() const
{
	mWorldEditor->end();
}


void EditManager::update(unsigned long* const timeSinceLastFrame)
{
	mCameraManager->update(timeSinceLastFrame);
}


void EditManager::frameStarted(unsigned long* const timeSinceLastFrame)
{
	if (mTerrainManager)
	{
		mTerrainManager->onFrameStart(timeSinceLastFrame);
	}
}

void EditManager::frameEnded(unsigned long* const timeSinceLastFrame)
{
	if (mTerrainManager)
	{
		mTerrainManager->onFrameEnd(timeSinceLastFrame);
	}

	if (mCameraManager)
	{
		EditManager::update(timeSinceLastFrame);
	}
}

bool EditManager::isMapLoaded() const
{
	return mMapLoaded;
}

void EditManager::setNewMap(string mapName, Ogre::Real width, Ogre::Real maxHeight)
{
	CameraManager* const cameraManager = EditManager::getSingletonPtr()->getCameraManager();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();

	Ogre::Vector3 camSceneNodePosition;



	/*** Set a new Terrain ***/
	mTerrainManager->setNewTerrain(width, maxHeight);


	/*** Reset the Camera and its SceneNode to look at the default position, which is down the positive z  Axis ***/
	cameraManager->resetDefaultPosition();


	/*	We want to get the Y collision height at the position were at.
	***	And to set the Y collision height + 200, to our Y position.
	 */
	if ( mRayCastManager->getPositionUnderCameraSceneNode(camSceneNodePosition) )
	{
		camSceneNodePosition.y += 200;
		cameraSceneNode->setPosition(camSceneNodePosition);
	}


	/*** Set the new map name ***/
	mMapName = mapName;


	/*** Let us know the map was loaded ***/
	mMapLoaded = true;
}

bool EditManager::importMapDotScene( std::string &mapName, Ogre::Real &mapWidth, Ogre::Real &mapHeight,
	std::string &mapHeightData, unsigned long long &mapHeightDataSize, std::vector<Ogre::SceneNode*> *pList, std::string file_name )
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();


	return	mDotScene->parseMapDotScene( mapName, mapWidth, mapHeight, mapHeightData, mapHeightDataSize,
						pList, file_name, "General", sceneManager );	
}

bool EditManager::saveMap( std::string filepath, std::string file_name, std::vector<Ogre::SceneNode*>* pList)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::StringConverter strConverter;

	// Variables for Ogre Scene nodes access and such;
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::MovableObject* movableObj = NULL;
	Ogre::Entity* ent = NULL;

	SPT::Terrain* const terrain = mTerrainManager->getTerrain();
	Ogre::Real terrainWidth = mTerrainManager->getWidth();
	Ogre::Real terrainHeight = mTerrainManager->getHeight();

	// Variables for the terrain heightmap data;
	SPT::Heightmap* hMap = terrain->getHeightmap();
	SPT::HEIGHTMAPTYPE* pHeightMap = hMap->getData();
	unsigned long long dataSize = hMap->getWidth() * hMap->getWidth();
	string heightMapDataStr;
	unsigned long long heightMapDataStrSize = 0;
	Ogre::Real tmpVal = 0.0;
	string::iterator it;

	SplatManager* const splatManager = mTerrainManager->getSplatManager();
	vector<string>* const vSplatTextureNames = splatManager->getTextures();
	const string splatmapFilename = file_name + SPLATMAP_UNIQUENAME_ENDPART;
	const string splatmapPathandFilename = FILEPATH_OF_SPLATMAPS + splatmapFilename;

	//Store the splat and detail Scales;
	const Ogre::Vector4 splatScales = splatManager->getSplatScales();
	const Ogre::Vector4 detailScales = splatManager->getDetailScales();

	// Open the file to write to;
	const string mapPathAndFilename = filepath + file_name;
	std::ofstream myfile( mapPathAndFilename.c_str() );



	/////////////////////////////////////////////////////////////////
	//// Get the terrain heightmap data and store it in a string ////
	/////////////////////////////////////////////////////////////////

	for (unsigned long long i = 0; i < dataSize; i++)
	{
		tmpVal = (Ogre::Real) pHeightMap[i];

		heightMapDataStr += strConverter.toString(tmpVal);

		heightMapDataStr += ",";
	}

	// Remove the comma at the end;
	it = heightMapDataStr.end() - 1;
	heightMapDataStr.erase(it);

	// Store the heightmap string Size;
	heightMapDataStrSize = heightMapDataStr.size();


	///////////////////////////////////////////
	//// Save the Terrain SplatMap texture ////
	///////////////////////////////////////////
	splatManager->saveTexture(splatmapPathandFilename);


	////////////////////////////////////////
	//// Write the data to the map file ////
	////////////////////////////////////////

	if( myfile.is_open() )
	{
		myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>" << std::endl;
            	myfile << "<scene formatVersion=\"1.0.0\">" << std::endl;


		//////////////////////////
		//// Handle the nodes ////
		//////////////////////////
		myfile << "  <nodes>" << std::endl;


		for( unsigned long indx = 0; indx < pList->size(); indx++ )
		{
		  std::cout << "Finished Saving." << std::endl;

			// Name
		        myfile << "    <node name=\"";
		        myfile << (*pList)[indx]->getName();
		        myfile << "\">" << std::endl;

			// Position
		        myfile << "      <position x=\"";
		        myfile << (*pList)[indx]->getPosition().x;
		        myfile << "\" y=\"";
		        myfile << (*pList)[indx]->getPosition().y;
		        myfile << "\" z=\"";
		        myfile << (*pList)[indx]->getPosition().z;
		        myfile << "\" />" << std::endl;

			// Rotation
		        myfile << "      <rotation qx=\"";
		        myfile << (*pList)[indx]->getOrientation().x;
		        myfile << "\" qy=\"";
		        myfile << (*pList)[indx]->getOrientation().y;
		        myfile << "\" qz=\"";
		        myfile << (*pList)[indx]->getOrientation().z;
		        myfile << "\" qw=\"";
		        myfile << (*pList)[indx]->getOrientation().w;
		        myfile << "\" />" << std::endl;

			// Roll
			myfile << "      <roll degree=\"";
		        myfile << (*pList)[indx]->_getDerivedOrientation().getRoll().valueDegrees();
		        myfile << "\" />" << std::endl;

			// Pitch
			myfile << "      <pitch degree=\"";
		        myfile << (*pList)[indx]->_getDerivedOrientation().getPitch().valueDegrees();
		        myfile << "\" />" << std::endl;

			// Scale
		        myfile << "      <scale x=\"";
		        myfile << (*pList)[indx]->getScale().x;
		        myfile << "\" y=\"";
		        myfile << (*pList)[indx]->getScale().y;
		        myfile << "\" z=\"";
		        myfile << (*pList)[indx]->getScale().z;
		        myfile << "\" />" << std::endl;

			Ogre::Light* light = this->isLightNode((*pList)[indx]);
			if (light) {
			  saveLightNode(myfile, light);
			} else {
			  for( unsigned long i = 0; i < (*pList)[indx]->numAttachedObjects(); i++ )
			    {
			      
			      movableObj = (*pList)[indx]->getAttachedObject(i);
			      
			      if( movableObj->getMovableType() == "Entity" )
				{
				  ent = sceneManager->getEntity( movableObj->getName() );
				  
				  myfile << "      <entity name=\"";
				  myfile << movableObj->getName();
				  myfile << "\" meshFile=\"";
				  myfile << ent->getMesh()->getName();
				  myfile << "\" />" << std::endl;
				}
			    }
			}
		        myfile << "    </node>" << std::endl;
		}

		myfile << "  </nodes>" << std::endl;


		/////////////////////////////////////////
		//// Handle the map and terrain data ////
		/////////////////////////////////////////
		myfile  << "  <terrains>" << std::endl;
		myfile 	<< "    <terrain name=\"" << mMapName << "\">" << std::endl;
		myfile 	<< "      <width w=\"" << terrainWidth << "\" />" << std::endl;
		myfile 	<< "      <maxheight mh=\"" << terrainHeight << "\" />" << std::endl;
		myfile 	<< "      <heightMapData data=\"" << heightMapDataStr << "\" />" << std::endl;
		myfile 	<< "      <heightMapDataSize size=\"" << heightMapDataStrSize << "\" />" << std::endl;
		myfile 	<< "      <splatMapFileName fileName=\"" << splatmapFilename  << "\" />" << std::endl;

		// Set the 4 Splat textures if they exist;
		if ( splatManager->getTextureCount() == 4 )
		{
		myfile 	<< "      <splatTexture1 fileName=\"" << vSplatTextureNames->at(0) << "\" />" << std::endl;
		myfile 	<< "      <splatTexture2 fileName=\"" << vSplatTextureNames->at(1)  << "\" />" << std::endl;
		myfile 	<< "      <splatTexture3 fileName=\"" << vSplatTextureNames->at(2)  << "\" />" << std::endl;
		myfile 	<< "      <splatTexture4 fileName=\"" << vSplatTextureNames->at(3)  << "\" />" << std::endl;
		}

		// Set the splat and detail scales;
		myfile 	<< "      <splatRegularScales1 RealVal=\"" << splatScales.x  << "\" />" << std::endl;
		myfile 	<< "      <splatRegularScales2 RealVal=\"" << splatScales.y  << "\" />" << std::endl;
		myfile 	<< "      <splatRegularScales3 RealVal=\"" << splatScales.z  << "\" />" << std::endl;
		myfile 	<< "      <splatRegularScales4 RealVal=\"" << splatScales.w  << "\" />" << std::endl;
		myfile 	<< "      <splatDetailScales1 RealVal=\"" << detailScales.x  << "\" />" << std::endl;
		myfile 	<< "      <splatDetailScales2 RealVal=\"" << detailScales.y  << "\" />" << std::endl;


		myfile 	<< "    </terrain>" << std::endl;
		myfile 	<< "  </terrains>" << std::endl;


		/////////////////////////
		//// Close the scene ////
		/////////////////////////
            	myfile << "</scene>" << std::endl;


		////////////////////////
		//// Close the file ////
		////////////////////////
		myfile.close();


		return true;
	}
	else
	{
		cout << "ERROR: bool EditManager::saveMap(...):  Couldn't open file for writting." << endl;

		return false;
	}

	
	return false;
}




void EditManager::importDotScene( std::vector<Ogre::SceneNode*> *pList, std::string file_name )
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();


	mDotScene->parseDotScene(pList, file_name, "General", sceneManager);
}

bool EditManager::exportDotScene( std::string file_name, std::vector<Ogre::SceneNode*>* pList)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre::MovableObject* movableObj = NULL;
	Ogre::Entity* ent = NULL;
	unsigned int i = 0;

	std::ofstream myfile( file_name.c_str() );
	if( myfile.is_open() )
	{
		myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>" << std::endl;
            	myfile << "<scene formatVersion=\"1.0.0\">" << std::endl;
		myfile << "  <nodes>" << std::endl;


		for( unsigned int indx = 0; indx < pList->size(); indx++ )
		{
			// Name
		        myfile << "    <node name=\"";
		        myfile << (*pList)[indx]->getName();
		        myfile << "\">" << std::endl;

			// Position
		        myfile << "      <position x=\"";
		        myfile << (*pList)[indx]->getPosition().x;
		        myfile << "\" y=\"";
		        myfile << (*pList)[indx]->getPosition().y;
		        myfile << "\" z=\"";
		        myfile << (*pList)[indx]->getPosition().z;
		        myfile << "\" />" << std::endl;

			// Rotation
		        myfile << "      <rotation qx=\"";
		        myfile << (*pList)[indx]->getOrientation().x;
		        myfile << "\" qy=\"";
		        myfile << (*pList)[indx]->getOrientation().y;
		        myfile << "\" qz=\"";
		        myfile << (*pList)[indx]->getOrientation().z;
		        myfile << "\" qw=\"";
		        myfile << (*pList)[indx]->getOrientation().w;
		        myfile << "\" />" << std::endl;

			// Roll
			myfile << "      <roll degree=\"";
		        myfile << (*pList)[indx]->_getDerivedOrientation().getRoll().valueDegrees();
		        myfile << "\" />" << std::endl;

			// Pitch
			myfile << "      <pitch degree=\"";
		        myfile << (*pList)[indx]->_getDerivedOrientation().getPitch().valueDegrees();
		        myfile << "\" />" << std::endl;

			// Scale
		        myfile << "      <scale x=\"";
		        myfile << (*pList)[indx]->getScale().x;
		        myfile << "\" y=\"";
		        myfile << (*pList)[indx]->getScale().y;
		        myfile << "\" z=\"";
		        myfile << (*pList)[indx]->getScale().z;
		        myfile << "\" />" << std::endl;

			for( i = 0; i < (*pList)[indx]->numAttachedObjects(); i++ )
			{
				movableObj = (*pList)[indx]->getAttachedObject(i);

				if( movableObj->getMovableType() == "Entity" );
				{
					ent = sceneManager->getEntity( movableObj->getName() );

				        myfile << "      <entity name=\"";
				        myfile << movableObj->getName();
				        myfile << "\" meshFile=\"";
				        myfile << ent->getMesh()->getName();
				        myfile << "\" />" << std::endl;
				}
			}

		        myfile << "    </node>" << std::endl;
		}

		myfile << "  </nodes>" << std::endl;
            	myfile << "</scene>" << std::endl;

		myfile.close();
		return true;
	}
	else
	{
		return false;
	}


	return false;
}


