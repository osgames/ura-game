/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "SplatManager.h"
#include "EditManager.h"
#include "TerrainManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "WorldEditorProperties.h"


using namespace std;
using namespace Ogre;



SplatManager::SplatManager() : mSplatMapTexture(NULL)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "SplatManager::SplatManager()";
	string strErrorWordName;


	/*** Allocate our class members dynamic memory ***/
	mDynamicTextureName = new string;
	mMaterialName = new string;

	mBufferHeight = new unsigned long;
	mBufferWidth = new unsigned long;

	mTerrainLengthWidth = new Ogre::Real;

	mVbrushNames = new vector<string>;
	mCurrentBrushName = new string;

	mVtextureNames = new vector<string>;
	mCurrentTextureName = new string;

	mSplatMapFileName = new string;

	mSplatScales = new Ogre::Vector4(0,0,0,0);
	mDetailScales = new Ogre::Vector4(0,0,0,0);



	/*** Check for memory allocation error ***/
	if (!mDynamicTextureName)
	{
		strErrorWordName = "mDynamicTextureName";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mMaterialName)
	{
		strErrorWordName = "mMaterialName";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mBufferHeight)
	{
		strErrorWordName = "mBufferHeight";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mBufferWidth)
	{
		strErrorWordName = "mBufferWidth";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mTerrainLengthWidth)
	{
		strErrorWordName = "mTerrainLengthWidth";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mVbrushNames)
	{
		strErrorWordName = "mVbrushNames";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mCurrentBrushName)
	{
		strErrorWordName = "mCurrentBrushName";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mVtextureNames)
	{
		strErrorWordName = "mVtextureNames";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mCurrentTextureName)
	{
		strErrorWordName = "mCurrentTextureName";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mSplatMapFileName)
	{
		strErrorWordName = "mSplatMapFileName";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mSplatScales)
	{
		strErrorWordName = "mSplatScales";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mDetailScales)
	{
		strErrorWordName = "mDetailScales";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	/*** Set the default splatmap filename ***/
	*mSplatMapFileName = SPLATMAP_DEFAULT_FILENAME;


	/*** Set the default splatmap material name ***/
	*mMaterialName = SPLATMAP_DEFAULT_MATERIALNAME;


	/*** Set the default splat and detail scales ***/
	mSplatScales->x = 300;
	mSplatScales->y = 400;
	mSplatScales->z = 220;
	mSplatScales->w = 150;

	mDetailScales->x = 1000;
	mDetailScales->y = 10000;
	mDetailScales->z = 1;
	mDetailScales->w = 1;
}

SplatManager::~SplatManager()
{
	// Free resource pointers before shutdown
	mSplatMapTexture.setNull();
	mSplatMapBuffer.setNull();


	/*** Free our heap memory ***/
	if (mDynamicTextureName)
	{
		delete mDynamicTextureName;
		mDynamicTextureName = NULL;
	}

	if (mMaterialName)
	{
		delete mMaterialName;
		mMaterialName = NULL;
	}

	if (mBufferHeight)
	{
		delete mBufferHeight;
		mBufferHeight = NULL;
	}

	if (mBufferWidth)
	{
		delete mBufferWidth;
		mBufferWidth = NULL;
	}

	if (mTerrainLengthWidth)
	{
		delete mTerrainLengthWidth;
		mTerrainLengthWidth = NULL;
	}

	if (mVbrushNames)
	{
		delete mVbrushNames;
		mVbrushNames = NULL;
	}

	if (mCurrentBrushName)
	{
		delete mCurrentBrushName;
		mCurrentBrushName = NULL;
	}

	if (mVtextureNames)
	{
		delete mVtextureNames;
		mVtextureNames = NULL;
	}

	if (mCurrentTextureName)
	{
		delete mCurrentTextureName;
		mCurrentTextureName = NULL;
	}

	if (mSplatMapFileName)
	{
		delete mSplatMapFileName;
		mSplatMapFileName = NULL;
	}

	if (mSplatScales)
	{
		delete mSplatScales;
		mSplatScales = NULL;
	}

	if (mDetailScales)
	{
		delete mDetailScales;
		mDetailScales = NULL;
	}
}

void SplatManager::setSplatMapFileName(string str)
{
	*mSplatMapFileName = str;
}

string SplatManager::getSplatMapFileName()
{
	return *mSplatMapFileName;
}


void SplatManager::setupNoDynamicTexture()
{
	if ( !mSplatMapFileName->empty() )
	{
		//string splatMaptexUnitAlias = "SplatMap";
		string splatMaptexUnitAlias = "Terrain__SplatMap__Tex__Unique";
		SplatManager::saveTexture(*mSplatMapFileName);

		//mTerrain->setMaterialSchemeParams( parameterName, *mSplatMapFileName, mTerrain->getDefaultMaterialScheme() );




		/*** Get a pointer to the material ***/
		Ogre::MaterialPtr sptMaterial = MaterialManager::getSingleton().getByName(*mMaterialName);


		/*** Change the materials splatmap texture to a dynamic texture ***/
		Material::TechniqueIterator itTech = sptMaterial->getTechniqueIterator();
		while (itTech.hasMoreElements())
		{
			Technique::PassIterator itPass = itTech.getNext()->getPassIterator();
			while (itPass.hasMoreElements())
			{
				Pass* pPass = itPass.getNext();
				TextureUnitState* pTexState = pPass->getTextureUnitState(splatMaptexUnitAlias);

				if (pTexState)
				{
					pTexState->setTextureName(*mSplatMapFileName);
				}
			}
		}
	}

}


void SplatManager::setup(Ogre::Real terrainLengthWidth,
	const unsigned long bufferWidth, const unsigned long bufferHeight,
	string materialName)
{
	Ogre::Image image;
	Ogre::uint8 alphaColor = 255;
	Ogre::uint8 redColor = 0;
	Ogre::uint8 greenColor = 0;
	Ogre::uint8 blueColor = 0;
	Ogre::uint32 int32Tmp = 0;



	/*** Set our heap class member data ***/
	*mTerrainLengthWidth = terrainLengthWidth;
	*mBufferWidth = bufferWidth;
	*mBufferHeight = bufferHeight;
	*mMaterialName = materialName;
	*mDynamicTextureName = "Terrain__SplatMap__Tex__Unique";	// Set our unique dynamic texture name;


	/*** Set the splatmapTexture Unit alias name ***/
	string splatMaptexUnitAlias = SPLATMAP_DEFAULT_TEXTUREUNIT_ALIAS;


	/*** Set the splat regular and detail scales ***/
	SplatManager::setSplatScales(*mSplatScales);
	SplatManager::setDetailScales(*mDetailScales);


	/*** Create a texture if it doesn't exist ***/
	if ( mSplatMapTexture.isNull() )
	{
		mSplatMapTexture = TextureManager::getSingleton().createManual(*mDynamicTextureName,
			ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			TEX_TYPE_2D, *mBufferWidth, *mBufferHeight,1,0, DEF_IMAGE_FORMAT, TU_DYNAMIC);
	}


	/*** Get a pointer to the buffer ***/
	mSplatMapBuffer = mSplatMapTexture->getBuffer(0, 0);


	/*** Get a pointer to the material ***/
	Ogre::MaterialPtr sptMaterial = MaterialManager::getSingleton().getByName(*mMaterialName);



	/*** Change the materials splatmap texture to a dynamic texture ***/
	Material::TechniqueIterator itTech = sptMaterial->getTechniqueIterator();
	while (itTech.hasMoreElements())
	{
		Technique::PassIterator itPass = itTech.getNext()->getPassIterator();
		while (itPass.hasMoreElements())
		{
			Pass* pPass = itPass.getNext();
			TextureUnitState* pTexState = pPass->getTextureUnitState(splatMaptexUnitAlias);

			if (pTexState)
			{
				pTexState->setTextureName(*mDynamicTextureName);
			}
		}
	}



	/*
	***	If the mSplatMapFileName string is empty then we use a single pixel color to feel in the texture,
	***	  else We load an image with the string name and blit it into the dynamic texture.
	 */

	if ( mSplatMapFileName->empty() )
	{
		Ogre::PixelUtil::packColour(alphaColor, redColor, greenColor, blueColor, DEF_IMAGE_FORMAT, &int32Tmp);

		// Feel dynamic texture with single texture pixel color;
		SplatManager::fillTextureWithSingleColor(int32Tmp);
	}
	else
	{
		// Load the texture into image memory to blit it into the dynamic texture;
		image.load(*mSplatMapFileName, ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

		// Blit the image into the dynamic texture;
		mSplatMapBuffer->blitFromMemory(image.getPixelBox(0,0));
	}
}

void SplatManager::addBrush(string name)
{
	(*mVbrushNames).push_back(name);
}

void SplatManager::removeBrush(string name)
{
	for (unsigned long i = 0; i < (*mVbrushNames).size(); i++)
	{
		// If we find the name of the brush, than remove it;
		if ( (*mVbrushNames).at(i) == name )
		{
			(*mVbrushNames).erase( (*mVbrushNames).begin() + i );

			// leave the function;
			return;
		}
	}
}

unsigned long SplatManager::getBrushCount() const
{
	return (*mVbrushNames).size();
}

bool SplatManager::setCurrentBrush(const unsigned long num)
{
	const unsigned long brushSize = (*mVbrushNames).size();

	if (brushSize == 0)
	{
		return false;
	}
	else if (num < 0 || num >= brushSize)
	{
		return false;
	}


	// Everything is fine so set the current brush name;
	*mCurrentBrushName = (*mVbrushNames).at(num);


	return true;
}

std::string* SplatManager::getCurrentBrush()
{
  return mCurrentBrushName;
}


void SplatManager::setTextures(string tex0, string tex1, string tex2, string tex3)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "void SplatManager::setTextures(...)";
	string strErrorMessage;

	Ogre::MaterialPtr sptMaterial = MaterialManager::getSingleton().getByName(*mMaterialName);

	string defaultTex0Alias = "Splat1";
	string defaultTex1Alias = "Splat2";
	string defaultTex2Alias = "Splat3";
	string defaultTex3Alias = "Splat4";



	if ( sptMaterial.isNull() )
	{
		strErrorMessage = "sptMaterial could not get a pointer to the material";
		ogre3dLogManager->logErrorMessage(strErrorFunction, strErrorMessage);
	}
	else
	{
		Material::TechniqueIterator itTech = sptMaterial->getTechniqueIterator();
		TextureUnitState* pTexState = NULL;
		Pass* pPass = NULL;

		TextureManager* textureManager = TextureManager::getSingletonPtr();


		/*** Change the textures to the ones we specify ***/
		while (itTech.hasMoreElements())
		{
			Technique::PassIterator itPass = itTech.getNext()->getPassIterator();

			while (itPass.hasMoreElements())
			{
				pPass = itPass.getNext();

				// Get and set the first texture;
				pTexState = pPass->getTextureUnitState(defaultTex0Alias );
				if (pTexState)
				{
					pTexState->setTextureName(tex0);
				}

				// Get and set the 2nd texture;
				pTexState = pPass->getTextureUnitState(defaultTex1Alias );
				if (pTexState)
				{
					pTexState->setTextureName(tex1);
				}

				// Get and set the third texture;
				pTexState = pPass->getTextureUnitState(defaultTex2Alias);
				if (pTexState)
				{
					pTexState->setTextureName(tex2);
				}

				// Get and set the fourth texture;
				pTexState = pPass->getTextureUnitState(defaultTex3Alias);
				if (pTexState)
				{
					pTexState->setTextureName(tex3);
				}
			}
		}


		/*
		*** Remove the unused old textures. Remember that if a texture is still in memory than it doesn't get removed.
		*** So we can just try to remove all, if its still in memory than its being used and isn't removed.
		 */
		for (unsigned long i = 0; i < mVtextureNames->size(); i++)
		{
			textureManager->remove( (*mVtextureNames).at(i) );
		}


		/*** Assign the new textures to mVtextureNames ***/
		mVtextureNames->clear();
		mVtextureNames->push_back(tex0);
		mVtextureNames->push_back(tex1);
		mVtextureNames->push_back(tex2);
		mVtextureNames->push_back(tex3);


		/*** For default we set the first texture as the current texture ***/
		*mCurrentTextureName  = mVtextureNames->at(0);
	}
}

vector<string>* SplatManager::getTextures()
{
	return mVtextureNames;
}

unsigned long SplatManager::getTextureCount() const
{
	return (*mVtextureNames).size();
}

bool SplatManager::setCurrentTexture(const unsigned long num)
{
	const unsigned long textureNameSize = (*mVtextureNames).size();

	if (textureNameSize == 0)
	{
		return false;
	}
	else if (num < 0 || num >= textureNameSize)
	{
		return false;
	}


	// Everything is fine so set the current texture name;
	*mCurrentTextureName  = (*mVtextureNames).at(num);


	return true;
}

void SplatManager::brush(unsigned long brushSizeX, unsigned long brushSizeY,
	const bool mirrorX, const bool mirrorY,
	long altAplhaColor, long altRedColor, long altGreenColor, long altBlueColor)
{
  brushSizeX /= 32;
  brushSizeY /= 32;

	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	const Ogre::Vector3 brushPos = terrainManager->getBrushPosition();

	const Ogre::Real lengthWidth_dividedByTwo = *mTerrainLengthWidth / static_cast<Ogre::Real>(2);
	const Ogre::Real unitPosX = lengthWidth_dividedByTwo + brushPos.x;
	const Ogre::Real unitPosY = lengthWidth_dividedByTwo + brushPos.z;


	// Get the pixelPosX
	const Ogre::Real percentX = unitPosX / ( *mTerrainLengthWidth / static_cast<Ogre::Real>(100) );
	const Ogre::Real pixelPosX = ( percentX / static_cast<Ogre::Real>(100) ) * static_cast<Ogre::Real>(*mBufferWidth);
	const unsigned long pixelRoundedPosX = static_cast<const unsigned long>(pixelPosX + 0.5);

	// Get the pixelPosY
	const Ogre::Real percentY = unitPosY / ( *mTerrainLengthWidth / static_cast<Ogre::Real>(100) );
	const Ogre::Real pixelPosY = ( percentY / static_cast<Ogre::Real>(100) ) * static_cast<Ogre::Real>(*mBufferHeight);
	const unsigned long pixelRoundedPosY = static_cast<const unsigned long>(pixelPosY + 0.5);

	// Get the tmp left and right corner;
	unsigned long tmp_x_left = pixelRoundedPosX - brushSizeX;
	unsigned long tmp_x_right = pixelRoundedPosX + brushSizeX;

	// Get the tmp up and down corner;
	unsigned long tmp_y_up = pixelRoundedPosY - brushSizeY;
	unsigned long tmp_y_down = pixelRoundedPosY + brushSizeY;


	Ogre::uint8 alphaColor = static_cast<Ogre::uint8>(altAplhaColor);
	Ogre::uint8 redColor = static_cast<Ogre::uint8>(altRedColor);
	Ogre::uint8 greenColor = static_cast<Ogre::uint8>(altGreenColor);
	Ogre::uint8 blueColor = static_cast<Ogre::uint8>(altBlueColor);
	Ogre::uint32 tmpInt32 = 0;




	/*** Before we do anything, we make sure our mouse cursor isn't out of range ***/
	if ( (percentY >= static_cast<Ogre::Real>(0.0) && percentY <= static_cast<Ogre::Real>(100.0))
		&& (percentX >= static_cast<Ogre::Real>(0.0) && percentX <= static_cast<Ogre::Real>(100.0)) )
	{
		/*** Make sure X left and right don't pass the image limit ***/
		if (tmp_x_left < 0)
		{
			tmp_x_left = 0;
		}

		if (tmp_x_right > *mBufferWidth)
		{
			tmp_x_right = *mBufferWidth;
		}


		/*** Make sure Y up and down don't pass the image limit ***/
		if (tmp_y_up < 0)
		{
			tmp_y_up = 0;
		}

		if (tmp_y_down > *mBufferHeight)
		{
			tmp_y_down = *mBufferHeight;
		}


		/*** Convert the rgba to a Ogre::uint32 ***/
		Ogre::PixelUtil::packColour(alphaColor, redColor, greenColor, blueColor, DEF_IMAGE_FORMAT, &tmpInt32);


		// If the brush size is 0 than draw one pixel, else draw multiple from the brush image;
		if (brushSizeX == 0 || brushSizeY == 0)
		{
			SplatManager::splatSinglePixel(pixelRoundedPosX, pixelRoundedPosY, tmpInt32);
		}
		else
		{
			SplatManager::splatBrushImage(brushSizeX, brushSizeY, tmp_y_up, tmp_y_down, tmp_x_left, tmp_x_right,
				tmpInt32, mirrorX, mirrorY);
		}
	}
}

void SplatManager::brush(unsigned long brushSizeX, unsigned long brushSizeY,
	const bool mirrorX, const bool mirrorY)
{
	TerrainManager* const terrainManager = EditManager::getSingletonPtr()->getTerrainManager();
	const Ogre::Vector3 brushPos = terrainManager->getBrushPosition();

	const Ogre::Real lengthWidth_dividedByTwo = *mTerrainLengthWidth / static_cast<Ogre::Real>(2);
	const Ogre::Real unitPosX = lengthWidth_dividedByTwo + brushPos.x;
	const Ogre::Real unitPosY = lengthWidth_dividedByTwo + brushPos.z;


	// Get the pixelPosX
	const Ogre::Real percentX = unitPosX / ( *mTerrainLengthWidth / static_cast<Ogre::Real>(100) );
	const Ogre::Real pixelPosX = ( percentX / static_cast<Ogre::Real>(100) ) * static_cast<Ogre::Real>(*mBufferWidth);
	const unsigned long pixelRoundedPosX = static_cast<const unsigned long>(pixelPosX + 0.5);

	// Get the pixelPosY
	const Ogre::Real percentY = unitPosY / ( *mTerrainLengthWidth / static_cast<Ogre::Real>(100) );
	const Ogre::Real pixelPosY = ( percentY / static_cast<Ogre::Real>(100) ) * static_cast<Ogre::Real>(*mBufferHeight);
	const unsigned long pixelRoundedPosY = static_cast<const unsigned long>(pixelPosY + 0.5);


	// Get the tmp left and right corner;
	unsigned long tmp_x_left = pixelRoundedPosX - brushSizeX;
	unsigned long tmp_x_right = pixelRoundedPosX + brushSizeX;

	// Get the tmp up and down corner;
	unsigned long tmp_y_up = pixelRoundedPosY - brushSizeY;
	unsigned long tmp_y_down = pixelRoundedPosY + brushSizeY;


	Ogre::uint8 alphaColor = 	0;
	Ogre::uint8 redColor = 		0;
	Ogre::uint8 greenColor = 	0;
	Ogre::uint8 blueColor = 	0;
	Ogre::uint32 tmpInt32 = 	0;


	/*** Make sure X left and right don't pass the image limit ***/
	if (tmp_x_left < 0)
	{
		tmp_x_left = 0;
	}

	if (tmp_x_right > *mBufferWidth)
	{
		tmp_x_right = *mBufferWidth;
	}


	/*** Make sure Y up and down don't pass the image limit ***/
	if (tmp_y_up < 0)
	{
		tmp_y_up = 0;
	}

	if (tmp_y_down > *mBufferHeight)
	{
		tmp_y_down = *mBufferHeight;
	}



	/*** Determine what pixel color/texture we are splatting ***/
	for (unsigned long i = 0; i < (*mVtextureNames).size(); i++)
	{
		if ( (*mVtextureNames).at(i) == *mCurrentTextureName )
		{
			switch(i)
			{
				case 0:
					{
						alphaColor = 255;
						break;
					}
				case 1:
					{
						redColor = 255;
						break;
					}
				case 2:
					{
						greenColor = 255;
						break;
					}
				case 3:
					{
						blueColor = 255;
						break;
					}
			}
		}
	}


	/*** Convert the rgba to a Ogre::uint32 ***/
	Ogre::PixelUtil::packColour(alphaColor, redColor, greenColor, blueColor, DEF_IMAGE_FORMAT, &tmpInt32);


	// If the brush size is 0 than draw one pixel, else draw multiple from the brush image;
	if (brushSizeX == 0 || brushSizeY == 0)
	{
		SplatManager::splatSinglePixel(pixelRoundedPosX, pixelRoundedPosY, tmpInt32);
	}
	else
	{
		SplatManager::splatBrushImage(brushSizeX, brushSizeY, tmp_y_up, tmp_y_down, tmp_x_left, tmp_x_right,
			 tmpInt32, mirrorX, mirrorY);
	}
}

void SplatManager::splatBrushImage(const unsigned long &brushSizeX, const unsigned long &brushSizeY,
		const unsigned long &tmp_y_top, const unsigned long &tmp_y_bottom,
		const unsigned long &tmp_x_left, const unsigned long &tmp_x_right,
		const Ogre::uint32 &int32Color, const bool &mirrorX, const bool &mirrorY)
{

	const unsigned long resizeImageX = tmp_x_right - tmp_x_left;
	const unsigned long resizeImageY = tmp_y_bottom - tmp_y_top;

	Ogre::Image image;
	unsigned long imageWidth = 0;
	unsigned long imageHeight = 0;
	vector<float> vimageData;



	// Don't do anything unless we have brushes;
	if ( (*mVbrushNames).size() > 0 )
	{
		image.load(*mCurrentBrushName,ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

		// Resize the image;
		image.resize(resizeImageX, resizeImageY);

		// Do we mirror the image around the X axis?;
		if (mirrorX)
		{
			image.flipAroundX();
		}

		// Do we mirror the image around the Y axis?;
		if (mirrorY)
		{
			image.flipAroundY();
		}

		// Store the new image width and height;
		imageWidth = image.getWidth();
		imageHeight = image.getHeight();


		/*** Store the images data in a vector ***/
		for (unsigned long int i = 0; i < imageHeight; i++)
		{
			for (unsigned long int j = 0; j < imageWidth; j++)
			{
				vimageData.push_back(image.getColourAt(j,i,0).r);
			}
		}


		mSplatMapBuffer->lock(HardwareBuffer::HBL_NORMAL );
		const PixelBox &pb = mSplatMapBuffer->getCurrentLock();

		Ogre::uint32 *data = static_cast<Ogre::uint32*>(pb.data);
		//const unsigned long& height = pb.getHeight();
		//const unsigned long& width = pb.getWidth();
		const unsigned long& pitch = pb.rowPitch; // Skip between rows of image


		unsigned long brushY = 0;
		unsigned long brushX = 0;

		/*** use left, right  up and down to set the pixel boundaries ***/
		for(unsigned long y = tmp_y_top; y < tmp_y_bottom; y++)
		{
			for(unsigned long x = tmp_x_left; x < tmp_x_right; x++)
			{
				if ( vimageData.at(brushX) == 1 )
				{
					data[pitch*y + x] = int32Color;
				}

				brushX++;

			}

			brushY++;
		}



		// Unlock the buffer again (frees it for use by the GPU)
		mSplatMapBuffer->unlock();
	}
}

void SplatManager::splatSinglePixel(const unsigned long &x, const unsigned long &y,
	Ogre::uint32 &int32Color)
{
	const unsigned long width = 1;
	const unsigned long height = 1;

	const unsigned long left = x;
	const unsigned long top = y;
	const unsigned long right = left + width;
	const unsigned long bottom = top + height;


	PixelBox pixelBox (width, height, 1, DEF_IMAGE_FORMAT, &int32Color);
	Image::Box imageBox (left, top, right, bottom);
	mSplatMapBuffer->blitFromMemory(pixelBox, imageBox);
}

void SplatManager::fillTextureWithSingleColor(const Ogre::uint32 &int32Color)
{
	mSplatMapBuffer->lock(HardwareBuffer::HBL_NORMAL );
	const PixelBox &pb = mSplatMapBuffer->getCurrentLock();

	Ogre::uint32 *data = static_cast<Ogre::uint32*>(pb.data);
	const unsigned long& height = pb.getHeight();
	const unsigned long& width = pb.getWidth();
	const unsigned long& pitch = pb.rowPitch; // Skip between rows of image


	for(unsigned long y = 0; y < height; ++y)
	{
		for(unsigned long x = 0; x < width; ++x)
		{
			data[pitch*y + x] = int32Color;
		}
	}


	mSplatMapBuffer->unlock();
}


void SplatManager::saveTexture(string fileName)
{
	Ogre::Image img;


	// Lock the buffer and get the current lock;
	mSplatMapBuffer->lock(HardwareBuffer::HBL_NORMAL );
	const PixelBox &readrefpb = mSplatMapBuffer->getCurrentLock();

	// Get a pointer to the data;
	Ogre::uchar* readrefdata = static_cast<Ogre::uchar*>(readrefpb.data);

	img = img.loadDynamicImage (readrefdata, mSplatMapTexture->getWidth(),
		mSplatMapTexture->getHeight(), mSplatMapTexture->getFormat());

	// Save the image;
	img.save(fileName);


	// Unlock the buffer;
	mSplatMapBuffer->unlock();
}

Ogre::Vector4 SplatManager::getSplatScales() const
{
	return *mSplatScales;
}

Ogre::Vector4 SplatManager::getDetailScales() const
{
	return *mDetailScales;
}

void SplatManager::setSplatScales(const Ogre::Vector4 v4)
{
	string parameterName = "vSplatScales";
	SPT::Terrain* const terrain = EditManager::getSingletonPtr()->getTerrainManager()->getTerrain();

	if (terrain)
	{
		terrain->setMaterialSchemeParams(parameterName, v4, terrain->getDefaultMaterialScheme());
	}

	*mSplatScales = v4;
}

void SplatManager::setDetailScales(const Ogre::Vector4 v4)
{
	string parameterName = "vDetailScales";
	SPT::Terrain* const terrain = EditManager::getSingletonPtr()->getTerrainManager()->getTerrain();

	if (terrain)
	{
		terrain->setMaterialSchemeParams(parameterName, v4, terrain->getDefaultMaterialScheme());
	}

	*mDetailScales = v4;
}


