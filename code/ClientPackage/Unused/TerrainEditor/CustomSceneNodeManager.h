/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CUSTOMSCENENODEMANAGER_H
#define CUSTOMSCENENODEMANAGER_H


/****************/
/*** INCLUDES ***/
/****************/
#include <Ogre.h>
#include <vector>
#include <string>
#include <iostream>
#include <set>

#include "CustomSceneNodeManipulator.h"



class CustomSceneNodeManager : public Ogre::Singleton<CustomSceneNodeManager>
{
protected:

Ogre::SceneManager* mSceneMgr;
	CustomSceneNodeManipulator* mCustomSceneNodeManipulator;

	void _destroyAllEntities(Ogre::Node *n);
	void _destroyAllSceneNodes(Ogre::SceneNode *n);

	void _removeAllEntities(Ogre::Node *n);
	void _removeAllSceneNodes(Ogre::SceneNode *n);
public:
	CustomSceneNodeManager();
	~CustomSceneNodeManager();

	static CustomSceneNodeManager* const getSingletonPtr();
	CustomSceneNodeManipulator* const getCustomSceneNodeManipulator() const;

	void dumpNodes(Ogre::Node *n, int level);
	void dumpNodes(Ogre::Node *n);

	void destroyMovableObjectsToBottom(Ogre::Node *n);
	void removeAllSceneNodeToBottom(Ogre::SceneNode *n);
	void removeAllSceneNodeToBottom_putToRoot(Ogre::SceneNode *n);
	void removeAllSceneNodeToBottom(Ogre::SceneNode *n, Ogre::SceneNode *removeToNode);

	void showBoundingBoxToBottom(Ogre::SceneNode *n, bool show);
	void showBoundingBoxTopToBottom(Ogre::SceneNode *n, bool show);

	bool isAboveSceneNode(Ogre::SceneNode *parentSceneNode, Ogre::SceneNode *childSceneNode);
	unsigned int getNumJumpsToRootSceneNode(Ogre::SceneNode *n);
	Ogre::SceneNode* getTopSceneNode(Ogre::SceneNode *n);
	bool isRootSceneNode(Ogre::SceneNode *n);

	void destroyAllSceneNodesAndMovableObjects();
	void destroyAllEntities();
	void destroyAllSceneNodes();

	void removeAllSceneNodesAndMovableObjects();
	void removeAllEntities();
	void removeAllSceneNodes();

	std::string getUniqueSceneNodeName();
	std::string getUniqueEntityName();
	std::string getUniqueLightName();

	bool getSceneNodeMovableObject(Ogre::SceneNode* const sn, std::string &objectName, const char* type);
	bool getSceneNodeEntity(Ogre::SceneNode* const sn, std::string &entName);
	bool getSceneNodeLight(Ogre::SceneNode* const sn, std::string &lightName);

	bool copySceneNode(Ogre::SceneNode* const sn, std::string &copiedSceneNodeName);

	bool getFreeAutoNameSceneNode(std::string &name);
	bool getFreeAutoNameEntity(std::string &name, std::string meshName);
	bool getFreeAutoNameLight(std::string &lightName, std::string &lightMeshName);

	bool getFreeSceneNode(std::string &name) const;
	bool getFreeEntity(std::string &name, std::string meshName) const;
	bool getFreeLight(std::string &lightName, std::string &lightMeshName) const;
	bool attachEntityToSceneNode(const std::string sceneNodeName, const std::string entityName) const;
	bool attachEntityToSceneNode(Ogre::SceneNode* const sn, Ogre::Entity* const ent) const;

	bool attachLightToSceneNode(const std::string sceneNodeName, const std::string lightName, const std::string lightMeshName) const;
	bool attachLightToSceneNode(Ogre::SceneNode* const sn, Ogre::Light* const light) const;

	bool attachObjectToSceneNode(Ogre::SceneNode* const sn, Ogre::MovableObject* const object) const;
};



#endif




