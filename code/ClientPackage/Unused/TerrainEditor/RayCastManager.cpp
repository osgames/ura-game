/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "RayCastManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"
#include "CeguiManager.h"
#include "CeguiCursorManager.h"
#include "EditManager.h"
#include "CameraManager.h"
#include "TerrainManager.h"
#include "WorldEditorProperties.h"


using namespace std;

RayCastManager::RayCastManager()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::SceneManager* const sceneManager = ogre3dManager->getSceneManager();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "RayCastManager::RayCastManager(...)";
	string strErrorMessage;



	/*** create the ray scene query object ***/
	mRaySceneQuery = sceneManager->createRayQuery(Ogre::Ray(), Ogre::SceneManager::WORLD_GEOMETRY_TYPE_MASK);

	if (!mRaySceneQuery)
	{
		strErrorMessage = "mRaySceneQuery Failed to create Ogre::RaySceneQuery instance";
		ogre3dLogManager->logErrorMessage(strErrorFunction, strErrorMessage);
	}

	mRaySceneQuery->setSortByDistance(true);
}

RayCastManager::~RayCastManager()
{

} 




// Get the mesh information for the given mesh.
// The license of this code is under the LGPL;
// Code found in Wiki: www.ogre3d.org/wiki/index.php/RetrieveVertexData
void RayCastManager::_getMeshInformation(const Ogre::MeshPtr mesh,
                                size_t &vertex_count,
                                Ogre::Vector3* &vertices,
                                size_t &index_count,
                                unsigned long* &indices,
                                const Ogre::Vector3 &position,
                                const Ogre::Quaternion &orient,
                                const Ogre::Vector3 &scale)
{
	bool added_shared = false;
	size_t current_offset = 0;
	size_t shared_offset = 0;
	size_t next_offset = 0;
	size_t index_offset = 0;

	vertex_count = index_count = 0;

	// Calculate how many vertices and indices we're going to need
	for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh( i );

		// We only need to add the shared vertices once
		if(submesh->useSharedVertices)
		{
			if( !added_shared )
			{
				vertex_count += mesh->sharedVertexData->vertexCount;
				added_shared = true;
			}
		}
		else
		{
			vertex_count += submesh->vertexData->vertexCount;
		}

		// Add the indices
		index_count += submesh->indexData->indexCount;
	}


	// Allocate space for the vertices and indices
	vertices = new Ogre::Vector3[vertex_count];
	indices = new unsigned long[index_count];

	added_shared = false;

	// Run through the submeshes again, adding the data into the arrays
	for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);

		Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

		if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared))
		{
			if(submesh->useSharedVertices)
			{
				added_shared = true;
				shared_offset = current_offset;
			}

			const Ogre::VertexElement* posElem =
			vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

			Ogre::HardwareVertexBufferSharedPtr vbuf =
			vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

			unsigned char* vertex =
			static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

			// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
			//  as second argument. So make it float, to avoid trouble when Ogre::Real will
			//  be comiled/typedefed as double:
			//      Ogre::Real* pReal;
			float* pReal;

			for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
			{
				posElem->baseVertexPointerToElement(vertex, &pReal);

				Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);

				vertices[current_offset + j] = (orient * (pt * scale)) + position;
			}

			vbuf->unlock();
			next_offset += vertex_data->vertexCount;
		}


		Ogre::IndexData* index_data = submesh->indexData;
		size_t numTris = index_data->indexCount / 3;
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

		bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

		unsigned long*  pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
		unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);


		size_t offset = (submesh->useSharedVertices) ? shared_offset : current_offset;

		if ( use32bitindexes )
		{
			for ( size_t k = 0; k < numTris*3; ++k)
			{
				indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
			}
		}
		else
		{
			for ( size_t k = 0; k < numTris*3; ++k)
			{
				indices[index_offset++] = static_cast<unsigned long>(pShort[k]) +
				static_cast<unsigned long>(offset);
			}
		}

		ibuf->unlock();
		current_offset = next_offset;
	}
}


// raycast from a point in to the scene.
// returns success or failure.
// on success the point is returned in the result.
bool RayCastManager::RaycastFromPoint(const Ogre::Vector3 &point,const Ogre::Vector3 &normal, Ogre::Vector3 &result)
{
	// The entity to check
	Ogre::Entity* pentity = NULL;

	// create the ray to test
	Ogre::Ray ray(Ogre::Vector3(point.x, point.y, point.z),
			Ogre::Vector3(normal.x, normal.y, normal.z));

	// check we are initialised
	if (mRaySceneQuery != NULL)
	{
		// create a query object
		mRaySceneQuery->setRay(ray);

		// execute the query, returns a vector of hits
		if (mRaySceneQuery->execute().size() <= 0)
		{
			// raycast did not hit an objects bounding box
			return (false);
		}
	}
	else
	{
		cout << "Cannot raycast without RaySceneQuery instance" << endl;
		return (false);
	}


	// at this point we have raycast to a series of different objects bounding boxes.
	// we need to test these different objects to see which is the first polygon hit.
	// there are some minor optimizations (distance based) that mean we wont have to
	// check all of the objects most of the time, but the worst case scenario is that
	// we need to test every triangle of every object.
	Ogre::Real closest_distance = -1.0f;
	Ogre::Vector3 closest_result;
    	Ogre::RaySceneQueryResult &query_result = mRaySceneQuery->getLastResults();


    	for (size_t qr_idx = 0; qr_idx < query_result.size(); qr_idx++)
    	{
		// stop checking if we have found a raycast hit that is closer
		// than all remaining entities
		if ((closest_distance >= 0.0f) &&
		(closest_distance < query_result[qr_idx].distance))
		{
			break;
		}

		// only check this result if its a hit against an entity
		if ((query_result[qr_idx].movable != NULL) &&
		(query_result[qr_idx].movable->getMovableType().compare("Entity") == 0))
		{
			// get the entity to check
			pentity = static_cast<Ogre::Entity*>(query_result[qr_idx].movable);
		

			// mesh data to retrieve
			size_t vertex_count;
			size_t index_count;
			Ogre::Vector3 *vertices;
			unsigned long *indices;

			// get the mesh information
			RayCastManager::_getMeshInformation(pentity->getMesh(), vertex_count, vertices, index_count, indices,
					pentity->getParentNode()->_getDerivedPosition(),
					pentity->getParentNode()->_getDerivedOrientation(),
					pentity->getParentNode()->_getDerivedScale());

			// test for hitting individual triangles on the mesh
			bool new_closest_found = false;
			for (int i = 0; i < static_cast<int>(index_count); i += 3)
			{
					// check for a hit against this triangle
					std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(ray, vertices[indices[i]],
					vertices[indices[i+1]], vertices[indices[i+2]], true, false);

					// if it was a hit check if its the closest
					if (hit.first)
					{
						if ((closest_distance < 0.0f) ||
							(hit.second < closest_distance))
						{
							// this is the closest so far, save it off
							closest_distance = hit.second;
							new_closest_found = true;
						}
					}
			}

			// free the verticies and indicies memory
			delete[] vertices;
			delete[] indices;

			// if we found a new closest raycast for this object, update the
			// closest_result before moving on to the next object.
			if (new_closest_found)
			{
				closest_result = ray.getPoint(closest_distance);
			}
		}
	}

	// return the result
	if (closest_distance >= 0.0f)
	{
		// raycast success
		result = closest_result;
		return (true);
	}
	else
	{
		// raycast failed
		return (false);
	}
}

bool RayCastManager::RaycastFromPointGetSceneNodeName(const Ogre::Vector3 &point,const Ogre::Vector3 &normal, Ogre::Vector3 &result, string &sceneNodeName)
{
	// The entity to check
	Ogre::Entity* pentity = NULL;

	// create the ray to test
	Ogre::Ray ray(Ogre::Vector3(point.x, point.y, point.z),
			Ogre::Vector3(normal.x, normal.y, normal.z));

	// check we are initialised
	if (mRaySceneQuery != NULL)
	{
		// create a query object
		mRaySceneQuery->setRay(ray);

		// execute the query, returns a vector of hits
		if (mRaySceneQuery->execute().size() <= 0)
		{
			// raycast did not hit an objects bounding box
			return (false);
		}
	}
	else
	{
		cout << "Cannot raycast without RaySceneQuery instance" << endl;
		return (false);
	}


	// at this point we have raycast to a series of different objects bounding boxes.
	// we need to test these different objects to see which is the first polygon hit.
	// there are some minor optimizations (distance based) that mean we wont have to
	// check all of the objects most of the time, but the worst case scenario is that
	// we need to test every triangle of every object.
	Ogre::Real closest_distance = -1.0f;
	Ogre::Vector3 closest_result;
    	Ogre::RaySceneQueryResult &query_result = mRaySceneQuery->getLastResults();


    	for (size_t qr_idx = 0; qr_idx < query_result.size(); qr_idx++)
    	{
		// stop checking if we have found a raycast hit that is closer
		// than all remaining entities
		if ((closest_distance >= 0.0f) &&
		(closest_distance < query_result[qr_idx].distance))
		{
			break;
		}

		// only check this result if its a hit against an entity
		if ((query_result[qr_idx].movable != NULL) &&
		    (query_result[qr_idx].movable->getMovableType().compare("Entity") == 0))
		{
			// get the entity to check
			pentity = static_cast<Ogre::Entity*>(query_result[qr_idx].movable);
		

			// mesh data to retrieve
			size_t vertex_count;
			size_t index_count;
			Ogre::Vector3 *vertices;
			unsigned long *indices;

			// get the mesh information
			RayCastManager::_getMeshInformation(pentity->getMesh(), vertex_count, vertices, index_count, indices,
					pentity->getParentNode()->_getDerivedPosition(),
					pentity->getParentNode()->_getDerivedOrientation(),
					pentity->getParentNode()->_getDerivedScale());

			// test for hitting individual triangles on the mesh
			bool new_closest_found = false;
			for (int i = 0; i < static_cast<int>(index_count); i += 3)
			{
					// check for a hit against this triangle
					std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(ray, vertices[indices[i]],
					vertices[indices[i+1]], vertices[indices[i+2]], true, false);

					// if it was a hit check if its the closest
					if (hit.first)
					{
						if ((closest_distance < 0.0f) ||
							(hit.second < closest_distance))
						{
							// this is the closest so far, save it off
							closest_distance = hit.second;
							new_closest_found = true;
						}
					}
			}

			// free the verticies and indicies memory
			delete[] vertices;
			delete[] indices;

			// if we found a new closest raycast for this object, update the
			// closest_result before moving on to the next object.
			if (new_closest_found)
			{
				closest_result = ray.getPoint(closest_distance);
			}
		}
	}

	// return the result
	if (closest_distance >= 0.0f)
	{
		// raycast success
		result = closest_result;

		/* Get the SceneNode name */
		if (pentity)
		{
			sceneNodeName = pentity->getParentSceneNode()->getName();
		}

		return (true);
	}
	else
	{
		// raycast failed
		return (false);
	}
}



bool RayCastManager::RaycastFromPointGetLightSceneNodeName(const Ogre::Vector3 &point,const Ogre::Vector3 &normal, Ogre::Vector3 &result, string &sceneNodeName)
{
	// The entity to check
	Ogre::Entity* pentity = NULL;

	// create the ray to test
	Ogre::Ray ray(Ogre::Vector3(point.x, point.y, point.z),
			Ogre::Vector3(normal.x, normal.y, normal.z));

	// check we are initialised
	if (mRaySceneQuery != NULL)
	{
		// create a query object
		mRaySceneQuery->setRay(ray);
		mRaySceneQuery->setSortByDistance(true);
		// execute the query, returns a vector of hits
		if (mRaySceneQuery->execute().size() <= 0)
		{
			// raycast did not hit an objects bounding box
			return (false);
		}
	}
	else
	{
		cout << "Cannot raycast without RaySceneQuery instance" << endl;
		return (false);
	}


	// at this point we have raycast to a series of different objects bounding boxes.
	// we need to test these different objects to see which is the first polygon hit.
	// there are some minor optimizations (distance based) that mean we wont have to
	// check all of the objects most of the time, but the worst case scenario is that
	// we need to test every triangle of every object.
    	Ogre::RaySceneQueryResult &query_result = mRaySceneQuery->getLastResults();

    	for (size_t qr_idx = 0; qr_idx < query_result.size(); qr_idx++)
    	{
		// only check this result if its a hit against an entity
		if ((query_result[qr_idx].movable != NULL) &&
		    (query_result[qr_idx].movable->getMovableType().compare("Entity") == 0) &&
		    (query_result[qr_idx].movable->getName().find(DEF_PARSE_LIGHT_MESH_NAME_START_STRING) == 0))
		{
			// get the entity to check
			pentity = static_cast<Ogre::Entity*>(query_result[qr_idx].movable);
		
			sceneNodeName = pentity->getParentSceneNode()->getName();
			return true;
		} else {
		  std::cout << "Failed" << std::endl;
		}
	}
	return false;
}






bool RayCastManager::setSceneNodeAtCursor(Ogre::SceneNode* const sn)
{
	EditManager* const editManager = EditManager::getSingletonPtr();

	CameraManager* const cameraManager = editManager->getCameraManager();
	Ogre::Camera* const camera = cameraManager->getCamera();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();	

	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();
	Ogre::Vector2 cursorPos;

	TerrainManager* const terrainManager = editManager->getTerrainManager();
	SPT::Terrain* const terrain = terrainManager->getTerrain();

	Ogre::Ray mouseRay;
	Ogre::Vector3 result;



	if ( ceguiCursorManager->getCursorScreenPosition(cursorPos) )
	{
		mouseRay = camera->getCameraToViewportRay(cursorPos.x, cursorPos.y);	

		if ( RayCastManager::RaycastFromPoint(cameraSceneNode->getPosition(), mouseRay.getDirection(), result) )
		{
			sn->setPosition(result);
			return true;
		}
		else	// Raycast from point isn't working on the terrain. So use getRayHeight especially for the terrain;
		{
			if (terrain)
			{
				if( terrain->getRayHeight(mouseRay, result) )
				{
					sn->setPosition(result);
					return true;
				}
			}
		}
	}


	return false;
}



bool RayCastManager::getSceneNodeNameAtCursor(string& sceneNodeName)
{
	EditManager* const editManager = EditManager::getSingletonPtr();

	CameraManager* const cameraManager = editManager->getCameraManager();
	Ogre::Camera* const camera = cameraManager->getCamera();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();	

	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();
	Ogre::Vector2 cursorPos;

	Ogre::Ray mouseRay;
	Ogre::Vector3 hitPos;



	if ( ceguiCursorManager->getCursorScreenPosition(cursorPos) )
	{
		mouseRay = camera->getCameraToViewportRay(cursorPos.x, cursorPos.y);	

		if ( RayCastManager::RaycastFromPointGetSceneNodeName(cameraSceneNode->getPosition(), mouseRay.getDirection(), hitPos, sceneNodeName) )
		{
			return true;
		}
	}


	return false;
}

bool RayCastManager::getLightSceneNodeNameAtCursor(string& sceneNodeName)
{
	EditManager* const editManager = EditManager::getSingletonPtr();
	CameraManager* const cameraManager = editManager->getCameraManager();
	Ogre::Camera* const camera = cameraManager->getCamera();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();	

	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();
	Ogre::Vector2 cursorPos;

	Ogre::Ray mouseRay;
	Ogre::Vector3 hitPos;



	if ( ceguiCursorManager->getCursorScreenPosition(cursorPos) )
	{
		mouseRay = camera->getCameraToViewportRay(cursorPos.x, cursorPos.y);	

		if ( RayCastManager::RaycastFromPointGetLightSceneNodeName(cameraSceneNode->getPosition(), mouseRay.getDirection(), hitPos, sceneNodeName) )
		{
			return true;
		}
	}


	return false;
}

bool RayCastManager::getPositionAtCursor(Ogre::Vector3& position)
{
	EditManager* const editManager = EditManager::getSingletonPtr();

	CameraManager* const cameraManager = editManager->getCameraManager();
	Ogre::Camera* const camera = cameraManager->getCamera();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();	

	CeguiCursorManager* const ceguiCursorManager = CeguiManager::getSingletonPtr()->getCeguiCursorManager();
	Ogre::Vector2 cursorPos;

	TerrainManager* const terrainManager = editManager->getTerrainManager();
	SPT::Terrain* const terrain = terrainManager->getTerrain();

	Ogre::Ray mouseRay;



	if ( ceguiCursorManager->getCursorScreenPosition(cursorPos) )
	{
		mouseRay = camera->getCameraToViewportRay(cursorPos.x, cursorPos.y);	

		if ( RayCastManager::RaycastFromPoint(cameraSceneNode->getPosition(), mouseRay.getDirection(), position) )
		{
			return true;
		}
		else	// Raycast from point isn't working on the terrain. So use getRayHeight especially for the terrain;
		{
			if (terrain)
			{
				if( terrain->getRayHeight(mouseRay, position) )
				{
					return true;
				}
			}
		}
	}


	return false;
}

bool RayCastManager::getHeightAtPosition(Ogre::Real x, Ogre::Real z, Ogre::Real &result)
{
	EditManager* const editManager = EditManager::getSingletonPtr();

	CameraManager* const cameraManager = editManager->getCameraManager();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();

	TerrainManager* const terrainManager = editManager->getTerrainManager();
	SPT::Terrain* const terrain = terrainManager->getTerrain();

	const Ogre::Real camSceneNodeY = cameraSceneNode->getPosition().y;
	Ogre::Vector3 pos(x, camSceneNodeY, z);
	Ogre::Ray mouseRay(pos, Ogre::Vector3::NEGATIVE_UNIT_Y);
	Ogre::Vector3 tmpResult;



	if ( RayCastManager::RaycastFromPoint(pos, mouseRay.getDirection(), tmpResult) )
	{
		result = tmpResult.y;
		return true;
	}
	else	// Raycast from point isn't working on the terrain. So use getRayHeight especially for the terrain;
	{
		if (terrain)
		{
			if( terrain->getRayHeight(mouseRay, tmpResult) )
			{
				result = tmpResult.y;
				return true;
			}
		}
	}


	return false;	
}

bool RayCastManager::getPositionUnderCameraSceneNode(Ogre::Vector3& position)
{
	EditManager* const editManager = EditManager::getSingletonPtr();

	CameraManager* const cameraManager = editManager->getCameraManager();
	Ogre::SceneNode* const cameraSceneNode = cameraManager->getCameraSceneNode();	
	const Ogre::Vector3 camSceneNodePos = cameraSceneNode->getPosition();

	TerrainManager* const terrainManager = editManager->getTerrainManager();
	SPT::Terrain* const terrain = terrainManager->getTerrain();

	Ogre::Ray mouseRay(camSceneNodePos, Ogre::Vector3::NEGATIVE_UNIT_Y);



	if ( RayCastManager::RaycastFromPoint(camSceneNodePos, mouseRay.getDirection(), position) )
	{
		return true;
	}
	else	// Raycast from point isn't working on the terrain. So use getRayHeight especially for the terrain;
	{
		if (terrain)
		{
			if( terrain->getRayHeight(mouseRay, position) )
			{
				return true;
			}
		}
	}


	return false;
}

