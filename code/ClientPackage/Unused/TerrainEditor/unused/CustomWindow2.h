/***
Copyright (C) 2008, The Ura Trinity Reign Team

This program is free software; you can redistribute it and/or modify it 
under the terms of the Artistic License 2.0.

This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the Artistic License 2.0 for more 
details.
***/



#ifndef CUSTOMWINDOW2_H
#define CUSTOMWINDOW2_H


#include <CEGUI/CEGUI.h>
#include <string>
#include <iostream>

using namespace std;
using namespace CEGUI;


class CustomWindow2
{
protected:
	vector<Window*> mOne;
	vector<Window*> mTwo;

	unsigned long mCount;
public:
	CustomWindow2()
	{
		mCount = 0;
	}

	~CustomWindow2()
	{

	}


	void add(Window* one, Window* two)
	{
		mOne.push_back(one);
		mTwo.push_back(two);
	}

	void get(unsigned long index, Window* one, Window* two)
	{
		one = mOne.at(index);
		two = mTwo.at(index);
	}


	Window* getOne(unsigned long index)
	{
		return mOne.at(index);
	}

	Window* getTwo(unsigned long index)
	{
		return mTwo.at(index);
	}


	void setOne(unsigned long index, Window* win)
	{
		mOne.at(index) = win;
	}

	void setTwo(unsigned long index, Window* win)
	{
		mTwo.at(index) = win;
	}


	unsigned long getSize()
	{
		return mCount;
	}
};

#endif

