/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef WORLD_EDITOR_PROPERTIES_H
#define WORLD_EDITOR_PROPERTIES_H



/************/
/*** MISC ***/
/************/

// Uncomment this to disable the spt lightmap code;
//#define DISABLE_SPT_LIGHTMAP_CODE


/******************/
/*** FILE PATHS ***/
/******************/

/* MAPS */
#define FILEPATH_OF_MAPS			"../../../resources/maps/"

/* MODELS */
#define FILEPATH_OF_MODELS			"../../../resources/models/"

/* SPLATMAPS */
#define FILEPATH_OF_SPLATMAPS			"../../../resources/maps/splatmaps/"

/* BRUSHES, SPLAT BRUSHES AND TEXTURES */
#define FILEPATH_OF_SPT_BRUSHES			"../../../resources/spt2_demo/brushes"
#define FILEPATH_OF_SPLAT_BRUSH			"../../../resources/spt2_demo/splatBrushes"
#define FILEPATH_OF_SPLAT_SAMPLEBRUSH		"../../../resources/spt2_demo/splatTextures/sampleTextures"
#define FILEPATH_OF_SPLAT_FULL			"../../../resources/spt2_demo/splatTextures/fullTextures"


/**************/
/*** OTHERS ***/
/**************/

/* Parsing names */
#define DEF_PARSE_SCENENODE_NAME_START_STRING	"sn_"
#define DEF_PARSE_ENTITY_NAME_START_STRING	"ent_"
#define DEF_PARSE_LIGHT_NAME_START_STRING	"lit_"
#define DEF_PARSE_LIGHT_MESH_NAME_START_STRING	"litmesh_"

/* BACKGROUND IMAGE FILENAME */
#define ROOTWINDOW_BGIMAGE_FILENAME		"EditorBGImage.jpg"

/* LIGHTING MESH NAME */
#define LIGHTING_MESH_NAME                      "Lightbulb.mesh"

/* SPLAT MAP FILE NAMING */
#define SPLATMAP_UNIQUENAME_ENDPART		"_splatmap.png"

#define SPLATMAP_SAMPLETEXTURE_FILENAME_STARTPART "sample_" 

/* DEFAULT SPLAT MAP FILE */
#define SPLATMAP_DEFAULT_FILENAME		"Highlands_Splat.png"

/* DEFAULT SPLAT MAP MATERIAL NAME */
#define SPLATMAP_DEFAULT_MATERIALNAME		"SPT_Highlands"

/* DEFAULT SPLAT MAP TEXTURE UNIT ALIAS */
#define SPLATMAP_DEFAULT_TEXTUREUNIT_ALIAS	"SplatMap"

/* DEFAULT TERRAIN HEIGHTMAP FILE NAME*/
#define TERRAIN_DEFAULT_HEIGHTMAP_FILENAME	"Highlands_Height.png"




#endif 
