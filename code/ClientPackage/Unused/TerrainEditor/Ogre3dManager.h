/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef OGRE3D_MANAGER_H
#define OGRE3D_MANAGER_H


#include <Ogre.h>
#include "Ogre3dResourceManager.h"
#include "Ogre3dTimerManager.h"
#include "Ogre3dLogManager.h"



class Ogre3dManager : public Ogre::Singleton<Ogre3dManager>
{
protected:
	Ogre3dResourceManager* mOgre3dResourceManager;
	Ogre3dTimerManager* mOgre3dTimerManager;
	Ogre3dLogManager* mOgre3dLogManager;

	Ogre::Root* mRoot;
	Ogre::SceneManager* mSceneManager;
public:
	Ogre3dManager();
	~Ogre3dManager();

	static Ogre3dManager* const getSingletonPtr();

	Ogre::Root* const getRoot();
	Ogre::SceneManager* const getSceneManager();
	Ogre::RenderWindow* const getRenderWindow();

	Ogre3dTimerManager* const getOgre3dTimerManager();
	Ogre3dLogManager* const getOgre3dLogManager();
};


#endif

