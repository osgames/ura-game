/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef OGRE3D_LOG_MANAGER_H
#define OGRE3D_LOG_MANAGER_H


#include <Ogre.h>
#include <string>



class Ogre3dLogManager
{
protected:
public:
	Ogre3dLogManager();
	~Ogre3dLogManager();

	void logMessage(const std::string &str);
	void logMessage(const std::string &functionName, const std::string &str);

	// Error logging functions;
	void logErrorMessage(const std::string &functionName, const std::string &errorMessage);
	void logErrorMemoryMessage(const std::string &functionName, const std::string &variableName);
};


#endif 

