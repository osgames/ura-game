/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GUIManager.h"
#include "Ogre3dManager.h"
#include "Ogre3dLogManager.h"


using namespace std;


template<> GUIManager* Ogre::Singleton<GUIManager>::ms_Singleton = 0;

GUIManager* const GUIManager::getSingletonPtr()
{
	return ms_Singleton;
}



GUIManager::GUIManager()
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre3dLogManager* const ogre3dLogManager = ogre3dManager->getOgre3dLogManager();
	string strErrorFunction = "GUIManager::GUIManager()";
	string strErrorWordName;

	mGUI = new GUI;
	mInput = new Input(mGUI);

	mEnabledCommandKeys = new bool;


	/*** Enable the keyboard command keys ***/
	*mEnabledCommandKeys = true;


	/*** Check for memory allocation errors ***/
	if (!mGUI)
	{
		strErrorWordName = "mGUI";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mInput)
	{
		strErrorWordName = "mInput";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}

	if (!mEnabledCommandKeys)
	{
		strErrorWordName = "mEnabledCommandKeys";
		ogre3dLogManager->logErrorMemoryMessage(strErrorFunction, strErrorWordName);
	}
} 

GUIManager::~GUIManager()
{
	if (mInput)
	{
		delete mInput;
		mInput = NULL;
	}

	if (mGUI)
	{
		delete mGUI;
		mGUI = NULL;
	}

	if (mEnabledCommandKeys)
	{
		delete mEnabledCommandKeys;
		mEnabledCommandKeys = NULL;
	}
}




Input* const GUIManager::getInput()
{
	return mInput;
}

GUI* const GUIManager::getGUI()
{
	return mGUI;
}

bool GUIManager::getEnabledCommandKeys() const
{
	return *mEnabledCommandKeys;
}

void GUIManager::setEnabledCommandKeys(bool enabled)
{
	*mEnabledCommandKeys = enabled;
}







