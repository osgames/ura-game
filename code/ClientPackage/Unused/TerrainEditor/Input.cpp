/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Input.h"
#include "Ogre3dManager.h"
#include "CeguiManager.h"


using namespace std;
using namespace Ogre;
using namespace OIS;



Input::Input (GUI* mGui) : mGUI(mGui)
{
	Ogre3dManager* const ogre3dManager = Ogre3dManager::getSingletonPtr();
	Ogre::RenderWindow* renderWindow = ogre3dManager->getRenderWindow();
	mSystem = CeguiManager::getSingletonPtr()->getSystem();


	ParamList parameters;
	ostringstream window_handle_string;
	size_t window_handle;
	unsigned int width, height, depth;
	int left, top;


	/************************************/
	/*** Setup the Gui key autoRepeat ***/
	/************************************/
	mGuiKeyAutoRepeat = new GuiKeyAutoRepeat;


	/************************************/
	/*** Setup the keyboard and mouse ***/
	/************************************/

	// Get a handle to the renderWindow (Unified now, no need for OS specific)
	renderWindow->getCustomAttribute("WINDOW", &window_handle);

	// Get the Window Paramters;
	window_handle_string << window_handle;
	parameters.insert (make_pair (string ("WINDOW"), window_handle_string.str ()));

	#if defined DEBUG
	#if defined OIS_LINUX_PLATFORM
	parameters.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
	parameters.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
	parameters.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
	parameters.insert(std::make_pair( std::string( "XAutoRepeatOn" ), std::string( "true"))); 
	#endif
	#endif


	// Initialise the OIS input manager
	mInputManager = InputManager::createInputSystem (parameters);

	// Enable buffered keyboard support
	mKeyboard = static_cast<Keyboard*>(mInputManager->createInputObject (OISKeyboard, true));

	// Enable buffered mouse support
	mMouse = static_cast<Mouse*>(mInputManager->createInputObject (OISMouse, true));


	// Define the renderWindow outline for our mouse
	renderWindow->getMetrics (width, height, depth, left, top);


	// Get the mouse state and set the width and height;
	const OIS::MouseState& mouse_state = mMouse->getMouseState ();
	mouse_state.width = width;
	mouse_state.height = height;


	// Set this class as the callback class
	mKeyboard->setEventCallback (this);
	mMouse->setEventCallback (this);
}

Input::~Input()
{
	//Destroying the manager will cleanup unfreed devices
	if( mInputManager )
	{	
		InputManager::destroyInputSystem(mInputManager);
	}

	if (mGuiKeyAutoRepeat)
	{
		delete mGuiKeyAutoRepeat;
		mGuiKeyAutoRepeat = NULL;
	}
}


bool Input::keyPressed(const OIS::KeyEvent& arg)
{
	mSystem->injectKeyDown(arg.key);
	mSystem->injectChar(arg.text);

	// Let us know a key was pressed by adding the key;
	mGuiKeyAutoRepeat->addKey(arg.key);

	return true;
}

bool Input::keyReleased(const OIS::KeyEvent& arg)
{
	mSystem->injectKeyUp( arg.key );


	// Let us know a key was released by removing the key;
	mGuiKeyAutoRepeat->removeKey(arg.key);
	
	// Alert GUI that a key was released;
	mGUI->keyReleased( &arg );


	return true;
}

bool Input::mouseMoved(const OIS::MouseEvent& arg)
{
	mSystem->injectMouseMove(arg.state.X.rel, arg.state.Y.rel);

	/*****************************/
	/*** Handle the mousewheel ***/
	/*****************************/

	// If the scroll values are bigger than or less than zero than we scroll. Else it was just a mouse move;
	if(arg.state.Z.rel > 0)
	{
		// We scroll away from the user;
		mSystem->injectMouseWheelChange(+1);

		mGUI->mouseWheelForward();
	}
	else if(arg.state.Z.rel < 0)
	{
		// Were scrowing towards the user;
		mSystem->injectMouseWheelChange(-1);

		mGUI->mouseWheelBackward();
	}

	return true;
}

bool Input::mousePressed(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	mSystem->injectMouseButtonDown( convertButton( id ) );

	// Alert GUI that a mouse was pressed;
	mGUI->mouseButtonPress( &id );

	return true;
}


bool Input::mouseReleased(const OIS::MouseEvent& arg, OIS::MouseButtonID id)
{
	mSystem->injectMouseButtonUp( convertButton( id ) );

	// Let the GUI know a mouse button was released;
	mGUI->mouseButtonRelease(&id);

	return true;
}

bool Input::update() const
{
	// Update the keyboard and mouse state
	mKeyboard->capture();
	mMouse->capture();


	// We Have the GUI check for key presses;
	mGUI->keyPressed(mGuiKeyAutoRepeat);
	

	return true;
}

bool Input::updateKeyBoard()
{
	// Update the keyboard and mouse state
	mKeyboard->capture();

	// We Have the GUI check for key presses;
	mGUI->keyPressed(mGuiKeyAutoRepeat);
	

	return true;
}

bool Input::updateMouse()
{
	mMouse->capture();
	return true;
}

OIS::Keyboard* const Input::getKeyboard() const
{
	return mKeyboard;
}

OIS::Mouse* const Input::getMouse()
{
	return mMouse;
}

CEGUI::MouseButton Input::convertButton(OIS::MouseButtonID buttonID)
{
	switch (buttonID)
	{
		case OIS::MB_Left:
			return CEGUI::LeftButton;
		
		case OIS::MB_Right:
			return CEGUI::RightButton;
		
		case OIS::MB_Middle:
			return CEGUI::MiddleButton;
		
		default:
			return CEGUI::LeftButton;
   	}
}

GuiKeyAutoRepeat* const Input::getGuiKeyAutoRepeat() const
{
	return mGuiKeyAutoRepeat;
}
