/*
Trinity Reign, an open source MMORPG.
Copyright (C) 2009-2013+ Trinity Reign Dev Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUT_H
#define INPUT_H



#include <OIS.h>
#include <CEGUI.h>
#include <Ogre.h>
#include <OgreCEGUIRenderer.h>

#include "GuiKeyAutoRepeat.h"
#include "GUI.h"


class GUI;



class Input : public OIS::KeyListener, public OIS::MouseListener
{
private:
	OIS::InputManager* mInputManager;
	OIS::Keyboard* mKeyboard;
	OIS::Mouse* mMouse;

	CEGUI::System* mSystem;

	GUI* const mGUI;
	GuiKeyAutoRepeat* mGuiKeyAutoRepeat;

	CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);
public:
	Input(GUI* mGui);
	~Input();

	bool update() const;
	bool updateKeyBoard();
	bool updateMouse();
	OIS::Keyboard* const getKeyboard() const;
	OIS::Mouse* const getMouse();

	GuiKeyAutoRepeat* const getGuiKeyAutoRepeat() const;

	// OIS related events
	virtual bool keyPressed (const OIS::KeyEvent& arg);
	virtual bool keyReleased (const OIS::KeyEvent& arg);

	virtual bool mouseMoved (const OIS::MouseEvent& arg);
	virtual bool mousePressed (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
	virtual bool mouseReleased (const OIS::MouseEvent& arg, OIS::MouseButtonID id);
};

#endif

