LIBRARIES USED:
* Ogre3d
* Cegui
* tinyxml
* sqlite
* boost
* boost file system library
* SPT - ogre Add-on "simple paged terrain", made by the talented programmer hexidave.
* Raknet
* OIS - keyboard and mouse output and input library.


NOTES:
Boost file system library doesn't like directories that start with "." as in ".svn".
So remove them in the directories resources/models, resources/maps and where-ever else needed.