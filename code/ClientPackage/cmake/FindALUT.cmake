# Try to find ALUT
#
# Once found, this will define
# ALUT_FOUND -- did we find it?
# ALUT_INCLUDE_DIRS -- path to includes
# ALUT_LIBRARIES -- libraries to link against

include(LibFindMacros)

# Dependencies
libfind_package(ALUT OpenAL)

# Include dir (use OpenAL as a hint)
find_path(ALUT_INCLUDE_DIR
	NAMES alut.h
	HINTS ${OPENAL_INCLUDE_DIR}
)


# And the library
find_library(ALUT_LIBRARY
	NAMES alut
# TODO: Fix up the library hinting logic and make it reusable
)

# Finish up
set(ALUT_PROCESS_INCLUDES ALUT_INCLUDE_DIR OPENAL_INCLUDE_DIR)
set(ALUT_PROCESS_LIBS ALUT_LIBRARY OPENAL_LIBRARY)
libfind_process(ALUT)

