# Try to find ODE
#
# Once found, this will define
# ODE_FOUND -- did we find it?
# ODE_INCLUDE_DIRS -- path to includes
# ODE_LIBRARIES -- libraries to link against

include(LibFindMacros)

# Include dir
find_path(ODE_INCLUDE_DIR
	NAMES ode.h
	PATHS /usr/include/ode
	C:/OgreSDK/include/ode
)

# And the library
find_library(ODE_LIBRARY
	NAMES ode
	PATHS C:/OgreSDK/lib
)

# Finish up
set(ODE_PROCESS_INCLUDES ODE_INCLUDE_DIR)
set(ODE_PROCESS_LIBS ODE_LIBRARY)
libfind_process(ODE)

